<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
        //parent::Model();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->database();
    }

    function clear_cache() {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    function login($credential) {
            // Check The Credentials
            $query = $this->db->get_where('admin' , $credential);
            // If User Exists
            if ($query->num_rows() > 0) {
                
                $row = $query->row();
                $this->session->set_userdata('admin_login' , '1');
                $this->session->set_userdata('login_type' , 'admin');
                $this->session->set_userdata('login_id' , $row->id);
                $this->session->set_userdata('company_id' , $row->id);
                $this->session->set_userdata('login_email' , $row->email);
                $this->session->set_userdata('login_admin_name' , $row->name);
                return 1;
        }
        else{
            $this->session->set_userdata('error_message' , 'Invalid Email & Password.');
            return 0;
        }  
    }
    
    function getdata(){
        $this->db->select('*');
        $this->db->from('users');
        $query = $this->db->get();
        return $query->result();
    }

    function getdata2($table_name){
        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query;
    }
    
    function getdatawhere($table_name,$where){
        $query = $this->db->get_where($table_name , $where);
        return $query->result();
    }

    function getdatawhere2($table_name,$where){
        $query = $this->db->get_where($table_name , $where);
        return $query;
    }
    
    function insert_data($table_name,$data)
    {
       $this->db->insert($table_name, $data);
       $insert_id = $this->db->insert_id();
       return  $insert_id;
    }
    
    function runCustomQuery($query)
    {
        return $this->db->query($query)->result();
    }
    
    function update_data($table_name,$data,$where)
    {
        $this->db->where($where);
        $this->db->update($table_name,$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return 1;
        }
        else 
        {
            return 0;
        }
    }
    
    function delete_data($table_name,$where)
    {
        $this->db->where($where);
        $this->db->delete($table_name);
        
        if($this->db->affected_rows() != 1) 
        {
           return 1;
        }
        else 
        {
            return 0;
        }
    }

    public function postCURL($_url, $_param){

        $postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v) 
        { 
          $postData .= $k . '='.$v.'&'; 
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}