
<!doctype html>
<html lang="en" dir="ltr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" href="<?php echo base_url(); ?>assets/images/Favicon.ico" type="image/x-icon"/>

<title>Ezdat : HRMS</title>

<!-- Bootstrap Core and vandor -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/toastr.min.css">
<!-- Core css -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/common.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme2.css"/>

</head>
<body class="font-montserrat">

<div class="auth">
    <div class="auth_left">
        <div class="card">
            <div class="text-center mb-5">
                <a class="header-brand" href="#"><img src="<?php echo base_url(); ?>assets/images/ezdat-logo.png" alt="Ezdat Logo"></a>
            </div>
            <form action="#" id="resetpassword" method="POST">
                <div class="card-body">
                    <div class="card-title">Forgot password</div>
                    <p class="text-muted">Enter your email and your new password.</p>
                 
                     <span id="editresetpass" class="text-danger"></span>
                    <div class="form-group">
                        <label class="form-label">New Password</label>
                        <input type="password" id="resetpass" name="resetpass" class="form-control" placeholder="Enter email">
                    </div>
                     <span id="editresetconfirmpass" class="text-danger"></span>
                    
                    <div class="form-group">
                        <label class="form-label">Confirm New Password</label>
                        <input type="password" id="resetconfirmpass" name="resetconfirmpass" class="form-control"placeholder="Enter email">
                        
                    </div>
                       
                    <span id="message" class="text-danger"></span>
                   
                    <div class="form-footer">
                        <button type="submit" id="editchange" class="btn btn-primary btn-block">Change Password</button>
                    </div>
                </div>
            </form>
        </div>      
    </div>
    <div class="auth_right">
        <div class="carousel slide" data-ride="carousel" data-interval="3000">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/login/slider1.svg" class="img-fluid" alt="login page" />
                    <div class="px-4 mt-4">
                        <h4>Fully Responsive</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/login/slider2.svg" class="img-fluid" alt="login page" />
                    <div class="px-4 mt-4">
                        <h4>Quality Code and Easy Customizability</h4>
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/login/slider3.svg" class="img-fluid" alt="login page" />
                    <div class="px-4 mt-4">
                        <h4>Cross Browser Compatibility</h4>
                        <p>Overview We're a group of women who want to learn JavaScript.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 // Reset'  password 
<script src="<?php echo base_url(); ?>assets/js/toastr.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>assets/js/lib.vendor.bundle.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    
      $("#resetpassword").submit(function(e) {
            
                    var formData = {
                        'user_id'        : localStorage.userid,
                        'email'          :        localStorage.emailid,
                        'password'         :  $('input[name=resetpass]').val(),
                        'confirm_password'        :  $('input[name=resetconfirmpass]').val()
                    };
                    $('#message').css('color', 'rgba(255, 165, 0, 0.98)').text('Please Wait...').show("slow");
            e.preventDefault();
                    $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url()?>api/callApi/resetPassword',
                            data: formData, 
                            dataType: 'json',
                            encode: true,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Token', localStorage.token);
                            }
                        })
                        .done(function(response) {
                            console.log(response);
                            
                          var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
               
                $('#message').css('color', 'rgba(255, 0, 0, 0.87)').text(jsonData).show("slow");	
				      	setTimeout(function(){window.location ="<?php echo base_url()?>willy/changePassword"},1000);
                  
                } else {
                    $('#message').css('color', 'rgba(255, 0, 0, 0.87)').text(falsedata).show("slow");		
                }
                        });
                    
                });
                
                 // Validation
                $(document).ready(function() {
                    $("#editchange").click(function(e) {
                        e.stopPropagation();
                        var errorCount = 0;
                   
                         if ($("#resetpass").val().trim() == '') {
                            $("#resetpass").focus();
                            $("#editresetpass").text("This field can't be empty.");
                            errorCount++;
                        } 
                        else {
                            $("#editresetpass").text("");
                        }
                        
                        if ($("#resetconfirmpass").val().trim() == '') {
                            $("#resetconfirmpass").focus();
                            $("#editresetconfirmpass").text("This field can't be empty.");
                            errorCount++;
                        } 
                        else {
                            $("#editresetconfirmpass").text("");
                        }

                         
                      
                      
                        if(errorCount > 0){
                            return false;
                        }
                    });
                });
             
</script>
 
</body>
</html>
