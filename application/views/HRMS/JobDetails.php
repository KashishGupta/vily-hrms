<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mt-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>JobPublic_emp">Jobs Public</a></li>
                <li class="breadcrumb-item active">Job Details</li>
            </ul>
        </div>
    </div>
</div>
<div class="section-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="job-info job-widget">
                    <h3 class="job-title" id="jobname"></h3>
                    <ul class="job-post-det">
                        <li><i class="fa fa-calendar"></i> Post Date: <span class="text-blue" id="startdate"></span></li>
                        <li><i class="fa fa-calendar"></i> Last Date: <span class="text-danger" id="enddate"></span></li>
                    </ul>
                </div>
                <div class="job-content job-widget">
                    <div class="job-desc-title"><h4>Job Description</h4></div>
                    <div class="job-description">
                        <p id="jobdescription"></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 kt-widget__items"></div>
        </div>
    </div>
    <!-- /Page Content -->
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    // Show Job list
       var job_id = <?php echo $jobId; ?>;
       $.ajax({
            url: base_url+"viewJobPublic",
            data: {job_id: job_id },
            type: "POST",
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
       })
       .done(function(response)
        {
            var startDate = new Date(response.data[0].start_date);
            var dd = String(startDate.getDate()).padStart(2, '0');
            var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = startDate.getFullYear();

            startDate = dd + '-' + mm + '-' + yyyy;
            var endDate = new Date(response.data[0].end_date);
            var dd = String(endDate.getDate()).padStart(2, '0');
            var mm = String(endDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = endDate.getFullYear();

            endDate = dd + '-' + mm + '-' + yyyy;
            $("#jobname").html(response.data[0].job_title);
            $("#startdate").html(startDate);
            $("#enddate").html(endDate);
            //$("#jobtype").html(response.data[0].job_type);
            //$("#vacancy").html(response.data[0].no_of_post);
            $("#jobstatus").val(response.data[0].job_status);
            //$("#location").html(response.data[0].branch_name);
            $("#jobdescription").html(response.data[0].job_description);
            $("#applyjobid").html(response.data[0].job_id);
            localStorage.removeItem('jobId');
        });

    //Show Employee
        var job_id = <?php echo $jobId; ?>;
        $.ajax({
                url: base_url + "viewJobPublic",
                data: {job_id : job_id },
                type: "POST",
                dataType: 'json',
                encode: true,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response) {
                for (i in response.data) {
                    var html =' <div class="job-det-info job-widget"><a class="btn job-btn" href="<?php echo base_url() ?>JobApply_emp/' + response.data[i].job_id + ' ">Referral Now</a><div class="info-list"><span><i class="fa fa-chart-bar"></i></span><h5>Job Type</h5> <p>' + response.data[i].job_type + '</p></div> <div class="info-list"><span><i class="fa fa-clipboard-check"></i></span><h5>Vacancy</h5><p>' + response.data[i].no_of_post + '</p> </div> <div class="info-list"><span><i class="fa fa-university"></i></span></span><h5>Key Skills </h5><p>' + response.data[i].key_skill + '</p> </div> <div class="info-list"><span><i class="fa fa-map-signs"></i></span><h5>Location</h5><p>' + response.data[i].branch_name + '</p> </div> </div>';
                    $('.kt-widget__items').append(html);
                }
            });
</script>
