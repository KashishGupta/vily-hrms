<style type="text/css">

.check {
	display: block;
	margin: 0;
	padding: 0;
	width: 0;
	height: 0;
	visibility: hidden;
	opacity: 0;
	pointer-events: none;
	position: absolute;
}
.checktoggle {
    background-color: #e0001a;
    border-radius: 12px;
    cursor: pointer;
    display: block;
    font-size: 0;
    height: 24px;
    margin-bottom: 0;
    position: relative;
    width: 48px;
}
.checktoggle:after {
	content: ' ';
	display: block;
	position: absolute;
	top: 50%;
	left: 0;
	transform: translate(5px, -50%);
	width: 16px;
	height: 16px;
	background-color: #fff;
	border-radius: 50%;
	transition: left 300ms ease, transform 300ms ease;
}
.check:checked + .checktoggle {
	background-color: #55ce63;
}
.check:checked + .checktoggle:after {
	left: 100%;
	transform: translate(calc(-100% - 5px), -50%);
}
.onoffswitch {
	margin-left: auto;
    position: relative;
	width: 73px;
    -webkit-user-select:none;
	-moz-user-select:none;
	-ms-user-select: none;
}
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; 
	overflow: hidden; 
	cursor: pointer;
	border-radius: 20px;
	margin-bottom: 0;
}
.onoffswitch-inner {
	display: block;
    margin-left: -100%;
    transition: margin 0.3s ease-in 0s;
    width: 200%;
}
.onoffswitch-inner:before, .onoffswitch-inner:after {
    box-sizing: border-box;
    color: #fff;
    display: block;
    float: left;
    font-size: 16px;
    height: 30px;
    line-height: 32px;
    padding: 0;
    width: 50%;
}
.onoffswitch-inner:before {
	background-color: #55ce63;
    color: #fff;
    content: "ON";
    padding-left: 14px;
}
.onoffswitch-inner:after {
    content: "OFF";
    padding-right: 14px;
    background-color: #ccc;
    color: #fff;
    text-align: right;
}
.onoffswitch-switch {
    background: #fff;
    border-radius: 20px;
    bottom: 0;
    display: block;
	height: 20px;
    margin: 5px;
    position: absolute;
    right: 43px;
    top: 0;
    transition: all 0.3s ease-in 0s;
    width: 20px;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
    right: 0px; 
}


.leave-item {
    max-width: 653px;
}
.leave-row {
    display: flex;
    margin-bottom: 15px;
}
.carry-forward-itemview {
    margin-bottom: 15px;
}
.earned-leave-itemview {
    margin-bottom: 15px;
}
.leave-row .leave-left {
    flex: 1 1 auto;
}
.leave-row .leave-left .input-box {
    max-width: 410px;
}
.leave-item .form-group {
    margin-bottom: 0;
}
.leave-right {
    align-items: center;
    display: flex;
    flex: 0 0 200px;
    justify-content: end;
    margin-top: 31px;
    min-height: 44px;
    text-align: right;
}
.leave-right .btn {
    min-height: 44px;
    min-width: 75px;
}
.leave-right .btn + .btn {
    margin-left: 10px;
}
.leave-edit-btn {
    color: #216ef4;
    background-color: transparent;
    border: 0;
    padding: 0 6px;
    transition: unset;
}
.leave-edit-btn[disabled] {
    cursor: not-allowed;
    opacity: 0.65;
}
.leave-inline-form {
    align-items: center;
    display: flex;
    min-height: 44px;
}
.leave-header {
    align-items: center;
    color: #212536;
    display: flex;
    justify-content: space-between;
    margin-bottom: 20px;
}
.leave-header .title {
    flex: 1 1 auto;
}
.leave-header .leave-action {
    flex: 1 1 auto;
    text-align: right;
}
.leave-table .l-name {
    width: 200px;
}
.leave-table .l-days {
    width: 140px;
}
.leave-box .subtitle {
    color: #8e8e8e;
    font-size: 14px;
    font-weight: normal;
    margin: 5px auto 0 5px;
}
.leave-duallist {
    background-color: #f9f9f9;
    border: 1px solid #e3e3e3;
    display: block;
    padding: 15px;
}
.card-title.with-switch {
    display: flex;
    justify-content: space-between;
}
.btn.leave-delete-btn {
    font-weight: 500;
    margin-left: 10px;
    min-height: 30px;
    padding: 2px 15px;
}

</style>

<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Leave Setting</li>
            </ul>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-body">
                <div class="row">
                        <div class="col-md-12">
                        
                            <!-- Annual Leave -->
                            <div class="card leave-box" id="leave_annual">
                                <div class="card-body">
                                    <div class="h3 card-title with-switch">
                                        Annual                                          
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_annual" checked="">
                                            <label class="onoffswitch-label" for="switch_annual">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="leave-item">
                                    
                                        <!-- Annual Days Leave -->
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <div class="form-group">
                                                        <label>Days</label>
                                                        <input type="text" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">Edit</button>
                                            </div>
                                        </div>
                                        <!-- /Annual Days Leave -->
                                        
                                        <!-- Carry Forward -->
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <label class="d-block">Carry forward</label>
                                                    <div class="leave-inline-form">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="carry_no" value="option1" disabled="">
                                                            <label class="form-check-label" for="carry_no">No</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="carry_yes" value="option2" disabled="">
                                                            <label class="form-check-label" for="carry_yes">Yes</label>
                                                        </div>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Max</span>
                                                            </div>
                                                            <input type="text" class="form-control" disabled="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /Carry Forward -->
                                        
                                        <!-- Earned Leave -->
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <label class="d-block">Earned leave</label>
                                                    <div class="leave-inline-form">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="earned_no" value="option1" disabled="">
                                                            <label class="form-check-label" for="earned_no">No</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="earned_yes" value="option2" disabled="">
                                                            <label class="form-check-label" for="earned_yes">Yes</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /Earned Leave -->
                                        
                                    </div>
                                    
                                    <!-- Custom Policy -->
                                    <div class="custom-policy">
                                        <div class="leave-header">
                                            <div class="title">Custom policy</div>
                                            <div class="leave-action">
                                                <button class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-target="#add_custom_policy"><i class="fa fa-plus"></i> Add custom policy</button>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-hover table-nowrap leave-table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th class="l-name">Name</th>
                                                        <th class="l-days">Days</th>
                                                        <th class="l-assignee">Assignee</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5 Year Service </td>
                                                        <td>5</td>
                                                        <td>
                                                            <a href="#" class="avatar"><img alt="" src="assets/img/profiles/avatar-02.jpg"></a>
                                                            <a href="#">John Doe</a>
                                                        </td>
                                                        <td class="text-right">
                                                            <div class="dropdown dropdown-action">
                                                                <a aria-expanded="false" data-toggle="dropdown" class="action-icon dropdown-toggle" href="#"><i class="material-icons">more_vert</i></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#edit_custom_policy"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#delete_custom_policy"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /Custom Policy -->
                                    
                                </div>
                            </div>
                            <!-- /Annual Leave -->
                            
                            <!-- Sick Leave -->
                            <div class="card leave-box" id="leave_sick">
                                <div class="card-body">
                                    <div class="h3 card-title with-switch">
                                        Sick                                            
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_sick" checked="">
                                            <label class="onoffswitch-label" for="switch_sick">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="leave-item">
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <div class="form-group">
                                                        <label>Days</label>
                                                        <input type="text" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Sick Leave -->
                            
                            <!-- Hospitalisation Leave -->
                            <div class="card leave-box" id="leave_hospitalisation">
                                <div class="card-body">
                                    <div class="h3 card-title with-switch">
                                        Hospitalisation                                             
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_hospitalisation">
                                            <label class="onoffswitch-label" for="switch_hospitalisation">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="leave-item">
                                    
                                        <!-- Annual Days Leave -->
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <div class="form-group">
                                                        <label>Days</label>
                                                        <input type="text" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn" disabled="">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /Annual Days Leave -->
                                        
                                    </div>
                                    
                                    <!-- Custom Policy -->
                                    <div class="custom-policy">
                                        <div class="leave-header">
                                            <div class="title">Custom policy</div>
                                            <div class="leave-action">
                                                <button class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-target="#add_custom_policy" disabled=""><i class="fa fa-plus"></i> Add custom policy</button>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-hover table-nowrap leave-table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th class="l-name">Name</th>
                                                        <th class="l-days">Days</th>
                                                        <th class="l-assignee">Assignee</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5 Year Service </td>
                                                        <td>5</td>
                                                        <td>
                                                            <a href="#" class="avatar"><img alt="" src="assets/img/profiles/avatar-02.jpg"></a>
                                                            <a href="#">John Doe</a>
                                                        </td>
                                                        <td class="text-right">
                                                            <div class="dropdown dropdown-action">
                                                                <a aria-expanded="false" data-toggle="dropdown" class="action-icon dropdown-toggle" href="#"><i class="material-icons">more_vert</i></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="#" class="dropdown-item"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                                    <a href="#" class="dropdown-item"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /Custom Policy -->
                                    
                                </div>
                            </div>
                            <!-- /Hospitalisation Leave -->
                            
                            <!-- Maternity Leave -->
                            <div class="card leave-box" id="leave_maternity">
                                <div class="card-body">
                                    <div class="h3 card-title with-switch">
                                        Maternity  <span class="subtitle">Assigned to female only</span>
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_maternity" checked="">
                                            <label class="onoffswitch-label" for="switch_maternity">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="leave-item">
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <div class="form-group">
                                                        <label>Days</label>
                                                        <input type="text" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Maternity Leave -->
                            
                            <!-- Paternity Leave -->
                            <div class="card leave-box" id="leave_paternity">
                                <div class="card-body">
                                    <div class="h3 card-title with-switch">
                                        Paternity  <span class="subtitle">Assigned to male only</span>
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_paternity">
                                            <label class="onoffswitch-label" for="switch_paternity">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="leave-item">
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <div class="form-group">
                                                        <label>Days</label>
                                                        <input type="text" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn" disabled="">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Paternity Leave -->
                            
                            <!-- Custom Create Leave -->
                            <div class="card leave-box mb-0" id="leave_custom01">
                                <div class="card-body">
                                    <div class="h3 card-title with-switch">
                                        LOP                                             
                                        <div class="onoffswitch">
                                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch_custom01" checked="">
                                            <label class="onoffswitch-label" for="switch_custom01">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                        <button class="btn btn-danger leave-delete-btn" type="button">Delete</button>
                                    </div>
                                    <div class="leave-item">
                                    
                                        <!-- Annual Days Leave -->
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <div class="form-group">
                                                        <label>Days</label>
                                                        <input type="text" class="form-control" disabled="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">Edit</button>
                                            </div>
                                        </div>
                                        <!-- /Annual Days Leave -->
                                        
                                        <!-- Carry Forward -->
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <label class="d-block">Carry forward</label>
                                                    <div class="leave-inline-form">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="carryForward" id="carry_no_01" value="option1" disabled="">
                                                            <label class="form-check-label" for="carry_no_01">No</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="carryForward" id="carry_yes_01" value="option2" disabled="">
                                                            <label class="form-check-label" for="carry_yes_01">Yes</label>
                                                        </div>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Max</span>
                                                            </div>
                                                            <input type="text" class="form-control" disabled="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /Carry Forward -->
                                        
                                        <!-- Earned Leave -->
                                        <div class="leave-row">
                                            <div class="leave-left">
                                                <div class="input-box">
                                                    <label class="d-block">Earned leave</label>
                                                    <div class="leave-inline-form">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" disabled="">
                                                            <label class="form-check-label" for="inlineRadio1">No</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" disabled="">
                                                            <label class="form-check-label" for="inlineRadio2">Yes</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="leave-right">
                                                <button class="leave-edit-btn">
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                        <!-- /Earned Leave -->
                                        
                                    </div>
                                    
                                    <!-- Custom Policy -->
                                    <div class="custom-policy">
                                        <div class="leave-header">
                                            <div class="title">Custom policy</div>
                                            <div class="leave-action">
                                                <button class="btn btn-sm btn-primary" type="button" data-toggle="modal" data-target="#add_custom_policy"><i class="fa fa-plus"></i> Add custom policy</button>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-hover table-nowrap leave-table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th class="l-name">Name</th>
                                                        <th class="l-days">Days</th>
                                                        <th class="l-assignee">Assignee</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5 Year Service </td>
                                                        <td>5</td>
                                                        <td>
                                                            <a href="#" class="avatar"><img alt="" src="assets/img/profiles/avatar-02.jpg"></a>
                                                            <a href="#">John Doe</a>
                                                        </td>
                                                        <td class="text-right">
                                                            <div class="dropdown dropdown-action">
                                                                <a aria-expanded="false" data-toggle="dropdown" class="action-icon dropdown-toggle" href="#"><i class="material-icons">more_vert</i></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#edit_custom_policy"><i class="fa fa-pencil m-r-5"></i> Edit</a>
                                                                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#delete_custom_policy"><i class="fa fa-trash-o m-r-5"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /Custom Policy -->
                                    
                                </div>
                            </div>
                            <!-- /Custom Create Leave -->
                            
                        </div>
                    </div>
            </div>
            <!-- /Page Content -->

            <!-- Add Custom Policy Modal -->
            <div id="add_custom_policy" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Add Custom Policy</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label>Policy Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>Days <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" />
                                </div>
                                <div class="form-group leave-duallist">
                                    <label>Add employee</label>
                                    <div class="row">
                                        <div class="col-lg-5 col-sm-5">
                                            <select name="customleave_from" id="customleave_select" class="form-control" size="5" multiple="multiple">
                                                <option value="1">Bernardo Galaviz </option>
                                                <option value="2">Jeffrey Warden</option>
                                                <option value="2">John Doe</option>
                                                <option value="2">John Smith</option>
                                                <option value="3">Mike Litorus</option>
                                            </select>
                                        </div>
                                        <div class="multiselect-controls col-lg-2 col-sm-2">
                                            <button type="button" id="customleave_select_rightAll" class="btn btn-block btn-white"><i class="fa fa-forward"></i></button>
                                            <button type="button" id="customleave_select_rightSelected" class="btn btn-block btn-white"><i class="fa fa-chevron-right"></i></button>
                                            <button type="button" id="customleave_select_leftSelected" class="btn btn-block btn-white"><i class="fa fa-chevron-left"></i></button>
                                            <button type="button" id="customleave_select_leftAll" class="btn btn-block btn-white"><i class="fa fa-backward"></i></button>
                                        </div>
                                        <div class="col-lg-5 col-sm-5">
                                            <select name="customleave_to" id="customleave_select_to" class="form-control" size="8" multiple="multiple"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="submit-section">
                                    <button class="btn btn-primary submit-btn">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Add Custom Policy Modal -->

            <!-- Edit Custom Policy Modal -->
            <div id="edit_custom_policy" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Custom Policy</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label>Policy Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="LOP" />
                                </div>
                                <div class="form-group">
                                    <label>Days <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="4" />
                                </div>
                                <div class="form-group leave-duallist">
                                    <label>Add employee</label>
                                    <div class="row">
                                        <div class="col-lg-5 col-sm-5">
                                            <select name="edit_customleave_from" id="edit_customleave_select" class="form-control" size="5" multiple="multiple">
                                                <option value="1">Bernardo Galaviz </option>
                                                <option value="2">Jeffrey Warden</option>
                                                <option value="2">John Doe</option>
                                                <option value="2">John Smith</option>
                                                <option value="3">Mike Litorus</option>
                                            </select>
                                        </div>
                                        <div class="multiselect-controls col-lg-2 col-sm-2">
                                            <button type="button" id="edit_customleave_select_rightAll" class="btn btn-block btn-white"><i class="fa fa-forward"></i></button>
                                            <button type="button" id="edit_customleave_select_rightSelected" class="btn btn-block btn-white"><i class="fa fa-chevron-right"></i></button>
                                            <button type="button" id="edit_customleave_select_leftSelected" class="btn btn-block btn-white"><i class="fa fa-chevron-left"></i></button>
                                            <button type="button" id="edit_customleave_select_leftAll" class="btn btn-block btn-white"><i class="fa fa-backward"></i></button>
                                        </div>
                                        <div class="col-lg-5 col-sm-5">
                                            <select name="customleave_to" id="edit_customleave_select_to" class="form-control" size="8" multiple="multiple"></select>
                                        </div>
                                    </div>
                                </div>

                                <div class="submit-section">
                                    <button class="btn btn-primary submit-btn">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Edit Custom Policy Modal -->

            <!-- Delete Custom Policy Modal -->
            <div class="modal custom-modal fade" id="delete_custom_policy" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-header">
                                <h3>Delete Custom Policy</h3>
                                <p>Are you sure want to delete?</p>
                            </div>
                            <div class="modal-btn delete-action text-right">
                                <a href="javascript:void(0);" class="btn btn-primary continue-btn">Delete</a>
                                <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Delete Custom Policy Modal -->
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
    // Leave Settings button show
    
    $(document).on('click', '.leave-edit-btn', function() {
        $(this).removeClass('leave-edit-btn').addClass('btn btn-white leave-cancel-btn').text('Cancel');
        $(this).closest("div.leave-right").append('<button class="btn btn-primary leave-save-btn" type="submit">Save</button>');
        $(this).parent().parent().find("input").prop('disabled', false);
        return false;
    });
    $(document).on('click', '.leave-cancel-btn', function() {
        $(this).removeClass('btn btn-white leave-cancel-btn').addClass('leave-edit-btn').text('Edit');
        $(this).closest("div.leave-right").find(".leave-save-btn").remove();
        $(this).parent().parent().find("input").prop('disabled', true);
        return false;
    });
    
    $(document).on('change', '.leave-box .onoffswitch-checkbox', function() {
        var id = $(this).attr('id').split('_')[1];
        if ($(this).prop("checked") == true) {
            $("#leave_"+id+" .leave-edit-btn").prop('disabled', false);
            $("#leave_"+id+" .leave-action .btn").prop('disabled', false);
        }
        else {
            $("#leave_"+id+" .leave-action .btn").prop('disabled', true);   
            $("#leave_"+id+" .leave-cancel-btn").parent().parent().find("input").prop('disabled', true);
            $("#leave_"+id+" .leave-cancel-btn").closest("div.leave-right").find(".leave-save-btn").remove();
            $("#leave_"+id+" .leave-cancel-btn").removeClass('btn btn-white leave-cancel-btn').addClass('leave-edit-btn').text('Edit');
            $("#leave_"+id+" .leave-edit-btn").prop('disabled', true);
        }
    });
    
    $('.leave-box .onoffswitch-checkbox').each(function() {
        var id = $(this).attr('id').split('_')[1];
        if ($(this).prop("checked") == true) {
            $("#leave_"+id+" .leave-edit-btn").prop('disabled', false);
            $("#leave_"+id+" .leave-action .btn").prop('disabled', false);
        }
        else {
            $("#leave_"+id+" .leave-action .btn").prop('disabled', true);   
            $("#leave_"+id+" .leave-cancel-btn").parent().parent().find("input").prop('disabled', true);
            $("#leave_"+id+" .leave-cancel-btn").closest("div.leave-right").find(".leave-save-btn").remove();
            $("#leave_"+id+" .leave-cancel-btn").removeClass('btn btn-white leave-cancel-btn').addClass('leave-edit-btn').text('Edit');
            $("#leave_"+id+" .leave-edit-btn").prop('disabled', true);
        }
    });
</script>