        <div class="section-body">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center ">
                    <ul class="breadcrumb mt-3 mb-0">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>onboardingList">On Boarding</a></li>
						<li class="breadcrumb-item active">View Document</li>
					</ul>
                </div>

                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">View Offer Letter</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover " id="users_table">
                            <tbody  class="kt-widget__offer">
                               
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">View Personal Document</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover " id="persoanltable">
                            <tbody class="kt-widget__personal">
                               
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">View Experience</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover " id="expieriencetabl">
                            <tbody class="tableexpierience">
                               
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">View Other Document</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover " id="othertable">
                             <thead>
                        <tr>
                           
                            <th class="text-left">Document Name</th>
                            <th class="text-left">Description</th>
                            <th class="text-right">Action</th>
                        </tr>
                        </thead>
                            <tbody class="tableotherCopde">
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
        </div> 
        
        
        <!-- Edit Offer Letter Modal -->
        <div class="modal custom-modal fade" id="edit_offer" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Offer Letter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="editoffer" method="POST">
                            <div class="form-group">
                                <label>Offer Letter<span class="text-danger">*</span></label>
                                <input type="hidden" value="" id="editofferletterid" name="editofferletterid">
                                <input type="file" id="resumefile"  class="dropify form-control" onchange="loading_file();" />
                                <input type="hidden" id="23arrdata">
                                </div>
                            <div class="submit-section pull-right">
                                <button id="editbranchbutton" class="btn btn-success submit-btn">Update Changes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit Offer Letter Modal -->
        
        <!-- Edit Personal Letter Modal -->
        <div class="modal custom-modal fade" id="edit_personal" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Personal Letter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="editpersonalletter" method="POST" action="#">
                            <div class="form-group">
                                <label>Personal Letter<span class="text-danger">*</span></label>
                                <input type="text" value="" id="editpersonalletterid" name="editpersonalletterid">
                                <input type="file" id="resumefilepersonal"  class="dropify form-control" onchange="loadingpersonal_file();" />
                                <input type="hidden" id="23arrdatapersonal">
                            </div>
                            <div class="submit-section pull-right">
                                <button id="#" class="btn btn-success submit-btn">Update Changes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit Personal Letter Modal -->
        
        <!-- Edit Experience Letter Modal -->
        <div class="modal custom-modal fade" id="edit_experience" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Experience Letter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="editexperienceletterform" method="POST">
                            <div class="form-group">
                                <label>Experience Letter<span class="text-danger">*</span></label>
                                <input type="hidden" value="" id="editexperienceletterid" name="editexperienceletterid">
                                <input type="file" id="resumefileexperience"  class="dropify form-control" onchange="loadingexperience_file();" />
                                <input type="hidden" id="23arrdataexperience">
                            </div>
                            <div class="submit-section pull-right">
                                <button id="#" class="btn btn-success submit-btn">Update Changes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit Experience Letter Modal -->
        
        <!-- Edit Other Letter Modal -->
        <div class="modal custom-modal fade" id="edit_other" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Other Letter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="editotherletterform" method="POST">
                            <div class="form-group">
                                <label>Other Letter<span class="text-danger">*</span></label>
                                <input type="hidden" value="" id="editotherletterid" name="editotherletterid">
                                <input type="file" id="resumefileother"  class="dropify form-control" onchange="loadingother_file();" />
                                <input type="hidden" id="23arrdataother">
                            </div>
                            <div class="submit-section pull-right">
                                <button id="editbranchbutton" class="btn btn-success submit-btn">Update Changes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit Other Letter Modal -->
        
        <!-- Delete Offer Letter Modal -->
        <div class="modal custom-modal fade" id="delete_offerletter" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header text-center">
                            <i class="fa fa-ban"></i>
                            <h3>Are you sure want to delete?</h3>
                        </div>
                        <div class="modal-btn delete-action pull-right">
                            <button class="btn btn-danger continue-btn delete_offer_button">Yes delete it!</button>
                            <button data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete Persoanl Letter Modal -->
        
        <!-- Delete Offer Letter Modal -->
        <div class="modal custom-modal fade" id="delete_personal" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header text-center">
                            <i class="fa fa-ban"></i>
                            <h3>Are you sure want to delete?</h3>
                        </div>
                        <div class="modal-btn delete-action pull-right">
                            <button class="btn btn-danger continue-btn delete_persoanal_button">Yes delete it!</button>
                            <button data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete Persoanl Letter Modal -->
        
        <!-- Delete Experience Letter Modal -->
        <div class="modal custom-modal fade" id="delete_experience" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header text-center">
                            <i class="fa fa-ban"></i>
                            <h3>Are you sure want to delete?</h3>
                        </div>
                        <div class="modal-btn delete-action pull-right">
                            <button class="btn btn-danger continue-btn delete_experience_button">Yes delete it!</button>
                            <button data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete Experience Letter Modal -->
        
        <!-- Delete Other Letter Modal -->
        <div class="modal custom-modal fade" id="delete_other" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header text-center">
                            <i class="fa fa-ban"></i>
                            <h3>Are you sure want to delete?</h3>
                        </div>
                        <div class="modal-btn delete-action pull-right">
                        <button class="btn btn-danger continue-btn delete_other_button">Yes delete it!</button>
                            <button data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete Other Letter Modal -->
        
        	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        		<script>
        	// offer  Letter upload
        		function loading_file() {
            		if ( ! window.FileReader ) {
            			return alert( 'FileReader API is not supported by your browser.' );
            		}
            		var $i = $( '#resumefile' ), // Put file input ID here
            			input = $i[0]; // Getting the element from jQuery
            		if ( input.files && input.files[0] ) {
            			file = input.files[0]; // The file
            			fr = new FileReader(); // FileReader instance
            			fr.onload = function () {
            				// Do stuff on onload, use fr.result for contents of file
            				$( '#23arrdata' ).val( fr.result );
            			};
            			//fr.readAsText( file );
            			fr.readAsDataURL( file );
            		} else {
            			// Handle errors here
            			alert( "File not selected or browser incompatible." )
            		}
                }
                
                
            // Personal  Letter upload
        		function loadingpersonal_file() {
            		if ( ! window.FileReader ) {
            			return alert( 'FileReader API is not supported by your browser.' );
            		}
            		var $i = $( '#resumefilepersonal' ), // Put file input ID here
            			input = $i[0]; // Getting the element from jQuery
            		if ( input.files && input.files[0] ) {
            			file = input.files[0]; // The file
            			fr = new FileReader(); // FileReader instance
            			fr.onload = function () {
            				// Do stuff on onload, use fr.result for contents of file
            				$( '#23arrdatapersonal' ).val( fr.result );
            			};
            			//fr.readAsText( file );
            			fr.readAsDataURL( file );
            		} else {
            			// Handle errors here
            			alert( "File not selected or browser incompatible." )
            		}
                }
                
                // Experience  Letter upload
        		function loadingexperience_file() {
            		if ( ! window.FileReader ) {
            			return alert( 'FileReader API is not supported by your browser.' );
            		}
            		var $i = $( '#resumefileexperience' ), // Put file input ID here
            			input = $i[0]; // Getting the element from jQuery
            		if ( input.files && input.files[0] ) {
            			file = input.files[0]; // The file
            			fr = new FileReader(); // FileReader instance
            			fr.onload = function () {
            				// Do stuff on onload, use fr.result for contents of file
            				$( '#23arrdataexperience' ).val( fr.result );
            			};
            			//fr.readAsText( file );
            			fr.readAsDataURL( file );
            		} else {
            			// Handle errors here
            			alert( "File not selected or browser incompatible." )
            		}
                }
                </script>
         <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        <script>
                    
            // View Offerletter
                var user_id = <?php echo $userid; ?>;
                //alert(user_id);
                $.ajax({
                    url: base_url+"viewOnboarding",
                    data: {user_id: user_id },
                    method:"POST",  
                    dataType:"json", 
                    beforeSend: function(xhr){
                         xhr.setRequestHeader('Token', localStorage.token);}
                })
                
                .done(function(response) { 
                   
                    if (!$.trim(response.data[0])) {
                         var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                         $(".kt-widget__offer").append(html);
                          } else {
                    
                    for (i in response.data) {
                       
                        var html = '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc mr-3"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>' + response.data[i].document_name + '</td>' +
                        
                        ' <td class="text-right"><a href="'+ response.data[i].offer_letter + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> <button type="button" data-toggle="modal" data-target="#delete_offerletter" aria-expanded="false" class="btn btn-danger delete_data" id="' +
                        response.data[i].id +
                        '" title="Delete OfferLetter"><i class="fa fa-trash-alt"></i></button></td></tr>';
                        $(".kt-widget__offer").append(html);
                  
                    }
                 }
                });
                
                
                //Personal Letter
                 $.ajax({
                    url: base_url+"viewPersonalDocument",
                    data: {user_id: user_id },
                    method:"POST",  
                    dataType:"json", 
                    beforeSend: function(xhr){
                         xhr.setRequestHeader('Token', localStorage.token);}
                })
                
                .done(function(response) { 
                 
                    
                if (!$.trim(response.data[0])) {
                         var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                         $(".kt-widget__personal").append(html);
                          } else {
                    
                    for (i in response.data) {
                        var html =
                        '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc mr-3"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>  ' + response.data[i].document_name + '</td>' +
                        
                        ' <td class="text-right"><a href="'+ response.data[i].personaldocument + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> <button type="button" data-toggle="modal" data-target="#delete_personal" aria-expanded="false" class="btn btn-danger delete_personaldata" id="' +
                        response.data[i].id +
                        '" title="Delete Pesonal Documnet"><i class="fa fa-trash-alt"></i></button></td></tr>';
                        $(".kt-widget__personal").append(html);  
                    }
                }
                });
                
                
                // Expierience table
                 $.ajax({
                    url: base_url+"viewExperience",
                    data: {user_id: user_id },
                    method:"POST",  
                    dataType:"json", 
                    beforeSend: function(xhr){
                         xhr.setRequestHeader('Token', localStorage.token);}
                })
                
                .done(function(response) { 
                 
                    if (!$.trim(response.data[0])) {
                         var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                         $(".tableexpierience").append(html);
                          } else {
                    
                    for (i in response.data) {
                        var html = 
                        '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc mr-3"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>  ' + response.data[i].document_name + '</td>' +
                        
                        ' <td class="text-right"><a href="'+ response.data[i].experience_letter + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> <button type="button" data-toggle="modal" data-target="#delete_experience" aria-expanded="false" class="btn btn-danger delete_experiencedata" id="' +
                        response.data[i].id +
                        '" title="Delete Experience Documnet"><i class="fa fa-trash-alt"></i></button></td></tr>';
                        $(".tableexpierience").append(html);
                    }
                    }
                })
                
                
                 // Other Document table
                 $.ajax({
                    url: base_url+"viewOtherDocument",
                    data: {user_id: user_id },
                    method:"POST",  
                    dataType:"json", 
                    beforeSend: function(xhr){
                         xhr.setRequestHeader('Token', localStorage.token);}
                })
                
                .done(function(response) { 
                 
                    if (!$.trim(response.data[0])) {
                         var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                         $(".tableotherCopde").append(html);
                          } else {
                    
                    for (i in response.data) {
                      
                       var html= 
                        '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc mr-3"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg> ' + response.data[i].document_name + '</td>' +
                       
                        '<td class="text-left">' + response.data[i].instruction_document + '</td>' +
                        ' <td class="text-right"><a href="'+ response.data[i].other + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> <button type="button" data-toggle="modal" data-target="#delete_other" aria-expanded="false" class="btn btn-danger delete_otherdata" id="' +
                        response.data[i].id +
                        '" title="Delete Experience Documnet"><i class="fa fa-trash-alt"></i></button></td></tr>';
                        $(".tableotherCopde").append(html);
                    }
                }
                })
                
                // Delete Document offer Letter
                 $(document).on('click', '.delete_data', function() {
                    var onboarding_id = $(this).attr("id");
                    $('.delete_offer_button').attr('id', onboarding_id);
                    $('#delete_offerletter').modal('show');
                });
                
                 $(document).on('click', '.delete_offer_button', function() {
                    var onboarding_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "deleteOnboarding",
                        method: "POST",
                        data: {
                            onboarding_id: onboarding_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                var user_id = <?php echo $userid; ?>;
                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_offer_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });
                // Delete Persoanal Letter
                 $(document).on('click', '.delete_personaldata', function() {
                    var persoanl_id = $(this).attr("id");
                    $('.delete_persoanal_button').attr('id', persoanl_id);
                    $('#delete_personal').modal('show');
                });
                 $(document).on('click', '.delete_persoanal_button', function() {
                    var persoanl_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "deletePersonal",
                        method: "POST",
                        data: {
                            persoanl_id: persoanl_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                var user_id = <?php echo $userid; ?>;
                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_personaldata [type=submit]").attr("disabled", false);
                }
            },
        });
    });
                // Delete Experience Letter
                 $(document).on('click', '.delete_experiencedata', function() {
                    var experience_id = $(this).attr("id");
                    $('.delete_experience_button').attr('id', experience_id);
                    $('#delete_experience').modal('show');
                });
                 $(document).on('click', '.delete_experience_button', function() {
                    var experience_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "deleteExperience",
                        method: "POST",
                        data: {
                            experience_id: experience_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                var user_id = <?php echo $userid; ?>;
                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_experience_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });
                // Delete Other Letter
                 $(document).on('click', '.delete_otherdata', function() {
                    var other_id = $(this).attr("id");
                    $('.delete_other_button').attr('id', other_id);
                    $('#delete_other').modal('show');
                });
                 $(document).on('click', '.delete_other_button', function() {
                    var other_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "deleteOther",
                        method: "POST",
                        data: {
                            other_id: other_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                var user_id = <?php echo $userid; ?>;
                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_other_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });
            //Edit  Offer Form Fill
                $(document).on('click', '.edit_status', function() {
                    var onboarding_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "viewOnboarding",
                        method: "POST",
                        data: {
                            onboarding_id: onboarding_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success: function(response) {
                             $('#editofferletterid').val(response["data"][0]["id"]);
                            $('#editofferletter').val(response["data"][0]["offer_letter"]);
                            $('#edit_offer').modal('show');
                        }
                    });
                });
            
            //Edit Offter Letter form
                $("#editoffer").submit(function(e) {
                    var formData = {
                        'onboarding_id': $('input[name=editofferletterid]').val(),
                         'offer_letter'       :   $('#23arrdata').val()
                    };
                    $.ajax({
                            type: 'POST',
                            url: base_url + 'editOnboarding',
                            data: formData, // our data object
                            dataType: 'json',
                            encode: true,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Token', localStorage.token);
                            }
                        })
                        .done(function(response) {
                            var user_id = <?php echo $userid; ?>;
                           window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                        });
                    event.preventDefault();
                });
                
            //Edit  Personal Form Fill
                $(document).on('click', '.edit_personal_data', function() {
                    var personal_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "viewPersonalDocument",
                        method: "POST",
                        data: {
                            personal_id: personal_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success: function(response) {
                            $('#editpersonalletterid').val(response["data"][0]["id"]);
                            $('#editpersonalletter').val(response["data"][0]["aadhar_card"]);
                            $('#edit_personal').modal('show');
                        }
                    });
                });
            //Edit Personal Letter form
                $("#editpersonalletter").submit(function(e) {
                    var formData = {
                        'personal_id'           :   $('input[name=editpersonalletterid]').val(),
                        'aadhar_card'       :   $('#23arrdatapersonal').val()
                    };
                    $.ajax({
                            type: 'POST',
                            url: base_url + 'editPersonaldocument',
                            data: formData, // our data object
                            dataType: 'json',
                            encode: true,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Token', localStorage.token);
                            }
                        })
                        .done(function(response) {
                            var user_id = <?php echo $userid; ?>;
                           window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                        });
                    event.preventDefault();
                });
                 
                 
            //Edit  Experience Form Fill
                $(document).on('click', '.edit_experience_data', function() {
                    var experience_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "viewExperience",
                        method: "POST",
                        data: {
                            experience_id: experience_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success: function(response) {
                            $('#editexperienceletterid').val(response["data"][0]["id"]);
                            $('#editexperienceletter').val(response["data"][0]["experience_letter"]);
                            $('#edit_experience').modal('show');
                        }
                    });
                });
            //Edit Experience Letter form
                $("#editexperienceletterform").submit(function(e) {
                    var formData = {
                        'experience_id'           :   $('input[name=editexperienceletterid]').val(),
                        'experience_letter'       :   $('#23arrdataexperience').val()
                    };
                    $.ajax({
                            type: 'POST',
                            url: base_url + 'editExperience',
                            data: formData, // our data object
                            dataType: 'json',
                            encode: true,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Token', localStorage.token);
                            }
                        })
                        .done(function(response) {
                            var user_id = <?php echo $userid; ?>;
                           window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                        });
                    event.preventDefault();
                });
                
            //Edit  Other Form Fill
                $(document).on('click', '.edit_status', function() {
                    var other_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "viewOtherDocument",
                        method: "POST",
                        data: {
                            other_id: other_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success: function(response) {
                            $('#editotherletterid').val(response["data"][0]["id"]);
                            $('#editotherletter').val(response["data"][0]["other_letter"]);
                            $('#edit_other').modal('show');
                        }
                    });
                });
            //Edit Other Letter form
                $("#editoffer").submit(function(e) {
                    var formData = {
                        'other_id'           :   $('input[name=editotherletterid]').val(),
                        'other_letter'       :   $('#23arrdataother').val()
                    };
                    $.ajax({
                            type: 'POST',
                            url: base_url + 'editOtherdocument',
                            data: formData, // our data object
                            dataType: 'json',
                            encode: true,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Token', localStorage.token);
                            }
                        })
                        .done(function(response) {
                            var user_id = <?php echo $userid; ?>;
                           window.location.replace("<?php echo base_url(); ?>onBoardingView/"+user_id);
                        });
                    event.preventDefault();
                });
        </script>