<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Tickets">Tickets</a></li>
                <li class="breadcrumb-item active">Add Tickets</li>
            </ul>
        </div>
    </div>
</div>

<div class="message_succes text-succes" id="message"></div>


<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Ticket</strong></h3>
            </div>
            <div class="card-body">
                <!-- Add Ticket Form -->
                <form id="addtickets" method="POST" action="#">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Type<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addtickettypevalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addtickettype" id="addtickettype"> </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Branch <span class="text-danger">*</span></label>
                            <br />
                            <span id="addbranchvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addbranchid" id="addbranchid"> </select>
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Department<span class="text-danger"> *</span></label>
                            <br />
                            <span id="adddepartmentvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="adddepartment" id="adddepartment"> </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Ticket Priority<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addpriorityvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addpriority" id="addpriority"> </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Subject<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addtitlevalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" id="addtitle" name="addtitle" placeholder="Please enter subject" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Description<span class="text-danger"> *</span></label>
                            <br />
                            <span id="adddescriptionvalidate" class="text-danger change-pos"></span>
                            <textarea class="form-control" id="adddescription" name="adddescription" placeholder="Enter tickit description."></textarea>
                        </div>
                    </div>
                    <span id="message" class="text-danger"></span>
                    <div class="submit-section pull-right">
                        <button id="addticketbutton" class="btn btn-success submit-btn">Submit</button>
                        <button type="reset" class="btn btn-secondary" id="test" data-dismiss="modal">Reset</button>
                    </div>
                </form>
                <!-- /Add Branch Form -->
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
  //for remove validation and empty input field

  $(document).ready(function () {
    $("#test").click(function () {
        jQuery("#addtickettypevalidate").text("");
        jQuery("#addbranchvalidate").text("");
        jQuery("#adddepartmentvalidate").text("");
        jQuery("#addpriorityvalidate").text("");
        jQuery("#addtitlevalidate").text("");
        jQuery("#adddescriptionvalidate").text("");
    });
});

     //Select Ticket Type by api
     $.ajax({
        url: base_url + "viewTicketsType",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addtickettype");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Ticket Type</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.type_name).text(entry.type_name));
        });
    });
// Select branch by api*/
$.ajax({
    url: base_url + "viewTokenBranch",
    data: {},
    type: "POST",
    dataType: "json", // what type of data do we expect back from the server
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    let dropdown = $("#addbranchid");

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose Branch</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
    });
});
// Select department by api*/
$("#addbranchid").change(function () {
    $.ajax({
        url: base_url + "viewDepartmentEmployee",
        data: { branch_id: $("select[name=addbranchid]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#adddepartment");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });
});

   

    //Select Ticket Priority by api
    $.ajax({
        url: base_url + "viewTicketsPriority",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addpriority");

        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Priority</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.priority));
        });
    });

    // Add  Tickets list
    $("#addtickets").submit(function (e) {
        var formData = {
            title: $("input[name=addtitle]").val(),
            priority: $("select[name=addpriority]").val(),
            type: $("select[name=addtickettype]").val(),
            send_department: $("select[name=adddepartment]").val(),
            send_branch: $("select[name=addbranchid]").val(),
            
            description: $("textarea[name=adddescription]").val(),
            
        };

        e.preventDefault();
        $.ajax({
            type: "POST", // define the type of HTTP verb we want to use (POST for our form)
            url: base_url + "addTickets", // the url where we want to POST
            data: formData, // our data object
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addtickets [type=submit]").attr("disabled", false);
            }
        });
    });

      //Add Tickets Validation
      $(document).ready(function () {
        $("#addticketbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addtickettype").val() == null) {
                $("#addtickettype").focus();
                $("#addtickettypevalidate").text("Please select a ticket type.");
                errorCount++;
            } else {
                $("#addtickettypevalidate").text("");
            }
            if ($("#addpriority").val() == null) {
                $("#addpriorityvalidate").text("Please select a priority type.");
                errorCount++;
            } else {
                $("#addpriorityvalidate").text("");
            }
            if ($("#adddepartment").val() == null) {
                $("#adddepartmentvalidate").text("Please select a department.");
                errorCount++;
            } else {
                $("#adddepartmentvalidate").text("");
            }
            if ($("#addbranchid").val() == null) {
                $("#addbranchvalidate").text("Please select a Branch.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }
            if ($("#addtitle").val().trim() == "") {
                $("#addtitlevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addtitlevalidate").text("");
            }
            if ($("#adddescription").val().trim() == "") {
                $("#adddescriptionvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#adddescriptionvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>


