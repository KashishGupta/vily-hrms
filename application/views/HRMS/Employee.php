<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item active">Employee</li>
            </ul>
            <div class="header-action mt-3">
                <a href="<?php echo base_url(); ?>EmployeeList" class="btn btn-info grid-system mr-2"><i class="fe fe-list"></i></a>
                <a href="<?php echo base_url(); ?>EmployeeAdd_emp" class="btn btn-primary"><i class="fe fe-plus mr-2"></i>Add Employee</a>
            </div>
        </div>
       
        <div class="row mt-3">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-users"></i></div>
                        <h3 class="mb-1" id="totalemployee"></h3>
                        <div>Total Employee</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-user"></i></div>
                        <h3 class="mb-1" id="totalnewemployee"></h3>
                        <div>New Employee</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-male"></i></div>
                        <h3 class="mb-1" id="totalMale"></h3>
                        <div>Male</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-female"></i></div>
                        <h3 class="mb-1" id="totalFemale"></h3>
                        <div>Female</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix kt-widget__items"></div>
    </div>
</div>

<!-- Edit Status Employee Modal -->
<div id="employee_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Employee Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editstatus" method="POST" action="#">
                    <div class="modal-body">
                        <div class="form-header">
                            <p>Are you sure want to Active Or Inactive for this employee?</p>
                        </div>
                        <div class="form-group">
                            <label>Employee Status<span class="text-danger">*</span></label>
                            <input value="" id="editstatid" name="editstatid" class="form-control" type="hidden" />
                            <select value="" id="editstatuschange" name="editstatuschange" class="form-control custom-select">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success submit-btn">Yes Change It!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
 $(function () {
        filterEmployee(4);
        $("#is_active").on("change", function () {
            is_active = $(this).val();
            filterEmployee(is_active);
        });
    });
    //Show Employee
    function filterEmployee(is_active) {
        //alert(is_active);
        req = {};
        req.is_active = is_active;
    $.ajax({
        url: base_url + "viewUser",
        data: req,
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        setInterval('refreshPage()', 5000);
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-lg-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Employee</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } 
                else {
                    UserImage = response.data[i].user_image;
                }

                var status_color = "";
                if (response.data[i].is_active == 0) {
                    status_color = "red";
                }
                if (response.data[i].is_active == 1) {
                    status_color = "white";
                }
                var isstatus = "";
                if (response.data[i].is_active == 1) {
                    isstatus = "Active";
                }
                if (response.data[i].is_active == 0) {
                    isstatus = "Inactive";
                }

                var html =
                    '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="profile-widget ribbon"><div class="ribbon-box cyan" style="color:' + status_color + '" >'+ isstatus +'</div><div class="profile-img mt-4"><img class="rounded-circle img-thumbnail w100 h100 mb-1" src="' +
                    UserImage +
                    '" alt=""></div><div class="dropdown profile-action"><a href="#" class="action-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-150px, 18px, 0px);"><a class="dropdown-item" href="EmployeeEdit/' +
                    response.data[i].user_code +
                    '"><i class="fa fa-edit"></i>Edit Profile</a><a class="dropdown-item" href="EmployeeView/' +
                    response.data[i].user_code +
                    '"><i class="fa fa-eye"></i>View Profile</a> <a class="dropdown-item edit_data" href="#" data-toggle="modal" data-target="#employee_status" aria-expanded="false" id="' + response.data[i].id + '"><i class="fa fa-edit"></i>Change Status</a></div></div><h4 class="user-name text-ellipsis"><a href="EmployeeView/' +
                    response.data[i].user_code +
                    '">' +
                    response.data[i].first_name +
                    '</a></h4><div class="small text-muted"><span>' +
                    response.data[i].emp_id +
                    "</span> - " +
                    response.data[i].designation_name +
                    "</div><span class='text-ellipsis'>" +
                    response.data[i].email +
                    "</span><span>" +
                    response.data[i].phone +
                    "</span></br></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });
   
}


    // Edit Employee Status
    $(document).on("click", ".edit_data", function () {
        var user_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewUserDashboard",
            method: "POST",
            data: {
                user_id: user_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editstatid").val(response["data"][0]["id"]);
                $("#editstatuschange").val(response["data"][0]["is_active"]);
                $("#employee_status").modal("show");
            },
        });
    });

    // Edit Employee Active form
    $("#editstatus").submit(function (e) {
        var formData = {
            users_id: $("input[name=editstatid]").val(),
            status: $("select[name=editstatuschange]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "deactivateUser",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Employee_emp";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editstatus [type=submit]").attr("disabled", false);
            }
        });
    });



    //Show Employee
    $.ajax({
        url: base_url + "totalEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            var test = response.data[0].id;
           // alert(test);
            $("#totalemployee").html(response.data[0].id);
        },
    });

    //Show New Employee In A Month
    $.ajax({
        url: base_url + "countAllNewEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totalnewemployee").html(response.data[0].NewEmployee);
        },
    });

    //total Male Employee
    $.ajax({
        url: base_url + "totalMaleEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totalMale").html(response.data[0].total_male);
        },
    });

    //total  Female Employee
    $.ajax({
        url: base_url + "totalFemaleEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totalFemale").html(response.data[0].total_Female);
        },
    });
</script>
