<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item active">Assets</li>
            </ul>
            <div class="header-action mt-3">
                <a class="btn btn-primary" href="<?php echo base_url(); ?>AssetAdd_emp"><i class="fa fa-plus mr-2"></i>Add New Assets</a>
            </div>
        </div>

        <!-- Search Assets Result-->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Assets Result</strong></h3>
            </div>
            <div class="card-body">
                <form id="filter-form" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Select Assets Category<span class="text-danger"> *</span> </label><br>
                            <span id="findassetsvalid" class="text-danger change-pos"></span>
                            <select name="findassets" id="findassets" class="form-control custom-select">
                                
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12 button-open">
                            <button id="filterassetsbutton" class="btn btn-success submit-btn">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list dataTable table-responsive table-responsive-xxl table-responsive-xl no-footer" id="assetstable">
                    <thead>
                        <tr>
                            <th class="text-center">Asset Id</th>
                            <th class="text-center">Asset Category</th>
                            <th class="text-center">Asset Name</th>
                            <th class="text-center">Purchase Date</th>
                            <th class="text-center">Purchase From</th>
                            <th class="text-center">Issue Assets</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyassets">

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Search Assets Result-->
    </div>
</div>

<!-- Delete Assets Modal -->
<div class="modal custom-modal fade" id="delete_assets" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" id="" class="btn btn-danger continue-btn delete_assets_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Assets Modal -->
<div id="issue_to_assets" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assets Issue To</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addassetsissued" method="POST">
                    <div class="form-group col-sm-12">
                        <label>Asset Category<span class="text-danger"> *</span></label><br>
                        <span id="assetcategoryvalid" class="text-danger change-pos"></span>
                        
                        <input class="form-control" type="hidden" name="companyId" id="companyId" readonly="">
                        <input class="form-control" type="text" name="companyAssetName" id="companyAssetName" readonly="">
                    </div>
                    <div class="form-group col-sm-12">
                        <label>Asset Name<span class="text-danger"> *</span></label><br>
                        <span id="assetnamevalid" class="text-danger change-pos"></span>
                        <input class="form-control" type="hidden" name="assetCategoryId" id="assetCategoryId" readonly="">
                        <input class="form-control" type="text" name="categoryAssetName" id="categoryAssetName" readonly="">
                    </div>
                    <div class="form-group col-sm-12">
                        <label>Issue Date<span class="text-danger"> *</span></label><br>
                        <span id="issuedatevalid" class="text-danger change-pos"></span>
                        <input class="form-control" data-provide="datepicker" data-date-autoclose="true" type="text" name="issuedate" id="issuedate" placeholder="YYYY/MM/DD" readonly="">
                    </div>
                    <div class="form-group col-sm-12">
                        <label>Issued To<span class="text-danger"> *</span></label><br>
                        <span id="issuedtovalid" class="text-danger change-pos"></span>
                        <select class="form-control custom-select" type="text" name="issuedto2" id="issuedto2">
                        </select>
                    </div>
                    
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 text-right">
                        <button type="submit" id="issuedbuttonto" class="btn btn-success continue-btn issue_to_assets_button">Yes, Issue it!</button>
                        <button type="reset" data-dismiss="modal" id="test" class="btn btn-secondary">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Delete Assets Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#issuedtovalid").text("");
            jQuery("#issuedatevalid").text("");
        });
    });
    //For remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("#findassets option:eq(0)").prop("selected", true);
            $("span#findassetsvalid").prop("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#findassetsvalid").text("");
        });
    });

    //Select Asset Category by api
    $.ajax({
        url: base_url + "viewAssetsCategoryActive",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#findassets");

        dropdown.empty();
        dropdown.append('<option selected="true" value="all">Choose Assets Category</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.category_assets));
        });
    });


     // Show Company Assets In table With Filter
     $ (function () {
        filterDepartment("all");
        $("#filter-form").submit();
    });

    function filterDepartment() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            category_id = $("#findassets").val();

            if (category_id == null) {
                category_id = "all";
            }
            req = {};
            req.category_id = category_id;
            $.ajax({
                url: base_url + "viewAssetsCompany",
                data: req,
                type: "POST",
                dataType: "json", // what type of data do we expect back from the server
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                $("#assetstable").DataTable().clear().destroy();
                var table = document.getElementById("tablebodyassets");
                for (i in response.data) {
	            
	            var date = new Date(response.data[i].purchage_date);
				var dd = String(date.getDate()).padStart(2, '0');
				var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
				var yyyy = date.getFullYear();

				date = dd + '-' + mm + '-' + yyyy;
                    var tr = document.createElement("tr");
                    tr.innerHTML =
                        '<td class="text-center text-uppercase">' +
                        response.data[i].assets_code +
                        "</td>" +

                        '<td class="text-center">' +
                        response.data[i].category_assets +
                        "</td>" +

                        '<td class="text-center"><a href="AssetDetails/' + response.data[i].assets_code +'">' +
                        response.data[i].assets_name +
                        "</a></td>" +

                        '<td class="text-center">' +
                        date +
                        "</td>" +

                        '<td class="text-center">' +
                        response.data[i].purchage_from +
                        "</td>" +

                        
                        '<td class="text-center"><button type="button" data-toggle="modal" data-target="#issue_to_assets" aria-expanded="false" class="btn btn-primary issue_assetsUser" id="' +
                        response.data[i].id +
                        '" ><i class="fa fa-hands-helping mr-2"></i>Issue to</button></td>'+
                        '<td class="text-right"><a href="AssetEdit_emp/' + response.data[i].assets_code +'" class="btn btn-info edit_data"  id="' + response.data[i].id + '"><i class="fa fa-edit"></i></a> <button type="button" data-toggle="modal" data-target="#delete_assets" aria-expanded="false" class="btn btn-danger delete_data" id="' +
                        response.data[i].id +
                        '" title="Delete Department"><i class="fa fa-trash-alt"></i></button></td>';
                    table.appendChild(tr);
                }
                $("#assetstable").DataTable({
                    dom: "Bfrtip",
                    buttons: ["excelHtml5", "pdfHtml5"],
                });
            });
        });
    }


    //Delete Assets 
    $(document).on('click', '.delete_data', function() {
        var assets_id = $(this).attr("id");
        $('.delete_assets_button').attr('id', assets_id);
        $('#delete_assets').modal('show');
    });

    $(document).on('click', '.delete_assets_button', function() {
        var assets_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteCompanyAssets",
            method: "POST",
            data: {
                assets_id: assets_id
            },
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            },
            success: function(response) {
                console.log(response);
               
                var jsonDa = response;                
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function(){window.location ="<?php echo base_url()?>Assets_emp"},1000);
              
            } 

            else {
                toastr.error(falsedata);
                $('#delete_assets_button [type=submit]').attr('disabled',false);
            } 
            }
        });
    });
    
    //Filter Asset Cateogory Validation
    $(document).ready(function () {
        $("#filterassetsbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#findassets").val() == null) {
                $("#findassets").focus();
                $("#findassetsvalid").text("Please select assets category.");
                errorCount++;
            } else {
                $("#findassetsvalid").text("");
            }

            
            if (errorCount > 0) {
                return false;
            }
        });
    });
 // Edit Branch Status Validation form Fill
 $(document).on("click", ".issue_assetsUser", function () {
        var assets_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewAssetsCompany",
            method: "POST",
            data: {
                assets_id: assets_id,
            },

            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#companyId").val(response["data"][0]["id"]);
                $("#companyAssetName").val(response["data"][0]["category_assets"]);
                $("#assetCategoryId").val(response["data"][0]["category_id"]);
                $("#categoryAssetName").val(response["data"][0]["assets_name"]);
                $("#branch_status").modal("show");
            },
        });
    });
//Select Assets Category For Add Assets Issued Add
$.ajax({
        url: base_url + "viewUserName",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#issuedto2");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Issue To</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
        });
    });
     // Add Issued Assets
     $("#addassetsissued").submit(function (e) {
        var formData = {
            asset_company_id: $("input[name=companyId]").val(),
            asset_category_id: $("input[name=assetCategoryId]").val(),
            issue_date: $("input[name=issuedate]").val(),
            issue_to:   $("select[name=issuedto2]").val(),

        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addAssetsUser",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Assets";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addassetsissued [type=submit]").attr("disabled", false);
            }
        });
    });

    //Add Tickets Validation
    $(document).ready(function() {
        $("#issuedbuttonto").click(function(e) {
            e.stopPropagation();
            var errorCount = 0;
            
            if ($("#issuedto2").val() == null) {
                $("#issuedtovalid").text("Please select a issued to.");
                errorCount++;
            } else {
                $("#issuedtovalid").text("");
            }           
            if ($("#issuedate").val().trim() == '') {
                $("#issuedate").focus();
                $("#issuedatevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#issuedatevalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>