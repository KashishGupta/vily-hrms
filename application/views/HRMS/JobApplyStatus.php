<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>JobPublic">Jobs Public</a></li>
                <li class="breadcrumb-item active">Job Applicants</li>
            </ul>
        </div>
    </div>
</div>
<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="row clearfix kt-widget__items"></div>
    </div>
</div>

<style type="text/css">
    .profile-widget-job {
        background-color: #fff;
        border: 1px solid #ededed;
        border-radius: 4px;
        margin-bottom: 20px;
        padding: 20px;
        position: relative;
        box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.2);
        overflow: hidden;
    }
</style>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Show Employee
    $.ajax({
        url: base_url + "viewReferredData",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-lg-12 col-md-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Job Applicants</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var startDate = new Date(response.data[i].apply_date);
                var dd = String(startDate.getDate()).padStart(2, "0");
                var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = startDate.getFullYear();

                startDate = dd + "-" + mm + "-" + yyyy;
                var status = "";
                if (response.data[i].status == 0) {
                    status = "Applied ";
                }
                if (response.data[i].status == 1) {
                    status = "Shortlisted ";
                }

                if (response.data[i].status == 2) {
                    status = "Interviewed ";
                }
                if (response.data[i].status == 3) {
                    status = "Rejected";
                }

                var html =
                    '<div class="col-lg-12 col-md-12"><div class="profile-widget-job"><div class="row"><div class="col-5"><p><strong>Referred To : </strong><span> ' +
                    response.data[i].first_name +
                    '</span></div><div class="col-7"><p><strong>Apply Date : </strong><span>' +
                    startDate +
                    '</span></div><div class="col-5"><p><strong>Exp : </strong><span>' +
                    response.data[i].experience +
                    '</span></div><div class="col-7"><p><strong>Current Location : </strong><span> ' +
                    response.data[i].current_location +
                    '</span></div><div class="col-5"><p><strong>Phone No :</strong> <span> ' +
                    response.data[i].phone +
                    '</span></p></div><div class="col-7"><p><strong>Email :</strong> <span> ' +
                    response.data[i].email +
                    '</span></p></div><div class="col-5"><p><strong>Current Designation :</strong> <span> ' +
                    response.data[i].current_designation +
                    '</span></p></div><div class="col-7"><p><strong>Current Company :</strong> <span>' +
                    response.data[i].current_company +
                    '</span></p></div><div class="col-5"><p><strong>Education : </strong><span> ' +
                    response.data[i].highest_education +
                    '</span></p></div><div class="col-5"><p><strong>Resume : </strong><span> <a href="' +
                    response.data[i].resume +
                    '"  class="btn btn-info ml-2"><i class="fa fa-download mr-2"></i><strong>Download Resume</a></strong></span></p></div><div class="col-12"><p><strong>Skills :</strong> <span>' +
                    response.data[i].skills +
                    '</span></p></div><div class="col-12"><p><strong>Job Status :</strong> <span>' +
                    status +
                    '</span></p></div> <div class="col-12" style="padding-top: 10px; border-top: 1px solid #E8E9E9; "></div>  </div></div></div>';
                $(".kt-widget__items").append(html);
            }
        }
    });
</script>
