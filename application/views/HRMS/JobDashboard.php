<!-- Plugins chart css -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/c3.min.css" />

<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xl-3 col-lg-6 col-md-12 col-6">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-body top_counter">
                        <div class="icon bg-orange"><i class="fa fa-building"></i></div>
                        <div class="content">
                            <span>Total Jobs</span>
                            <h5 class="number mb-0" id="totaljobs"></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12  col-6">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-body top_counter">
                        <div class="icon bg-orange"><i class="fa fa-building"></i></div>
                        <div class="content">
                            <span>Open Jobs</span>
                            <h5 class="number mb-0" id="openjobs"></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12  col-6">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-body top_counter">
                        <div class="icon bg-orange"><i class="fa fa-building"></i></div>
                        <div class="content">
                            <span>Close Job</span>
                            <h5 class="number mb-0" id="closejobs"></h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-6 col-md-12  col-6">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-body top_counter">
                        <div class="icon bg-orange"><i class="fa fa-building"></i></div>
                        <div class="content">
                            <span>Hold Job</span>
                            <h5 class="number mb-0" id="holdjobs"></h5>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="col-lg-8 col-md-6">
                <div class="card">
                	<div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title">Top Active Jobs Graph</h3>
                    </div>
                    <div class="card-body">
                        <div id="chart-combination" style="height: 14rem"></div>
                    </div>
                </div>                
            </div> -->

            <div class="col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title">Acquisitions</h3>
                        <div class="card-options">
                            <a href="javascript:void(0)">Current Month</a>
                        </div>
                    </div>
                    <table class="table card-table table-vcenter" style="height: 264px;">
                        <tbody>
                            <tr>
                                <td class="text-left" style="font-weight: 600;">Applications</td>
                                <td class="text-center"id="Totalapplication"></td>
                            </tr>
                            <tr>
                                <td class="text-left" style="font-weight: 600;">Applied</td>
                                <td class="text-center" id="Totalapplied"></td>
                            </tr>
                            <tr>
                                <td class="text-left" style="font-weight: 600;">Shortlisted</td>
                                <td class="text-center" id="Totalshortlisted"></td>
                            </tr>
                            <tr>
                                <td class="text-left" style="font-weight: 600;">Rejected</td>
                                <td class="text-center" id="Totalrejected"></td>
                            </tr>
                            <tr>
                                <td class="text-left" style="font-weight: 600;">Interviwed</td>
                                <td class="text-center" id="Totalinterviwed"></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title">New Applicants</h3>
                        <div class="card-options">
                            <a href="javascript:void(0)">Last 7 Days</a>
                        </div>
                    </div>
                    <ul class="recent_comments job-applicants p-2">
                    </ul>
                </div>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h2 class="card-title">Job Type</h2>
                    </div>
                    <ul class="recent_comments list-unstyled Kt-jobtype"></ul>
                    <div class="card-footer text-center">
                        <a href="<?php echo base_url() ?>JobType" class="btn btn-primary">View Details</a>
                        <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#add_job_type">Add Job type</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h2 class="card-title">Job Status</h2>
                    </div>
                    <ul class="recent_comments list-unstyled Kt-applicantstatus"></ul>
                    <div class="card-footer text-center">
                        <a href="<?php echo base_url() ?>JobStatus" class="btn btn-primary">View Details</a>
                        <!-- <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#addjobstatus">Add Status</a> -->
                    </div>
                </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title">Jobs</h3>
                        <div class="card-options">
                            <a href="<?php echo base_url() ?>JobAdd" class="btn btn-primary">Add Job</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover mb-0 table-vcenter text-nowrap table-responsive table-responsive-xxxl dataTable" id="jobtable">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">Job ID</th>
                                    <th class="text-center">Job Title</th>
                                    <th class="text-center">Branch</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">Expire Date</th>
                                    <th class="text-center">Job Type</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Applicants</th>
                                    <th class="text-center">Approvals</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodyjob"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Job Type -->
<div id="add_job_type" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Job Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addjobtype" method="POST">
                    <div class="form-group col-md-12">
                        <label>Job Type<span class="text-danger"> *</span></label><br />
                        <span id="addjobtypenamevalidate" class="text-danger change-pos"></span>
                        <input name="addjobtypename" id="addjobtypename" class="form-control name-valid" type="text" placeholder="Please enter job type" autocomplete="off" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Job Type Color<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="addjobtypestatuscolorvalidate"></span>
                        <input name="addjobtypestatuscolor" id="addjobtypestatuscolor" class="form-control" type="color" placeholder="e.g. #000000" autocomplete="off" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Job Type Description<span class="text-danger"></span></label><br />
                        <span id="addjobtypedescriptionvalidate" class="text-danger change-pos"></span>
                        <textarea name="addjobtypedescription" id="addjobtypedescription" class="form-control" type="text" placeholder="Please enter job type description" autocomplete="off"></textarea>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="addjobtypebutton" class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Add Applicant Status -->
<!-- <div id="addjobstatus" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Job Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addapplicantstatus" method="POST">
                    <div class="form-group col-md-12">
                        <label>Applicant Status<span class="text-danger"> *</span></label><br />
                        <span class="text-danger change-pos" id="addapplicantstatusvalidate"></span>
                        <input name="addapplicantstatusname" id="addapplicantstatusname" class="form-control name-valid" type="text" placeholder="Please enter application status" autocomplete="off" />
                    </div>
                    <div class="form-group col-sm-12">
                        <label>Status Color<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="addstatuscolorvalidate"></span>
                        <input name="addstatuscolor" id="addstatuscolor" class="form-control" type="color" placeholder="e.g.  #000000" autocomplete="off" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="addapplicantstatusbutton" class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> -->

<!-- Status Job Modal -->
<div id="status_job" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Status Job</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="statusjobs" method="POST">
                    <div class="form-group">
                        <label>Status</label>
                        <select value="" class="custom-select form-control" name="editstatus" id="editstatus"> </select>
                        <input type="hidden" id="statusjobsid" name="statusjobsid" class="form-control" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editTicketbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete Job Modal -->
<div class="modal custom-modal fade" id="delete_managejob" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_managejob_button">Delete</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Job Modal -->

<script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.min.js"></script>

<!-- Charts Homepage -->
<script src="<?php echo base_url(); ?>assets/js/c3.bundle.js"></script>
<script src="<?php echo base_url(); ?>assets/js/c3.js"></script>

<script type="text/javascript">
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#addjobtypename").val("");
            $("input#addjobtypestatuscolor").val("");
            $("textarea#addjobtypedescription").val("");

            $("input#addapplicantstatusname").val("");
            $("input#addstatuscolor").val("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });
    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addjobtypenamevalidate").text("");
            jQuery("#addjobtypedescriptionvalidate").text("");
            jQuery("#addjobtypestatuscolorvalidate").text("");

            jQuery("#addstatuscolorvalidate").text("");
            jQuery("#addapplicantstatusvalidate").text("");
        });
    });

    //Show Total Job
    $.ajax({
        url: base_url + "totalJobs",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totaljobs").html(response.data[0].totalJobs);
            $("#alltime").html(response.data[0].totalJobs);
        },
    });

    //Show Total Today Job
    $.ajax({
        url: base_url + "viewTodayJob",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#todayjobs").html(response.data[0].today);
        },
    });

    //Show Total Cureent month Job
    $.ajax({
        url: base_url + "viewCurrentMonthJob",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#currentmonth").html(response.data[0].totalCurrentMonthJobs);
        },
    });

    //Show Total Open Job
    $.ajax({
        url: base_url + "totalOpenJobs",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#openjobs").html(response.data[0].totalOpenJobs);
        },
    });

    //Show Total Close Job
    $.ajax({
        url: base_url + "totalCloseJobs",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#closejobs").html(response.data[0].totalCloseJobs);
        },
    });

    //Show Total Hold Job
    $.ajax({
        url: base_url + "totalHoldJobs",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#holdjobs").html(response.data[0].totalHoldJobs);
        },
    });
    //Show Total Last Month Job
    $.ajax({
        url: base_url + "totalLastMonthJobs",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#LastMonthjobs").html(response.data[0].LastMonth);
        },
    });
     //Show Total Application Month Job
     $.ajax({
        url: base_url + "viewApplicatiionMonth",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            for (i in response.data) {
            $("#Totalapplication").html(response.data[i].totalJobsMonth);
             $("#Totalshortlisted").html(response.data[i].Shortlisted);
            $("#Totalrejected").html(response.data[i].rejcted);
            $("#Totalinterviwed").html(response.data[i].interviewd);
            $("#Totalapplied").html(response.data[i].Applied);
            }
        },
    });

    // View Job Type
    $.ajax({
        url: base_url + "viewJobType",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="col-md-12"><div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".Kt-jobtype").append(html);
        } else {
        for (i in response.data) {
            var html = '<li><div class="comment_body"><h6>' + response.data[i].job_type + "</h6><p>" + response.data[i].jobtype_description + "</p></div></li>";
            $(".Kt-jobtype").append(html);
        }
        }
    });

    // Add Job Type
    $("#addjobtype").submit(function (e) {
        var formData = {
            job_type: $("input[name=addjobtypename]").val(),
            type_color: $("input[name=addjobtypestatuscolor]").val(),
            jobtype_description: $("textarea[name=addjobtypedescription]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addJobType",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
            // using the done promise callback
            .done(function (response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url(); ?>JobDashboard";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#addjobtype [type=submit]").attr("disabled", false);
                }
            });
    });

    // View Job Applicant Status
    $.ajax({
        url: base_url + "viewJobApplicantStatus",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="col-md-12"><div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".Kt-applicantstatus").append(html);
        } else {
        for (i in response.data) {
            var html = '<li><div class="comment_body"><h6>' + response.data[i].job_applicant_status + "</h6></div></li>";
            $(".Kt-applicantstatus").append(html);
        }
        }
    });


    // View Job New Applicant Status
    $.ajax({
        url: base_url + "viewLastSevenApplication",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="col-md-12"><div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".recent_comments.job-applicants").append(html);
        } else {
        for (i in response.data) {
            var html = '<li class="online"> <div class="avatar_img"> <img class="rounded img-fluid" src="http://52.86.51.162/assets/images/avatar1.jpg" alt=""> </div> <div class="comment_body"> <h6><a href="#">'+ response.data[i].first_name+'</a></h6> <p>Applied for <span>'+ response.data[i].department_name+'</span></p> </div> </li>';
            $(".recent_comments.job-applicants").append(html);
        }
        }
    });


    // Add Apllicants Status
    /*$("#addapplicantstatus").submit(function (e) {
        var formData = {
            job_applicant_status: $("input[name=addapplicantstatusname]").val(),
        };

        e.preventDefault();

        $.ajax({
            type: "POST",
            url: base_url + "addJobApplicantStatus",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;

            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>JobDashboard";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addapplicantstatus [type=submit]").attr("disabled", false);
            }
        });
    });*/

    //Show Job  list
    $.ajax({
        url: base_url+"viewJob",
        data: { },
        type: "POST",
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true,
        beforeSend: function(xhr){xhr.setRequestHeader('Token', localStorage.token);}
    })
    .done(function(response) { 
        var j = 1;
        var table = document.getElementById('tablebodyjob');
        for (i in response.data) {
            var startDate = new Date(response.data[i].start_date);
            var dd = String(startDate.getDate()).padStart(2, '0');
            var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = startDate.getFullYear();
            
            startDate = dd + '-' + mm + '-' + yyyy;
            var endDate = new Date(response.data[i].end_date);
            var dd = String(endDate.getDate()).padStart(2, '0');
            var mm = String(endDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = endDate.getFullYear();

            endDate = dd + '-' + mm + '-' + yyyy;


          var approval = "";
          if (response.data[i].approval == 0) {
            approval =
                '<div class="dropdown action-label"><a class="btn btn-primary btn-sm btn-rounded  dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-purple"></i> Pending</a><div class="dropdown-menu dropdown-menu-right" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-17px, 31px, 0px); z-index:9999;"> <a class="dropdown-item approved_data" href="#" data-toggle="modal" id="' +
                response.data[i].job_id +
                '" data-target="#approved_job"><i class="fa fa-dot-circle-o text-success"></i> Approved</a> </div></div>';
            }
            
            if (response.data[i].approval == 1) {
                approval = '<div class="dropdown action-label"><a class="btn btn-success btn-sm btn-rounded " href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-success"></i> Approved</a></div>';
            }

            var tr = document.createElement('tr');
            tr.innerHTML = 
            '<td class="text-center text-uppercase">' + response.data[i].job_number + '</td>' +

            '<td class="text-center">' + response.data[i].job_title + '</td>' +

            '<td class="text-center">' + response.data[i].branch_name + '</td>' +

            '<td class="text-center">' + response.data[i].department_name + '</td>' +

            '<td class="text-center">' + startDate + '</td>' +

            '<td class="text-center">' + endDate + '</td>' +

            '<td class="text-center" style="color:'+ response.data[i].type_color +'">' + response.data[i].job_type + '</td>' +

            '<td class="text-center" style="color:'+ response.data[i].status_color +'">' + response.data[i].job_applicant_status + '</td>' +

            '<td class="text-center"><a href="jobApplicants/'+ response.data[i].job_id +'" class="viewbutton btn-primary btn">'+response.data[i].total_candidate+'</a></td>' +

            '<td class="text-center">' + approval + '</td>' +
           
           '<td class="text-center"><button type="button" data-toggle="modal" data-target="#status_job" aria-expanded="false" class="btn btn-info edit_status" id="'+ response.data[i].job_id +'" title="Edit Status"><i class="fa fa-eye"></i></button> <a href="JobEdit/' + response.data[i].job_id +'" class="btn btn-info edit_data" title="Edit Job" id="'+ response.data[i].job_id +'"><i class="fa fa-edit"></i></a> <button type="button" data-toggle="modal" data-target="#delete_job" aria-expanded="false" class="btn btn-danger delete_data" id="'+ response.data[i].job_id +'" title="Delete Job" ><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr)
        }
        $('#jobtable').DataTable({
            "buttons": ["excelHtml5", "pdfHtml5"],
        });
    });


    //Select Job Status by api
    $.ajax({
        url: base_url + "viewJobApplicantStatus",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#editstatus");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Job Status</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.job_applicant_status));
        });
    });

    // Active or inactive ticket form fill
    $(document).on("click", ".edit_status", function () {
        var job_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewJob",
            method: "POST",
            data: {
                job_id: job_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#statusjobsid").val(response["data"][0]["job_id"]);
                $("#editstatus").val(response["data"][0]["job_status"]);

                $("#status_job").modal("show");
            },
        });
    });

    // Active or inactive ticket form edit
    $("#statusjobs").submit(function (e) {
        var formData = {
            job_id: $("input[name=statusjobsid]").val(),
            job_status: $("select[name=editstatus]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "changeJobStatus",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>JobDashboard";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#statusjobs [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Manage Job */
    $(document).on("click", ".delete_data", function () {
        var job_id = $(this).attr("id");
        $(".delete_managejob_button").attr("id", job_id);
        $("#delete_managejob").modal("show");
    });

    $(document).on("click", ".delete_managejob_button", function () {
        var job_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteJob",
            method: "POST",
            data: { job_id: job_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                window.location.replace("<?php echo base_url(); ?>JobDashboard");
            },
        });
    });

    // Add Job type validation form
    $(document).ready(function () {
        $("#addjobtypebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addjobtypename").val().trim() == "") {
                $("#addjobtypename").focus();
                $("#addjobtypenamevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addjobtypenamevalidate").text("");
            }
            if ($("#addjobtypestatuscolor").val().trim() == "") {
                $("#addjobtypestatuscolor").focus();
                $("#addjobtypestatuscolorvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addjobtypestatuscolorvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Add Applicant Status validation form
    $(document).ready(function () {
        $("#addapplicantstatusbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addapplicantstatusname").val().trim() == "") {
                $("#addapplicantstatusname").focus();
                $("#addapplicantstatusvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addapplicantstatusvalidate").text("");
            }

            if ($("#addstatuscolor").val().trim() == "") {
                $("#addstatuscolor").focus();
                $("#addstatuscolorvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addstatuscolorvalidate").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
