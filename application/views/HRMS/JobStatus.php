<style>
textarea {
  resize: none;
}
</style>

<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Job Status</li>
            </ul>
        </div>
    </div>
</div>

<!-- <div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3"><div class="card-header"><h3 class="card-title"><strong>Add Job Status</strong></h3></div><div class="card-body-open"><form id="addapplicantstatus" method="POST" action="#"><div class="row clearfix"><div class="form-group col-lg-4 col-md-6 col-sm-12"><label>Job Status<span class="text-danger"> *</span></label><br> <span class="text-danger change-pos" id="addapplicantstatusvalidate"></span> <input name="addapplicantstatusname" id="addapplicantstatusname" class="form-control name-valid" type="text" placeholder="Please enter job status"  autocomplete="off"> </div> <div class="form-group col-lg-4 col-md-6 col-sm-12">  <label>Status Color<span class="text-danger"> *</span></label> <br> <span class="text-danger change-pos" id="addstatuscolorvalidate"></span> <input name="addstatuscolor" id="addstatuscolor" class="form-control" type="color" placeholder="e.g. #Red" autocomplete="off"> </div><div class="form-group col-lg-4 col-md-6 col-sm-12 button-open"><button id="addapplicantstatusbutton" class="btn btn-success submit-btn">Submit</button> <button type="reset" class="btn btn-secondary"  data-dismiss="modal">Reset</button> </div>  </div> </form> </div> </div>
        
    </div>            
</div>  -->


<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Job Status Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-sm" id="applicantstatus">
                    <thead>
                        <tr>
                            <th class="text-center">Jobs Status</th>
                            <th class="text-center">Status Color</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="applicantstatustable">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
            
<!-- Edit Job Status Modal -->
<div id="edit_applicantstatus" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Job Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editapplicantstatus" method="POST">
                    <div class="form-group col-md-12">
                        <label>Job Status<span class="text-danger"> *</span></label>
                        <br>
                        <span class="text-danger change-pos" id="editapplicantstatusvalidate"></span>
                        <input value="" name="editapplicantstatusname" id="editapplicantstatusname" class="form-control name-valid" type="text" placeholder="Please enter application status"  autocomplete="off">
                        <input value="" id="editapplicantstatusid" name="editapplicantstatusid" class="form-control" type="hidden">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Job Color<span class="text-danger"> *</span></label>
                        <br>
                        <span class="text-danger change-pos" id="editstatuscolorevalidate"></span>
                        <input name="editstatuscolor" id="editstatuscolor" class="form-control" type="color" placeholder="e.g. #000" autocomplete="off" required>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editapplicantstatusbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Job Status Modal -->

<!-- Delete Job StatusModal -->
<!-- <div class="modal custom-modal fade" id="delete_applicantstatus" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" id="" class="btn btn-danger continue-btn delete_applicantstatus_button">Submit</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- /Delete Job Status Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    //for remove validation and empty input field
        $(document).ready(function() {
            $('.modal').click(function(){
                $('input#addapplicantstatusname').val('');
                $('input#addstatuscolor').val('');
                $('span#addapplicantstatusvalidate').prop('')
            });
            
            $('.modal .modal-dialog .modal-body > form').click(function(e){
                e.stopPropagation();
            });
            
            $('form button[data-dismiss]').click(function(){
                $('.modal').click();
            });
        });
        
        $(document).ready(function() {
            $('.modal').click(function(){
                jQuery('#addapplicantstatusvalidate').text('');
                jQuery('#addstatuscolorvalidate').text('');
                jQuery('#editapplicantstatusvalidate').text('');
                jQuery('#editstatuscolorevalidate').text('');
            });
        });



    //Show Applicant Status list
        $.ajax({
            url: base_url + "viewJobApplicantStatus",
            data: {},
            type: "POST",
            dataType: 'json', // what type of data do we expect back from the server
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            }
        })
        .done(function(response) {
            var table = document.getElementById('applicantstatustable');
            for (i in response.data) {
                var tr = document.createElement('tr');
                tr.innerHTML = 
                    '<td class="text-center">' + response.data[i].job_applicant_status + '</td>' +
                    '<td class="text-center">' + response.data[i].status_color + '</td>' +
                    '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_applicantstatus" aria-expanded="false" class="btn btn-info edit_data" title="Edit Job Status" id="' + response.data[i].id + '"><i class="fa fa-edit"></i></button> </td>';
                    /*<button type="button" data-toggle="modal" data-target="#delete_applicantstatus" aria-expanded="false" class="btn btn-danger delete_data" id="' + response.data[i].id + '" title="Delete Job Type" ><i class="fa fa-trash-alt"></i></button>*/
                table.appendChild(tr);
            }
            $('#applicantstatus').DataTable();
        })

    // Add Apllicants Status
        /*$("#addapplicantstatus").submit(function(e) {
            var formData = {
                'job_applicant_status': $('input[name=addapplicantstatusname]').val(),
                'status_color': $('input[name=addstatuscolor]').val()
            };

            e.preventDefault();
    
            $.ajax({
                type: 'POST',
                url: base_url + 'addJobApplicantStatus',
                data: formData,
                dataType: 'json',
                encode: true,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response) {
                console.log(response);
                var jsonDa = response;
                
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                    setTimeout(function(){window.location ="<?php echo base_url() ?>JobStatus"},1000);
                  
                } 

                else {
                    toastr.error(falsedata);
                        $('#addapplicantstatus [type=submit]').attr('disabled',false);
                }
            });
        });*/
    
    //Edit  Applicant Status Form Fill
        $(document).on('click', '.edit_data', function() {
            var job_applicant_status_id = $(this).attr("id");
            $.ajax({
                url: base_url + "viewJobApplicantStatus",
                method: "POST",
                data: {
                    job_applicant_status_id: job_applicant_status_id
                },
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                },
                success: function(response) {
                    $('#editapplicantstatusname').val(response["data"][0]["job_applicant_status"]);
                    $('#editapplicantstatusid').val(response["data"][0]["id"]);
                    $('#editstatuscolor').val(response["data"][0]["status_color"]);
                    $('#edit_applicantstatus').modal('show');
                }
            });
        });
    
    //Edit Applicant Status form
        $("#editapplicantstatus").submit(function(e) {
            var formData = {
                'job_applicant_status_id': $('input[name=editapplicantstatusid]').val(),
                'job_applicant_status': $('input[name=editapplicantstatusname]').val(),
                'status_color': $('input[name=editstatuscolor]').val()
            };
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: base_url + 'editJobApplicantStatus',
                data: formData,
                dataType: 'json',
                encode: true,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                    setTimeout(function(){window.location ="<?php echo base_url() ?>JobStatus"},1000);
                  
                } 

                else {
                    toastr.error(falsedata);
                        $('#editapplicantstatus [type=submit]').attr('disabled',false);
                }
            });
        });
    
    //Delete Applicants Status 
        /*$(document).on('click', '.delete_data', function() {
            var job_applicant_status_id = $(this).attr("id");
            $('.delete_applicantstatus_button').attr('id', job_applicant_status_id);
            $('#delete_applicantstatus').modal('show');
        });
    
        $(document).on('click', '.delete_applicantstatus_button', function() {
            var job_applicant_status_id = $(this).attr("id");
            $.ajax({
                url: base_url + "deleteJobApplicantStatus",
                method: "POST",
                data: {
                    job_applicant_status_id: job_applicant_status_id
                },
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                },
                success: function(response) {
                    console.log(response);
                    var jsonDa = response;                
                    
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function(){window.location ="<?php echo base_url()?>JobStatus"},1000);
                      
                    } 

                    else {
                        toastr.error(falsedata);
                        $('#delete_applicantstatus_button [type=submit]').attr('disabled',false);
                    } 
                }
            });
        });*/
 

    // Add Applicant Status validation form
        /*$(document).ready(function () {
            $("#addapplicantstatusbutton").click(function (e) {
                e.stopPropagation();
                var errorCount = 0;
                if ($("#addapplicantstatusname").val().trim() == "") {
                    $("#addapplicantstatusname").focus();
                    $("#addapplicantstatusvalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addapplicantstatusvalidate").text("");
                }
                if ($("#addstatuscolor").val().trim() == "") {
                    $("#addstatuscolor").focus();
                    $("#addstatuscolorvalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addstatuscolorvalidate").text("");
                }
                if (errorCount > 0) {
                    return false;
                }
            });
        });
*/
    // Edit Applicant Status validation form
        $(document).ready(function () {
            $("#editapplicantstatusbutton").click(function (e) {
                e.stopPropagation();
                var errorCount = 0;
                if ($("#editapplicantstatusname").val().trim() == "") {
                    $("#editapplicantstatusname").focus();
                    $("#editapplicantstatusvalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editapplicantstatusvalidate").text("");
                }
                if ($("#editstatuscolor").val().trim() == "") {
                    $("#editstatuscolor").focus();
                    $("#editstatuscolorevalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editstatuscolorevalidate").text("");
                }
                if (errorCount > 0) {
                    return false;
                }
            });
        });
        
</script>