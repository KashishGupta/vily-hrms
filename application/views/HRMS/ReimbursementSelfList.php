<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Reimbursement Self</li>
                
            </ul>
            <div class="header-action mt-3">
                <a href="<?php echo base_url(); ?>ReimbursementSelfAdd" class="btn btn-primary"><i class="fe fe-plus mr-2"></i>Add Reimbursement</a>
            </div>
        </div>


        <!-- Search Employee List Result Table -->
        <div class="card mt-3">
            <div class="card-status bg-orange"></div>
            <div class="card-header">
                <h3 class="card-title"><strong>Reimbursement's Self List</strong></h3>
                <div class="card-options">
                    <a href="<?php echo base_url(); ?>ReimbursementSelf" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xl" id="employeetablebody">
                    <thead>
                        <tr>
                            <th class="text-center">Start City</th>
                            <th class="text-center">End City</th>
                            <th class="text-center">Start Date</th>
                            <th class="text-center">End date</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Approved Amount</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="employeedatatable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

   
        //Show Employee In table
        $.ajax({
            url: base_url + "viewTourEmployee",
            data: {},
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            $("#employeetablebody").DataTable().clear().destroy();
                $("#employeetablebody tbody").empty();
            var table = document.getElementById("employeedatatable");
            for (i in response.data) {

                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }

               
               
                var arrivactiondate = "";
                if (response.data[i].start_date == "0000-00-00") {
                    arrivactiondate = "Nil";
                } else {
                    var startDate = new Date(response.data[i].start_date);
                var dd = String(startDate.getDate()).padStart(2, "0");
                var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = startDate.getFullYear();

                startDate = dd + "-" + mm + "-" + yyyy;
                    arrivactiondate = startDate;
                }
                var arrivalEnddate = "";
                if (response.data[i].end_date == "0000-00-00") {
                    arrivalEnddate = "Nil";
                } else {
                    var endDate = new Date(response.data[i].end_date);
                var dd = String(endDate.getDate()).padStart(2, "0");
                var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = endDate.getFullYear();

                endDate = dd + "-" + mm + "-" + yyyy;
                    arrivalEnddate = endDate;
                }
                var touramount =  response.data[i].amount ;
                var hotelamount =  response.data[i].hotel_amount ;
                var otheramount =  response.data[i].employee_amount ;
                var foodamount =  response.data[i].food_amount ;
                var totalamount = Number(touramount) + Number(hotelamount) +Number(otheramount) + Number(foodamount);
               

                var totalamount = Number(touramount) + Number(hotelamount) + Number(otheramount) + Number(foodamount);
                var adminamount = response.data[i].fixed_amount;
                var adminhotelamount = response.data[i].clear_amount;
                var adminotheramount = response.data[i].remarked_amount;
                var adminfoodamount = response.data[i].admin_amount;

                var totaladminamount = Number(adminamount) + Number(adminhotelamount) + Number(adminotheramount) + Number(adminfoodamount);
                var tr = document.createElement("tr");
                tr.innerHTML =

                    '<td class="text-center">' +
                    response.data[i].starting_city +
                    "</td>" +

                    '<td class="text-center">' +
                    response.data[i].end_city +
                    "</td>" +

                    '<td class="text-center">' +
                    arrivactiondate +
                    "</td>" +
                    '<td class="text-center">' +
                    arrivalEnddate +
                    "</td>" +
                    
                    '<td class="text-center">' +
                    totalamount +
                    "</td>" +
                    '<td class="text-center">' +
                    totaladminamount +
                    "</td>" +
                    '<td class="text-center"> <a href="<?php echo base_url(); ?>ReimbursementSelfView/' +
                    response.data[i].id +
                    '" class="btn btn-info"><i class="fa fa-eye"></i></a> </td>';
                table.appendChild(tr);
            }
            $("#employeetablebody").DataTable({
                "dom": "Bfrtip",
                "buttons": ["excelHtml5", "pdfHtml5"],
            });
        });
   
</script>