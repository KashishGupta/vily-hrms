<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>EmployeeList">Employee</a></li>
                <li class="breadcrumb-item active">Employee Profile</li>
            </ul>
        </div>
      
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card card-profile mb-0">
                    <div class="card-body">
                        <div class="profile-view">
                            <div class="profile-img-wrap">
                                <div class="profile-img" id="fgfimage">
                                 
                                </div>
                            </div>
                            <div class="profile-basic">
                                <div class="row clearfix">
                                    <div class="col-md-5">
                                        <div class="profile-info-left">
                                            <h4 class="user-name m-t-0 mb-0"><span id="profilenames"></span></h4>
                                            <div class="staff-id"> <span id="profiledesignations"></span> - <span id="profiledepartment"></span></div>
                                            <div class="staff-id"> <span id="empid"></span> - <span id="joiningdate"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Father Name:</div>
                                                <div class="text" id="fathername"></div>
                                            </li>
                                            <li>
                                                <div class="title">Phone:</div>
                                                <div class="text" id="phone"></div>
                                            </li>

                                            <li>
                                                <div class="title">Address:</div>
                                                <div class="text1" id="address"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card tab-box">
                    <div class="row user-tabs">
                        <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
                            <ul class="nav nav-tabs nav-tabs-bottom" style="border-bottom: 0px;">
                                <li class="nav-item"><a href="#personal_details" data-toggle="tab" class="nav-link active">Personal Details</a></li>
                                <li class="nav-item"><a href="#family_details" data-toggle="tab" class="nav-link">Emergency Details</a></li>
                                <li class="nav-item"><a href="#education_experience" data-toggle="tab" class="nav-link">Education & Experience</a></li>
                                <li class="nav-item"><a href="#office_details" data-toggle="tab" class="nav-link">Office Details</a></li>
                                <li class="nav-item"><a href="#bank_details" data-toggle="tab" class="nav-link">Bank Details</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <!-- Profile Details Tab -->
                    <div id="personal_details" class="pro-overview tab-pane fade active show">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                	<div class="card-header">
                                        <h3 class="card-title">Personal Details</h3>
                                	</div>
                                    <div class="card-body">
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Personal Email:</div>
                                                <div class="text text-ellipsis" id="email"></div>
                                            </li>
                                            <li>
                                                <div class="title">Birthday:</div>
                                                <div class="text" id="dob"></div>
                                            </li>
                                            <li>
                                                <div class="title">Gender:</div>
                                                <div class="text" id="gender"></div>
                                            </li>
                                            <li>
                                                <div class="title">Nationality:</div>
                                                <div class="text" id="nationality"></div>
                                            </li>
                                            <li>
                                                <div class="title">Religion:</div>
                                                <div class="text" id="religion"></div>
                                            </li>
                                            <li>
                                                <div class="title">Marital Status:</div>
                                                <div class="text" id="maritalstatus"></div>
                                            </li>
                                            <li>
                                                <div class="title">No. of Children:</div>
                                                <div class="text" id="noofchildren"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                	<div class="card-header">
                                        <h3 class="card-title">Personal Identity Proof</h3>
                                	</div>
                                    <div class="card-body">
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Aadhar Card No.</div>
                                                <div class="text" id="aadharcardno"></div>
                                            </li>
                                            <li>
                                                <div class="title">Pancard No.</div>
                                                <div class="text text-uppercase" id="pancardno"></div>
                                            </li>
                                            <li>
                                                <div class="title">Passport No.</div>
                                                <div class="text text-uppercase" id="passportno"></div>
                                            </li>
                                            <li>
                                                <div class="title">Passport Exp Date:</div>
                                                <div class="text" id="passportexpiry"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Profile Details Tab -->

                    <!-- Emergency Details Tab -->
                    <div class="tab-pane fade" id="family_details">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                	<div class="card-header">
                                        <h3 class="card-title">Emergency Informations</h3>
                                	</div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Relationship</th>
                                                        <th>Phone</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><div id="emergencyname"></div></td>
                                                        <td id="emergencyrelationship"></td>
                                                        <td id="emergencyphone"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Emergency Details Tab -->

                    <!-- Education & Experience Tab -->
                    <div class="tab-pane fade" id="education_experience">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                    <div class="card-header">
                                        <h3 class="card-title">Education Informations</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="experience-box">
                                            <ul class="experience-list kt-widget__education"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                	<div class="card-header">
                                        <h3 class="card-title">Experience Informations</h3>
                                	</div>
                                    <div class="card-body">
                                        <div class="experience-box">
                                            <ul class="experience-list kt-widget__items"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Education & Experience Tab -->

                    <!-- Office Details Tab -->
                    <div class="tab-pane fade" id="office_details">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                	<div class="card-header">
                                        <h3 class="card-title">Office Details</h3>
                                	</div>
                                    <div class="card-body">
                                        <ul class="personal-info">
                                           
                                            <li>
                                                <div class="title">Branch Name:</div>
                                                <div class="text" id="branchname"></div>
                                            </li>
                                            <li>
                                                <div class="title">Department:</div>
                                                <div class="text" id="department"></div>
                                            </li>
                                            <li>
                                                <div class="title">Designation:</div>
                                                <div class="text" id="designations"></div>
                                            </li>
                                            <li>
                                                <div class="title">Joining Date:</div>
                                                <div class="text" id="joindate"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Office Details Tab -->

                    <!-- Bank Details Tab -->
                    <div class="tab-pane fade" id="bank_details">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                	<div class="card-header">
                                        <h3 class="card-title">Bank information</h3>
                                	</div>
                                    <div class="card-body">
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Bank Name:</div>
                                                <div class="text" id="bankname"></div>
                                            </li>
                                            <li>
                                                <div class="title">Branch Name:</div>
                                                <div class="text" id="bankbranchname"></div>
                                            </li>
                                            <li>
                                                <div class="title">Account Holder Name:</div>
                                                <div class="text" id="bankholdername"></div>
                                            </li>
                                            <li>
                                                <div class="title">Bank Account No:</div>
                                                <div class="text" id="bankaccountno"></div>
                                            </li>
                                            <li>
                                                <div class="title">IFSC Code:</div>
                                                <div class="text text-uppercase" id="ifsecode"></div>
                                            </li>
                                            <li>
                                                <div class="title">PAN No:</div>
                                                <div class="text text-uppercase" id="pancardno1"></div>
                                            </li>
                                            <li>
                                                <div class="title">Aadhaar No:</div>
                                                <div class="text" id="aadharno"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Bank Details Tab -->
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script>

    //Show Profile list
        var user_code = '<?php echo $userid; ?>';
        $.ajax({
            url: base_url+"viewUserEdit",
            data: {user_code: user_code},
            method:"POST",
            dataType:"json",
            beforeSend: function(xhr){
                xhr.setRequestHeader('Token', localStorage.token);
            },

            success:function(response){

                var joiningDate = new Date(response.data[0].joining_date);
                var dd = String(joiningDate.getDate()).padStart(2, '0');
                var mm = String(joiningDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = joiningDate.getFullYear();

                joiningDate = dd + '-' + mm + '-' + yyyy;


                var joindate = new Date(response.data[0].joining_date);
                var dd = String(joindate.getDate()).padStart(2, '0');
                var mm = String(joindate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = joindate.getFullYear();

                joindate = dd + '-' + mm + '-' + yyyy;


                var dob = new Date(response.data[0].dob);
                var dd = String(dob.getDate()).padStart(2, '0');
                var mm = String(dob.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = dob.getFullYear();

                dob = dd + '-' + mm + '-' + yyyy;


                var passportexpiry = new Date(response.data[0].passport_expiry);
                var dd = String(passportexpiry.getDate()).padStart(2, '0');
                var mm = String(passportexpiry.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = passportexpiry.getFullYear();

                passportexpiry = dd + '-' + mm + '-' + yyyy;


                
                if (!$.trim(response.data[0].user_image)) {
                $("#fgfimage").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
        } else {
            $("#fgfimage").append('<img src="' + response.data[0].user_image + '" />');
        }
              
                $("#profilenames").html(response.data[0].first_name);
                $("#profiledesignations").html(response.data[0].designation_name);
                $("#profiledepartment").html(response.data[0].department_name);
                $("#empid").html(response.data[0].emp_id);
                $("#joiningdate").html(joiningDate);

                $("#phone").html(response.data[0].phone);
                $("#email").html(response.data[0].personal_email);
                $("#fathername").html(response.data[0].father_name);
                $("#dob").html(dob);
                $("#address").html(response.data[0].address);
                $("#gender").html(response.data[0].gender);

                $("#passportno").html(response.data[0].passport_no);
                $("#passportexpiry").html(passportexpiry);
                $("#nationality").html(response.data[0].nationality);
                $("#religion").html(response.data[0].religion);
                $("#maritalstatus").html(response.data[0].marital_status);
                $("#noofchildren").html(response.data[0].no_of_children);

                $("#emergencyname").html(response.data[0].contactperson_name);
                $("#emergencyrelationship").html(response.data[0].relationship);
                $("#emergencyphone").html(response.data[0].contactperson_no);

                $("#companyname").html(response.data[0].company_name);
                $("#branchname").html(response.data[0].branch_name);
                $("#department").html(response.data[0].department_name);
                $("#designations").html(response.data[0].designation_name);
                $("#joindate").html(joindate);


                $("#companyName").html(response.data[0].company_name);
                $("#bankname").html(response.data[0].bank_name);
                $("#bankbranchname").html(response.data[0].branch);
                $("#bankholdername").html(response.data[0].holder_name);
                $("#bankaccountno").html(response.data[0].account_no);
                $("#ifsecode").html(response.data[0].ifsc);
                $("#pancardno").html(response.data[0].pan_card);
                $("#pancardno1").html(response.data[0].pan_card);
                $("#aadharno").html(response.data[0].aadhar_card);
                $("#aadharcardno").html(response.data[0].aadhar_card);
            localStorage.removeItem('$userid');
            }
        });

    // Show Experience
        var user_code = '<?php echo $userid; ?>';
        $.ajax({
            url: base_url + "viewExperienceEmployee",
            data: {user_code: user_code},
            type: "POST",
            dataType: 'json',
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            }
        })
        .done(function(response) {
            for (i in response.data) {

                var periodfrom = new Date(response.data[0].period_from);
                var dd = String(periodfrom.getDate()).padStart(2, '0');
                var mm = String(periodfrom.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = periodfrom.getFullYear();

                periodfrom = dd + '-' + mm + '-' + yyyy;


                var periodto = new Date(response.data[0].period_to);
                var dd = String(periodto.getDate()).padStart(2, '0');
                var mm = String(periodto.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = periodto.getFullYear();

                periodto = dd + '-' + mm + '-' + yyyy;
                var html ='<li><div class="experience-user"><div class="before-circle"></div></div><div class="experience-content"><div class="timeline-content"><a href="#/" class="name">' + response.data[i].company_name + '</a><div>' + response.data[i].location + '</div><div>' + response.data[i].job_position + '</div><span class="time">' + periodfrom + '</span> - <span class="time">' + periodto + '</span></div></div></li>';
                $('.kt-widget__items').append(html);
            }
        });

    // Show Education
        var user_code = '<?php echo $userid; ?>';
        $.ajax({
            url: base_url + "viewEducationbEmployee",
            data: {user_code: user_code},
            type: "POST",
            dataType: 'json',
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            }
        })
        .done(function(response) {
            for (i in response.data) {

                var endDate = new Date(response.data[0].end_date);
                var dd = String(endDate.getDate()).padStart(2, '0');
                var mm = String(endDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = endDate.getFullYear();

                endDate = dd + '-' + mm + '-' + yyyy;
                var html ='<li><div class="experience-user"><div class="before-circle"></div></div><div class="experience-content"><div class="timeline-content"><a href="#/" class="name">' + response.data[i].institution + '</a> <div>' + response.data[i].education_degree + '</div><div>' + response.data[i].subject + '</div><div>' + response.data[i].grade + '</div><span class="time">' + endDate + '</span> </div></div></li>';
                $('.kt-widget__education').append(html);
            }
        });
</script>
