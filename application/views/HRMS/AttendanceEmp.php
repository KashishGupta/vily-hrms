<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Attendance</li>
            </ul>
        </div>

        <div class="row mt-3">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div class="card punch-status">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Timesheet</strong></h3>
                    </div>
                    <div class="card-body">
                        <div class="punch-det">
                            <h6>Punch In at</h6>
                            <p id="dateval"></p>
                        </div>
                        <div class="punch-info">
                            <div class="punch-hours">
                                <span>3.45 hrs</span>
                            </div>
                        </div>
                        <div class="punch-det">
                            <h6>Punch Out at</h6>
                            <p id="datevalout"></p>
                        </div>
                        <div class="statistics">
                            <div class="row">
                                <div class="col-md-6 col-6 text-center">
                                    <!-- <div class="stats-box">
                                        <p>Break</p>
                                        <h6>1.21 hrs</h6>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6 text-center">
                                    <div class="stats-box">
                                        <p>Overtime</p>
                                        <h6>3 hrs</h6>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><strong>ACTIVITY</strong></h3>
                    </div>
                    <div class="card-status"></div>
                    <div class="card-body" style="height: 374px;">
                        <ul class="new_timeline mt-3 kt-widget__items"></ul>
                    </div>
                </div>
            </div>

            <div class="col-xl-12">
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Attendance List</strong></h3>
                    </div>
                    <div class="card-body">
                        <form id="filter-form" method="POST" action="#">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label>Start Date<span class="text-danger"> *</span></label><br />
                                    <span id="filterstartdatevalidate" class="text-danger change-pos"></span>
                                    <input data-provide="datepicker" data-date-autoclose="true" id="startDate" name="startDate" class="form-control" type="text" placeholder="yyyy/mm/dd" readonly="" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label>End Date<span class="text-danger"> *</span></label><br />
                                    <span id="filterenddatevalidate" class="text-danger change-pos"></span>
                                    <input data-provide="datepicker" data-date-autoclose="true" id="enddate" name="enddate" class="form-control" type="text" placeholder="yyyy/mm/dd" readonly="" />
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                                    <button id="filterattendancebutton" class="btn btn-success submit-btn">Search</button>
                                </div>
                            </div>
                        </form>
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xl" id="employeetablebody">
                            <thead>
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Branch</th>
                                    <th class="text-center">Designation</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Date</th>
                                </tr>
                            </thead>
                            <tbody id="employeedatatable"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Attendance Modal -->
    <div class="modal custom-modal fade" id="attendance_info" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Attendance Info</h5>
                    <button type="button" class="close" onclick="location.href='<?php echo base_url()?>AttendanceEmp'" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="Attendance_print">
                    <div class="modal-body">
                        <div class="row" style="display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;flex-wrap: wrap;margin-right: -7.5px;margin-left: -7.5px;">
                            <div class="col-md-6" style=" -ms-flex: 0 0 50%;flex: 0 0 50%;max-width: 50%;">
                                <div class="card punch-status" style="position: relative;display: -ms-flexbox;display: flex;-ms-flex-direction: column;flex-direction: column;min-width: 0;word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 1px solid #E6E9ED;border-radius: 0.25rem;box-shadow: 0 3px 10px rgba(41,43,48,0.05);-webkit-transition: all 0.5s ease-in-out;-moz-transition: all 0.5s ease-in-out;-ms-transition: all 0.5s ease-in-out;-o-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;width: 100%;">
                                    <div class="card-body" style="padding: 20px 20px;flex: 1 1 auto;position: relative;">
                                        <h5 class="card-title" style="font-size: 16px;font-weight: 400;line-height: 1.2;text-transform: uppercase;">Timesheet <small class="text-muted" id="datevaloutpeupper"></small></h5>
                                        <div class="punch-det" style="background-color: #f9f9f9;border: 1px solid #e3e3e3;border-radius: 4px;margin-bottom: 15px;padding: 10px 15px;">
                                            <h6 style="line-height: 20px;margin-bottom: 0;font-size: 12px;">Punch In at</h6>
                                            <p id="datevalperdaye"></p>
                                        </div>
                                        <div class="punch-info" style="margin-bottom: 20px;">
                                            <div class="punch-hours" style="    align-items: center;background-color: #f9f9f9;border: 5px solid #e3e3e3;border-radius: 50%;display: flex;font-size: 18px;height: 120px;justify-content: center;margin: 0 auto;width: 120px;">
                                                <span>3.45 hrs</span>
                                            </div>
                                        </div>
                                        <div class="punch-det" style="background-color: #f9f9f9;border: 1px solid #e3e3e3;border-radius: 4px;margin-bottom: 0;padding: 10px 15px;">
                                            <h6 style="line-height: 20px;margin-bottom: 0;font-size: 12px;">Punch Out at</h6>
                                            <p id="datevaloutperDay" style="color: #727272;font-size: 12px;margin-bottom: 0;"></p>
                                        </div>

                                        <!-- <div class="statistics">
                                            <div class="row">
                                                <div class="stats-box">
			                                        <p>Break</p>
			                                        <h6>1.21 hrs</h6>
			                                    </div>
			                                </div>
			                                <div class="col-md-6 col-6 text-center">
			                                    <div class="stats-box">
			                                        <p>Overtime</p>
			                                        <h6>3 hrs</h6>
			                                    </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style=" -ms-flex: 0 0 50%;flex: 0 0 50%;max-width: 50%;">
                                <div class="card recent-activity"style="position: relative;display: -ms-flexbox;display: flex;-ms-flex-direction: column;flex-direction: column;min-width: 0;word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 1px solid #E6E9ED;border-radius: 0.25rem;box-shadow: 0 3px 10px rgba(41,43,48,0.05);-webkit-transition: all 0.5s ease-in-out;-moz-transition: all 0.5s ease-in-out;-ms-transition: all 0.5s ease-in-out;-o-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;width: 100%;">
                                    <div class="card-body" style="padding: 20px 20px;flex: 1 1 auto;position: relative;">
                                        <h5 class="card-title"  style="font-size: 16px;font-weight: 400;line-height: 1.2;text-transform: uppercase;">Activity</h5>
                                        <ul class="new_timeline mt-3 kt-widgettest__items" style="padding: 0 0 0 5px;list-style: none;position: relative;"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <a onClick="PrintElem('#Attendance_print')" class="btn btn-primary" style="font-weight: 600; font-size: 15px; color: #fff; text-align: right;">
                                Print Attendance Details
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /Attendance Modal -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <script>
        function PrintElem(elem) {
            //Popup($(elem).html());
            Popup($("<div/>").append($(elem).clone()).html());
        }

        function Popup(data) {
            var mywindow = window.open("", "payroll", "height=400,width=600");
            mywindow.document.write("<html><head><title>Attendance Details</title>");
            mywindow.document.write('<link rel="stylesheet" href="assets/css/neon-theme.css">');
            mywindow.document.write('<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css">');
            mywindow.document.write("</head><body >");
            mywindow.document.write(data);
            mywindow.document.write("</body></html>");
            mywindow.document.close();
            mywindow.onload = function () {
                // necessary if the div contain images

                mywindow.focus(); // necessary for IE >= 10
                mywindow.print();
                mywindow.close();
            };
            return true;
        }
        //Show Employee In table
        $(function () {
            filterDepartment("all");
            $("#filter-form").submit();
        });
        function filterDepartment() {
            $("#filter-form").off("submit");
            $("#filter-form").on("submit", function (e) {
                e.preventDefault();
                start_date = $("#startDate").val();
                end_date = $("#enddate").val();
                //alert(start_date);
                if (start_date == "") {
                    start_date = "all";
                }
                req = {};
                req.start_date = start_date;
                req.end_date = end_date;
                $.ajax({
                    url: base_url + "viewAttendanceSelfUserFilter",
                    data: req,
                    type: "POST",
                    dataType: "json",
                    encode: true,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Token", localStorage.token);
                    },
                }).done(function (response) {
                    $("#employeetablebody").DataTable().clear().destroy();
                    $("#employeetablebody tbody").empty();
                    var table = document.getElementById("employeedatatable");
                    for (i in response.data) {
                        var de = new Date();
                        var dateObj = new Date("May 18, 89 " + response.data[i].in_time + "");
                        var difference = de.getTime() - dateObj.getTime();
                        var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                        difference -= daysDifference * 1000 * 60 * 60 * 24;

                        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
                        difference -= hoursDifference * 1000 * 60 * 60;

                        var minutesDifference = Math.floor(difference / 1000 / 60);
                        difference -= minutesDifference * 1000 * 60;

                        var secondsDifference = Math.floor(difference / 1000);

                        var attendanceDate = new Date(response.data[i].date);
                        var dd = String(attendanceDate.getDate()).padStart(2, "0");
                        var mm = String(attendanceDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                        var yyyy = attendanceDate.getFullYear();
                        attendanceDate = dd + "-" + mm + "-" + yyyy;

                        var finalstatus = "";
                        if (response.data[i].status == 0) {
                            finalstatus = "Present";
                        }
                        if (response.data[i].status == null) {
                            finalstatus = "Absent";
                        }
                        var UserImage = "";
                        if (!$.trim(response.data[i].user_image)) {
                            UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                        } else {
                            UserImage = response.data[i].user_image;
                        }

                        var tr = document.createElement("tr");
                        tr.innerHTML =
                            '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                            UserImage +
                            '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                            response.data[i].user_code +
                            '" title="">' +
                            response.data[i].first_name +
                            '</a> <p class="mb-0">' +
                            response.data[i].email +
                            "</p> </div> </div></td>" +
                            '<td class="text-center">' +
                            response.data[i].branch_name +
                            "</td>" +
                            '<td class="text-center">' +
                            response.data[i].designation_name +
                            "</td>" +
                            '<td class="text-center">' +
                            finalstatus +
                            "</td>" +
                            '<td class="text-center"><a href="#" class="btn btn-info edit_data" data-toggle="modal" data-target="#attendance_info" id="' +
                            response.data[i].id +
                            '">' +
                            attendanceDate +
                            "</a></td>";
                        table.appendChild(tr);
                    }
                    $("#employeetablebody").DataTable({
                        /*"scrollY": 200,
                "scrollX": true,*/
                        dom: "Bfrtip",
                        buttons: ["excelHtml5", "pdfHtml5"],
                    });
                });
            });
        }

        $.ajax({
            url: base_url + "viewSelfAttendance",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                const d = new Date(response.data[0].date);
                const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
                const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
                const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
                endDate = `${da} ${mo} ${ye}`;
                $("#dateval").html(endDate);
                $("#datevalout").html(endDate);

                for (i in response.data) {
                    var html =
                        '<li><div class="bullet pink"></div><div class="desc"><h3>Punch In at</h3></div><div class="time">' +
                        response.data[i].in_time +
                        '</div></li><li><div class="bullet pink"></div><div class="desc"><h3>Punch Out at</h3></div><div class="time">' +
                        response.data[i].out_time +
                        "</div></li>";
                    $(".kt-widget__items").append(html);
                }
                $("#edit_branch").modal("show");
            },
        });
        //View Attendance List
        $(document).on("click", ".edit_data", function () {
            var attendance_id = $(this).attr("id");

            $.ajax({
                url: base_url + "viewAttendancePerDay",
                method: "POST",
                data: {
                    attendance_id: attendance_id,
                },
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
                success: function (response) {
                    const d = new Date(response.data[0].date);
                    const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
                    const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
                    const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
                    endDate = `${da} ${mo} ${ye}`;
                    $("#datevalperdaye").html(endDate);
                    $("#datevaloutperDay").html(endDate);
                    $("#datevaloutpeupper").html(endDate);
                    for (i in response.data) {
                        var html =
                            '<li style="padding-bottom: 25px;padding-left: 20px;"><div class="bullet pink" style="-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;background: #fff;left: 5px;width: 10px;height: 10px;z-index: 2;position: absolute;border: 2px solid;margin-top: 5px;border-color: #de5d83;"></div><div class="desc"><h3 style="font-weight: 600;margin: 0;font-size: 15px;">Punch In at</h3></div><div class="time" style="font-size: 12px;">' +
                            response.data[i].in_time +
                            '</div></li><li style="padding-bottom: 25px;padding-left: 20px;"><div class="bullet pink" style="-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;background: #fff;left: 5px;width: 10px;height: 10px;z-index: 2;position: absolute;border: 2px solid;margin-top: 5px;border-color: #de5d83;"></div><div class="desc"><h3 style="font-weight: 600;margin: 0;font-size: 15px;">Punch Out at</h3></div><div class="time" style="font-size: 12px;">' +
                            response.data[i].out_time +
                            "</div></li>";
                        $(".kt-widgettest__items").append(html);
                    }
                    $("#edit_branch").modal("show");
                },
            });
        });
    </script>
</div>
