<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Leave Type</li>
            </ul>
        </div>

        <!-- Add Leave Type -->
        <div class="card mt-3" >
            <div class="card-header">
                <h3 class="card-title"><strong>Add Leave Type</strong></h3>
            </div>
            <div class="card-body">
                <form id="addleavetype" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>Leave Type<span class="text-danger"> *</span></label><br />
                            <span id="addleavetypevalidate" class="text-danger change-pos"></span><input name="addleavetypename" id="addleavetypename" class="form-control" type="text" placeholder="Please enter leave type name." autocomplete="off" />
                        </div>
                        
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>Leave Type Description<span class="text-danger"> *</span></label><br />
                            <span id="addleavetypedescriptionvalidate" class="text-danger change-pos"></span>
                            <textarea rows="2" name="addleavetypedescription" id="addleavetypedescription" class="form-control" type="text" placeholder="Please enter leave type Description." autocomplete="off" style="resize: none;"></textarea>
                        </div>
                       
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 text-right" id="addLeaveTypeDiv">
                          
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <!-- Search Leave Type Result -->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Leave Type Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-sm" id="leavetype">
                    <thead>
                        <tr>
                        <th class="text-center">Type Id</th>
                            <th class="text-center">Leave Type</th>
                            <th class="text-center">Leave Type Description</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="leavetypetable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Edit Leave Type Modal -->
<div id="edit_leavetype" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Leave Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editleavetype" method="POST">
                    <div class="form-group col-md-12">
                        <label>Leave Type<span class="text-danger"> *</span></label>
                        <br />
                        <span id="editleavetypevalidate" class="text-danger change-pos"></span>
                        <input value="" name="editleavetypename" id="editleavetypename" class="form-control name-valid" type="text" placeholder="Please enter leave type name." autocomplete="off" />
                        <input value="" id="editleavetypeid" name="editleavetypeid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Leave Type Description<span class="text-danger"> *</span></label>
                        <br />
                        <span id="editleavetypedescriptionvalidate" class="text-danger change-pos"></span>
                        <input value="" name="editleavetypedescription" id="editleavetypedescription" class="form-control" type="text" placeholder="Please enter leave type name." autocomplete="off" />
                    </div>
                    
                    <div class="submit-section pull-right">
                        <button id="editleavetypebutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Leave Type Modal -->

<!-- Delete Leave Type Modal -->
<div class="modal custom-modal fade" id="delete_leavetype" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" id="" class="btn btn-danger continue-btn delete_leavetype_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Leave Type Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
$.ajax({
    url: base_url + "viewAllRoll",
    data: {},
    type: "POST",
    dataType: 'json', // what type of data do we expect back from the server
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Token', localStorage.token);
    }
})
.done(function(response) {
    var jsonData = response;
    for (i in response.data) {
  // For Hide Permissions
     if(response.data[i].designation_id == logIndesignation && response.data[i].resource_name =='leave_setting' && response.data[i].can_add == 'Y')
        {
        $('#addLeaveTypeDiv').html('  <button id="addleavetypebutton" type="submit" class="btn btn-success submit-btn">Submit</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">Reset</button>');
        } 
    }
}); 
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#addleavetypename").val("");
            $("input#addnoofleaves").val("");
            $("textarea#addleavetypedescription").val("");
            $("span#addleavetypevalidate").prop("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addleavetypevalidate").text("");
            jQuery("#addnoofleavesvalidate").text("");
            jQuery("#editleavetypevalidate").text("");
            jQuery("#editnoofleavesvalidate").text("");
            jQuery("#addleavetypedescriptionvalidate").text("");
            jQuery("#editleavetypedescriptionvalidate").text("");
        });
    });

    // View Leave Type
    $.ajax({
        url: base_url + "viewLeaveType",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        //var j = 1;
        var table = document.getElementById("leavetypetable");
        for (i in response.data) {
            var tr = document.createElement("tr");
            tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
            '<td class="text-center">' +
                response.data[i].id +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].leave_type +
                "</td>" +
                '<td class="text-center" style="white-space: break-spaces;">' +
                response.data[i].description +
                "</td>" +
                '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_leavetype" aria-expanded="false" id="' +
                response.data[i].id +
                '" class="btn btn-info edit_data" title="Edit Leave Type" ><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_leavetype" aria-expanded="false" id="' +
                response.data[i].id +
                '"  class="btn btn-danger delete_data" title="Delete Leave Type"><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr);
        }
        $("#leavetype").DataTable();
    });

    // Add Leave Type
    $("#addleavetype").submit(function (e) {
       

        var formData = {
            leave_type: $("input[name=addleavetypename]").val(),
            description: $("textarea[name=addleavetypedescription]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addLeaveType",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>LeaveType";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addleavetype [type=submit]").attr("disabled", false);
            }
        });
    });

    // Edit  Leave Type  Form Fill
    $(document).on("click", ".edit_data", function () {
        var leave_type_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewLeaveType",
            method: "POST",
            data: {
                leave_type_id: leave_type_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editleavetypename").val(response["data"][0]["leave_type"]);
                $("#editleavetypeid").val(response["data"][0]["id"]);
                $("#editnoofleaves").val(response["data"][0]["no_of_leave"]);
                $("#editleavetypedescription").val(response["data"][0]["description"]);
                $("#edit_leavetype").modal("show");
            },
        });
    });

    // Edit Leave Type
    $("#editleavetype").submit(function (e) {
        var formData = {
            leave_type_id: $("input[name=editleavetypeid]").val(),
            leave_type: $("input[name=editleavetypename]").val(),
            description: $("input[name=editleavetypedescription]").val(),
           
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editLeaveType",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>LeaveType";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editleavetype [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Leave Type
    $(document).on("click", ".delete_data", function () {
        var leave_type_id = $(this).attr("id");
        $(".delete_leavetype_button").attr("id", leave_type_id);
        $("#delete_leavetype").modal("show");
    });

    $(document).on("click", ".delete_leavetype_button", function () {
        var leave_type_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteLeaveType",
            method: "POST",
            data: {
                leave_type_id: leave_type_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>LeaveType";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_leavetype_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    // Add Leave Type  validation form
    $(document).ready(function () {
        $("#addleavetypebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addleavetypename").val().trim() == "") {
                $("#addleavetypename").focus();
                $("#addleavetypevalidate").text("Please enter a valid leave type.");
                errorCount++;
            } else {
                $("#addleavetypevalidate").text("");
            }

            if ($("#addleavetypedescription").val().trim() == "") {
                $("#addleavetypedescription").focus();
                $("#addleavetypedescriptionvalidate").text("Please enter a valid leave type.");
                errorCount++;
            } else {
                $("#addleavetypedescriptionvalidate").text("");
            }

            if ($("#addnoofleaves").val().trim() == "") {
                $("#addnoofleaves").focus();
                $("#addnoofleavesvalidate").text("Please enter a valid no of leaves.");
                errorCount++;
            } else {
                $("#addnoofleavesvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Edit Leave Type validation form
    $(document).ready(function () {
        $("#editleavetypebutton").click(function () {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editleavetypename").val().trim() == "") {
                $("#editleavetypename").focus();
                $("#editleavetypevalidate").text("Please enter a valid leave type.");
                errorCount++;
            } else {
                $("#editleavetypevalidate").text("");
            }

            if ($("#editleavetypedescription").val().trim() == "") {
                $("#editleavetypedescription").focus();
                $("#editleavetypedescriptionvalidate").text("Please enter a valid leave type.");
                errorCount++;
            } else {
                $("#editleavetypedescriptionvalidate").text("");
            }

            if ($("#editnoofleaves").val().trim() == "") {
                $("#editnoofleaves").focus();
                $("#editnoofleavesvalidate").text("Please enter a valid no of leaves.");
                errorCount++;
            } else {
                $("#editnoofleavesvalidate").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
