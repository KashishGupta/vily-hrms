<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Manager/Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">On Boarding</li>
            </ul>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title">View Offer Letter</h3>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <tbody class="kt-widget__offer">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title">View Personal Document</h3>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <tbody class="kt-widget__personal">
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title">View Experience</h3>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <tbody class="kt-widget__Experience">
                      
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title">View Other Document</h3>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="othertable">
                    <thead>
                        <tr>
                            <th class="text-center">Document Name</th>
                            <th class="text-center">instruction</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody class="kt-widget__other">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Edit Offer Letter Modal -->
<div class="modal custom-modal fade" id="edit_offer" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Offer Letter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editoffer" method="POST">
                    <div class="form-group">
                        <label>Offer Letter<span class="text-danger">*</span></label>
                        <input type="hidden" value="" id="editofferletterid" name="editofferletterid" />
                        <input type="file" id="resumefile" class="dropify form-control" onchange="loading_file();" />
                        <input type="hidden" id="23arrdata" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editbranchbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Offer Letter Modal -->

<!-- Edit Personal Letter Modal -->
<div class="modal custom-modal fade" id="edit_personal" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Personal Letter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editpersonalletter" method="POST" action="#">
                    <div class="form-group">
                        <label>Personal Letter<span class="text-danger">*</span></label>
                        <input type="text" value="" id="editpersonalletterid" name="editpersonalletterid" />
                        <input type="file" id="resumefilepersonal" class="dropify form-control" onchange="loadingpersonal_file();" />
                        <input type="hidden" id="23arrdatapersonal" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="#" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Personal Letter Modal -->

<!-- Edit Experience Letter Modal -->
<div class="modal custom-modal fade" id="edit_experience" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Experience Letter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editexperienceletterform" method="POST">
                    <div class="form-group">
                        <label>Experience Letter<span class="text-danger">*</span></label>
                        <input type="hidden" value="" id="editexperienceletterid" name="editexperienceletterid" />
                        <input type="file" id="resumefileexperience" class="dropify form-control" onchange="loadingexperience_file();" />
                        <input type="hidden" id="23arrdataexperience" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="#" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Experience Letter Modal -->

<!-- Edit Other Letter Modal -->
<div class="modal custom-modal fade" id="edit_other" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Other Letter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editotherletterform" method="POST">
                    <div class="form-group">
                        <label>Other Letter<span class="text-danger">*</span></label>
                        <input type="hidden" value="" id="editotherletterid" name="editotherletterid" />
                        <input type="file" id="resumefileother" class="dropify form-control" onchange="loadingother_file();" />
                        <input type="hidden" id="23arrdataother" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editbranchbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Other Letter Modal -->

<!-- Delete Offer Letter Modal -->
<div class="modal custom-modal fade" id="delete_offerletter" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_offer_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Persoanl Letter Modal -->

<!-- Delete Offer Letter Modal -->
<div class="modal custom-modal fade" id="delete_personal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_persoanal_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Persoanl Letter Modal -->

<!-- Delete Experience Letter Modal -->
<div class="modal custom-modal fade" id="delete_experience" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_experience_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Experience Letter Modal -->

<!-- Delete Other Letter Modal -->
<div class="modal custom-modal fade" id="delete_other" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_other_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Other Letter Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
            // View Offerletter
                $.ajax({
                    url: base_url+"viewLoggedinUser",
                    data: {},
                    method:"POST",  
                    dataType:"json", 
                    beforeSend: function(xhr){
                         xhr.setRequestHeader('Token', localStorage.token);}
                })
                
                .done(function(response) { 
                   
                   if (!$.trim(response.data[0])) {
                        var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                        $(".kt-widget__offer").append(html);
                         } else {
                   
                   for (i in response.data) {
                      
                       var html = '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>' + response.data[i].document_name + '</td>' +
                       
                       ' <td class="text-right"><a href="'+ response.data[i].offer_letter + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> </td></tr>';
                       $(".kt-widget__offer").append(html);
                 
                   }
                }
               });
    $.ajax({
        url: base_url + "viewLoggedinUser",
        data: {},
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        var table = document.getElementById("tabledocument");

        for (i in response.data) {
            var tr = document.createElement("tr");
            tr.innerHTML =
                '<td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>' +
                response.data[i].document_name +
                "</td>" +
                '<td class="text-right"><a href="' +
                response.data[i].offer_letter +
                '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a></td>';
            table.appendChild(tr);
        }
        $("#reimbursementtable").DataTable();
    });

    //Personal Letter
    $.ajax({
        url: base_url + "viewLoggedinPersonal",
        data: {},
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function(response) { 
                 
                    
                 if (!$.trim(response.data[0])) {
                          var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                          $(".kt-widget__personal").append(html);
                           } else {
                     
                     for (i in response.data) {
                         var html =
                         '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>  ' + response.data[i].document_name + '</td>' +
                         
                         ' <td class="text-right"><a href="'+ response.data[i].personaldocument + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> </td></tr>';
                         $(".kt-widget__personal").append(html);  
                     }
                 }
                 });
    // Expierience table
    $.ajax({
        url: base_url + "viewLoggedinExperience",
        data: {},
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function(response) { 
                 
                 if (!$.trim(response.data[0])) {
                      var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                      $(".kt-widget__Experience").append(html);
                       } else {
                 
                 for (i in response.data) {
                     var html = 
                     '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>  ' + response.data[i].document_name + '</td>' +
                     
                     ' <td class="text-right"><a href="'+ response.data[i].experience_letter + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> </td></tr>';
                     $(".kt-widget__Experience").append(html);
                 }
                 }
             });

    // Other Document table
    $.ajax({
        url: base_url + "viewLoggedinOther",
        data: {},
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function(response) { 
                 
                 if (!$.trim(response.data[0])) {
                      var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                      $(".kt-widget__other").append(html);
                       } else {
                 
                 for (i in response.data) {
                   
                    var html= 
                     '<tr><td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg> ' + response.data[i].document_name + '</td>' +
                    
                     '<td class="text-center">' + response.data[i].instruction_document + '</td>' +
                     ' <td class="text-right"><a href="'+ response.data[i].other + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a> </td></tr>';
                     $(".kt-widget__other").append(html);
                 }
             }
             })
</script>
