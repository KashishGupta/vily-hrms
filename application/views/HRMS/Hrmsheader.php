<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
    <?php 

$query1 = $this->db->query("SELECT company_logo FROM `company`");
$arrPolicy = $query1->row();
$arrTest1 = $arrPolicy->company_logo;
$arrTest = $arrPolicy->company_logo;
?>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
        <link rel="icon" href="<?php echo $arrTest;?>" type="image/x-icon" />
        <title><?php echo $title; ?></title>

        <!-- Bootstrap Core and vandor -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/toastr.min.css" />

        <!-- Core css -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dropify.min.css" />

        <!-- Text Editor  -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/summernote.css" />

        <script>
            var base_url = "<?php echo base_url(); ?>api/callApi/";
            var base_urlEvents = "<?php echo base_url(); ?>";
            var logIndesignation = localStorage.designationid;
        </script>
    </head>

    <body class="font-montserrat">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader"></div>
        </div>

        <div id="main_content">
            <div id="header_top" class="header_top">
                <div class="container">
                    <div class="hleft">
                        <a class="header-brand" href="<?php echo base_url(); ?>Home">
                            <img src="<?php echo $arrTest1 ?>" />
                        </a>
                        <div class="dropdown" id="adminEvents"></div>
                        <div class="dropdown" id="adminYellow"></div>
                        <div class="dropdown" id="adminHoliday"></div>
                        <div class="dropdown"><a href="<?php echo base_url(); ?>Notification_emp" class="nav-link icon" title="Notification list"><i class="fa fa-bell"></i></a></div>
                        <div class="dropdown"><a href="<?php echo base_url(); ?>Chat_emp/1" class="nav-link icon" title="Notification list"><i class="fa fa-comments"></i></a></div>
                    </div>
                    <div class="hright">
                        <div class="dropdown">
                            <a href="javascript:void(0)" class="nav-link icon menu_toggle">
                                <i class="fa fa-align-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="left-sidebar" class="sidebar">
                <h5 class="brand-name">Ezdat HRMS</h5>
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul class="metismenu">
                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"> <i class="fa fa-home"></i><span>Home</span> </a>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                                <li id="homeAdminBranch"></li>
                                <li id="homeAdminDepartment"></li>
                                <li id="homeAdminDesignation"></li>
                                <li id="homeAdminEmployee"></li>
                                <li id="homeAdminAttendance"></li>
                                <li id="homeAdminLeaveType"></li>
                                <li id="homeAdminLeave"></li>
                                <li><a href="<?php echo base_url(); ?>LeaveSelf_emp">Leave Self</a></li>
                                <li><a href="<?php echo base_url(); ?>Tickets_emp">Tickets</a></li>
                            </ul>
                        </li>

                        <li class="metismenu_item" id="homeAdminProjectDashboard">
   
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c">
                                <i class="fa fa-suitcase"></i>
                                <span>Job Portal</span>
                            </a>
                            <ul>
                                <li id="homeAdminjobDashboard"></li>
                                <li id="homeAdminjobType"></li>
                                <li id="homeAdminjobStatus"></li>
                                <li id="homeAdminjobs"></li>
                                <li><a href="<?php echo base_url(); ?>JobPublic_emp">Public Jobs</a></li>
                                <li><a href="<?php echo base_url(); ?>JobApplyStatus_emp">My Referral</a></li>
                            </ul>
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c">
                                <i class="fa fa-briefcase"></i>
                                <span>Payroll</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>PayrollView_emp"><span>Payroll View</span> </a>
                                </li>
                            </ul>
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"> <i class="fa fa-object-ungroup"></i><span>Assets Portal</span> </a>

                            <ul>
                                <li id="homeAdminAssetsCategory"></li>
                                <li id="homeAdminAssets"></li>
                                <li id="homeAdminIssueAssets"></li>
                                <li>
                                    <a href="<?php echo base_url(); ?>AssetsSelf_emp"><span>Assets Self</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-rupee-sign"></i><span>Reimbursement</span></a>
                            <ul>
                                <li id="homeAdminReimbursementPending"></li>
                                <li><a href="<?php echo base_url(); ?>ReimbursementSelfList">Self</a></li>
                            </ul>
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-tasks"></i><span>OnBoarding</span></a>
                            <ul>
                                <li id="homeAdminOnboarding"></li>
                                <li><a href="<?php echo base_url(); ?>onBoardingSelf">Self</a></li>
                            </ul>
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-tasks"></i><span>OffBoarding</span></a>
                            <ul>
                                <li id="homeAdminOffboarding"></li>
                                <li><a href="<?php echo base_url(); ?>offboardingSelf">self</a></li>
                            </ul>
                        </li>

                        <li class="metismenu_item" id="homeAdminAttendance"></li>
                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-cog"></i><span>Settings</span></a>
                            <ul>
                                <li id="homeAdminSettingsChangePassword"></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="page">
                <div id="page_top" class="section-body sticky-top">
                    <div class="container-fluid">
                        <div class="page-header">
                            <div class="left">
                                <h1 class="page-title">HRMS Dashboard</h1>
                            </div>
                            <div class="right">
                                <div class="notification d-flex">
                                    <div class="dropdown d-flex">
                                        <a class="nav-link icon d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown">
                                            <i class="fa fa-bell"></i>
                                            <span class="badge badge-success nav-unread" id="totalnotification"></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <ul class="right_chat list-unstyled Kt-OpenNotification w250 p-1"></ul>
                                            <div class="dropdown-divider"></div>
                                            <a href="<?php echo base_url()?>vily/Notification" class="dropdown-item text-center text-muted-dark readall"></a>
                                        </div>
                                    </div>
                                    <div class="dropdown d-flex">
                                        <a href="<?php echo base_url(); ?>Profile" class="nav-link icon d-md-flex btn btn-default btn-icon ml-1">
                                            <i class="fa fa-user"></i>
                                        </a>
                                    </div>
                                    <div class="dropdown d-flex">
                                        <a class="nav-link icon d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown">
                                            <i class="fa fa-power-off"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <form id="logoutdata" method="POST">
                                                <button class="dropdown-item submit-btn" style="border: none; background: none; padding-right: 100px;"><i class="dropdown-icon fe fe-log-out"></i> Log out</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    button.dropdown-item.submit-btn:hover {
                        background: #e8e9e9 !important;
                    }
                </style>

                <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
                <script>
                    //Show Project

                    $.ajax({
                        url: base_url + "viewAllRoll",
                        data: {},
                        type: "POST",
                        dataType: "json", // what type of data do we expect back from the server
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },
                    }).done(function (response) {
                        var jsonData = response;
                        for (i in response.data) {
                            // For Side Menu Calander
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "events" && response.data[i].can_view == "Y") {
                                $("#adminEvents").html('<a href="<?php echo base_url(); ?>Calendar_emp" class="nav-link icon app_inbox" title="Ezdat Calendar"><i class="fa fa-calendar-week"></i> </a>');
                            }

                            // For Side Menu Yellow Page
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "yellow_pages" && response.data[i].can_view == "Y") {
                                $("#adminYellow").html(' <a href="<?php echo base_url(); ?>YellowPageList_emp" class="nav-link icon" title="Employee Contact"><i class="fa fa-id-card"></i></a>');
                            }

                            // For Side Menu Holiday
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "holiday" && response.data[i].can_view == "Y") {
                                $("#adminHoliday").html('<a href="<?php echo base_url(); ?>Holiday_emp" class="nav-link icon" title="Holiday list"><i class="fa fa-plane-departure"></i></a>');
                            }

                           

                            // For Main Menu Branch  (Submenu Home)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "branch" && response.data[i].can_view == "Y") {
                                $("#homeAdminBranch").html('<a href="<?php echo base_url(); ?>Branch_emp">Branch</a>');
                            }

                            // For Main Menu Department  (Submenu Home)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "department" && response.data[i].can_view == "Y") {
                                $("#homeAdminDepartment").html('<a href="<?php echo base_url(); ?>Department_emp">Department</a>');
                            }

                            // For Main Menu Designation  (Submenu Home)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "designation" && response.data[i].can_view == "Y") {
                                $("#homeAdminDesignation").html('<a href="<?php echo base_url(); ?>Designation_emp">Designation</a>');
                            }

                            // For Main Menu Employee  (Submenu Home)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "employee" && response.data[i].can_view == "Y") {
                                $("#homeAdminEmployee").html('<a href="<?php echo base_url(); ?>EmployeeList_emp">Employee</a>');
                            }

                            // For Main Menu Employee  (Submenu Home)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "attendance" && response.data[i].can_view == "Y") {
                                $("#homeAdminAttendance").html('<a href="<?php echo base_url(); ?>Attendance_emp">Attendance</a>');
                            }

                            // For Main Menu Leave Type  (Submenu Home)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "leave_setting" && response.data[i].can_view == "Y") {
                                $("#homeAdminLeaveType").html('<a href="<?php echo base_url(); ?>LeaveType_emp">Leave Type</a>');
                            }

                            // For Main Menu Leave (Submenu Home)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "leave_management" && response.data[i].can_view == "Y") {
                                $("#homeAdminLeave").html('<a href="<?php echo base_url(); ?>Leave_emp">Leave</a>');
                            }

                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "leave" && response.data[i].can_view == "Y") {
                                $("#homeHrLeave").html(" ");
                            }

                            // For Main Menu Project Dashboard (Submenu Project)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "projects" && response.data[i].can_view == "Y") {
                                $("#homeAdminProjectDashboard").html(' <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-list-ul"></i><span>Project</span></a><ul><li><a href="<?php echo base_url(); ?>ProjectList_emp">Projects</a></li></ul>');
                            }

                            // For Main Menu Job Dashboard (Submenu Job)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "jobs" && response.data[i].can_view == "Y") {
                                $("#homeAdminjobDashboard").html('<a href="<?php echo base_url(); ?>JobDashboard_emp">Dashboard</a>');
                            }

                            // For Main Menu Job Type (Submenu Job)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "job_type" && response.data[i].can_view == "Y") {
                                $("#homeAdminjobType").html('<a href="<?php echo base_url(); ?>JobType_emp">Job Type</a>');
                            }

                            // For Main Menu Job Status (Submenu Job)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "job_status" && response.data[i].can_view == "Y") {
                                $("#homeAdminjobStatus").html('<a href="<?php echo base_url(); ?>JobStatus_emp">Job Status</a>');
                            }

                            // For Main Menu Jobs (Submenu Job)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "jobs" && response.data[i].can_view == "Y") {
                                $("#homeAdminjobs").html('<a href="<?php echo base_url(); ?>Jobs_emp">Jobs</a>');
                            }

                            // For Main Menu Assets Category (Submenu Assets)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "assets" && response.data[i].can_view == "Y") {
                                $("#homeAdminAssetsCategory").html(' <a href="<?php echo base_url(); ?>AssetsCategory_emp" ><span>Assets Category</span></a>');
                            }

                            // For Main Menu Company Assets (Submenu Assets)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "assets" && response.data[i].can_view == "Y") {
                                $("#homeAdminAssets").html(' <a href="<?php echo base_url(); ?>Assets_emp" ><span>Company Assets</span></a>');
                            }

                            // For Main Menu Issued Assets (Submenu Assets)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "assets" && response.data[i].can_view == "Y") {
                                $("#homeAdminIssueAssets").html(' <a href="<?php echo base_url(); ?>AssetsManagement_emp" ><span>Assets Management</span></a>');
                            }

                            // For Main Menu Reimbursement Pending (Submenu Reimbursement)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "reimbursement" && response.data[i].can_view == "Y") {
                                $("#homeAdminReimbursementPending").html('<a href="<?php echo base_url(); ?>ReimbursementList_emp">Admin View</a>');
                            }
                            
                            // For Main Menu On Boarding Pending  (Submenu On Boarding)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "onboarding" && response.data[i].can_view == "Y") {
                                $("#homeAdminOnboarding").html('<a href="<?php echo base_url(); ?>onBoardingList_emp" >onBoarding Admin</a>');
                            }

                            // For Main Menu Off Boarding Pending  (Submenu Off Boarding)
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "onboarding" && response.data[i].can_view == "Y") {
                                $("#homeAdminOffboarding").html('<a href="<?php echo base_url(); ?>offBoardingList_emp" >offBoarding</a>');
                            }

                            // For Main Menu Attendance
                            if (response.data[i].designation_id == logIndesignation && response.data[i].resource_name == "attendance" && response.data[i].can_view == "Y") {
                                $("#homeAdminAttendance").html('<a href="<?php echo base_url(); ?>Attendance_emp" ><span>Attendance</span></a>');
                            }

                            // For Main Menu Role Base & Permission (Submenu Setting)
                            if (response.data[i].designation_id == logIndesignation) {
                                $("#homeAdminSettingsRole").html('<a href="<?php echo base_url(); ?>Role_emp">Role Base & Permission</a>');
                            }

                            // For Main Menu Leave Setting (Submenu Setting)
                            if (response.data[i].designation_id == logIndesignation) {
                                $("#homeAdminSettingsLeaveSetting").html('<a href="<?php echo base_url(); ?>LeaveSetting_emp">Leave Setting</a>');
                            }

                            // For Main Menu App Setting   (Submenu Setting)
                            if (response.data[i].designation_id == logIndesignation) {
                                $("#homeAdminSettings").html('<a href="<?php echo base_url(); ?>AppSetting_emp">App Setting</a>');
                            }

                            // For Main Menu Change Password   (Submenu Setting)
                            if (response.data[i].designation_id == logIndesignation) {
                                $("#homeAdminSettingsChangePassword").html('<a href="<?php echo base_url(); ?>ChangePassword_emp">Change password </a>');
                            }
                        }
                    });

                    // Edit Branch form
                    $("#logoutdata").submit(function (e) {
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: base_url + "logout",
                            data: {},
                            dataType: "json",
                            encode: true,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Token", localStorage.token);
                            },
                        }).done(function (response) {
                            localStorage.clear();
                            console.log(response);
                            window.location.replace("<?php echo base_url();?>");
                        });
                    });

                    // view branch form

                    $.ajax({
                        type: "POST",
                        url: base_url + "viewTokenExpired",
                        data: {},
                        dataType: "json",
                        encode: true,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr);
                            console.log(thrownError);
                            console.log(ajaxOptions);
                            if (xhr.status === 401) {
                                window.location = "<?php echo base_url()?>";
                            }
                            //  window.location ="<?php echo base_url()?>";
                            //window.location ="<?php echo base_url()?>";
                        },
                    }).done(function (response) {
                        console.log(response);
                        var resData = response["data"];
                        if (resData == "Unauthorized Token!") {
                            window.location = "<?php echo base_url()?>";
                        } else if (resData == "Token Time Expired.") {
                            window.location = "<?php echo base_url()?>";
                        } else {
                            window.location = "<?php echo base_url()?>Home";
                        }
                    });

                    // all new totalnotification count
                    $.ajax({
                        url: base_url + "viewTotalNotification",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#totalnotification").html(response.data[0].Notification);
                        },
                    });
                    
                    // View Notification Public

                    function getValue(id) { 
                        var notification_id = id;
                       // alert(users_id); 
                            var status = 1;
                            $.ajax({
                            type: "POST",
                            url: base_url + "notificationRead",
                            data: "notification_id=" + notification_id + "&status=" + status,
                            dataType: "json",
                            encode: true,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Token", localStorage.token);
                            },
                            }).done(function (response) {
                            var jsonDa = response;
                            var jsonData = response["data"]["message"];
                        
                                toastr.success(jsonData);
                                setTimeout(function () {
                                    window.location = "<?php echo base_url()?>Notification";
                                }, 1000);
                            
                        });
                    } 
                    
                    $.ajax({
                        url: base_url + "viewNotification",
                        data: {},
                        type: "POST",
                        dataType: "json",
                        encode: true,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },
                    }).done(function (response) {
                        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-lg-12"><div class="card"><div class="card-header"></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! There is No Notification</p></div></div></div>';
            $(".Kt-OpenNotification").append(html);
        } else {
                        for (i in response.data) {
                            var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                            var html =
                                '<li class="online"> <a href="#" onclick="getValue('+response.data[i].id+');"> <div class="media"> <img class="media-object " src="' +
                            UserImage +
                            '" alt=""> <div class="media-body"> <span class="name">' +
                                response.data[i].sender_name +
                                ' <small class="float-right">' +
                                timeago(response.data[i].created_at) +
                                '</small></span> <span class="message">' +
                                response.data[i].message +
                                '</span> <span class="badge badge-outline status"></span> </div> </div> </a> </li>';
                            $(".Kt-OpenNotification").append(html);
                        }
                    }
                    });

                    // all new Employe count
                    $.ajax({
                        url: base_url + "countAllNewEmployee",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#totalnewemployees").html(response.data[0].NewEmployee);
                        },
                    });
                    //Total Tickets
                    $.ajax({
                        url: base_url + "viewTicketsTotal",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#totalticketsheader").html(response.data[0].id);
                            var number = response.data[0].id;
                            var percentToGet = 100;
                            var percent = (percentToGet / 100) * number;
                            $("#totalpercent").html(percent);
                        },
                    });

                    function timeago(stringDate) {
                        var currDate = new Date();
                        var diffMs = currDate.getTime() - new Date(stringDate).getTime();

                        var sec = diffMs / 1000;
                        // alert(sec);
                        if (sec <= 60) return '<span style = "color:red;">just now</span>';
                        var min = sec / 60;
                        if (min < 60) return parseInt(min) + " minute" + (parseInt(min) > 1 ? "s ago" : " ago");
                        var h = min / 60;
                        if (h <= 24) return parseInt(h) > 1 ? parseInt(h) + " hrs ago" : " an hour ago";
                        var d = h / 24;
                        if (d <= 7) return parseInt(d) + " day" + (parseInt(d) > 1 ? "s ago" : " ago");
                        var w = d / 7;
                        if (w <= 4.3) return parseInt(w) > 1 ? parseInt(w) + " weeks ago" : " a week ago";
                        var m = d / 30;
                        if (m <= 12) return parseInt(m) + " month" + (parseInt(m) > 1 ? "s ago" : " ago");
                        var y = m / 12;
                        return parseInt(y) + " year" + (parseInt(y) > 1 ? "s ago" : " ago");
                    }

                    // birthday Self
                    $.ajax({
                        url: base_url + "checkUserBirthday",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#birthdayname")
                                .css({ padding: "4px 10px", "background-color": "#f8d3a8", "border-radius": "5px", animation: "pulse 3s infinite", "box-shadow": " 0 0 0 rgba(67, 74, 84, 0.9)" })
                                .html(response.data[0].first_name);
                        },
                    });
                </script>
