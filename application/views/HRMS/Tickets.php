<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item active">Ticket</li>
            </ul>
            <div class="header-action">
                <a href="<?php echo base_url(); ?>Ticketadd_emp" class="btn btn-primary"><i class="fe fe-plus mr-2"></i>Add Ticket</a>
            </div>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="widgets2">
                            <div class="state">
                                <h6>
                                    Total <br />Tickets
                                </h6>
                                <h2 id="totaltickets" style="margin-bottom: 0px;"></h2>
                            </div>
                            <div class="icon">
                                <i class="fa fa-database"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="widgets2">
                            <div class="state">
                                <h6>
                                    Total <br />
                                    High Priority
                                </h6>
                                <h2 id="totalHighpercent" style="margin-bottom: 0px;"></h2>
                            </div>
                            <div class="icon">
                                <i class="fa fa-users"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="widgets2">
                            <div class="state">
                                <h6>
                                    Total <br />
                                    Medium Priority
                                </h6>
                                <h2 id="totalMediumtickets" style="margin-bottom: 0px;"></h2>
                            </div>
                            <div class="icon">
                                <i class="fa fa-plane-departure"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="widgets2">
                            <div class="state">
                                <h6>
                                    Total <br />
                                    Low Priority
                                </h6>
                                <h2 id="totaLowpercent" style="margin-bottom: 0px;"></h2>
                            </div>
                            <div class="icon">
                                <i class="fa fa fa-envelope"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs b-none">
                                <li class="nav-item">
                                    <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list"><i class="fa fa-ticket-alt"></i>Tickets</a>
                                </li>
                               <!-- <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addnew"><i class="fa fa-thumbs-up"></i>Ticket Type</a>
                                </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="list" role="tabpanel">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Search Tickets Result</strong></h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list dataTable table-responsive table-responsive-xl no-footer" id="Tickettable">
                            <thead>
                                <tr>
                                    <th class="text-center">Ticket Id</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Ticket Type</th>
                                    <th class="text-center">Priority</th>
                                    <th class="text-center">Ticket Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodyTicket"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="addnew" role="tabpanel">
                <!-- Add ticket Type -->
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Add Ticket Type</strong></h3>
                    </div>
                    <div class="card-body">
                        <form id="addtickettype1" method="POST" action="#">
                            <div class="row clearfix">
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Ticket Type<span class="text-danger"> *</span></label><br />
                                    <span id="addtickettypevalidate1" class="text-danger change-pos"></span>
                                    <input name="addtickettypename1" id="addtickettypename1" class="form-control" type="text" placeholder="Please enter ticket type name." autocomplete="off" />
                                </div>
                               
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Ticket Type Description<span class="text-danger"> *</span></label><br />
                                    <span id="addtickettypedescriptionvalidate1" class="text-danger change-pos"></span>
                                    <textarea
                                        rows="2"
                                        name="addtickettypedescription1"
                                        id="addtickettypedescription1"
                                        class="form-control"
                                        type="text"
                                        placeholder="Please enter ticket type Description."
                                        autocomplete="off"
                                        style="resize: none;"
                                    ></textarea>
                                </div>
                                <div class="form-group col-md-4 col-sm-12 button-open">
                                    <button id="addtickettypebutton1" type="submit" class="btn btn-success submit-btn">Submit</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Search ticket Type Result -->
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Search Ticket Type Result</strong></h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="tickettype1">
                            <thead>
                                <tr>
                                    <th class="text-center">Type Id</th>
                                    <th class="text-center">Ticket Type</th>
                                    <th class="text-center">Ticket Type Description</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="tickettypetable1"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Ticket Modal -->
<div class="modal custom-modal fade" id="delete_ticket" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" id="" class="btn btn-danger continue-btn delete_ticket_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Delete Ticket Modal -->


<!-- Edit ticket Type Modal -->
<div id="edit_tickettype" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Ticket Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="edittickettype1" method="POST">
                    <div class="form-group col-md-12">
                        <label>Ticket Type<span class="text-danger"> *</span></label>
                        <br />
                        <span id="edittickettypevalidate1" class="text-danger change-pos"></span>
                        <input value="" name="edittickettypename1" id="edittickettypename1" class="form-control name-valid" type="text" placeholder="Please enter ticket type name." autocomplete="off" />
                        <input value="" id="edittickettypeid1" name="edittickettypeid1" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Ticket Type Description<span class="text-danger"> *</span></label>
                        <br />
                        <span id="edittickettypedescriptionvalidate1" class="text-danger change-pos"></span>
                        <input value="" name="edittickettypedescription1" id="edittickettypedescription1" class="form-control" type="text" placeholder="Please enter ticket type name." autocomplete="off" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="edittickettypebutton1" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit ticket Type Modal -->

<!-- Delete ticket Type Modal -->
<div class="modal custom-modal fade" id="delete_tickettype" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" id="" class="btn btn-danger continue-btn delete_tickettype_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete ticket Type Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script>
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#addtitle").val("");
            $("#addtickettype option:eq(0)").prop("selected", true);
            $("#addpriority option:eq(0)").prop("selected", true);
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addtickettypevalidate").text("");
            jQuery("#addpriorityvalidate").text("");
            jQuery("#addtitlevalidate").text("");
            jQuery("#editsubjectvalidate").text("");
        });
    });

    //Show Ticket list
    $.ajax({
        url: base_url + "viewTickets",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        var table = document.getElementById("tablebodyTicket");
        for (i in response.data) {
            var date = new Date(response.data[i].date);
            var dd = String(date.getDate()).padStart(2, "0");
            var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = date.getFullYear();

            date = dd + "-" + mm + "-" + yyyy;

            var testcheck = "";
            if (response.data[i].status == 0) {
                testcheck = "checked";
            }
            if (response.data[i].status == 1) {
                testcheck = "";
            }
           
            var priority_color = "";
            if (response.data[i].priority == 0) {
                priority_color = "blue";
            }
            if (response.data[i].priority == 1) {
                priority_color = "green";
            }
            if (response.data[i].priority == 2) {
                priority_color = "red";
            }
            var priority = "";
            if (response.data[i].priority == 0) {
                priority = "Low";
            }
            if (response.data[i].priority == 1) {
                priority = "Medium";
            }
            if (response.data[i].priority == 2) {
                priority = "High";
            }

            var UserImage = "";
            if (!$.trim(response.data[i].user_image)) {
                UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
            } else {
                UserImage = response.data[i].user_image;
            }

            var sDate = new Date(response.data[i].date);
            var dd = String(sDate.getDate()).padStart(2, "0");
            var mm = String(sDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = sDate.getFullYear();
            sDate = dd + "-" + mm + "-" + yyyy;

            var tr = document.createElement("tr");
            tr.innerHTML ='<td class="text-center text-uppercase"><a href="TicketsCommnets_emp/'+ response.data[i].id +'" class="viewbutton text-primary">' +
                response.data[i].ticket_no +
                '</a></td>' +
                '<td class="text-center">' +
                sDate +
                '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                        UserImage +
                        '" alt=""></span><div class="ml-3">  <a href="EmployeeView_emp/' +
                        response.data[i].user_code +
                        '" title="">' + response.data[i].emp_id + ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' +
                response.data[i].branch_name +
                '</p> </div> </div></td>' +

                '<td class="text-center">' +
                response.data[i].ticket_type +
                "</td>" +
                
                '<td class="text-center"style="color:' +
                priority_color +
                '">' +
                priority +
                "</td>" +
                '<td class="text-center"> <div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox" '+testcheck+' id="'+response.data[i].id+'" value="'+ response.data[i].status +'" onclick="getstatus('+response.data[i].id+')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>' +
                '<td class="text-center"> <button type="button" data-toggle="modal" data-target="#delete_ticket" aria-expanded="false" id="' +
                response.data[i].id +
                '"  class="btn btn-danger delete_data" title="Delete Ticket"><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr);
        }

        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#Tickettable").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Tickets Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Tickets Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4 ]
                    }
                }
            ]
        });
    });


    function getstatus(id) {
        var ticket_id = id;
        var status = $("#" + id).val();
        console.log(status);
        //alert(status);
        if (status == 0) {
            var status = 1;
            $.ajax({
                type: "POST",
                url: base_url + "editTicketsActive",
                data: "ticket_id=" + ticket_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets_emp";
                }, 1000);
            });
        } else {
            var status = 0;
            $.ajax({
                type: "POST",
                url: base_url + "editTicketsActive",
                data: "ticket_id=" + ticket_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets_emp";
                }, 1000);
            });
        }
    }

    //edit Tickets form
    $("#activetickets").submit(function (e) {
        var formData = {
            ticket_id: $("input[name=activeticketsid]").val(),
            status: $("select[name=editstatus]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editTicketsActive",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets_emp";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#activetickets [type=submit]").attr("disabled", false);
            }
        });
    });

    //Edit  Tickets Form Fill
    $(document).on("click", ".edit_data", function () {
        var ticket_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewTickets",
            method: "POST",
            data: {
                ticket_id: ticket_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#ticketsid").val(response["data"][0]["id"]);
                $("#editsubject").val(response["data"][0]["title"]);
                $("#edittickettype").val(response["data"][0]["ticket_type"]);
                $("#editpriority").val(response["data"][0]["priority"]);
                $("#edit_Ticket").modal("show");
            },
        });
    });

    //edit Tickets form
    $("#edittickets").submit(function (e) {
        var formData = {
            comment: $("textarea[name=addcomments]").val(),
            ticket_id: $("input[name=ticketsid]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addTicketsComments",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets_emp";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#edittickets [type=submit]").attr("disabled", false);
            }
        });
    });

    //Delete Tickets
    $(document).on("click", ".delete_data", function () {
        var tickets_id = $(this).attr("id");
        $(".delete_ticket_button").attr("id", tickets_id);
        $("#delete_ticket").modal("show");
    });

    $(document).on("click", ".delete_ticket_button", function () {
        var tickets_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteTickets",
            method: "POST",
            data: {
                tickets_id: tickets_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>Tickets_emp";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_ticket_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    //Add Tickets Validation
    $(document).ready(function () {
        $("#addTicketbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addtickettype").val() == null) {
                $("#addtickettype").focus();
                $("#addtickettypevalidate").text("Please select a ticket type.");
                errorCount++;
            } else {
                $("#addtickettypevalidate").text("");
            }
            if ($("#addpriority").val() == null) {
                $("#addpriorityvalidate").text("Please select a priority type.");
                errorCount++;
            } else {
                $("#addpriorityvalidate").text("");
            }
            if ($("#addtitle").val().trim() == "") {
                $("#addtitlevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addtitlevalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });



    //Edit Tickets Validation
    $(document).ready(function () {
        $("#editTicketbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addcomments").val().trim() == "") {
                $("#addcomments").focus();
                $("#editsubjectvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editsubjectvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Total Tickets
    $.ajax({
        url: base_url + "viewTicketsTotal",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totaltickets").html(response.data[0].id);
            var number = response.data[0].id;
            var percentToGet = 100;
            var percent = (percentToGet / 100) * number;
            $("#totalpercent").html(percent);
        },
    });

    //Total Attendence
    $.ajax({
        url: base_url + "viewTotalLowPriority",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totaLowpercent").html(response.data[0].lowPriority);
            //var number = response.data[0].lowPriority;
            //var percentToGet = 100;
           // var percent = (percentToGet / 100) * number;
            //$("#totaLowpercent").html(percent);
        },
    });

    //Total Leavs
    $.ajax({
        url: base_url + "viewTotalHighTicket",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
          
            $("#totalHighpercent").html(response.data[0].highPriority);
        },
    });

    //Total Payroll
    $.ajax({
        url: base_url + "viewTotalmediumTicket",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totalMediumtickets").html(response.data[0].mediumPriority);
           
        },
    });
</script>

<script>
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#addtickettypename1").val("");
            $("textarea#addtickettypedescription1").val("");
            $("span#addticketypevalidate1").prop("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addtickettypevalidate1").text("");
            jQuery("#edittickettypevalidate1").text("");
            jQuery("#addtickettypedescriptionvalidate1").text("");
            jQuery("#edittickettypedescriptionvalidate1").text("");
        });
    });

    // View ticket Type
    $.ajax({
        url: base_url + "viewTicketsType",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        //var j = 1;
        var table = document.getElementById("tickettypetable1");
        for (i in response.data) {
            var tr = document.createElement("tr");
            tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
                '<td class="text-center">' +
                response.data[i].id +
                "</td>" +
                '<td class="text-center" style="white-space: break-spaces;">' +
                response.data[i].type_name +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].description +
                "</td>" +
                '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_tickettype" aria-expanded="false" id="' +
                response.data[i].id +
                '" class="btn btn-info edit_data" title="Edit Ticket Type" ><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_tickettype" aria-expanded="false" id="' +
                response.data[i].id +
                '"  class="btn btn-danger delete_data1" title="Delete Ticket Type"><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr);
        }
        $("#tickettype1").DataTable();
    });


    // Add Ticket Type
    $("#addtickettype1").submit(function (e) {
        var formData = {
            type_name: $("input[name=addtickettypename1]").val(),
            description: $("textarea[name=addtickettypedescription1]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addTicketsType",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets_emp";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addtickettype1 [type=submit]").attr("disabled", false);
            }
        });
    });

    // Edit  Ticket Type  Form Fill
    $(document).on("click", ".edit_data", function () {
        var type_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewTicketsType",
            method: "POST",
            data: {
                type_id: type_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#edittickettypename1").val(response["data"][0]["type_name"]);
                $("#edittickettypeid1").val(response["data"][0]["id"]);
                $("#edittickettypedescription1").val(response["data"][0]["description"]);
                $("#edit_tickettype").modal("show");
            },
        });
    });

    // Edit Ticket Type
    $("#edittickettype1").submit(function (e) {
        var formData = {
            type_id: $("input[name=edittickettypeid1]").val(),
            type_name: $("input[name=edittickettypename1]").val(),
            description: $("input[name=edittickettypedescription1]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editTicketsType",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets_emp";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#edittickettype1 [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Ticket Type
    $(document).on("click", ".delete_data1", function () {
        var type_id = $(this).attr("id");
        $(".delete_tickettype_button").attr("id", type_id);
        $("#delete_tickettype").modal("show");
    });

    $(document).on("click", ".delete_tickettype_button", function () {
        var type_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteTicketsType",
            method: "POST",
            data: {
                type_id: type_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>Tickets_emp";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_tickettype_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    // Add ticket Type  validation form
    $(document).ready(function () {
        $("#addtickettypebutton1").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addtickettypename1").val().trim() == "") {
                $("#addtickettypename1").focus();
                $("#addtickettypevalidate1").text("Please enter a valid ticket type.");
                errorCount++;
            } else {
                $("#addtickettypevalidate1").text("");
            }

            if ($("#addtickettypedescription1").val().trim() == "") {
                $("#addtickettypedescription1").focus();
                $("#addtickettypedescriptionvalidate1").text("Please enter a valid ticket description.");
                errorCount++;
            } else {
                $("#addtickettypedescriptionvalidate1").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Edit ticket Type validation form
    $(document).ready(function () {
        $("#edittickettypebutton1").click(function () {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#edittickettypename1").val().trim() == "") {
                $("#edittickettypename1").focus();
                $("#edittickettypevalidate1").text("Please enter a valid ticket type.");
                errorCount++;
            } else {
                $("#edittickettypevalidate1").text("");
            }

            if ($("#edittickettypedescription1").val().trim() == "") {
                $("#edittickettypedescription1").focus();
                $("#edittickettypedescriptionvalidate1").text("Please enter a valid ticket type.");
                errorCount++;
            } else {
                $("#edittickettypedescriptionvalidate1").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>