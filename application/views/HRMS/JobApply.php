<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mt-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>JobPublic">Jobs Public</a></li>
                <li class="breadcrumb-item active">Apply Job</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <form id="applyjob" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Applicant Name<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addapplicantnamevalidate" class="text-danger change-pos"></span>
                            <input type="hidden" name="jobstatus" id="jobstatus">
                            <input class="form-control name-valid" type="text" id="applicantname" name="applicantname" placeholder="Please enter applicant name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Mobile No<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addapplicantmobilevalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="tel" id="applicantmobile" name="applicantmobile" maxlength="10" onkeypress="return onlyNumberKey(event)" placeholder="Please enter mobile no." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Email Id<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addapplicantemailvalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="email" id="applicantemail" name="applicantemail" placeholder="Please enter email id." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Experience<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addapplicantexperiencevalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" min="0" id="applicantexperience" name="applicantexperience" placeholder="Please enter your experience e.g. 1 Year" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Applicant Address<span class="text-danger"> *</span></label>
                            <br />
                            <span id="applicantlocationvalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" min="0" id="applicantlocation" name="applicantlocation" placeholder="Please enter your current address" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Current Designation<span class="text-danger"> *</span></label>
                            <br />
                            <span id="applicantdesignationvalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" min="0" id="applicantdesignation" name="applicantdesignation" placeholder="Please enter your current designation" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Current Company<span class="text-danger"> *</span></label>
                            <br />
                            <span id="applicantccompanyvalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" min="0" id="applicantccompany" name="applicantccompany" placeholder="Please enter your current company" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Education Type<span class="text-danger"> *</span></label><br>
                            <span id="applicanteducationtypevalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="applicanteducationtype" id="applicanteducationtype" />
                                
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Education<span class="text-danger"> *</span></label>
                            <br />
                            <span id="applicanteducationvalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" id="applicantiondegree" name="applicantiondegree" placeholder="Please enter Education." autocomplete="off" />
                           
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>College<span class="text-danger"> *</span></label>
                            <br />
                            <span id="applicantcollegevalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" id="applicantcollege" name="applicantcollege" placeholder="Please enter college." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Skills<span class="text-danger"> *</span></label>
                            <br />
                            <span id="applicantskillsvalidate" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" id="applicantskills" name="applicantskills" placeholder="Please enter your skills" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Resume<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addfilevalidate" class="text-danger change-pos"></span>
                            <input type="file" name="file" id="file" class="dropify form-control" onchange="load_file();" data-allowed-file-extensions="pdf doc" data-max-file-size="2MB" autocomplete="off" />                      
                            <input type="hidden" id="12arrdata" />
                        </div>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="addjobpost" class="btn btn-success submit-btn">Apply Now</button>
                        <button type="reset" id="test" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    //for remove validation and empty input field
        $(document).ready(function() {
            $('#test').click(function(){
                jQuery('#addapplicantnamevalidate').text('');
                jQuery('#addapplicantexperiencevalidate').text('');
                jQuery('#applicantlocationvalidate').text('');
                jQuery('#addapplicantemailvalidate').text('');
                jQuery('#addapplicantmobilevalidate').text('');
                jQuery('#applicantdesignationvalidate').text('');
                jQuery('#applicanteducationvalidate').text('');
                jQuery('#applicantskillsvalidate').text('');
                jQuery('#applicantccompanyvalidate').text('');
                jQuery('#addfilevalidate').text('');
            });
        });


        function load_file() {
            if (!window.FileReader) {
                return alert("FileReader API is not supported by your browser.");
            }
            var $i = $("#file"), 
                input = $i[0]; 
            if (input.files && input.files[0]) {
                file = input.files[0]; 
                fr = new FileReader(); 
                fr.onload = function () {
                    $("#12arrdata").val(fr.result);
                };
                fr.readAsDataURL(file);
            } else {
                // Handle errors here
                alert("File not selected or browser incompatible.");
            }
        }
    
    // Select Education Type by api*/
        $.ajax({
            url: base_url + "viewJobEducation",
            data: {},
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#applicanteducationtype");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Education Type</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.type).text(entry.type));
            });
        });

    // Show Job list
       var job_id = <?php echo $jobId; ?>;
       $.ajax({
            url: base_url+"viewJobPublic",
            data: {job_id: job_id },
            type: "POST",
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
       })
       .done(function(response)
        {            
            $("#jobstatus").val(response.data[0].job_status);
            localStorage.removeItem('jobId');
        });

    //Job Apply form
        $("#applyjob").submit(function(e) {
            var job_id = <?php echo $jobId; ?>;
            var formData = {
                'job_id'                    :  job_id,
                'first_name'                :  $('input[name=applicantname]').val(),
                'job_applicant_status_id'   :  $('input[name=jobstatus]').val(),
                'experience'                :  $('input[name=applicantexperience]').val(),
                'current_location'          :  $('input[name=applicantlocation]').val(),
                'email'                     :  $('input[name=applicantemail]').val(),
                'phone'                     :  $('input[name=applicantmobile]').val(),
                'current_designation'       :  $('input[name=applicantdesignation]').val(),
                'hieghest_education'        :  $('select[name=applicanteducationtype]').val(),
                'education'                 :  $('input[name=applicantiondegree]').val(),
                'skills'                    :  $('input[name=applicantskills]').val(),
                'current_company'           :  $('input[name=applicantccompany]').val(),
                'resume'                    :  $('#12arrdata').val()
            };

            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: base_url + 'addJobApplicant',
                data: formData,
                dataType: 'json',
                encode: true,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
           .done(function(response) {
               var job_id = <?php echo $jobId; ?>;
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                    setTimeout(function(){window.location ="<?php echo base_url(); ?>JobApplyStatus"},1000);
                } 
                else {
                    toastr.error(falsedata);
                    $('#applyjob [type=submit]').attr('disabled',false);
                }
           });
       });


    //addJob validation form
        $(document).ready(function () {
            $("#addjobpost").click(function (e) {
                e.stopPropagation();
                var errorCount = 0;
                if ($("#applicantname").val().trim() == "") {
                    $("#applicantname").focus();
                    $("#addapplicantnamevalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#addapplicantnamevalidate").text("");
                }

                if ($("#applicantmobile").val().trim() == "") {
                    $("#applicantmobile").focus();
                    $("#addapplicantmobilevalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#addapplicantmobilevalidate").text("");
                }

                if ($("#applicantemail").val().trim() == "") {
                    $("#applicantemail").focus();
                    $("#addapplicantemailvalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#addapplicantemailvalidate").text("");
                }

                if ($("#applicantexperience").val().trim() == "") {
                    $("#applicantexperience").focus();
                    $("#addapplicantexperiencevalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#addapplicantexperiencevalidate").text("");
                }

                if ($("#applicantlocation").val().trim() == "") {
                    $("#applicantlocation").focus();
                    $("#applicantlocationvalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#applicantlocationvalidate").text("");
                }

                if ($("#applicantdesignation").val().trim() == "") {
                    $("#applicantdesignation").focus();
                    $("#applicantdesignationvalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#applicantdesignationvalidate").text("");
                }

                if ($("#applicantccompany").val().trim() == "") {
                    $("#applicantccompany").focus();
                    $("#applicantccompanyvalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#applicantccompanyvalidate").text("");
                }

                if ($("#applicanteducationtype").val() == null) {
                    $("#applicanteducationtype").focus();
                    $("#applicanteducationtypevalidate").text("Please select a eductaion type.");
                    errorCount++;
                } else {
                    $("#applicanteducationtypevalidate").text("");
                }

                if ($("#applicantiondegree").val().trim() == "") {
                    $("#applicantiondegree").focus();
                    $("#applicanteducationvalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#applicanteducationvalidate").text("");
                }

                if ($("#applicantcollege").val().trim() == "") {
                    $("#applicantcollege").focus();
                    $("#applicantcollegevalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#applicantcollegevalidate").text("");
                }

                if ($("#applicantskills").val().trim() == "") {
                    $("#applicantskills").focus();
                    $("#applicantskillsvalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#applicantskillsvalidate").text("");
                }

                if ($("#file").val().trim() == "") {
                    $("#file").focus();
                    $("#addfilevalidate").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#addfilevalidate").text("");
                }

                if (errorCount > 0) {
                    return false;
                }
            });
        });

</script>