<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mt-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">View Reimbursement</li>
            </ul>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="groups-tab" data-toggle="tab" href="#travelling-details" role="tab" aria-controls="travelling-details" aria-selected="true"><i class="fa fa-route"></i>Travelling Details </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " id="users-tab" data-toggle="tab" href="#allowance-details" role="tab" aria-controls="allowance-details" aria-selected="false"><i class="fa fa-hotel"></i>Allowance Details </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="groups-tab" data-toggle="tab" href="#expense-details" role="tab" aria-controls="expense-details" aria-selected="false"><i class="fa fa-expand-arrows-alt"></i>Expense Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="groups-tab" data-toggle="tab" href="#other-details" role="tab" aria-controls="other-details" aria-selected="false"><i class="fa fa-file-invoice"></i>Other Details</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="travelling-details" role="tabpanel" aria-labelledby="travelling-details">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Travelling Details</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-xl table-responsive-md" id="tourtable">
                            <thead>
                                <tr>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End Date</th>
                                    <th class="text-center">Start Time</th>
                                    <th class="text-center">End Time</th>
                                    <th class="text-center">Mode Of Journey</th>
                                    <th class="text-center">Employee Amount</th>
                                    <th class="text-center">Admin Amount</th>
                                    <th class="text-center">File</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodytour"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="allowance-details" role="tabpanel" aria-labelledby="allowance-details">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Allowance Details </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-xl table-responsive-md" id="travellingtable">
                            <thead>
                                <tr>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End Date</th>
                                    <th class="text-center">Hotel Name</th>
                                    <th class="text-center">Employee Amount</th>
                                    <th class="text-center">Admin Amount</th>
                                    <th class="text-center">File</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodytravelling"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="expense-details" role="tabpanel" aria-labelledby="expense-details">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Expense Details</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xl table-responsive-md" id="foodtable">
                            <thead>
                                <tr>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">City</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Employee Amount</th>
                                    <th class="text-center">Admin Amount</th>
                                    <th class="text-center">File</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodyfood"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="other-details" role="tabpanel" aria-labelledby="other-details">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Other Details</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-md table-responsive-sm" id="othertable">
                            <thead>
                                <tr>
                                    <th class="text-center">Details</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Employee Amount</th>
                                    <th class="text-center">Admin Amount</th>
                                    <th class="text-center">Bill</th>
                                    <th class="text-center">File</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodyother"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Allowance Status Mpodal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    // view in table Allowance
        var reimbursement_id = <?php echo $reimbursementid; ?> ;
        $.ajax({
            url: base_url + "viewReimbursement",
            data:{reimbursement_id:reimbursement_id}, 
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        .done(function (response) {
            var table = document.getElementById("tablebodytravelling");
            for (i in response.data) {


                var departureDate = new Date(response.data[i].departure_date);
                var dd = String(departureDate.getDate()).padStart(2, '0');
                var mm = String(departureDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = departureDate.getFullYear();

                departureDate = dd + '-' + mm + '-' + yyyy;

                var arrivalDate = new Date(response.data[i].arrival_date);
                var dd = String(arrivalDate.getDate()).padStart(2, '0');
                var mm = String(arrivalDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = arrivalDate.getFullYear();

                arrivalDate = dd + '-' + mm + '-' + yyyy;

               
                var status = "";
                if (response.data[i].status == 0) {
                    status = 'Pending';
                }
                if (response.data[i].status == 1) {
                    status = 'Declined';
                }
                if (response.data[i].status == 2) {
                    status = 'Approved';
                }

                var status_color = "";
                if (response.data[i].status == 0) {
                    status_color = 'Blue';
                }
                if (response.data[i].status == 1) {
                    status_color = 'red';
                }
                if (response.data[i].status == 2) {
                    status_color = 'green';
                }
                  
                var departuredate = "";
                if(response.data[i].departure_date == '0000-00-00'){
                    departuredate = 'Nil';  
                }else{
                 var start = departureDate;
                 departuredate = start;
                }
                var arrivaldate = "";
                if(response.data[i].arrival_date == '0000-00-00'){
                    arrivaldate = 'Nil';  
                }else{
                 var start = arrivalDate;
                 arrivaldate = start;
                }
                var hotelname = "";
                if(response.data[i].hotel_name == ''){
                    hotelname = 'Nil';  
                }else{
                 var start = response.data[i].hotel_name;
                 hotelname = start;
                }
                var hotelamount = "";
                if(response.data[i].hotel_amount == ''){
                    hotelamount = 'Nil';  
                }else{
                 var start = response.data[i].hotel_amount;
                 hotelamount = start;
                }
                var remarkedamount = "";
                if(response.data[i].remarked_amount == null){
                    remarkedamount = 'Nil';  
                }else{
                 var start = response.data[i].remarked_amount;
                 remarkedamount = start;
                }
                var arrivabillldate = "";
                if(response.data[i].hotel_bill == 0){
                    arrivabillldate = '<a href="javascript:void(0)" class="btn btn-info"><strong>No File Uploaded</strong></a>';  
                }else{
                 var start = '<a href="'+ response.data[i].hotel_bill + '" class="btn btn-info" target="__blank"><i class="fa fa-download mr-2"></i><strong>Download</strong></a>';
                 arrivabillldate = start;
                }
                var arrivastatusdate = "";
                if(response.data[i].arrival_date == '0000-00-00'){
                    arrivastatusdate = 'Nil';  
                }else{
                 var start = status;
                 arrivastatusdate = start;
                }
               
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +
                    departuredate +
                    "</td>" +
                    '<td class="text-center">' +
                    arrivaldate +
                    "</td>" +
                    '<td class="text-center">' +
                    hotelname +
                    "</td>" +
                    '<td class="text-center">' +
                    hotelamount +
                    "</td>" +
                    '<td class="text-center">' +
                    remarkedamount +
                    "</td>" +
                    '<td class="text-center">'+ arrivabillldate +'</td>' +
                    '<td class="text-center" style="color:' + status_color + '">' + arrivastatusdate + '</td>';
                table.appendChild(tr);
            }
            $("#travellingtable").DataTable();
        });
 

	// view in table Travelling
        var reimbursement_id = <?php echo $reimbursementid; ?> ;
        $.ajax({
            url: base_url + "viewTourReimbursement",
            data:{reimbursement_id:reimbursement_id}, 
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        .done(function (response) {
            var table = document.getElementById("tablebodytour");
            for (i in response.data) {
               
                var status = "";
                if (response.data[i].status == 0) {
                    status = 'Pending';
                }
                if (response.data[i].status == 1) {
                    status = 'Declined';
                }
                if (response.data[i].status == 2) {
                    status = 'Approved';
                }

                var status_color = "";
                if (response.data[i].status == 0) {
                    status_color = 'Blue';
                }
                if (response.data[i].status == 1) {
                    status_color = 'red';
                }
                if (response.data[i].status == 2) {
                    status_color = 'green';
                }
                var startsdate = new Date(response.data[i].start_date);
                var dd = String(startsdate.getDate()).padStart(2, '0');
                var mm = String(startsdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = startsdate.getFullYear();

                startsdate = dd + '-' + mm + '-' + yyyy;
                var endsdate = new Date(response.data[i].end_date);
                var dd = String(endsdate.getDate()).padStart(2, '0');
                var mm = String(endsdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = endsdate.getFullYear();

                endsdate = dd + '-' + mm + '-' + yyyy;

                var startdate = "";
                if(response.data[i].start_date == '0000-00-00'){
                    startdate = 'Nil';  
                }else{
                 var start = startsdate;
                 startdate = start;
                }
                var enddate = "";
                if(response.data[i].end_date == '0000-00-00'){
                    enddate = 'Nil';  
                }else{
                 var start = endsdate;
                 enddate = start;
                }
                var starttime = "";
                if(response.data[i].start_time == '00:00:00'){
                    starttime = 'Nil';  
                }else{
                 var start = response.data[i].start_time;
                 starttime = start;
                }
                var endtime = "";
                if(response.data[i].end_time == '00:00:00'){
                    endtime = 'Nil';  
                }else{
                 var start = response.data[i].end_time;
                 endtime = start;
                }
                var modeofjourney = "";
                if(response.data[i].mode_of_journey == ""){
                    modeofjourney = 'Nil';  
                }else{
                 var start = response.data[i].mode_of_journey;
                 modeofjourney = start;
                }
                var amount = "";
                if(response.data[i].amount == ""){
                    amount = 'Nil';  
                }else{
                 var start = response.data[i].amount;
                 amount = start;
                }
                var fixedamount = "";
                if(response.data[i].fixed_amount == ''){
                    fixedamount = 'Nil';  
                }else{
                 var start = response.data[i].fixed_amount;
                 fixedamount = start;
                }
                var startimagedate = "";
                if(response.data[i].image == 0){
                    startimagedate = '<a href="javascript:void(0)" class="btn btn-info"><strong>No File Uploaded</strong></a>';  
                }else{
                 var start = '<a href="'+ response.data[i].image + '" class="btn btn-info"><i class="fa fa-download mr-2"></i><strong>Download</strong></a>';
                 startimagedate = start;
                }
                var startstatusdate = "";
                if(response.data[i].start_date == '0000-00-00'){
                    startstatusdate = 'Nil';  
                }else{
                 var start = status;
                 startstatusdate = start;
                }
               
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +
                    startdate +
                    "</td>" +
                    '<td class="text-center">' +
                    enddate +
                    "</td>" +
                    '<td class="text-center">' +
                    starttime +
                    "</td>" +
                    '<td class="text-center">' +
                    endtime +
                    "</td>" +
                    '<td class="text-center">' +
                    modeofjourney +
                    "</td>" +
                    '<td class="text-center">' +
                    amount +
                    "</td>" +
                    '<td class="text-center">' +
                    fixedamount +
                    "</td>" +
                    '<td class="text-center">'+ startimagedate +'</td>' +
                    '<td class="text-center"style="color:' + status_color + '">' + startstatusdate + '</td>';
                table.appendChild(tr);
            }
            $("#tourtable").DataTable();
        });
   
    
    // view in table Expence
        var reimbursement_id = <?php echo $reimbursementid; ?> ;
        $.ajax({
            url: base_url + "viewFoodReimbursement",
            data:{reimbursement_id:reimbursement_id}, 
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        .done(function (response) {
            var table = document.getElementById("tablebodyfood");
            for (i in response.data) {
               

                var status = "";
                if (response.data[i].status == 0) {
                    status = 'Pending';
                }
                if (response.data[i].status == 1) {
                    status = 'Declined';
                }
                if (response.data[i].status == 2) {
                    status = 'Approved';
                }

                var status_color = "";
                if (response.data[i].status == 0) {
                    status_color = 'Blue';
                }
                if (response.data[i].status == 1) {
                    status_color = 'red';
                }
                if (response.data[i].status == 2) {
                    status_color = 'green';
                }
                 var foodname ="";
                 if(response.data[i].food_name == "")
                 {
                    foodname = 'Nil';
                 }else{
                   var food = response.data[i].food_name;
                   foodname = food;
                 }
                 var cityname ="";
                 if(response.data[i].city == "")
                 {
                    cityname = 'Nil';
                 }else{
                   var food = response.data[i].city;
                   cityname = food;
                 }
                 var foodamount ="";
                 if(response.data[i].food_amount == "")
                 {
                    foodamount = 'Nil';
                 }else{
                   var food = response.data[i].food_amount;
                   foodamount = food;
                 }
                 var clearamount ="";
                 if(response.data[i].clear_amount == null)
                 {
                    clearamount = 'Nil';
                 }else{
                   var food = response.data[i].clear_amount;
                   clearamount = food;
                 }
                 var foodsdate = new Date(response.data[i].food_date);
                var dd = String(foodsdate.getDate()).padStart(2, '0');
                var mm = String(foodsdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = foodsdate.getFullYear();

                foodsdate = dd + '-' + mm + '-' + yyyy;
                 var fooddate ="";
                 if(response.data[i].food_date == '0000-00-00')
                 {
                    fooddate = 'Nil';
                 }else{
                   var food = foodsdate;
                   fooddate = food;
                 }
                 var fooddatebill ="";
                 if(response.data[i].food_bill == 0)
                 {
                    fooddatebill = '<a href="javascript:void(0)" class="btn btn-info"><strong>No File Uploaded</strong></a>';
                 }else{
                   var food = '<a href="'+ response.data[i].food_bill + '" class="btn btn-info"><i class="fa fa-download mr-2"></i><strong>Download</strong></a';
                   fooddatebill = food;
                 }
                 var fooddatestatus ="";
                 if(response.data[i].food_date == '0000-00-00')
                 {
                    fooddatestatus = 'Nil';
                 }else{
                   var food = status;
                   fooddatestatus = food;
                 }
                
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +
                    foodname+
                    "</td>" +
                    '<td class="text-center">' +
                    cityname +
                    "</td>" +
                    '<td class="text-center">' +
                    fooddate +
                    "</td>" +
                    '<td class="text-center">' +
                    foodamount +
                    "</td>" +
                    '<td class="text-center">' +
                    clearamount +
                    "</td>" +
                    '<td class="text-center">' + fooddatebill + '</td>' +
                    '<td class="text-center"style="color:' + status_color + '">' + fooddatestatus + '</td>';
                table.appendChild(tr);
            }
            $("#foodtable").DataTable();
        });

   
   
        // view in table Other
	        var reimbursement_id = <?php echo $reimbursementid; ?> ;
	        $.ajax({
	            url: base_url + "viewOtherReimbursement",
	            data:{reimbursement_id:reimbursement_id}, 
	            type: "POST",
	            dataType: "json", 
	            encode: true,
	            beforeSend: function (xhr) {
	                xhr.setRequestHeader("Token", localStorage.token);
	            },
	        })

	        .done(function (response) {
	            var table = document.getElementById("tablebodyother");
	            for (i in response.data) {

	                var status = "";
	                if (response.data[i].status == 0) {
	                    status = 'Pending';
	                }
	                if (response.data[i].status == 1) {
	                    status = 'Declined';
	                }
	                if (response.data[i].status == 2) {
	                    status = 'Approved';
	                }

	                var status_color = "";
	                if (response.data[i].status == 0) {
	                    status_color = 'Blue';
	                }
	                if (response.data[i].status == 1) {
	                    status_color = 'red';
	                }
	                if (response.data[i].status == 2) {
	                    status_color = 'green';
	                }

                   var detail = "";
                   if (response.data[i].other_details == "") {
                       detail ='Nil';
                   }
                   else{
                       var testing = response.data[i].other_details;
                       detail = testing;
                   }
                   var employeeamount = "";
                   if (response.data[i].employee_amount == '') {
                    employeeamount ='Nil';
                   }
                   else{
                       var testing = response.data[i].employee_amount;
                       employeeamount = testing;
                   }
                   var adminamount = "";
                   if (response.data[i].admin_amount == null) {
                    adminamount ='Nil';
                   }
                   else{
                       var testing = response.data[i].admin_amount;
                       adminamount = testing;
                   }
                   var othersdate = new Date(response.data[i].other_date);
                var dd = String(othersdate.getDate()).padStart(2, '0');
                var mm = String(othersdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = othersdate.getFullYear();

                othersdate = dd + '-' + mm + '-' + yyyy;
                   var otherdate = "";
                   if (response.data[i].other_date == '0000-00-00') {
                    otherdate ='Nil';
                   }
                   else{
                       var testing = othersdate;
                       otherdate = testing;
                   }
                   var billname = "";
                   if (response.data[i].bill_name == "") {
                    billname ='Nil';
                   }
                   else{
                       var testing = response.data[i].bill_name;
                       billname = testing;
                   }
                   var otherdateimage = "";
                   if (response.data[i].other_image == 0) {
                    otherdateimage ='<a href="javascript:void(0)" class="btn btn-info"><strong>No File Uploaded</strong></a>';
                   }
                   else{
                       var testing = '<a href="'+ response.data[i].other_image + '" class="btn btn-info" ><i class="fa fa-download mr-2"></i><strong>Download</strong></a>';
                       otherdateimage = testing;
                   }
                   var otherdatestatus = "";
                   if (response.data[i].other_date == '0000-00-00') {
                    otherdatestatus ='Nil';
                   }
                   else{
                       var testing = status;
                       otherdatestatus = testing;
                   }
                  
	                var tr = document.createElement("tr");
	                tr.innerHTML =
	                    '<td class="text-center">' +
                        detail +
	                    "</td>" +
	                    '<td class="text-center">' +
                        otherdate +
	                    "</td>" +
	                    '<td class="text-center">' +
	                    employeeamount + 
	                    "</td>" +
	                    '<td class="text-center">' +
	                    adminamount +
	                    "</td>" +
	                    '<td class="text-center">' +
	                    billname +
	                    "</td>" +
	                    '<td class="text-center">'+ otherdateimage +'</td>' +
	                    '<td class="text-center"style="color:' + status_color + '">' + otherdatestatus + '</td>';
	                table.appendChild(tr);
	            }
	            $("#othertable").DataTable();
	        });
	  
</script>