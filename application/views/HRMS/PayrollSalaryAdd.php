<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>vily/Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>vily/PayrollSalary">Payroll Salary</a></li>
                <li class="breadcrumb-item active">Add Payroll Salary</li>
            </ul>
        </div>

        <div class="card mt-3">
            <form id="addPayrolls" action="#" class="form-groups" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                <div class="card-header mb-3">
                    <h3 class="card-title"><strong>Monthly Wise </strong></h3>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="card-header">
                            <h3 class="card-title"><strong>Basic</strong></h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label>Basic Salary<span class="text-danger"> *</span></label><br>
                                <span id="basicsalaryvalid" class="text-danger change-pos"></span>
                                <input type="text" name="basicsalary" id="basicsalary" class="form-control" placeholder="Please enter basic salary." value="0" readonly="">
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label>DA<span class="text-danger"> </span></label>
                                <input type="text" name="da" id="da" class="form-control" placeholder="Please enter da." value="0">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="card-header">
                            <h3 class="card-title"><strong>HRA</strong></h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label>HRA<span class="text-danger"> *</span></label><br>
                                <span id="hravalid" class="text-danger change-pos"></span>
                                <input type="text" name="hra" id="hra" class="form-control" placeholder="Please enter hra." value="0">
                                <p class="font-12">10% to 40% of basic salary</p>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label>HRA Amount</label>
                                <input type="text" name="hraamount" id="hraamount" class="form-control" placeholder="Please enter hra amount." value="0" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="card-header">
                            <h3 class="card-title"><strong>Medical</strong></h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label>Medical <span class="text-danger"> *</span></label><br>
                                <span id="medicalvalid" class="text-danger change-pos"></span>
                                <input type="text" name="medical" id="medical" class="form-control" placeholder="Please enter medical." value="0">
                                <p class="font-12">10% to 40% of basic salary</p>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label>Medical Amount</label>
                                <input type="text" name="medicalamount" id="medicalamount" class="form-control" placeholder="Please enter medical amount." value="0" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="card-header">
                            <h3 class="card-title"><strong>Conveyance</strong></h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label>Conveyance <span class="text-danger"> *</span></label><br>
                                <span id="conveyancevalid" class="text-danger change-pos"></span>
                                <input type="text" name="conveyance" id="conveyance" class="form-control" placeholder="Please enter conveyance." value="0">
                                <p class="font-12">10% to 40% of basic salary</p>
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label>Conveyance Amount</label>
                                <input type="text" name="conveyanceamount" id="conveyanceamount" class="form-control" placeholder="Please enter conveyance amount." value="0" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="card-header">
                            <h3 class="card-title"><strong>Special Allowance</strong></h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label>Special Allowance <span class="text-danger"> *</span></label><br>
                                <span id="specialallowancevalid" class="text-danger change-pos"></span>
                                <input type="text" name="specialallowance" id="specialallowance" class="form-control" placeholder="Please enter special allowance." value="0">
                                <p class="font-12">10% to 40% of basic salary</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card-header">
                            <h3 class="card-title"><strong>Basic</strong></h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>LTA <span class="text-danger"> *</span></label><br>
                                <span id="ltavalid" class="text-danger change-pos"></span>
                                <input type="text" name="lta" id="lta" class="form-control" placeholder="Please enter lta." value="0">
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>House Allowance<span class="text-danger"> *</span></label><br>
                                <span id="housevalid" class="text-danger change-pos"></span>
                                <input type="text" name="house" id="house" class="form-control" placeholder="Please enter house allowance." value="0">
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Company Bonus <span class="text-danger"> *</span></label><br>
                                <span id="bonusvalid" class="text-danger change-pos"></span>
                                <input type="text" name="bonus" id="bonus" class="form-control" placeholder="Please enter company bonus." value="0">
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Retiral Benefits<span class="text-danger"> *</span></label><br>
                                <span id="retiralvalid" class="text-danger change-pos"></span>
                                <input type="text" name="retiral" id="retiral" class="form-control" placeholder="Please enter retiral benefits." value="0">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="card-header">
                            <h3 class="card-title"><strong>Deduction</strong></h3>
                        </div>
                        <div class="card-body row">
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>PF <span class="text-danger"> *</span></label><br>
                                <span id="pfvalid" class="text-danger change-pos"></span>
                                <input type="text" name="pf" id="pf" class="form-control" placeholder="Please enter pf." value="0">
                                <p class="font-12">10% to 40% of basic salary</p>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>EPF <span class="text-danger"> *</span></label><br>
                                <span id="epfvalid" class="text-danger change-pos"></span>
                                <input type="text" name="epf" id="epf" class="form-control" placeholder="Please enter epf." value="0">
                                <p class="font-12">10% to 40% of basic salary</p>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>ESI <span class="text-danger"> *</span></label><br>
                                <span id="esivalid" class="text-danger change-pos"></span>
                                <input type="text" name="esi" id="esi" class="form-control" placeholder="Please enter esi." value="0">
                                <p class="font-12">10% to 40% of basic salary</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card-body row">
                            <div class="form-group col-lg-12 text-right">
                                <button id="addpayrollbutton" class="btn btn-success submit-btn">Submit</button> 
                                <button type="reset" id="test" class="btn btn-secondary">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#basicsalaryvalid").text("");
            jQuery("#hravalid").text("");
            jQuery("#medicalvalid").text("");
            jQuery("#conveyancevalid").text("");
            jQuery("#specialallowancevalid").text("");
            jQuery("#ltavalid").text("");
            jQuery("#housevalid").text("");
            jQuery("#bonusvalid").text("");
            jQuery("#retiralvalid").text("");
            jQuery("#pfvalid").text("");
            jQuery("#epfvalid").text("");
            jQuery("#esivalid").text("");
        });
    });
// Edit' Form Fill
var user_id = <?php echo $userId; ?>;
        
        $.ajax({
            url: base_url+"viewUserEdit",
            data: {user_id: user_id },
            type: "POST",
            dataType    : 'json',
            beforeSend: function(xhr){
                xhr.setRequestHeader('Token', localStorage.token);
            },

            success:function(response){
                $('#edituserid').val(response["data"][0]["id"]);
              
                if (!$.trim(response.data[0].user_image)) {
                $("#edituserimagedata").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
                  } else {
                $("#edituserimagedata").append('<img src="' + response.data[0].user_image + '" />');
                  }  
                $('#basicsalary').val(response["data"][0]["salary"]);
                $('#adduserId').val(response["data"][0]["id"]);
                localStorage.removeItem('$userid');
            }
        });
    //Add Payroll Validation
    $(document).ready(function () {
        $("#addpayrollbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#basicsalary").val().trim() == "") {
                $("#basicsalary").focus();
                $("#basicsalaryvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#basicsalaryvalid").text("");
            }
            if ($("#hra").val().trim() == "") {
                $("#hra").focus();
                $("#hravalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#hravalid").text("");
            }
            if ($("#medical").val().trim() == "") {
                $("#medical").focus();
                $("#medicalvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#medicalvalid").text("");
            }
            if ($("#conveyance").val().trim() == "") {
                $("#conveyance").focus();
                $("#conveyancevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#conveyancevalid").text("");
            }
            if ($("#specialallowance").val().trim() == "") {
                $("#specialallowance").focus();
                $("#specialallowancevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#specialallowancevalid").text("");
            }

            if ($("#lta").val().trim() == "") {
                $("#lta").focus();
                $("#ltavalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#ltavalid").text("");
            }
            if ($("#house").val().trim() == "") {
                $("#house").focus();
                $("#housevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#housevalid").text("");
            }
            if ($("#bonus").val().trim() == "") {
                $("#bonus").focus();
                $("#bonusvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#bonusvalid").text("");
            }
            if ($("#retiral").val().trim() == "") {
                $("#retiral").focus();
                $("#retiralvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#retiralvalid").text("");
            }
            if ($("#pf").val().trim() == "") {
                $("#pf").focus();
                $("#pfvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#pfvalid").text("");
            }
            if ($("#epf").val().trim() == "") {
                $("#epf").focus();
                $("#epfvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#epfvalid").text("");
            }
            if ($("#esi").val().trim() == "") {
                $("#esi").focus();
                $("#esivalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#esivalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
