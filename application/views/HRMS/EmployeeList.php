<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item">
                    <a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                    <li class="breadcrumb-item active">Employee List</li>
                </ul>
                <div class="header-action mt-3">
                    <a href="<?php echo base_url(); ?>EmployeeAdd_emp" class="btn btn-primary">
                        <i class="fe fe-plus mr-2"></i>Add Employee
                    </a>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-value float-right">
                                <i class="fa fa-users"></i>
                            </div>
                            <h3 class="mb-1" id="totalemployee"></h3>
                            <div>Total Employee</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-value float-right">
                                <i class="fa fa-user"></i>
                            </div>
                            <h3 class="mb-1" id="totalnewemployee"></h3>
                            <div>New Employee</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-value float-right">
                                <i class="fa fa-male"></i>
                            </div>
                            <h3 class="mb-1" id="totalMale"></h3>
                            <div>Male</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-value float-right">
                                <i class="fa fa-female"></i>
                            </div>
                            <h3 class="mb-1" id="totalFemale"></h3>
                            <div>Female</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-status bg-orange"></div>
                <div class="card-header">
                    <h3 class="card-title"><strong>Search Employee's</strong></h3>
                </div>
                <div class="card-body">
                    <form id="filter-form" method="POST" action="#">
                        <div class="row">
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Branch <span class="text-danger">*</span></label><br>
                                <span id="addbranchvalidate" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="addbranchid" id="addbranchid" /></select>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Department<span class="text-danger">*</span></label><br>
                                <span id="adddepartmentidvalidate" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/></select>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Designation<span class="text-danger">*</span></label><br>
                                <span id="designationidvalidate" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="designation_id" id="designation_id" /></select>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                                <button id="filteremployeebutton" class="btn btn-success submit-btn">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Search Employee List Result Table -->
            <div class="card mt-3">
                <div class="card-status bg-orange"></div>
                <div class="card-header">
                    <h3 class="card-title"><strong>Employee's List</strong></h3>
                     <div class="card-options">
                        <a href="<?php echo base_url(); ?>Employee_emp" class="btn btn-info grid-system">
                            <i class="fe fe-grid"></i>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl" id="employeetablebody">
                        <thead>
                            <tr>
                                <th class="text-left">Employee Name</th>
                                <th class="text-center">Mobile</th>
                                <th class="text-center">Branch</th>
                                <th class="text-center">Department</th>
                                <th class="text-center">Designation</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody id="employeedatatable"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript">
    //Show Employee
    $.ajax({
        url: base_url + "totalEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            var test = response.data[0].id; // alert(test);
            $("#totalemployee").html(response.data[0].id);
        },
    }); 

    //Show New Employee In A Month
    $.ajax({
        url: base_url + "countAllNewEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalnewemployee").html(response.data[0].NewEmployee);
        },
    }); 
    
    //total Male Employee
    $.ajax({
        url: base_url + "totalMaleEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalMale").html(response.data[0].total_male);
        },
    }); 

    //total  Female Employee
    $.ajax({
        url: base_url + "totalFemaleEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalFemale").html(response.data[0].total_Female);
        },
    }); 

    // Select branch by api*/
    $.ajax({
        url: base_url + "viewTokenBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addbranchid");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0); // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select department by api*/
    $("#addbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartmentEmployee",
            data: {
                branch_id: $("select[name=addbranchid]").val(),
            },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#adddepartmentid");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0); // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid").change(function () {
        $.ajax({
            url: base_url + "viewDesignationEmployee",
            data: {
                department_id: $("select[name=adddepartmentid]").val(),
            },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#designation_id");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0); // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });
    
    /*-----------------------------Inactive Button------------------------------------*/
    function getstatus(id) {
        var users_id = id;
        var status = $("#" + id).val();
        console.log(status);
        //alert(status);
        if (status == 0) {
            var status = 1;
            $.ajax({
            type: "POST",
            url: base_url + "deactivateUser",
            data: "users_id=" + users_id + "&status=" + status,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            })
            .done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];
           
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>EmployeeList_emp";
                }, 1000);
            });
        }
        else {
            var status = 0;
            $.ajax({
                type: "POST",
                url: base_url + "deactivateUser",
                data: "users_id=" + users_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>EmployeeList_emp";
                }, 1000);
            })
         
        }
    }
    
    

    $(function () {
        filterEmployeeDash('all');
        $("#filter-form").submit();
    });
    function filterEmployeeDash() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            designation_id = $("#designation_id").val();
            if (designation_id == null) {
                designation_id = 'all';
            }
            req = {};
            req.designation_id = designation_id; //Show Employee In table
            $.ajax({
                url: base_url + "viewUserDashboard",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                $("#employeetablebody").DataTable().clear().destroy();
                $("#employeetablebody tbody").empty();
                var table = document.getElementById("employeedatatable");
                for (i in response.data) {
                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                    var testcheck = "";
                    if (response.data[i].is_active == 1) {
                        testcheck = "checked";
                    }
                    if (response.data[i].is_active == 0) {
                        testcheck = "";
                    }
                    var tr = document.createElement("tr");
                    tr.innerHTML =
                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                        UserImage +
                        '" alt=""></span><div class="ml-3">  <a href="EmployeeView_emp/' +
                        response.data[i].user_code +
                        '" title="">' +
                        response.data[i].emp_id +
                        ' - ' +
                        response.data[i].first_name +
                        '</a> <p class="mb-0">' +
                        response.data[i].email +
                        "</p> </div> </div></td>" +
                        '<td class="text-center">' +
                        response.data[i].phone +
                        "</td>" +
                        '<td class="text-center">' +
                        response.data[i].branch_name +
                        "</td>" +
                        '<td class="text-center">' +
                        response.data[i].department_name +
                        "</td>" +
                        '<td class="text-center">' +
                        response.data[i].designation_name +
                        "</td>" +
                        '<td class="text-center"> <div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox" ' +
                        testcheck +
                        ' id="' +
                        response.data[i].id +
                        '" value="' +
                        response.data[i].is_active +
                        '" onclick="getstatus(' +
                        response.data[i].id +
                        ')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>' +
                        '<td class="text-center"><a href="EmployeeEdit_emp/' +
                        response.data[i].user_code +
                        '" class="btn btn-info" title="Edit Employee" id="' +
                        response.data[i].job_id +
                        '"><i class="fa fa-edit"></i></a> </td>';
                    table.appendChild(tr);
                }

                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#employeetablebody").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Employee Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Employee Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4 ]
                            }
                        }
                    ]
                });
            });
        });
    }

    //Add Employee Validation
    $(document).ready(function () {
        $("#filteremployeebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }
            if ($("#adddepartmentid").val() == null) {
                $("#adddepartmentid").focus();
                $("#adddepartmentidvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#adddepartmentidvalidate").text("");
            }
            if ($("#designation_id").val() == null) {
                $("#designation_id").focus();
                $("#designationidvalidate").text("Please select a designation name.");
                errorCount++;
            } else {
                $("#designationidvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
