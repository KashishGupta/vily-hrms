<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Jobs">Jobs</a></li>
				<li class="breadcrumb-item active">Add Job</li>
			</ul>
        </div>
        
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Job</strong></h3>
            </div>
            <div class="card-body">
                <form id="addjob" method="POST">
					<div class="row">
						<div class="form-group col-lg-3 col-md-6 col-sm-12">
							<label>Branch<span class="text-danger"> *</span></label><br>
							<span id="addbranchvalidate" class="text-danger change-pos"></span>
							<select class="custom-select form-control" name="addbranchid" id="addbranchid" />
							    
							</select>
						</div>
						<div class="form-group col-lg-3 col-md-6 col-sm-12">
							<label>Department<span class="text-danger"> *</span></label><br>
							<span id="adddepartmentvalidate" class="text-danger change-pos"></span>
							<select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/>
							    
							</select>
						</div>
						<div class="form-group col-lg-3 col-md-6 col-sm-12">
							<label>Job Title<span class="text-danger"> *</span></label><br>
							<span id="addjobtitlevalidate" class="text-danger change-pos"></span>
                            <input type="text" class="form-control" name="addjobtitle" id="addjobtitle" placeholder="Please enter job title." />
						</div>
						<div class="form-group col-lg-3 col-md-6 col-sm-12">
							<label>Job Type<span class="text-danger"> *</span></label><br>
							<span id="addjobtypevalidate" class="text-danger change-pos"></span>
							<select class="custom-select form-control" name="addjobtypeid" id="addjobtypeid" />
							    
							</select>
						</div>
                       <!-- <div class="form-group col-md-6 col-sm-12">
                            <label>Education Type<span class="text-danger"> *</span></label><br>
                            <span id="addeducationtypevalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addeducationtypeid" id="addeducationtypeid" />
                                
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Higher Degree<span class="text-danger"> *</span></label><br>
                            <span id="addhigherdegreevalidate" class="text-danger change-pos"></span>
                            <input type="text" name="addhigherdegree" id="addhigherdegree" class="form-control" placeholder="Please enter higher degree. ">
                        </div> -->
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Key Skills<span class="text-danger"> *</span></label><br>
                            <span id="addkeyskillsvalidate" class="text-danger change-pos"></span>
                            <input type="text" class="form-control" name="addkeyskills" id="addkeyskills" placeholder="Please enter key skills." />
                        </div>
						<div class="form-group col-lg-3 col-md-6 col-sm-12">
							<label>Job Active Date<span class="text-danger"> *</span></label><br>
                            <span id="addstartdatevalit" class="text-danger change-pos"></span>
							<input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="addstartdate" id="addstartdate" placeholder="YYYY/MM/DD" readonly="" autocomplete="off" />
						</div>
						<div class="form-group col-lg-3 col-md-6 col-sm-12">
							<label>Job Inactive Date<span class="text-danger"> *</span></label><br>
							<span id="addenddatevalidate" class="text-danger change-pos"></span>
							<input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="addenddate" id="addenddate" placeholder="YYYY/MM/DD" readonly=""   />
						</div>
						<div class="form-group col-lg-3 col-md-6 col-sm-12">
							<label>No. of Vacancies<span class="text-danger"> *</span></label><br>
							<span id="addnoofvacanciesvalidate" class="text-danger change-pos"></span>
							<input class="form-control" name="addnoofvacancies" id="addnoofvacancies" type="number" min="1" max="100" autocomplete="off" onkeypress="return onlyNumberKey(event)" />
						</div>
						<div class="form-group col-md-12 col-sm-12">
							<label>Description<span class="text-danger"> *</span></label><br>
							<span id="adddescriptionvalidate" class="text-danger change-pos"></span>
							<textarea class="form-control summernote" name="adddescription" id="adddescription" autocomplete="off"  /></textarea>
						</div>
                        <div class="form-group col-md-12 col-sm-12 text-right">
                            <button id="addjobbutton" class="btn btn-success submit-btn">Submit</button>
                            <button type="reset" id="test" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                        </div>
					</div>
				</form>
            </div>
        </div>
    </div>            
</div>  

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
//for remove validation and empty input field
$(document).ready(function () {
    $("#test").click(function () {
        jQuery("#addbranchvalidate").text("");
        jQuery("#adddepartmentvalidate").text("");
        jQuery("#addjobtitlevalidate").text("");
        jQuery("#addjobtypevalidate").text("");
        jQuery("#addkeyskillsvalidate").text("");
        jQuery("#addstartdatevalit").text("");
        jQuery("#addenddatevalidate").text("");
        jQuery("#addenddatevalidate").text("");
        jQuery("#addnoofvacanciesvalidate").text("");
        jQuery("#adddescriptionvalidate").text("");
    });
});

// Select branch by api*/
$.ajax({
    url: base_url + "viewTokenBranch",
    data: {},
    type: "POST",
    dataType: "json", // what type of data do we expect back from the server
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    let dropdown = $("#addbranchid");

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose Branch</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
    });
});

// Select branch by api*/
$.ajax({
    url: base_url + "viewJobEducation",
    data: {},
    type: "POST",
    dataType: "json", // what type of data do we expect back from the server
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    let dropdown = $("#addeducationtypeid");

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose Education Type</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.type).text(entry.type));
    });
});
// Select department by api*/
$("#addbranchid").change(function () {
    $.ajax({
        url: base_url + "viewDepartmentEmployee",
        data: { branch_id: $("select[name=addbranchid]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#adddepartmentid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });
});


// Select Job Type API */
$.ajax({
    url: base_url + "viewactiveJobType",
    data: {},
    type: "POST",
    dataType: "json", // what type of data do we expect back from the server
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    let dropdown = $("#addjobtypeid");

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose Job Type</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.id).text(entry.job_type));
    });
});

// Add Job
$("#addjob").submit(function (e) {
    var formData = {
        department_id: $("select[name=adddepartmentid]").val(),
        job_type_id: $("select[name=addjobtypeid]").val(),
        start_date: $("input[name=addstartdate]").val(),
        end_date: $("input[name=addenddate]").val(),
        end_date: $("input[name=addenddate]").val(),
        no_of_post: $("input[name=addnoofvacancies]").val(),
        job_description: $("textarea[name=adddescription]").val(),
        key_skill: $("input[name=addkeyskills]").val(),
        job_title: $("input[name=addjobtitle]").val(),
    };

    e.preventDefault();
    $.ajax({
        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
        url: base_url + "addJob", // the url where we want to POST
        data: formData, // our data object
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
        // using the done promise callback
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>Jobs";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addjob [type=submit]").attr("disabled", false);
            }
        });
});

//Add Job Validation
$(document).ready(function () {
    $("#addjobbutton").click(function (e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#addbranchid").val() == null) {
            $("#addbranchid").focus();
            $("#addbranchvalidate").text("Please select a branch.");
            errorCount++;
        } else {
            $("#addbranchvalidate").text("");
        }
        if ($("#adddepartmentid").val() == null) {
            $("#adddepartmentid").focus();
            $("#adddepartmentvalidate").text("Please select a department.");
            errorCount++;
        } else {
            $("#adddepartmentvalidate").text("");
        }
        if ($("#addjobtitle").val().trim() == "") {
            $("#addjobtitle").focus();
            $("#addjobtitlevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addjobtitlevalidate").text("");
        }
        if ($("#addjobtypeid").val() == null) {
            $("#addjobtypeid").focus();
            $("#addjobtypevalidate").text("Please select a job type.");
            errorCount++;
        } else {
            $("#addjobtypevalidate").text("");
        }

        if ($("#addnoofvacancies").val().trim() == "") {
            $("#addnoofvacanciesvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addnoofvacanciesvalidate").text("");
        }

        if ($("#addkeyskills").val().trim() == "") {
            $("#addkeyskillsvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addkeyskillsvalidate").text("");
        }
        if ($("#adddescription").val().trim() == "") {
            $("#adddescriptionvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#adddescriptionvalidate").text("");
        }
        if ($("#addstartdate").val().trim() == "") {
            $("#addstartdatevalit").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addstartdatevalit").text("");
        }
        if ($("#addenddate").val().trim() == "") {
            $("#addenddatevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addenddatevalidate").text("");
        }

        if (errorCount > 0) {
            return false;
        }
    });
});

</script>
