<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>offBoardingList">Off Boarding</a></li>
                <li class="breadcrumb-item active">Add Document</li>
            </ul>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h5 class="modal-title">Select Document To Add</h5>
            </div>
            <div class="card-body">
                <div class="sc-kfGgVZ ceDrlv">
                    <div class="sc-eXEjpC jSRNhT" data-toggle="modal" data-target="#add_Offer">
                        <div class="sc-kPVwWT kQKRxC">
                            <div class="sc-dnqmqq nucGq">
                                <svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc">
                                    <path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path>
                                    <path
                                        d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z"
                                        fill="#dadcdf"
                                    ></path>
                                    <path
                                        d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z"
                                        fill="#dadcdf"
                                    ></path>
                                </svg>
                                <div>
                                    <strong>OffBoarding Letter / Resign Letter</strong>
                                    <div class="sc-gzVnrw hywHFG">Send Resign letter.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                   
                    

<!-- Add OffBoarding letter/Agreement Modal -->
<div id="add_Offer" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Offboarding Letter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addofferletter" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-sm-12">
                            <label>Letter Name <span class="text-danger"> *</span></label><br>
                            <span class="text-danger change-pos" id="offerdocumentnamevalid"></span>
                            <input type="text" id="offboardingdescription" name="offboardingdescription" class="form-control" placeholder="Please letter file name." />
                        </div>
                        <div class="form-group col-sm-12">
                            <label>File <span class="text-danger"> *</span></label><br>
                            <span class="text-danger change-pos" id="resumefilevalid"></span>
                            <input type="hidden" id="addoffboardinguserid" name="addoffboardinguserid" />
                            <input type="file" id="resumefile" class="form-control" onchange="loading_file();" />
                            <input type="hidden" id="23arrdata" />
                        </div>

                        <div class="col-lg-12 mt-3 text-right">
                            <button id="addofferletterbutton" class="btn btn-primary submit-btn">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- /Add Personal Documents Modal -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //for remove validation and empty input field
        $(document).ready(function () {
            $(".modal").click(function () {
                $("input#offerdocumentname").val("");
                $("input#resumefile").val("");

                $("input#addexperiencedocumentname").val("");
                $("input#experirencefile").val("");

                $("input#offerdocumentname").val("");
                $("input#resumefile").val("");

                $("input#documentname").val("");
                $("input#personalfile").val("");

                $("input#adddocumentname").val("");
                $("input#addinstructiondocument").val("");
                $("input#otherfile").val("");
            });

            $(".modal .modal-dialog .modal-body > form").click(function (e) {
                e.stopPropagation();
            });

            $("form button[data-dismiss]").click(function () {
                $(".modal").click();
            });
        });

        $(document).ready(function () {
            $(".modal").click(function () {
                jQuery("#offerdocumentnamevalid").text("");
                jQuery("#resumefilevalid").text("");

                jQuery("#addexperiencedocumentnamevalid").text("");
                jQuery("#experirencefilevalid").text("");

                jQuery("#documentnamevalid").text("");
                jQuery("#personalfilealid").text("");

                jQuery("#adddocumentnamealid").text("");
                jQuery("#addinstructiondocumentalid").text("");
                jQuery("#addotherfilealid").text("");
            });
        });


    // offer  Letter upload
        function loading_file() {
            if (!window.FileReader) {
                return alert("FileReader API is not supported by your browser.");
            }
            var $i = $("#resumefile"), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if (input.files && input.files[0]) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    $("#23arrdata").val(fr.result);
                };
                //fr.readAsText( file );
                fr.readAsDataURL(file);
            } else {
                // Handle errors here
                alert("File not selected or browser incompatible.");
            }
        }

    

    // Edit Reimbursement
        var user_id = <?php echo $userid; ?>;
        $('#addoffboardinguserid').val(user_id);
        $.ajax({
            url: base_url+"viewUser",
            data: {user_id: user_id },
            type: "POST",
            dataType    : 'json',
            beforeSend: function(xhr){
                xhr.setRequestHeader('Token', localStorage.token);
            },

           success:function(response){

           $('#addoffboardinguserid').val(response["data"][0]["id"]);
         
            localStorage.removeItem('$reimbursementid');
        }
        });
    
    //Add Document offer letter form
        $("#addofferletter").submit(function(e) {

            var formData = {
                'user_id'           :   $('input[name=addoffboardinguserid]').val(),
                'description'     :   $('input[name=offboardingdescription]').val(),
                'offboarding_doc'      :   $('#23arrdata').val()
            };
         e.preventDefault();
            $.ajax({
                type: 'POST',
                url: base_url + 'addOffboarding',
                data: formData,
                dataType: 'json',
                encode: true,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response) {
            console.log(response);
             var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
            toastr.success(jsonData);
			      	setTimeout(function(){window.location ="<?php echo base_url()?>offBoardingAdd/"+user_id},1000);
              
            } else {
                toastr.error(falsedata);
				$('#addofferletter [type=submit]').attr('disabled',false);
            }
               
            });
          
        }); 

    
    //Add Offer Letter Validation
        $(document).ready(function () {
            $("#addofferletterbutton").click(function (e) {
                e.stopPropagation();
                var errorCount = 0;
                if ($("#offboardingdescription").val().trim() == "") {
                    $("#offboardingdescription").focus();
                    $("#offerdocumentnamevalid").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#offerdocumentnamevalid").text("");
                }

                if ($("#resumefile").val().trim() == "") {
                    $("#resumefilevalid").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#resumefilevalid").text("");
                }
                
                if (errorCount > 0) {
                    return false 
                }
            });
        });


   
</script>
