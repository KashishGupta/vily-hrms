
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mt-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>EmployeeList">Employee</a></li>
                <li class="breadcrumb-item active">Edit Employee</li>
            </ul>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="users-tab" data-toggle="tab" href="#personal-details" role="tab" aria-controls="personal-details" aria-selected="true">Personal Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="groups-tab" data-toggle="tab" href="#employee-email" role="tab" aria-controls="employee-email" aria-selected="false">Email & Password</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="groups-tab" data-toggle="tab" href="#bank-details" role="tab" aria-controls="bank-details" aria-selected="false">Bank Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="groups-tab" data-toggle="tab" href="#other-details" role="tab" aria-controls="other-details" aria-selected="false">Other Details</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="personal-details" role="tabpanel" aria-labelledby="personal-details">
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Personal Details</h3>
                    </div>
                    <div class="card-body">
                        <form role="form" action="" id="edituser" method="POST">
                            <div class="row claerfix">
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Employee Name <span class="text-danger"> *</span></label><br />
                                    <span class="text-danger change-pos" id="editemployeenamevalidate"></span>
                                    <input value="" id="edituserid" name="edituserid" class="form-control name-valid" type="hidden" />
                                    <input name="editlastname" id="editlastname" value="" class="form-control name-valid" type="text" placeholder="Please enter employee name." autocomplete="off" />
                                </div>

                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Father's Name <span class="text-danger"> *</span></label><br />
                                    <span class="text-danger change-pos" id="editfathernamevalidate"></span>
                                    <input name="editfathername" id="editfathername" value="" class="form-control name-valid" type="text" placeholder="Please enter father name." autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Employee Contact No. <span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editphonevalidate"></span>
                                    <input name="editphone" id="editphone" value="" maxlength="10" onkeypress="return onlyNumberKey(event)" class="form-control" type="text" placeholder="Please enter ten digit contact no." autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Joining Date<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editrelativenamevalidate"></span>
                                  
                                    <input data-provide="datepicker" data-date-autoclose="true" name="editjoining" id="editjoining" value="" class="form-control name-valid" type="text" readonly="" autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Personal Email <span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editemailvalidate"></span>
                                    <input name="editpersonalemail" id="editpersonalemail" value="" class="form-control" type="text" placeholder="e.g. hello@example.com" autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>DOB <span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editdobvalidate"></span>
                                    <input data-provide="datepicker" data-date-autoclose="true" value="" name="editdob" id="editdob" class="form-control" type="text" placeholder="yyyy/mm/dd" readonly autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Gender <span class="text-danger"> *</span></label>
                                    <select value="" id="editgender" name="editgender" class="form-control custom-select"> </select>
                                </div>
                                <div class="form-group col-md-8 col-sm-12">
                                    <label>Complete Address<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editaddressvalidate"></span>
                                    <textarea name="editaddress" value="" id="editaddress" class="form-control" type="text" autocomplete="off" placeholder="Please enter complete address."></textarea>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Branch<span class="text-danger">*</span></label><br>
                                    <span id="editbranchvalidate" class="text-danger change-pos"></span>
                                    <input value="" id="editjobid" name="editjobid" class="form-control" type="hidden">
                                    <select class="custom-select form-control" name="editbranchid" id="editbranchid">
                                        
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Department<span class="text-danger">*</span></label><br>
                                    <span id="editdepartmentvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="editdepartmentid" id="editdepartmentid">
                                        
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Designation<span class="text-danger">*</span></label><br>
                                    <span id="editdesignationvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="editdesignationid" id="editdesignationid">
                                    <input value="" id="edituseridcompany" name="edituseridcompany" class="form-control" type="hidden" />
                                    </select>
                                </div>
                               
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Image<span class="text-danger"> *</span></label>
                                    <br />
                                    <?php 
                                        $queryfile = $this->db->query("SELECT * FROM users INNER JOIN user_details ON users.id = user_details.user_id WHERE users.user_code='".$userid."'");
                                        $arrfile = $queryfile->row();
                                        $arrfile1 = $arrfile->user_image;
                                    ?>
                                    <span class="text-danger change-pos" id="editemployeeimagevalidate"></span>
                                    <input type="file" class="dropify form-control" name="file" id="file" data-default-file="<?php echo $arrfile1;?>" onchange="load_file();"  data-max-file-size="10MB">
                                    <input type="hidden" id="1arrdata" />
                                </div>
                                
                                <div class="form-group col-sm-12 text-right">
                                    <button id="editpersonalbutton" class="btn btn-primary submit-btn">Update Changes</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="employee-email" role="tabpanel" aria-labelledby="employee-email">                    
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Change Employee Password Or Email</h3>
                    </div>
                    <div class="card-body">
                        <form role="form" action="" id="edituserpass" method="POST">
                            <div class="row claerfix">
                                <div class="form-group col-md-4 col-sm-12">
                                    <label> Office Email<span class="text-danger"> *</span></label><br />
                                    <input value="" id="edituserid" name="edituserid" class="form-control name-valid" type="hidden" />
                                    <input name="editemail" id="editemail" value="" class="form-control" type="text" placeholder="Please enter Password." autocomplete="off" readonly="" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Reset Password <span class="text-danger"> *</span></label><br />
                                    <input value="" id="editemail" name="editemail" class="form-control name-valid" type="hidden" />
                                    <input name="editpassword" id="editpassword" value="" class="form-control" type="password" placeholder="Please enter Password." autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Confirm Password <span class="text-danger"> *</span></label><br />
                                    <input name="editcnfrmpassword" id="editcnfrmpassword" value="" class="form-control" type="password" placeholder="Please enter confirm password." autocomplete="off" />
                                </div>
                               
                                <div class="form-group col-sm-12 text-right">
                                    <button id="editpersonalbutton" class="btn btn-primary submit-btn">Update Changes</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="bank-details" role="tabpanel" aria-labelledby="bank-details">                    
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Bank Details</h3>
                    </div>
                    <div class="card-body">
                        <form role="form" action="" id="editbank_detail" method="POST">
                            <div class="row claerfix">
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Account Holder Name<span class="text-danger"> *</span></label><br />
                                    <span class="text-danger change-pos" id="editholdernamevalidate"></span>
                                    <input value="" id="editbankuserid" name="editbankuserid" class="form-control" type="hidden" />
                                    <input name="editholdername" id="editholdername" value="" class="form-control name-valid" type="text" placeholder="Please enter account holder name." autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Bank Name<span class="text-danger"> *</span></label><br />
                                    <span class="text-danger change-pos" id="editbanknamevalidate"></span>
                                    <input name="editbankname" id="editbankname" value="" class="form-control" type="text" placeholder="Please enter bank name."  autocomplete="off"/>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Branch Name<span class="text-danger"> *</span></label><br />
                                    <span class="text-danger change-pos" id="editbranchvalidate"></span>
                                    <input name="editbranch" id="editbranch" value="" class="form-control" type="text" placeholder="Please enter branch name." autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Account No<span class="text-danger"> *</span></label><br />
                                    <span class="text-danger change-pos" id="editaccountnovalidate"></span>
                                    <input name="editaccountno" id="editaccountno" maxlength="16" onkeypress="return onlyNumberKey(event)" value="" class="form-control" type="text" placeholder="Please enter account no."  autocomplete="off"/>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>IFSC Code<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editifscvalidate"></span>
                                    <input name="editifsc" id="editifsc" value="" class="form-control" type="text" maxlength="11" placeholder="Please enter ifsc code." autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Basic Salary<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editctcvalidate"></span>
                                    <input name="editctc" id="editctc" value="" class="form-control" type="text" maxlength="9" placeholder="e.g. ₹ 1200" onkeypress="return onlyNumberKey(event)" autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Pancard No<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editpancardvalidate"></span>
                                    <input name="editpancard" id="editpancard" maxlength="10" value="" class="form-control" type="text" placeholder="Please enter pancard no." autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Aadhar No<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editaadharcardvalidate"></span>
                                    <input name="editaadharcard" id="editaadharcard" value="" maxlength="12" onkeypress="return onlyNumberKey(event)" class="form-control" type="text" placeholder="Please enter aadhar card no." autocomplete="off" />
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="editbankbutton" class="btn btn-primary submit-btn">Update Changes</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="other-details" role="tabpanel" aria-labelledby="other-details">                    
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Other Information</h3>
                    </div>
                    <div class="card-body">
                        <form role="form" action="" id="editpesonal" method="POST">
                            <div class="row claerfix">
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Passport No.<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editpassportnovalidate"></span>
                                    <input value="" id="editpassportnoid" name="editpassportnoid" class="form-control" type="hidden" />
                                    <input name="editpassportno" id="editpassportno" value="" class="form-control" maxlength="9" placeholder="Please enter passport no." type="text" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Nationality <span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger" id="editnationalityvalidate"></span>
                                    <select name="editnationality" id="editnationality" class="form-control custom-select"> </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Religion.<span class="text-danger">*</span></label><br />
                                    <span class="text-danger" id="editreligionvalidate"></span>
                                    <select value="" name="editreligion" id="editreligion" class="form-control custom-select"> </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Marital status.<span class="text-danger">*</span></label><br />
                                    <span class="text-danger" id="editmaritalstatusvalidate"></span>
                                    <select value="" name="editmaritalstatus" id="editmaritalstatus" class="form-control custom-select"> </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-12" id="hideshow">
                                    <label>No. of children.<span class="text-danger">*</span></label><br />
                                    <span class="text-danger" id="editnoofchildrenvalidate"></span>
                                    <input value="" min="0" max="9" name="editnoofchildren" id="editnoofchildren" class="form-control" type="number" autocomplete="off" onkeypress="return onlyNumberKey(event)" />
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Name<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editrelativenamevalidate"></span>
                                    <input value="" id="editrelativeuserid" name="editrelativeuserid" class="form-control" type="hidden" />
                                    <input name="editrelativename" id="editrelativename" value="" class="form-control name-valid" placeholder="Please enter emergency conatct person name" type="text" autocomplete="off" />
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Relationship<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editrelationshipvalidate"></span>
                                    <select value="" name="editrelationship" id="editrelationship" class="form-control custom-select"> </select>
                                   
                                </div>
                                <div class="form-group col-md-4 col-sm-12">
                                    <label>Emergency Phone<span class="text-danger"> *</span></label>
                                    <br />
                                    <span class="text-danger change-pos" id="editemergencyphonevalidate"></span>
                                    <input name="editemergencyphone" id="editemergencyphone" value="" class="form-control" type="text"  placeholder="Please enter emergency phone"maxlength="10" onkeypress="return onlyNumberKey(event)"  autocomplete="off" />
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="editpersonalinfobutton" class="btn btn-primary submit-btn">Update Changes</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
$(function () {
    $("#editmaritalstatus").change(function () {
        if ($(this).val() == "Single/Unmarried") {
            $("#hideshow").hide();
        } else {
            $("#hideshow").show();
        }
    });
});

function load_file() {
    if (!window.FileReader) {
        return alert("FileReader API is not supported by your browser.");
    }
    var $i = $("#file"), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
    if (input.files && input.files[0]) {
        file = input.files[0]; // The file
        fr = new FileReader(); // FileReader instance
        fr.onload = function () {
            // Do stuff on onload, use fr.result for contents of file
            $("#1arrdata").val(fr.result);
        };
        //fr.readAsText( file );
        fr.readAsDataURL(file);
    } else {
        // Handle errors here
        alert("File not selected or browser incompatible.");
    }
}

//Select Gender by api
$.ajax({
    url: base_url + "viewGenderPublic",
    data: {},
    type: "POST",
    dataType: "json",
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
}).done(function (response) {
    let dropdown = $("#editgender");
    dropdown.empty();
    dropdown.append('<option selected="true" disabled>Select Gender</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.gender).text(entry.gender));
    });
});
//Select fetchRelations by api
function fetchRelationstatus(relationship) {
    $.ajax({
        url: base_url + "viewRelationnamePublic",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editrelationship");

        dropdown.empty();

        dropdown.append("<option disabled>Select Relationship</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (relationship == entry.relation) {
                dropdown.append($("<option selected></option>").attr("value", entry.relation).text(entry.relation));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.relation).text(entry.relation));
            }
        });
    });
}
//Select Nationality by api
function fetchNationalitystatus(nationality) {
    $.ajax({
        url: base_url + "viewNationalityPublic",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editnationality");

        dropdown.empty();

        dropdown.append("<option disabled>Select nationality</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (nationality == entry.nationality) {
                dropdown.append($("<option selected></option>").attr("value", entry.nationality).text(entry.nationality));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.nationality).text(entry.nationality));
            }
        });
    });
}
//Select Religion by api

function fetchReligionstatus(religion) {
    $.ajax({
        url: base_url + "viewReligionPublic",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editreligion");

        dropdown.empty();

        dropdown.append("<option disabled>Select religion</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (religion == entry.religion) {
                dropdown.append($("<option selected></option>").attr("value", entry.religion).text(entry.religion));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.religion).text(entry.religion));
            }
        });
    });
}
//Select Martial Status by api
function fetchMartialstatus(marital_status) {
    $.ajax({
        url: base_url + "viewMartialPublic",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editmaritalstatus");

        dropdown.empty();

        dropdown.append("<option disabled>Select marital status</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (marital_status == entry.status) {
                dropdown.append($("<option selected></option>").attr("value", entry.status).text(entry.status));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.status).text(entry.status));
            }
        });
    });
}
//Fetech Branch
function fetchBranch(branch_id) {
    $.ajax({
        url: base_url + "viewactiveBranch",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editbranchid");

        dropdown.empty();

        dropdown.append("<option disabled>Choose Branch</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (branch_id == entry.id) {
                dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.branch_name));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
            }
        });
    });
}

//Fetch Department
function fetchDepartment(department_id) {
    $.ajax({
        url: base_url + "viewDepartmentSelect",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editdepartmentid");

        dropdown.empty();

        dropdown.append("<option disabled>Choose Department</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (department_id == entry.department_id) {
                dropdown.append($("<option selected></option>").attr("value", entry.department_id).text(entry.department_name));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            }
        });
    });
}

//Fetch Designation
function fetchDesignation(designation_id) {
    $.ajax({
        url: base_url + "viewDesignationSelect",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editdesignationid");
        dropdown.empty();
        dropdown.append("<option disabled>Choose Designation</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (designation_id == entry.designation_id) {
                dropdown.append($("<option selected></option>").attr("value", entry.designation_id).text(entry.designation_name));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            }
        });
    });
}
// Edit' Form Fill
var user_code = "<?php echo $userid; ?>";
$.ajax({
    url: base_url + "viewUserEdit",
    data: { user_code: user_code },
    type: "POST",
    dataType: "json",
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },

    success: function (response) {
        var branch_id = response["data"][0]["branch_id"];
        var department_id = response["data"][0]["department_id"];
        var designation_id = response["data"][0]["designation_id"];
        fetchBranch(branch_id);
        fetchDepartment(department_id);
        fetchDesignation(designation_id);
        $("#edituseridcompany").val(response["data"][0]["id"]);
        $("#edituserid").val(response["data"][0]["id"]);
        $("#editlastname").val(response["data"][0]["first_name"]);
        $("#editemail").val(response["data"][0]["email"]);
        $("#editfathername").val(response["data"][0]["father_name"]);
        $("#editphone").val(response["data"][0]["phone"]);
        $("#editpersonalemail").val(response["data"][0]["personal_email"]);
        $("#editdob").val(response["data"][0]["dob"]);
        $("#editgender").val(response["data"][0]["gender"]);
        $("#editaddress").val(response["data"][0]["address"]);

        if (!$.trim(response.data[0].user_image)) {
            $("#edituserimagedata").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
        } else {
            $("#edituserimagedata").append('<img src="' + response.data[0].user_image + '" />');
        }

        $("#editbankuserid").val(response["data"][0]["id"]);
        $("#editholdername").val(response["data"][0]["holder_name"]);
        $("#editbankname").val(response["data"][0]["bank_name"]);
        $("#editbranch").val(response["data"][0]["branch"]);
        $("#editaccountno").val(response["data"][0]["account_no"]);
        $("#editifsc").val(response["data"][0]["ifsc"]);
        $("#editctc").val(response["data"][0]["salary"]);
        $("#editpancard").val(response["data"][0]["pan_card"]);
        $("#editaadharcard").val(response["data"][0]["aadhar_card"]);

        $("#editpassportnoid").val(response["data"][0]["id"]);
        $("#editpassportno").val(response["data"][0]["passport_no"]);
        var nationality = response["data"][0]["nationality"];
        fetchNationalitystatus(nationality);
        var religion = response["data"][0]["religion"];
        fetchReligionstatus(religion);

        var marital_status = response["data"][0]["marital_status"];
        fetchMartialstatus(marital_status);
        $("#editnoofchildren").val(response["data"][0]["no_of_children"]);

        $("#editrelativeuserid").val(response["data"][0]["id"]);
        $("#editrelativename").val(response["data"][0]["contactperson_name"]);
        var relationship = response["data"][0]["relationship"];
        fetchRelationstatus(relationship);

        $("#editemergencyphone").val(response["data"][0]["contactperson_no"]);
        // company Details

        $("#editjoining").val(response["data"][0]["joining_date"]);

        localStorage.removeItem("$userid");
    },
});
$("#editbranchid").change(function () {
    $.ajax({
        url: base_url + "viewDepartment",
        data: { branch_id: $("select[name=editbranchid]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editdepartmentid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });
});

$("#editdepartmentid").change(function () {
    $.ajax({
        url: base_url + "viewDesignation",
        data: { department_id: $("select[name=editdepartmentid]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editdesignationid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Desigination</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
        });
    });
});

// Edit User Details form
$("#edituser").submit(function (e) {
    var formData = {
        user_id: $("input[name=edituserid]").val(),
        first_name: $("input[name=editlastname]").val(),
        father_name: $("input[name=editfathername]").val(),
        phone: $("input[name=editphone]").val(),
        personal_email: $("input[name=editpersonalemail]").val(),
        dob: $("input[name=editdob]").val(),
        gender: $("select[name=editgender]").val(),
        address: $("textarea[name=editaddress]").val(),
        joining_date: $("input[name=editjoining]").val(),
        branch_id: $("select[name=editbranchid]").val(),
        department_id: $("select[name=editdepartmentid]").val(),
        designation_id: $("select[name=editdesignationid]").val(),
        user_image: $("input[name=editimgserid]").val(),

        user_image: $("#1arrdata").val(),
    };

    e.preventDefault();
    $.ajax({
        type: "POST",
        url: base_url + "editDetails",
        data: formData, // our data object
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        console.log(response);

        var jsonDa = response;
        var jsonData = response["data"]["message"];
        var falsedata = response["data"];
        if (jsonDa["data"]["status"] == "1") {
            toastr.success(jsonData);
            setTimeout(function () {
                window.location = "<?php echo base_url(); ?>EmployeeList";
            }, 1000);
        } else {
            toastr.error(falsedata);
            $("#edituser [type=submit]").attr("disabled", false);
        }
    });
});
// Edit User Details form
$("#edituserpass").submit(function (e) {
    var formData = {
        user_id: $("input[name=edituserid]").val(),
        email: $("input[name=editemail]").val(),
        password: $("input[name=editpassword]").val(),
        confirm_password: $("input[name=editcnfrmpassword]").val(),
    };

    e.preventDefault();
    $.ajax({
        type: "POST",
        url: base_url + "changePasswordEmployee",
        data: formData, // our data object
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        console.log(response);

        var jsonDa = response;
        var jsonData = response["data"]["message"];
        var falsedata = response["data"];
        if (jsonDa["data"]["status"] == "1") {
            toastr.success(jsonData);
            setTimeout(function () {
                window.location = "<?php echo base_url(); ?>EmployeeList";
            }, 1000);
        } else {
            toastr.error(falsedata);
            $("#edituser [type=submit]").attr("disabled", false);
        }
    });
});
// Edit Educaton Details form
$("#editbank_detail").submit(function (e) {
    var formData = {
        user_id: $("input[name=editbankuserid]").val(),
        holder_name: $("input[name=editholdername]").val(),
        bank_name: $("input[name=editbankname]").val(),
        branch: $("input[name=editbranch]").val(),
        account_no: $("input[name=editaccountno]").val(),
        ifsc: $("input[name=editifsc]").val(),
        salary: $("input[name=editctc]").val(),
        pan_card: $("input[name=editpancard]").val(),
        aadhar_card: $("input[name=editaadharcard]").val(),
    };

    e.preventDefault();
    $.ajax({
        type: "POST",
        url: base_url + "editBank",
        data: formData, // our data object
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        console.log(response);

        var jsonDa = response;
        var jsonData = response["data"]["message"];
        var falsedata = response["data"];
        if (jsonDa["data"]["status"] == "1") {
            toastr.success(jsonData);
            setTimeout(function () {
                window.location = "<?php echo base_url(); ?>EmployeeList";
            }, 1000);
        } else {
            toastr.error(falsedata);
            $("#editbank_detail [type=submit]").attr("disabled", false);
        }
    });
});

// Edit personal Details form*/
$("#editpesonal").submit(function (e) {
    var formData = {
        user_id: $("input[name=editpassportnoid]").val(),
        passport_no: $("input[name=editpassportno]").val(),
        nationality: $("select[name=editnationality]").val(),
        religion: $("select[name=editreligion]").val(),
        marital_status: $("select[name=editmaritalstatus]").val(),
        no_of_children: $("input[name=editnoofchildren]").val(),
        contactperson_name: $("input[name=editrelativename]").val(),
        contactperson_no: $("input[name=editemergencyphone]").val(),
        relationship: $("select[name=editrelationship]").val(),
    };

    e.preventDefault();
    $.ajax({
        type: "POST",
        url: base_url + "editPersonal",
        data: formData, // our data object
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        console.log(response);

        var jsonDa = response;
        var jsonData = response["data"]["message"];
        var falsedata = response["data"];
        if (jsonDa["data"]["status"] == "1") {
            toastr.success(jsonData);
            setTimeout(function () {
                window.location = "<?php echo base_url(); ?>EmployeeList";
            }, 1000);
        } else {
            toastr.error(falsedata);
            $("#editpesonal [type=submit]").attr("disabled", false);
        }
    });
});

// Edi Personal Details Validation
$(document).ready(function () {
    $("#editpersonalbutton").click(function (e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#editlastname").val().trim() == "") {
            $("#editlastname").focus();
            $("#editemployeenamevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editemployeenamevalidate").text("");
        }

        if ($("#editfathername").val().trim() == "") {
            $("#editfathername").focus();
            $("#editfathernamevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editfathernamevalidate").text("");
        }

        if ($("#editpersonalemail").val().trim() == "") {
            $("#editpersonalemail").focus();
            $("#editemailvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editemailvalidate").text("");
        }

        if ($("#editdob").val().trim() == "") {
            $("#editdob").focus();
            $("#editdobvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editdobvalidate").text("");
        }

        if ($("#editaddress").val().trim() == "") {
            $("#editaddress").focus();
            $("#editaddressvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editaddressvalidate").text("");
        }

        if (errorCount > 0) {
            return false;
        }
    });
});

// Edi Bank Details Validation
$(document).ready(function () {
    $("#editbankbutton").click(function (e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#editholdername").val().trim() == "") {
            $("#editholdername").focus();
            $("#editholdernamevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editholdernamevalidate").text("");
        }

        if ($("#editbankname").val().trim() == "") {
            $("#editbankname").focus();
            $("#editbanknamevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editbanknamevalidate").text("");
        }

        if ($("#editbranch").val().trim() == "") {
            $("#editbranch").focus();
            $("#editbranchvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editbranchvalidate").text("");
        }

        if ($("#editaccountno").val().trim() == "") {
            $("#editaccountno").focus();
            $("#editaccountnovalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editaccountnovalidate").text("");
        }

        if ($("#editifsc").val().trim() == "") {
            $("#editifsc").focus();
            $("#editifscvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editifscvalidate").text("");
        }

        if ($("#editctc").val().trim() == "") {
            $("#editctc").focus();
            $("#editctcvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editctcvalidate").text("");
        }

        if ($("#editpancard").val().trim() == "") {
            $("#editpancard").focus();
            $("#editpancardvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editpancardvalidate").text("");
        }

        if ($("#editaadharcard").val().trim() == "") {
            $("#editaadharcard").focus();
            $("#editaadharcardvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editaadharcardvalidate").text("");
        }

        if (errorCount > 0) {
            return false;
        }
    });
});

// Edi Bank Details Validation
$(document).ready(function () {
    $("#editrelativebutton").click(function (e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#editrelativename").val().trim() == "") {
            $("#editrelativename").focus();
            $("#editrelativenamevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editrelativenamevalidate").text("");
        }

        if ($("#editrelationship").val().trim() == "") {
            $("#editrelationship").focus();
            $("#editrelationshipvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editrelationshipvalidate").text("");
        }

        if ($("#editemergencyphone").val().trim() == "") {
            $("#editemergencyphone").focus();
            $("#editemergencyphonevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editemergencyphonevalidate").text("");
        }

        if (errorCount > 0) {
            return false;
        }
    });
});

</script>
