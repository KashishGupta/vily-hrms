<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url() ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Projects</li>
            </ul>
            <div class="header-action mt-3">
                <a href="<?php echo base_url(); ?>ProjectList" class="btn btn-info grid-system"><i class="fe fe-list"></i></a>
              
            </div>
        </div>
    </div>
</div>

<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="row kt-widget__items"></div>
    </div>
</div>

<!-- Delete Project Modal -->
<div class="modal custom-modal fade" id="delete_project" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" class="btn btn-danger continue-btn delete_project_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Project Modal -->

<div id="project_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Project Status</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="editstatus" method="POST" action="#">
                <div class="modal-body">
                    <div class="form-header">
                        <p>Are you sure want to change project status?</p>
                    </div>

                    <div class="form-group">
                        <label>Project Status<span class="text-danger">*</span></label>
                        <input value="" id="editprojectidstatus" name="editprojectidstatus" class="form-control" type="hidden" />
                        <select value="" id="editprojectstatus" name="editprojectstatus" class="form-control">
                            <option value="0">Pending</option>
                            <option value="1">Ongoing</option>
                            <option value="2">Completed</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="editleavebutton" class="btn btn-success submit-btn">Yes Change It!</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Show Project
    $.ajax({
        url: base_url + "viewProjectSelf",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="card"><div class="card-header"><h3 class="card-title"><strong>Projects</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var status_color = "";
                    if (response.data[i].status == 0) {
                        status_color = "Blue";
                    }
                    if (response.data[i].status == 1) {
                        status_color = "orange";
                    }
                    if (response.data[i].status == 2) {
                        status_color = "Green";
                    }

                var status = "";
                if (response.data[i].status == 0) {
                    isstatus = "Pending";
                }
                if (response.data[i].status == 1) {
                    isstatus = "Ongoing";
                }

                if (response.data[i].status == 2) {
                    isstatus = "Completed";
                }
                
                var startDate = new Date(response.data[i].start_date);
                var dd = String(startDate.getDate()).padStart(2, "0");
                var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = startDate.getFullYear();

                startDate = dd + "-" + mm + "-" + yyyy;
                var endDate = new Date(response.data[i].end_date);
                var dd = String(endDate.getDate()).padStart(2, "0");
                var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = endDate.getFullYear();

                endDate = dd + "-" + mm + "-" + yyyy;

                var html =
                    '<div class="col-lg-6 col-md-12"><div class="card"><div class="card-header"><h3 class="card-title">' +
                    response.data[i].project_name +
                    '</h3><div class="dropdown profile-action"><a href="#" class="action-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-156px, 18px, 0px); z-index: 99999;"><a class="dropdown-item edit_status" href="#" data-toggle="modal" data-target="#project_status" aria-expanded="false" id="' +
                    response.data[i].id +
                    '"><i class="fa fa-eye"></i>Edit Status</a> <a class="dropdown-item" href="ProjectEdit/' +
                    response.data[i].id +
                    '" id="' +
                    response.data[i].id +
                    '"><i class="fa fa-edit"></i>Edit Project</a> <a class="dropdown-item delete_data" href="#" data-toggle="modal" data-target="#delete_project" aria-expanded="false" id="' +
                    response.data[i].id +
                    '"><i class="fa fa-trash-alt"></i>Delete Project</a></div></div></div><div class="card-body"><p class="short-details">' +
                    response.data[i].project_description +
                    '</p><div class="row"><div class="col-5 py-1"><strong>Client Name:</strong></div><div class="col-7 py-1">' +
                    response.data[i].client_name +
                    '</div><div class="col-5 py-1"><strong>Creator Name:</strong></div><div class="col-7 py-1">' +
                    response.data[i].creator_name +
                    '</div><div class="col-5 py-1"><strong>Created Date:</strong></div><div class="col-7 py-1">' +
                    startDate +
                    '</div><div class="col-5 py-1"><strong>Deadline Date:</strong></div><div class="col-7 py-1 text-danger">' +
                    endDate +
                    '</div><div class="col-5 py-1"><strong>Project Details:</strong></div><div class="col-7 py-1 text-danger"><a href="' +
                    response.data[i].project_file +
                    '" download="file.pdf" class="btn btn-info"><i class="fa fa-download mr-2"></i>Project Details</a></div><div class="col-5 py-1"><strong>Project Langauge:</strong></div><div class="col-7 py-1">' +
                    response.data[i].project_language +
                    '</div><div class="col-5 py-1"><strong>Project Leader :</strong></div><div class="col-7 py-1">' +
                    response.data[i].first_name +
                    '</div><div class="col-5 py-1"><strong>Department :</strong></div><div class="col-7 py-1">' +
                    response.data[i].department_name +
                    '</div><div class="col-5 py-1"><strong>Project Status :</strong></div><div class="col-7 py-1"><span class="tag text-white"  style="background-color:' +
                    status_color +
                    '">' +
                    isstatus +
                    "</span></div></div></div></div></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });

    // Delete Project
    $(document).on("click", ".delete_data", function () {
        var project_id = $(this).attr("id");
        $(".delete_project_button").attr("id", project_id);
        $("#delete_project").modal("show");
    });

    $(document).on("click", ".delete_project_button", function () {
        var project_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteProject",
            method: "POST",
            data: { project_id: project_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>vily/Projects";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_project_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    // Edit Status
    $(document).on("click", ".edit_status", function () {
        var project_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewProject",
            method: "POST",
            data: {
                project_id: project_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editprojectidstatus").val(response["data"][0]["id"]);
                $("#editprojectstatus").val(response["data"][0]["status"]);
                $("#project_status").modal("show");
            },
        });
    });

    // Edit Project Active  form
    $("#editstatus").submit(function (e) {
        var formData = {
            project_id: $("input[name=editprojectidstatus]").val(),
            status: $("select[name=editprojectstatus]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "changeProjectStatus",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Projects";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editstatus [type=submit]").attr("disabled", false);
            }
        });
    });
</script>
