<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Holiday</li>
            </ul>
        </div>

        <!-- Add Holiday -->
        <div class="card mt-3" >
        <div class="card-header"><h3 class="card-title"><strong>Add Holiday</strong></h3></div><div class="card-body"><form id="addholidayy" method="POST" action="#"><div class="row clearfix"><div class="form-group col-md-4"><label>Branch Name<span class="text-danger"> *</span></label><br /><span id="addbranchvalidate" class="text-danger change-pos"></span><select class="custom-select form-control" name="addbranchid" id="addbranchid"> </select></div><div class="form-group col-md-4"><label>Holiday Name<span class="text-danger"> *</span></label><br /><span id="addholidaynamevalidate" class="text-danger change-pos"></span><input id="addholidayname" name="addholidayname" placeholder="Please enter holiday name." class="form-control" type="text" /></div><div class="form-group col-md-4"><label>Holiday Date<span class="text-danger"> *</span></label><br /><span id="addholidaydatevalidate" class="text-danger change-pos"></span><input data-provide="datepicker" data-date-autoclose="true" id="addholidaydate" name="addholidaydate" class="form-control" type="text" placeholder="YYYY/MM/DD" readonly /></div><div class="form-group col-lg-12 col-md-12 col-sm-12 text-right"><button id="addholidaybutton" class="btn btn-success submit-btn">Submit</button> <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button></div></div></form></div>
        </div>
        <!-- / Add Holiday -->

       
        <!-- Holiday List Show -->
        <div class="card mt-3"><div class="card-header">
                <h3 class="card-title"><strong>Search Holiday Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="holidaytable">
                    <thead>
                        <tr>
                            <th class="text-center">Holiday Date</th>
                            <th class="text-center">Holiday Name</th>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyholiday"></tbody>
                </table>
            </div>
        </div>
        <!-- /Holiday List Show -->
    </div>
</div>

<!-- Edit Holiday Modal -->
<div class="modal custom-modal fade" id="edit_holiday" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Holiday</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editholiday" method="POST">
                    <div class="form-group col-md-12">
                        <label>Branch Name<span class="text-danger"> *</span></label><br />
                        <span id="editbranchvalidate" class="text-danger change-pos"></span>
                        <select value="" class="custom-select form-control" name="editbranchid" id="editbranchid"> </select>
                        <input value="" id="editholidayid" name="editholidayid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Holiday Name<span class="text-danger"> *</span></label><br />
                        <span id="editholidaynamevalidate" class="text-danger change-pos"></span>
                        <input id="editholidayname" name="editholidayname" class="form-control name-valid" value="" type="text" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Holiday Date<span class="text-danger"> *</span></label><br />
                        <span id="editholidaydatevalidate" class="text-danger change-pos"></span>
                        <input id="editholidaydate" name="editholidaydate" class="form-control" data-provide="datepicker" data-date-autoclose="true" value="" type="text" readonly />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editholidaybutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Holiday Modal -->

<!-- Delete Holiday Modal -->
<div class="modal custom-modal fade" id="delete_holiday" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_holiday_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Holiday Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
 
    //For remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addbranchvalidate").text("");
            jQuery("#addholidaynamevalidate").text("");
            jQuery("#addholidaydatevalidate").text("");
            jQuery("#editbranchvalidate").text("");
            jQuery("#editholidaynamevalidate").text("");
            jQuery("#editholidaydatevalidate").text("");
        });
    });
    $("form button[data-dismiss]").click(function () {
        $(".modal").click();
    });

    $.ajax({
        url: base_url + "viewHolidayBranch",
        data:{},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        $("#holidaytable").DataTable().clear().destroy();
        var table = document.getElementById("tablebodyholiday");
        for (i in response.data) {

            var holidayDate = new Date(response.data[i].holiday_date);
            var dd = String(holidayDate.getDate()).padStart(2, '0');
            var mm = String(holidayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = holidayDate.getFullYear();
            holidayDate = dd + '-' + mm + '-' + yyyy;
            
            var tr = document.createElement("tr");
            tr.innerHTML =
                '<td class="text-center">' +
                holidayDate +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].holiday_title +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].branch_name +
                "</td>" +
                '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_holiday" aria-expanded="false" id="' +
                response.data[i].holiday_id +
                '" class="btn btn-primary edit_data" title="Edit" ><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_holiday" aria-expanded="false" id="' +
                response.data[i].holiday_id +
                '"  class="btn btn-danger delete_data"><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr);
        }

        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#holidaytable").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Holiday Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Holiday Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                }
            ]
        });
    });
       

    //Select branch by api
    $.ajax({
        url: base_url + "viewTokenBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Add Holiday
    $("#addholidayy").submit(function (e) {
        var formData = {
            branch_id: $("select[name=addbranchid]").val(),
            holiday_title: $("input[name=addholidayname]").val(),
            holiday_date: $("input[name=addholidaydate]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addHoliday",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        // using the done promise callback
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Holiday";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addholidaybutton [type=submit]").attr("disabled", false);
            }
        });
    });

    function fetchBranch(branch_id) {
        $.ajax({
            url: base_url + "viewTokenBranch",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editbranchid");

            dropdown.empty();

            dropdown.append("<option disabled>Choose Branch</option>");
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                if (branch_id == entry.id) {
                    dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.branch_name));
                } else {
                    dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
                }
            });
        });
    }

    // Edit  Holiday Form Fill
    $(document).on("click", ".edit_data", function () {
        var holiday_id = $(this).attr("id");
      //  alert(holiday_id);
        $.ajax({
            url: base_url + "viewEditHoliday",
            method: "POST",
            data: { holiday_id: holiday_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var branch_id = response["data"][0]["branch_id"];
                fetchBranch(branch_id);
                $("#editholidayid").val(response["data"][0]["holiday_id"]);
                $("#editholidayname").val(response["data"][0]["holiday_title"]);
                $("#editholidaydate").val(response["data"][0]["holiday_date"]);
                $("#edit_holiday").modal("show");
            },
        });
    });

    //edit Holiday form
    $("#editholiday").submit(function (e) {
        var formData = {
            branch_id: $("select[name=editbranchid]").val(),
            holiday_id: $("input[name=editholidayid]").val(),
            holiday_title: $("input[name=editholidayname]").val(),
            holiday_date: $("input[name=editholidaydate]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editHoliday",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Holiday";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editholiday [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Holiday
    $(document).on("click", ".delete_data", function () {
        var holiday_id = $(this).attr("id");
        $(".delete_holiday_button").attr("id", holiday_id);
        $("#delete_holiday").modal("show");
    });

    $(document).on("click", ".delete_holiday_button", function () {
        var holiday_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteHoliday",
            method: "POST",
            data: { holiday_id: holiday_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Holiday";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#delete_holiday_button [type=submit]").attr("disabled", false);
            }
            },
        });
    });

    //Add Holiday Validation
    $(document).ready(function () {
        $("#addholidaybutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            var test = $("#addholidaydate").val();
            if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }
            if ($("#addholidayname").val().trim() == "") {
                $("#addholidaynamevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addholidaynamevalidate").text("");
            }
            if ($("#addholidaydate").val().trim() == "") {
                $("#addholidaydatevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addholidaydatevalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Edit Holiday Validation
    $(document).ready(function () {
        $("#editholidaybutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editbranchid").val() == null) {
                $("#editbranchid").focus();
                $("#editbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#editbranchvalidate").text("");
            }
            if ($("#editholidayname").val().trim() == "") {
                $("#editholidaynamevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editholidaynamevalidate").text("");
            }
            if ($("#editholidaydate").val().trim() == "") {
                $("#editholidaydatevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editholidaydatevalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Filter Department Validation
    $(document).ready(function () {
        $("#filterholidaybutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#findbranch").val() == null) {
                $("#findbranch").focus();
                $("#findbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#findbranchvalidate").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>