<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Attendance</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Attendance</strong></h3>
            </div>
            <div class="card-body">
                <form id="filterattendance" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-md-3 col-sm-12">
                            <label>Branch Name<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="branchnamevalid"></span>
                            <select class="custom-select form-control" name="branchname" id="branchname" required="" />

                            </select>
                        </div>

                        <div class="form-group col-md-3 col-sm-12">
                            <label>Department Name<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="departmentnamevalid"></span>
                            <select class="custom-select form-control" name="departmentname" id="departmentname" required="" />

                            </select>
                        </div>

                        <div class="form-group col-md-3 col-sm-12">
                            <label>Designation Name<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="designationnamevalid"></span>
                            <select class="custom-select form-control" name="designationname" id="designationname" required="" />

                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <label>Employee Name<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="employeevalid"></span>
                            <select class="custom-select form-control" name="filterempid" id="filterempid" required="" />

                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12"> 
                            <label>Year<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="filteryearvalid"></span>
                            <select name="filteryear" id="filteryear" class="form-control custom-select">
                                <option>Select year</option>
                                <option>2019</option>
                                <option>2018</option>
                                <option>2017</option>
                                <option>2016</option>
                                <option>2015</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12"> 
                            <label>Month<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="filtermonthvalid"></span>
                            <select name="filtermonth" id="filtermonth" class="form-control custom-select">
                                <option>Select month</option>
                                <option>Jan</option>
                                <option>Feb</option>
                                <option>Mar</option>
                                <option>Apr</option>
                                <option>May</option>
                                <option>Jun</option>
                                <option>Jul</option>
                                <option>Aug</option>
                                <option>Sep</option>
                                <option>Oct</option>
                                <option>Nov</option>
                                <option>Dec</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12 button-open">
                            <button id="filteremployeebutton" class="btn btn-success submit-btn">Search</button>
                            <button type="reset" id="test" class="btn btn-secondary">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-status bg-orange"></div>
            <div class="card-header">
                <h3 class="card-title"><strong>Employee's Attendance List</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl" id="employeetablebody">
                    <thead>
                        <tr>
                            <th class="text-center">Emp ID</th>
                            <th class="text-left">Employee Name</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">In Time</th>
                            <th class="text-center">Out Time</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Working Hour</th>
                            <th class="text-center">in Image</th>
                            <th class="text-center">Out Image</th>
                        </tr>
                    </thead>
                    <tbody id="employeedatatable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

        
<div class="section-body note">
    <div class="container-fluid">
        <p>Note *</p>
        <ul>
            <li class="text-danger mb-2">Absent: A</li>
            <li class="text-success mb-2">Present: P</li>
            <li class="text-info mb-2">Half Day: HL</li>
            <li class=" mb-2">Leave: L</li>
            <li class="text-warning mb-2">Off Day: O</li>
            <li class=" mb-2">Holiday: H</li>
        </ul>
    </div>            
</div> 


<!-- Attendance Modal -->
<div class="modal custom-modal fade" id="attendance_info" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Attendance Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card punch-status">
                            <div class="card-body">
                                <h5 class="card-title">Timesheet <small class="text-muted">11 Mar 2019</small></h5>
                                <div class="punch-det">
                                    <h6>Punch In at</h6>
                                    <p>Wed, 11th Mar 2019 10.00 AM</p>
                                </div>
                                <div class="punch-info">
                                    <div class="punch-hours">
                                        <span>3.45 hrs</span>
                                    </div>
                                </div>
                                <div class="punch-det">
                                    <h6>Punch Out at</h6>
                                    <p>Wed, 20th Feb 2019 9.00 PM</p>
                                </div>
                                <div class="statistics">
                                    <div class="row">
                                        <div class="col-md-6 col-6 text-center">
                                            <div class="stats-box">
                                                <p>Break</p>
                                                <h6>1.21 hrs</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-6 text-center">
                                            <div class="stats-box">
                                                <p>Overtime</p>
                                                <h6>3 hrs</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card recent-activity">
                            <div class="card-body">
                                <h5 class="card-title">Activity</h5>
                                <ul class="res-activity-list">
                                    <li>
                                        <p class="mb-0">Punch In at</p>
                                        <p class="res-activity-time">
                                            <i class="fa fa-clock-o"></i>
                                            10.00 AM.
                                        </p>
                                    </li>
                                    <li>
                                        <p class="mb-0">Punch Out at</p>
                                        <p class="res-activity-time">
                                            <i class="fa fa-clock-o"></i>
                                            11.00 AM.
                                        </p>
                                    </li>
                                    <li>
                                        <p class="mb-0">Punch In at</p>
                                        <p class="res-activity-time">
                                            <i class="fa fa-clock-o"></i>
                                            11.15 AM.
                                        </p>
                                    </li>
                                    <li>
                                        <p class="mb-0">Punch Out at</p>
                                        <p class="res-activity-time">
                                            <i class="fa fa-clock-o"></i>
                                            1.30 PM.
                                        </p>
                                    </li>
                                    <li>
                                        <p class="mb-0">Punch In at</p>
                                        <p class="res-activity-time">
                                            <i class="fa fa-clock-o"></i>
                                            2.00 PM.
                                        </p>
                                    </li>
                                    <li>
                                        <p class="mb-0">Punch Out at</p>
                                        <p class="res-activity-time">
                                            <i class="fa fa-clock-o"></i>
                                            7.30 PM.
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="time"></div>
<!-- /Attendance Modal -->
<script src="<?php echo base_url()?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#branchnamevalid").text("");
            jQuery("#departmentnamevalid").text("");
            jQuery("#designationnamevalid").text("");
            jQuery("#employeevalid").text("");
            jQuery("#filteryearvalid").text("");
            jQuery("#filtermonthvalid").text("");
        });
    });


       function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  // add a zero in front of numbers<10
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
  t = setTimeout(function() {
    startTime()
  }, 500);
}
startTime();

      //Show Employee In table
      $.ajax({
            url: base_url + "viewAttendancePublic",
            data: {},
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            $("#employeetablebody").DataTable().clear().destroy();
                $("#employeetablebody tbody").empty();
            var table = document.getElementById("employeedatatable");
            for (i in response.data) {
                var de = new Date();
                var dateObj = new Date('May 18, 89 '+ response.data[i].in_time  +'');
                var difference = de.getTime() - dateObj.getTime();
     var daysDifference = Math.floor(difference/1000/60/60/24);
     difference -= daysDifference*1000*60*60*24;
   
      var hoursDifference = Math.floor(difference/1000/60/60);
      difference -= hoursDifference*1000*60*60;
      
      var minutesDifference = Math.floor(difference/1000/60);
      difference -= minutesDifference*1000*60;
      
      var secondsDifference = Math.floor(difference/1000);  


                var finalstatus = "";
                if(response.data[i].final_status == 0)
                {
                    finalstatus = 'IN';
                }
                if(response.data[i].final_status == 1)
                {
                    finalstatus = 'OUT'; 
                }
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
               
                var InImageUrl = "";
                if (!$.trim(response.data[i].in_image_url)) {
                    InImageUrl = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    InImageUrl = response.data[i].in_image_url;
                } 
                 
                var OutImageUrl = "";
                if (!$.trim(response.data[i].out_image_url)) {
                    OutImageUrl = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    OutImageUrl = response.data[i].out_image_url;
                }
            

                var tr = document.createElement("tr");
                tr.innerHTML =

                    '<td class="text-center">' +
                    response.data[i].emp_id +
                    "</td>" +

                    '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                        UserImage +
                        '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                        response.data[i].user_code +
                        '" title="">' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +

                    '<td class="text-center">' +
                    response.data[i].date +
                    "</td>" +

                    '<td class="text-center">' +
                    response.data[i].in_time +
                    "</td>" +

                    '<td class="text-center">' +
                    response.data[i].out_time +
                    "</td>" +

                    '<td class="text-center">' +
                    finalstatus +
                    "</td>" +
                    '<td class="text-center">' +
                    hoursDifference + ':' + minutesDifference + ':' + secondsDifference+' (Realtime Hours)' +
                    "</td>" +
                   '<td class="text-center"><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                   InImageUrl +
                        '" alt=""></span></div></td>' +
                  '<td class="text-center"><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                  OutImageUrl +
                        '" alt=""></span></div></td>';
                table.appendChild(tr);
            }
            $("#employeetablebody").DataTable({
                /*"scrollY": 200,
                "scrollX": true,*/
                "dom": "Bfrtip",
                "buttons": ["excelHtml5", "pdfHtml5"],
            });
        });

$.ajax({
    url: base_url + "viewactiveBranch",
    data: {},
    type: "POST",
    dataType: "json", // what type of data do we expect back from the server
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    let dropdown = $("#branchname");

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose Branch</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
    });
});

// Select department by api*/
$("#branchname").change(function () {
    $.ajax({
        url: base_url + "viewDepartment",
        data: { branch_id: $("select[name=branchname]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#departmentname");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });
});

// Select Designation by api*/
$("#departmentname").change(function () {
    $.ajax({
        url: base_url + "viewDesignation",
        data: { department_id: $("select[name=departmentname]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#designationname");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Designation</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
        });
    });
});

//Add Designation Validation
    $(document).ready(function () {
        $("#filteremployeebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#branchname").val() == null) {
                $("#branchname").focus();
                $("#branchnamevalid").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#branchnamevalid").text("");
            }
            if ($("#departmentname").val() == null) {
                $("#departmentname").focus();
                $("#departmentnamevalid").text("Please select a department name.");
                errorCount++;
            } else {
                $("#departmentnamevalid").text("");
            }
            if ($("#designationname").val() == null) {
                $("#designationname").focus();
                $("#designationnamevalid").text("Please select a designation name.");
                errorCount++;
            } else {
                $("#designationnamevalid").text("");
            }
            if ($("#filterempid").val() == null) {
                $("#filterempid").focus();
                $("#employeevalid").text("Please select a employee name.");
                errorCount++;
            } else {
                $("#employeevalid").text("");
            }
            if ($("#filteryear").val() == null) {
                $("#filteryear").focus();
                $("#filteryearvalid").text("Please select a year.");
                errorCount++;
            } else {
                $("#filteryearvalid").text("");
            }
            if ($("#filtermonth").val() == null) {
                $("#filtermonth").focus();
                $("#filteryearvalid").text("Please select a year.");
                errorCount++;
            } else {
                $("#filteryearvalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

       </script>