<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Tickets">Tickets</a></li>
                <li class="breadcrumb-item active">Add Tickets</li>
            </ul>
        </div>
    </div>
</div>

<div class="message_succes text-succes" id="message"></div>


<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Ticket</strong></h3>
            </div>
            <div class="card-body">
                <!-- Add Ticket Form -->
                <form id="edittickets" method="POST" action="#">
                    <div class="row">
                        <div class="form-group col-md-6">
                        <label>Type<span class="text-danger"> *</span></label>
                            <br />
                            <span id="addtickettypevalidate" class="text-danger change-pos"></span>
                        <select class="custom-select form-control" value="" name="edittickettype" id="edittickettype"> </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Branch <span class="text-danger">*</span></label>
                            <br />
                            <span id="addbranchvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="editbranchid" id="editbranchid"> </select>
                        </div>
                        
                        <div class="form-group col-md-6">
                        <label>Department<span class="text-danger"> *</span></label>
                        <br />
                        <span id="adddepartmentvalidate" class="text-danger change-pos"></span>
                        <select class="custom-select form-control" name="editdepartmentid" id="editdepartmentid"> </select>
                        </div>
                        <div class="form-group col-md-6">
                        <label>Ticket Priority<span class="text-danger"> *</span></label>
                        <br />
                        <span id="addpriorityvalidate" class="text-danger change-pos"></span>
                        <select class="custom-select form-control" value="" name="editpriority" id="editpriority"> </select>
                        </div>
                        <div class="form-group col-md-6">
                        <label>Subject<span class="text-danger"> *</span></label>
                        <br />
                        <span id="addtitlevalidate" class="text-danger change-pos"></span>
                        <input class="form-control" type="text" value="" id="editsubject" name="editsubject" placeholder="Please enter subject" autocomplete="off" />
                        <input class="form-control" type="hidden" value="" id="ticketsid" name="ticketsid"/>
                        </div>
                        <div class="form-group col-md-6">
                        <label>Description<span class="text-danger"> *</span></label>
                        <br />
                        <span id="adddescriptionvalidate" class="text-danger change-pos"></span>
                       <textarea class="form-control" value="" id="editdescription" name="editdescription"></textarea>
                        </div>
                    </div>
                    <span id="message" class="text-danger"></span>
                    <div class="submit-section pull-right">
                        <button id="addticketbutton" class="btn btn-success submit-btn">Submit</button>
                        <button type="reset" class="btn btn-secondary" id="test" data-dismiss="modal">Reset</button>
                    </div>
                </form>
                <!-- /Add Branch Form -->
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
  //for remove validation and empty input field

  $(document).ready(function () {
    $("#test").click(function () {
        jQuery("#addtickettypevalidate").text("");
        jQuery("#addbranchvalidate").text("");
        jQuery("#adddepartmentvalidate").text("");
        jQuery("#addpriorityvalidate").text("");
        jQuery("#addtitlevalidate").text("");
        jQuery("#adddescriptionvalidate").text("");
    });
});

     //Select Ticket Type by api
     $.ajax({
        url: base_url + "viewTicketsType",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#edittickettype");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Ticket Type</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.type_name).text(entry.type_name));
        });
    });
     //Fetech Branch              
     function fetchBranch(branch_id) {
            $.ajax({  
                url: base_url+"viewactiveBranch",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
              let dropdown = $('#editbranchid');
    
                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Branch</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(branch_id == entry.id)
                        {
                            dropdown.append($('<option selected="true"></option>').attr('value', entry.id).text(entry.branch_name));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.branch_name));
                        }
                    })
            }); 
        }
    
    //Fetch Department                
        function fetchDepartment(department_id) {
            $.ajax({  
                url: base_url+"viewDepartmentSelect",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
                let dropdown = $('#editdepartmentid');
    
                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Department</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(department_id == entry.department_id)
                        {
                            dropdown.append($('<option selected></option>').attr('value', entry.department_id).text(entry.department_name));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.department_id).text(entry.department_name));
                        }
                    })
            }); 
        }


    //Select Ticket Priority by api
    $.ajax({
        url: base_url + "viewTicketsPriority",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editpriority");

        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Priority</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.priority));
        });
    });

///Edit  Tickets Form Fill
var ticket_id = '<?php echo $ticketid; ?>';

        $.ajax({
            url: base_url + "viewTicketsEdit",
            method: "POST",
            data: {
                ticket_id: ticket_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                var branch_id = response["data"][0]["send_branch"];
                var department_id = response["data"][0]["send_department"];
                fetchBranch(branch_id);
                fetchDepartment(department_id);
                $("#ticketsid").val(response["data"][0]["id"]);
                $("#editsubject").val(response["data"][0]["title"]);
                
                $("#edittickettype").val(response["data"][0]["ticket_type"]);
                $("#editpriority").val(response["data"][0]["priority"]);
                $("#editdescription").val(response["data"][0]["description"]);
            },
        });
     //Edit Tickets form
     $("#edittickets").submit(function (e) {
        var formData = {
            title: $("input[name=editsubject]").val(),
            ticket_type: $("select[name=edittickettype]").val(),
            ticket_id: $("input[name=ticketsid]").val(),
            priority: $("select[name=editpriority]").val(),
            send_department: $("select[name=editdepartmentid]").val(),
            send_branch: $("select[name=editbranchid]").val(),
            description: $("textarea[name=editdescription]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editTickets",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Tickets";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#edittickets [type=submit]").attr("disabled", false);
            }
        });
    });

      //Add Tickets Validation
      $(document).ready(function () {
        $("#addticketbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#edittickettype").val() == null) {
                $("#addtickettype").focus();
                $("#addtickettypevalidate").text("Please select a ticket type.");
                errorCount++;
            } else {
                $("#addtickettypevalidate").text("");
            }
            if ($("#editpriority").val() == null) {
                $("#addpriorityvalidate").text("Please select a priority type.");
                errorCount++;
            } else {
                $("#addpriorityvalidate").text("");
            }
            if ($("#editdepartmentid").val() == null) {
                $("#adddepartmentvalidate").text("Please select a department.");
                errorCount++;
            } else {
                $("#adddepartmentvalidate").text("");
            }
            if ($("#editbranchid").val() == null) {
                $("#addbranchvalidate").text("Please select a Branch.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }
            if ($("#editsubject").val().trim() == "") {
                $("#addtitlevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addtitlevalidate").text("");
            }
            if ($("#editdescription").val().trim() == "") {
                $("#adddescriptionvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#adddescriptionvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>


