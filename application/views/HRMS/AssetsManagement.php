<style type="text/css">
    tbody#tablebodyassets tr {
        background: #f6a9d1 !important;
    }
</style>
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item">Assets Management</a></li>
            </ul>
        </div>

        <div class="row mt-3">
            <div class="col-sm-12 search-buttons-wrapper clearfix">
                <a id="test" class="search_by_status btn btn-default" href="javascript:void(0);" style="background: #f6a9d1;">Total Issued Assets</a>
                <a id="test1" class="search_by_status btn btn-default" href="javascript:void(0);">Non Issued Assets</a>
            </div>
        </div>

        <!-- Search Assets Result-->
        <div class="card mt-3" id="totalissued">
            <div class="card-header">
                <h3 class="card-title"><strong>Issued Assets</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style dataTable table-responsive table-responsive-xxl no-footer" id="assetstable">
                    <thead>
                        <tr style="background: #f6a9d1;">
                            <th class="text-center">Asset Id</th>
                            <th class="text-center">Asset Name</th>
                            <th class="text-center">Asset Category</th>
                            <th class="text-center">Issue Date</th>
                            <th class="text-center">Issued To</th>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Employee Id</th>
                            <th class="text-center">Issued By</th>
                            <th class="text-center">Serial Number</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyassets"></tbody>
                </table>
            </div>
        </div>
        <!-- /Search Assets Result-->

        <!-- Search Assets Result-->
        <div class="card mt-3" id="nonissuedassets">
            <div class="card-header">
                <h3 class="card-title"><strong>Non Issued Assets</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style dataTable table-responsive table-responsive-xxl no-footer" id="assetissuedTableNone">
                    <thead >
                        <tr>
                            <th class="text-center">Asset Id</th>
                            <th class="text-center">Asset Name</th>
                            <th class="text-center">Asset Warranty</th>
                            <th class="text-center">Purchage Date</th>
                            <th class="text-center">Assets Condition</th>
                            <th class="text-center">Purchage By</th>
                            <th class="text-center">Serial Number</th>
                        </tr>
                    </thead>
                    <tbody id="tableNoneissued"></tbody>
                </table>
            </div>
        </div>
        <!-- /Search Assets Result-->
    </div>
</div>
<!-- Assets Issue Edit Modal -->
<div class="modal custom-modal fade" id="edit_assetissue" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Release Assets</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="edituserForm" method="POST">
                    <div class="form-group col-md-12">
                        <label>Asset Category<span class="text-danger">*</span></label>
                        <br />
                        <span class="text-danger change-pos" id="editbranchnamevalidate"></span>
                        <input type="text" class="form-control" name="assetCategory" id="assetCategory" readonly="">
                        <input value="" id="edituserid" name="edituserid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Asset Name<span class="text-danger">*</span></label>
                        <br />
                        <span class="text-danger" id="editbranchlocationvalidate"></span>
                        <input type="text" class="form-control" name="assetName" id="assetName" readonly="">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Issue Date <span id="editbranchlatvalidate" class="text-danger">*</span></label>
                        <input type="text" value="" id="editissuedate" name="editissuedate" class="form-control" readonly="" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Issued To <span id="editbranchlngvalidate" class="text-danger">*</span></label>
                        <input list="browsers" name="issuedto" id="issuedto" class="form-control" readonly="" />
                        <input type="hidden" name="issuedto2" id="issuedto2" class="form-control" readonly="" />
                        <datalist id="browsers">
                            <option id="result"> </option>
                        </datalist>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Release/Non Release<span class="text-danger">*</span></label>
                        <br />
                        <span class="text-danger change-pos" id="editbranchlocationvalidate"></span>
                        <select class="custom-select form-control" name="editrelease" id="editrelease">
                            <option value="1">Relase</option>
                            <option value="0">Non Relase</option>
                        </select>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editbranchbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Assets Issue Edit Modal -->
<!-- <style type="text/css">
    #nonissuedassets {
        display: none;
    }
</style> -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#test1").click(function () {
            $("#nonissuedassets").show();
        });
        $("#test1").click(function () {
            $("#totalissued").hide();
        });
    });

    $(document).ready(function () {
        $("#nonissuedassets").css("display", "none");
    });

    $(document).ready(function () {
        $("#test").click(function () {
            $("#totalissued").show();
        });
        $("#test").click(function () {
            $("#nonissuedassets").hide();
        });
    });
  
    // View active job type
    $(document).on("click", ".edit_data", function () {
        var assetsUser_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewAsseteditsUser",
            method: "POST",
            data: {
                assetsUser_id: assetsUser_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                
                
                $("#assetCategory").val(response["data"][0]["category_assets"]);
                $("#assetName").val(response["data"][0]["assets_name"]);
                $("#edituserid").val(response["data"][0]["id"]);
                $("#editissuedate").val(response["data"][0]["issue_date"]);
                $("#issuedto").val(response["data"][0]["first_name"]);
                $("#issuedto2").val(response["data"][0]["user_id"]);
                $("#editrelease").val(response["data"][0]["is_release"]);

                $("#edit_assetissue").modal("show");
            },
        });
    });
    // edit Asssets User form*/
    $("#edituserForm").submit(function (e) {
        var formData = {
            userStatus_id: $("input[name=edituserid]").val(),
            is_release: $("select[name=editrelease]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editAssetsUser",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>AssetsManagement";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editjobs [type=submit]").attr("disabled", false);
            }
        });
    });

    $(document).ready(function () {
        $("#issuedto").keyup(function () {
            var first_name = $(this).val();
            $.ajax({
                url: base_url + "viewUserName",
                data: { first_name: first_name },
                type: "POST",
                dataType: "json",
                timeout: 6000,
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                $("#issuedto2").val(response["data"][0]["user_id"]);
                var test = response.data[0].first_name;
                //alert(test);
                //$("#result").html(response.data[0].first_name);
                $("#result").html(test);
            });
        });
    });
    $.ajax({
        url: base_url + "viewAssetsUserTotal",
        data: {},
        type: "POST",
        dataType: "json",
        timeout: 6000,
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        $("#assetstable").DataTable().clear().destroy();
        $("#assetstable tbody").empty();
        var table = document.getElementById("tablebodyassets");
        for (i in response.data) {
            var date = new Date(response.data[i].issue_date);
            var dd = String(date.getDate()).padStart(2, "0");
            var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = date.getFullYear();

            date = dd + "-" + mm + "-" + yyyy;

            var tr = document.createElement("tr");
            tr.innerHTML =
                '<td class="text-center text-uppercase">' +
                response.data[i].assets_code +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].assets_name +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].category_assets +
                "</td>" +
                '<td class="text-center">' +
                date +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].first_name +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].branch_name +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].emp_id +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].issued_by +
                "</td>" +
                '<td class="text-center text-uppercase">' +
                response.data[i].serial_no +
                "</td>" +
                '<td class="text-center"> <button type="button" data-toggle="modal" data-target="#edit_assetissue" aria-expanded="false" id="' +
                response.data[i].id +
                '" class="btn btn-info edit_data" title="Edit Assets Issue" ><i class="fa fa-edit"></i></button> </td>';
            table.appendChild(tr);
        }

        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#assetstable").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Issued Assets Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Issued Assets Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                    }
                }
            ]
        });
    });

    $.ajax({
        url: base_url + "viewNoneAssetsIssued",
        data: {},
        type: "POST",
        dataType: "json",
        timeout: 6000,
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        $("#assetissuedTableNone").DataTable().clear().destroy();
        $("#assetissuedTableNone tbody").empty();
        var table = document.getElementById("tableNoneissued");
        for (i in response.data) {
            var date = new Date(response.data[i].purchage_date);
            var dd = String(date.getDate()).padStart(2, "0");
            var mm = String(date.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = date.getFullYear();

            date = dd + "-" + mm + "-" + yyyy;

            var tr = document.createElement("tr");
            tr.innerHTML =
                '<td class="text-center text-uppercase">' +
                response.data[i].assets_code +
                "</td>" +
                '<td class="text-center"><a href="AssetDetails/' + response.data[i].assets_code +'">' +
                        response.data[i].assets_name +
                        "</a></td>" +
                '<td class="text-center">' +
                response.data[i].assets_warranty +
                "</td>" +
                '<td class="text-center">' +
                date +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].assets_cond +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].purchage_by +
                "</td>" +
                '<td class="text-center text-uppercase">' +
                response.data[i].serial_no +
                "</td>";
            table.appendChild(tr);
        }
        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#assetissuedTableNone").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Non Issued Assets Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Non Issued Assets Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                    }
                }
            ]
        });
    });
</script>
