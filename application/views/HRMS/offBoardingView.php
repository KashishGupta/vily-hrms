        <div class="section-body">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center ">
                    <ul class="breadcrumb mt-3 mb-0">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active">View Document</li>
					</ul>
                </div>
            </div>
        </div>
        
        <div class="section-body">
            <div class="container-fluid">
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title">View Offboarding Letter</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover " id="reimbursementtable">
                            <tbody id="tabledocument">
                                <tr>
                                   
                                   
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
        </div> 
        
        <!-- Delete Offer Letter Modal -->
        <div class="modal custom-modal fade" id="delete_offboarding" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header text-center">
                            <i class="fa fa-ban"></i>
                            <h3>Are you sure want to delete?</h3>
                        </div>
                        <div class="modal-btn delete-action pull-right">
                            <a href="#" class="btn btn-danger continue-btn delete_offer_button">Yes delete it!</a>
                            <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete Persoanl Letter Modal -->
        
       
        	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        		<script>
        	// offer  Letter upload
        		function loading_file() {
            		if ( ! window.FileReader ) {
            			return alert( 'FileReader API is not supported by your browser.' );
            		}
            		var $i = $( '#resumefile' ), // Put file input ID here
            			input = $i[0]; // Getting the element from jQuery
            		if ( input.files && input.files[0] ) {
            			file = input.files[0]; // The file
            			fr = new FileReader(); // FileReader instance
            			fr.onload = function () {
            				// Do stuff on onload, use fr.result for contents of file
            				$( '#23arrdata' ).val( fr.result );
            			};
            			//fr.readAsText( file );
            			fr.readAsDataURL( file );
            		} else {
            			// Handle errors here
            			alert( "File not selected or browser incompatible." )
            		}
                }
                
                
            // Personal  Letter upload
        	
                </script>
         <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        <script>
                    
            // View Offerletter
                var user_id = <?php echo $userid; ?>;
                $.ajax({
                    url: base_url+"viewOffboarding",
                    data: {user_id: user_id },
                    method:"POST",  
                    dataType:"json", 
                    beforeSend: function(xhr){
                         xhr.setRequestHeader('Token', localStorage.token);}
                })
                
                .done(function(response) { 
                 
                    var table = document.getElementById('tabledocument');
                    
                    for (i in response.data) {
                        var tr = document.createElement('tr');
                        tr.innerHTML = 
                        '<td><svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc"><path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path><path d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z" fill="#dadcdf"></path><path d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z" fill="#dadcdf"></path> </svg>' + response.data[i].description + '</td>' +
                        
                        ' <td class="text-right"><a href="'+ response.data[i].offboarding_doc + '" class="btn btn-success view_data" ><i class="fa fa-arrow-down"></i></a>  <button type="button" data-toggle="modal" data-target="#delete_offboarding" aria-expanded="false" class="btn btn-danger delete_data" id="' + response.data[i].id + '" ><i class="fa fa-trash-alt"></i></button></td>';
                        table.appendChild(tr);
                    }
                    $('#reimbursementtable').DataTable();
                })
                
                
              
                
                // Delete Document offer Letter
                 $(document).on('click', '.delete_data', function() {
                    var offboarding_id = $(this).attr("id");
                    $('.delete_offer_button').attr('id', offboarding_id);
                    $('#delete_offboarding').modal('show');
                });
                
                 $(document).on('click', '.delete_offer_button', function() {
                    var offboarding_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "deleteOffboarding",
                        method: "POST",
                        data: {
                            offboarding_id: offboarding_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success: function(response) {
                           // console.log(response);
                           var user_id = <?php echo $userid; ?>;
                           window.location.replace("<?php echo base_url(); ?>offBoardingView/"+user_id);
                           
                        }
                    });
                });
                
               
        </script>