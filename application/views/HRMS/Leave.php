<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Leave</li>
            </ul>
        </div>
        <div class="row mt-3">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-users"></i></div>
                        <h3 class="mb-1" id="totalemployee"></h3>
                        <div>Total Employee</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-plane-departure"></i></div>
                        <h3 class="mb-1" id="leaveAbsentemployee"></h3>
                        <div>Total On Leave</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-chart-line"></i></div>
                        <h3 class="mb-1" id="presentemployee"></h3>
                        <div>Total Present</div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-chart-bar"></i></div>
                        <h3 class="mb-1" id="absentemployee"></h3>
                        <div>Total Absent</div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Search Leave Result -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Leave Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl" id="leavetable">
                    <thead>
                        <tr>
                            <th class="text-center">Employee Name</th>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Department</th>
                            <th class="text-center">Leave Type</th>
                            <th class="text-center">From</th>
                            <th class="text-center">To</th>
                            <th class="text-center">Total Days</th>
                            <th class="text-center">Reason</th>
                            <th class="text-center">Status</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyleave"></tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>


<!-- Approved Leave Modal -->
<div id="approved_leave" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header">
                    <h3>Leave Approve</h3>
                    <p>Are you sure want to approve for this leave?</p>
                </div>
                <form id="editleave" method="POST" action="#">
                    <input value="" id="Leaveapprovedtype" name="Leaveapprovedtype" class="form-control" type="hidden" />
                    <input value="" id="Leaveapprovedapply" name="Leaveapprovedapply" class="form-control" type="hidden" />
                    <input value="" id="Leaveapprovedfrom" name="Leaveapprovedfrom" class="form-control" type="hidden" />

                    <input value="" id="editleavetypeid" name="editleavetypeid" class="form-control" type="hidden" />
                    <input value="" id="editUserId" name="editUserId" class="form-control" type="hidden" />
                    <input value="" id="Leavedays" name="Leavedays" class="form-control" type="hidden" />
                    
                    <input value="1" id="editleaveaction" name="editleaveaction" class="form-control" type="hidden" />

                    <div class="submit-section pull-right">
                        <button id="ediapprovedbutton" class="btn btn-success submit-btn">Yes Approved It!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Approved Leave Modal -->

<!-- Denied Leave Modal -->
<div class="modal custom-modal fade" id="denied_leave" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header">
                    <h3>Leave Denied</h3>
                    <p>Are you sure want to denied for this leave?</p>
                </div>
                <form id="declinedleave" method="POST" action="#">
                    <input value="" id="Leavedelclinedleavetype" name="Leavedelclinedleavetype" class="form-control" type="hidden" />
                    <input value="" id="Leavedelclinedfrom" name="Leavedelclinedfrom" class="form-control" type="hidden" />
                    <input value="" id="Leavedelclinedapply" name="Leavedelclinedapply" class="form-control" type="hidden" />
                    <input value="" id="editLeavedelclinedid" name="editLeavedelclinedid" class="form-control" type="hidden" />
                    <input value="" id="editLeavedelclinedemail" name="editLeavedelclinedemail" class="form-control" type="hidden" />
                    <input value="2" id="editLeavedeclined" name="editLeavedeclined" class="form-control" type="hidden" />
                    <div class="form-group">
                        <label>Leave Declined Reason <span class="text-danger">*</span></label><br>
                        <span id="declineleavevalid" class="text-danger"></span>
                        <textarea rows="3" class="form-control" name="declineleave" id="declineleave" placeholder="Please enter denied reason."></textarea>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="leavedeniedbutton" class="btn btn-success submit-btn">Yes denied It!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
  
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("textarea#declineleave").val("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });
// delclined leave Button
    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#declineleavevalid").text("");
        });
    });

    //Show All Employee
    $.ajax({
        url: base_url + "totalEmployeeLeave",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totalemployee").html(response.data[0].id);
        },
    });

    // onLeave Employee Today
    $.ajax({
        url: base_url + "totalAbsentEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            //var testingData = response.data[0].leaveAbsent;

            $("#leaveAbsentemployee").html(response.data[0].totalonleave);
            $("#absentemployee").html(response.data[0].totalonleave);
        },
    });
    
    // Attendances present Employee
    $.ajax({
        url: base_url + "totalPresentEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            var presentData = response.data;
          //alert(presentData);
            $("#presentemployee").html(presentData);
        },
    });

   

    //Show Leave list Table
    $.ajax({
        url: base_url + "viewAllLeave",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        $("#leavetable").DataTable().clear().destroy();
        var table = document.getElementById("tablebodyleave");
        for (i in response.data) {

            var startDate = new Date(response.data[i].start_date);
            var dd = String(startDate.getDate()).padStart(2, '0');
            var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = startDate.getFullYear();

            startDate = dd + '-' + mm + '-' + yyyy;


            var endDate = new Date(response.data[i].end_date);
            var dd = String(endDate.getDate()).padStart(2, '0');
            var mm = String(endDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = endDate.getFullYear();

            endDate = dd + '-' + mm + '-' + yyyy;

            var UserImage = "";
            if (!$.trim(response.data[i].user_image)) {
                UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
            } else {
                UserImage = response.data[i].user_image;
            }


            var status = "";
            if (response.data[i].status == 0) {
                status =
                    '<div class="dropdown action-label"><a class="btn btn-primary btn-sm btn-rounded  dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-purple"></i> Pending</a><div class="dropdown-menu dropdown-menu-right" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-17px, 31px, 0px); z-index:9999;"> <a class="dropdown-item approved_data" href="#" data-toggle="modal" id="' +
                    response.data[i].leave_id +
                    '" data-target="#approved_leave"><i class="fa fa-dot-circle-o text-success"></i> Approved</a> <a class="dropdown-item declined_data" href="#" data-toggle="modal" id="' +
                    response.data[i].leave_id +
                    '" data-target="#denied_leave"><i class="fa fa-dot-circle-o text-danger"></i> Declined</a></div></div>';
            }
            
            if (response.data[i].status == 1) {
                status = '<div class="dropdown action-label"><a class="btn btn-success btn-sm btn-rounded " href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-success"></i> Approved</a></div>';
            }
            if (response.data[i].status == 2) {
                status = '<div class="dropdown action-label"><a class="btn btn-danger btn-sm btn-rounded " href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-danger"></i> Declined</a></div>';
            }
            var totalPercent = "";
           
            if(response.data[i].allow_short == 1)
            {
                totalPercent = response.data[i].days;
            }else if(response.data[i].allow_half == 1)
            {
                totalPercent = response.data[i].days;
            }
            else{
                var number = response.data[i].days;
            var percentToGet = 1;
            var percent =number + percentToGet;
            totalPercent = percent;
            }
            var tr = document.createElement("tr");
            tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +

                '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                    UserImage +
                    '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                    response.data[i].user_code +
                    '" title="">' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].emp_id + ' - ' + response.data[i].designation_name + '</p> </div> </div></td>' +

                '<td class="text-center">' +
                response.data[i].branch_name +
                "</td>" +

                '<td class="text-center">' +
                response.data[i].department_name+

                "</td>" +
                '<td class="text-center">' +
                response.data[i].leave_type +
                "</td>" +

                '<td class="text-center">' +
                startDate +
                "</td>" +

                '<td class="text-center text-danger">' +
                endDate +
                "</td>" +

                '<td class="text-center">' +
                totalPercent +
                "</td>" +

                '<td class="text-center"  style=" white-space: break-spaces;">' +
                response.data[i].reason +
                "</td>" +

                '<td class="text-center">' +
                status +
                "</td>";

            table.appendChild(tr);
        }

        $("#leavetable").DataTable({
            dom: "Bfrtip",
            buttons: ["excelHtml5", "pdfHtml5"],
        });
    });

    // Denied Leave Form 
    $(document).on("click", ".declined_data", function () {
        var leave_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewAllLeave_Where",
            method: "POST",
            data: {
                leave_id: leave_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#Leavedelclinedleavetype").val(response["data"][0]["leave_type"]);
                $("#editLeavedelclinedid").val(response["data"][0]["leave_id"]);
                $("#editLeavedelclinedemail").val(response["data"][0]["email"]);
                $("#Leavedelclinedapply").val(response["data"][0]["start_date"]);
                $("#Leavedelclinedfrom").val(response["data"][0]["end_date"]);

                $("#denied_leave").modal("show");
            },
        });
    });

    //Declined Leave Api 
    $("#declinedleave").submit(function (e) {
        var formData = {
            leave_type: $("input[name=Leavedelclinedleavetype]").val(),
            start_date: $("input[name=Leavedelclinedapply]").val(),
            end_date: $("input[name=Leavedelclinedfrom]").val(),
            email: $("input[name=editLeavedelclinedemail]").val(),
            reason: $("textarea[name=declineleave]").val(),
            leave_id: $("input[name=editLeavedelclinedid]").val(),
            action: $("input[name=editLeavedeclined]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "leaveAction",
            data: formData, 
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Leave";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#declinedleave [type=submit]").attr("disabled", false);
            }
        });
    });

    //Approved Leave Form Fill
    $(document).on("click", ".approved_data", function () {
        var leave_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewAllLeave_Where",
            method: "POST",
            data: {
                leave_id: leave_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editleavetypeid").val(response["data"][0]["leave_id"]);
                $("#editUserId").val(response["data"][0]["user_id"]);
                $("#Leaveapprovedapply").val(response["data"][0]["start_date"]);
                $("#Leaveapprovedfrom").val(response["data"][0]["end_date"]);
                $("#Leaveapprovedtype").val(response["data"][0]["leave_type"]);
                $("#Leavedays").val(response["data"][0]["days"]);
                $("#approved_leave").modal("show");
            },
        });
    });

    //Approved Leave form
    $("#editleave").submit(function (e) {
        var formData = {
            leave_type: $("input[name=Leaveapprovedtype]").val(),
            start_date: $("input[name=Leaveapprovedapply]").val(),
            end_date: $("input[name=Leaveapprovedfrom]").val(),
            user_id: $("input[name=editUserId]").val(),
            leave_id: $("input[name=editleavetypeid]").val(),
            days: $("input[name=Leavedays]").val(),
            action: $("input[name=editleaveaction]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "leaveAction",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Leave";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editleave [type=submit]").attr("disabled", false);
            }
        });
    });

    // Leave Denied  Validation
    $(document).ready(function () {
        $("#leavedeniedbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;

            if ($("#declineleave").val().trim() == "") {
                $("#declineleavevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#declineleavevalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
    
</script>
