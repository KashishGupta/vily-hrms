<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>ReimbursementSelfList">Reimbursement</a></li>
                <li class="breadcrumb-item active">Add Reimbursement</li>
            </ul>
        </div>
        
        <div class="card mt-3">
            <form id="addtravelling" method="POST" action="#">
                <div class="card-header">
                    <h3 class="card-title"><strong>Travelling</strong></h3>
                </div>
                <div class="card-body row">
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Starting City<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addstartcityvalidation"></span>
                        <input name="addstartcity" id="addstartcity" class="form-control" type="text" placeholder="Please enter starting city." autocomplete="off" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Ending City<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addendcityvalidation"></span>
                        <input name="addendcity" id="addendcity" class="form-control" type="text" placeholder="Please enter ending city." autocomplete="off" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Departure Date<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="adddeparturedatevalid"></span>
                        <input data-provide="datepicker" data-date-autoclose="true" name="addstartdate" id="addstartdate" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly="" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Departure Time<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="adddeparturetimevalid"></span>
                        <input name="addstarttime" id="addstarttime" class="form-control" type="time" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Arrival Date<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addarrivaldatevalid"></span>
                        <input data-provide="datepicker" data-date-autoclose="true" name="addenddate" id="addenddate" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Arrival Time<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addendtimevalidation"></span>
                        <input name="addendtime" id="addendtime" class="form-control" type="time" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Mode of Journey<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addjourneytypevalid"></span>
                        <select class="custom-select form-control" onchange="GetSelectedTextValue(this)" name="addmodejourney" id="addmodejourney" />
                        <option selected="false" disabled="" value="">Please Select Mode of Journey. </option>   
                            <?php
                            $query = $this->db->query("SELECT * FROM `reimbursement_policy_travel`");
                            $arrProjectsData = $query->result();
                            foreach($arrProjectsData as $team) { ?>         
                            <option value="<?php echo $team->id; ?>"><?php echo $team->type; ?></option>
                            <?php } ?>
                            >
                        </select>
                        <input name="addperkm" hidden id="addperkm" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter travelling amount." autocomplete="off" readonly=""/>
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Per KM Price<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addperkmpricevalid"></span>
                        <input name="addperkm1" id="addperkm1" class="form-control" type="text" value="" autocomplete="off" readonly="" />
                    </div>
                   
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>KM<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addkmvalid"></span>
                        <input name="addkm" id="addkm" class="form-control" type="text" placeholder="Please enter km. eg. 8" autocomplete="off" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Amount<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="net_salaryvalid"></span>
                        <input name="net_salary" id="net_salary" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter amount." autocomplete="off" readonly="" />
                    </div>
                  
                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                        <label>File <span class="text-danger"> </span></label>
                        <input type="file" name="file" id="file" class="dropify form-control" onchange="loading_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                        <input type="hidden" id="23arrdata" />
                    </div>



                    <!-- <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Starting City<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addstartcityvalidation2"></span>
                        <input name="addstartcity1" id="addstartcity2" class="form-control" type="text" placeholder="Please enter starting city." autocomplete="off" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Ending City<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addendcityvalidation1"></span>
                        <input name="addendcity1" id="addendcity1" class="form-control" type="text" placeholder="Please enter ending city." autocomplete="off" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Departure Date<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="adddeparturedatevalid1"></span>
                        <input data-provide="datepicker" data-date-autoclose="true" name="addstartdate1" id="addstartdate1" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly="" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Departure Time<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="adddeparturetimevalid1"></span>
                        <input name="addstarttime1" id="addstarttime1" class="form-control" type="time" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Arrival Date<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addarrivaldatevalid1"></span>
                        <input data-provide="datepicker" data-date-autoclose="true" name="addenddate1" id="addenddate1" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Arrival Time<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addendtimevalidation1"></span>
                        <input name="addendtime1" id="addendtime1" class="form-control" type="time" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Mode of Journey<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addjourneytypevalid"></span>
                        <select class="custom-select form-control" onchange="GetSelectedTextValue(this)" name="addmodejourney" id="addmodejourney" />
                        <option selected="false" disabled="" value="">Please Select Mode of Journey. </option>   
                            <?php
                            $query = $this->db->query("SELECT * FROM `reimbursement_policy_travel`");
                            $arrProjectsData = $query->result();
                            foreach($arrProjectsData as $team) { ?>         
                            <option value="<?php echo $team->id; ?>"><?php echo $team->type; ?></option>
                            <?php } ?>
                            >
                        </select>
                        <input name="addperkm" hidden id="addperkm" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter travelling amount." autocomplete="off" readonly=""/>
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Per KM Price<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addperkmpricevalid1"></span>
                        <input name="addperkm11" id="addperkm11" class="form-control" type="text" value="" autocomplete="off" readonly="" />
                    </div>
                   
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>KM<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="addkmvalid1"></span>
                        <input name="addkm1" id="addkm1" class="form-control" type="text" placeholder="Please enter km. eg. 8" autocomplete="off" />
                    </div>
                    <div class="form-group col-lg-3 col-md-6 col-sm-12">
                        <label>Amount<span class="text-danger"> *</span></label><br>
                        <span class="text-danger change-pos" id="net_salaryvalid"></span>
                        <input name="net_salary" id="net_salary" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter amount." autocomplete="off" readonly="" />
                    </div>
                  
                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                        <label>File <span class="text-danger"> </span></label>
                        <input type="file" name="file" id="file" class="dropify form-control" onchange="loading_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                        <input type="hidden" id="23arrdata" />
                    </div> -->
                </div>

                <div class="card-header">
                    <h3 class="card-title"><strong>Allowance</strong></h3>
                </div>

                <div  class="card-body col-md-12"><!-- id="allowance" -->
                    <div class="row claerfix">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Name<span class="text-danger"> </span></label>
                            <input name="addallownacename" id="addallownacename" class="form-control" type="text" placeholder="Please enter name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Period Of Stay From<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="addstaydatefrom" id="addstaydatefrom" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Period Of Stay To<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="adddstaydateto" id="adddstaydateto" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>

                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Amount<span class="text-danger"> </span></label>
                            <input name="addallownaceamount" id="addallownaceamount" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter amount." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>File<span class="text-danger"> </span></label>
                            <input type="file" id="223file" name="223file" class="dropify form-control" onchange="addload_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="22arrdata" />
                        </div>

                        <!-- <div class="form-group col-md-12">
                            <button type="button" class="btn btn-primary btn-sm btn-icon icon-left" onClick="add_allowance()"><i class="fa fa-plus mr-2"></i> Add New Allowance </button>
                        </div> -->
                    </div>
                </div>

                <!-- <div id="allowance_input" class="card-body col-md-12">
                    <div class="row claerfix">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Name<span class="text-danger"> </span></label>
                            <input name="addallownacename[]" id="addallownacename" class="form-control" type="text" placeholder="Please enter name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Period Of Stay From<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="addstaydatefrom[]" id="addstaydatefrom" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Period Of Stay To<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="adddstaydateto[]" id="adddstaydateto" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>

                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Amount<span class="text-danger"> </span></label>
                            <input name="addallownaceamount[]" id="addallownaceamount" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter amount." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>File<span class="text-danger"> </span></label>
                            <input type="file" id="223file" name="223file[]" class="dropify form-control" onchange="addload_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="22arrdata" />
                        </div>
                        <div class="form-group col-md-1 col-sm-12 button-open">
                            <button type="button" class="btn btn-danger" id="allowance_amount_delete" onclick="deleteAllowanceParentElement(this)"> <i class="fa fa-trash-alt"></i> </button>
                        </div>
                    </div>
                </div> -->

                <div class="card-header">
                    <h3 class="card-title"><strong>Expense</strong></h3>
                </div>
                <div class="card-body col-md-12"><!-- id="expense"  -->
                    <div class="row claerfix">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Date<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="adddexpensesdate" id="adddexpensesdate" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Amount<span class="text-danger"> </span></label>
                            <input name="addexpenseamount" id="addexpenseamount" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter amount." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>City<span class="text-danger"> </span></label>
                            <input name="addexpensescity" id="addexpensescity" class="form-control" type="text" placeholder="Please enter city name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Details<span class="text-danger"> </span></label>
                            <input name="addexpensesdetails" id="addexpensesdetails" class="form-control" type="text" placeholder="Please enter details." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>File<span class="text-danger"> </span></label>
                            <input type="file" id="224file" name="224file" class="dropify form-control" onchange="load_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="29arrdata" />
                        </div>
                        <!-- <div class="form-group col-md-12">
                            <button type="button" class="btn btn-primary btn-sm btn-icon icon-left" onClick="add_expense()"><i class="fa fa-plus mr-2"></i> Add New Expense </button>
                        </div> -->
                    </div>
                </div>

                <!-- <div id="expense_input" class="card-body col-md-12">
                    <div class="row claerfix">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Date<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="adddexpensesdate[]" id="adddexpensesdate" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Amount<span class="text-danger"> </span></label>
                            <input name="addexpenseamount[]" id="addexpenseamount" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter amount." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>City<span class="text-danger"> </span></label>
                            <input name="addexpensescity[]" id="addexpensescity" class="form-control" type="text" placeholder="Please enter city name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Details<span class="text-danger"> </span></label>
                            <input name="addexpensesdetails[]" id="addexpensesdetails" class="form-control" type="text" placeholder="Please enter details." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>File<span class="text-danger"> </span></label>
                            <input type="file" id="223file" name="223file[]" class="dropify form-control" onchange="load_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="29arrdata" />
                        </div>
                        <div class="form-group col-md-1 col-sm-12 button-open">
                            <button type="button" class="btn btn-danger" onclick="deleteExpenseParentElement(this)"> <i class="fa fa-trash-alt"></i> </button>
                        </div>
                    </div>
                </div> -->

                <div class="card-header">
                    <h3 class="card-title"><strong>Other</strong></h3>
                </div>
                <div  class="card-body col-md-12"><!-- id="other" -->
                    <div class="row claerfix">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Receipt No.<span class="text-danger"> </span></label>
                            <input name="addotherreceipt" id="addotherreceipt" class="form-control" type="text" placeholder="Please enter receipt no." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Date<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="addotherdate" id="addotherdate" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Amount<span class="text-danger"> </span></label>
                            <br />
                            <input name="addotheramount" id="addotheramount" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter other amount." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Details<span class="text-danger"> </span></label>
                            <input name="addothersdetails" id="addothersdetails" class="form-control" autocomplete="off" type="text" placeholder="Please enter details."/>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>File<span class="text-danger"> </span></label>
                            <input type="file" id="225file" name="225file" class="dropify form-control" onchange="reimload_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="30arrdata" />
                        </div>
                        <!-- <div class="form-group col-md-12">
                            <button type="button" class="btn btn-primary btn-sm btn-icon icon-left" onClick="add_other()"><i class="fa fa-plus mr-2"></i> Add New Other </button>
                        </div> -->
                    </div>
                </div>

                <!-- <div id="other_input" class="card-body col-md-12">
                    <div class="row claerfix">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Receipt No.<span class="text-danger"> </span></label>
                            <input name="addotherreceipt[]" id="addotherreceipt" class="form-control" type="text" placeholder="Please enter receipt no." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Date<span class="text-danger"> </span></label>
                            <input data-provide="datepicker" data-date-autoclose="true" name="addotherdate[]" id="addotherdate" class="form-control" type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Amount<span class="text-danger"> </span></label>
                            <br />
                            <input name="addotheramount[]" id="addotheramount" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Please enter other amount." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Details<span class="text-danger"> </span></label>
                            <input name="addothersdetails[]" id="addothersdetails" class="form-control" autocomplete="off" type="text" placeholder="Please enter details."/>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>File<span class="text-danger"> </span></label>
                            <input type="file" id="225file" name="225file[]" class="dropify form-control" onchange="reimload_file();" data-allowed-file-extensions="pdf doc docx jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="30arrdata" />
                        </div>
                        <div class="form-group col-md-1 col-sm-12 button-open">
                            <button type="button" class="btn btn-danger" onclick="deleteOtherParentElement(this)"> <i class="fa fa-trash-alt"></i> </button>
                        </div>
                    </div>
                </div> -->

                <div class="card-body row">
                    <div class="form-group col-sm-12 text-right">
                        <button id="addtravellingbutton" class="btn btn-success submit-btn">Submit</button>
                        <button class="btn btn-secondary" id="cancelform" type="reset">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--/Details of Reimbursement-->


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">

$('#allowance_input').hide();
    
    // CREATING BLANK ALLOWANCE INPUT
    var blank_allowance = '';
    $(document).ready(function () {
        blank_allowance = $('#allowance_input').html();
    });
    function add_allowance()
{
    $("#allowance").append(blank_allowance);
}
    // REMOVING ALLOWANCE INPUT
function deleteAllowanceParentElement(n) {
    n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
}



$('#expense_input').hide();
    
    // CREATING BLANK ALLOWANCE INPUT
    var blank_expense = '';
    $(document).ready(function () {
        blank_expense = $('#expense_input').html();
    });
    function add_expense()
{
    $("#expense").append(blank_expense);
}
    // REMOVING ALLOWANCE INPUT
function deleteExpenseParentElement(n) {
    n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
}


$('#other_input').hide();
    
    // CREATING BLANK ALLOWANCE INPUT
    var blank_other = '';
    $(document).ready(function () {
        blank_other = $('#other_input').html();
    });
    function add_other()
{
    $("#other").append(blank_other);
}
    // REMOVING ALLOWANCE INPUT
function deleteOtherParentElement(n) {
    n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
}




    // Active or inactive ticket form fill
    function GetSelectedTextValue(status) {
        var selectedText = status.options[status.selectedIndex].innerHTML;
        var status = status.value;
        //alert(status);
        req={};
        req.travel_id = status;
        // console.log(req);
        $.ajax({
            type: "POST",
            url: base_url + "viewReimbursementTravelPolicy",
            data: req,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        .done(function (response) {
            console.log(response);

            if(response.data == 0){
                $("#addperkm").val(response.data);
                        
            }else{
                $("#addperkm").val(response["data"][0]["per_km"]);
                $("#addperkm1").val(response["data"][0]["per_km"]);
               
            }
        });
    }

    $(document).ready(function () {
        $("#addkm").keyup(function () {
            var address = $(this).val();
            net_salary = parseInt($('#addperkm').val()) * parseInt(address);
            $('#net_salary').attr('value', net_salary);
        });
    });
    //for remove validation  empty input field
        $(document).ready(function() {
            $('#cancelform').click(function(){
                jQuery('#addstartcityvalidation').text('');
                jQuery('#addendcityvalidation').text('');
                jQuery('#adddeparturedatevalid').text('');
                jQuery('#adddeparturetimevalid').text('');
                jQuery('#addarrivaldatevalid').text('');
                jQuery('#addendtimevalidation').text('');
                jQuery('#addjourneytypevalid').text('');
                jQuery('#addkmvalid').text('');
                jQuery('#net_salaryvalid').text('');
            });
        });

    // Image upload
        function loading_file() {
            if (!window.FileReader) {
                return alert("FileReader API is not supported by your browser.");
            }
            var $i = $("#file"), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if (input.files && input.files[0]) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    $("#23arrdata").val(fr.result);
                };
                //fr.readAsText( file );
                fr.readAsDataURL(file);
            } else {
                // Handle errors here
                alert("File not selected or browser incompatible.");
            }
        }
    // Image upload
        function addload_file() {
            if (!window.FileReader) {
                return alert("FileReader API is not supported by your browser.");
            }
            var $i = $("#223file"), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if (input.files && input.files[0]) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    $("#22arrdata").val(fr.result);
                };
                //fr.readAsText( file );
                fr.readAsDataURL(file);
            } else {
                // Handle errors here
                alert("File not selected or browser incompatible.");
            }
        }

    // Image upload
        function load_file() {
            if (!window.FileReader) {
                return alert("FileReader API is not supported by your browser.");
            }
            var $i = $("#224file"), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if (input.files && input.files[0]) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    $("#29arrdata").val(fr.result);
                };
                //fr.readAsText( file );
                fr.readAsDataURL(file);
            } else {
                // Handle errors here
                alert("File not selected or browser incompatible.");
            }
        }

    // Image upload
        function reimload_file() {
            if (!window.FileReader) {
                return alert("FileReader API is not supported by your browser.");
            }
            var $i = $("#225file"), // Put file input ID here
                input = $i[0]; // Getting the element from jQuery
            if (input.files && input.files[0]) {
                file = input.files[0]; // The file
                fr = new FileReader(); // FileReader instance
                fr.onload = function () {
                    // Do stuff on onload, use fr.result for contents of file
                    $("#30arrdata").val(fr.result);
                };
                //fr.readAsText( file );
                fr.readAsDataURL(file);
            } else {
                // Handle errors here
                alert("File not selected or browser incompatible.");
            }
        }


    //add Travelling form
        $("#addtravelling").submit(function (e) {
            var formData = {
                starting_city: $("input[name=addstartcity").val(),
                end_city: $("input[name=addendcity").val(),
                start_time: $("input[name=addstarttime]").val(),
                end_time: $("input[name=addendtime]").val(),
                mode_of_journey: $("select[name=addmodejourney]").val(),
                //per_km: $("input[name=addperkmprice]").val(),
                start_date: $("input[name=addstartdate]").val(),
                end_date: $("input[name=addenddate]").val(),
                amount: $("input[name=net_salary]").val(),
                image: $("#23arrdata").val(),


                hotel_name: $("input[name=addallownacename").val(),
                departure_date: $("input[name=adddstaydateto]").val(),
                arrival_date: $("input[name=addstaydatefrom]").val(),
                hotel_amount: $("input[name=addallownaceamount]").val(),
                hotel_bill: $("#22arrdata").val(),


                food_date: $("input[name=adddexpensesdate]").val(),
                food_amount:     $("input[name=addexpenseamount]").val(),
                food_name: $("input[name=addexpensesdetails").val(),
                city: $("input[name=addexpensescity]").val(),
                food_bill: $("#29arrdata").val(),


                employee_amount: $("input[name=addotheramount]").val(),
                bill_name:     $("input[name=addotherreceipt]").val(),
                other_details : $("input[name=addothersdetails").val(),
                other_date : $("input[name=addotherdate").val(),
                other_image: $("#30arrdata").val(),
            };


            e.preventDefault();
            $.ajax({
                type: "POST",
                url: base_url + "addReimbursement",
                data: formData,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
            console.log(response);
               var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                        setTimeout(function(){window.location ="<?php echo base_url()?>ReimbursementSelfList"},1000);
                  
                } else {
                    toastr.error(falsedata);
                    $('#addtravelling [type=submit]').attr('disabled',false);
                }
               
            });
        });

    //Add Details of Travelling Validation
       $(document).ready(function () {
            $("#addtravellingbutton").click(function (e) {
                e.stopPropagation();
                var errorCount = 0;
                if ($("#addstartcity").val().trim() == "") {
                    $("#addstartcity").focus();
                    $("#addstartcityvalidation").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#addstartcityvalidation").text("");
                }

                if ($("#addendcity").val().trim() == "") {
                    $("#addendcity").focus();
                    $("#addendcityvalidation").text("This field can't be empty.");
                    errorCount++;
                } else {
                    $("#addendcityvalidation").text("");
                }

                if ($("#addstartdate").val().trim() == "") {
	                $("#adddeparturedatevalid").text("This field can't be empty.");
	                errorCount++;
                } else {
                	$("#adddeparturedatevalid").text("");
                }

                if ($("#addstarttime").val().trim() == "") {
	                $("#adddeparturetimevalid").text("This field can't be empty.");
	                errorCount++;
                } else {
                	$("#adddeparturetimevalid").text("");
                }

                if ($("#addenddate").val().trim() == "") {
	                $("#addarrivaldatevalid").text("This field can't be empty.");
	                errorCount++;
                } else {
                	$("#addarrivaldatevalid").text("");
                }

                if ($("#addendtime").val().trim() == "") {
	                $("#addendtimevalidation").text("This field can't be empty.");
	                errorCount++;
                } else {
                	$("#addendtimevalidation").text("");
                }

                if ($("#addmodejourney").val().trim() == "") {
	                $("#addjourneytypevalid").text("This field can't be empty.");
	                errorCount++;
                } else {
                	$("#addjourneytypevalid").text("");
                }

                if ($("#addkm").val().trim() == "") {
	                $("#addkmvalid").text("This field can't be empty.");
	                errorCount++;
                } else {
                	$("#addkmvalid").text("");
                }
                if ($("#net_salary").val().trim() == "") {
	                $("#net_salaryvalid").text("This field can't be empty.");
	                errorCount++;
                } else {
                	$("#net_salaryvalid").text("");
                }
               
                
                if (errorCount > 0) {
                    return false 
                }
            });
        });

</script>
