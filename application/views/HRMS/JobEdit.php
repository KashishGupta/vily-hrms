<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Jobs">Jobs</a></li>
                <li class="breadcrumb-item active">Edit Job</li>
            </ul>
        </div>
        
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Edit Job</strong></h3>
            </div>
            <div class="card-body">
                <form id="editjobs" method="POST">
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Branch<span class="text-danger"> *</span></label><br>
                            <span id="editbranchvalidate" class="text-danger change-pos"></span>
                            <input value="" id="editjobid" name="editjobid" class="form-control" type="hidden">
                            <select class="custom-select form-control" name="editbranchid" id="editbranchid">
                                
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Department<span class="text-danger"> *</span></label><br>
                            <span id="editdepartmentvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="editdepartmentid" id="editdepartmentid">
                                
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Job Title<span class="text-danger"> *</span></label><br>
                            <span id="editjobtitlevalidate" class="text-danger change-pos"></span>
                            <input type="text"  class="form-control" name="editjobtitle" id="editjobtitle" placeholder="Please enter job type." >
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Job Type<span class="text-danger"> *</span></label><br>
                            <span id="editjobtypevalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="editjobtypeid" id="editjobtypeid">
                                
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Key Skills<span class="text-danger"> *</span></label><br>
                            <span id="editkeyskillvalidate" class="text-danger change-pos"></span>
                            <input type="text" class="form-control" name="editkeyskill" id="editkeyskill" placeholder="Please enter key skills.">
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Job Active Date<span class="text-danger"> *</span></label><br>
                            <span id="editstartdatevalidate" class="text-danger change-pos"></span>
                            <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="editstartdate" id="editstartdate" readonly>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Job Inactive Date<span class="text-danger"> *</span></label><br>
                            <span id="editenddatevalidate" class="text-danger change-pos"></span>
                            <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="editenddate" id="editenddate" readonly>
                        </div>
                        <div class="form-group  col-lg-6 col-md-12 col-sm-12">
                            <label>No of Vacancies<span class="text-danger"> *</span></label><br>
                            <span id="editnoofvacanciesvalidate" class="text-danger change-pos"></span>
                            <input class="form-control" name="editnoofvacancies" id="editnoofvacancies" min="1" max="100" type="number" onkeypress="return onlyNumberKey(event)">
                        </div>
                        <div class="form-group col-md-12 col-sm-12">
                            <label>Description<span class="text-danger"> *</span></label><br>
                            <span id="editdescriptionvalidate" class="text-danger change-pos"></span>
                            <textarea class="form-control summernote" name="editdescription" id="editdescription" autocomplete="off"  /></textarea>
                        </div>
                       
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editjobbutton" class="btn btn-success submit-btn">Update Changes</button> 
                        <button type="reset" id="test" class="btn btn-secondary">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>            
</div>  

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script>
    //Fetech Branch              
        function fetchBranch(branch_id) {
            $.ajax({  
                url: base_url+"viewactiveBranch",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
              let dropdown = $('#editbranchid');
    
                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Branch</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(branch_id == entry.id)
                        {
                            dropdown.append($('<option selected="true"></option>').attr('value', entry.id).text(entry.branch_name));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.branch_name));
                        }
                    })
            }); 
        }
    
    //Fetch Department                
        function fetchDepartment(department_id) {
            $.ajax({  
                url: base_url+"viewDepartmentSelect",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
                let dropdown = $('#editdepartmentid');
    
                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Department</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(department_id == entry.department_id)
                        {
                            dropdown.append($('<option selected></option>').attr('value', entry.department_id).text(entry.department_name));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.department_id).text(entry.department_name));
                        }
                    })
            }); 
        }

    //Fetch Job type                
        function fetchJobType(job_type_id)
        {
            $.ajax({  
                url: base_url+"viewJobType",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
                let dropdown = $('#editjobtypeid');

                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Jobtype</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(job_type_id == entry.id)
                        {
                            dropdown.append($('<option selected></option>').attr('value', entry.id).text(entry.job_type));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.job_type));
                        }
                    })
            }); 
        }
    //Fetch Job type                
    function fetcheducationType(education_type)
        {
            $.ajax({  
                url: base_url+"viewJobEducation",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
                let dropdown = $('#editeducationtype');

                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Educationtype</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(education_type == entry.type)
                        {
                            dropdown.append($('<option selected></option>').attr('value', entry.type).text(entry.type));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.type).text(entry.type));
                        }
                    })
            }); 
        }

    // Edit  Designation Form Fill */
        var job_id = <?php echo $jobid; ?>;
        //alert(job_id);
            $.ajax({
                url: base_url+"viewJob",
                data:{job_id:job_id}, 
                type: "POST",
                dataType    : 'json', 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                },
          
               success:function(response){
           
                        var branch_id = response["data"][0]["branch_id"];
                        var department_id = response["data"][0]["department_id"];
                        var job_type_id = response["data"][0]["job_type_id"];
                        var education_type = response["data"][0]["education_type"];
                        fetchBranch(branch_id); 
                        fetchDepartment(department_id);
                        fetchJobType(job_type_id);
                        fetcheducationType(education_type);
                        $('#editjobtitle').val(response["data"][0]["job_title"]); 
                        $('#editkeyskill').val(response["data"][0]["key_skill"]); 
                        $('#editstartdate').val(response["data"][0]["start_date"]); 
                        $('#editenddate').val(response["data"][0]["end_date"]); 
                        $("#editdescription").summernote("code", response["data"][0]["job_description"]); 
                        $('#editjobid').val(response["data"][0]["job_id"]);
                        $('#editnoofvacancies').val(response["data"][0]["no_of_post"]);
                        $('#edit_job').modal('show');  
                    }  
                });  
            
                $("#editbranchid").change(function() {
                    $.ajax({
                        url: base_url+"viewDepartment",
                        data: { 'branch_id':  $('select[name=editbranchid]').val() },
                        type: "POST",
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true,
                        beforeSend: function(xhr){xhr.setRequestHeader('Token', localStorage.token);}
                    })
                 
                 
                    .done(function(response) 
                    { 
                        let dropdown = $('#editdepartmentid');
            
                        dropdown.empty();
                        
                        dropdown.append('<option selected="true" disabled>Choose Department</option>');
                        dropdown.prop('selectedIndex', 0);
                        
                        // Populate dropdown with list of provinces
                            $.each(response.data, function (key, entry) {
                                dropdown.append($('<option></option>').attr('value', entry.department_id).text(entry.department_name));
                            })
                    });
                });
                 
                $("#editdepartmentid").change(function() {
                    $.ajax({
                        url: base_url+"viewDesignation",
                        data: { 'department_id':  $('select[name=editdepartmentid]').val() },
                        type: "POST",
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true,
                        beforeSend: function(xhr){xhr.setRequestHeader('Token', localStorage.token);}
                    })
                    
                    
                    .done(function(response) 
                    { 
                        let dropdown = $('#editdesignationid');
            
                        dropdown.empty();
                        
                        dropdown.append('<option selected="true" disabled>Choose Desigination</option>');
                        dropdown.prop('selectedIndex', 0);
                        
                        // Populate dropdown with list of provinces
                            $.each(response.data, function (key, entry) {
                                dropdown.append($('<option></option>').attr('value', entry.designation_id).text(entry.designation_name));
                            })
                    });
                });
             
               // Edit Project form*/
               $("#editjobs").submit(function(e) {
                var formData = {
                        'job_id'              :  $('input[name=editjobid]').val(),
                        'department_id'      :  $('select[name=editdepartmentid]').val(),
                        'education_type'      :  $('select[name=editeducationtype').val(),
                        'job_title'          :  $('input[name=editjobtitle]').val(),
                        'key_skill'            :  $('input[name=editkeyskill]').val(),
                        'job_type_id'         :  $('select[name=editjobtypeid]').val(),
                        'start_date'          :  $('input[name=editstartdate]').val(),
                        'end_date'            :  $('input[name=editenddate]').val(),
                        'no_of_post'          :  $('input[name=editnoofvacancies]').val(),
                        'job_description'     :  $('textarea[name=editdescription]').val()
                    };

                       e.preventDefault();
                       $.ajax({
                           type        : 'POST',
                           url         : base_url+'editJob',
                           data        : formData, // our data object
                           dataType    : 'json',
                           encode      : true,
                           beforeSend: function(xhr){
                               xhr.setRequestHeader('Token', localStorage.token);
                           }
                       })
                       .done(function(response) {
                           console.log(response);

                           var jsonDa = response
                           var jsonData = response["data"]["message"];
                           var falsedata = response["data"];

                           if (jsonDa["data"]["status"] == "1") {
                           toastr.success(jsonData);
                               setTimeout(function(){window.location ="<?php echo base_url()?>Jobs"},1000);

                           }

                           else {
                               toastr.error(falsedata);
                                   $('#editjobs [type=submit]').attr('disabled',false);
                           }
                       });
                   });
               

        //Add Job Validation
        $(document).ready(function() {
            $("#editjobbutton").click(function(e) {
            e.stopPropagation();
                var errorCount = 0;
               if ($("#editbranchid").val() == null) {
                    $("#editbranchid").focus();
                    $("#editbranchvalidate").text("Please select a branch.");
                    errorCount++;
                } else {
                    $("#editbranchvalidate").text("");
                }
                if ($("#editdepartmentid").val() == null) {
                    $("#editdepartmentid").focus();
                    $("#editdepartmentvalidate").text("Please select a department.");
                    errorCount++;
                } else {
                    $("#editdepartmentvalidate").text("");
                }

                if ($("#editjobtitle").val().trim() == '') {
                    $("#editjobtitlevalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editjobtitlevalidate").text("");
                }

                if ($("#editjobtypeid").val() == null) {
                    $("#editjobtypeid").focus();
                    $("#editjobtypevalidate").text("Please select a Jobe type.");
                    errorCount++;
                } else {
                    $("#editjobtypevalidate").text("");
                }

                if ($("#editkeyskill").val().trim() == '') {
                    $("#editkeyskillvalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editkeyskillvalidate").text("");
                }
                
                if ($("#editstartdate").val().trim() == '') {
                    $("#editstartdatevalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editstartdatevalidate").text("");
                }
                 if ($("#editenddate").val().trim() == '') {
                    $("#editenddatevalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editenddatevalidate").text("");
                }
                 if ($("#editnoofvacancies").val().trim() == '') {
                    $("#editnoofvacanciesvalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editnoofvacanciesvalidate").text("");
                }
                 if ($("#editdescription").val().trim() == '') {
                    $("#editdescriptionvalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#editdescriptionvalidate").text("");
                }


                if ($("#editstartdate").val() > $("#editenddate").val()) {
                        $("#editstartdate").focus();
                        $("#editenddatevalidate").text("End Date Should be ahead From Start Date.");
                        errorCount++;
                    } 
                    else {
                        $("#editenddatevalidate").text("");
                    }
                    var today = new Date();
                    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                    var test = new Date($("#editstartdate").val());
                  
                     if (test  < today) {
                        $("#editstartdatevalidate1").text("Job Cannot Be Applied On Past Date.");
                        errorCount++;
                    } 
                    else {
                        $("#editstartdatevalidate1").text("");
                    }
               
                    
                if(errorCount > 0){
                    return false;
                }
            });
        });
</script>