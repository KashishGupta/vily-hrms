<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Off Boarding Complete</li>
            </ul>
            <div class="header-action mt-3">
                <a href="<?php echo base_url(); ?>offBoardingComplete_emp" class="btn btn-info grid-system"><i class="fe fe-list"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid mt-3">
        <div class="row clearfix kt-widget__items"></div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Show Employee
    $.ajax({
        url: base_url + "viewOffboardingUserCompleted",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-md-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Offboarding</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
                var html =
                    '<div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-4"><div class="profile-widget"><div class="profile-img"><img class="rounded-circle img-thumbnail w100 h100" src="' +
                    UserImage +
                    '" alt=""></div><div class="dropdown profile-action"><a href="#" class="action-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-156px, 18px, 0px);"><a class="dropdown-item" href="<?php echo base_url(); ?>offBoardingView/' +
                    response.data[i].id +
                    '"><i class="fa fa-edit"></i>View Letter</a></div></div><h4 class="user-name mt10 text-ellipsis"><a href="<?php echo base_url(); ?>offBoardingView/' +
                    response.data[i].id +
                    '">' +
                    response.data[i].first_name +
                    '</a></h4><div class="small text-muted"><span>' +
                    response.data[i].emp_id +
                    "</span>- Web Designer</div><span>" +
                    response.data[i].email +
                    "</span><br><span>" +
                    response.data[i].phone +
                    "</span></br></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });
</script>
