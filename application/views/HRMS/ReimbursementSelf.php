<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Hr/Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Reimbursement</li>
            </ul>
            <div class="header-action mt-3">
                <a class="btn btn-primary" href="<?php echo base_url(); ?>ReimbursementSelfAdd"><i class="fe fe-plus mr-2"></i>Add Reimbursement</a>
            </div>
        </div>
    </div>
</div>

<div class="section-body mt-3">
    <div class="container-fluid ">
    	<div class="row clearfix kt-widget__items"></div>
    	
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Show Employee
    $.ajax({
        url: base_url + "viewTourEmployee",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-xl-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Reimbursement</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                statusemp = "";

                if (response.data[i].status == 1) {
                    statusemp = "Approved";
                }
                if (response.data[i].status == 0) {
                    statusemp = "Pending";
                }
                if (response.data[i].status == 2) {
                    statusemp = "Declined";
                }
                var touramount = response.data[i].amount;
                var hotelamount = response.data[i].hotel_amount;
                var otheramount = response.data[i].employee_amount;
                var foodamount = response.data[i].food_amount;

                var totalamount = Number(touramount) + Number(hotelamount) + Number(otheramount) + Number(foodamount);
                var adminamount = response.data[i].fixed_amount;
                var adminhotelamount = response.data[i].clear_amount;
                var adminotheramount = response.data[i].remarked_amount;
                var adminfoodamount = response.data[i].admin_amount;

                var totaladminamount = Number(adminamount) + Number(adminhotelamount) + Number(adminotheramount) + Number(adminfoodamount);
                var startDate = new Date(response.data[i].start_date);
                var dd = String(startDate.getDate()).padStart(2, "0");
                var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = startDate.getFullYear();
                startDate = dd + "-" + mm + "-" + yyyy;
                var endDate = new Date(response.data[i].end_date);
                var dd = String(endDate.getDate()).padStart(2, "0");
                var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = endDate.getFullYear();
                endDate = dd + "-" + mm + "-" + yyyy;
                var arrivactiondate = "";
                if (response.data[i].start_date == "0000-00-00") {
                    arrivactiondate = "Nil";
                } else {
                    var start = startDate;
                    arrivactiondate = start;
                }
                var arrivalEnddate = "";
                if (response.data[i].end_date == "0000-00-00") {
                    arrivalEnddate = "Nil";
                } else {
                    var start = endDate;
                    arrivalEnddate = start;
                }
                var html =
                    '<div class="col-xl-4 col-lg-4 col-md-6"><div class="card"><div class="card-body text-center ribbon"><div class="ribbon-box green">' +
                    statusemp +
                    '</div><h6 class="mt-2 mb-0">' +
                    response.data[i].first_name +
                    "</h6><span>" +
                    response.data[i].email +
                    '</span><ul class="mb-2 list-unstyled d-flex justify-content-center"><li>' +
                    response.data[i].emp_id +
                    "</li><li>" +
                    response.data[i].branch_name +
                    '</li></ul><a href="<?php echo base_url(); ?>ReimbursementSelfView/' +
                    response.data[i].id +
                    '" class="btn btn-info">View Details</a><div class="row text-center mt-3"><div class="col-lg-6 border-right"><label class="mb-0">Emp Amount</label><h4 class="font-18">' +
                    totalamount +
                    '</h4></div><div class="col-lg-6"><label class="mb-0">Admin Amount</label><h4 class="font-18">' +
                    totaladminamount +
                    '</h4></div></div><div class="row text-center mt-2"><div class="col-lg-6 border-right"><label class="mb-0">Start Date</label><h4 class="font-18">' +
                    arrivactiondate +
                    '</h4></div><div class="col-lg-6"><label class="mb-0">End Date</label><h4 class="font-18">' +
                    arrivalEnddate +
                    "</h4></div></div></div></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });
</script>