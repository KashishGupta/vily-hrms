<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item active">Designation</li>
            </ul>
        </div>
 <!-- Add Designation -->
 <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Designation</strong></h3>
            </div>
            <div class="card-body">
                <form id="adddesignation" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Branch Name<span class="text-danger"> *</span></label><br />
                            <span id="addbranchvalidate" class="checkfield change-pos" style="color: red;"></span>
                            <select class="custom-select form-control" name="addbranchid" id="addbranchid" required> </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Department Name<span class="text-danger"> *</span></label><br />
                            <span id="adddepartmentvalidate" class="checkfield change-pos" style="color: red;"></span>
                            <select class="custom-select form-control" name="adddepartmenttest" id="adddepartmenttest" required> </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Designation Name<span class="text-danger"> *</span></label><br />
                            <span id="adddesignationvalidate" class="text-danger change-pos"></span>
                            <input required type="text" name="adddesignationname" id="adddesignationname" class="form-control" placeholder="Please enter designation name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 text-right">
                            <button id="adddesignationbutton" class="btn btn-success submit-btn">Submit</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Search Designation Result  -->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Designation Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="designationExample">
                    <thead>
                        <tr>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Department</th>
                            <th class="text-center">Designation</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="designationtable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Edit Designation Modal -->
<div id="edit_designation" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Designation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editdesignation" method="POST">
                    <div class="form-group col-md-12">
                        <label>Branch Name<span class="text-danger">*</span></label><br />
                        <span id="editbranchvalidate" class="text-danger change-pos"></span>
                        <select class="custom-select form-control" name="editbranchid" id="editbranchid"> </select>
                        <input value="" id="editdesignationid" name="editdesignationid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Department Name<span class="text-danger">*</span></label><br />
                        <span id="editdepartmentvalidate" class="text-danger change-pos"></span>
                        <select class="custom-select form-control" name="editdepartmentid" id="editdepartmentid"> </select>
                        <input value="" id="editdepartmennametid" name="editdepartmentnameid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Designation Name<span class="text-danger">*</span></label><br />
                        <span id="editdesignationvalidate" class="text-danger change-pos"></span>
                        <input type="text" name="editdesignationname" id="editdesignationname" class="form-control" placeholder="Please enter designation name." autocomplete="off" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editdesignationbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Designation Modal -->

<!-- Delete Designation Modal -->
<div class="modal custom-modal fade" id="delete_designation" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" class="btn btn-danger continue-btn delete_designation_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Designation Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#adddesignationname").val("");
            $("#adddepartmenttest option:eq(0)").prop("selected", true);
            $("#addbranchid option:eq(0)").prop("selected", true);

            $("#filterbranchid option:eq(0)").prop("selected", true);
            $("#filterdepartmentid option:eq(0)").prop("selected", true);
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addbranchvalidate").text("");
            jQuery("#adddepartmentvalidate").text("");
            jQuery("#adddesignationvalidate").text("");
            jQuery("#filterbranchidvalidate").text("");
            jQuery("#filterdepartmentidvalidate").text("");
            jQuery("#editbranchvalidate").text("");
            jQuery("#editdepartmentvalidate").text("");
            jQuery("#editdesignationvalidate").text("");
        });
    });

   

   

            //Show Designation list
            $.ajax({
                url: base_url + "viewDesignationEmployee",
                data: {},
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                //var j =1;
                $("#designationExample").DataTable().clear().destroy();
                var table = document.getElementById("designationtable");
                for (i in response.data) {
                    var tr = document.createElement("tr");
                    tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
                        
                        '<td class="text-center">' +
                        response.data[i].branch_name +
                        "</td>" +
                        '<td class="text-center">' +
                        response.data[i].department_name +
                        "</td>" +
                        '<td class="text-center">' +
                        response.data[i].designation_name +
                        "</td>" +
                        '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_designation" aria-expanded="false" id="' +
                        response.data[i].designation_id +
                        '" class="btn btn-info edit_data" title="Edit Designation" ><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_designation" aria-expanded="false" id="' +
                        response.data[i].designation_id +
                        '" class="btn btn-danger delete_data" title="Delete Designation"><i class="fa fa-trash-alt"></i></button></td>';
                    table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#designationExample").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Designation Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Designation Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                });
            });
      
    //Select branch For Filter
    $.ajax({
        url: base_url + "viewTokenBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#filterbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select Department by select branch For Add Designation Filter
    $("#filterbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=filterbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#filterdepartmentid");

            dropdown.empty();

            dropdown.append('<option selected="true"  required="true">Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    //Select Branch For Add Designation
    $.ajax({
        url: base_url + "viewTokenBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select Department by select branch For Add Designation
    $("#addbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#adddepartmenttest");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled required="true">Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Add Designation
    $("#adddesignation").submit(function (e) {
        var formData = {
            department_id: $("select[name=adddepartmenttest]").val(),
            designation_name: $("input[name=adddesignationname]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addDesignation",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Designation_emp";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#adddesignation [type=submit]").attr("disabled", false);
            }
        });
    });

    //Select Branch for edit designation
    function fetchBranch(branch_id) {
        $.ajax({
            url: base_url + "viewTokenBranch",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editbranchid");

            dropdown.empty();

            dropdown.append("<option disabled>Choose Branch</option>");
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                if (branch_id == entry.id) {
                    dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.branch_name));
                } else {
                    dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
                }
            });
        });
    }

    // Select Department by select branch For Edit Designation
    function fetchDepartment(department_id) {
        $.ajax({
            url: base_url + "viewDepartmentEmployee",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editdepartmentid");

            dropdown.empty();

            dropdown.append("<option disabled>Choose Department</option>");
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                if (department_id == entry.department_id) {
                    dropdown.append($("<option selected></option>").attr("value", entry.department_id).text(entry.department_name));
                } else {
                    dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
                }
            });
        });
    }

    // Edit  Department Form Fill
    $(document).on("click", ".edit_data", function () {
        var designation_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewDesignationEmployee",
            method: "POST",
            data: { designation_id: designation_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                var branch_id = response["data"][0]["branch_id"];
                var department_id = response["data"][0]["department_id"];
                fetchBranch(branch_id);
                fetchDepartment(department_id);
                $("#editdesignationid").val(response["data"][0]["designation_id"]);
                $("#editdesignationname").val(response["data"][0]["designation_name"]);
                $("#edit_designation").modal("show");
            },
        });
    });

    $("#editbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartmentEmployee",
            data: { branch_id: $("select[name=editbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editdepartmentid");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Edit Designation form
    $("#editdesignation").submit(function (e) {
        var formData = {
            department_id: $("select[name=editdepartmentid]").val(),
            designation_id: $("input[name=editdesignationid]").val(),
            designation_name: $("input[name=editdesignationname]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editDesignation",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Designation_emp";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editdesignation [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Designation
    $(document).on("click", ".delete_data", function () {
        var designation_id = $(this).attr("id");
        $(".delete_designation_button").attr("id", designation_id);
        $("#delete_designation").modal("show");
    });

    $(document).on("click", ".delete_designation_button", function () {
        var designation_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteDesignation",
            method: "POST",
            data: { designation_id: designation_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>Designation_emp";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_designation_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    //Add Designation Validation
    $(document).ready(function () {
        $("#adddesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }
            if ($("#adddepartmenttest").val() == null) {
                $("#adddepartmenttest").focus();
                $("#adddepartmentvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#adddepartmentvalidate").text("");
            }
            if ($("#adddesignationname").val().trim() == "") {
                $("#adddesignationname").focus();
                $("#adddesignationvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#adddesignationvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Edit Designation Validation
    $(document).ready(function () {
        $("#editdesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editbranchid").val() == null) {
                $("#editbranchid").focus();
                $("#editbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#editbranchvalidate").text("");
            }
            if ($("#editdepartmentid").val() == null) {
                $("#editdepartmentid").focus();
                $("#editdepartmentvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#editdepartmentvalidate").text("");
            }
            if ($("#editdesignationname").val().trim() == "") {
                $("#editdesignationname").focus();
                $("#editdesignationvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editdesignationvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Filter Designation Validation
    $(document).ready(function () {
        $("#filterdesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#filterbranchid").val() == null) {
                $("#filterbranchid").focus();
                $("#filterbranchidvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#filterbranchidvalidate").text("");
            }
            if ($("#filterdepartmentid").val() == null) {
                $("#filterdepartmentid").focus();
                $("#filterdepartmentidvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#filterdepartmentidvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
