<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mt-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">View Reimbursement</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Allowance Details </strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-md" id="travellingtable">
                    <thead>
                        <tr>
                            <th class="text-center">Start Date</th>
                            <th class="text-center">End Date</th>
                            <th class="text-center">Hotel Name</th>
                            <th class="text-center">Employee Amount</th>
                            <th class="text-center">Admin Amount</th>
                            <th class="text-center">Hotel Bill</th>
                            <th class="text-center">Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodytravelling"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Allowance Status Mpodal -->
<div id="travelling_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reimbursement Details Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-header">
                    <p>Are you sure want to Approved or Declined for this Reimbursement?</p>
                </div>
                <form id="editstatus" method="POST" action="#">
                    <input value="" id="edittravellingidstatus" name="edittravellingidstatus" class="form-control" type="hidden" />
                    <div class="form-group">
                        <label>Reimbursement Details Status<span class="text-danger"> *</span></label>
                        <br />
                        <select value="" id="edittravellingstatus" name="edittravellingstatus" class="form-control">
                            <option value="0">Pending</option>
                            <option value="1">Declined</option>
                            <option value="2">Approved</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Employee Amount</label>
                        <input value="" id="editamounttravellingidstatus" name="editamounttravellingidstatus" class="form-control" type="text" readonly="" onkeypress="return onlyNumberKey(event)" />
                    </div>
                    <div class="form-group">
                        <label>Admin Amount</label>
                        <input value="" id="editadminamounttravellingidstatus" name="editadminamounttravellingidstatus" class="form-control" type="text" onkeypress="return onlyNumberKey(event)"  />
                    </div>
                    <div class="submit-section pull-right">
                        <button class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Allowance Status Mpodal -->




<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Travelling Details</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-xl table-responsive-md" id="tourtable">
                    <thead>
                        <tr>
                            <th class="text-center">Start Time</th>
                            <th class="text-center">End Time</th>
                            <th class="text-center">Start Date</th>
                            <th class="text-center">End Date</th>
                            <th class="text-center">Mode Of Journey</th>
                            <th class="text-center">Employee Amount</th>
                            <th class="text-center">Admin Amount</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodytour"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Reimbursement Status Mpodal -->
<div id="tour_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reimbursement Details Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-header">
                    <p>Are you sure want to Approved or Declined for this Reimbursement?</p>
                </div>
                <form id="edit_tour_status" method="POST" action="#">
                    <input value="" id="edittouridstatus" name="edittouridstatus" class="form-control" type="hidden"/>
                    <div class="form-group">
                        <label>Reimbursement Details Status<span class="text-danger">*</span></label>
                        <br />
                        <select value="" id="edittourstatus" name="edittourstatus" class="form-control">
                            <option value="0">Pending</option>
                            <option value="1">Declined</option>
                            <option value="2">Approved</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Employee Amount</label>
                        <input value="" id="editamounttouridstatus" name="editamounttouridstatus" class="form-control" type="text" readonly="" onkeypress="return onlyNumberKey(event)" />
                    </div>
                    <div class="form-group">
                        <label>Admin Amount</label>
                        <input value="" id="editadminamounttouridstatus" name="editadminamounttouridstatus" class="form-control" type="text" onkeypress="return onlyNumberKey(event)"  />
                    </div>
                    <div class="submit-section pull-right">
                        <button class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Reimbursement Status Mpodal -->



<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Expense Details</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-md" id="foodtable">
                    <thead>
                        <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">City</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Employee Amount</th>
                            <th class="text-center">Admin Amount</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyfood"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Expence Status Mpodal -->
<div id="food_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reimbursement Details Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-header">
                    <p>Are you sure want to Approved or Declined for this Reimbursement?</p>
                </div>
                <form id="edit_food_status" method="POST" action="#">
                    <input value="" id="editfoodidstatus" name="editfoodidstatus" class="form-control" type="hidden" />
                    <div class="form-group">
                        <label>Reimbursement Details Status<span class="text-danger">*</span></label>
                        <br />
                        <select value="" id="editfoodstatus" name="editfoodstatus" class="form-control">
                            <option value="0">Pending</option>
                            <option value="1">Declined</option>
                            <option value="2">Approved</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Employee Amount</label>
                        <input value="" id="editamountexpenceidstatus" name="editamountexpenceidstatus" class="form-control" type="text" readonly="" onkeypress="return onlyNumberKey(event)" />
                    </div>
                    <div class="form-group">
                        <label>Admin Amount</label>
                        <input value="" id="editadminamountexpenceidstatus" name="editadminamountexpenceidstatus" class="form-control" type="text" onkeypress="return onlyNumberKey(event)"  />
                    </div>
                    <div class="submit-section pull-right">
                        <button class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Expence Status Mpodal -->


<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Other Details</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-md table-responsive-sm" id="othertable">
                    <thead>
                        <tr>
                            <th class="text-center">Details</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Employee Amount</th>
                            <th class="text-center">Admin Amount</th>
                            <th class="text-center">Bill</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyother"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Other Status Mpodal -->
<div id="other_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reimbursement Details Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-header">
                    <p>Are you sure want to Approved or Declined for this Reimbursement?</p>
                </div>
                <form id="edit_other_status" method="POST" action="#">
                    <input value="" id="editotheridstatus" name="editotheridstatus" class="form-control" type="hidden" />
                    <div class="form-group">
                        <label>Reimbursement Details Status<span class="text-danger">*</span></label>
                        <br />
                        <select value="" id="editotherstatus" name="editotherstatus" class="form-control">
                            <option value="0">Pending</option>
                            <option value="1">Declined</option>
                            <option value="2">Approved</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Employee Amount</label>
                        <input value="" id="editamountotheridstatus" name="editamountotheridstatus" class="form-control" type="text" readonly="" onkeypress="return onlyNumberKey(event)" />
                    </div>
                    <div class="form-group">
                        <label>Admin Amount</label>
                        <input value="" id="editadminamountotheridstatus" name="editadminamountotheridstatus" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" />
                    </div>
                    <div class="submit-section pull-right">
                        <button class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Other Status Mpodal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    // view in table Allowance
        var reimbursement_id = <?php echo $reimbursementid; ?> ;
        $.ajax({
            url: base_url + "viewReimbursement",
            data:{reimbursement_id:reimbursement_id}, 
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        .done(function (response) {
            var table = document.getElementById("tablebodytravelling");
            for (i in response.data) {


                var departureDate = new Date(response.data[i].departure_date);
                var dd = String(departureDate.getDate()).padStart(2, '0');
                var mm = String(departureDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = departureDate.getFullYear();

                departureDate = dd + '-' + mm + '-' + yyyy;

                var arrivalDate = new Date(response.data[i].arrival_date);
                var dd = String(arrivalDate.getDate()).padStart(2, '0');
                var mm = String(arrivalDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = arrivalDate.getFullYear();

                arrivalDate = dd + '-' + mm + '-' + yyyy;

                var action = "";
                if (response.data[i].status == 0 ) {
                    action = '<button type="button" data-toggle="modal" data-target="#travelling_status" aria-expanded="false" class="btn btn-info edit_status" title="Status" id="' + response.data[i].reimbursement_id + '"><i class="fa fa-eye"></i></button>';
                }
                else{
                    action = '<button type="button" aria-expanded="false" class="btn btn-info" title="Status"><i class="fa fa-eye"></i></button>';  
                }
                var status = "";
                if (response.data[i].status == 0) {
                    status = 'Pending';
                }
                if (response.data[i].status == 1) {
                    status = 'Declined';
                }
                if (response.data[i].status == 2) {
                    status = 'Approved';
                }

                var status_color = "";
                if (response.data[i].status == 0) {
                    status_color = 'Blue';
                }
                if (response.data[i].status == 1) {
                    status_color = 'red';
                }
                if (response.data[i].status == 2) {
                    status_color = 'green';
                }
                  
                var departuredate = "";
                if(response.data[i].departure_date == '0000-00-00'){
                    departuredate = 'Nil';  
                }else{
                 var start = departureDate;
                 departuredate = start;
                }
                var arrivaldate = "";
                if(response.data[i].arrival_date == '0000-00-00'){
                    arrivaldate = 'Nil';  
                }else{
                 var start = arrivalDate;
                 arrivaldate = start;
                }
                var hotelname = "";
                if(response.data[i].hotel_name == ''){
                    hotelname = 'Nil';  
                }else{
                 var start = response.data[i].hotel_name;
                 hotelname = start;
                }
                var hotelamount = "";
                if(response.data[i].hotel_amount == ''){
                    hotelamount = 'Nil';  
                }else{
                 var start = response.data[i].hotel_amount;
                 hotelamount = start;
                }
                var remarkedamount = "";
                if(response.data[i].remarked_amount == null){
                    remarkedamount = 'Nil';  
                }else{
                 var start = response.data[i].remarked_amount;
                 remarkedamount = start;
                }
                var arrivabillldate = "";
                if(response.data[i].arrival_date == '0000-00-00'){
                    arrivabillldate = 'Nil';  
                }else{
                 var start = '<a href="'+ response.data[i].hotel_bill + '" class="btn btn-info" target="__blank"><i class="fa fa-download mr-2"></i><strong>Download</strong></a>';
                 arrivabillldate = start;
                }
                var arrivastatusdate = "";
                if(response.data[i].arrival_date == '0000-00-00'){
                    arrivastatusdate = 'Nil';  
                }else{
                 var start = status;
                 arrivastatusdate = start;
                }
                var arrivactiondate = "";
                if(response.data[i].arrival_date == '0000-00-00'){
                    arrivactiondate = 'Nil';  
                }else{
                 var start = action;
                 arrivactiondate = start;
                }
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +
                    departuredate +
                    "</td>" +
                    '<td class="text-center">' +
                    arrivaldate +
                    "</td>" +
                    '<td class="text-center">' +
                    hotelname +
                    "</td>" +
                    '<td class="text-center">' +
                    hotelamount +
                    "</td>" +
                    '<td class="text-center">' +
                    remarkedamount +
                    "</td>" +
                    '<td class="text-center">'+ arrivabillldate +'</td>' +
                    '<td class="text-center" style="color:' + status_color + '">' + arrivastatusdate + '</td>' +
                    '<td class="text-right">' + arrivactiondate + ' </td>';
                table.appendChild(tr);
            }
            $("#travellingtable").DataTable();
        });


    // Status Allowance Form Fill
        $(document).on('click', '.edit_status', function() {
            var reimbursement_id = $(this).attr("id");
            $.ajax({
                url: base_url + "viewReimbursement",
                method: "POST",
                data: {
                    reimbursement_id: reimbursement_id
                },
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                },
                success: function(response) {
                
                    $('#edittravellingidstatus').val(response["data"][0]["reimbursement_id"]);
                    $('#edittravellingstatus').val(response["data"][0]["status"]);
                    $('#editamounttravellingidstatus').val(response["data"][0]["hotel_amount"]);
                    $('#editadminamounttravellingidstatus').val(response["data"][0]["remarked_amount"]);
                    $('#travelling_status').modal('show');
                }
            });
        });

    //Status Allowance Form Edit 
        $("#editstatus").submit(function(e) {
            var formData = {
                'reimbursement_id'      : $('input[name=edittravellingidstatus]').val(),
                'status'                : $('select[name=edittravellingstatus]').val(),
                'remarked_amount'       : $('input[name=editadminamounttravellingidstatus]').val()
            };
            e.preventDefault();
            $.ajax({
                    type: 'POST',
                    url: base_url + 'editReimbursementStatus',
                    data: formData, // our data object
                    dataType: 'json',
                    encode: true,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Token', localStorage.token);
                    }
                })
                .done(function(response) {
                    console.log(response);

                    var jsonDa = response;
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function(){window.location ="<?php echo base_url()?>ReimbursementView/"+reimbursement_id},1000);
                      
                    } 

                    else {
                        toastr.error(falsedata);
                        $('#editstatus [type=submit]').attr('disabled',false);
                    } 
                });
        });


	// view in table Travelling
        var reimbursement_id = <?php echo $reimbursementid; ?> ;
        $.ajax({
            url: base_url + "viewTourReimbursement",
            data:{reimbursement_id:reimbursement_id}, 
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        .done(function (response) {
            var table = document.getElementById("tablebodytour");
            for (i in response.data) {
                var action = "";
                if (response.data[i].status == 0 ) {
                    action = '<button type="button" data-toggle="modal" data-target="#tour_status" aria-expanded="false" class="btn btn-info edit_statust" title="Status" id="' + response.data[i].id + '"><i class="fa fa-eye"></i></button>';
                }else{
                    action = '<button type="button" class="btn btn-info" ><i class="fa fa-eye"></i></button>';  
                }

                var status = "";
                if (response.data[i].status == 0) {
                    status = 'Pending';
                }
                if (response.data[i].status == 1) {
                    status = 'Declined';
                }
                if (response.data[i].status == 2) {
                    status = 'Approved';
                }

                var status_color = "";
                if (response.data[i].status == 0) {
                    status_color = 'Blue';
                }
                if (response.data[i].status == 1) {
                    status_color = 'red';
                }
                if (response.data[i].status == 2) {
                    status_color = 'green';
                }
                var startsdate = new Date(response.data[i].start_date);
                var dd = String(startsdate.getDate()).padStart(2, '0');
                var mm = String(startsdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = startsdate.getFullYear();

                startsdate = dd + '-' + mm + '-' + yyyy;
                var endsdate = new Date(response.data[i].end_date);
                var dd = String(endsdate.getDate()).padStart(2, '0');
                var mm = String(endsdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = endsdate.getFullYear();

                endsdate = dd + '-' + mm + '-' + yyyy;

                var startdate = "";
                if(response.data[i].start_date == '0000-00-00'){
                    startdate = 'Nil';  
                }else{
                 var start = startsdate;
                 startdate = start;
                }
                var enddate = "";
                if(response.data[i].end_date == '0000-00-00'){
                    enddate = 'Nil';  
                }else{
                 var start = endsdate;
                 enddate = start;
                }
                var starttime = "";
                if(response.data[i].start_time == '00:00:00'){
                    starttime = 'Nil';  
                }else{
                 var start = response.data[i].start_time;
                 starttime = start;
                }
                var endtime = "";
                if(response.data[i].end_time == '00:00:00'){
                    endtime = 'Nil';  
                }else{
                 var start = response.data[i].end_time;
                 endtime = start;
                }
                var modeofjourney = "";
                if(response.data[i].mode_of_journey == ""){
                    modeofjourney = 'Nil';  
                }else{
                 var start = response.data[i].mode_of_journey;
                 modeofjourney = start;
                }
                var amount = "";
                if(response.data[i].amount == ""){
                    amount = 'Nil';  
                }else{
                 var start = response.data[i].amount;
                 amount = start;
                }
                var fixedamount = "";
                if(response.data[i].fixed_amount == ''){
                    fixedamount = 'Nil';  
                }else{
                 var start = response.data[i].fixed_amount;
                 fixedamount = start;
                }
                var startimagedate = "";
                if(response.data[i].start_date == '0000-00-00'){
                    startimagedate = 'Nil';  
                }else{
                 var start = '<a href="'+ response.data[i].image + '" class="btn btn-info"><i class="fa fa-download mr-2"></i><strong>Download</strong></a>';
                 startimagedate = start;
                }
                var startstatusdate = "";
                if(response.data[i].start_date == '0000-00-00'){
                    startstatusdate = 'Nil';  
                }else{
                 var start = status;
                 startstatusdate = start;
                }
                var startactiondate = "";
                if(response.data[i].start_date == '0000-00-00'){
                    startactiondate = 'Nil';  
                }else{
                 var start = action;
                 startactiondate = start;
                }
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +
                    startdate +
                    "</td>" +
                    '<td class="text-center">' +
                    enddate +
                    "</td>" +
                    '<td class="text-center">' +
                    starttime +
                    "</td>" +
                    '<td class="text-center">' +
                    endtime +
                    "</td>" +
                    '<td class="text-center">' +
                    modeofjourney +
                    "</td>" +
                    '<td class="text-center">' +
                    amount +
                    "</td>" +
                    '<td class="text-center">' +
                    fixedamount +
                    "</td>" +
                    '<td class="text-center">'+ startimagedate +'</td>' +
                    '<td class="text-center"style="color:' + status_color + '">' + startstatusdate + '</td>' +
                    '<td class="text-right">' + startactiondate + ' </td>';
                table.appendChild(tr);
            }
            $("#tourtable").DataTable();
        });
    // Status Travelling Form Fill
        $(document).on('click', '.edit_statust', function() {
            var reimbursement_id = $(this).attr("id");
            $.ajax({
                url: base_url + "viewTourReimbursement",
                method: "POST",
                data: {
                    reimbursement_id: reimbursement_id
                },
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                },
                success: function(response) {
               

                    $('#edittouridstatus').val(response["data"][0]["id"]);
                    $('#edittourstatus').val(response["data"][0]["status"]);
                    $('#editamounttouridstatus').val(response["data"][0]["amount"]);
                    $('#editadminamounttouridstatus').val(response["data"][0]["fixed_amount"]);
                    $('#tour_status').modal('show');
                }
            });
        });
    //Status Travelling Form Edit 
        $("#edit_tour_status").submit(function(e) {
            var formData = {
                'reimbursement_id'      : $('input[name=edittouridstatus]').val(),
                'status'                : $('select[name=edittourstatus]').val(),
                'fixed_amount'          : $('input[name=editadminamounttouridstatus]').val()
            };
            e.preventDefault();
            $.ajax({
                    type: 'POST',
                    url: base_url + 'editTourStatus',
                    data: formData, // our data object
                    dataType: 'json',
                    encode: true,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Token', localStorage.token);
                    }
                })
                .done(function(response) {
                    console.log(response);

                    var jsonDa = response;
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function(){window.location ="<?php echo base_url()?>ReimbursementView/"+reimbursement_id},1000);
                      
                    } 

                    else {
                        toastr.error(falsedata);
                        $('#edit_tour_status [type=submit]').attr('disabled',false);
                    } 
                });
        });

    // view in table Expence
        var reimbursement_id = <?php echo $reimbursementid; ?> ;
        $.ajax({
            url: base_url + "viewFoodReimbursement",
            data:{reimbursement_id:reimbursement_id}, 
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        .done(function (response) {
            var table = document.getElementById("tablebodyfood");
            for (i in response.data) {
                var action = "";
                if (response.data[i].status == 0 ) {
                    action = '<button type="button" data-toggle="modal" data-target="#food_status" aria-expanded="false" class="btn btn-info edit_statuse" title="Status" id="' + response.data[i].reimbursement_id + '"><i class="fa fa-eye"></i></button>';
                }else{
                    action = '<button type="button" class="btn btn-info" ><i class="fa fa-eye"></i></button>';  
                }

                var status = "";
                if (response.data[i].status == 0) {
                    status = 'Pending';
                }
                if (response.data[i].status == 1) {
                    status = 'Declined';
                }
                if (response.data[i].status == 2) {
                    status = 'Approved';
                }

                var status_color = "";
                if (response.data[i].status == 0) {
                    status_color = 'Blue';
                }
                if (response.data[i].status == 1) {
                    status_color = 'red';
                }
                if (response.data[i].status == 2) {
                    status_color = 'green';
                }
                 var foodname ="";
                 if(response.data[i].food_name == "")
                 {
                    foodname = 'Nil';
                 }else{
                   var food = response.data[i].food_name;
                   foodname = food;
                 }
                 var cityname ="";
                 if(response.data[i].city == "")
                 {
                    cityname = 'Nil';
                 }else{
                   var food = response.data[i].city;
                   cityname = food;
                 }
                 var foodamount ="";
                 if(response.data[i].food_amount == null)
                 {
                    foodamount = 'Nil';
                 }else{
                   var food = response.data[i].food_amount;
                   foodamount = food;
                 }
                 var clearamount ="";
                 if(response.data[i].clear_amount == null)
                 {
                    clearamount = 'Nil';
                 }else{
                   var food = response.data[i].clear_amount;
                   clearamount = food;
                 }
                 var foodsdate = new Date(response.data[i].food_date);
                var dd = String(foodsdate.getDate()).padStart(2, '0');
                var mm = String(foodsdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = foodsdate.getFullYear();

                foodsdate = dd + '-' + mm + '-' + yyyy;
                 var fooddate ="";
                 if(response.data[i].food_date == '0000-00-00')
                 {
                    fooddate = 'Nil';
                 }else{
                   var food = foodsdate;
                   fooddate = food;
                 }
                 var fooddatebill ="";
                 if(response.data[i].food_date == '0000-00-00')
                 {
                    fooddatebill = 'Nil';
                 }else{
                   var food = '<a href="'+ response.data[i].food_bill + '" class="btn btn-info"><i class="fa fa-download mr-2"></i><strong>Download</strong></a';
                   fooddatebill = food;
                 }
                 var fooddatestatus ="";
                 if(response.data[i].food_date == '0000-00-00')
                 {
                    fooddatestatus = 'Nil';
                 }else{
                   var food = status;
                   fooddatestatus = food;
                 }
                 var fooddateaction ="";
                 if(response.data[i].food_date == '0000-00-00')
                 {
                    fooddateaction = 'Nil';
                 }else{
                   var food = action;
                   fooddateaction = food;
                 }
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +
                    foodname+
                    "</td>" +
                    '<td class="text-center">' +
                    cityname +
                    "</td>" +
                    '<td class="text-center">' +
                    fooddate +
                    "</td>" +
                    '<td class="text-center">' +
                    foodamount +
                    "</td>" +
                    '<td class="text-center">' +
                    clearamount +
                    "</td>" +
                    '<td class="text-center">' + fooddatebill + '</td>' +
                    '<td class="text-center"style="color:' + status_color + '">' + fooddatestatus + '</td>' +
                    '<td class="text-right">' + fooddateaction + ' </td>';
                table.appendChild(tr);
            }
            $("#foodtable").DataTable();
        });

    // Status Reimbursement Form Fill
        $(document).on('click', '.edit_statuse', function() {
            var reimbursement_id = $(this).attr("id");
            $.ajax({
                url: base_url + "viewFoodReimbursement",
                method: "POST",
                data: {
                    reimbursement_id: reimbursement_id
                },
                dataType: "json",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                },
                success: function(response) {
                
                    $('#editfoodidstatus').val(response["data"][0]["reimbursement_id"]);
                    $('#editfoodstatus').val(response["data"][0]["status"]);
                    $('#editamountexpenceidstatus').val(response["data"][0]["food_amount"]);
                    $('#editadminamountexpenceidstatus').val(response["data"][0]["clear_amount"]);
                    $('#food_status').modal('show');
                }
            });
        });

    //Status Reimbursement Form Edit 
        $("#edit_food_status").submit(function(e) {
            var formData = {
                'reimbursement_id'      : $('input[name=editfoodidstatus]').val(),
                'clear_amount'      	: $('input[name=editadminamountexpenceidstatus]').val(),
                'status'                : $('select[name=editfoodstatus]').val()
            };
            e.preventDefault();
            $.ajax({
                    type: 'POST',
                    url: base_url + 'editFoodStatus',
                    data: formData, // our data object
                    dataType: 'json',
                    encode: true,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Token', localStorage.token);
                    }
                })
                .done(function(response) {
                    console.log(response);

                    var jsonDa = response;
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function(){window.location ="<?php echo base_url()?>ReimbursementView/"+reimbursement_id},1000);
                      
                    } 

                    else {
                        toastr.error(falsedata);
                        $('#edit_food_status [type=submit]').attr('disabled',false);
                    } 
                });
        });
        // view in table Other
	        var reimbursement_id = <?php echo $reimbursementid; ?> ;
	        $.ajax({
	            url: base_url + "viewOtherReimbursement",
	            data:{reimbursement_id:reimbursement_id}, 
	            type: "POST",
	            dataType: "json", 
	            encode: true,
	            beforeSend: function (xhr) {
	                xhr.setRequestHeader("Token", localStorage.token);
	            },
	        })

	        .done(function (response) {
	            var table = document.getElementById("tablebodyother");
	            for (i in response.data) {
	                var action = "";
	                if (response.data[i].status == 0 ) {
	                    action = '<button type="button" data-toggle="modal" data-target="#other_status" aria-expanded="false" class="btn btn-info edit_statuso" title="Status" id="' + response.data[i].reimbursement_id + '"><i class="fa fa-eye"></i></button>';
	                }else{
                    action = '<button type="button" class="btn btn-info" ><i class="fa fa-eye"></i></button>';  
                }

	                var status = "";
	                if (response.data[i].status == 0) {
	                    status = 'Pending';
	                }
	                if (response.data[i].status == 1) {
	                    status = 'Declined';
	                }
	                if (response.data[i].status == 2) {
	                    status = 'Approved';
	                }

	                var status_color = "";
	                if (response.data[i].status == 0) {
	                    status_color = 'Blue';
	                }
	                if (response.data[i].status == 1) {
	                    status_color = 'red';
	                }
	                if (response.data[i].status == 2) {
	                    status_color = 'green';
	                }

                   var detail = "";
                   if (response.data[i].other_details == null) {
                       detail ='Nil';
                   }
                   else{
                       var testing = response.data[i].other_details;
                       detail = testing;
                   }
                   var employeeamount = "";
                   if (response.data[i].employee_amount == '') {
                    employeeamount ='Nil';
                   }
                   else{
                       var testing = response.data[i].employee_amount;
                       employeeamount = testing;
                   }
                   var adminamount = "";
                   if (response.data[i].admin_amount == null) {
                    adminamount ='Nil';
                   }
                   else{
                       var testing = response.data[i].admin_amount;
                       adminamount = testing;
                   }
                   var othersdate = new Date(response.data[i].other_date);
                var dd = String(othersdate.getDate()).padStart(2, '0');
                var mm = String(othersdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = othersdate.getFullYear();

                othersdate = dd + '-' + mm + '-' + yyyy;
                   var otherdate = "";
                   if (response.data[i].other_date == '0000-00-00') {
                    otherdate ='Nil';
                   }
                   else{
                       var testing = othersdate;
                       otherdate = testing;
                   }
                   var billname = "";
                   if (response.data[i].bill_name == "") {
                    billname ='Nil';
                   }
                   else{
                       var testing = response.data[i].bill_name;
                       billname = testing;
                   }
                   var otherdateimage = "";
                   if (response.data[i].other_date == '0000-00-00') {
                    otherdateimage ='Nil';
                   }
                   else{
                       var testing = '<a href="'+ response.data[i].other_image + '" class="btn btn-info" ><i class="fa fa-download mr-2"></i><strong>Download</strong></a>';
                       otherdateimage = testing;
                   }
                   var otherdatestatus = "";
                   if (response.data[i].other_date == '0000-00-00') {
                    otherdatestatus ='Nil';
                   }
                   else{
                       var testing = status;
                       otherdatestatus = testing;
                   }
                   var otherdateaction = "";
                   if (response.data[i].other_date == '0000-00-00') {
                    otherdateaction ='Nil';
                   }
                   else{
                       var testing = action;
                       otherdateaction = testing;
                   }
	                var tr = document.createElement("tr");
	                tr.innerHTML =
	                    '<td class="text-center">' +
                        detail +
	                    "</td>" +
	                    '<td class="text-center">' +
                        otherdate +
	                    "</td>" +
	                    '<td class="text-center">' +
	                    employeeamount + 
	                    "</td>" +
	                    '<td class="text-center">' +
	                    adminamount +
	                    "</td>" +
	                    '<td class="text-center">' +
	                    billname +
	                    "</td>" +
	                    '<td class="text-center">'+ otherdateimage +'</td>' +
	                    '<td class="text-center"style="color:' + status_color + '">' + otherdatestatus + '</td>' +
	                    '<td class="text-right">' + otherdateaction + ' </td>';
	                table.appendChild(tr);
	            }
	            $("#othertable").DataTable();
	        });
	    // Status Other Form Fill
	        $(document).on('click', '.edit_statuso', function() {
	            var reimbursement_id = $(this).attr("id");
	            $.ajax({
	                url: base_url + "viewOtherReimbursement",
	                method: "POST",
	                data: {
	                    reimbursement_id: reimbursement_id
	                },
	                dataType: "json",
	                beforeSend: function(xhr) {
	                    xhr.setRequestHeader('Token', localStorage.token);
	                },
	                success: function(response) {
	                
	                    $('#editotheridstatus').val(response["data"][0]["reimbursement_id"]);
	                    $('#editotherstatus').val(response["data"][0]["status"]);
	                    $('#editamountotheridstatus').val(response["data"][0]["employee_amount"]);
	                    $('#editadminamountotheridstatus').val(response["data"][0]["admin_amount"]);
	                    $('#other_status').modal('show');
	                }
	            });
	        });

    //Status Reimbursement Form Edit 
        $("#edit_other_status").submit(function(e) {
            var formData = {
                'reimbursement_id'      : $('input[name=editotheridstatus]').val(),
                'admin_amount'      	: $('input[name=editadminamountotheridstatus]').val(),
                'status'                : $('select[name=editotherstatus]').val()
            };
            e.preventDefault();
            $.ajax({
                    type: 'POST',
                    url: base_url + 'editOtherStatus',
                    data: formData, // our data object
                    dataType: 'json',
                    encode: true,
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Token', localStorage.token);
                    }
                })
                .done(function(response) {
                    console.log(response);

                    var jsonDa = response;
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function(){window.location ="<?php echo base_url()?>ReimbursementView/"+reimbursement_id},1000);
                      
                    } 

                    else {
                        toastr.error(falsedata);
                        $('#edit_other_status [type=submit]').attr('disabled',false);
                    } 
                });
        });

</script>