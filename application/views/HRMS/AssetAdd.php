<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Assets">Assets</a></li>
                <li class="breadcrumb-item active">Add New Asset</li>
            </ul>
        </div>

        <!-- Search Assets Result-->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add New Asset</strong></h3>
            </div>
            <div class="card-body">
                <form id="addassets" method="POST">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Asset Category<span class="text-danger"> *</span></label><br>
                            <span id="assetcategoryvalid" class="text-danger change-pos"></span>
                            <select class="form-control custom-select" type="text" name="assetcategory" id="assetcategory" >
                                
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Asset Name<span class="text-danger"> *</span></label><br>
                            <span id="assetnamevalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" name="assetname" id="assetname" placeholder="Please enter asset name." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Purchase From<span class="text-danger"> *</span></label><br>
                            <span id="purchasefromvalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" name="purchasefrom" id="purchasefrom" placeholder="Please enter purchase from.">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Purchase By<span class="text-danger"> *</span></label><br>
                            <span id="purchasebyvalid" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" value="" name="purchaseby" id="purchaseby">
                                
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Purchase Date<span class="text-danger"> *</span></label><br>
                            <span id="purchasedatevalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="date" name="purchasedate" id="purchasedate">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Serial Number<span class="text-danger"> *</span></label><br>
                            <span id="serialnumbervalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" name="serialnumber" id="serialnumber" placeholder="Please enter serial number." autocomplete="off" >
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Condition<span class="text-danger"> *</span></label><br>
                            <span id="conditionvalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" name="condition" id="condition" placeholder="Please enter assets condition. e.g. new old ">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Warranty<span class="text-danger"> *</span></label><br>
                            <span id="warrantyvalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" name="warranty" id="warranty" placeholder="Please enter warranty in months. ">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Amount<span class="text-danger"> *</span></label><br>
                            <span id="amountvalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" name="amount" id="amount" placeholder="Please enter amount. " onkeypress="return onlyNumberKey(event)">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Bill No<span class="text-danger"> *</span></label><br>
                            <span id="billnovalid" class="text-danger change-pos"></span>
                            <input class="form-control" type="text" name="billno" id="billno" placeholder="Please enter bill no." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Description<span class="text-danger"> </span></label><!-- <br>
                            <span id="descriptionvalid" class="text-danger change-pos"></span> -->
                            <textarea class="form-control" type="text" name="description" id="description" placeholder="Please enter description."></textarea>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 text-right">
                            <button id="assetsbutton" class="btn btn-success submit-btn">Submit</button> 
                            <button type="reset" class="btn btn-secondary" id="test">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Search Assets Result-->
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("#assetcategory option:eq(0)").prop("selected", true);
            $("#purchaseby option:eq(0)").prop("selected", true);
            $("input#assetname").val("");
            $("input#purchasedate").val("");
            $("input#purchasefrom").val("");
            $("input#serialnumber").val("");
            $("input#condition").val("");
            $("input#warranty").val("");
            $("input#amount").val("");
            $("input#billno").val("");
            $("textarea#description").val("");
            $("span#assetcategoryvalid").prop("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#assetcategoryvalid").text("");
            jQuery("#assetnamevalid").text("");
            jQuery("#purchasedatevalid").text("");
            jQuery("#purchasefromvalid").text("");
            jQuery("#purchasebyvalid").text("");
            jQuery("#serialnumbervalid").text("");
            jQuery("#conditionvalid").text("");
            jQuery("#warrantyvalid").text("");
            jQuery("#amountvalid").text("");
            jQuery("#billnovalid").text("");

            jQuery("#editbranchvalidate").text("");
            jQuery("#editdepartmentvalidate").text("");
        });
    });


    //Select Asset Category by api
    $.ajax({
        url: base_url + "viewAssetsCategoryActive",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#assetcategory");

        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Assets Category</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.category_assets));
        });
    });
   
    // Add Asset Category Form
    $("#addassets").submit(function (e) {
        var formData = {
            assets_name: $("input[name=assetname]").val(),
            purchage_date: $("input[name=purchasedate]").val(),
            purchage_from: $("input[name=purchasefrom]").val(),
            purchage_by: $("select[name=purchaseby]").val(),
            serial_no: $("input[name=serialnumber]").val(),
            assets_cond: $("input[name=condition]").val(),
            assets_warranty: $("input[name=warranty]").val(),
            assets_amount: $("input[name=amount]").val(),
            assets_billno: $("input[name=billno]").val(),
            description: $("textarea[name=description]").val(),
            category_id: $("select[name=assetcategory]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addAssetsCompany",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Assets";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addassetcategory [type=submit]").attr("disabled", false);
            }
        });
    });

    
    //Add Asset Cateogory Validation
    $(document).ready(function () {
        $("#assetsbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#assetcategory").val() == null) {
                $("#assetcategory").focus();
                $("#assetcategoryvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#assetcategoryvalid").text("");
            }

            if ($("#assetname").val().trim() == "") {
                $("#assetname").focus();
                $("#assetnamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#assetnamevalid").text("");
            }

            if ($("#purchasedate").val().trim() == "") {
                $("#purchasedatevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#purchasedatevalid").text("");
            }

            if ($("#purchasefrom").val().trim() == "") {
                $("#purchasefrom").focus();
                $("#purchasefromvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#purchasefromvalid").text("");
            }

            if ($("#purchaseby").val() == null) {
                $("#purchaseby").focus();
                $("#purchasebyvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#purchasebyvalid").text("");
            }

            if ($("#serialnumber").val().trim() == "") {
                $("#serialnumber").focus();
                $("#serialnumbervalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#serialnumbervalid").text("");
            }
            if ($("#condition").val().trim() == "") {
                $("#condition").focus();
                $("#conditionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#conditionvalid").text("");
            }
            if ($("#warranty").val().trim() == "") {
                $("#warranty").focus();
                $("#warrantyvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#warrantyvalid").text("");
            }
            if ($("#amount").val().trim() == "") {
                $("#amount").focus();
                $("#amountvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#amountvalid").text("");
            }
            if ($("#billno").val().trim() == "") {
                $("#billno").focus();
                $("#billnovalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#billnovalid").text("");
            }     


            if (errorCount > 0) {
                return false;
            }
        });
    });
 //Select Asset Category by api
 $.ajax({
        url: base_url + "viewUserId",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#purchaseby");

        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Purchase By</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.first_name).text(entry.first_name));
        });
    });
   
</script>