<!-- Plugins chart css -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/c3.min.css" />

<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title">Project Statistics</h3>
                    </div>
                    <div class="card-body">
                        <div class="row text-center align-items-center">
                            <div class="col-sm-3 border-right pb-4 pt-4">
                                <label class="mb-0">Total Project</label>
                                <h4 class="font-30 font-weight-bold text-col-blue counter" id="totalproject"></h4>
                            </div>
                            <div class="col-sm-3 border-right pb-4 pt-4">
                                <label class="mb-0">On Going</label>
                                <h4 class="font-30 font-weight-bold text-col-blue counter" id="totalongoing"></h4>
                            </div>
                            <div class="col-sm-3 pb-4 pt-4">
                                <label class="mb-0">Pending</label>
                                <h4 class="font-30 font-weight-bold text-col-blue counter" id="totalpending"></h4>
                            </div>
                            <div class="col-sm-3 pb-4 pt-4">
                                <label class="mb-0">Complete</label>
                                <h4 class="font-30 font-weight-bold text-col-blue counter" id="totalcomplete"></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title">Project Summary</h3>
                        <div class="card-options">
                            <a href="<?php echo base_url() ?>ProjectAdd" class="btn btn-primary">Add Project</a>
                            <a href="<?php echo base_url() ?>Projects" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        
                        <table class="table table-bordered table-hover mb-0 table-vcenter table-responsive table-responsive-xxl table-responsive-l text-nowrap dataTable" id="usertable">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">Project Name</th>
                                    <th class="text-center">Branch</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Deadline Date</th>
                                    <th class="text-center">Project Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="usertbody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Project Modal -->
<div class="modal custom-modal fade" id="delete_project" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_project_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Project Modal -->

<div id="project_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Project Status</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="editstatus" method="POST" action="#">
                <div class="modal-body">
                    <div class="form-header">
                        <p>Are you sure want to change project status?</p>
                    </div>

                    <div class="form-group">
                        <label>Project Status<span class="text-danger">*</span></label>
                        <input value="" id="editprojectidstatus" name="editprojectidstatus" class="form-control" type="hidden" />
                        <select value="" id="editprojectstatus" name="editprojectstatus" class="form-control">
                            <option value="0">Pending</option>
                            <option value="1">Ongoing</option>
                            <option value="2">Completed</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="editleavebutton" class="btn btn-success submit-btn">Yes Change It!</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<!-- Charts Homepage -->
<script src="<?php echo base_url(); ?>assets/js/c3.bundle.js"></script>
<script src="<?php echo base_url(); ?>assets/js/c3.js"></script>
<script type="text/javascript">
    //Show Total Project
    $.ajax({
        url: base_url + "totalProjectSelf",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalproject").html(response.data[0].totalProjects);
        },
    });

    //Show Total Ongoing Project
    $.ajax({
        url: base_url + "totalOngoingProjectSelf",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalongoing").html(response.data[0].totalOngoing);
        },
    });

    //Show Total Pending Project
    $.ajax({
        url: base_url + "totalPendingProjectSelf",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalpending").html(response.data[0].totalPending);
        },
    });

    //Show Total Complete Project
    $.ajax({
        url: base_url + "totalCompleteProjectSelf",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalcomplete").html(response.data[0].totalComplete);
        },
    });

   

            $.ajax({
                url: base_url + "viewProjectSelf",
                data: {},
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                $("#usertable").DataTable().clear().destroy();
                $("#usertable tbody").empty();
                var table = document.getElementById("usertbody");
                for (i in response.data) {
                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }

                    var status_color = "";
                    if (response.data[i].status == 0) {
                        status_color = "Blue";
                    }
                    if (response.data[i].status == 1) {
                        status_color = "Orange";
                    }
                    if (response.data[i].status == 2) {
                        status_color = "Green";
                    }
                    var isstatus = "";
                    if (response.data[i].status == 0) {
                        isstatus = "Pending";
                    }
                    if (response.data[i].status == 1) {
                        isstatus = "Ongoing";
                    }
                    if (response.data[i].status == 2) {
                        isstatus = "Completed";
                    }

                    var endDate = new Date(response.data[i].end_date);
                    var dd = String(endDate.getDate()).padStart(2, "0");
                    var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                    var yyyy = endDate.getFullYear();

                    endDate = dd + "-" + mm + "-" + yyyy;

                    var tr = document.createElement("tr");
                    tr.innerHTML =

                        '<td class="text-center"><a href="ProjectDetails/' + response.data[i].id +' ">' +  response.data[i].project_name + '</a></td>' +

                        '<td class="text-center">' + response.data[i].branch_name + '</td>' +
                        
                        '<td class="text-center">' + response.data[i].department_name + '</td>' +
                        
                        '<td class="text-center text-danger">' + endDate + '</td>' +

                        '<td class="text-center"><span class="tag text-white" style="background-color:' + status_color + '">' + isstatus + '</span></td>' +

                        '<td class="text-center"><button type="button" data-toggle="modal" data-target="#project_status" aria-expanded="false" class="btn btn-primary edit_status" id="' + response.data[i].id + '" title="Status Change" ><i class="fa fa-eye"></i></button> <a href="ProjectEdit/' + response.data[i].id + '" class="btn btn-info edit_data" title="Edit Job" id="' + response.data[i].id + '"><i class="fa fa-edit"></i></a> <button type="button" data-toggle="modal" data-target="#delete_project" aria-expanded="false" class="btn btn-danger delete_data" id="' + response.data[i].id + '" title="Delete Job" ><i class="fa fa-trash-alt"></i></button></td>';
                    table.appendChild(tr);
                }
                $("#usertable").DataTable({
                    /*dom: "Bfrtip",
                    buttons: ["excelHtml5", "pdfHtml5"],*/
                });
            });
       

    //Search Filter Validation
    $(document).ready(function () {
        $("#filterdesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#filterbranchid").val() == null) {
                $("#filterbranchid").focus();
                $("#filterbranchidvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#filterbranchidvalidate").text("");
            }
            if ($("#filterdepartmentid").val() == null) {
                $("#filterdepartmentid").focus();
                $("#filterdepartmentidvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#filterdepartmentidvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Delete Project
    $(document).on("click", ".delete_data", function () {
        var project_id = $(this).attr("id");
        $(".delete_project_button").attr("id", project_id);
        $("#delete_project").modal("show");
    });

    $(document).on("click", ".delete_project_button", function () {
        var project_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteProject",
            method: "POST",
            data: {
                project_id: project_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                window.location.replace("<?php echo base_url(); ?>ProjectList");
            },
        });
    });

    // Edit Status
    $(document).on("click", ".edit_status", function () {
        var project_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewProject",
            method: "POST",
            data: {
                project_id: project_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editprojectidstatus").val(response["data"][0]["id"]);
                $("#editprojectstatus").val(response["data"][0]["status"]);
                $("#project_status").modal("show");
            },
        });
    });

    // Edit Project Active  form
    $("#editstatus").submit(function (e) {
        var formData = {
            project_id: $("input[name=editprojectidstatus]").val(),
            status: $("select[name=editprojectstatus]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "changeProjectStatus",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>ProjectList";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editstatus [type=submit]").attr("disabled", false);
            }
        });
    });
</script>
