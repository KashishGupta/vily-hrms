<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Jobs">Jobs</a></li>
                <li class="breadcrumb-item active">Job Applicants</li>
            </ul>
        </div>
    </div>
</div>
<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="row clearfix kt-widget__items"></div>
    </div>
</div>

<!-- Status Job Modal -->
<div id="status_job" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Job Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="statusjobs" method="POST">
                    <div class="form-group">
                        <label>Status</label>
                        <select value="" class="select form-control" name="editstatus" id="editstatus">
                            <option>Applied </option>
                            <option>Shortlisted </option>
                            <option>Interviewed </option>
                            <option>Rejected </option>
                        </select>
                        <input type="hidden" id="statusjobsid" name="statusjobsid" class="form-control" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editTicketbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .profile-widget-job {
        background-color: #fff;
        border: 1px solid #ededed;
        border-radius: 4px;
        margin-bottom: 20px;
        padding: 20px;
        position: relative;
        box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.2);
        overflow: hidden;
    }
</style>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
                //Show Employee
                    var job_id = <?php echo $jobId; ?>;
                    $.ajax({
                            url: base_url + "viewJobApplicant",
                            data: {job_id : job_id },
                            type: "POST",
                            dataType: 'json',
                            encode: true,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Token', localStorage.token);
                            }
                        })
                        .done(function(response) {
                            if (!$.trim(response.data[0])) {
                var html = '<div class="card"><div class="card-header"><h3 class="card-title"><strong>Job Applicants</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
                $(".kt-widget__items").append(html);
            } else {
                            for (i in response.data) {
                                var startDate = new Date(response.data[i].apply_date);
                            var dd = String(startDate.getDate()).padStart(2, '0');
                            var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                            var yyyy = startDate.getFullYear();

                            startDate = dd + '-' + mm + '-' + yyyy;
                            
                          
                                var html ='<div class="col-lg-12 col-md-12"><div class="profile-widget-job"><h5 class="mb-4">' + response.data[i].first_name + '</h5><div class="row"><div class="col-5"><p><strong>Referred By: </strong><span>' + response.data[i].referredby + '</span></div><div class="col-7"><p><strong>Apply Date:</strong> <span> <strong>' + startDate + '</strong></span></div><div class="col-5"><p>Exp: <span> <strong>' + response.data[i].experience + '</strong></span></div><div class="col-7"><p>Current Location: <span>' + response.data[i].current_location + '</span></div><div class="col-5"><p><strong>Phone No: </strong><span>' + response.data[i].phone + '</span></p></div><div class="col-7"><p>Email: <span><strong> ' + response.data[i].email + '</strong></span></p></div><div class="col-5"><p><strong>Current Designation: </strong><span>' + response.data[i].current_designation + '</span></p></div><div class="col-7"><p><strong>Current Company:</strong> <span>' + response.data[i].current_company + '</span></p></div><div class="col-5"><p><strong>Education: </strong><span> ' + response.data[i].hieghest_education + '</span></p></div><div class="col-5"><p>Resume: <span><strong> <a href="'+ response.data[i].resume + '" class="btn btn-info"><i class="fa fa-download mr-2"></i>Download Resume</a></strong></span></p></div><div class="col-12"><p><strong>Skills:</strong> <span>' + response.data[i].skills + '</span></p></div> <div class="col-12" style="padding-top: 10px; border-top: 1px solid #E8E9E9; "><form id="filter-form"><div class="row clearfix"><div class="form-group col-md-4 col-sm-12" style="margin-bottom: 0;"><select class="form-control custom-select" onchange="GetSelectedTextValue(this,'+response.data[i].job_applicant_id+')"><option value="">Choose Applicant Status</option><option value="0">Applied </option><option value="1">Shortlisted </option><option value="2">Interviewed </option><option value="3">Rejected</option></select></div></div></form></div>  </div></div></div>';
                                $('.kt-widget__items').append(html);
                               
                            }

                        }
                        });


    //Edit  Department Form Fill


                // Active or inactive ticket form fill
                function GetSelectedTextValue(status,VL_id) {
            var selectedText = status.options[status.selectedIndex].innerHTML;
            var statustest = VL_id;
          
            var status = status.value;
          
               req={};
               req.job_applicant_id = statustest;
               req.status = status;
               console.log(req);
          $.ajax({
                    type: "POST",
                    url: base_url + "changeStatusOfJobApplicant",
                    data: req,
                    dataType: "json",
                    encode: true,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Token", localStorage.token);
                    },
                })

                .done(function (response) {
                    console.log(response);
                    var jsonDa = response;
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function () {
                            window.location = "<?php echo base_url()?>JobApplicants/"+job_id;
                        }, 1000);
                    } else {
                        toastr.error(falsedata);
                        $("#addtask [type=submit]").attr("disabled", false);
                    }
                });


        }
</script>
