<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>/Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Profile</li>
            </ul>
        </div>

        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card card-profile mb-0">
                    <div class="card-body">
                        <div class="profile-view">
                            <div class="profile-img-wrap">
                                <div class="profile-img" id="fgfimage"></div>
                            </div>
                            <div class="profile-basic">
                                <div class="row clearfix">
                                    <div class="col-md-5">
                                        <div class="profile-info-left">
                                            <h4 class="user-name m-t-0 mb-0"><span id="profilenames"></span></h4>
                                            <div class="staff-id text-muted"><span id="profiledesignations"></span> - <span id="profiledepartment"></span></div>
                                            <div class="staff-id"><span id="empid"></span> - <span id="joiningdate"></span></div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Phone No:</div>
                                                <div class="text" id="phone"></div>
                                            </li>
                                            <li>
                                                <div class="title">Personal Email:</div>
                                                <div class="text1" id="email"></div>
                                            </li>
                                            <li>
                                                <div class="title">Father's Name:</div>
                                                <div class="text1" id="fathername"></div>
                                            </li>
                                            <li>
                                                <div class="title">DOB:</div>
                                                <div class="text" id="dob"></div>
                                            </li>
                                            <li>
                                                <div class="title">Address:</div>
                                                <div class="text1" id="address"></div>
                                            </li>
                                            <li>
                                                <div class="title">Gender:</div>
                                                <div class="text" id="gender"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pro-edit">
                                <a data-target="#edit_user" data-toggle="modal" class="edit-icon" href="#"><i class="fa fa-edit text-info"></i></a>
                            </div>

                            <!-- Edit Profile Info Modal -->
                            <div id="edit_user" class="modal custom-modal fade" role="dialog">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Profile Info</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="edituser" method="POST" action="#">
                                                <div class="row clearfix">
                                                    <div class="form-group col-sm-6 col-md-6">
                                                        <label>Email<span class="text-danger"> *</span></label><br />
                                                        <span class="text-danger change-pos" id="editpersonalemailvalid"></span>
                                                        <input value="" id="editpersonalemail" name="editpersonalemail" class="form-control" type="email" autocomplete="off" placeholder="Please enter your email." />
                                                        <input value="" id="editpersonalid" name="editpersonalid" class="form-control" type="hidden" />
                                                    </div>
                                                    
                                                    <div class="form-group col-sm-6 col-md-6">
                                                        <label>Father's Name<span class="text-danger"> *</span></label><br />
                                                        <span class="text-danger change-pos" id="editfathernamevalid"></span>
                                                        <input value="" id="editfathername" name="editfathername" class="form-control name-valid" type="text" autocomplete="off" placeholder="Please enter your father's name." />
                                                    </div>
                                                    <div class="form-group col-sm-6 col-md-6">
                                                        <label>Phone No<span class="text-danger"> *</span></label><br />
                                                        <span class="text-danger change-pos" id="editphonevalid"></span>
                                                        <input value="" id="editphone" name="editphone" class="form-control" type="text" autocomplete="off" placeholder="Please enter phone no." onkeypress="return onlyNumberKey(event)" />
                                                    </div>
                                                    <div class="form-group col-sm-6 col-md-6">
                                                        <label>Date Of Birth<span class="text-danger"> *</span></label><br />
                                                        <span class="text-danger change-pos" id="editdobvalid"></span>
                                                        <input value="" data-provide="datepicker" data-date-autoclose="true" id="editdob" name="editdob" class="form-control" placeholder="yyyy/mm/dd" type="text" readonly="" autocomplete="off" />
                                                    </div>
                                                    <div class="form-group col-sm-6 col-md-6">
                                                        <label>Gender<span class="text-danger"> *</span></label><br />
                                                        <span class="text-danger change-pos" id="editgendervalid"></span>
                                                        <select value="" id="editgender" name="editgender" class="custom-select form-control"> </select>
                                                    </div>
                                                    <div class="form-group col-sm-12 col-md-12">
                                                        <label>Complete Address<span class="text-danger"> *</span></label><br />
                                                        <span class="text-danger change-pos" id="editaddressvalid"></span>
                                                        <textarea value="" cols="2" rows="3" id="editaddress" name="editaddress" class="form-control" type="text" autocomplete="off" placeholder="Please enter your complete address."></textarea>
                                                    </div>
                                                </div>
                                                <div class="submit-section pull-right">
                                                    <button id="editpersonalinfo" class="btn btn-primary submit-btn">Update Changes</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Family Details Info Modal -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card tab-box">
                    <div class="row user-tabs">
                        <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
                            <ul class="nav nav-tabs nav-tabs-bottom" style="border-bottom: 0px;">
                                <li class="nav-item"><a href="#personal_details" data-toggle="tab" class="nav-link active">Personal Details</a></li>
                                <li class="nav-item"><a href="#family_details" data-toggle="tab" class="nav-link">Emergency Contact</a></li>
                                <li class="nav-item"><a href="#education_experience" data-toggle="tab" class="nav-link">Education & Experience</a></li>
                                <li class="nav-item"><a href="#office_details" data-toggle="tab" class="nav-link">Office Details</a></li>
                                <li class="nav-item"><a href="#bank_details" data-toggle="tab" class="nav-link">Bank Details</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <!-- Profile Info Tab -->
                    <div id="personal_details" class="pro-overview tab-pane fade active show">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                    <div class="card-body">
                                        <h3 class="card-title">
                                            <strong>Personal Informations</strong>
                                            <a href="#" class="edit-icon" data-toggle="modal" data-target="#personal_info">
                                                <i class="fa fa-edit text-info"></i>
                                            </a>
                                        </h3>
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Passport No:</div>
                                                <div class="text text-uppercase" id="passportno"></div>
                                            </li>
                                            <li>
                                                <div class="title">Pass. Exp Date:</div>
                                                <div class="text" id="passportexpiry"></div>
                                            </li>
                                            <li>
                                                <div class="title">Nationality:</div>
                                                <div class="text" id="nationality"></div>
                                            </li>
                                            <li>
                                                <div class="title">Religion:</div>
                                                <div class="text" id="religion"></div>
                                            </li>
                                            <li>
                                                <div class="title">Marital Status:</div>
                                                <div class="text" id="maritalstatus"></div>
                                            </li>
                                            <li>
                                                <div class="title">No. of children:</div>
                                                <div class="text" id="noofchildren"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Profile Info Tab -->

                    <!-- Family Info Tab -->
                    <div class="tab-pane fade" id="family_details">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                    <div class="card-body">
                                        <h3 class="card-title">
                                            <strong>Contact Informations</strong> <a href="#" class="edit-icon" data-toggle="modal" data-target="#emergency_contact"><i class="fa fa-edit text-info"></i></a>
                                        </h3>
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Name:</div>
                                                <div class="text" id="familyname"></div>
                                            </li>
                                            <li>
                                                <div class="title">Relationship:</div>
                                                <div class="text" id="familyrelationship"></div>
                                            </li>
                                            <li>
                                                <div class="title">Phone:</div>
                                                <div class="text" id="familyphone"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Family Info Tab -->

                    <!-- Education Tab -->
                    <div class="tab-pane fade" id="education_experience">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                    <div class="card-body">
                                        <h3 class="card-title">
                                            <strong>Experience Informations</strong> <a href="#" class="edit-icon" data-toggle="modal" data-target="#add_experience"><i class="fa fa-plus text-primary"></i></a>
                                        </h3>
                                        <div class="experience-box">
                                            <ul class="experience-list kt-widget__items"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                    <div class="card-body">
                                        <h3 class="card-title">
                                            <strong>Education Informations </strong> <a href="#" class="edit-icon" data-toggle="modal" data-target="#add_education"><i class="fa fa-plus text-primary"></i></a>
                                        </h3>
                                        <div class="experience-box">
                                            <ul class="experience-list kt-widget__education"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Education Tab -->

                    <!-- Office Tab -->
                    <div class="tab-pane fade" id="office_details">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                    <div class="card-body">
                                        <h3 class="card-title"><strong>Office Details</strong></h3>
                                        <div class="experience-box">
                                            <ul class="personal-info">
                                                <li>
                                                    <div class="title">Branch Name:</div>
                                                    <div class="text" id="branchname"></div>
                                                </li>
                                                <li>
                                                    <div class="title">Department:</div>
                                                    <div class="text" id="department"></div>
                                                </li>
                                                <li>
                                                    <div class="title">Designation:</div>
                                                    <div class="text" id="designations"></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Office Tab -->

                    <!-- Bank Statutory Tab -->
                    <div class="tab-pane fade" id="bank_details">
                        <div class="row">
                            <div class="col-md-6 d-flex">
                                <div class="card profile-box flex-fill">
                                    <div class="card-body">
                                        <h3 class="card-title"><strong>Bank information</strong></h3>
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Bank Name:</div>
                                                <div class="text" id="bankname"></div>
                                            </li>
                                            <li>
                                                <div class="title">Branch Name:</div>
                                                <div class="text1" id="bankbranchname"></div>
                                            </li>
                                            <li>
                                                <div class="title">Holder Name:</div>
                                                <div class="text" id="bankholdername"></div>
                                            </li>
                                            <li>
                                                <div class="title">Account No:</div>
                                                <div class="text" id="bankaccountno"></div>
                                            </li>
                                            <li>
                                                <div class="title">IFSC Code:</div>
                                                <div class="text" id="ifsecode"></div>
                                            </li>
                                            <li>
                                                <div class="title">PAN No:</div>
                                                <div class="text" id="pancardno"></div>
                                            </li>
                                            <li>
                                                <div class="title">Aadhaar No:</div>
                                                <div class="text" id="aadharno"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Bank Statutory Tab -->
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Edit Personal Info Modal -->
<div id="personal_info" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Profile Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editpesonal" method="POST">
                    <div class="row clearfix">
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Passport No.<span class="text-danger"></span></label><br />
                            <span class="text-danger change-pos" id="editpassportnovalid"></span>
                            <input value="" name="editpassportno" id="editpassportno" class="form-control" type="text" maxlength="10" autocomplete="off" placeholder="Please enter your passport no." />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Passport Exp Date<span class="text-danger"></span></label><br />
                            <span class="text-danger change-pos" id="editpassportexpiryvalid"></span>
                            <input value="" data-provide="datepicker" data-date-autoclose="true" name="editpassportexpiry" id="editpassportexpiry" class="form-control" placeholder="yyyy/mm/dd" type="text" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Nationality<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editnationalityvalid"></span>
                            <select value="" name="editnationality" id="editnationality" class="custom-select form-control"> </select>
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Religion<span class="text-danger">*</span></label><br />
                            <span class="text-danger change-pos" id="editreligionvalid"></span>
                            <select value="" name="editreligion" id="editreligion" class="custom-select form-control"> </select>
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Marital status<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editmaritalstatusvalid"></span>
                            <select value="" name="editmaritalstatus" id="editmaritalstatus" class="custom-select form-control"> </select>
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>No. of children<span class="text-danger"> </span></label><br />
                            <span class="text-danger change-pos" id="editnoofchildrenvalid"></span>
                            <input value="" name="editnoofchildren" id="editnoofchildren" class="form-control" type="number" autocomplete="off" onkeypress="return onlyNumberKey(event)" />
                        </div>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editpersonalinfo2" class="btn btn-primary submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Personal Info Modal -->

<!-- Edit Emergency Info Modal -->
<div id="emergency_contact" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Emergency Contact Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editrelative" method="POST">
                    <div class="row clearfix">
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Name<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editnamevalid"></span>
                            <input value="" name="editfamilyname" id="editfamilyname" class="form-control" type="text" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Relationship<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editrelationshipvalid"></span>
                            <select value="" name="editfamilyrelationship" id="editfamilyrelationship" class="form-control custom-select"> </select>
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Phone<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editemergencyphonevalid"></span>
                            <input value="" name="editfamilyphone" id="editfamilyphone" class="form-control" type="tel" maxlength="10" onkeypress="return onlyNumberKey(event)" autocomplete="off" />
                        </div>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editemergencyinfo" class="btn btn-primary submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Emergency Info Modal -->

<!-- Add Experience Info Modal -->
<div id="add_experience" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Experience</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addexperienceDATA" method="POST">
                    <div class="row clearfix">
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Company Name<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addcompanynamevalid"></span>
                            <input name="addcompanyname" id="addcompanyname" class="form-control" placeholder="Enter company name." type="text" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Company Location<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addlocationvalid"></span>
                            <input name="addlocation" id="addlocation" class="form-control" type="text" placeholder="Enter company location." autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Job Position<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addjobpositionvalid"></span>
                            <input name="addjobposition" id="addjobposition" class="form-control" type="text" placeholder="Enter job position." autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Period From<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addperiodfromvalid"></span>
                            <input data-provide="datepicker" name="addperiodfrom" id="addperiodfrom" class="form-control" type="text" placeholder="YYYY/MM/DD" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Period To<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addperiodtoevalid"></span>
                            <input data-provide="datepicker" name="addperiodto" id="addperiodto" class="form-control" type="text" readonly="" placeholder="YYYY/MM/DD" autocomplete="off" />
                        </div>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="addexperiencedetails" class="btn btn-primary submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Experience Info Modal -->

<!-- Edit Experience Info Modal -->
<div id="user_experience" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Experience Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="edit_experience_form" method="POST">
                    <div class="row clearfix">
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Company Name<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editcompanynamevalid"></span>
                            <input value="" name="editcompanyname" id="editcompanyname" class="form-control" type="text" autocomplete="off" />
                            <input value="" id="editcompanynameid" name="editcompanynameid" class="form-control" type="hidden" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Company Location<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editlocationvalid"></span>
                            <input value="" name="editlocation" id="editlocation" class="form-control" type="text" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Job Position<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editjobpositionvalid"></span>
                            <input value="" name="editjobposition" id="editjobposition" class="form-control" type="text" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Period From<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editperiodfromvalid"></span>
                            <input value="" data-provide="datepicker" name="editperiodfrom" id="editperiodfrom" class="form-control" type="text" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Period To<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editperiodtoevalid"></span>
                            <input value="" data-provide="datepicker" name="editperiodto" id="editperiodto" class="form-control" type="text" readonly="" autocomplete="off" />
                        </div>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editexperiencedetail" class="btn btn-primary submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Experience Info Modal -->

<!-- Delete Experience Modal -->
<div class="modal custom-modal fade" id="delete_experience" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" id="" class="btn btn-danger continue-btn delete_experience_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Branch Modal -->

<!-- Add Education Info Modal -->
<div id="add_education" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Education Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addeducation" method="POST">
                    <div class="row clearfix">

                        <div class="form-group col-sm-6 col-md-6">
                            <label>Degree<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addeducationdegreevalid"></span>
                            <input name="addeducationdegree" id="addeducationdegree" class="form-control" type="text" placeholder="Enter degree." autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Institution/ School/ College<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addinstitutionvalid"></span>
                            <input name="addinstitution" id="addinstitution" class="form-control" type="text" placeholder="Enter details." autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Subject<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addsubjectvalid"></span>
                            <input name="addsubject" id="addsubject" class="form-control" type="text" placeholder="Enter subject." autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Complete Date<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addenddatevalid"></span>
                            <input data-provide="datepicker" name="addenddate" id="addenddate" class="form-control" type="text" placeholder="YYYY/MM/DD" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Grade<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addgradevalid"></span>
                            <input name="addgrade" id="addgrade" class="form-control" placeholder="Enter grade." type="text" />
                        </div>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="addeducationdetails" class="btn btn-primary submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Education Info Modal -->

<!-- Edit Education Info Modal -->
<div id="user_education" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Education Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editeducation" method="POST">
                    <div class="row clearfix">
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Institution<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editinstitutionvalid"></span>
                            <input value="" name="editinstitution" id="editinstitution" class="form-control" type="text" autocomplete="off" />
                            <input value="" name="editinstitutionid" id="editinstitutionid" class="form-control" type="hidden" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Subject<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editsubjectvalid"></span>
                            <input value="" name="editsubject" id="editsubject" class="form-control" type="text" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Complete Date<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editenddatevalid"></span>
                            <input value="" data-provide="datepicker" name="editenddate" id="editenddate" class="form-control" type="text" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Degree<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editeducationdegreevalid"></span>
                            <input value="" name="editeducationdegree" id="editeducationdegree" class="form-control" type="text" autocomplete="off" />
                        </div>
                        <div class="form-group col-sm-6 col-md-6">
                            <label>Grade<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="editgradevalid"></span>
                            <input value="" name="editgrade" id="editgrade" class="form-control" type="text" autocomplete="off" />
                        </div>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editeducationdetails" class="btn btn-primary submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Education Info Modal -->

<!-- Delete Experience Modal -->
<div class="modal custom-modal fade" id="delete_education" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" id="" class="btn btn-danger continue-btn delete_education_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /Delete Branch Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

           

    // For remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#departmentname").val("");
            $("span#addbranchvalidate").prop("");
            $("input#addcompanyname").val("");
            $("input#addlocation").val("");
            $("input#addjobposition").val("");
            $("input#addperiodfrom").val("");
            $("input#addperiodtoe").val("");
            $("#addinstitution").val("");
            $("#addsubject").val("");
            $("#addenddate").val("");
            $("#addeducationdegree").val("");
            $("#addgrade").val("");
            $("#addbranchid option:eq(0)").prop("selected", true);
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#editlastnamevalid").text("");
            jQuery("#editphonevalid").text("");
            jQuery("#editpersonalemailvalid").text("");
            jQuery("#editfathernamevalid").text("");
            jQuery("#editdobvalid").text("");
            jQuery("#editgendervalid").text("");
            jQuery("#editaddress").text("");

            jQuery("#editpassportnovalid").text("");
            jQuery("#editpassportexpiryvalid").text("");
            jQuery("#editnationalityvalid").text("");
            jQuery("#editreligionvalid").text("");
            jQuery("#editmaritalstatusvalid").text("");
            jQuery("#editnoofchildrenvalid").text("");

            jQuery("#editfamilymembervalid").text("");
            jQuery("#edituserrelationshipvalid").text("");
            jQuery("#editmemberphonevalid").text("");

            jQuery("#editfamilymembervalid").text("");
            jQuery("#edituserrelationshipvalid").text("");
            jQuery("#editmemberphonevalid").text("");

            jQuery("#editinstitutionvalid").text("");
            jQuery("#editsubjectvalid").text("");
            jQuery("#editenddatevalid").text("");
            jQuery("#editeducationdegreevalid").text("");
            jQuery("#editgradevalid").text("");

            jQuery("#addcompanynamevalid").text("");
            jQuery("#addlocationvalid").text("");
            jQuery("#addjobpositionvalid").text("");
            jQuery("#addperiodfromvalid").text("");
            jQuery("#addperiodtoevalid").text("");

            jQuery("#editcompanynamevalid").text("");
            jQuery("#editlocationvalid").text("");
            jQuery("#editjobpositionvalid").text("");
            jQuery("#editperiodfromvalid").text("");
            jQuery("#editperiodtoevalid").text("");

            jQuery("#addinstitutionvalid").text("");
            jQuery("#addsubjectvalid").text("");
            jQuery("#addenddatevalid").text("");
            jQuery("#addeducationdegreevalid").text("");
            jQuery("#addgradevalid").text("");

            jQuery("#editinstitutionvalid").text("");
            jQuery("#editsubjectvalid").text("");
            jQuery("#editenddatevalid").text("");
            jQuery("#editeducationdegreevalid").text("");
            jQuery("#editgradevalid").text("");
        });
    });
// Select Gender by api
$.ajax({
        url: base_url + "viewRelationnamePublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editfamilyrelationship");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select gender</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.relation).text(entry.relation));
        });
    });
    

    // Show Profile list
    $.ajax({
        url: base_url + "viewProfile",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            var joiningdate = new Date(response.data[0].joining_date);
            var dd = String(joiningdate.getDate()).padStart(2, "0");
            var mm = String(joiningdate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = joiningdate.getFullYear();

            joiningdate = dd + "-" + mm + "-" + yyyy;
            if (!$.trim(response.data[0].user_image)) {
                $("#fgfimage").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
            } else {
                $("#fgfimage").append('<img src="' + response.data[0].user_image + '" />');
            }

            $("#profilenames").html(response.data[0].first_name);
            $("#profiledesignations").html(response.data[0].designation_name);
            $("#profiledepartment").html(response.data[0].department_name);
            $("#empid").html(response.data[0].emp_id);
            $("#joiningdate").html(joiningdate);

            $("#phone").html(response.data[0].phone);
            $("#email").html(response.data[0].personal_email);
            $("#fathername").html(response.data[0].father_name);
            var dobdate = new Date(response.data[0].dob);
            var dd = String(dobdate.getDate()).padStart(2, "0");
            var mm = String(dobdate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = dobdate.getFullYear();

            dobdate = dd + "-" + mm + "-" + yyyy;
            $("#dob").html(dobdate);
            $("#address").html(response.data[0].address);
            $("#gender").html(response.data[0].gender);

            $("#passportno").html(response.data[0].passport_no);


            var passportexpiry = new Date(response.data[0].passport_expiry);
            var dd = String(passportexpiry.getDate()).padStart(2, "0");
            var mm = String(passportexpiry.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = passportexpiry.getFullYear();

            passportexpiry = dd + "-" + mm + "-" + yyyy;


            var passportexpiry = "";
                if(response.data[0].passport_expiry == '0000-00-00'){
                    passportexpiry = 'Nil';  
                }else{
                 var start = passportexpiry;
                 passportexpiry = start;
                }
            $("#passportexpiry").html(passportexpiry);
            $("#nationality").html(response.data[0].nationality);
            $("#religion").html(response.data[0].religion);
            $("#maritalstatus").html(response.data[0].marital_status);
            $("#noofchildren").html(response.data[0].no_of_children);

            $("#emergencyname").html(response.data[0].name);
            $("#emergencyrelationship").html(response.data[0].relationship);
            $("#emergencyphone").html(response.data[0].emergency_phone);

            $("#company_name").html(response.data[0].company_name);
            $("#location").html(response.data[0].location);
            $("#job_position").html(response.data[0].job_position);
            $("#period_from").html(response.data[0].period_from);
            $("#period_to").html(response.data[0].period_to);

            $("#institution").html(response.data[0].institution);
            $("#degree").html(response.data[0].education_degree);
            $("#grade").html(response.data[0].grade);
            $("#subject").html(response.data[0].subject);
            var startDate = new Date(response.data[0].start_date);
            var dd = String(startDate.getDate()).padStart(2, "0");
            var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = startDate.getFullYear();
            startDate = dd + "-" + mm + "-" + yyyy;

            var endDate = new Date(response.data[0].end_date);
            var dd = String(endDate.getDate()).padStart(2, "0");
            var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = endDate.getFullYear();
            endDate = dd + "-" + mm + "-" + yyyy;

            $("#start_date").html(startDate);
            $("#end_date").html(endDate);

            $("#branchname").html(response.data[0].branch_name);
            $("#department").html(response.data[0].department_name);
            $("#designations").html(response.data[0].designation_name);
            $("#rolename").html(response.data[0].role_name);
            $("#companyName").html(response.data[0].company_name);

            $("#bankname").html(response.data[0].bank_name);
            $("#bankbranchname").html(response.data[0].branch);
            $("#bankholdername").html(response.data[0].holder_name);
            $("#bankaccountno").html(response.data[0].account_no);
            $("#ifsecode").html(response.data[0].ifsc);
            $("#pancardno").html(response.data[0].pan_card);
            $("#aadharno").html(response.data[0].aadhar_card);

            $("#familyname").html(response.data[0].contactperson_name);
            $("#familyrelationship").html(response.data[0].relationship);
            $("#familyphone").html(response.data[0].contactperson_no);
            // For Edit
            $("#editfamilyname").val(response.data[0].contactperson_name);
            $("#editfamilyrelationship").val(response.data[0].relationship);
            $("#editfamilyphone").val(response.data[0].contactperson_no);
        },
    });
    // Edit Relative form
    $("#editrelative").submit(function (e) {
        var formData = {
            contactperson_name: $("input[name=editfamilyname]").val(),
            relationship: $("select[name=editfamilyrelationship]").val(),
            contactperson_no: $("input[name=editfamilyphone]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editPersonalDetails",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editrelative [type=submit]").attr("disabled", false);
            }
        });
    });
    // Select Gender by api
    $.ajax({
        url: base_url + "viewGenderPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editgender");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select gender</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.gender).text(entry.gender));
        });
    });

    // Select Nationality by api
    $.ajax({
        url: base_url + "viewNationalityPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editnationality");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select nationality</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.nationality).text(entry.nationality));
        });
    });

    // Select Religion by api
    $.ajax({
        url: base_url + "viewReligionPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editreligion");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select religion</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.religion).text(entry.religion));
        });
    });

    // Select Martial Status by api
    $.ajax({
        url: base_url + "viewMartialPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editmaritalstatus");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select marital status</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.status).text(entry.status));
        });
    });

    // Edit Profile view list
    $.ajax({
        url: base_url + "viewProfile",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#editphone").val(response["data"][0]["phone"]);
            $("#editpersonalemail").val(response.data[0]["personal_email"]);
            $("#editfathername").val(response.data[0]["father_name"]);
            $("#editfirstname").val(response.data[0]["first_name"]);
            $("#editdob").val(response.data[0]["dob"]);
            $("#editaddress").val(response.data[0]["address"]);
            $("#editgender").val(response.data[0]["gender"]);
            $("#editname").val(response.data[0]["name"]);
            $("#editrelationship").val(response.data[0]["relationship"]);
            $("#editemergencyphone").val(response.data[0]["emergency_phone"]);
            $("#editpassportno").val(response.data[0]["passport_no"]);
            $("#editpassportexpiry").val(response.data[0]["passport_expiry"]);
            $("#editnationality").val(response.data[0]["nationality"]);
            $("#editreligion").val(response.data[0]["religion"]);
            $("#editmaritalstatus").val(response.data[0]["marital_status"]);
            $("#editnoofchildren").val(response.data[0]["no_of_children"]);
            $("#editcompanyname").val(response.data[0]["company_name"]);
            $("#editlocation").val(response.data[0]["location"]);
            $("#editjobposition").val(response.data[0]["job_position"]);
            $("#editperiodfrom").val(response.data[0]["period_from"]);
            $("#editperiodto").val(response.data[0]["period_to"]);
            $("#editinstitution").val(response.data[0]["institution"]);
            $("#editsubject").val(response.data[0]["subject"]);
            $("#editstartdate").val(response.data[0]["start_date"]);
            $("#editenddate").val(response.data[0]["end_date"]);
            $("#editeducationdegree").val(response.data[0]["education_degree"]);
            $("#editgrade").val(response.data[0]["grade"]);
        },
    });
 // Edit Relative form
 $("#edit_user").submit(function (e) {
        var formData = {
            first_name: $("input[name=editfirstname]").val(),
            phone: $("input[name=editphone]").val(),
            personal_email: $("input[name=editpersonalemail]").val(),
            father_name: $("input[name=editfathername]").val(),
            dob: $("input[name=editdob]").val(),
            gender: $("select[name=editgender]").val(),
            address: $("textarea[name=editaddress]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editDetailsprofile",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editrelative [type=submit]").attr("disabled", false);
            }
        });
    });
    // Edit Relative form
    $("#editrelative").submit(function (e) {
        var formData = {
            name: $("input[name=editname]").val(),
            relationship: $("input[name=editrelationship]").val(),
            emergency_phone: $("input[name=editemergencyphone]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editRelativeDetails",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editrelative [type=submit]").attr("disabled", false);
            }
        });
    });

    // Edit personal Details form*/
    $("#editpesonal").submit(function (e) {
        var formData = {
            passport_no: $("input[name=editpassportno]").val(),
            passport_expiry: $("input[name=editpassportexpiry]").val(),
            nationality: $("select[name=editnationality]").val(),
            religion: $("select[name=editreligion]").val(),
            marital_status: $("select[name=editmaritalstatus]").val(),
            no_of_children: $("input[name=editnoofchildren]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editPersonalDetails",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editpesonal [type=submit]").attr("disabled", false);
            }
        });
    });

    // Show Experience
    $.ajax({
        url: base_url + "viewExperienceEmployee",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        for (i in response.data) {
            if (response.data[i].intern == 1 ) {
                var html =
                '<div class="col-lg-12"><div class="card"><div class="card-header"></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! You Have No Experience</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            var periodfromexp = new Date(response.data[i].period_from);
            var dd = String(periodfromexp.getDate()).padStart(2, "0");
            var mm = String(periodfromexp.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = periodfromexp.getFullYear();
            periodfromexp = dd + "-" + mm + "-" + yyyy;

            var periodtoexp = new Date(response.data[i].period_to);
            var dd = String(periodtoexp.getDate()).padStart(2, "0");
            var mm = String(periodtoexp.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = periodtoexp.getFullYear();
            periodtoexp = dd + "-" + mm + "-" + yyyy;



            var periodfromexp = "";
            if(response.data[0].period_from == '0000-00-00'){
                periodfromexp = 'Nil';  
            }else{
             var start = periodfromexp;
             periodfromexp = start;
            }

            var periodtoexp = "";
            if(response.data[0].period_to == '0000-00-00'){
                periodtoexp = 'Nil';  
            }else{
             var start = periodtoexp;
             periodtoexp = start;
            }
            
            var html =
                '<li><div class="experience-user"><div class="before-circle"></div></div><div class="experience-content"><div class="timeline-content"><a href="#/" class="name">' +
                response.data[i].company_name +
                '</a> <small class="float-right"> <a href="#" class="edit-icon delete_data" data-toggle="modal" data-target="#delete_experience" id="' +
                response.data[i].id +
                '"><i class="fa fa-trash-alt text-danger"></i></a> <a href="#" class="edit-icon edit_datae mr-2" data-toggle="modal" data-target="#user_experience" id="' +
                response.data[i].id +
                '"><i class="fa fa-edit text-info"></i></a> </small><div>' +
                response.data[i].location +
                "</div><div>" +
                response.data[i].job_position +
                '</div><span class="time">' + periodfromexp +
                '</span> - <span class="time">' +
                periodtoexp +
                "</span></div></div></li>";
            $(".kt-widget__items").append(html);
        }
        }
    });

    // Add  Experience list
    $("#addexperienceDATA").submit(function (e) {
        var formData = {
            company_name: $("input[name=addcompanyname]").val(),
            location: $("input[name=addlocation]").val(),
            job_position: $("input[name=addjobposition]").val(),
            period_from: $("input[name=addperiodfrom]").val(),
            period_to: $("input[name=addperiodto]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addExperienceDetails",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addexperienceDATA [type=submit]").attr("disabled", false);
            }
        });
    });

    // Edit  Experience Form Fill
    $(document).on("click", ".edit_datae", function () {
        var experience_id = $(this).attr("id");

        $.ajax({
            url: base_url + "viewExperienceEmployee",
            method: "POST",
            data: {
                experience_id: experience_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editcompanyname").val(response["data"][0]["company_name"]);
                $("#editcompanynameid").val(response["data"][0]["id"]);
                $("#editlocation").val(response["data"][0]["location"]);
                $("#editjobposition").val(response["data"][0]["job_position"]);
                $("#editperiodfrom").val(response["data"][0]["period_from"]);
                $("#editperiodto").val(response["data"][0]["period_to"]);
                $("#user_experience").modal("show");
            },
        });
    });

    // Edit Experience form
    $("#edit_experience_form").submit(function (e) {
        var formData = {
            experience_id: $("input[name=editcompanynameid]").val(),
            company_name: $("input[name=editcompanyname]").val(),
            location: $("input[name=editlocation]").val(),
            job_position: $("input[name=editjobposition]").val(),
            period_from: $("input[name=editperiodfrom]").val(),
            period_to: $("input[name=editperiodto]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editExperienceDetails",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#edit_experience_form [type=submit]").attr("disabled", false);
            }
        });
    });

    //Delete Experience
    $(document).on("click", ".delete_data", function () {
        var experience_id = $(this).attr("id");
        $(".delete_experience_button").attr("id", experience_id);
        $("#delete_experience").modal("show");
    });

    $(document).on("click", ".delete_experience_button", function () {
        var experience_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteExperienceData",
            method: "POST",
            data: {
                experience_id: experience_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                window.location.replace("<?php echo base_url() ?>Profile");
            },
        });
    });

    // Show Education
    $.ajax({
        url: base_url + "viewEducationbEmployee",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        for (i in response.data) {
            var enddateedu = new Date(response.data[i].end_date);
            var dd = String(enddateedu.getDate()).padStart(2, "0");
            var mm = String(enddateedu.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = enddateedu.getFullYear();
            enddateedu = dd + "-" + mm + "-" + yyyy;
            var html =
                '<li><div class="experience-user"><div class="before-circle"></div></div><div class="experience-content"><div class="timeline-content"><a href="#/" class="name">' +
                response.data[i].institution +
                '</a> <small class="float-right"> <a href="#" class="edit-icon delete_datap" data-toggle="modal" data-target="#delete_education" id="' +
                response.data[i].id +
                '"><i class="fa fa-trash-alt text-danger"></i></a> <a href="#" class="edit-icon edit_datap mr-2" data-toggle="modal" data-target="#user_education" id="' +
                response.data[i].id +
                '"><i class="fa fa-edit text-info"></i></a> </small><div>' +
                response.data[i].education_degree +
                "</div><div>" +
                response.data[i].subject +
                "</div><div>" +
                response.data[i].grade +
                '</div><span class="time">' +enddateedu +
                "</span> </div></div></li>";
            $(".kt-widget__education").append(html);
        }
    });

    // Add Education list
    $("#addeducation").submit(function (e) {
        var formData = {
            institution: $("input[name=addinstitution]").val(),
            subject: $("input[name=addsubject]").val(),
            end_date: $("input[name=addenddate]").val(),
            education_degree: $("input[name=addeducationdegree]").val(),
            grade: $("input[name=addgrade]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addEducationDetails",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addeducation [type=submit]").attr("disabled", false);
            }
        });
    });

    // Edit Education Form Fill
    $(document).on("click", ".edit_datap", function () {
        var education_id = $(this).attr("id");

        $.ajax({
            url: base_url + "viewEducationbEmployee",
            method: "POST",
            data: {
                education_id: education_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editinstitution").val(response["data"][0]["institution"]);
                $("#editinstitutionid").val(response["data"][0]["id"]);
                $("#editsubject").val(response["data"][0]["subject"]);
                $("#editenddate").val(response["data"][0]["end_date"]);
                $("#editeducationdegree").val(response["data"][0]["education_degree"]);
                $("#editgrade").val(response["data"][0]["grade"]);
                $("#user_education").modal("show");
            },
        });
    });

    // Edit Education form
    $("#editeducation").submit(function (e) {
        var formData = {
            education_id: $("input[name=editinstitutionid]").val(),
            institution: $("input[name=editinstitution]").val(),
            subject: $("input[name=editsubject]").val(),
            end_date: $("input[name=editenddate]").val(),
            education_degree: $("input[name=editeducationdegree]").val(),
            grade: $("input[name=editgrade]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editEducation",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Profile";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editeducation [type=submit]").attr("disabled", false);
            }
        });
    });

    //Delete Education
    $(document).on("click", ".delete_datap", function () {
        var education_id = $(this).attr("id");
        $(".delete_education_button").attr("id", education_id);
        $("#delete_education").modal("show");
    });

    $(document).on("click", ".delete_education_button", function () {
        var education_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteEducationData",
            method: "POST",
            data: {
                education_id: education_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                window.location.replace("<?php echo base_url() ?>Profile");
            },
        });
    });

    //Add Personal Information Validation
    $(document).ready(function () {
        $("#editpersonalinfo").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editgender").val() == null) {
                $("#editgender").focus();
                $("#editgendervalid").text("Please select gender.");
                errorCount++;
            } else {
                $("#editgendervalid").text("");
            }

            /*if ($("#edifirstname").val().trim() == "") {
                $("#edifirstname").focus();
                $("#edifirstnamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#edifirstnamevalid").text("");
            }*/

            if ($("#editphone").val().length < 10) {
                $("#editphone").focus();
                $("#editphonevalid").text("Enter valid conatct no.");
                errorCount++;
            } else {
                $("#editphonevalid").text("");
            }

            if ($("#editpersonalemail").val().trim() == "") {
                $("#editpersonalemail").focus();
                $("#editpersonalemailvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editpersonalemailvalid").text("");
            }

            if ($("#editfathername").val().trim() == "") {
                $("#editfathername").focus();
                $("#editfathernamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editfathernamevalid").text("");
            }

            if ($("#editdob").val().trim() == "") {
                $("#editdob").focus();
                $("#editdobvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editdobvalid").text("");
            }

            if ($("#editaddress").val().trim() == "") {
                $("#editaddress").focus();
                $("#editaddressvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editaddressvalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Add Personal Information 2 Validation
    $(document).ready(function () {
        $("#editpersonalinfo2").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editnationality").val() == null) {
                $("#editnationality").focus();
                $("#editnationalityvalid").text("Please select nationality.");
                errorCount++;
            } else {
                $("#editnationalityvalid").text("");
            }

            if ($("#editreligion").val() == null) {
                $("#editreligion").focus();
                $("#editreligionvalid").text("Please select religion.");
                errorCount++;
            } else {
                $("#editreligionvalid").text("");
            }

            if ($("#editmaritalstatus").val() == null) {
                $("#editmaritalstatus").focus();
                $("#editmaritalstatusvalid").text("Please select marital status.");
                errorCount++;
            } else {
                $("#editmaritalstatusvalid").text("");
            }

            if ($("#editpassportno").val().length < 10) {
                $("#editpassportno").focus();
                $("#editpassportnovalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editpassportnovalid").text("");
            }

            if ($("#editpassportexpiry").val().trim() == "") {
                $("#editpassportexpiry").focus();
                $("#editpassportexpiryvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editpassportexpiryvalid").text("");
            }

           /* if ($("#editnoofchildren").val().trim() == "") {
                $("#editnoofchildren").focus();
                $("#editnoofchildrenvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editnoofchildrenvalid").text("");
            }*/

            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Add Family Information Validation
    $(document).ready(function () {
        $("#editfamilyinfo").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;

            if ($("#editfamilymember").val().trim() == "") {
                $("#editfamilymember").focus();
                $("#editfamilymembervalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editfamilymembervalid").text("");
            }

            if ($("#editmemberphone").val().length < 10) {
                $("#editmemberphone").focus();
                $("#editmemberphonevalid").text("Enter valid conatct no.");
                errorCount++;
            } else {
                $("#editmemberphonevalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Add Emergency Information Validation
    $(document).ready(function () {
        $("#editemergencyinfo").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;

            if ($("#editname").val().trim() == "") {
                $("#editname").focus();
                $("#editnamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editnamevalid").text("");
            }

            if ($("#editemergencyphone").val().length < 10) {
                $("#editemergencyphone").focus();
                $("#editemergencyphonevalid").text("Enter valid conatct no.");
                errorCount++;
            } else {
                $("#editemergencyphonevalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

    // Edit Education validation form
    $(document).ready(function () {
        $("#editeducationdetails").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editinstitution").val().trim() == "") {
                $("#editinstitution").focus();
                $("#editinstitutionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editinstitutionvalid").text("");
            }

            if ($("#editsubject").val().trim() == "") {
                $("#editsubject").focus();
                $("#editsubjectvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editsubjectvalid").text("");
            }

            if ($("#editenddate").val().trim() == "") {
                $("#editenddate").focus();
                $("#editenddatevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editenddatevalid").text("");
            }

            if ($("#editeducationdegree").val().trim() == "") {
                $("#editeducationdegree").focus();
                $("#editeducationdegreevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editeducationdegreevalid").text("");
            }

            if ($("#editgrade").val().trim() == "") {
                $("#editgrade").focus();
                $("#editgradevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editgradevalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    // Edit Education validation form
    $(document).ready(function () {
        $("#editexperiencedetail").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editcompanyname").val().trim() == "") {
                $("#editcompanyname").focus();
                $("#editcompanynamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editcompanynamevalid").text("");
            }

            if ($("#editlocation").val().trim() == "") {
                $("#editlocation").focus();
                $("#editlocationvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editlocationvalid").text("");
            }

            if ($("#editjobposition").val().trim() == "") {
                $("#editjobposition").focus();
                $("#editjobpositionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editjobpositionvalid").text("");
            }

            if ($("#editperiodfrom").val().trim() == "") {
                $("#editperiodfromvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editperiodfromvalid").text("");
            }

            if ($("#editperiodto").val().trim() == "") {
                $("#editperiodtoevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editperiodtoevalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Add Education validation form
    $(document).ready(function () {
        $("#addeducationdetails").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addinstitution").val().trim() == "") {
                $("#addinstitution").focus();
                $("#addinstitutionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addinstitutionvalid").text("");
            }

            if ($("#addsubject").val().trim() == "") {
                $("#addsubject").focus();
                $("#addsubjectvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addsubjectvalid").text("");
            }

            if ($("#addenddate").val().trim() == "") {
                $("#addenddatevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addenddatevalid").text("");
            }

            if ($("#addeducationdegree").val().trim() == "") {
                $("#addeducationdegree").focus();
                $("#addeducationdegreevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addeducationdegreevalid").text("");
            }

            if ($("#addgrade").val().trim() == "") {
                $("#addgrade").focus();
                $("#addgradevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addgradevalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    // Add Education validation form
    $(document).ready(function () {
        $("#addexperiencedetails").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addcompanyname").val().trim() == "") {
                $("#addcompanyname").focus();
                $("#addcompanynamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addcompanynamevalid").text("");
            }

            if ($("#addlocation").val().trim() == "") {
                $("#addlocation").focus();
                $("#addlocationvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addlocationvalid").text("");
            }

            if ($("#addjobposition").val().trim() == "") {
                $("#addjobposition").focus();
                $("#addjobpositionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addjobpositionvalid").text("");
            }

            if ($("#addperiodfrom").val().trim() == "") {
                $("#addperiodfromvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addperiodfromvalid").text("");
            }

            if ($("#addperiodto").val().trim() == "") {
                $("#addperiodtoevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addperiodtoevalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
