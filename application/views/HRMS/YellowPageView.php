<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>YellowPageList">Yellow Pages</a></li>
                <li class="breadcrumb-item active">Yellow Page Profile</li>
            </ul>
        </div>

        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card card-profile mb-0">
                    <div class="card-body">
                        <div class="profile-view">
                            <div class="profile-img-wrap">
                                <div class="profile-img" id="fgfimage">
                                    <a href="#"><img alt="" src="<%=chartUrl.toString()%>" /></a>
                                </div>
                            </div>
                            <div class="profile-basic">
                                <div class="row clearfix">
                                    <div class="col-md-4">
                                        <div class="profile-info-left">
                                            <h4 class="user-name m-t-0 mb-0"><span id="profilenames"></span> <span id="profilelastnames"></span></h4>
                                            <div class="staff-id"><span id="profiledesignations"></span> - <span id="profiledepartment"></span></div>
                                            <div class="staff-id" id="empid"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <ul class="personal-info">
                                            <li>
                                                <div class="title">Father Name:</div>
                                                <div class="text" id="fathername"></div>
                                            </li>
                                            <li>
                                                <div class="title">Phone:</div>
                                                <div class="text" id="phone"></div>
                                            </li>
                                            <li>
                                                <div class="title">Email:</div>
                                                <div class="text" id="email"></div>
                                            </li>
                                            <li>
                                                <div class="title">Date of Birth:</div>
                                                <div class="text" id="dob"></div>
                                            </li>
                                            <li>
                                                <div class="title">Gender:</div>
                                                <div class="text" id="gender"></div>
                                            </li>
                                            <li>
                                                <div class="title">Address:</div>
                                                <div class="text1" id="address"></div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-md-6 d-flex">
                <div class="card profile-box flex-fill">
                    <div class="card-header">
                        <h3 class="card-title">Office Details</h3>
                    </div>
                    <div class="card-body">
                        <ul class="personal-info">
                           
                            <li>
                                <div class="title">Branch Name:</div>
                                <div class="text" id="branchname"></div>
                            </li>
                            <li>
                                <div class="title">Department:</div>
                                <div class="text" id="department"></div>
                            </li>
                            <li>
                                <div class="title">Designation:</div>
                                <div class="text" id="designations"></div>
                            </li>
                            <li>
                                <div class="title">Joining Date:</div>
                                <div class="text" id="joindate"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Show Profile list
    var user_code = "<?php echo $userid; ?>";
    $.ajax({
        url: base_url + "viewYellowPageUsers",
        data: { user_code: user_code },
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            var dob = new Date(response.data[0].dob);
            var dd = String(dob.getDate()).padStart(2, "0");
            var mm = String(dob.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = dob.getFullYear();

            dob = dd + "-" + mm + "-" + yyyy;

            var joiningdate = new Date(response.data[0].joining_date);
            var dd = String(joiningdate.getDate()).padStart(2, "0");
            var mm = String(joiningdate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = joiningdate.getFullYear();

            joiningdate = dd + "-" + mm + "-" + yyyy;
            if (!$.trim(response.data[0].user_image)) {
                UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
            } else {
                UserImage = response.data[0].user_image;
            }
            $("#fgfimage").html('<img src="' + UserImage + '" />');
            $("#profilenames").html(response.data[0].first_name);
            $("#profilelastnames").html(response.data[0].last_name);
            $("#profiledesignations").html(response.data[0].designation_name);
            $("#profiledepartment").html(response.data[0].department_name);
            $("#empid").html(response.data[0].emp_id);

            $("#fathername").html(response.data[0].father_name);
            $("#phone").html(response.data[0].phone);
            $("#email").html(response.data[0].personal_email);
            $("#dob").html(dob);
            $("#gender").html(response.data[0].gender);
            $("#address").html(response.data[0].address);

            $("#companyname").html(response.data[0].company_name);
            $("#branchname").html(response.data[0].branch_name);
            $("#department").html(response.data[0].department_name);
            $("#designations").html(response.data[0].designation_name);
            $("#rolename").html(response.data[0].role_name);
            $("#joindate").html(joiningdate);
            localStorage.removeItem("$userid");
        },
    });
</script>
