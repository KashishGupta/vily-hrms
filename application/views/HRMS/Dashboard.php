<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="row clearfix row-deck">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-2 text-center">
                                <div class="welcome-img" id="fgfimage"></div>
                            </div>
                            <div class="col-sm-7">
                                <h4>Welcome, <span id="profilenames"></span>!</h4>
                                <div class="staff-id text-muted"><span id="profiledesignations"></span> - <span id="profiledepartment"></span></div>
                                <div class="staff-id"><span id="empid"></span> - <span id="joiningdate"></span></div>
                            </div>
                            <div class="col-sm-3 lastLogin">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-6 col-md-6">
                <div class="card hello-card-scroll">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h2 class="card-title"><strong>My Leaves</strong></h2>
                    </div>
                    <table class="table card-table table-vcenter">
                        <tbody>
                            <tr>
                                <td class="text-left">
                                    <span class="tag tag-success" id="grade"></span>
                                </td>
                                <td class="text-left">Total Leave</td>
                            </tr>
                            <tr>
                                <td class="text-left">
                                    <span class="tag tag-success" id="totalApproveleave"></span>
                                </td>
                                <td class="text-left">Leave Taken</td>
                            </tr>
                            <tr>
                                <td class="text-left">
                                    <span class="tag tag-success" id="totalBalanceleave"></span>
                                </td>
                                <td class="text-left">Leave Balance</td>
                            </tr>
                            <tr>
                                <td class="text-left">
                                    <span class="tag tag-success">0</span>
                                </td>
                                <td class="text-left">Paid Leave</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="card-footer text-center custom-data">
                        <a href="<?php echo base_url(); ?>LeaveSelf_emp" class="btn btn-primary">View Details</a>
                        <a href="javascript:void(0);" class="btn btn-primary" data-toggle="modal" data-target="#add_leave">Apply Leave</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6">
                <div class="card scroll-bar">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>My Projects</strong></h3>
                    </div>
                    <div class="myproject_scroll">
                        <ol class="dd-list right_chat list-unstyled Kt-project"></ol>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card hello-scroll">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Tickets</strong></h3>
                    </div>
                    <ul class="right_chat list-unstyled Kt-ticketsKt"></ul>
                    <div class="card-footer text-center">
                        <a href="<?php echo base_url(); ?>Tickets_emp" class="btn btn-primary">View Details</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6">
                <div class="card scroll-section">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Jobs With Details</strong></h3>
                    </div>
                    <ul class="right_chat list-unstyled Kt-OpenJobs"></ul>
                    <div class="card-footer text-center">
                        <a href="<?php echo base_url(); ?>Jobs_emp" class="btn btn-primary">View All</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Attendance</strong></h3>
                    </div>
                    <div class="card-body">
                        <div class="punch-det">
                            <h6>Punch In at</h6>
                            <p>Wed, 11th Mar 2019 10.00 AM</p>
                        </div>
                        <div class="punch-det">
                            <h6>Punch Out at</h6>
                            <p>Wed, 20th Feb 2019 9.00 PM</p>
                        </div>
                        <div class="statistics">
                            <div class="row">
                                <div class="col-md-6 col-6 text-center">
                                    <div class="stats-box">
                                        <p>Break</p>
                                        <h6>1.21 hrs</h6>
                                    </div>
                                </div>
                                <div class="col-md-6 col-6 text-center">
                                    <div class="stats-box">
                                        <p>Overtime</p>
                                        <h6>3 hrs</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card data-scroll">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Task List</strong></h3>
                    </div>
                    <div class="todo_list task_scroll">
                        <table class="table table-hover table-bordered table-responsive mb-0 table-vcenter">
                            <thead class="thead-dark">
                                <tr>
                                    <th  class="text-left">Task Name</th>
                                    <th class="text-center">Due</th>
                                    <th class="text-center">Priority</th>
                                </tr>
                            </thead>
                            <tbody id="taskdatatableSelf"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Wishes and Greetings</strong></h3>
                    </div>
                    <div class="card-body scrolldata">
                        <div class="row clearfix Kt-Ulili"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Leave Modal -->
<div id="add_leave" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Leave</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addleave" method="POST" action="#">
                    <div class="form-group col-md-12">
                        <label>Leave Type <span class="text-danger">*</span></label>
                        <br>
                        <span id="addleavetypevalidate" class="text-danger change-pos"></span>
                        <select class="custom-select form-control" name="addleaveid" id="addleaveid">
                        
                        </select>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="test" name="example-inline-radios">
                            <span class="custom-control-label">Allowed To Half Day Leave</span>
                        </label>
                        <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="testshort" name="example-inline-radios">
                            <span class="custom-control-label">Allowed To short Leave</span>
                        </label>
                    </div>
                       
                    <div class="form-group col-md-12">
                        <label>From <span class="text-danger">*</span></label>
                        <br>
                        <span id="addleavestartdatevalidate" class="text-danger change-pos"></span>
                        <span id="addleavestartdatevalidatee" class="text-danger change-pos"></span>
                        <input data-provide="datepicker" data-date-autoclose="true" id="addleavestartdate" name="addleavestartdate" class="form-control " type="text" placeholder="yyyy/mm/dd" autocomplete="off" readonly>
                    </div>

                    <div class="form-group col-md-12">
                        <label>To <span class="text-danger">*</span></label>
                        <br>
                        <span id="addleaveenddatevalidate" class="text-danger change-pos"></span>
                        <input data-provide="datepicker" data-date-autoclose="true" id="addleaveenddate" name="addleaveenddate" class="form-control " type="text" placeholder="yyyy/mm/dd" autocomplete="off" readonly>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Leave Reason <span class="text-danger">*</span></label>
                        <br>
                        <span id="addleavereasonvalidate" class="text-danger change-pos"></span>
                        <textarea name="addleavereason" id="addleavereason" class="form-control" type="text" autocomplete="off"></textarea>
                    </div>

                    <div class="submit-section pull-right">
                        <button id="addleavebutton" class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Leave Modal -->

<!-- Delete Project Modal -->
<div class="modal custom-modal fade" id="StatusTask" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to incomplete task staus?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" class="btn btn-danger continue-btn delete_project_button" id="inactive_btn">Yes, incomplete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Project Modal -->

<!-- add Notification -->
<div id="add_notification" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Wish Now</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addmessageform" method="POST">
                    <div class="form-group col-md-12">
                        <label>Message <span class="text-danger">*</span></label>
                        <br />
                        <span id="editdepartmentvalidate" class="text-danger change-pos"></span>
                        <input type="text" id="addmessage" name="addmessage" class="form-control" placeholder="Please enter a message" autocomplete="off" />
                        <input value="" id="addnotificationuserid" name="addnotificationuserid" class="form-control" type="hidden" />
                    </div>

                    <div class="submit-section pull-right">
                        <button id="editdepartmentbutton" class="btn btn-success submit-btn">Send message</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /AssignTask Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    //for remove validation and empty input field
        $(document).ready(function() {
            $('.modal').click(function(){
                $('input#addleavestartdate').val('');
                $('input#addleaveenddate').val('');
                $('textarea#addleavereason').val('');                
                $('#addleaveid option:eq(0)').prop('selected', true)
            });
          
            $('.modal .modal-dialog .modal-body > form').click(function(e){
                e.stopPropagation();
            });
          
            $('form button[data-dismiss]').click(function(){
                $('.modal').click();
            });
        });
      
        $(document).ready(function() {
            $('.modal').click(function(){
                jQuery('#addleavetypevalidate').text('');
                jQuery('#addleavestartdatevalidate').text('');
                jQuery('#addleaveenddatevalidate').text('');
                jQuery('#addleavereasonvalidate').text('');
                jQuery('#addleavestartdatevalidatee').text('');
            });
        });
     
    //  Tickets View
    $.ajax({
        url: base_url + "viewTicketsLoggedInuser",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="col-md-12"><div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".Kt-ticketsKt").append(html);
        } else {
            for (i in response.data) {
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
                var html =
                    '<li class="online"><a href="javascript:void(0);"><div class="media"><img class="media-object " src="' +
                    UserImage +
                    '" alt=""><div class="media-body"><span class="name">' +
                    response.data[i].ticket_type +
                    '</span><span class="name">' +
                    response.data[i].title +
                    '</span><span class="message"><i class="fa fa-calendar"></i> ' +
                    response.data[i].date +
                    "</span></div></div></a></li>";
                $(".Kt-ticketsKt").append(html);
            }
        }
    });

    // View Job Public
    $.ajax({
        url: base_url + "viewJobPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="col-md-12"><div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".Kt-OpenJobs").append(html);
        } else {
            for (i in response.data) {
                var html =
                    '<li class="online"><a href="<?php echo base_url(); ?>Jobs"><div class="media"><div class="media-body"><span class="name">' +
                    response.data[i].job_title  +
                    '</span><span class="message">' +
                    response.data[i].department_name +
                    "</span></div></div></a> </li>";
                $(".Kt-OpenJobs").append(html);
            }
        }
    });

    //Show Profile list
    $.ajax({
        url: base_url + "viewProfile",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            if (!$.trim(response.data[0].user_image)) {
                $("#fgfimage").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
            } else {
                $("#fgfimage").append('<img src="' + response.data[0].user_image + '" />');
            }
            var today = new Date(response.data[0].joining_date);
            var dd = String(today.getDate()).padStart(2, "0");
            var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = today.getFullYear();

            today = dd + "-" + mm + "-" + yyyy;
            $("#profilenames").html(response.data[0].first_name);
            $("#profiledesignations").html(response.data[0].designation_name);
            $("#profiledepartment").html(response.data[0].department_name);
            $("#empid").html(response.data[0].emp_id);
            $("#joiningdate").html(today);
        },
    });
    // Show User Last Login
    $.ajax({
        url: base_url + "viewLastLogin",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            console.log(response);
            if (!$.trim(response.data[0])) {
               
               
        }else{
            var html = '<p class="login-status">Last Login- <span>'+response.data[0].time+'</span></p>';
            $(".lastLogin").append(html);
           
        }
        },
    });

    //Approve Leave
    $.ajax({
        url: base_url + "viewApprovedLeave",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#totalApproveleave").html(response.data);
        },
    });

    $.ajax({
        url: base_url + "leaveTotal",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },

        success: function (response) {
            $("#grade").html(response.data[0].leaves);
        },
    });

    //Select Leave Type by api
    $.ajax({
        url: base_url + "viewLgggedUserPloicies",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addleaveid");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Leave Type</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.leave_type));
        });
    });

    // Add leave 
        $("#addleave").submit(function(e) {
            if($('#test').prop("checked") == true){
            var halfleave = 1;
            }else{
                var halfleave = 0;
            }
            if($('#testshort').prop("checked") == true){
                var shortleave = 1;
            }else{
                var shortleave = 0;
            }  
            var formData = {
                'user_id': localStorage.userid,
                'leave_policy': $('select[name=addleaveid]').val(),
                'start_date': $('input[name=addleavestartdate]').val(),
                'end_date': $('input[name=addleaveenddate]').val(),
                'reason': $('textarea[name=addleavereason]').val(),
                'allow_half': halfleave,
                'allow_short': shortleave,
            };
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: base_url + 'addLeave',
                data: formData,
                dataType: 'json',
                encode: true,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                    setTimeout(function(){window.location ="<?php echo base_url()?>Home"},1000);
                  
                } else {
                    toastr.error(falsedata);
                    $('#addleave [type=submit]').attr('disabled',false);
                }
            });
        });

    // Fetch Leave Type
    function fetchLeaveType(leave_type_id) {
        $.ajax({
            url: base_url + "viewLeaveTypeSelected",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editleaveid");

            dropdown.empty();

            dropdown.append("<option disabled>Choose Leave Type</option>");
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                if (leave_type_id == entry.id) {
                    dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.leave_type));
                } else {
                    dropdown.append($("<option></option>").attr("value", entry.id).text(entry.leave_type));
                }
            });
        });
    }

    //Show Upcoming Birthday
    $.ajax({
        url: base_url + "checkUpcomingBirthday",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="col-md-12"><div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".Kt-Ulili").append(html);
        } else {
            for (i in response.data) {
                var today = new Date(response.data[i].dob);
                var dd = String(today.getDate()).padStart(2, "0");
                var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = today.getFullYear();

                today = dd + "-" + mm + "-" + yyyy;
                var birthdayImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    birthdayImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    birthdayImage = response.data[i].user_image;
                }
                var html =
                    '<div class="col-lg-4 col-md-12"><div class="card-1 mb-3"><div class="card-body w_user"><div class="user_avtar" style="width:100px"><img style="width:100%;height: 133px;" class="rounded-circle" src="' +
                    birthdayImage +
                    '" alt=""></div><div class="wid-u-info"><h5>' +
                    response.data[i].first_name +
                    '</h5><p class="text-muted m-b-0">' +
                    today +
                    '</p><p class="text-muted m-b-0">Birthday</p><ul class="list-unstyled"><li><button type="button" data-toggle="modal" data-target="#add_notification" aria-expanded="false" class="btn btn-info add_datanotification" title="Edit Department" id="' +
                    response.data[i].user_id +
                    '">Wish Now</button></li></ul></div></div></div></div>';
                $(".Kt-Ulili").append(html);
            }
        }
    });
    //add Notification Form Fill
    $(document).on("click", ".add_datanotification", function () {
        var user_id = $(this).attr("id");
        $.ajax({
            url: base_url + "checkUpcomingBirthday",
            method: "POST",
            data: {
                user_id: user_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#addnotificationuserid").val(response["data"][0]["id"]);

                $("#add_notification").modal("show");
            },
        });
    });
    // Add Message
    $("#addmessageform").submit(function (e) {
        var formData = {
            message: $("input[name=addmessage]").val(),
            user_id: $("input[name=addnotificationuserid]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addNotfication",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Home";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addmessageform [type=submit]").attr("disabled", false);
            }
        });
    });
    // Add Leave validation form-->
    $(document).ready(function () {
        $("#addleavebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addleaveid").val() == null) {
                $("#addleaveid").focus();
                $("#addleavetypevalidate").text("Please select a leave type.");
                errorCount++;
            } else {
                $("#addleavetypevalidate").text("");
            }

            if ($("#addleavestartdate").val().trim() == "") {
                $("#addleavestartdatevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addleavestartdatevalidate").text("");
            }

            if ($("#addleaveenddate").val().trim() == "") {
                $("#addleaveenddatevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addleaveenddatevalidate").text("");
            }

            if ($("#addleavereason").val().trim() == "") {
                $("#addleavereason").focus();
                $("#addleavereasonvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addleavereasonvalidate").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

     //Show Total Balance Leave Count
     $.ajax({
            url: base_url + "viewBalanceLeave",
            method: "POST",
            dataType: "json",
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            },
    
            success: function(response) {
              
               // var number = response.data[0].remained_leave;
    
               
                $("#totalBalanceleave").html(response.data);
            }
        });


    
    //Show Task
    $.ajax({
        url: base_url + "viewTaskSelf",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        var table = document.getElementById("taskdatatableSelf");
        for (i in response.data) {
            var testcheck = "";
            if (response.data[i].status == 1) {
                testcheck = "checked";
            }
            if (response.data[i].status == 0) {
                testcheck = "";
            }

            var colour = "";
            if (response.data[i].priority == 'High') {
                colour = "#db2828";
            }
            if (response.data[i].priority == 'Low') {
                colour = "#fbbd08";
            }
            if (response.data[i].priority == 'Medium') {
                colour = "#5a5278";
            }

            var today = new Date(response.data[i].due_date);
            var dd = String(today.getDate()).padStart(2, "0");
            var mm = String(today.getMonth() + 1).padStart(2, "0");
            var yyyy = today.getFullYear();

            today = dd + "-" + mm + "-" + yyyy;
            var tr = document.createElement("tr");
            tr.innerHTML =
                '<td><label class="custom-control custom-checkbox"><input type="checkbox" '+testcheck+' id="'+response.data[i].id+'" value="'+ response.data[i].status +'" onclick="getstatus('+response.data[i].id+')" class="custom-control-input"><span class="custom-control-label">' +
                response.data[i].task_name +
                "</span></label></td>" +
                '<td class="text-center text-nowrap">' +
                today +
                "</td>" +
                '<td class="text-center"><span class="tag tag-warning ml-0 mr-0" style="background-color:' +
                    colour +
                '">' +
                response.data[i].priority +
                "</span></td>";

            table.appendChild(tr);
        }
    });
   
    function getstatus(id) {
        var task_id = id;
        var status = $("#" + id).val();
        console.log(status);
        //alert(status);
        if (status == 0) {
            var status = 1;
            $.ajax({
            type: "POST",
            url: base_url + "editTask",
            data: "task_id=" + task_id + "&status=" + status,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            }).done(function (response) {
            var jsonDa = response;
            var jsonData = response["data"]["message"];
           
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Home";
                }, 1000);
            
        });
        }
        else {
            $("#inactive_btn").attr("data-id", id);
        $("#StatusTask").modal("show");
         
        }
    }
     /*-----------------------------Inactive Button------------------------------------*/
     $("#inactive_btn").on("click", function () {
        var task_id = $(this).data("id");
        var status = 0;
        $.ajax({
            type: "POST",
            url: base_url + "editTask",
            data: "task_id=" + task_id + "&status=" + status,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            }).done(function (response) {
            console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>Home";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#inactive_btn [type=submit]").attr("disabled", false);
                }
        });
    });

    $.ajax({
        url: base_url + "viewProjectSelf",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html = '<div class="col-md-12"><div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".Kt-project").append(html);
        } else {
            for (i in response.data) {
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }


                var startdate = new Date(response.data[i].start_date);
                var dd = String(startdate.getDate()).padStart(2, "0");
                var mm = String(startdate.getMonth() + 1).padStart(2, "0");
                var yyyy = startdate.getFullYear();
                startdate = dd + "-" + mm + "-" + yyyy;

                var endate = new Date(response.data[i].end_date);
                var dd = String(endate.getDate()).padStart(2, "0");
                var mm = String(endate.getMonth() + 1).padStart(2, "0");
                var yyyy = endate.getFullYear();
                endate = dd + "-" + mm + "-" + yyyy;


                var html =
                    '<li class="dd-item"><div class="dd-handle"><h6>' + response.data[i].project_name + '</h6><span class="time"><span class="text-primary">Start:  ' + startdate + '</span> to <span class="text-danger">Deadline:  ' + endate + '</span></span><p>' + response.data[i].project_description + '</p></div></li>';
                $(".Kt-project").append(html);
            }
        }
    });



</script>
