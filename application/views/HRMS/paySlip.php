<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-body">
                <div id="payroll_print">
                    <table width="100%" border="0">
                        <tr>
                            <td width="50%">
                                <h5 style="margin-top: 0px; margin-bottom: 0px" id="viewbranchname"></h5>
                                <span id="viewcompanyaddress"></span>
                            </td>
                            <td align="right">
                                <img src="<?php echo base_url(); ?>assets/images/ezdat-logo.png" style="max-width: 180px;" />
                            </td>
                        </tr>
                    </table>

                    <h5 style="">Employee Pay Summary</h5>
                    <table style="margin-bottom: 10px; width: 60%; float: left;">
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                Emp ID
                            </td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: left;">:</td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; text-align: left;" id="viewusercode"> </td>
                        </tr>
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                Emp Name
                            </td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: left;">:</td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; text-align: left;" id="viewfirstName"> </td>
                        </tr>
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                Designation
                            </td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: left;">:</td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; text-align: left;"id="viewdesignation"></td>
                        </tr>
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                Department
                            </td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: left;">:</td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; text-align: left;"id="viewdepartment"></td>
                        </tr>
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                Joining Date
                            </td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: left;">:</td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; text-align: left;" id="viewjoiningdate"></td>
                        </tr>
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                Pay Period
                            </td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: left;">:</td>
                            <td style="font-weight: 600; font-size: 15px; color: #000; text-align: left;" ><span id="viewmonth"></span> - <span id="viewyear"></span> </td>
                        </tr>
                    </table>
                    <table style="margin-bottom: 10px; width: 20%; text-align: center; float: right; background-color: #eee;">
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                Employee Net Pay
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                ₹ <span id="netsalary1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: 600; font-size: 15px; color: #000;">
                                LOP Days : <span>3</span>
                            </td>
                        </tr>
                        
                    </table>

                    <table class="table" width="100%">
                        <thead style="background: #eee;">
                            <tr>
                                <th style="font-weight: 600; text-align: left; width: 16%">Earning</th>
                                <th style="font-weight: 600; text-align: center; width: 16%">Amount</th>
                                <th style="font-weight: 600; text-align: right; width: 16%">YTD</th>
                                <th style="font-weight: 600; text-align: left; width: 16%">Deduction</th>
                                <th style="font-weight: 600; text-align: center; width: 16%">Amount</th>
                                <th style="font-weight: 600; text-align: right; width: 16%">YTD</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <th style="font-weight: 600; text-align: left; width: 16%">Basic</th>
                                <th style="font-weight: 100; text-align: center; width: 17%" >₹ <span id="basicsalary"></span></th>
                                <th style="font-weight: 100; text-align: right; width: 17%"> ₹ <span id="basicsalaryyearly"></span></th>
                                <th style="font-weight: 600; text-align: left; width: 16%">EPF</th>
                                <th style="font-weight: 100; text-align: center; width: 15%">₹ <span id="viewpf"></span></th>
                                <th style="font-weight: 100; text-align: right; width: 15%">₹ <span id="viewpfyearly"></span></th>
                            </tr>
                            <tr>
                                <th style="font-weight: 600; text-align: left">HRA</th>
                                <th style="font-weight: 100; text-align: center;">₹ <span id="viewhra"></span></th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewhrayearly"></span></th>
                                <th style="font-weight: 600; text-align: left">ESI</th>
                                <th style="font-weight: 100; text-align: center;">₹ <span id="viewesi"></span></th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewesiyearly"></span></th>
                            </tr>
                            <tr>
                                <th style="font-weight: 600; text-align: left">Conveyance</th>
                                <th style="font-weight: 100; text-align: center;">₹ <span id="viewconveyance"></span></th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewconveyanceyearly"></span></th>
                                <th style="font-weight: 600; text-align: left">Medical</th>
                                <th style="font-weight: 100; text-align: center;">₹ <span id="viewmedical"></span></th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewmedicalyearly"></span></th>
                            </tr>
                            <tr>
                                <th colspan="2" style="font-weight: 600; text-align: left">Bonus</th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewbonus"></span></th>
                            </tr>
                            <tr>
                                <th colspan="2" style="font-weight: 600; text-align: left">LTA</th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewlta"></span></th>
                            </tr>
                            <tr>
                                <th colspan="2" style="font-weight: 600; text-align: left">Commission</th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewscommission"></span></th>
                            </tr>
                            <tr>
                                <th colspan="2" style="font-weight: 600; text-align: left">Adjustments</th>
                                <th style="font-weight: 100; text-align: right;">₹ <span id="viewadjustments"></span></th>
                            </tr>

                            <tr style="background: #eee;">
                                <th style="font-weight: 600; text-align: left">Gross Earning</th>
                                <th style="text-align: center; font-weight: 600;">₹ <span id="viewgrossearning"></span></th>
                                <th style="font-weight: 600; text-align: right">₹ <span id="viewgrossearningyearly"></span></th>
                                <th style="font-weight: 600; text-align: left">Total Deduction</th>
                                <th style="text-align: center; font-weight: 600;" >₹ <span id="viewtotaldeduction"></span></th>
                                <th style="font-weight: 600; text-align: right">₹ <span id="viewtotaldeductionyearly"></span></th>
                            </tr>
                        </tbody>
                    </table>

<br>
<h5 style="text-align: center; margin-bottom: 0px">Other Earning</h5>
<?php

$queryfile = $this->db->query("SELECT * FROM `users_payslip` WHERE user_id='".$userId."'");
$arrPolicyfile = $queryfile->row();
if($arrPolicyfile->allowance_type == '[]') { ?>
<center style="width: 100%;"><div style="background-color: #eee; padding : 5px;">No Other Allowance</div></center>
<?php } else {

?>
<table class="table table-bordered" width="100%">
<thead style="background: #eee;">
<tr >
<th style="font-weight: 600; text-align: center;" width="30%">type</th>
<th style="font-weight: 600; text-align: center;" width="30%">Amount</th>
<th style="font-weight: 600; text-align: center;" width="30%">YTD</th>

</tr>
</thead>

<tbody>
<div>
<?php
$total_allowance = 0;
$allowances = json_decode($arrPolicyfile->allowance_type);
$i = 1;
//print_r($allowances);
foreach ($allowances as $allowance)
{
$total_allowance += $allowance->percent; ?>
<tr>
<td  style="font-weight: 600; text-align: center;"><?php echo $allowance->type; ?></td>
<td style="text-align: center;">₹ <span><?php echo $allowance->percent; ?></span></td>
<td style="text-align: center;">₹ <span><?php echo $allowance->allowance_amount; ?></span></td>
</tr>
<?php } ?>
</div>
</tbody>
</table>
<?php } ?>


<br>

<h5 style="text-align: center;  margin-bottom: 0px">Other Deduction</h5>
<?php

if($arrPolicyfile->deduction_type == '[]') { ?>
<center style="width: 100%;"><div style="background-color: #eee; padding : 5px;">No Other Deduction</div></center>
<?php } else {

?>
<table class="table table-bordered" width="100%">
<thead style="background: #eee;">
<tr >
<th  style="font-weight: 600; text-align: center;"  width="30%">Type</th>
<th  style="font-weight: 600; text-align: center;"  width="30%">Amount</th>
<th  style="font-weight: 600; text-align: center;"  width="30%">YTD</th>

</tr>
</thead>

<tbody>
<div>
<?php
$total_deduction = 0;
$deductions = json_decode($arrPolicyfile->deduction_type);
$i = 1;
//print_r($allowances);
foreach ($deductions as $deduction)
{
$total_deduction += $deduction->deduction_percent; ?>
<tr>
<td style="font-weight: 600; text-align: center;"><?php echo $deduction->type; ?></td>
<td style="text-align: center;"> ₹ <span><?php echo $deduction->deduction_percent; ?></span></td>
<td style="text-align: center;"> ₹ <span><?php echo $deduction->deduction_amount; ?></span></td>
</tr>
<?php } ?>
</div>
</tbody>
</table>
<?php } ?>
                    <br />
                    <h3 style="text-align: center; margin-bottom: 0px;">Payslip Summary</h3>
                    <center><hr style="margin: 5px 0px 5px 0px; width: 50%;" /></center>
                    <center>
                        <table>
                            <tr>
                                <td style="font-weight: 600; font-size: 15px; color: #000;">
                                    Gross Earning
                                </td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: center;">:</td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; text-align: right;">₹ <span id="viewgrossearning1"></span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: 600; font-size: 15px; color: #000;">
                                    Reimbursements 
                                </td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: center;">:</td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; text-align: right;">₹ <span id="viewtelephonereimbursements"></span></td>
                            </tr>
                            <tr>
                                <td style="font-weight: 600; font-size: 15px; color: #000;">
                                    Total Deduction
                                </td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: center;">:</td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; text-align: right;">₹ <span id="viewtotaldeduction1"></span></td>
                            </tr>
                            <tr>
                                <td colspan="3"><hr style="margin: 5px 0px;" /></td>
                            </tr>
                            <tr>
                                <td style="font-weight: 600; font-size: 15px; color: #000;">
                                    Net Salary
                                </td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%; text-align: center;">:</td>
                                <td style="font-weight: 600; font-size: 15px; color: #000; text-align: right;">₹ <span id="netsalary"></span>
                                    
                                </td>
                            </tr>
                        </table>
                    </center>
                    <br />
                    <center><h6>This is computer generated receipt, no authorised signatory required.</h6></center>
                </div>

                <div class="text-center">
                    <a onClick="PrintElem('#payroll_print')" class="btn btn-primary" style="font-weight: 600; font-size: 15px; color: #fff; text-align: right;">
                        Print Payslip Details
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
        function PrintElem(elem) {
           //Popup($(elem).html());
           Popup($("<div/>").append($(elem).clone()).html());
        }

        function Popup(data) {
            var mywindow = window.open("", "payroll", "height=400,width=600");
            mywindow.document.write("<html><head><title>Payslip Details</title>");
            //mywindow.document.write('<link rel="stylesheet" href="assets/css/neon-theme.css">');
            //mywindow.document.write('<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css">');
            mywindow.document.write("</head><body >");
            mywindow.document.write(data);
            mywindow.document.write("</body></html>");
            mywindow.document.close();
            mywindow.onload = function () {
               // necessary if the div contain images

            mywindow.focus(); // necessary for IE >= 10
            mywindow.print();
            mywindow.close();
        };
           return true;
        }

        // Edit' Form Fill
        var user_id = <?php echo $userId; ?>;

        var month = <?php echo $montha; ?>;

        var year = <?php echo $yeara; ?>;
        var dataString = 'user_id='+ user_id + '&month='+ month + '&year='+ year;
        $.ajax({
           url: base_url+"viewPayrollother",
           data: dataString,
           type: "POST",
           dataType    : 'json',
           beforeSend: function(xhr){
               xhr.setRequestHeader('Token', localStorage.token);
           },

            success:function(response){
                var status = "";
                if (response.data[0].status == 1) {
                   status = "Paid";
                } else {
                   status = "Unpaid";
                }
                $("#viewstatus").html(status);

                   $("#viewcompanyaddress").html(response.data[0].branch_location);
                   $("#viewbranchname").html(response.data[0].branch_name);


                   $("#viewusercode").html(response.data[0].emp_id);
                   $("#viewfirstName").html(response.data[0].first_name);
                   $("#viewdesignation").html(response.data[0].designation_name);
                   $("#viewdepartment").html(response.data[0].department_name);
                   var startDate = new Date(response.data[0].joining_date);
                    var dd = String(startDate.getDate()).padStart(2, '0');
                    var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = startDate.getFullYear();

                    startDate = dd + '-' + mm + '-' + yyyy;
                   $("#viewjoiningdate").html(startDate);
                   var month = '';
                   if(response.data[0].month == 1)
                   {
                    month = 'January'
                   }else if(response.data[0].month == 2)
                   {
                    month = 'February'
                   }else if(response.data[0].month == 3)
                   {
                    month = 'March'
                   }else if(response.data[0].month == 4)
                   {
                    month = 'April'
                   }
                   else if(response.data[0].month == 5)
                   {
                    month = 'May'
                   }else if(response.data[0].month == 6)
                   {
                    month = 'June'
                   }else if(response.data[0].month == 7)
                   {
                    month = 'July'
                   }else if(response.data[0].month == 8)
                   {
                    month = 'August'
                   }else if(response.data[0].month == 9)
                   {
                    month = 'September'
                   }else if(response.data[0].month == 10)
                   {
                    month = 'October'
                   }else if(response.data[0].month == 11)
                   {
                    month = 'November'
                   }else if(response.data[0].month == 12)
                   {
                    month = 'December'
                   }

                   $("#viewmonth").html(month);
                   $("#viewyear").html(response.data[0].year);


                   $("#basicsalary").html(response.data[0].basic_monthly);
                   $("#basicsalaryyearly").html(response.data[0].basic_yearly);

                   $("#viewhra").html(response.data[0].hra_monthly);
                   $("#viewhrayearly").html(response.data[0].hra_yearly);

                   $("#viewconveyance").html(response.data[0].conveyance_monthly);
                   $("#viewconveyanceyearly").html(response.data[0].yearly_conveyance);


                   $("#viewbonus").html(response.data[0].bonus_yearly);

                   $("#viewlta").html(response.data[0].lta);
                   $("#viewscommission").html(response.data[0].commission);
                   $("#viewadjustments").html(response.data[0].adujestment);



                   $("#viewpf").html(response.data[0].pf_monthly);
                   $("#viewpfyearly").html(response.data[0].pf_yearly);

                   $("#viewesi").html(response.data[0].esi_monthly);
                   $("#viewesiyearly").html(response.data[0].esi_yearly);

                   $("#viewmedical").html(response.data[0].medical_monthly);
                   $("#viewmedicalyearly").html(response.data[0].medical_yearly);

                   var grossearning =  parseFloat(response.data[0].basic_monthly) + parseFloat(response.data[0].hra_monthly) + parseFloat(response.data[0].conveyance_monthly);
                   $("#viewgrossearning").html(grossearning);
                   $("#viewgrossearning1").html(grossearning);

                   var grossearningyearly =  parseFloat(response.data[0].basic_yearly) + parseFloat(response.data[0].hra_yearly) + parseFloat(response.data[0].yearly_conveyance) + parseFloat(response.data[0].bonus_yearly) + parseFloat(response.data[0].lta) + parseFloat(response.data[0].commission) + parseFloat(response.data[0].adujestment);
                   $("#viewgrossearningyearly").html(grossearningyearly);

                   var totaldeduction =  parseFloat(response.data[0].pf_monthly) + parseFloat(response.data[0].esi_monthly) + parseFloat(response.data[0].medical_monthly);
                   $("#viewtotaldeduction").html(totaldeduction);
                   $("#viewtotaldeduction1").html(totaldeduction);

                   var totaldeductionyearly =  parseFloat(response.data[0].pf_yearly) + parseFloat(response.data[0].esi_yearly) + parseFloat(response.data[0].medical_yearly);
                   $("#viewtotaldeductionyearly").html(totaldeductionyearly);


                   $("#viewtelephonereimbursements").html(response.data[0].reimbursement);

                   var netsalaryemp =  parseFloat(grossearning) - parseFloat(totaldeduction);
                   $("#netsalary").html(netsalaryemp);
                   $("#netsalary1").html(netsalaryemp);

                   localStorage.removeItem('$userid');
               }
           });

</script>
