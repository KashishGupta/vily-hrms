<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
				<li class="breadcrumb-item active">Company Setting</li>
			</ul>
        </div>
        
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Company Setting</strong></h3>
            </div>
            <div class="card-body">
            	<form id="editcompanydetails" class="form-horizontal form-groups-bordered" method="post" accept-charset="utf-8">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Company Name <span class="text-danger">*</span></label><br>
                                <span id="companynamevalid" class="text-danger change-pos"></span>
                                <input class="form-control" type="hidden" value="" id="ediid" name="ediid" placeholder="Please enter company name.">
                                <input class="form-control" type="text" value="" id="editcompanyname" name="editcompanyname" placeholder="Please enter company name.">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Contact Person Name <span class="text-danger">*</span></label>
                                <br>
                                <span id="contactpersonvalid" class="text-danger change-pos"></span>
                                <input class="form-control" value="" type="text" id="editcontactperson" name="editcontactperson" placeholder="Please enter contact person.">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Owner Name <span class="text-danger">*</span></label>
                                <br>
                                <span id="contactpersonvalid" class="text-danger change-pos"></span>
                                <input class="form-control" value="" type="text" id="editownername" name="editownername" placeholder="Please enter contact person.">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Mobile Number <span class="text-danger">*</span></label>
                                <br>
                                <span id="mobilenumbervalid" class="text-danger change-pos"></span>
                                <input class="form-control" type="text"  name="editphoneno"  id="editphoneno"  placeholder="Please enter contact number.">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Email <span class="text-danger">*</span></label>
                                <br>
                                <span id="emailvalid" class="text-danger change-pos"></span>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="editcompanyemail" id="editcompanyemail" placeholder="Please enter email.">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Address <span class="text-danger">*</span></label>
                                <br>
                                <span id="addressvalid" class="text-danger change-pos"></span>
                                <textarea class="form-control" placeholder="Enter address." id="editcompanyaddress" name="editcompanyaddress" aria-label="With textarea"></textarea>
                            </div>
                        </div>
                       
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Country <span class="text-danger">*</span></label>
                                <br>
                                <span id="countryvalid" class="text-danger change-pos"></span>
                                <input type="text" class="form-control"  name="editcountryname" id="editcountryname" placeholder="Please enter country Name.">
                            </div>
                        </div>                                
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>State/Province <span class="text-danger">*</span></label>
                                <br>
                                <span id="statevalid" class="text-danger change-pos"></span>
                                <input type="text" class="form-control"  name="editstate" id="editstate" placeholder="Please enter state Name.">
                               
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>City</label>
                                <input class="form-control" name="editcity" id="editcity" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Postal Code</label>
                                <input class="form-control"  name="editpostalcode" id="editpostalcode" type="text">
                            </div>
                        </div>
                       
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Fax</label>
                                <input class="form-control"  name="editfax_no" id="editfax_no" type="text">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>
                                link</label>
                                <input class="form-control"  name="editapilink" id="editapilink" type="text">
                            </div>
                        </div>
                        
                       
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>status <span class="text-danger">*</span></label>
                                <br>
                                <span id="statevalid" class="text-danger change-pos"></span>
                                <select class="select form-control"  id="editstatus" name="editstatus">
                                <option value="1" selected>Inactive</option>
                                                <option value="0">Active</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 text-right m-t-20">
                        	<button type="submit" class="btn btn-info"> Submit </button>
                            <button type="reset" id="test" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>            
</div>  

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#companyname").val("");
            $("input#contactperson").prop("");
            $("input#mobilenumber").val("");
            $("input#address").prop("");
            $("input#email").val("");
            $("input#website").prop("");
            //$("#addbranchid option:eq(0)").prop("selected", true);
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#companynamevalid").text("");
            jQuery("#contactpersonvalid").text("");
            jQuery("#mobilenumbervalid").text("");
            jQuery("#addressvalid").text("");
            jQuery("#emailvalid").text("");
            jQuery("#websitevalid").text("");
            jQuery("#postalcodevalid").text("");
            jQuery("#companylogovalid").text("");
        });
    });

    $.ajax({
            url: base_url+"viewCompany",
            data: {},
            type: "POST",
            dataType    : 'json',
            beforeSend: function(xhr){
                xhr.setRequestHeader('Token', localStorage.token);
            },

            success:function(response){
                
                $('#ediid').val(response["data"][0]["id"]);
                $('#editcompanyname').val(response["data"][0]["company_name"]);
                $('#editcontactperson').val(response["data"][0]["contactperson"]);
                $('#editcompanyaddress').val(response["data"][0]["company_address"]);
                $('#editownername').val(response["data"][0]["owner_name"]);
                $('#editphoneno').val(response["data"][0]["phone_no"]);
                $('#editcompanyemail').val(response["data"][0]["company_email"]);
                $('#editcountryname').val(response["data"][0]["country_name"]);
                $('#editstate').val(response["data"][0]["state"]);
                $('#editcity').val(response["data"][0]["city"]);
                $('#editpostalcode').val(response["data"][0]["postal_code"]);
                $('#editfax_no').val(response["data"][0]["fax_no"]);
                $('#editapilink').val(response["data"][0]["api_link"]);
                $('#editcompanylogo').val(response["data"][0]["company_logo"]);
                $('#editstatus').val(response["data"][0]["status"]);
            }
        });
 $("#editcompanydetails").submit(function (e) {
    var formData = {
        company_id: $("input[name=ediid]").val(),
        company_name: $("input[name=editcompanyname]").val(),
        contactperson  : $("input[name=editcontactperson]").val(),
        company_address: $("input[name=location]").val(),
        company_email: $("input[name=editcompanyemail]").val(),
        owner_name: $("input[name=editownername]").val(),
        phone_no: $("input[name=editphoneno]").val(),
        country_name: $("input[name=editcountryname]").val(),
        state: $("input[name=editstate]").val(),
        city  : $("input[name=editcity]").val(),
        postal_code: $("input[name=editpostalcode]").val(),
        fax_no: $("input[name=editfax_no]").val(),
        api_link: $("input[name=editapilink]").val(),
        company_logo: $("input[name=editcompanylogo]").val(),
        status: $("select[name=editstatus]").val(),
    };

    e.preventDefault();
    $.ajax({
        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
        url: base_url + "editCompany", // the url where we want to POST
        data: formData, // our data object
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        console.log(response);

        var jsonData = response;
        var falseData = response["data"];
        var successData = response["data"]["message"];
        if (jsonData["data"]["status"] == "1") {
            toastr.success(successData);
            setTimeout(function () {
                window.location = "<?php echo base_url();?>CompanySetting";
            }, 1000);
        } else {
            toastr.error(falseData);
            $("#editcompanydetails [type=submit]").attr("disabled", false);
        }
    });
});
    //Company Setting Validation
    $(document).ready(function () {
        $("#companysetting").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            /*if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }*/

            if ($("#companyname").val().trim() == "") {
                $("#companynamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#companynamevalid").text("");
            }

            if ($("#contactperson").val().trim() == "") {
                $("#contactpersonvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#contactpersonvalid").text("");
            }

            if ($("#mobilenumber").val().trim() == "") {
                $("#mobilenumbervalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#mobilenumbervalid").text("");
            }

            if ($("#address").val().trim() == "") {
                $("#addressvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addressvalid").text("");
            }

            if ($("#email").val().trim() == "") {
                $("#emailvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#emailvalid").text("");
            }

            if ($("#website").val().trim() == "") {
                $("#websitevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#websitevalid").text("");
            }

            

            if ($("#companylogo").val().trim() == "") {
                $("#companylogovalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#companylogovalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>