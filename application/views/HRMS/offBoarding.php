<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item active">Off Boarding</li>
            </ul>
        </div>
    

        <!-- Search Payroll Salary -->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Users Offboarding</strong></h3>
            </div>
            <div class="card-body">
               <form id="filter-form" method="POST" action="#">
                
                </form>
            </div>
        </div>
        <!-- /Search Payroll Salary -->

        <div class="row clearfix kt-widget__items"></div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    $(function () {
        filterOffboarding(01);
        $("#filter-form").submit();
    });
    function filterOffboarding() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            user_id = $("#finduserId").val();
           
            //alert(user_id);
            if (user_id == null) {
                user_id = 01;
            }
           
          //  alert(user_id);
            req = {};
            req.user_id = user_id;
    $.ajax({
        url: base_url + "viewOffboardingUser",
        data: req,
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-md-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Offboarding</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
                var html =
                    '<div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-4"><div class="profile-widget"><div class="profile-img"><img class="rounded-circle img-thumbnail w100 h100" src="' +
                    UserImage +
                    '" alt=""></div><div class="dropdown profile-action"><a href="#" class="action-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-156px, 18px, 0px);"><a class="dropdown-item" href="<?php echo base_url(); ?>offBoardingAdd/' +
                    response.data[i].id +
                    '"><i class="fa fa-edit"></i>Add Letter</a><a class="dropdown-item" href="<?php echo base_url(); ?>offBoardingView/' +
                    response.data[i].id +
                    '"><i class="fa fa-edit"></i>View Letter</a></div></div><h4 class="user-name mt10 text-ellipsis"><a href="<?php echo base_url(); ?>offBoardingView/' +
                    response.data[i].id +
                    '">' +
                    response.data[i].first_name +
                    '</a></h4><div class="small text-muted"><span>' +
                    response.data[i].emp_id +
                    "</span>- Web Designer</div><span>" +
                    response.data[i].email +
                    "</span><br><span>" +
                    response.data[i].phone +
                    "</span></br></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });
});
    }
      //Select Purchase By
      $.ajax({
        url: base_url + "viewUserId",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
       // var jsonData = response["data"]["last_name"];
        let dropdown = $("#finduserId");

        dropdown.empty();

        dropdown.append('<option selected="true" value="01">Choose Employee Name</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.user_id).text(entry.first_name));
        });
    });
</script>
