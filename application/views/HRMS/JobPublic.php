<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item active">Jobs Public</li>
            </ul>
        </div>
    </div>
</div>
<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Jobs Public Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl " id="publicjobtable">
                    <thead>
                        <tr><th class="text-center">Job ID</th>
                            <th class="text-center">Job Title</th>
                            <th class="text-center">Branch Name</th>
                            <th class="text-center">Department Name</th>
                            <th class="text-center">Start Date</th>
                            <th class="text-center">Expire Date</th>
                            <th class="text-center">Job Type</th>
                            <th class="text-center">Status</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodypublicjob"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Show Public Jobs lis
    $.ajax({
        url: base_url + "viewJobPublic",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        var table = document.getElementById("tablebodypublicjob");
        for (i in response.data) {
            var startDate = new Date(response.data[i].start_date);
            var dd = String(startDate.getDate()).padStart(2, "0");
            var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = startDate.getFullYear();

            startDate = dd + "-" + mm + "-" + yyyy;
            var endDate = new Date(response.data[i].end_date);
            var dd = String(endDate.getDate()).padStart(2, "0");
            var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = endDate.getFullYear();

            endDate = dd + "-" + mm + "-" + yyyy;
            var tr = document.createElement("tr");
            tr.innerHTML =
            	'<td class="text-center text-uppercase">' + response.data[i].job_number + '</td>' +
                '<td class="text-center"><a href="JobDetails_emp/' +
                response.data[i].job_id +
                '" >' +
                response.data[i].job_title  +
                "</a></td>" +
                '<td class="text-center">' +
                response.data[i].branch_name +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].department_name +
                "</td>" +
                '<td class="text-center">' +
                startDate +
                "</td>" +
                '<td class="text-center text-danger">' +
                endDate +
                "</td>" +
                '<td class="text-center" style="color:' +
                response.data[i].type_color +
                '">' +
                response.data[i].job_type +
                "</td>" +
                '<td class="text-center" style="color:' +
                response.data[i].status_color +
                '">' +
                response.data[i].job_applicant_status +
                "</td>";
            table.appendChild(tr);
        }
        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#publicjobtable").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Jobs Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Jobs Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                    }
                }
            ]
        });
    });
</script>
