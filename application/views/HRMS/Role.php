<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Role</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Designation</strong></h3>
            </div>
            <div class="card-body">
                <form id="filter-form" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-md-4 col-sm-12">
                            <label>Branch<span class="text-danger"> *</span></label><br />
                            <span id="filterbranchidvalidate" class="text-danger change-pos-open"></span>
                            <select name="filterbranchid" id="filterbranchid" class="form-control custom-select"> </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label>Department<span class="text-danger"> *</span></label><br />
                            <span id="filterdepartmentidvalidate" class="text-danger change-pos-open"></span>
                            <select name="filterdepartmentid" id="filterdepartmentid" class="form-control custom-select"> </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12 button-open">
                            <button id="filterdesignationbutton" type="submit" class="btn btn-success submit-btn">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive" id="designationExample">
                    <thead>
                        <tr>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Department</th>
                            <th class="text-center">Designation</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="designationtable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    $(function () {
        filterDepartment("all");
        $("#filter-form").submit();
    });

    function filterDepartment() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            department_id = $("#filterdepartmentid").val();
            //alert(status);
            if (department_id == null) {
                department_id = "all";
            }
            req = {};
            req.department_id = department_id;

            //Show Designation list
            $.ajax({
                url: base_url + "viewDesignation",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                //var j =1;
                $("#designationExample").DataTable().clear().destroy();
                var table = document.getElementById("designationtable");
                for (i in response.data) {
                    var tr = document.createElement("tr");
                    tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +

                        '<td class="text-center">' +
                        response.data[i].branch_name +
                        "</td>" +

                        '<td class="text-center">' +
                        response.data[i].department_name +
                        "</td>" +
                        
                        '<td class="text-center">' +
                        response.data[i].designation_name +
                        "</td>" +
                        
                        '<td class="text-right"><a class="btn btn-success submit-btn" href="<?php echo base_url()?>UpdatePermission/' +
                        response.data[i].designation_id +
                        '">Update Permission</a></td>';
                    table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#designationExample").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Role Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 4 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Department Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 4 ]
                            }
                        }
                    ]
                });
            });
        });
    }
    //Select branch For Filter
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#filterbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select Department by select branch For Add Designation Filter
    $("#filterbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=filterbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#filterdepartmentid");

            dropdown.empty();

            dropdown.append('<option selected="true"  required="true">Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    //Select Branch For Add Designation
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select Department by select branch For Add Designation
    $("#addbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#adddepartmenttest");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled required="true">Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Add Designation
    $("#adddesignation").submit(function (e) {
        var formData = {
            department_id: $("select[name=adddepartmenttest]").val(),
            designation_name: $("input[name=adddesignationname]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addDesignation",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Role";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#adddesignation [type=submit]").attr("disabled", false);
            }
        });
    });

    //Select Branch for edit designation
    function fetchBranch(branch_id) {
        $.ajax({
            url: base_url + "viewactiveBranch",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editbranchid");

            dropdown.empty();

            dropdown.append("<option disabled>Choose Branch</option>");
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                if (branch_id == entry.id) {
                    dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.branch_name));
                } else {
                    dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
                }
            });
        });
    }

    // Select Department by select branch For Edit Designation
    function fetchDepartment(department_id) {
        $.ajax({
            url: base_url + "viewDepartmentSelect",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editdepartmentid");

            dropdown.empty();

            dropdown.append("<option disabled>Choose Department</option>");
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                if (department_id == entry.department_id) {
                    dropdown.append($("<option selected></option>").attr("value", entry.department_id).text(entry.department_name));
                } else {
                    dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
                }
            });
        });
    }

    // Edit  Department Form Fill
    $(document).on("click", ".edit_data", function () {
        var designation_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewDesignation",
            method: "POST",
            data: { designation_id: designation_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                var branch_id = response["data"][0]["branch_id"];
                var department_id = response["data"][0]["department_id"];
                fetchBranch(branch_id);
                fetchDepartment(department_id);
                $("#editdesignationid").val(response["data"][0]["designation_id"]);
                $("#editdesignationname").val(response["data"][0]["designation_name"]);
                $("#edit_designation").modal("show");
            },
        });
    });

    $("#editbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=editbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editdepartmentid");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Edit Designation form
    $("#editdesignation").submit(function (e) {
        var formData = {
            department_id: $("select[name=editdepartmentid]").val(),
            designation_id: $("input[name=editdesignationid]").val(),
            designation_name: $("input[name=editdesignationname]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editDesignation",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Role";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editdesignation [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Designation
    $(document).on("click", ".delete_data", function () {
        var designation_id = $(this).attr("id");
        $(".delete_designation_button").attr("id", designation_id);
        $("#delete_designation").modal("show");
    });

    $(document).on("click", ".delete_designation_button", function () {
        var designation_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteDesignation",
            method: "POST",
            data: { designation_id: designation_id },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>Role";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_designation_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    //Add Designation Validation
    $(document).ready(function () {
        $("#adddesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }
            if ($("#adddepartmenttest").val() == null) {
                $("#adddepartmenttest").focus();
                $("#adddepartmentvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#adddepartmentvalidate").text("");
            }
            if ($("#adddesignationname").val().trim() == "") {
                $("#adddesignationname").focus();
                $("#adddesignationvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#adddesignationvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Edit Designation Validation
    $(document).ready(function () {
        $("#editdesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editbranchid").val() == null) {
                $("#editbranchid").focus();
                $("#editbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#editbranchvalidate").text("");
            }
            if ($("#editdepartmentid").val() == null) {
                $("#editdepartmentid").focus();
                $("#editdepartmentvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#editdepartmentvalidate").text("");
            }
            if ($("#editdesignationname").val().trim() == "") {
                $("#editdesignationname").focus();
                $("#editdesignationvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editdesignationvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Filter Designation Validation
    $(document).ready(function () {
        $("#filterdesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#filterbranchid").val() == null) {
                $("#filterbranchid").focus();
                $("#filterbranchidvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#filterbranchidvalidate").text("");
            }
            if ($("#filterdepartmentid").val() == null) {
                $("#filterdepartmentid").focus();
                $("#filterdepartmentidvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#filterdepartmentidvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
