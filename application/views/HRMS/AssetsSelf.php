<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Manager/Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Issues Assets</li>
            </ul>
           <div class="header-action mt-3">
            </div>
        </div>
        
        <!-- Search Assets Result-->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Issues Assets Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list dataTable table-responsive table-responsive-md no-footer" id="assetstable">
                    <thead>
                        <tr>
                            <th class="text-center">Asset Id</th>
                            <th class="text-center">Asset Name</th>
                            <th class="text-center">Issue Date</th>
                            <th class="text-center">Serial Number</th>
                            <th class="text-center">Issued By</th>
                            <th class="text-center">Status</th>
                           
                        </tr>
                    </thead>
                    <tbody id="tablebodyassets">

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Search Assets Result-->
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
	$.ajax({
        url: base_url + "viewAssetsSelf",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
    	var table = document.getElementById('tablebodyassets');
        for (i in response.data) {

        var status_color = "";
        if (response.data[i].status == 0) {
            status_color = "red";
        }
        if (response.data[i].status == 1) {
            status_color = "green";
        }
        var status = "";
        if (response.data[i].status == 0) {
            status = "Inactive";
        }
        if (response.data[i].status == 1) {
            status = "Active";
        }
        
        var date = new Date(response.data[i].issue_date);
		var dd = String(date.getDate()).padStart(2, '0');
		var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = date.getFullYear();

		date = dd + '-' + mm + '-' + yyyy;
		var tr = document.createElement('tr');
            tr.innerHTML =
                '<td class="text-center text-uppercase">' +
                response.data[i].assets_code +
                "</td>" +

                '<td class="text-center">' +
                response.data[i].assets_name +
                "</td>" +

                '<td class="text-center">' +
                date +
                "</td>" +

                '<td class="text-center text-uppercase">' +
                response.data[i].serial_no +
                "</td>" +

                '<td class="text-center">' +
                response.data[i].issued_by +
                "</td>" +

                 '<td class="text-center" style="color:' +
                    status_color +
                    '">' +
                status +
                "</td>";

                
            table.appendChild(tr);
        }
        $("#assetstable").DataTable({
            dom: "Bfrtip",
            buttons: ["excelHtml5", "pdfHtml5"],
        });
    });

   
</script>