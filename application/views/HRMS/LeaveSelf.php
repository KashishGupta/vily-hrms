<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home">Dashboard</a></li>
                <li class="breadcrumb-item active">Self Leave's</li>
            </ul>
            <div class="header-action">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_leave"><i class="fe fe-plus mr-2"></i>Apply Leave</button>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-database"></i></div>
                        <h3 class="mb-1" id="totalleaves"></h3>
                        <div>Total Leaves</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-plane-departure"></i></div>
                        <h3 class="mb-1" id="totalApproveleave"></h3>
                        <div>Taken Leave</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-chart-line"></i></div>
                        <h3 class="mb-1" id="totalBalanceleave"></h3>
                        <div>Balance Leave</div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="card-value float-right"><i class="fa fa-chart-bar"></i></div>
                        <h3 class="mb-1" id="totalPaidleave"></h3>
                        <div>Unpaid Leave</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Self Leave Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="leavetable">
                    <thead>
                        <tr>
                            <th class="text-center">Policy Name</th>
                            <th class="text-center">From</th>
                            <th class="text-center">To</th>
                            <th class="text-center">Days</th>
                            <th class="text-center">Reason</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyleave">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Add Leave Modal -->
<div id="add_leave" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Apply Leave</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addleave" method="POST" action="#">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Leave Type <span class="text-danger">*</span></label>
                            <br>
                            <span id="addleavetypevalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addleaveid" id="addleaveid">
                            
                            </select>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="test" name="example-inline-radios">
                                <span class="custom-control-label">Allowed To Half Day Leave</span>
                            </label>
                            <label class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="testshort" name="example-inline-radios">
                                <span class="custom-control-label">Allowed To short Leave</span>
                            </label>
                        </div>
                           
                        <div class="form-group col-md-6 col-sm-12">
                            <label>From <span class="text-danger">*</span></label>
                            <br>
                            <span id="addleavestartdatevalidate" class="text-danger change-pos"></span>
                            <span id="addleavestartdatevalidatee" class="text-danger change-pos"></span>
                            <input data-provide="datepicker" data-date-autoclose="true" id="addleavestartdate" name="addleavestartdate" class="form-control " type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly>
                        </div>

                        <div class="form-group col-md-6 col-sm-12">
                            <label>To <span class="text-danger">*</span></label>
                            <br>
                            <span id="addleaveenddatevalidate" class="text-danger change-pos"></span>
                            <input data-provide="datepicker" data-date-autoclose="true" id="addleaveenddate" name="addleaveenddate" class="form-control " type="text" placeholder="YYYY/MM/DD" autocomplete="off" readonly>
                        </div>

                        <div class="form-group col-md-12">
                            <label>Leave Reason <span class="text-danger">*</span></label>
                            <br>
                            <span id="addleavereasonvalidate" class="text-danger change-pos"></span>
                            <textarea name="addleavereason" id="addleavereason" class="form-control" type="text" autocomplete="off" placeholder="Enter reason."></textarea>
                        </div>

                        <div class="form-group col-md-12 pull-right">
                            <button id="addleavebutton" class="btn btn-success submit-btn">Submit</button>
                            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Leave Modal -->

<!-- Edit Leave Modal -->
<div id="edit_leave" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Leave</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editleave" method="POST" action="#">
                    <div class="form-group">
                        <label>Leave Type <span class="text-danger">*</span></label>
                        <br>
                        <span id="editleavetypevalidate" class="text-danger"></span>
                        <select class="custom-select form-control" name="editleaveid" id="editleaveid">
                        </select>
                        <input type="hidden" value="" id="editleavetypeid" name="editleavetypeid">
                    </div>
                     <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="edittest1" name="example-inline-radios">
                            <span class="custom-control-label">Allowed To Half Day Leave</span>
                        </label>
                        <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="edittestshort1" name="example-inline-radios">
                            <span class="custom-control-label">Allowed To short Leave</span>
                        </label>
                    </div>
                
                    <div class="form-group">
                        <label>From <span class="text-danger">*</span></label>
                        <br>
                        <span id="editleavestartdatevalidate" class="text-danger"></span>
                        <input id="editleavestartdate" data-provide="datepicker" data-date-autoclose="true" name="editleavestartdate" class="form-control " type="text" placeholder="yyyy/mm/dd" readonly>
                    </div>
                    <div class="form-group">
                        <label>To <span class="text-danger">*</span></label>
                        <br>
                        <span id="editleaveenddatevalidate" class="text-danger"></span>
                        <input id="editleaveenddate" data-provide="datepicker" data-date-autoclose="true" name="editleaveenddate" class="form-control " type="text" placeholder="yyyy/mm/dd" readonly>
                    </div>
                    <div class="form-group">
                        <label>Leave Reason <span class="text-danger">*</span></label>
                        <br>
                        <span id="editleavereasonvalidate" class="text-danger"></span>
                        <textarea name="editleavereason" id="editleavereason" class="form-control" type="text"></textarea>

                    </div>
                    <div class="submit-section pull-right">
                        <button id="editleavebutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Leave Modal -->


<!-- Delete Leave Modal -->
<div class="modal custom-modal fade" id="delete_leave" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_leave_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Leave Modal -->

<div id="view_leave" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Leave Reason</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="viewleave" method="POST" action="#">
                    <div class="row">
                        <p></p>

                        <div class="form-group col-md-12 pull-right">
                            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

//for remove validation and empty input field
$(document).ready(function () {
    $(".modal").click(function () {
        $("input#addleavestartdate").val("");
        $("input#addleaveenddate").val("");
        $("input#test").val("");
        $("input#testshort").val("");
        $("textarea#addleavereason").val("");
        $("#addleaveid option:eq(0)").prop("selected", true);
    });
    $(".modal .modal-dialog .modal-body > form").click(function (e) {
        e.stopPropagation();
    });
    $("form button[data-dismiss]").click(function () {
        $(".modal").click();
    });
});

$(document).ready(function () {
    $(".modal").click(function () {
        jQuery("#addleavetypevalidate").text("");
        jQuery("#addleavestartdatevalidate").text("");
        jQuery("#addleaveenddatevalidate").text("");
        jQuery("#addleavereasonvalidate").text("");
        jQuery("#addleavestartdatevalidatee").text("");
    });
});

//Show Total Leave Count
$.ajax({
    url: base_url + "leaveTotal",
    method: "POST",
    dataType: "json",
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
    success: function (response) {
        $("#totalleaves").html(response.data[0].leaves);
    },
});

//Show Total Approve Leave Count
$.ajax({
    url: base_url + "viewApprovedLeave",
    method: "POST",
    dataType: "json",
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
    success: function (response) {
        $("#totalApproveleave").html(response.data);
    },
});

//Show Total Balance Leave Count
$.ajax({
    url: base_url + "viewBalanceLeave",
    method: "POST",
    dataType: "json",
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
    success: function (response) {
        $("#totalBalanceleave").html(response.data);
    },
});

//Show Total Paid Leave Count
$.ajax({
    url: base_url + "viewPaidLeave",
    method: "POST",
    dataType: "json",
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
    success: function (response) {
        if (response.data.paid_total == null) {
            $("#totalPaidleave").html(0);
        } else {
            $("#totalPaidleave").html(response.data.paid_total);
        }
    },
});

//Show Leave list
$.ajax({
    url: base_url + "viewLeave",
    data: {},
    type: "POST",
    dataType: "json",
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    //var j = 1;
    var table = document.getElementById("tablebodyleave");
    for (i in response.data) {
        var status = "";
        if (response.data[i].status == 0) {
            status = "Pending";
        }
        if (response.data[i].status == 1) {
            status = "Approved";
        }
        if (response.data[i].status == 2) {
            status = "Declined";
        }
        var status_color = "";
        if (response.data[i].status == 0) {
            status_color = "blue";
        }
        if (response.data[i].status == 1) {
            status_color = "green";
        }
        if (response.data[i].status == 2) {
            status_color = "red";
        }
        var action = "";
        if (response.data[i].status == 0) {
            action =
                '<button type="button" data-toggle="modal" data-target="#edit_leave" aria-expanded="false" id="' +
                response.data[i].leave_id +
                '" class="btn btn-info edit_data" title="Edit" ><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_leave" aria-expanded="false" id="' +
                response.data[i].leave_id +
                '" class="btn btn-danger delete_data"><i class="fa fa-trash-alt"></i></button>';
        } else {
            action = '<button type="button" class="btn btn-danger"><i class="fa fa-times-circle"></i></button> <button type="button" data-toggle="modal" data-target="#view_leave" aria-expanded="false" id="' + response.data[i].leave_id + '" class="btn btn-primary"><i class="fa fa-eye"></i></button>';
        }
        var totalPercent = "";
           
           if(response.data[i].allow_short == 1)
           {
               totalPercent = response.data[i].days;
           }else if(response.data[i].allow_half == 1)
           {
               totalPercent = response.data[i].days;
           }
           else{
               var number = response.data[i].days;
               var percentToGet = 1;
               var percent =number + percentToGet;
               totalPercent = percent;
           }
        var startDate = new Date(response.data[i].start_date);
        var dd = String(startDate.getDate()).padStart(2, "0");
        var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
        var yyyy = startDate.getFullYear();

        startDate = dd + "-" + mm + "-" + yyyy;
        var endDate = new Date(response.data[i].end_date);
        var dd = String(endDate.getDate()).padStart(2, "0");
        var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
        var yyyy = endDate.getFullYear();

        endDate = dd + "-" + mm + "-" + yyyy;
        var tr = document.createElement("tr");
        tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
            '<td class="text-center">' +
            response.data[i].policy_name +
            "</td>" +
            '<td class="text-center">' +
            startDate +
            "</td>" +
            '<td class="text-center">' +
            endDate +
            "</td>" +
            '<td class="text-center">' +
            totalPercent +
            "</td>" +
            '<td class="text-center" style="white-space: break-spaces;">' +
            response.data[i].reason +
            "</td>" +
            '<td class="text-center" style="color:' +
            status_color +
            '">' +
            status +
            "</td>" +
            '<td class="text-center">' +
            action +
            "</td>";
        table.appendChild(tr);
    }

    $("#leavetable").DataTable({
        dom: "Bfrtip",
        buttons: ["excelHtml5", "pdfHtml5"],
    });
});

//Select Leave Type by api
$.ajax({
    url: base_url + "viewLgggedUserPloicies",
    data: {},
    type: "POST",
    dataType: "json", // what type of data do we expect back from the server
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    let dropdown = $("#addleaveid");
    dropdown.empty();
    dropdown.append('<option selected="true" disabled>Choose Leave Policy</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.id).text(entry.policy_name));
    });
});

// Add leave
$("#addleave").submit(function (e) {
    if ($("#test").prop("checked") == true) {
        var halfleave = 1;
    } else {
        var halfleave = 0;
    }
    if ($("#testshort").prop("checked") == true) {
        var shortleave = 1;
    } else {
        var shortleave = 0;
    }
    var formData = {
        user_id: localStorage.userid,
        leave_policy: $("select[name=addleaveid]").val(),
        start_date: $("input[name=addleavestartdate]").val(),
        end_date: $("input[name=addleaveenddate]").val(),
        reason: $("textarea[name=addleavereason]").val(),
        allow_half: halfleave,
        allow_short: shortleave,
    };
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: base_url + "addLeave",
        data: formData,
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        console.log(response);

        var jsonDa = response;
        var jsonData = response["data"]["message"];
        var falsedata = response["data"];
        if (jsonDa["data"]["status"] == "1") {
            toastr.success(jsonData);
            setTimeout(function () {
                window.location = "<?php echo base_url()?>LeaveSelf_emp";
            }, 1000);
        } else {
            toastr.error(falsedata);
            $("#addleave [type=submit]").attr("disabled", false);
        }
    });
});

// Fetch Leave Type For Leave Edit
function fetchLeaveType(leave_type_id) {
    $.ajax({
        url: base_url + "viewLgggedUserPloicies",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#editleaveid");
        dropdown.empty();
        dropdown.append("<option disabled>Choose Leave Type</option>");
        dropdown.prop("selectedIndex", 0);
        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (leave_type_id == entry.id) {
                dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.policy_name));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.policy_name));
            }
        });
    });
}

// Edit  Leave Form Fill
$(document).on("click", ".edit_data", function () {
    var leave_id = $(this).attr("id");
    $.ajax({
        url: base_url + "viewLeave_Where",
        method: "POST",
        data: {
            leave_id: leave_id,
        },
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            var leave_type_id = response["data"][0]["leave_policy_id"];
            fetchLeaveType(leave_type_id);
            $("#editleavetypeid").val(response["data"][0]["leave_id"]);
            $("#editleavestartdate").val(response["data"][0]["start_date"]);
            $("#editleaveenddate").val(response["data"][0]["end_date"]);
            $("#editleavereason").html(response["data"][0]["reason"]);
            if (response["data"][0]["allow_half"] == 1){
                $("#edittest1").prop( "checked", true );   
            }
            if(response["data"][0]["allow_short"] == 1)
            {
                $("#edittestshort1").prop( "checked", true );
            }
            $("#edit_leave").modal("show");
        },
    });
});

// Edit Leave form
$("#editleave").submit(function (e) {
    if ($("#edittest1").prop("checked") == true) {
        var halfleave1 = 1;
    } else {
        var halfleave1 = 0;
    }
    if ($("#edittestshort1").prop("checked") == true) {
        var shortleave1 = 1;
    } else {
        var shortleave1 = 0;
    }
    var formData = {
        user_id: localStorage.userid,
        leave_policy: $("select[name=editleaveid]").val(),
        leave_id: $("input[name=editleavetypeid]").val(),
        start_date: $("input[name=editleavestartdate]").val(),
        end_date: $("input[name=editleaveenddate]").val(),
        reason: $("textarea[name=editleavereason]").val(),
        allow_half: halfleave1,
        allow_short: shortleave1,
    };
    e.preventDefault();
    $.ajax({
        type: "POST",
        url: base_url + "editLeave",
        data: formData, // our data object
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        console.log(response);

        var jsonDa = response;
        var jsonData = response["data"]["message"];
        var falsedata = response["data"];
        if (jsonDa["data"]["status"] == "1") {
            toastr.success(jsonData);
            setTimeout(function () {
                window.location = "<?php echo base_url()?>LeaveSelf_emp";
            }, 1000);
        } else {
            toastr.error(falsedata);
            $("#editleave [type=submit]").attr("disabled", false);
        }
    });
});

/*Delete Leave */
$(document).on("click", ".delete_data", function () {
    var leave_id = $(this).attr("id");
    $(".delete_leave_button").attr("id", leave_id);
    $("#delete_leave").modal("show");
});

$(document).on("click", ".delete_leave_button", function () {
    var leave_id = $(this).attr("id");
    $.ajax({
        url: base_url + "deleteLeave",
        method: "POST",
        data: {
            leave_id: leave_id,
        },
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            console.log(response);
            window.location.replace("<?php echo base_url()?>LeaveSelf_emp");
        },
    });
});

// Add Leave validation form-->
$(document).ready(function () {
    $("#addleavebutton").click(function (e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#addleaveid").val() == null) {
            $("#addleaveid").focus();
            $("#addleavetypevalidate").text("Please select a leave type.");
            errorCount++;
        } else {
            $("#addleavetypevalidate").text("");
        }

        if ($("#addleavestartdate").val().trim() == "") {
            $("#addleavestartdatevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addleavestartdatevalidate").text("");
        }

        if ($("#addleaveenddate").val().trim() == "") {
            $("#addleaveenddatevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addleaveenddatevalidate").text("");
        }

        if ($("#addleavereason").val().trim() == "") {
            $("#addleavereason").focus();
            $("#addleavereasonvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addleavereasonvalidate").text("");
        }

        if (errorCount > 0) {
            return false;
        }
    });
});

// Edit Leave validation form-->
$(document).ready(function () {
    $("#editleavebutton").click(function (e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#editleaveid").val() == null) {
            $("#editleaveid").focus();
            $("#editleavetypevalidate").text("Please select a leave type.");
            errorCount++;
        } else {
            $("#editleavetypevalidate").text("");
        }

        if ($("#editleavestartdate").val().trim() == "") {
            $("#editleavestartdatevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editleavestartdatevalidate").text("");
        }

        if ($("#editleaveenddate").val().trim() == "") {
            $("#editleaveenddatevalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editleaveenddatevalidate").text("");
        }

        if ($("#editleavereason").val().trim() == "") {
            $("#editleavereason").focus();
            $("#editleavereasonvalidate").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#editleavereasonvalidate").text("");
        }

        if (errorCount > 0) {
            return false;
        }
    });
});
</script>