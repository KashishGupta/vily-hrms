<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">On Boarding</li>
            </ul>
            <div class="header-action mt-3">
                <a href="<?php echo base_url(); ?>onboardingList" class="btn btn-info grid-system"><i class="fe fe-list"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid mt-3">
        <div class="row clearfix kt-widget__items"></div>
    </div>
</div>


<div id="onboarding_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit On Boarding Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editstatus" method="POST" action="#">
                <div class="modal-body">
                    <div class="form-header">
                        <p>Are you sure want to Active Or Inactive for this on boarding status?</p>
                    </div>

                    <div class="form-group">
                        <input value="1" id="editstesttatus" name="editstesttatus" class="form-control" type="hidden">
                        <input value="" id="editstatusidt" name="editstatusidt" class="form-control" type="hidden">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success submit-btn">Yes Change It!</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Show Employee
    $.ajax({
        url: base_url + "viewOnboardingUser",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-xl-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>OnBoarding Pending</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
                var html =
                    '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="profile-widget ribbon"><div class="ribbon-box cyan">Pending</div><div class="profile-img mt-4"><img class="img-thumbnail w100 h100" src="' +
                    UserImage +
                    '" alt=""></div><div class="dropdown profile-action"><a href="#" class="action-icon" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-156px, 18px, 0px);"><a class="dropdown-item" href="<?php echo base_url(); ?>onBoardingAddDocument/' +
                    response.data[i].id +
                    '"><i class="fe fe-plus"></i>Add Emp Document </a><a class="dropdown-item" href="<?php echo base_url(); ?>onBoardingAddDetails/' +
                    response.data[i].id +
                    '"><i class="fe fe-plus"></i>Add Emp Details </a><a class="dropdown-item" href="<?php echo base_url(); ?>onBoardingView/' +
                    response.data[i].id +
                    '"><i class="fa fa-eye"></i>View Document</a> <a class="dropdown-item delete_data" href="#" data-toggle="modal" data-target="#onboarding_status" id="' +
                    response.data[i].id +
                    '"><i class="fa fa-edit"></i>Change Status</a></div></div><h4 class="user-name"><a href="<?php echo base_url(); ?>onBoardingView/' +
                    response.data[i].id +
                    '">' +
                    response.data[i].first_name +
                    '</a></h4><div class="small text-muted"><span>' +
                    response.data[i].emp_id +
                    "</span>-<span>" +
                    response.data[i].designation_name +
                    "</span> </div><span class='text-ellipsis'>" +  response.data[i].email +  "</span><span>" +
                    response.data[i].phone +
                    "</span></br></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });


    // Edit Status
    $(document).on("click", ".delete_data", function () {
        var user_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewOnboardingUser",
            method: "POST",
            data: {
                user_id: user_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editstatusidt").val(response["data"][0]["id"]);
                $("#onboarding_status").modal("show");
            },
        });
    });

    // Edit Onboardong Active  form
    $("#editstatus").submit(function (e) {
        var formData = {
            user_id: $("input[name=editstatusidt]").val(),
            status: $("input[name=editstesttatus]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editStatusdoc",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>onBoardingComplete";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editstatus [type=submit]").attr("disabled", false);
            }
        });
    });
</script>
