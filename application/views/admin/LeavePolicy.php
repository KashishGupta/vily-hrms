<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Leave Policy</li>
            </ul>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs b-none">
                                <li class="nav-item">
                                    <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list"><i class="fa fa-list-ul"></i> Leave Policy</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addnew"><i class="fa fa-plus"></i> Add Leave Policy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="list" role="tabpanel">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive" id="users">
                            <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="leavePolicy">
                                <thead>
                                    <tr>
                                        <th class="text-center">Policy Name</th>
                                        <th class="text-center">Leave Type</th>
                                        <th class="text-center">Created at</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="leavePolicytable"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="addnew" role="tabpanel">
                <div class="card">
                    <form id="addleavePloicy" method="POST">
                    	<div class="card-header">
			                <h3 class="card-title"><strong>Basic Details</strong></h3>
			            </div>
                    	<div class="card-body row clearfix">
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Policy Name<span class="text-danger"> *</span></label><br />
                                <span id="addpolicynamevalid" class="text-danger change-pos"></span>
                                <input name="addpolicyname" id="addpolicyname" class="form-control" type="text" placeholder="Name of the policy that will be displayed." autocomplete="off" />
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Leave Type<span class="text-danger"> *</span></label><br />
                                <span id="addleavetypevalid" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="addleavetype" id="addleavetype"></select>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is Earned Leave</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isEarned" name="isEarned" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <p class="font-12">Can leaves be Earned Leave employee.</p>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Total Per Year<span class="text-danger"> *</span></label><br />
                                <span id="addtotalperyearvalid" class="text-danger change-pos"></span>
                                <input
                                    name="addtotalperyear"
                                    id="addtotalperyear"
                                    onkeypress="return onlyNumberKey(event)"
                                    class="form-control"
                                    type="text"
                                    placeholder="Number of leaves allotted per year according to policy."
                                    autocomplete="off"
                                />
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Description<span class="text-danger"> </span></label><br />
                                <span id="adddescriptionvalid" class="text-danger change-pos"></span>
                                <input name="adddescription" id="adddescription" class="form-control" type="text" placeholder="Add Description." autocomplete="off" />
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Apply before days<span class="text-danger"> </span></label><br />
                                <span id="addapplybeforevalid" class="text-danger change-pos"></span>
                                <input name="addapplybefore" id="addapplybefore" class="form-control" type="text"  onkeypress="return onlyNumberKey(event)" placeholder="Min. no of days of gap from the date of leave application to the first date of applicable leave." autocomplete="off"
                                />
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Accrual Method <span class="text-danger"> </span></label><br />
                                <span id="addaccuralvalid" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="addaccural" id="addaccural"></select>
                            </div>
                           <!-- <div class="form-group col-lg-4 col-md-6 col-sm-12" id="hideshow">
                                <label>Accrual Period<span class="text-danger"> </span></label><br />
                                <span id="addaccuralperiodvalid" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="addaccuralperiod" id="addaccuralperiod">
                                <option value="">Select Accrual Period</option>
                                    <option value="Weekly">Weekly</option>
                                    <option value="Monthly">Monthly</option>
                                </select>
                            </div>-->
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is Accrual Fixed</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isAccuralFixed" name="isAccuralFixed" checked onclick="return false;" onkeydown="return false;" class="custom-switch-input" readonly="" checked="" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                            </div>
                            <style></style>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is Accrual reset</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isAccuralreset" name="isAccuralreset" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is Accrual at beginning</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isAccrualbeginning" name="isAccrualbeginning" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <p class="font-12">Does accrual happened at the beginning or at the end of the period.</p>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Max. carry days<span class="text-danger"> </span></label><br />
                                <span id="carryDaysvalid" class="text-danger change-pos"></span>
                                <input name="carryDays" id="carryDays" class="form-control" onkeypress="return onlyNumberKey(event)" type="text" placeholder="How many days of leave can carry forward year on year." autocomplete="off" />
                            </div>
                            
                            
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is Encashable</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isEncashable" name="isEncashable" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <p class="font-12">Can leaves be encashed at end of year or on employee relieving.</p>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Allow Back Date</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isBackDate" name="isBackDate" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <p class="font-12">Can leaves be allow to back Date.</p>
                            </div>
                        </div>

                        <div class="card-header">
			                <h3 class="card-title"><strong>Advance Details</strong></h3>
			            </div>
			            <div class="card-body row clearfix">
                           	<!-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                <label>Waiting Period Unit<span class="text-danger"></span></label><br />
                                <span id="addaccuralperiodvalid" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="WaitingPeriod" id="WaitingPeriod">
                                    <option value="">Select Waiting Period Unit</option>
                                    <option value="none">None</option>
                                    <option value="Day">Day</option>
                                    <option value="Week">Week</option>
                                    <option value="Week">Month</option>
                                </select>
                                <p class="font-12">If there is a wiating period then is that unit in days, weeks or months</p>
                            </div>-->

                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Waiting Period length<span class="text-danger"> </span></label><br>
                                <span id="addwaitingperiodvalid" class="text-danger change-pos"></span>
                                <input name="addwaitingperiod" id="addwaitingperiod" class="form-control" type="text" placeholder="Is there a waiting time before leaves start accruing" autocomplete="off">
                            </div>

                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is holiday excluded</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isholidayexcluded" name="isholidayexcluded" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <p class="font-12">If any company holidays fall between leave apply days, will that be counted?</p>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is weekend excluded</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isweekendexcluded" name="isweekendexcluded" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <p class="font-12">If any weekends (as defined in attendance policy) fall between leave apply days, will that be counted?</p>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Limit Inclusion<span class="text-danger"> </span></label><br />
                                <span id="addInclusionvalid" class="text-danger change-pos"></span>
                                <input name="addInclusion" id="addInclusion" class="form-control" onkeypress="return onlyNumberKey(event)" type="text" placeholder="Add limit inclusion." autocomplete="off" />
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Max. Consecutive<span class="text-danger"> </span></label><br />
                                <span id="addConsecutivevalid" class="text-danger change-pos"></span>
                                <input
                                    name="addConsecutive"
                                    id="addConsecutive"
                                    class="form-control"
                                    onkeypress="return onlyNumberKey(event)"
                                    type="text"
                                    placeholder="Maximum no. of leaves of same type that may be applied at one go."
                                    autocomplete="off"
                                />
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Max. Balance Days<span class="text-danger"> </span></label><br />
                                <span id="addBalancevalid" class="text-danger change-pos"></span>
                                <input name="addBalance" id="addBalance" class="form-control" onkeypress="return onlyNumberKey(event)" type="text" placeholder="How many days of leaves can accrue in total." autocomplete="off" />
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Is DOJ considered</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="isDOJconsidered" name="isDOJconsidered" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <div class="form-label">Negative Balance Allowed</div>
                                <div class="custom-controls-stacked" style="margin-top: 19px;">
                                    <label class="custom-control custom-radio custom-control-inline">
                                        <input type="checkbox" id="NegativeBalanceAllowed" name="NegativeBalanceAllowed" class="custom-switch-input" />
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <p class="font-12">Can the balance become negative (i.e. whether policy is enforced in a strict way)</p>
                            </div>
                            <div class="col-lg-12 mt-3 text-right">
                                <button type="submit" id="addleavepolicybutton" class="btn btn-primary">Add Policy</button>
                                <button type="reset" id="test" class="btn btn-default">Reset</button>
                            </div>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal custom-modal fade" id="delete_leavepolicy" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" id="" class="btn btn-danger continue-btn delete_leavepolicy_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #hideshow, #hideshow1{
        display: none;
    }

</style>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#addpolicynamevalid").text("");
            jQuery("#addleavetypevalid").text("");
            jQuery("#addtotalperyearvalid").text("");
            jQuery("#adddescriptionvalid").text("");
            jQuery("#addapplybeforevalid").text("");
            jQuery("#addaccuralvalid").text("");
            jQuery("#carryDaysvalid").text("");
            jQuery("#addwaitingperiodvalid").text("");
            jQuery("#addInclusionvalid").text("");
            jQuery("#addConsecutivevalid").text("");
            jQuery("#addBalancevalid").text("");
        });
    });

    $(function () {
        $("#addaccural").change(function () {
            if ($(this).val() == "Throughout Every Period") {
                $("#hideshow").show();
            } else {
                $("#hideshow").hide();
            }
        });
    });

    $(function () {
        $("#WaitingPeriod").change(function () {
            if ($(this).val() == "none") {
                $("#hideshow1").hide();
            } else {
                $("#hideshow1").show();
            }
        });
    });

    // View Leave Type
    $.ajax({
        url: base_url + "viewLeavePloicies",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        //var j = 1;
        var table = document.getElementById("leavePolicytable");
        for (i in response.data) {
            const d = new Date(response.data[0].created_at);
            const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
            const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
            const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
            endDate = `${da} ${mo} ${ye}`;
            var tr = document.createElement("tr");
            tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
                '<td class="text-center">' +
                response.data[i].policy_name +
                "</td>" +
                '<td class="text-center" style="white-space: break-spaces;">' +
                response.data[i].leave_type +
                "</td>" +
                '<td class="text-center">' +
                endDate +
                "</td>" +
                '<td class="text-center"><a href="Leavepolicy_edit/' +
                response.data[i].id +
                '" class="btn btn-info" title="Edit Employee" id="undefined"><i class="fa fa-edit"></i></a> <button type="button" data-toggle="modal" data-target="#delete_leavepolicy" aria-expanded="false" id="' +
                response.data[i].id +
                '"  class="btn btn-danger delete_data" title="Delete Leave Type"><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr);
        }
        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#leavePolicy").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Reimbursement Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Reimbursement Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                }
            ]
        });
    });

    // Delete Leave Type
    $(document).on("click", ".delete_data", function () {
        var leave_type_id = $(this).attr("id");
        $(".delete_leavepolicy_button").attr("id", leave_type_id);
        $("#delete_leavepolicy").modal("show");
    });

    $(document).on("click", ".delete_leavepolicy_button", function () {
        var leave_policy_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteLeavePolicy",
            method: "POST",
            data: {
                leave_policy_id: leave_policy_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>LeavePolicy";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_leavepolicy_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });
    $(document).ready(function () {
        $("#addleavepolicybutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addpolicyname").val().trim() == "") {
                $("#addpolicyname").focus();
                $("#addpolicynamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addpolicynamevalid").text("");
            }

            if ($("#addleavetype").val() == null) {
                $("#addleavetype").focus();
                $("#addleavetypevalid").text("Please select a leave type.");
                errorCount++;
            } else {
                $("#addleavetypevalid").text("");
            }

            if ($("#addtotalperyear").val().trim() == "") {
                $("#addtotalperyear").focus();
                $("#addtotalperyearvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addtotalperyearvalid").text("");
            }

/*
            if ($("#adddescription").val().trim() == "") {
                $("#adddescription").focus();
                $("#adddescriptionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#adddescriptionvalid").text("");
            }

            if ($("#addapplybefore").val().trim() == "") {
                $("#addapplybefore").focus();
                $("#addapplybeforevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addapplybeforevalid").text("");
            }

            if ($("#addaccural").val() == null) {
                $("#addaccural").focus();
                $("#addaccuralvalid").text("Please select a accural method.");
                errorCount++;
            } else {
                $("#addaccuralvalid").text("");
            }

            if ($("#carryDays").val().trim() == "") {
                $("#carryDays").focus();
                $("#carryDaysvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#carryDaysvalid").text("");
            }


            if ($("#addwaitingperiod").val().trim() == "") {
                $("#addwaitingperiod").focus();
                $("#addwaitingperiodvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addwaitingperiodvalid").text("");
            }

            if ($("#addInclusion").val().trim() == "") {
                $("#addInclusion").focus();
                $("#addInclusionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addInclusionvalid").text("");
            }

            if ($("#addConsecutive").val().trim() == "") {
                $("#addConsecutive").focus();
                $("#addConsecutivevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addConsecutivevalid").text("");
            }

            if ($("#addBalance").val().trim() == "") {
                $("#addBalance").focus();
                $("#addBalancevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addBalancevalid").text("");
            }
*/
            if (errorCount > 0) {
                return false;
            }
        });
    });
    // Select Leave Type by api*/
    $.ajax({
        url: base_url + "viewLeaveType",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addleavetype");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Leave Type</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.leave_type));
        });
    });

     
    // Select Leave Type by api*/
    $.ajax({
        url: base_url + "viewAccuralForLeave",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addaccural");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Select From Accrual Type</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.accrual_name).text(entry.accrual_name));
        });
    });
    // Add Leave Type
    $("#addleavePloicy").submit(function (e) {
        if ($("#isAccrualbeginning").prop("checked") == true) {
            var accural = 1;
        } else {
            var accural = 0;
        }
        if ($("#isAccuralFixed").prop("checked") == true) {
            var accuralFixed = 1;
        } else {
            var accuralFixed = 0;
        }
        if ($("#isAccuralreset").prop("checked") == true) {
            var accuralreset = 1;
        } else {
            var accuralreset = 0;
        }
        if ($("#isEncashable").prop("checked") == true) {
            var Encashable = 1;
        } else {
            var Encashable = 0;
        }
        if ($("#isholidayexcluded").prop("checked") == true) {
            var holidayexcluded = 1;
        } else {
            var holidayexcluded = 0;
        }
        if ($("#isweekendexcluded").prop("checked") == true) {
            var weekendexcluded = 1;
        } else {
            var weekendexcluded = 0;
        }
        if ($("#isDOJconsidered").prop("checked") == true) {
            var DOJconsidered = 1;
        } else {
            var DOJconsidered = 0;
        }
        if ($("#NegativeBalanceAllowed").prop("checked") == true) {
            var NegativeBalance = 1;
        } else {
            var NegativeBalance = 0;
        }
        if ($("#isBackDate").prop("checked") == true) {
            var BackDate = 1;
        } else {
            var BackDate = 0;
        }
        if ($("#isEarned").prop("checked") == true) {
            var EarnedLeave = 1;
        } else {
            var EarnedLeave = 0;
        }
        
        var formData = {
            leave_type_id: $("select[name=addleavetype]").val(),
            policy_name: $("input[name=addpolicyname]").val(),
            total_per_year: $("input[name=addtotalperyear]").val(),
            description: $("input[name=adddescription]").val(),
            apply_before_days: $("input[name=addapplybefore]").val(),
            accural_method: $("select[name=addaccural]").val(),
           // accrual_period:$("select[name=addaccuralperiod]").val(),
            is_accrual_fixed: accuralFixed,
            is_accrual_reset: accuralreset,
            is_accrual_beginning: accural,
            max_carry_days: $("input[name=carryDays]").val(),
            is_encashable: Encashable,
            back_date :BackDate,
            is_holiday_excluded: holidayexcluded,
            waiting_period_length: $("input[name=addwaitingperiod]").val(),
            is_weekend_excluded: weekendexcluded,
            limit_inclusion: $("input[name=addInclusion]").val(),
            max_consecutive: $("input[name=addConsecutive]").val(),
            max_balance_days: $("input[name=addBalance]").val(),
            is_doj_considered: DOJconsidered,
            check_leave_type :EarnedLeave,
            negative_balance_allowed: NegativeBalance,
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addLeavePolicy",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>LeavePolicy";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addleavetype [type=submit]").attr("disabled", false);
            }
        });
    });
</script>
