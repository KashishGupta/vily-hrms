<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
				<li class="breadcrumb-item active">Module Addon</li>
			</ul>
        </div>
        <div class="card mt-3">
            <div class="card-header"><h3 class="card-title"><strong>Module Addon</strong></h3></div>
            <div class="row card-body kt-widget__items">
                
            </div>
        </div>
    </div>            
</div>       






<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

$.ajax({
    url: base_url + "viewsModules",
    data: {},
    type: "POST",   
    dataType: "json",
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})

.done(function (response) {
    if (!$.trim(response.data[0])) {
        var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
        $(".kt-widget__items").append(html);
    } 

    else {
        for (i in response.data) {
            var isstatus = "";
                if (response.data[i].status == 0) {
                    isstatus = "uncheck";
                }
                if (response.data[i].status == 1) {
                    isstatus = "checked";
                }
                var ischecked = "";
                if (response.data[i].status == 0) {
                    ischecked = "Disabled";
                }
                if (response.data[i].status == 1) {
                    ischecked = "Enabled";
                }
                
            var html ='<div class="col-md-6 col-lg-4"><div class="settings-power-up"><div class="settings-power-up-image" style="background-image: url(<?php echo base_url()?>assets/images/'+response.data[i].image_name+');"> </div> <div class="settings-power-up-content"><h2>' +
                    response.data[i].name +
                    '</h2> <p>'+response.data[i].description +'</p> <div class="settings-container"> <div class="custom-controls-stacked text-right"> <label class="custom-control custom-checkbox"><input type="checkbox" ' + isstatus + ' id="' + response.data[i].id + '" value="' + response.data[i].status + '" onclick="getstatus(' + response.data[i].id + ')"> <span> ' + ischecked + ' </span></label></div></div></div></div></div>';
            $(".kt-widget__items").append(html);
        }
    }
});



function getstatus(id) { 
    var module_id = id;
        var module_status = $("#" + id).val();
       
  
        if (module_status == 0) {
            var module_status = 1;
       
                  $.ajax({
                    type: "POST",
                    url: base_url + "editModulestatus",
                    data: "module_id=" + module_id + "&module_status=" + module_status,
                    dataType: "json",
                    encode: true,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Token", localStorage.token);
                    },
                })

                .done(function (response) {
                    console.log(response);
                    var jsonDa = response;
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function () {
                            window.location = "<?php echo base_url()?>moduleAddon";
                        }, 1000);
                    } else {
                        toastr.error(falsedata);
                        $("#addtask [type=submit]").attr("disabled", false);
                    }
                });
            } else {
            var module_status = 0;
            $.ajax({
                type: "POST",
                url: base_url + "editModulestatus",
                data: "module_id=" + module_id + "&module_status=" + module_status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>moduleAddon";
                }, 1000);
            });
        }
        }






</script>