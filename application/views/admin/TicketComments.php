<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Tickets">Tickets</a></li>
                <li class="breadcrumb-item active">Ticket Comments</li>
            </ul>
        </div>
        <div class="row clearfix mt-3">
            <div class="col-lg-3 col-md-12">
                <div class="card">
                    <div class="card-body text-center">
                        <div class="mb-3" id="fgfimage"></div>
                        <h6 class="mt-3 mb-0" id="profilenames"></h6>
                        <span id="useremail"></span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ticket Info</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <small class="text-muted">Ticket ID: </small>
                                <p class="mb-0 text-uppercase" id="ticketid"></p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">Ticket Type: </small>
                                <p class="mb-0" id="ticketType"></p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">Department: </small>
                                <p class="mb-0" id="ticketsendTodepartment"></p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">Date: </small>
                                <p class="mb-0" id="ticketDate"></p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">Ticket Status: </small>
                                <p class="mb-0" id="ticketstatus"></p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">Ticket Priority: </small>
                                <p class="mb-0" id="ticketpriority"></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ticket Details</h3>
                    </div>
                    <div class="card-body">
                        <span id="tickettitle"> </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ticket Replies</h3>
                    </div>
                    <div class="card-body ticket_comment_replay Kt-ticketComments"></div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form id="edittickets" method="POST">
                            <input type="hidden" id="ticketsid" name="ticketsid" class="form-control" value="" />
                            <div class="form-group col-md-12">
                                <label>Comments<span class="text-danger"> *</span></label>
                                <br />
                                <span id="editsubjectvalidate" class="text-danger change-pos"></span>
                                <textarea class="form-control summernote" cols="3" name="addcomments" id="addcomments" placeholder="Enter text here..."></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label>Image<span class="text-danger"> *</span></label>
                                <br />
                                <span class="text-danger change-pos" id="imagevalid"></span>
                                <input type="file" class="dropify form-control" name="image" id="image" onchange="load_file();" data-allowed-file-extensions="png jpg jpeg pdf docx" data-max-file-size="5MB" />
                                <input type="hidden" class="form-control text-ellipsis" id="12arrdata" />
                            </div>
                            <div class="submit-section pull-right">
                                <button id="editTicketbutton" class="btn btn-success submit-btn">Add Comment</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /Edit Ticket Modal -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
    function load_file() {
        if (!window.FileReader) {
            return alert("FileReader API is not supported by your browser.");
        }
        var $i = $("#image"), // Put file input ID here
            input = $i[0]; // Getting the element from jQuery
        if (input.files && input.files[0]) {
            file = input.files[0]; // The file
            fr = new FileReader(); // FileReader instance
            fr.onload = function () {
                // Do stuff on onload, use fr.result for contents of file
                $("#12arrdata").val(fr.result);
            };
            //fr.readAsText( file );
            fr.readAsDataURL(file);
        } else {
            // Handle errors here
            alert("File not selected or browser incompatible.");
        }
    }
     var ticket_id = <?php echo $ticketid; ?>;
     $('#ticketsid').val(ticket_id);
        // View Notification Public
        $.ajax({
            url: base_url + "viewTicketsComments",
            data: {ticket_id: ticket_id},
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            if (!$.trim(response.data[0])) {
                var html =
                    '<div class="col-xl-12 text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div>';
                $(".Kt-ticketComments").append(html);
            } else {
            for (i in response.data) {
                const d = new Date(response.data[i].created_at);
                        const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
                        const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
                        const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
                        endDate = `${da} ${mo} ${ye}`;
                        var testcheck = "";
                    
                    if (response.data[i].image_name == 0) {
                        testcheck = "";
                    }else{
                        testcheck = '<a href="'+response.data[i].image_name+'" download>document </a>';
                    }
                var html ='<div class="timeline_item"><img class="tl_avatar" src="'+response.data[i].user_image+'" alt="" /><span><a href="javascript:void(0);" title="">'+response.data[i].first_name+'</a> <small class="float-right text-right">'+endDate+' - '+timeago(response.data[i].created_at)+'</small></span><div class="msg">'+response.data[i].comment+'</div><span>'+testcheck+'</span></div></div>';
                $(".Kt-ticketComments").append(html);
            }
        }
        });

         //edit Tickets form
         $("#edittickets").submit(function (e) {
            var formData = {
                comment: $("textarea[name=addcomments]").val(),
                ticket_id: $("input[name=ticketsid]").val(),
                image_name: $("#12arrdata").val(),
            };

            e.preventDefault();
            $.ajax({
                type: "POST",
                url: base_url + "addTicketsComments",
                data: formData, // our data object
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url();?>TicketComments/"+ticket_id;
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#edittickets [type=submit]").attr("disabled", false);
                }
            });
        });

        $.ajax({
                url: base_url + "viewTickets",
                method: "POST",
                data: {
                    ticket_id: ticket_id,
                },
                dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
                success: function (response) {
                    if (!$.trim(response.data[0].user_image)) {
                    $("#fgfimage").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
            } else {
                $("#fgfimage").append('<img class="img-thumbnail" src="' + response.data[0].user_image + '" alt="" style="width:200px; border-radius: 15px;" />');

                }
                if(response.data[0].status = 0)
                {
                    $("#ticketstatus").css({"color":"red"}).html('Inactive');
                }else{
                    $("#ticketstatus").css({"color":"green"}).html('Active');
                }
                if(response.data[0].priority  = 0)
                {
                    $("#ticketpriority").css({"color":"blue"}).html('Low');
                }else if(response.data[0].priority  = 1){
                    $("#ticketpriority").css({"color":"green"}).html('Medium');
                }else{
                    $("#ticketpriority").css({"color":"red"}).html('High');
                }

                var ticketdate = new Date(response.data[0].date);
                    var dd = String(ticketdate.getDate()).padStart(2, '0');
                    var mm = String(ticketdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = ticketdate.getFullYear();

                    ticketdate = dd + '-' + mm + '-' + yyyy;
                    $("#ticketDate").html(ticketdate);
                $("#profilenames").html(response.data[0].first_name);
                $("#tickettitle").html(response.data[0].title);
                $("#useremail").html(response.data[0].email);
                $("#ticketid").html(response.data[0].ticket_no);
                $("#ticketType").html(response.data[0].ticket_type);
                $("#ticketsendTodepartment").html(response.data[0].send_department);

            },
            });

            function timeago(stringDate)
        {

              var currDate = new Date();
              var diffMs=currDate.getTime() - new Date(stringDate).getTime();

              var sec=diffMs/1000;
             // alert(sec);
              if(sec<=60)
                  return '<span style = "color:red;">just now</span>';
              var min=sec/60;
              if(min<60)
                  return parseInt(min)+' minute'+(parseInt(min)>1?'s ago':' ago');
              var h=min/60;
              if(h<=24)
                  return (parseInt(h)>1? parseInt(h) +' hrs ago': ' an hour ago');
              var d=h/24;
              if(d<=7)
                  return parseInt(d)+' day'+(parseInt(d)>1?'s ago':' ago');
              var w=d/7;
              if(w<=4.3)
                  return (parseInt(w)>1? parseInt(w)+' weeks ago': ' a week ago');
              var m=d/30;
              if(m<=12)
                  return parseInt(m)+' month'+(parseInt(m)>1?'s ago':' ago');
              var y=m/12;
              return parseInt(y)+' year'+(parseInt(y)>1?'s ago':' ago');
       }
</script>
