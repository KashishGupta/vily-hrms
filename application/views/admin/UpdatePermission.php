<?php $input_params=$this->input->get(); $arrParams =explode("/", $_SERVER['REQUEST_URI']); $role_id=(end($arrParams)); ?>
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Role">Role</a></li>
                <li class="breadcrumb-item active">Add Permissions</li>
            </ul>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-body">
                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="table-responsive">
                            <form method="post" id="userPermission">
                            <input type="checkbox" onclick="checkAll(this)">
                                <table class="table table-striped custom-table" id="roletable">
                                    <thead>
                                        <tr>
                                            <th>Module Permission</th>
                                            <th class="text-center">View</th>
                                            <th class="text-center">Edit</th>
                                            <th class="text-center">Create</th>
                                            <th class="text-center">Delete</th>
                                            <th class="text-center">Check All</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tablebodyrole">

                                    </tbody>
                                </table>
                                <div class="submit-section pull-right">
                                    <button id="addbranchbutton" class="btn btn-success submit-btn">Submit</button>
                                    <button type="reset" class="btn btn-secondary" id="test" data-dismiss="modal">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
function checkAll(bx) {
  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
  }
}
function checkAllCheckBox(value,VL_id)
{
    var statustest = VL_id;
    var test = "check_";
    var var3 = "."+test + statustest;
    if($('#select_all_').is(':checked')){
   $(var3).attr ( "checked" ,"checked" );
    }
    else
    {
        $(var3).removeAttr('checked');
    }

 }


//View All Resources
var role_id = "<?php echo $role_id;?>";
$.ajax({
        url: base_url + "viewAllResources",
        data: {
            role_id: role_id,
        },
        type: "POST",
        dataType: 'json',
        encode: true,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Token', localStorage.token);
        }
    })
    .done(function(response) {

        var table = document.getElementById('tablebodyrole');
        console.log(response.data);
        for (i in response.data) {
            var canaddcheck = "";
            if (response.data[i].can_add == 'Y') {
                canaddcheck = "checked";
            }
            if (response.data[i].can_add == 'N') {
                canaddcheck = "unchecked";
            }
            var canviewcheck = "";
            if (response.data[i].can_view == 'Y') {
                canviewcheck = "checked";
            }
            if (response.data[i].can_view == 'N') {
                canviewcheck = "unchecked";
            }
            var caneditcheck = "";
            if (response.data[i].can_edit == 'Y') {
                caneditcheck = "checked";
            }
            if (response.data[i].can_edit == 'N') {
                caneditcheck = "unchecked";
            }
            var candeletecheck = "";
            if (response.data[i].can_delete == 'Y') {
                candeletecheck = "checked";
            }
            if (response.data[i].can_delete == 'N') {
                candeletecheck = "unchecked";
            }
            var tr = document.createElement('tr');
            tr.innerHTML =
                '<td>' + response.data[i].display_name +
                '</td>' +
                '<td class="text-center"><input class="check_'+response.data[i]
                .id+'"  name="resource_permission[<?php echo $role_id;?>]['+response.data[i]
                .id+'][read]" ' + canviewcheck +
                ' type="checkbox" value="Y"  /></td>' +
                '<td class="text-center"><input class="check_'+response.data[i]
                .id+'"  name="resource_permission[<?php echo $role_id;?>]['+response.data[i]
                .id+'][write]" ' + caneditcheck +
                ' type="checkbox" value="Y"  /></td>' +
                '<td class="text-center"><input class="check_'+response.data[i]
                .id+'"   name="resource_permission[<?php echo $role_id;?>]['+response.data[i]
                .id+'][create]" ' + canaddcheck +
                ' type="checkbox" value="Y"  /</td>' +
                '<td class="text-center"><input class="check_'+response.data[i]
                .id+'"   name="resource_permission[<?php echo $role_id;?>]['+response.data[i]
                .id+'][delete]" ' +
                candeletecheck +
                '  type="checkbox" value="Y"  /</td>'+
                '<td class="text-center"><input  onchange="checkAllCheckBox(this,'+response.data[i]
                .id+');" id="select_all_" type="checkbox" /</td>';


            table.appendChild(tr);
        }

    })
$('#isAgeSelected').change(function() {
    if ($(this).is(":checked")) { // or if($("#isAgeSelected").attr('checked') == true){
        $('#txtAge').show();
    } else {
        $('#txtAge').hide();
    }
});
//add Permission form
$("#userPermission").submit(function(e) {
    e.preventDefault();

        var formdata = {};
        var can_read = 0;
        var can_write = 0;
        var can_create = 0;
        var can_delete = 0;
    var resource_id = 0;
    var temp_resource_id = 0;
    var role_id = "";
    var arrTmpFormat = [];
    $("#userPermission input:checkbox[name=resource_permission]").each(function() {
        let arrVal = $(this).val().split("_");
      // alert(arrVal);
        resource_id = arrVal[0];
        var can_read = 'N';
        var can_write = 'N';
        var can_create = 'N';
        var can_delete = 'N';

        if (resource_id && $(this).prop('checked') == true) {
            console.log("checked");
            console.log($(this).prop('checked'));
            //alert(arrVal[1]);
            if (arrVal[1] == "read") {
               
                arrTmpFormat[resource_id]['can_read']='Y';
                //can_read = 'Y';
            } else if (arrVal[1] == "write") {
                arrTmpFormat[resource_id]['can_write']='Y';
                
                //can_write = 'Y';
            } else if (arrVal[1] == "create") {
                arrTmpFormat[resource_id]['can_create']='Y';
               
               
                //can_create = 'Y';
            } else if (arrVal[1] == "delete") {
                arrTmpFormat[resource_id]['can_delete']='Y';
               
               
               // can_delete = 'Y';
            }
            
           
        //alert("Read: "+can_read+"Write: "+can_write+"Create: "+can_create+"Delete: "+can_delete);
       

        } else {
            console.log("not checked");
        }

        formdata[resource_id] = {
            resource_id: resource_id,
            role_id: "<?php echo $role_id;?>",
           
        };
    });
    
    var myJSON = JSON.stringify(formdata);
    console.log(formdata);


    $.ajax({
            url: base_url + "addResources",
            data: $('#userPermission').serialize(),
            type: "POST",
            dataType: 'json',
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            },

        })
        .done(function(response) {
            console.log(response);
            var jsonarrData = response
            var jsonData = response["data"]["message"];
            var falsearrdata = response["data"];
            var role_id = "<?php echo $role_id;?>";
            if (jsonarrData["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function(){window.location ="<?php echo base_url(); ?>UpdatePermission/"+ role_id},1000);

            } else {
                toastr.error(falsearrdata);
                $('#userPermission [type=submit]').attr('disabled', false);
            }


        });

});
</script>