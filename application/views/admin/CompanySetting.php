<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
				<li class="breadcrumb-item active">Company Setting</li>
			</ul>
        </div>
        
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Company Setting</strong></h3>
            </div>
            <div class="card-body">
            	<form id="editcompanydetails" class="form-horizontal form-groups-bordered" method="post" accept-charset="utf-8">
                    <div class="row">

                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Company Name <span class="text-danger">*</span></label><br>
                                <span id="companynamevalid" class="text-danger change-pos"></span>
                                <input class="form-control" type="hidden" value="" id="ediid" name="ediid" placeholder="Please enter company name.">
                                <input class="form-control" type="text" value="" id="editcompanyname" name="editcompanyname" placeholder="Please enter company name.">
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Contact Person Name <span class="text-danger">*</span></label>
                                <br>
                                <span id="contactpersonvalid" class="text-danger change-pos"></span>
                                <input class="form-control" value="" type="text" id="editcontactperson" name="editcontactperson" placeholder="Please enter contact person.">
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Owner Name <span class="text-danger">*</span></label>
                                <br>
                                <span id="contactpersonvalid" class="text-danger change-pos"></span>
                                <input class="form-control" value="" type="text" id="editownername" name="editownername" placeholder="Please enter contact person.">
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Mobile Number <span class="text-danger">*</span></label>
                                <br>
                                <span id="mobilenumbervalid" class="text-danger change-pos"></span>
                                <input class="form-control" type="text"  name="editphoneno"  id="editphoneno"  placeholder="Please enter contact number.">
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                                <label>Email <span class="text-danger">*</span></label>
                                <br>
                                <span id="emailvalid" class="text-danger change-pos"></span>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="editcompanyemail" id="editcompanyemail" placeholder="Please enter email.">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Address <span class="text-danger">*</span></label>
                                <br>
                                <span id="addressvalid" class="text-danger change-pos"></span>
                                <textarea class="form-control" placeholder="Enter address." id="editcompanyaddress" name="editcompanyaddress" aria-label="With textarea"></textarea>
                            </div>
                        </div>
                       
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Country <span class="text-danger">*</span></label>
                                <br>
                                <span id="countryvalid" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="editcountryname" id="editcountryname">
                                        
                                        </select>
                               
                            </div>
                        </div>          

                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>State/Province <span class="text-danger">*</span></label>
                                <br>
                                <span id="statevalid" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="editstate" id="editstate">
                                        
                                        </select>
                                                           
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>City</label>
                                <select class="custom-select form-control" name="editcity" id="editcity">
                                        
                                        </select>
                               
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="form-group">
                                <label>Postal Code</label>
                                <input class="form-control"  name="editpostalcode" id="editpostalcode" type="text">
                            </div>
                        </div>
                       
                       

                       
                        <div class="form-group col-md-4 col-sm-12">
                                    <label>Company Logo<span class="text-danger"> *</span></label>
                                    <br />
                                    <?php 
                                        $queryfile = $this->db->query("SELECT * FROM company");
                                        $arrfile = $queryfile->row();
                                        $arrfile1 = $arrfile->company_logo;
                                    ?>
                                    <span class="text-danger change-pos" id="editemployeeimagevalidate"></span>
                                    <input type="file" class="dropify form-control" name="file" id="file" data-default-file="<?php echo $arrfile1;?>" onchange="load_file();"  data-max-file-size="10MB">
                                    <input type="hidden" id="1arrdata" />
                                </div>
                       
                      
                        <div class="col-sm-12 text-right m-t-20">
                        	<button type="submit" class="btn btn-info"> Submit </button>
                            <button type="reset" id="test" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>            
</div>  

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#companyname").val("");
            $("input#contactperson").prop("");
            $("input#mobilenumber").val("");
            $("input#address").prop("");
            $("input#email").val("");
            $("input#website").prop("");
            //$("#addbranchid option:eq(0)").prop("selected", true);
        });
        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });
        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });
    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#companynamevalid").text("");
            jQuery("#contactpersonvalid").text("");
            jQuery("#mobilenumbervalid").text("");
            jQuery("#addressvalid").text("");
            jQuery("#emailvalid").text("");
            jQuery("#websitevalid").text("");
            jQuery("#postalcodevalid").text("");
            jQuery("#companylogovalid").text("");
        });
    });

    function load_file() {
    if (!window.FileReader) {
        return alert("FileReader API is not supported by your browser.");
    }
    var $i = $("#file"), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
    if (input.files && input.files[0]) {
        file = input.files[0]; // The file
        fr = new FileReader(); // FileReader instance
        fr.onload = function () {
            // Do stuff on onload, use fr.result for contents of file
            $("#1arrdata").val(fr.result);
        };
        //fr.readAsText( file );
        fr.readAsDataURL(file);
    } else {
        // Handle errors here
        alert("File not selected or browser incompatible.");
    }
}
//Fetech Branch
function fetchCountry(countries_id) {
    $.ajax({
        url: base_url + "viewCountries",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editcountryname");

        dropdown.empty();

        dropdown.append("<option disabled>Choose Country</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (countries_id == entry.id) {
                dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.name));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.name));
            }
        });
    });
}

//Fetch Department
function fetchState(state_id) {
    $.ajax({
        url: base_url + "viewStates",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editstate");

        dropdown.empty();

        dropdown.append("<option disabled>Choose State</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (state_id == entry.id) {
                dropdown.append($("<option selected></option>").attr("value", entry.id).text(entry.name));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.name));
            }
        });
    });
}

//Fetch Designation
function fetchCity(city_id) {
    $.ajax({
        url: base_url + "viewCities",
        method: "POST",
        data: {},
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editcity");
        dropdown.empty();
        dropdown.append("<option disabled>Choose city</option>");
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            if (city_id == entry.id) {
                dropdown.append($("<option selected></option>").attr("value", entry.id).text(entry.name));
            } else {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.name));
            }
        });
    });
}
    $.ajax({
        url: base_url+"viewCompany",
        data: {},
        type: "POST",
        dataType    : 'json',
        beforeSend: function(xhr){
            xhr.setRequestHeader('Token', localStorage.token);
        },
        success:function(response){
            var countries_id = response["data"][0]["countries_id"];
        var state_id = response["data"][0]["state_id"];
        var city_id = response["data"][0]["city_id"];
        fetchCountry(countries_id);
        fetchState(state_id);
        fetchCity(city_id);
            $('#ediid').val(response["data"][0]["id"]);
            $('#editcompanyname').val(response["data"][0]["company_name"]);
            $('#editcontactperson').val(response["data"][0]["contactperson"]);
            $('#editcompanyaddress').val(response["data"][0]["company_address"]);
            $('#editownername').val(response["data"][0]["owner_name"]);
            $('#editphoneno').val(response["data"][0]["phone_no"]);
            $('#editcompanyemail').val(response["data"][0]["company_email"]);
            $('#editpostalcode').val(response["data"][0]["postal_code"]);
           
            $('#editcompanylogo').val(response["data"][0]["company_logo"]);
           
        }
    });
    $("#editcountryname").change(function () {
    $.ajax({
        url: base_url + "viewStates",
        data: { country_id: $("select[name=editcountryname]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editstate");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose State</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.name));
        });
    });
});

$("#editstate").change(function () {
    $.ajax({
        url: base_url + "viewCities",
        data: { state_id: $("select[name=editstate]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#editcity");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose city</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.name));
        });
    });
});
    $("#editcompanydetails").submit(function (e) {
        var formData = {
            company_id: $("input[name=ediid]").val(),
            company_name: $("input[name=editcompanyname]").val(),
            contactperson  : $("input[name=editcontactperson]").val(),
            company_address: $("textarea[name=editcompanyaddress]").val(),
            company_email: $("input[name=editcompanyemail]").val(),
            owner_name: $("input[name=editownername]").val(),
            phone_no: $("input[name=editphoneno]").val(),
            country_name: $("select[name=editcountryname]").val(),
            state: $("select[name=editstate]").val(),
            city  : $("select[name=editcity]").val(),
            postal_code: $("input[name=editpostalcode]").val(),
            fax_no: $("input[name=editfax_no]").val(),
            api_link: $("input[name=editapilink]").val(),
            company_logo: $("#1arrdata").val(),
            status: $("select[name=editstatus]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST", // define the type of HTTP verb we want to use (POST for our form)
            url: base_url + "editCompany", // the url where we want to POST
            data: formData, // our data object
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonData = response;
            var falseData = response["data"];
            var successData = response["data"]["message"];
            if (jsonData["data"]["status"] == "1") {
                toastr.success(successData);
                setTimeout(function () {
                    window.location = "<?php echo base_url();?>CompanySetting";
                }, 1000);
            } else {
                toastr.error(falseData);
                $("#editcompanydetails [type=submit]").attr("disabled", false);
            }
        });
    });

    //Company Setting Validation
    $(document).ready(function () {
        $("#companysetting").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#companyname").val().trim() == "") {
                $("#companynamevalid").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#companynamevalid").text("");
            }
            if ($("#contactperson").val().trim() == "") {
                $("#contactpersonvalid").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#contactpersonvalid").text("");
            }
            if ($("#mobilenumber").val().trim() == "") {
                $("#mobilenumbervalid").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#mobilenumbervalid").text("");
            }
            if ($("#address").val().trim() == "") {
                $("#addressvalid").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#addressvalid").text("");
            }
            if ($("#email").val().trim() == "") {
                $("#emailvalid").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#emailvalid").text("");
            }
            if ($("#website").val().trim() == "") {
                $("#websitevalid").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#websitevalid").text("");
            }
            if ($("#companylogo").val().trim() == "") {
                $("#companylogovalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#companylogovalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>