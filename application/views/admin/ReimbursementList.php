<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Reimbursement</li>
            </ul>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs b-none">
                                <li class="nav-item">
                                    <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list"><i class="fa fa-hand-holding-water"></i>Pending Reimbursement</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addnew"><i class="fa fa-thumbs-up"></i>Approved Reimbursement</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addhotelnew"><i class="fa fa-times-circle"></i>Declined Reimbursement</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="list" role="tabpanel">

                <!-- Search Employee List Result Table -->
                <div class="card mt-3">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Reimbursement's Pending List</strong></h3>
                        <div class="card-options">
                            <a href="<?php echo base_url(); ?>Reimbursement" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                        </div>
                    </div>

                    <div class="card-body">
                        <form id="filter-form" method="POST" action="#">
                            <div class="row">
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Branch <span class="text-danger"> *</span></label><br>
                                    <span id="addbranchvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="addbranchid" id="addbranchid" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Department<span class="text-danger"> *</span></label><br>
                                    <span id="adddepartmentidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Designation<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="adddesignationid" id="adddesignationid" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Users<span class="text-danger"> *</span></label><br>
                                    <span id="uservalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="adduserid" id="adduserid" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="filteremployeebutton" class="btn btn-success submit-btn">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-xl table-responsive-l" id="employeetablebody">
                            <thead>
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Branch</th>
                                    <th class="text-center">Start City</th>
                                    <th class="text-center">End City</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End date</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="employeedatatable"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="addnew" role="tabpanel">

                <!-- Search Employee List Result Table -->
                <div class="card mt-3">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Reimbursement's Approved List</strong></h3>
                        <div class="card-options">
                            <a href="<?php echo base_url(); ?>ReimbursementApproved" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="filter-form1" method="POST" action="#">
                            <div class="row">
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Branch <span class="text-danger"> *</span></label><br>
                                    <span id="addbranchvalidate1" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="addbranchid1" id="addbranchid1" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Department<span class="text-danger"> *</span></label><br>
                                    <span id="adddepartmentidvalidate1" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="adddepartmentid1" id="adddepartmentid1"/>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Designation<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate1" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="designation_id1" id="designation_id1" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Users<span class="text-danger"> *</span></label><br>
                                    <span id="uservalidate1" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="user_id1" id="user_id1" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="filteremployeebutton1" class="btn btn-success submit-btn">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-xl table-responsive-l" id="employeetablebody1">
                            <thead>
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Branch</th>
                                    <th class="text-center">Start City</th>
                                    <th class="text-center">End City</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End date</th>
                                    <th class="text-center">Employee Amount</th>
                                    <th class="text-center">Admin Amount</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="employeedatatable1"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="tab-pane fade" id="addhotelnew" role="tabpanel">

                <!-- Search Employee List Result Table -->
                <div class="card mt-3">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Reimbursement's Declined List</strong></h3>
                        <div class="card-options">
                            <a href="<?php echo base_url(); ?>ReimbursementDenied" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <form id="filter-form2" method="POST" action="#">
                            <div class="row">
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Branch <span class="text-danger"> *</span></label><br>
                                    <span id="addbranchvalidate2" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="addbranchid2" id="addbranchid2" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Department<span class="text-danger"> *</span></label><br>
                                    <span id="adddepartmentidvalidate2" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="adddepartmentid2" id="adddepartmentid2"/>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Designation<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate2" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="designation_id2" id="designation_id2" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Users<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate2" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="user_id2" id="user_id2" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="filteremployeebutton2" class="btn btn-success submit-btn">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl table-responsive-xl table-responsive-l" id="employeetablebody2">
                            <thead>
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Branch</th>
                                    <th class="text-center">Start City</th>
                                    <th class="text-center">End City</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End date</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Admin Amount</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="employeedatatable2"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Reimbursement Modal -->
<div id="reimbursement_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reimbursement Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editstatus" method="POST" action="#">
                    <div class="modal-body">
                        <div class="form-header">
                            <p>Are you sure want to change the reimbursement status?</p>
                        </div>
                        <div class="form-group">
                            <label>Reimbursement Status<span class="text-danger">*</span></label>
                            <input value="" id="editstatid" name="editstatid" class="form-control" type="hidden" />
                            <select value="" id="editstatuschange" name="editstatuschange" class="form-control">
                                <option value="0">Pending</option>
                                <option value="1">Approved</option>
                                <option value="2">Decline</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success submit-btn">Yes Change It!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Reimbursement Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    // Select branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select department by api*/
    $("#addbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddepartmentid");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddesignationid");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddesignationid").change(function () {
        $.ajax({
            url: base_url + "viewUserDashboard",
            data: { designation_id: $("select[name=adddesignationid]").val() },
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adduserid");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose User</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
            });
        });
    });

    $(function () {
        filterEmployeeDash('all');
            $("#filter-form").submit();
        });
        function filterEmployeeDash() {
            $("#filter-form").off("submit");
            $("#filter-form").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#adduserid").val();
                if (user_id == null) {
                    user_id = 'all';
                }
            req = {};
            req.user_id = user_id;
            //Show Employee In table
            $.ajax({
                url: base_url + "viewReimbursementBranch",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                $("#employeetablebody").DataTable().clear().destroy();
                    $("#employeetablebody tbody").empty();
                    var table = document.getElementById("employeedatatable");
                    for (i in response.data) {

                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }

                    var startDate = new Date(response.data[i].start_date);
                    var dd = String(startDate.getDate()).padStart(2, "0");
                    var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                    var yyyy = startDate.getFullYear();

                    startDate = dd + "-" + mm + "-" + yyyy;
                    var endDate = new Date(response.data[i].end_date);
                    var dd = String(endDate.getDate()).padStart(2, "0");
                    var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                    var yyyy = endDate.getFullYear();

                    endDate = dd + "-" + mm + "-" + yyyy;
                    var arrivactiondate = "";
                    if (response.data[i].start_date == "0000-00-00") {
                        arrivactiondate = "Nil";
                    } else {
                        var start = startDate;
                        arrivactiondate = start;
                    }
                    var arrivalEnddate = "";
                    if (response.data[i].end_date == "0000-00-00") {
                        arrivalEnddate = "Nil";
                    } else {
                        var start = endDate;
                        arrivalEnddate = start;
                    }
                    var touramount = response.data[i].amount;
                    var hotelamount = response.data[i].hotel_amount;
                    var otheramount = response.data[i].employee_amount;
                    var foodamount = response.data[i].food_amount;
                    var totalamount = Number(touramount) + Number(hotelamount) + Number(otheramount) + Number(foodamount);

                    var tr = document.createElement("tr");
                    tr.innerHTML =
                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage + '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' + response.data[i].user_code + '" title="">' + response.data[i].emp_id + ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +
                        
                        '<td class="text-center">' + response.data[i].branch_name + '</td>' +

                        '<td class="text-center">' + response.data[i].starting_city + '</td>' +

                        '<td class="text-center">' + response.data[i].end_city + '</td>' +

                        '<td class="text-center">' + arrivactiondate + '</td>' +

                        '<td class="text-center">' + arrivalEnddate + '</td>' +

                        '<td class="text-center">₹ ' + totalamount + '</td>' +

                        '<td class="text-center"> <button type="button" data-toggle="modal" data-target="#reimbursement_status" aria-expanded="false" class="btn btn-info edit_data" id="' + response.data[i].id + '" title="Change Status" ><i class="fa fa-eye"></i></button>  <a href="<?php echo base_url(); ?>ReimbursementView/' + response.data[i].id + '" class="btn btn-info"><i class="fa fa-edit"></i></a> </td>';
                    table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#employeetablebody").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Pending Reimbursement Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Pending Reimbursement Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                            }
                        }
                    ]
                });
            });
        });
    }

    //Add Pending Reimbursement Filter Validation
    $(document).ready(function () {
        $("#filteremployeebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }
            if ($("#adddepartmentid").val() == null) {
                $("#adddepartmentid").focus();
                $("#adddepartmentidvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#adddepartmentidvalidate").text("");
            }
            if ($("#adddesignationid").val() == null) {
                $("#adddesignationid").focus();
                $("#designationidvalidate").text("Please select a designation name.");
                errorCount++;
            } else {
                $("#designationidvalidate").text("");
            }
            if ($("#adduserid").val() == null) {
                $("#adduserid").focus();
                $("#uservalidate").text("Please select a user name.");
                errorCount++;
            } else {
                $("#uservalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    // Edit StatusReimbursement
    $(document).on("click", ".edit_data", function () {
        var reimbursement_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewReimbursementeditBranch",
            method: "POST",
            data: {
                reimbursement_id: reimbursement_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editstatid").val(response["data"][0]["id"]);
                $("#editstatuschange").val(response["data"][0]["status"]);
                $("#reimbursement_status").modal("show");
            },
        });
    });

    // Edit Reimbursement Active form
    $("#editstatus").submit(function (e) {
        var formData = {
            reimbursement_id: $("input[name=editstatid]").val(),
            status: $("select[name=editstatuschange]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editchangeStatus",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>ReimbursementList";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editstatus [type=submit]").attr("disabled", false);
            }
        });
    });
</script>

<script type="text/javascript">
    // Select Branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addbranchid1");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select Department by api*/
    $("#addbranchid1").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid1]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#adddepartmentid1");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid1").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid1]").val() },
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#designation_id1");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Select User by api*/
    $("#designation_id1").change(function () {
        $.ajax({
            url: base_url + "viewUserDashboard",
            data: { designation_id: $("select[name=designation_id1]").val() },
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#user_id1");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose User</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
            });
        });
    });

    $(function () {
        filterEmployeeDash1('all');
        $("#filter-form1").submit();
    });
    function filterEmployeeDash1() {
        $("#filter-form1").off("submit");
        $("#filter-form1").on("submit", function (e) {
            e.preventDefault();
            user_id = $("#user_id1").val();
            if (user_id == null) {
                user_id = 'all';
            }
            req = {};
            req.user_id = user_id;
            //Show Employee In table
            $.ajax({
                url: base_url + "viewReimbursementApproved",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                $("#employeetablebody1").DataTable().clear().destroy();
                $("#employeetablebody1 tbody").empty();
                var table = document.getElementById("employeedatatable1");
                for (i in response.data) {
                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }

                    var startDate = new Date(response.data[i].start_date);
                    var dd = String(startDate.getDate()).padStart(2, "0");
                    var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                    var yyyy = startDate.getFullYear();

                    startDate = dd + "-" + mm + "-" + yyyy;
                    var endDate = new Date(response.data[i].end_date);
                    var dd = String(endDate.getDate()).padStart(2, "0");
                    var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                    var yyyy = endDate.getFullYear();

                    endDate = dd + "-" + mm + "-" + yyyy;
                    var arrivactiondate = "";
                    if (response.data[i].start_date == "0000-00-00") {
                        arrivactiondate = "Nil";
                    } else {
                        var start = startDate;
                        arrivactiondate = start;
                    }
                    var arrivalEnddate = "";
                    if (response.data[i].end_date == "0000-00-00") {
                        arrivalEnddate = "Nil";
                    } else {
                        var start = endDate;
                        arrivalEnddate = start;
                    }

                    var touramount =  response.data[i].amount ;
                    var hotelamount =  response.data[i].hotel_amount ;
                    var otheramount =  response.data[i].employee_amount ;
                    var foodamount =  response.data[i].food_amount ;
                    var totalamount = Number(touramount) + Number(hotelamount) +Number(otheramount) + Number(foodamount);
               

                   

                    var adminamount = response.data[i].fixed_amount;
                    var adminhotelamount = response.data[i].clear_amount;
                    var adminotheramount = response.data[i].remarked_amount;
                    var adminfoodamount = response.data[i].admin_amount;
                    var totalamountadmin = Number(adminamount) + Number(adminhotelamount) + Number(adminotheramount) + Number(adminfoodamount);
                    var tr = document.createElement("tr");
                    tr.innerHTML =
                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage + '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' + response.data[i].user_code + '" title="">' + response.data[i].emp_id + ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +
                        
                        '<td class="text-center">' + response.data[i].branch_name + '</td>' +

                        '<td class="text-center">' + response.data[i].starting_city + '</td>' +

                        '<td class="text-center">' + response.data[i].end_city + '</td>' +
                        
                        '<td class="text-center">' + arrivactiondate + '</td>' +

                        '<td class="text-center">' + arrivalEnddate + '</td>' +

                        '<td class="text-center">₹ ' + totalamount + '</td>' +

                        '<td class="text-center">₹ ' + totalamountadmin + '</td>' +

                        '<td class="text-center"> <a href="<?php echo base_url(); ?>ReimbursementView/' + response.data[i].id + '" class="btn btn-info"><i class="fa fa-eye"></i></a> </td>';
                    table.appendChild(tr);
                }

                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#employeetablebody1").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Approved Reimbursement Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Approved Reimbursement Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                            }
                        }
                    ]
                });
            });
        });
    }

    //Add Employee Validation
    $(document).ready(function () {
        $("#filteremployeebutton1").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid1").val() == null) {
                $("#addbranchid1").focus();
                $("#addbranchvalidate1").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate1").text("");
            }
            if ($("#adddepartmentid1").val() == null) {
                $("#adddepartmentid1").focus();
                $("#adddepartmentidvalidate1").text("Please select a department name.");
                errorCount++;
            } else {
                $("#adddepartmentidvalidate1").text("");
            }
            if ($("#designation_id1").val() == null) {
                $("#designation_id1").focus();
                $("#designationidvalidate1").text("Please select a designation name.");
                errorCount++;
            } else {
                $("#designationidvalidate1").text("");
            }
            if ($("#user_id1").val() == null) {
                $("#user_id1").focus();
                $("#uservalidate1").text("Please select a user name.");
                errorCount++;
            } else {
                $("#uservalidate1").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>

<script type="text/javascript">
    // Select branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid2");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select department by api*/
    $("#addbranchid2").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid2]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddepartmentid2");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid2").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid2]").val() },
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#designation_id2");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Select User by api*/
    $("#designation_id2").change(function () {
        $.ajax({
            url: base_url + "viewUserDashboard",
            data: { designation_id: $("select[name=designation_id2]").val() },
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#user_id2");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose User</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
            });
        });
    });

    $(function () {
        filterEmployeeDash2('all');
            $("#filter-form2").submit();
        });
        function filterEmployeeDash2() {
            $("#filter-form2").off("submit");
            $("#filter-form2").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#user_id2").val();
                if (user_id == null) {
                    user_id = 'all';
                }
            req = {};
            req.user_id = user_id;
        //Show Employee In table
        $.ajax({
            url: base_url + "viewReimbursementDeclined",
            data: req,
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            $("#employeetablebody2").DataTable().clear().destroy();
                $("#employeetablebody2 tbody").empty();
            var table = document.getElementById("employeedatatable2");
            for (i in response.data) {

                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }

                var startDate = new Date(response.data[i].start_date);
                var dd = String(startDate.getDate()).padStart(2, "0");
                var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = startDate.getFullYear();

                startDate = dd + "-" + mm + "-" + yyyy;
                var endDate = new Date(response.data[i].end_date);
                var dd = String(endDate.getDate()).padStart(2, "0");
                var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = endDate.getFullYear();

                endDate = dd + "-" + mm + "-" + yyyy;
                var arrivactiondate = "";
                if (response.data[i].start_date == "0000-00-00") {
                    arrivactiondate = "Nil";
                } else {
                    var start = startDate;
                    arrivactiondate = start;
                }
                var arrivalEnddate = "";
                if (response.data[i].end_date == "0000-00-00") {
                    arrivalEnddate = "Nil";
                } else {
                    var start = endDate;
                    arrivalEnddate = start;
                }
                var touramount =  response.data[i].amount ;
                var hotelamount =  response.data[i].hotel_amount ;
                var otheramount =  response.data[i].employee_amount ;
                var foodamount =  response.data[i].food_amount ;
                var totalamount = Number(touramount) + Number(hotelamount) +Number(otheramount) + Number(foodamount);
           

               
                var adminamount = response.data[i].fixed_amount;
                var adminhotelamount = response.data[i].clear_amount;
                var adminotheramount = response.data[i].remarked_amount;
                var adminfoodamount = response.data[i].admin_amount;
                var totalamountadmin = Number(adminamount) + Number(adminhotelamount) + Number(adminotheramount) + Number(adminfoodamount);
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage + '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' + response.data[i].user_code + '" title=""> ' + response.data[i].emp_id + ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +
                    
                    '<td class="text-center">' + response.data[i].branch_name + '</td>' +

                    '<td class="text-center">' + response.data[i].starting_city + '</td>' +

                    '<td class="text-center">' + response.data[i].end_city + '</td>' +

                    '<td class="text-center">' + arrivactiondate + '</td>' +

                    '<td class="text-center">' + arrivalEnddate + '</td>' +

                    '<td class="text-center">₹ ' + totalamount + '</td>' +

                    '<td class="text-center">₹ ' + totalamountadmin + '</td>' +

                    '<td class="text-center"> <a href="<?php echo base_url(); ?>ReimbursementView/' + response.data[i].id + '" class="btn btn-info"><i class="fa fa-edit"></i></a> </td>';
                table.appendChild(tr);
            }

            var currentDate = new Date()
            var day = currentDate.getDate()
            var month = currentDate.getMonth() + 1
            var year = currentDate.getFullYear()
            var d = day + "-" + month + "-" + year;
            $("#employeetablebody2").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: d+ ' Declined Reimbursement Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: d+ ' Declined Reimbursement Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                        }
                    }
                ]
            });
        });
    });
}

//Add Employee Validation
    $(document).ready(function () {
        $("#filteremployeebutton2").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid2").val() == null) {
                $("#addbranchid2").focus();
                $("#addbranchvalidate2").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate2").text("");
            }

            if ($("#adddepartmentid2").val() == null) {
                $("#adddepartmentid2").focus();
                $("#adddepartmentidvalidate2").text("Please select a department name.");
                errorCount++;
            } else {
                $("#adddepartmentidvalidate2").text("");
            }

            if ($("#designation_id2").val() == null) {
                $("#designation_id2").focus();
                $("#designationidvalidate2").text("Please select a designation name.");
                errorCount++;
            } else {
                $("#designationidvalidate2").text("");
            }


            if (errorCount > 0) {
                return false;
            }
        });
    });

     
</script>