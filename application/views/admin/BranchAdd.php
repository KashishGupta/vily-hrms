<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Branch">Branch</a></li>
                <li class="breadcrumb-item active">Add Branch</li>
            </ul>
        </div>
    </div>
</div>

<div class="message_succes text-succes" id="message"></div>

<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Branch</strong></h3>
            </div>
            <div class="card-body">
                <!-- Add Branch Form -->
                <form id="addbranch" method="POST" action="#">
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label>Branch Name <span class="text-danger">*</span></label>
                            <br />
                            <span id="addbranchnamevalidate" class="text-danger change-pos"></span>
                            <input type="text" name="addbranchname" id="addbranchname" class="form-control" placeholder="Please enter branch name." autocomplete="off" />
                        </div>

                        <div class="form-group col-md-6">
                            <label>Branch Location <span class="text-danger">*</span></label>
                            <br />
                            <span id="addbranchlocationvalidate" class="text-danger change-pos"></span>
                            <input type="text" name="addbranchlocation" id="addbranchlocation" class="form-control" placeholder="Please enter branch location." autocomplete="off" />
                        </div>

                        <div class="form-group col-md-6">
                            <label>Branch Latitude <span class="text-danger"></span></label>
                            <br />
                            <span id="addbranchlatitudevalidate" class="text-danger change-pos"></span>
                            <input type="text" name="lat" id="lat" class="form-control" placeholder="Please enter branch latitude." readonly="" />
                        </div>

                        <div class="form-group col-md-6">
                            <label>Branch Longitude <span class="text-danger"></span></label>
                            <br />
                            <span id="addbranchlongitudevalidate" class="text-danger change-pos"></span>
                            <input type="text" name="lng" id="lng" class="form-control" placeholder="Please enter branch longitude." readonly="" />
                        </div>

                    </div>
                    <span id="message" class="text-danger"></span>
                    <div class="submit-section pull-right">
                        <button id="addbranchbutton" class="btn btn-success submit-btn">Submit</button>
                        <button type="reset" class="btn btn-secondary" id="test" data-dismiss="modal">Reset</button>
                    </div>
                </form>
                <!-- /Add Branch Form -->
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <div id="mapCanvas"></div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6SeEWVn2z0Phkp5gnlxAe-wSs2lzvAck&sensor=false"></script>
<script>
    //for remove validation and empty input field
    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#addbranchnamevalidate").text("");
            jQuery("#addbranchlocationvalidate").text("");
            jQuery("#addbranchlatitudevalidate").text("");
            jQuery("#addbranchlongitudevalidate").text("");
            jQuery("#message").html("");
        });
    });

    //Add Branch Form
    $("#addbranch").submit(function (e) {
        var formData = {
            branch_name: $("input[name=addbranchname]").val(),
            branch_location: $("input[name=addbranchlocation]").val(),
            branch_lat: $("input[name=lat]").val(),
            branch_long: $("input[name=lng]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addBranch",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Branch";
                }, 1000);
            } 
            else {
                toastr.error(falsedata);
                $("#addbranch [type=submit]").attr("disabled", false);
            }
        });
    });

    //Add Branch Validation Form
    $(document).ready(function () {
        $("#addbranchbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchname").val().trim() == "") {
                $("#addbranchname").focus();
                $("#addbranchnamevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addbranchnamevalidate").text("");
            }
            if ($("#addbranchlocation").val().trim() == "") {
                $("#addbranchlocation").focus();
                $("#addbranchlocationvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addbranchlocationvalidate").text("");
            }
            if ($("#lat").val().trim() == "") {
                $("#lat").focus();
                $("#addbranchlatitudevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addbranchlatitudevalidate").text("");
            }
            if ($("#lng").val().trim() == "") {
                $("#lng").focus();
                $("#addbranchlongitudevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addbranchlongitudevalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    // Branch Fetch Latlong
    $(document).ready(function () {
        $("#addbranchlocation").keyup(function () {
            var address = $(this).val();
            var url = "https://maps.googleapis.com/maps/api/geocode/json";
            var key = "AIzaSyB6SeEWVn2z0Phkp5gnlxAe-wSs2lzvAck"; //  real
            jQuery.ajax({
                type: "GET",
                dataType: "json",
                url: url + "?address=" + address + "&key=" + key,
                success: function (data) {
                    if (data.results.length) {
                        jQuery("#lat").val(data.results[0].geometry.location.lat);
                        jQuery("#lng").val(data.results[0].geometry.location.lng);
                    } else {
                        jQuery("#lat").val("");
                        jQuery("#lng").val("");
                    }
                },
            });
        });
    });

    var position = [28.153381669504025, 77.32297897338867];
    function initialize() {
        var latlng = new google.maps.LatLng(position[0], position[1]);
        var myOptions = {
            zoom: 16,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        map = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);
        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Latitude:" + position[0] + " | Longitude:" + position[1],
        });
        google.maps.event.addListener(map, "click", function (event) {
            jQuery("#lat").val(event.latLng.lat());
            jQuery("#lng").val(event.latLng.lng());
        });
        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(map, "click", function (event) {
            geocoder.geocode(
                {
                    latLng: event.latLng,
                },
                function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            jQuery("#addbranchlocation").val(results[0].formatted_address);
                            //alert(results[0].formatted_address);
                        }
                    }
                }
            );
        });
        google.maps.event.addListener(map, "click", function (event) {
            var result = [event.latLng.lat(), event.latLng.lng()];
            transition(result);
        });
    }

    //Load google map
    google.maps.event.addDomListener(window, "load", initialize);
    var numDeltas = 100;
    var delay = 10; //milliseconds
    var i = 0;
    var deltaLat;
    var deltaLng;
    function transition(result) {
        i = 0;
        deltaLat = (result[0] - position[0]) / numDeltas;
        deltaLng = (result[1] - position[1]) / numDeltas;
        moveMarker();
    }
    function moveMarker() {
        position[0] += deltaLat;
        position[1] += deltaLng;
        var latlng = new google.maps.LatLng(position[0], position[1]);
        marker.setTitle("Latitude:" + position[0] + " | Longitude:" + position[1]);
        marker.setPosition(latlng);
        if (i != numDeltas) {
            i++;
            setTimeout(moveMarker, delay);
        }
    }

    $("#addbranchlocation").keyup(function () {
        var address = $(this).val();
        var infowindow = new google.maps.InfoWindow({
            size: new google.maps.Size(150, 50),
        });
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode(
            {
                address: address,
            },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var position = [results[0].geometry.location.lat(), results[0].geometry.location.lng()];
                    var latlng = new google.maps.LatLng(position[0], position[1]);
                    map.setCenter(latlng);
                    if (marker) {
                        marker.setMap(null);
                        if (infowindow) infowindow.close();
                    }
                    marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        position: latlng,
                    });
                    google.maps.event.addListener(marker, "dragend", function () {
                        geocodePosition(marker.getPosition());
                    });
                    google.maps.event.addListener(marker, "click", function () {
                        if (marker.formatted_address) {
                            infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
                        } else {
                            infowindow.setContent(address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
                        }
                        infowindow.open(map, marker);
                    });
                    google.maps.event.trigger(marker, "click");
                } else {
                   // alert("Geocode was not successful for the following reason: " + status);
                }
            }
        );
    });
</script>
<style>
    #mapCanvas {
        width: 100%;
        height: 400px;
    }
</style>
