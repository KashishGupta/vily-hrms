<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Role</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Designation</strong></h3>
            </div>
            <div class="card-body">
                <form id="filter-form" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-md-4 col-sm-12">
                            <label>Branch<span class="text-danger"> *</span></label><br />
                            <span id="filterbranchidvalidate" class="text-danger change-pos"></span>
                            <select name="filterbranchid" id="filterbranchid" class="form-control custom-select"> </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label>Department<span class="text-danger"> *</span></label><br />
                            <span id="filterdepartmentidvalidate" class="text-danger change-pos"></span>
                            <select name="filterdepartmentid" id="filterdepartmentid" class="form-control custom-select"> </select>
                        </div>
                        <div class="form-group col-md-3 col-sm-12 button-open">
                            <button id="filterdesignationbutton" type="submit" class="btn btn-success submit-btn">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="designationExample">
                    <thead>
                        <tr>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Department</th>
                            <th class="text-center">Designation</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="designationtable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#adddesignationname").val("");
            $("#adddepartmenttest option:eq(0)").prop("selected", true);
            $("#addbranchid option:eq(0)").prop("selected", true);
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addbranchvalidate").text("");
            jQuery("#adddepartmentvalidate").text("");
            jQuery("#adddesignationvalidate").text("");
            jQuery("#filterbranchidvalidate").text("");
            jQuery("#filterdepartmentidvalidate").text("");
            jQuery("#editbranchvalidate").text("");
            jQuery("#editdepartmentvalidate").text("");
            jQuery("#editdesignationvalidate").text("");
        });
    });

    $(function () {
        filterDepartment("all");
        $("#filter-form").submit();
    });

    function filterDepartment() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            department_id = $("#filterdepartmentid").val();
            //alert(status);
            if (department_id == null) {
                department_id = "all";
            }
            req = {};
            req.department_id = department_id;

            //Show Designation list
            $.ajax({
                url: base_url + "viewDesignation",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                //var j =1;
                $("#designationExample").DataTable().clear().destroy();
                var table = document.getElementById("designationtable");
                for (i in response.data) {
                    var tr = document.createElement("tr");
                    tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +

                        '<td class="text-center">' +
                        response.data[i].branch_name +
                        "</td>" +

                        '<td class="text-center">' +
                        response.data[i].department_name +
                        "</td>" +

                        '<td class="text-center">' +
                        response.data[i].designation_name +
                        "</td>" +

                        
                        '<td class="text-right"><a class="btn btn-success submit-btn" href="<?php echo base_url()?>UpdatePermission/' +
                        response.data[i].designation_id +
                        '">Update Permission</a></td>';
                    table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#designationExample").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Role Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Role Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }
                        }
                    ]
                });
            });
        });
    }
    //Select branch For Filter
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#filterbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select Department by select branch For Add Designation Filter
    $("#filterbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=filterbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#filterdepartmentid");

            dropdown.empty();

            dropdown.append('<option selected="true"  required="true">Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    //Filter Designation Validation
    $(document).ready(function () {
        $("#filterdesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#filterbranchid").val() == null) {
                $("#filterbranchid").focus();
                $("#filterbranchidvalidate").text("Please select a branch.");
                errorCount++;
            } else {
                $("#filterbranchidvalidate").text("");
            }
            if ($("#filterdepartmentid").val() == null) {
                $("#filterdepartmentid").focus();
                $("#filterdepartmentidvalidate").text("Please select a department.");
                errorCount++;
            } else {
                $("#filterdepartmentidvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
