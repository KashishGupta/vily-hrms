<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Payroll Salary</li>
            </ul>
           
        </div>

        <!-- Search Payroll Salary -->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Payroll Slip</strong></h3>
            </div>
            <div class="card-body">
                <form id="addPayrolls" methos="POST">
                <div class="row clearfix">
                        
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Month<span class="text-danger"> *</span></label><br />
                            <span id="adddepartmentvalidate" class="checkfield change-pos" style="color: red;"></span>
                            <select class="custom-select form-control" name="findmonth" id="findmonth" required="">
                            <option value=" ">Select Month</option>
                                                <?php  for($i = 1; $i <= 12; $i++){  ?>
                                               
                                                  <option value="<?= $i ?>"><?= date('M', strtotime('2020-'.$i.'-01')) ?></option>
                                                <?php }  ?>
                                                </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Year<span class="text-danger"> *</span></label><br />
                            <span id="adddesignationvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="findyear" id="findyear"  required="">
                            <option value=" ">Select Year</option>
                                                <?php   $months = array("2020", "2021", "2022","2022","2023","2024","2025","2026");
                                                  foreach ($months as $month) {
                                                  
                                                   echo "<option value=\"" . $month . "\">" . $month . "</option>";
                                                  }
                                                 ?>
                                                </select>   
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12 button-open">
                            <button id="adddesignationbutton" class="btn btn-success submit-btn">Submit</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Search Payroll Salary -->

       

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    $(document).ready(function () {
        $("#adddesignationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
          

            if ($("#findmonth").val() == 0) {
                $("#findmonth").focus();
                $("#adddepartmentvalidate").text("Please select a month.");
                errorCount++;
            } else {
                $("#adddepartmentvalidate").text("");
            }
            if ($("#findyear").val() == 0) {
                $("#findyear").focus();
                $("#adddesignationvalidate").text("Please select a year.");
                errorCount++;
            } else {
                $("#adddesignationvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
      //Select Purchase By
      $.ajax({
        url: base_url + "viewpayrollCreateSearch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
       // var jsonData = response["data"]["last_name"];
        let dropdown = $("#finduserId");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Employee Name</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
        });
    });
    //add Payroll form
    $("#addPayrolls").submit(function (e) {
      
    
       
            var formData = {
                month: $("select[name=findmonth]").val(),
                year: $("select[name=findyear]").val(),
            };
           
           
            e.preventDefault();
           
            $.ajax({
                type: "POST",
                url: base_url + "checkPayrollAvailability",
                data: formData,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                console.log(response);
               var jsonDa = response;
              var arrUser = $("select[name=finduserId]").val();
              var arrMonth = $("select[name=findmonth]").val();
              var arrYear = $("select[name=findyear]").val();
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                          setTimeout(function(){window.location ="<?php echo base_url()?>PayrollSalaryAdd/"+arrMonth+ "/"+arrYear},1000);
                } else {
                    toastr.error(falsedata);
                    $('#addPayrolls [type=submit]').attr('disabled',false);
                }
            });
        });
</script>
