<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mt-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>ProjectDashboard">Projects</a></li>
                <li class="breadcrumb-item active">Project Details</li>
            </ul>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="job-info job-widget">
                    <h3 class="job-title" id="jobname"></h3>
                    <ul class="job-post-det">
                        <li><i class="fa fa-calendar"></i> Created Date: <span class="text-blue" id="startdate"></span></li>
                        <li><i class="fa fa-calendar"></i> Deadline Date: <span class="text-danger" id="enddate"></span></li>
                    </ul>
                    <ul class="job-post-det mt-2">
                        <li> Project Status: <span  id="Projectstatus"></span></li>
                    </ul>
                </div>
                <div class="job-content job-widget">
                    <div class="job-desc-title"><h4>Project Team</h4></div>
                    <div class="job-description">
                        <p id="projectTeam"></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 kt-widget__items"> 
                <div class="job-det-info job-widget ">
                <div class="info-list Kt-projectFile">
                        
                    </div>
                    <div class="info-list">
                        <span>
                            <i class="fa fa-chart-bar"></i>
                        </span>
                        <h5>Branch</h5> 
                        <p id="branchname"></p>
                    </div> 
                    <div class="info-list">
                        <span>
                            <i class="fa fa-chart-bar"></i>
                        </span>
                        <h5>Department</h5> 
                        <p id="departmentname"></p>
                    </div> 
                    <div class="info-list">
                        <span><i class="fa fa-clipboard-check"></i></span>
                        <h5>Client Name</h5>
                        <p id="clientName"></p>
                    </div> 
                    <div class="info-list">
                        <span><i class="fa fa-university"></i></span>
                        <h5>Creater Name</h5><p id="CreaterName"></p> 
                    </div> 
                    <div class="info-list">
                        <span><i class="fa fa-university"></i></span>
                        <h5>Project lead</h5><p id="projectLead"></p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

	// Show Job list
    var project_id = <?php echo $projectid; ?>;
    $.ajax({
        url: base_url+"viewProject",
        data: {project_id: project_id },
        type: "POST",
        dataType    : 'json', // what type of data do we expect back from the server
        beforeSend: function(xhr){
           xhr.setRequestHeader('Token', localStorage.token);
        },
    })
    .done(function(response)
    {
        var Html ='<a class="btn job-btn" download="file.pdf" href="' + response.data[0].project_file +'"><i class="fa fa-download mr-2"></i>Project Details</a>';
        $(".Kt-projectFile").append(Html);
        var startDate = new Date(response.data[0].start_date);
        var dd = String(startDate.getDate()).padStart(2, "0");
        var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
        var yyyy = startDate.getFullYear();
        startDate = dd + "-" + mm + "-" + yyyy;

        var endDate = new Date(response.data[0].end_date);
        var dd = String(endDate.getDate()).padStart(2, "0");
        var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
        var yyyy = endDate.getFullYear();
        endDate = dd + "-" + mm + "-" + yyyy;
        var isstatus = "";
                    if (response.data[0].status == 0) {
                        isstatus = "Pending";
                    }
                    if (response.data[0].status == 1) {
                        isstatus = "Ongoing";
                    }
                    if (response.data[0 ].status == 2) {
                        isstatus = "Completed";
                    }
        
            $("#Projectstatus").html(isstatus);
        
        $("#projectTeam").html(response.data[0].project_team_name);
        $("#projectLead").html(response.data[0].project_leader_id);
        $("#CreaterName").html(response.data[0].creator_name);
        $("#clientName").html(response.data[0].client_name);
        $("#jobname").html(response.data[0].project_name);
        $("#startdate").html(startDate);
        $("#enddate").html(endDate);
        $("#branchname").html(response.data[0].branch_name);
        $("#departmentname").html(response.data[0].department_name);
        
        localStorage.removeItem('projectid');
    });

    //Show Employee
        /*var job_id = <?php echo $jobId; ?>;
        $.ajax({
                url: base_url + "viewJobPublic",
                data: {job_id : job_id },
                type: "POST",
                dataType: 'json',
                encode: true,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response) {
                for (i in response.data) {
                    var html =' <div class="job-det-info job-widget"><a class="btn job-btn" href="<?php echo base_url() ?>Hrms/JobApply/' + response.data[i].job_id + ' ">Referral Now</a><div class="info-list"><span><i class="fa fa-chart-bar"></i></span><h5>Job Type</h5> <p>' + response.data[i].job_type + '</p></div> <div class="info-list"><span><i class="fa fa-clipboard-check"></i></span><h5>Vacancy</h5><p>' + response.data[i].no_of_post + '</p> </div> <div class="info-list"><span><i class="fa fa-university"></i></span></span><h5>Key Skills </h5><p>' + response.data[i].key_skill + '</p> </div> <div class="info-list"><span><i class="fa fa-map-signs"></i></span><h5>Location</h5><p>' + response.data[i].branch_name + '</p> </div> </div>';
                    $('.kt-widget__items').append(html);
                }
            });*/
</script>
