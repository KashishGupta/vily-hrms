<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Yellow Page</li>
            </ul>
            <div class="header-action mt-3">
                <a href="<?php echo base_url(); ?>YellowPageList" class="btn btn-info grid-system"><i class="fe fe-list"></i></a>
            </div>
        </div>
        <!-- Search Designation -->
       
        <div class="row clearfix kt-widget__items  mt-3"></div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
   
         
    $.ajax({
        url: base_url + "viewYellowPageUsers",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
 var html ='<div class="col-md-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Yellow Pages</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
                var html = //Show Employee Card View Section HTML
                    '<div class="col-md-6 col-sm-6 col-xl-4"><div class="card"><div class="card-body"><div class="profile-img"><img class="img-thumbnail w100 h100" src="' +
                    UserImage +
                    '" alt=""></div><h4 class="user-name text-ellipsis"><a href="YellowPageView/' +
                    response.data[i].user_code +
                    '">' +
                    response.data[i].first_name +
                    '</a></h4><div class="small text-muted"><span>' +
                    response.data[i].emp_id +
                    "</span> - " +
                    response.data[i].designation_name +
                    "</div><span  class='text-ellipsis'>" +
                    response.data[i].email +
                    "</span><span>" +
                    response.data[i].phone +
                    "</span></div></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });

     //Select User For Filter
     $.ajax({
        url: base_url + "viewDepartmentSelect",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#filterDepartment");

        dropdown.empty();

        dropdown.append('<option value="01">Choose All Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });

     // Select Department by select branch For Add Designation Filter
     $("#filterDepartment").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=filterDepartment]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#filterdesignation");

            dropdown.empty();

            dropdown.append('<option value="01"  required="true">Choose All Designation</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });
    // Select Department by select branch For Add Designation Filter
    $("#filterdesignation").change(function () {
        $.ajax({
            url: base_url + "viewDojDesignation",
            data: { designation_id: $("select[name=filterdesignation]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#filterDesgnationid");

            dropdown.empty();

            dropdown.append('<option value="01"  required="true">Choose All Designation</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.user_id).text(entry.first_name));
            });
        });
    });

</script>
