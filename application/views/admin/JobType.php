<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Job Type</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Job Type</strong></h3>
            </div>
            <div class="card-body">
                <form id="addjobtype" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Job Type<span class="text-danger"> *</span></label><br />
                            <span id="addjobtypenamevalidate" class="text-danger change-pos"></span>
                            <input name="addjobtypename" id="addjobtypename" class="form-control" type="text" placeholder="Please enter job type" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Job Type Color<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addstatuscolorvalidate"></span>
                            <input name="addstatuscolor" id="addstatuscolor" class="form-control" type="color" placeholder="e.g. black or #000;" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Job Type Description<span class="text-danger"> </span></label><br />
                            <span id="addjobtypedescriptionvalidate" class="text-danger change-pos"></span>
                            <textarea name="addjobtypedescription" id="addjobtypedescription" class="form-control" type="text" placeholder="Please enter job type description" autocomplete="off"></textarea>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                            <button id="addjobtypebutton" class="btn btn-success submit-btn">Submit</button> 
                            <button type="reset" class="btn btn-secondary" id="test">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Job Type Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="jobtype">
                    <thead>
                        <tr>
                            <th class="text-center">Job Type</th>
                            <th class="text-center">Job Type Color</th>
                            <th class="text-center">Job Type Description</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="jobtypetable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Edit Job Type Modal -->
<div id="edit_jobtype" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Job Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editjobtype" method="POST">
                    <div class="form-group col-md-12">
                        <label>Job Type<span class="text-danger">*</span></label>
                        <br />
                        <span id="editjobtypenamevalidate" class="text-danger change-pos"></span>
                        <input value="" name="editjobtypename" id="editjobtypename" class="form-control" type="text" placeholder="Please enter job type" required="" autocomplete="off" />
                        <input value="" id="editjobtypeid" name="editjobtypeid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Job Type Color<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="editstatuscolorvalidate"></span>
                        <input name="editstatuscolor" id="editstatuscolor" class="form-control" type="color" placeholder="e.g. black or #000;" autocomplete="off" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Job Type Description<span class="text-danger"> </span></label>
                        <br />
                        <span id="editjobtypedescriptionvalidate" class="text-danger change-pos"></span>
                        <textarea value="" name="editjobtypedescription" id="editjobtypedescription" class="form-control" type="text" placeholder="Please enter job description" autocomplete="off"></textarea>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editjobtypebutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Job Type Modal -->



<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#addjobtypename").val("");
            $("textarea#editstatuscolorvalidate").val("");
            $("span#addjobtypenamevalidate").prop("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#addjobtypenamevalidate").text("");
            jQuery("#addstatuscolorvalidate").text("");

            jQuery("#editjobtypenamevalidate").text("");
            jQuery("#editstatuscolorvalidate").text("");
        });
    });

    // SHow Job Type
    $.ajax({
        url: base_url + "viewJobType",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        var table = document.getElementById("jobtypetable");
        for (i in response.data) {

            var testcheck = "";
            if (response.data[i].status == 0) {
                testcheck = "checked";
            }
            if (response.data[i].status == 1) {
                testcheck = "";
            }

            var tr = document.createElement("tr");
            tr.innerHTML =
                '<td class="text-center">' +
                response.data[i].job_type +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].type_color +
                "</td>" +
                '<td class="text-center" style="white-space: break-spaces;">' +
                response.data[i].jobtype_description +
                "</td>" +
                '<td class="text-center"> <div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox" '+testcheck+' id="'+response.data[i].id+'" value="'+ response.data[i].status +'" onclick="getstatus('+response.data[i].id+')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>' +

                '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_jobtype" aria-expanded="false" class="btn btn-info edit_data" title="Edit Job Type" id="' +
                response.data[i].id +
                '"><i class="fa fa-edit"></i></button> </td>';
                /*<button type="button" data-toggle="modal" data-target="#delete_jobtype" aria-expanded="false" class="btn btn-danger delete_data" id="' +
                response.data[i].id +
                '" title="Delete Job Type" ><i class="fa fa-trash-alt"></i></button>*/
            table.appendChild(tr);
        }
        $("#jobtype").DataTable();
    });

    // Add Job Type
    $("#addjobtype").submit(function (e) {
        var formData = {
            job_type: $("input[name=addjobtypename]").val(),
            type_color: $("input[name=addstatuscolor]").val(),
            jobtype_description: $("textarea[name=addjobtypedescription]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addJobType",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })

        // using the done promise callback
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>JobType";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addjobtype [type=submit]").attr("disabled", false);
            }
        });
    });

    function getstatus(id) {
        var job_type_id = id;
        var status = $("#" + id).val();
        console.log(status);
        //alert(status);
        if (status == 0) {
            var status = 1;
            $.ajax({
                type: "POST",
                url: base_url + "editJobTypeActive",
                data: "job_type_id=" + job_type_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>JobType";
                }, 1000);
            });
        } else {
            var status = 0;
            $.ajax({
                type: "POST",
                url: base_url + "editJobTypeActive",
                data: "job_type_id=" + job_type_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>JobType";
                }, 1000);
            });
        }
    }

   
    // Edit  Job type Form Fill
    $(document).on("click", ".edit_data", function () {
        var job_type_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewJobType",
            method: "POST",
            data: {
                job_type_id: job_type_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editjobtypename").val(response["data"][0]["job_type"]);
                $("#editjobtypedescription").val(response["data"][0]["jobtype_description"]);
                $("#editstatuscolor").val(response["data"][0]["type_color"]);
                $("#editjobtypeid").val(response["data"][0]["id"]);
                $("#edit_jobtype").modal("show");
            },
        });
    });

    //edit Job type form
    $("#editjobtype").submit(function (e) {
        var formData = {
            job_type_id: $("input[name=editjobtypeid]").val(),
            jobtype_description: $("textarea[name=editjobtypedescription]").val(),
            job_type: $("input[name=editjobtypename]").val(),
            type_color: $("input[name=editstatuscolor]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editJobType",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>JobType";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editjobtype [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Job type
    /*$(document).on("click", ".delete_data", function () {
        var job_type_id = $(this).attr("id");
        $(".delete_jobtype_button").attr("id", job_type_id);
        $("#delete_jobtype").modal("show");
    });

    $(document).on("click", ".delete_jobtype_button", function () {
        var job_type_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteJobType",
            method: "POST",
            data: {
                job_type_id: job_type_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>JobType";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_jobtype_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });*/

    // Add Job type validation form
    $(document).ready(function () {
        $("#addjobtypebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addjobtypename").val().trim() == "") {
                $("#addjobtypename").focus();
                $("#addjobtypenamevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addjobtypenamevalidate").text("");
            }

            if ($("#addstatuscolor").val() == null) {
                $("#addstatuscolor").focus();
                $("#addstatuscolorvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addstatuscolorvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
    // Edit Job type validation form
    $(document).ready(function () {
        $("#editjobtypebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editjobtypename").val().trim() == "") {
                $("#editjobtypename").focus();
                $("#editjobtypenamevalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editjobtypenamevalidate").text("");
            }

            if ($("#editstatuscolor").val().trim() == "") {
                $("#editstatuscolor").focus();
                $("#editstatuscolorvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editstatuscolorvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
