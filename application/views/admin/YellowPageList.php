<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
				<li class="breadcrumb-item active">User</li>
			</ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Users Result</strong></h3>
                <div class="card-options">
                    <a href="<?php echo base_url(); ?>YellowPage" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                </div>
            </div>

            <div class="card-body">
                <form id="filter-form" method="POST" action="#">
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Branch <span class="text-danger"> *</span></label><br>
                            <span id="addbranchvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addbranchid" id="addbranchid" />
                                
                            </select>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Department<span class="text-danger"> *</span></label><br>
                            <span id="adddepartmentidvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/>
                                
                            </select>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Designation<span class="text-danger"> *</span></label><br>
                            <span id="designationidvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="user_id" id="user_id" />
                                
                            </select>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                            <button id="filteremployeebutton" class="btn btn-success submit-btn">Search</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style table-responsive table-responsive-xxl" id="usertable">
                    <thead>
                        <tr>
                            <th class="text-left">Employee Name</th>
                            <th class="text-center">Mobile</th>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Department</th>
                            <th class="text-center">Designation</th>
                        </tr>
                    </thead>
                    <tbody id="usertbody">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>            
</div>       

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    // Select branch by api*/
$.ajax({
    url: base_url + "viewactiveBranch",
    data: {},
    type: "POST",
    dataType: "json", // what type of data do we expect back from the server
    encode: true,
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },
})
.done(function (response) {
    let dropdown = $("#addbranchid");

    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Choose Branch</option>');
    dropdown.prop("selectedIndex", 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function (key, entry) {
        dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
    });
});

// Select department by api*/
$("#addbranchid").change(function () {
    $.ajax({
        url: base_url + "viewDepartment",
        data: { branch_id: $("select[name=addbranchid]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#adddepartmentid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });
});

// Select Designation by api*/
$("#adddepartmentid").change(function () {
    $.ajax({
        url: base_url + "viewDesignation",
        data: { department_id: $("select[name=adddepartmentid]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#user_id");

        dropdown.empty();

        dropdown.append('<option value="01">Choose All Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
        });
    });
});

$(function () {
        filterEmployeeDash(4);
            $("#filter-form").submit();
        });
        function filterEmployeeDash() {
            $("#filter-form").off("submit");
            $("#filter-form").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#user_id").val();
              /*alert(user_id);*/
                if (user_id == null) {
                    user_id = 01;
                }
            req = {};
            req.user_id = user_id;
        //Show Employee In table
        $.ajax({
            url: base_url + "viewYellowPage",
            data: req,
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            $("#usertable").DataTable().clear().destroy();
                $("#usertable tbody").empty();
            var table = document.getElementById("usertbody");
            for (i in response.data) {

                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }

                var tr = document.createElement("tr");
                tr.innerHTML = 

                '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                    UserImage +
                    '" alt=""></span><div class="ml-3">  <a href="YellowPageView/' +
                    response.data[i].user_code +
                    '" title="">' + response.data[i].emp_id + ' - ' + response.data[i].first_name + ' </a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +
                    
                '<td class="text-center">' + response.data[i].phone + '</td>' +

                '<td class="text-center">' + response.data[i].branch_name + '</td>' +

                '<td class="text-center">' + response.data[i].department_name + '</td>' +

                '<td class="text-center">' + response.data[i].designation_name + '</td>',
                table.appendChild(tr);
            }
            var currentDate = new Date()
            var day = currentDate.getDate()
            var month = currentDate.getMonth() + 1
            var year = currentDate.getFullYear()
            var d = day + "-" + month + "-" + year;
            $("#usertable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: d+ ' YellowPage Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: d+ ' YellowPage Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ]
                        }
                    }
                ]
            });
        });
    });
}

//Add Yellow Page Validation
    $(document).ready(function () {
        $("#filteremployeebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }

            if ($("#adddepartmentid").val() == null) {
                $("#adddepartmentid").focus();
                $("#adddepartmentidvalidate").text("Please select a department name.");
                errorCount++;
            } else {
                $("#adddepartmentidvalidate").text("");
            }

            if ($("#user_id").val() == null) {
                $("#user_id").focus();
                $("#designationidvalidate").text("Please select a designation name.");
                errorCount++;
            } else {
                $("#designationidvalidate").text("");
            }


            if (errorCount > 0) {
                return false;
            }
        });
    });

</script>
