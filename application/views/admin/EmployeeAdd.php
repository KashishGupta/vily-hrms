<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mt-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>EmployeeList">Employee</a></li>
                <li class="breadcrumb-item active">Add Employee</li>
            </ul>
        </div>

        <div class="card mt-3">
            <form role="form" method="POST" action="#" id="addemployee">
            	<div class="card-header">
                    <h3 class="card-title"><strong>Basic Details</strong></h3>
                </div>
	            <div class="card-body row">
                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Full Name <span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="firstnamevalid"></span>
                        <input type="text" class="form-control name-valid" name="firstname" id="firstname" placeholder="Please enter employee name." required="" />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Father's / Spouse / Guardian Name <span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="fathernamevalid"></span>
                        <input type="text" class="form-control name-valid" name="fathername" id="fathername" placeholder="Please enter father's name." required="" />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Contact No.<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="contactnovalid"></span>
                        <input type="tel" class="form-control" name="contactno" id="contactno" maxlength="10" onkeypress="return onlyNumberKey(event)" placeholder="Please enter contact no." required="" />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Personal Email ID<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="emailidvalid"></span>
                        <input type="email" class="form-control" name="emailid" id="emailid" placeholder="Please enter email id." required="" />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee DOB<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="dobvalid"></span>
                        <span class="text-danger change-pos" id="dobvalidstatus"></span>
                        <input type="date" class="form-control" name="dob" id="dob">
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Gender<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="gendervalid"></span>
                        <select class="custom-select form-control" name="gender" id="gender" required />
                        </select>
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Complete Address<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="addressvalid"></span>
                        <textarea class="form-control" name="address" cols="5" rows="4" id="address" placeholder="Please enter complete address." required=""></textarea>
                    </div> 

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Image<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="imagevalid"></span>
                        <input type="file" class="dropify form-control" name="image" id="image" onchange="load_file();" data-allowed-file-extensions="png jpg jpeg" data-max-file-size="2MB">
                        <input type="hidden" class="form-control text-ellipsis" id="12arrdata" />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Joining Date <span class="text-danger"> *</span></label>
                        <br>
                        <span class="text-danger change-pos" id="joiningdatevalid"></span>
                        <input class="form-control" type="date" name="joiningdate" id="joiningdate"/>
                    </div>
                    
                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Passport No. <span class="text-danger"> </span></label>
                        <br />
                        <span class="text-danger change-pos" id="passportnovalid"></span>
                        <input type="hidden" class="form-control" name="useraddfamily" id="useraddfamily" maxlength="9" placeholder="Please enter passport no." />
                        <input type="text" class="form-control" name="passportno" id="passportno" maxlength="9" placeholder="Please enter passport no." />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Passport Exp. Date <span class="text-danger"> </span></label>
                        <br />
                        <span class="text-danger change-pos" id="passportnoexpirevalid"></span>
                        <input type="date" class="form-control" name="passportnoexpire" id="passportnoexpire" placeholder="YYYY/MM/DD" />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Nationality<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="nationalityvalid"></span>
                        <select class="custom-select form-control" name="nationality" id="nationality" />

                        </select>
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Religion<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="religionvalid"></span>
                        <select class="custom-select form-control" name="religion" id="religion" />

                        </select>
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Marital Status <span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="maritalstatusvalid"></span>
                        <select class="custom-select form-control" name="maritalstatus" id="maritalstatus" />

                        </select>
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12" id="hideshow">
                        <label>No. of Child <span class="text-danger"> </span></label>
                        <br />
                        <span class="text-danger change-pos" id="childvalid"></span>
                        <input type="number" min="0" max="9" class="form-control name-valid" name="child" id="child" autocomplete="off" placeholder="Choose no. of child." onkeypress="return onlyNumberKey(event)"  />
                    </div>
                </div>

                <div class="card-header">
                    <h3 class="card-title"><strong>Emergency Details</strong></h3>
                </div>
	            <div class="card-body row">
                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Emergency Contact Person Name <span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="familynamevalid"></span>
                        <input type="text" class="form-control name-valid" name="familyname" id="familyname" placeholder="Please enter emergency contact person name." />
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Emergency Contact Person Relation<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="familyrelationvalid"></span>
                        <select class="custom-select form-control" name="familyrelation" id="familyrelation" />

                        </select>
                    </div>

                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Emergency Contact No.<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="familycontactnovalid"></span>
                        <input type="text" class="form-control" name="familycontactno" id="familycontactno" maxlength="10" onkeypress="return onlyNumberKey(event)" placeholder="Please enter contact no." />
                    </div>
                </div>

                <div class="card-header">
                    <h3 class="card-title"><strong>Official Details</strong></h3>
                </div>
	            <div class="card-body row">
                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Employee Official E-mail<span class="text-danger"> *</span></label>
                        <br>
                        <span class="text-danger change-pos" id="officialemailidvalid"></span>
                        <input class="form-control text-lowercase" type="email" name="officialemailid" id="officialemailid"  placeholder="Please enter official email id." required="" />
                    </div>
                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
						<label>Password <span class="text-danger"> *</span></label>
                        <br>
                        <span class="text-danger change-pos" id="passwordvalid"></span>
						<div class="row gutters-xs">
							<div class="col">
								<input class="form-control" type="password" name="password" id="password" placeholder="Please create password." title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" autocomplete="off" required="" />
							</div>
							<span toggle="#password-field" class="col-auto" style="margin-left: -8px;"><button class="btn btn-secondary" type="button"><i class="fa fa-fw fa-eye field_icon toggle-password"></i></button></span>
						</div>
					</div>
                    <div class="form-group col-lg-4 col-md-6 col-sm-12">
                        <label>Confirm Password <span class="text-danger"> *</span></label>
                        <br>
                        <span class="text-danger change-pos" id="confirmpasswordvalid"></span>
                        <input class="form-control" type="password" name="confirmpassword" id="confirmpassword" placeholder="Please confirm create password." autocomplete="off" required="" />
                        <div id="msg"></div>
                    </div>
                    <div class="form-group col-md-4 col-sm-12">
                        <label>Employee Branch<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="branchnamevalidr"></span>
                        <select class="select form-control" name="branchUser" id="branchUser" required="" />

                        </select>
                    </div>

                    <div class="form-group col-md-4 col-sm-12">
                        <label>Employee Department<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="departmentnamevalidr"></span>
                        <select class="select form-control" name="departmentUser" id="departmentUser" required="" />

                        </select>
                    </div>

                    <div class="form-group col-md-4 col-sm-12">
                        <label>Employee Designation<span class="text-danger"> *</span></label>
                        <br />
                        <span class="text-danger change-pos" id="designationnamevalidr"></span>
                        <select class="select form-control" name="designationUser" id="designationUser" required="" />

                        </select>
                    </div>
                    <div class="form-group col-sm-12 text-right">
                    <button id="addemployeebutton" class="btn btn-success submit-btn">Submit</button>
                        <button class="btn btn-secondary" id="test" type="reset">Cancel</button>
                    </div>
                </div>
            </form>
            <div id="message">
              <p id="letter" class="invalid">A <b>lowercase</b> letter</p>
              <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p>
              <p id="number" class="invalid">A <b>number</b></p>
              <p id="length" class="invalid">Minimum <b>8 characters</b></p>
            </div>
        </div>
    </div>
</div>
<style>
/* The message box is shown when the user clicks on the password field */
#message {
  display:none;
  background: #f1f1f1;
  color: #000;
  position: relative;
  padding: 20px;
  margin-top: 10px;
}

#message p {
  padding: 0px 35px;
  font-size: 12px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
  color: green;
}

.valid:before {
  position: relative;
  left: -35px;
  content: "✔";
}

/* Add a red text color and an "x" when the requirements are wrong */
.invalid {
  color: red;
}

.invalid:before {
  position: relative;
  left: -35px;
  content: "✖";
}
</style>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

$(document).on('click', '.toggle-password', function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $("#password");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});


var myInput = document.getElementById("password");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
  document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
  document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  
  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 8) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }
}
</script>
<script type="text/javascript">



    //Select Maritial Relation by api
    $.ajax({
        url: base_url + "viewMartialPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#maritalstatus");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Marital Status</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.status).text(entry.status));
        });
    });

    //Select religion Relation by api
    $.ajax({
        url: base_url + "viewReligionPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#religion");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Religion</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.religion).text(entry.religion));
        });
    });

    //Select nationality Relation by api
    $.ajax({
        url: base_url + "viewNationalityPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#nationality");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Nationality</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.nationality).text(entry.nationality));
        });
    });
    //Select Family Relation by api
    $.ajax({
        url: base_url + "viewRelationnamePublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#familyrelation");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Relation</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.relation).text(entry.relation));
        });
    });

    $(function () {
        $("#maritalstatus").change(function () {
            if ($(this).val() == "Single/Unmarried") {
                $("#hideshow").hide();
            } else {
                $("#hideshow").show();
            }
        });
    });

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#firstnamevalid").text("");
            jQuery("#fathernamevalid").text("");
            jQuery("#contactnovalid").text("");
            jQuery("#emailidvalid").text("");
            jQuery("#dobvalid").text("");
            jQuery("#gendervalid").text("");
            jQuery("#addressvalid").text("");
            jQuery("#imagevalid").text("");
            jQuery("#branchnamevalid").text("");
            jQuery("#departmentnamevalid").text("");
            jQuery("#designationnamevalid").text("");
            jQuery("#joiningdatevalid").text("");
            jQuery("#officialemailidvalid").text("");
            jQuery("#passwordvalid").text("");
            jQuery("#passportnovalid").text("");
            jQuery("#passportnoexpirevalid").text("");
            jQuery("#nationalityvalid").text("");
            jQuery("#religionvalid").text("");
            jQuery("#maritalstatusvalid").text("");
            jQuery("#familynamevalid").text("");
            jQuery("#familyrelationvalid").text("");
            jQuery("#familycontactnovalid").text("");
            jQuery("#confirmpasswordvalid").text("");

        });
    });

   
    // For Personal Details Form Validation
    $(document).ready(function () {
        $("#addemployeebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#firstname").val().trim() == "") {
                $("#firstname").focus();
                $("#firstnamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#firstnamevalid").text("");
            }

            if ($("#fathername").val().trim() == "") {
                $("#fathername").focus();
                $("#fathernamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#fathernamevalid").text("");
            }

            if ($("#contactno").val().length < 10) {
                $("#contactno").focus();
                $("#contactnovalid").text("Enter valid conatct no.");
                errorCount++;
            } else {
                $("#contactnovalid").text("");
            }
            if ($("#emailid").val().trim() == "") {
                $("#emailid").focus();
                $("#emailidvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#emailidvalid").text("");
            }

            if ($("#dob").val().trim() == "") {
                //$("#dob").focus();
                $("#dobvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#dobvalid").text("");
            }

            if ($("#gender").val() == null) {
                $("#gender").focus();
                $("#gendervalid").text("Please select a gender.");
                errorCount++;
            } else {
                $("#gendervalid").text("");
            }

            if ($("#address").val().trim() == "") {
                $("#address").focus();
                $("#addressvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addressvalid").text("");
            }

            if ($("#image").val().trim() == "") {
                $("#image").focus();
                $("#imagevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#imagevalid").text("");
            }

            if ($("#joiningdate").val().trim() == "") {
                $("#joiningdatevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#joiningdatevalid").text("");
            }

            if ($("#officialemailid").val().trim() == "") {
                $("#officialemailid").focus();
                $("#officialemailidvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#officialemailidvalid").text("");
            }

            if ($("#password").val().trim() == "") {
                $("#password").focus();
                $("#passwordvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#passwordvalid").text("");
            }

            if ($("#confirmpassword").val().trim() == "") {
                $("#confirmpassword").focus();
                $("#confirmpasswordvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#confirmpasswordvalid").text("");
            }
            if ($("#password").val() != $("#confirmpassword").val()) {
                $("#confirmpassword").focus();
                $("#confirmpasswordvalid").text("Password do not match.");
                errorCount++;
            } else {
                $("#confirmpasswordvalid").text("");
            }
            /*if ($("#passportno").val().trim() == "") {
                $("#passportno").focus();
                $("#passportnovalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#passportnovalid").text("");
            }*/

            /*if ($("#passportnoexpire").val().trim() == "") {
                $("#passportnoexpirevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#passportnoexpirevalid").text("");
            }*/
            if ($("#branchUser").val() == null) {
            $("#branchUser").focus();
            $("#branchnamevalidr").text("Please select a branch name.");
            errorCount++;
        } else {
            $("#branchnamevalidr").text("");
        }

        if ($("#departmentUser").val() == null) {
            $("#departmentUser").focus();
            $("#departmentnamevalidr").text("Please select a department name.");
            errorCount++;
        } else {
            $("#departmentnamevalidr").text("");
        }

        if ($("#designationUser").val() == null) {
            $("#designationUser").focus();
            $("#designationnamevalidr").text("Please select a designation name.");
            errorCount++;
        } else {
            $("#designationnamevalidr").text("");
        }
            if ($("#nationality").val() == null) {
                $("#nationality").focus();
                $("#nationalityvalid").text("Please select a nationality.");
                errorCount++;
            } else {
                $("#nationalityvalid").text("");
            }

            if ($("#religion").val() == null) {
                $("#religion").focus();
                $("#religionvalid").text("Please select a religion.");
                errorCount++;
            } else {
                $("#religionvalid").text("");
            }

            if ($("#maritalstatus").val() == null) {
                $("#maritalstatus").focus();
                $("#maritalstatusvalid").text("Please select a marital status.");
                errorCount++;
            } else {
                $("#maritalstatusvalid").text("");
            }

            if ($("#familyname").val().trim() == "") {
                $("#familyname").focus();
                $("#familynamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#familynamevalid").text("");
            }

            if ($("#familyrelation").val() == null) {
                $("#familyrelation").focus();
                $("#familyrelationvalid").text("Please select a emergency contact person relation.");
                errorCount++;
            } else {
                $("#familyrelationvalid").text("");
            }

            if ($("#familycontactno").val().length < 10) {
                $("#familycontactno").focus();
                $("#familycontactnovalid").text("Please enter valid conatct no.");
                errorCount++;
            } else {
                $("#familycontactnovalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    function load_file() {
        if (!window.FileReader) {
            return alert("FileReader API is not supported by your browser.");
        }
        var $i = $("#image"), // Put file input ID here
            input = $i[0]; // Getting the element from jQuery
        if (input.files && input.files[0]) {
            file = input.files[0]; // The file
            fr = new FileReader(); // FileReader instance
            fr.onload = function () {
                // Do stuff on onload, use fr.result for contents of file
                $("#12arrdata").val(fr.result);
            };
            //fr.readAsText( file );
            fr.readAsDataURL(file);
        } else {
            // Handle errors here
            alert("File not selected or browser incompatible.");
        }
    }

    //Select Gender by api
    $.ajax({
        url: base_url + "viewGenderPublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#gender");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Select Gender</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.gender).text(entry.gender));
        });
    });

    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#branchUser");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });
    $("#branchUser").change(function () {
    $.ajax({
        url: base_url + "viewDepartment",
        data: { branch_id: $("select[name=branchUser]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#departmentUser");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Department</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });
});

$("#departmentUser").change(function () {
    $.ajax({
        url: base_url + "viewDesignation",
        data: { department_id: $("select[name=departmentUser]").val() },
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#designationUser");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Desigination</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
        });
    });
});

    // Add Employee list
    $("#addemployee").submit(function (e) {
        var formData = {
            //Personal Details
            branch_id: $("select[name=branchUser]").val(),
            department_id: $("select[name=departmentUser]").val(),
            designation_id: $("select[name=designationUser]").val(),
            first_name: $("input[name=firstname]").val(),
            father_name: $("input[name=fathername]").val(),
            phone: $("input[name=contactno]").val(),
            personal_email: $("input[name=emailid]").val(),
            dob: $("input[name=dob]").val(),
            gender: $("select[name=gender]").val(),
            address: $("textarea[name=address]").val(),
            user_image: $("#12arrdata").val(),
            joining_date: $("input[name=joiningdate]").val(),
            
            passport_no: $("input[name=passportno]").val(),
            passport_expiry: $("input[name=passportnoexpire]").val(),
            nationality: $("select[name=nationality]").val(),
            religion: $("select[name=religion]").val(),
            marital_status: $("select[name=maritalstatus]").val(),
            no_of_children: $("input[name=child]").val(),

            contactperson_name: $("input[name=familyname]").val(),
            contactperson_no: $("input[name=familycontactno]").val(),
            relationship: $("select[name=familyrelation]").val(),

            email: $("input[name=officialemailid]").val(),
            password: $("input[name=password]").val(),
            confirm_password: $("input[name=confirmpassword]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST", // define the type of HTTP verb we want to use (POST for our form)
            url: base_url + "addUser", // the url where we want to POST
            data: formData, // our data object
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonData = response;
            var falseData = response["data"];
            var successData = response["data"]["message"];
            if (jsonData["data"]["status"] == "1") {
                toastr.success(successData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>EmployeeList";
                }, 1000);
            } else {
                toastr.error(falseData);
                $("#addemployee [type=submit]").attr("disabled", false);
            }
        });
    });

    $(document).ready(function () {
        $("#addconfirmpassword").keyup(function () {
            if ($("#addpassword").val() != $("#addconfirmpassword").val()) {
                $("#msg").html("Password do not match").css("color", "red");
                $("#btnSubmit").attr("disabled", true);
            } else {
                $("#msg").html("Password matched").css("color", "green");
                $("#btnSubmit").attr("disabled", false);
            }
        });
    });
</script>