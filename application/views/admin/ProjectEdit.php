<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>ProjectDashboard">Project</a></li>
                <li class="breadcrumb-item active">Edit Project</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <form id="editproject" method="POST">
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Branch<span class="text-danger"> *</span></label><br>
                            <span id="editbranchvalidate" class="text-danger change-pos"></span>
                            <input value="" type="hidden" name="projectid" id="projectid">
                            <select class="custom-select form-control" name="editbranchid" id="editbranchid" >
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Department<span class="text-danger"> *</span></label><br>
                            <span id="editdepartmentvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="editdepartmentid" id="editdepartmentid"/>
                                
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Project Name<span class="text-danger"> *</span></label><br>
                            <span id="editprojectnamevalit" class="text-danger change-pos"></span>
                            <input value="" type="text" class="form-control name-valid" name="editprojectname" id="editprojectname" placeholder="Please enter project name." autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Client Name<span class="text-danger"> *</span></label><br>
                            <span id="editclientnamevalit" class="text-danger change-pos"></span>
                            <input value="" type="text" class="form-control name-valid" name="editclientname" id="editclientname" placeholder="Please enter client name." autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Created Date<span class="text-danger"> *</span></label><br>
                            <span id="editstartdatevalit" class="text-danger change-pos"></span>
                            <input value="" type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="editstartdate" id="editstartdate" placeholder="YYYY/MM/DD" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Deadline Date<span class="text-danger"> *</span></label><br>
                            <span id="editenddatevalidate" class="text-danger change-pos"></span>
                            <input value="" type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="editenddate" id="editenddate" placeholder="YYYY/MM/DD" readonly=""   />
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Project File<span class="text-danger"> *</span></label>
                            <br>
                            <span id="editfilevalidate" class="text-danger change-pos"></span>
                            <?php 
                             $queryfile = $this->db->query("SELECT * FROM `user_project` WHERE id='".$userid."'");
                             $arrPolicyfile = $queryfile->row();
                             $arrfile1 = $arrPolicyfile->project_file;
                            ?>
                            <input type="file" class="dropify form-control" name="file" id="file" onchange="load_file();" data-default-file="<?php echo $arrfile1;?>" data-allowed-file-extensions="pdf xlsx xls doc docx" data-max-file-size="5mb">                         
                            <input value="" type="hidden" id="12arrdata">
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Project Language<span class="text-danger"> *</span></label>
                            <br>
                            <span id="editprojectlanguagevalidate" class="text-danger change-pos"></span>
                            <input value="" class="form-control" cols="5" rows="4" type="text" id="editprojectlanguage" name="editprojectlanguage" placeholder="Please enter your project Language." autocomplete="off">
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Project Leader<span class="text-danger"> *</span></label><br>
                            <span id="editprojectleadervalit" class="text-danger change-pos"></span>
                            <select value="" class="form-control custom-control" name="editprojectleader" id="editprojectleader" />
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Project Team<span class="text-danger"> *</span></label><br>
                            <span id="addprojectleadervalit" class="text-danger change-pos"></span>
                            <?php 
                                             $query1 = $this->db->query("SELECT project_team FROM `user_project` WHERE id='".$userid."'");
                                             $arrPolicy = $query1->row();
                                             $arrTest1 = $arrPolicy->project_team;
                                            
                                             $query = $this->db->query("SELECT * FROM `users` WHERE id IN ($arrTest1)");
                                             $arrProjectsData1 = $query->result();
                                           $arrtempData =  array();
                                             foreach($arrProjectsData1 as $team1) {
                                                $arrtempData[] = $team1->first_name;
                                              } 
                                            $arrattet = implode(",",$arrtempData);
                                            
                                            ?>
                            <input class="form-control" type="hidden" value="<?php echo $arrattet;  ?>" name="SelectName" id="SelectName" />
                            <select class="custom-select form-control chosen-select" multiple name="addprojectTeam" id="UserRole"/>
                            <?php
                         $query = $this->db->query("SELECT id,first_name FROM `users`");
                         $arrProjectsData = $query->result();
                             foreach($arrProjectsData as $team) { ?>                                                        
                                <option value="<?php echo $team->first_name; ?>"><?php echo $team->first_name; ?></option>
                                     <?php } ?>
                            </select>
                            
                        </div>
                        <div class="form-group col-md-12 col-sm-12">
                            <label>Description<span class="text-danger"> *</span></label><br>
                            <span id="editdescriptionvalidate" class="text-danger change-pos"></span>
                            <textarea value="" class="form-control" name="editdescription" id="editdescription" autocomplete="off"  /></textarea>
                        </div>
                        <div class="form-group col-md-12 col-sm-12 text-right">
                            <button id="editjobbutton" class="btn btn-success submit-btn">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>            
</div>  
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script>
function load_file() {
        if (!window.FileReader) {
            return alert("FileReader API is not supported by your browser.");
        }
        var $i = $("#file"), // Put file input ID here
            input = $i[0]; // Getting the element from jQuery
        if (input.files && input.files[0]) {
            file = input.files[0]; // The file
            fr = new FileReader(); // FileReader instance
            fr.onload = function () {
                // Do stuff on onload, use fr.result for contents of file
                $("#12arrdata").val(fr.result);
            };
            //fr.readAsText( file );
            fr.readAsDataURL(file);
        } else {
            // Handle errors here
            alert("File not selected or browser incompatible.");
        }
    }
$(".chosen-select").chosen({
  no_results_text: "Oops, nothing found!"
});
var selectedUserRole = document.getElementById('SelectName').value;
  
  var str_array = selectedUserRole.split(',');
 console.log(str_array);
  for (var i = 0; i < str_array.length; i++) {
      str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
  }

  $("#UserRole").val(str_array).trigger("chosen:updated");
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>


<script type="text/javascript">
       $(document).ready(function () {
           $("#test").click(function () {
               $("#editbranchid option:eq(0)").prop("selected", true);
               $("#editdepartmentid option:eq(0)").prop("selected", true);

               $("#editprojectleadervalit option:eq(0)").prop("selected", true);
           });
       });

       //for remove validation and empty input field
           $(document).ready(function() {
               $('#test').click(function(){
                   jQuery('#editbranchvalidate').text('');
                   jQuery('#editdepartmentvalidate').text('');
                   jQuery('#editprojectnamevalit').text('');
                   jQuery('#editclientnamevalit').text('');
                   jQuery('#editstartdatevalit').text('');
                   jQuery('#editenddatevalidate').text('');
                   jQuery('#editprojectamountvalit').text('');
                   jQuery('#editadvanceamountvalit').text('');

                   jQuery('#editprojectlanguagevalidate').text('');
                   jQuery('#editfilevalidate').text('');
                   jQuery('#editdescriptionvalidate').text('');
               });
           });
       //Select Branch for edit designation
           function fetchBranch(branch_id) {
               $.ajax({
                   url: base_url + "viewactiveBranch",
                   method: "POST",
                   data: {},
                   dataType: "json",
                   beforeSend: function (xhr) {
                       xhr.setRequestHeader("Token", localStorage.token);
                   },
               }).done(function (response) {
                   let dropdown = $("#editbranchid");

                   dropdown.empty();

                   dropdown.append("<option disabled>Choose Branch</option>");
                   dropdown.prop("selectedIndex", 0);

                   // Populate dropdown with list of provinces
                   $.each(response.data, function (key, entry) {
                       if (branch_id == entry.id) {
                           dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.branch_name));
                       } else {
                           dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
                       }
                   });
               });
           }


       // Select Department by select branch For Edit Designation
           function fetchDepartment(department_id) {
               $.ajax({
                   url: base_url + "viewDepartmentSelect",
                   method: "POST",
                   data: {},
                   dataType: "json",
                   beforeSend: function (xhr) {
                       xhr.setRequestHeader("Token", localStorage.token);
                   },
               }).done(function (response) {
                   let dropdown = $("#editdepartmentid");

                   dropdown.empty();

                   dropdown.append("<option disabled>Choose Department</option>");
                   dropdown.prop("selectedIndex", 0);

                   // Populate dropdown with list of provinces
                   $.each(response.data, function (key, entry) {
                       if (department_id == entry.department_id) {
                           dropdown.append($("<option selected></option>").attr("value", entry.department_id).text(entry.department_name));
                       } else {
                           dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
                       }
                   });
               });
           }



           $("#editbranchid").change(function () {
               $.ajax({
                   url: base_url + "viewDepartmentSelect",
                   data: { branch_id: $("select[name=editbranchid]").val() },
                   type: "POST",
                   dataType: "json", // what type of data do we expect back from the server
                   encode: true,
                   beforeSend: function (xhr) {
                       xhr.setRequestHeader("Token", localStorage.token);
                   },
               })
               .done(function (response) {
                   let dropdown = $("#editdepartmentid");

                   dropdown.empty();

                   dropdown.append('<option selected="true" disabled>Choose Department</option>');
                   dropdown.prop("selectedIndex", 0);

                   $.each(response.data, function (key, entry) {
                       dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
                   });
               });
           });

           function fetchJobType(project_leader_id)
        {
           
            $.ajax({  
                url: base_url+"viewUserName",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
                let dropdown = $('#editprojectleader');

                dropdown.empty();
                
                dropdown.append('<option disabled>Choose project Leader</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(project_leader_id == entry.first_name)
                        {
                            dropdown.append($('<option selected></option>').attr('value', entry.first_name).text(entry.first_name));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.first_name).text(entry.first_name));
                        }
                    })
            }); 
        }
                 // Edit  Project Form Fill */
           var project_id = <?php echo $userid; ?>;
               $.ajax({
                   url: base_url+"viewProject",
                   data:{project_id:project_id},
                   type: "POST",
                   dataType    : 'json',
                   beforeSend: function(xhr){
                       xhr.setRequestHeader('Token', localStorage.token);
                   },

                  success:function(response){

                         var branch_id = response["data"][0]["branch_id"];
                       var department_id = response["data"][0]["department_id"];
                       fetchBranch(branch_id);
                       fetchDepartment(department_id);
                        var project_leader_id = response["data"][0]["project_leader_id"];
                     fetchJobType(project_leader_id);
                           $('#projectid').val(response["data"][0]["id"]);
                           $('#editprojectname').val(response["data"][0]["project_name"]);
                           $('#editclientname').val(response["data"][0]["client_name"]);
                           $('#editstartdate').val(response["data"][0]["start_date"]);
                           $('#editenddate').val(response["data"][0]["end_date"]);
                           $('#editprojectlanguage').val(response["data"][0]["project_language"]);
                           $('#editdescription').val(response["data"][0]["project_description"]);
                           
                           $('#edit_job').modal('show');
                       }
                   });

               // Edit Project form*/
                   $("#editproject").submit(function(e) {
                       var formData = {
                           'project_id'          :  $('input[name=projectid]').val(),
                           'department_id'       :  $('select[name=editdepartmentid]').val(),
                           'project_name'        :  $('input[name=editprojectname]').val(),
                           'client_name'         :  $('input[name=editclientname]').val(),
                           'start_date'          :  $('input[name=editstartdate]').val(),
                           'end_date'            :  $('input[name=editenddate]').val(),
                           'project_language'    :  $('input[name=editprojectlanguage]').val(),
                           'project_description' :  $('textarea[name=editdescription]').val(),
                           'project_leader_id'   :  $('select[name=editprojectleader]').val(),
                           'project_team'        :  $("select[name=addprojectTeam]").val(),
                           'project_file'        :  $('#12arrdata').val()
                       };

                       e.preventDefault();
                       $.ajax({
                           type        : 'POST',
                           url         : base_url+'editProject',
                           data        : formData, // our data object
                           dataType    : 'json',
                           encode      : true,
                           beforeSend: function(xhr){
                               xhr.setRequestHeader('Token', localStorage.token);
                           }
                       })
                       .done(function(response) {
                           console.log(response);

                           var jsonDa = response
                           var jsonData = response["data"]["message"];
                           var falsedata = response["data"];

                           if (jsonDa["data"]["status"] == "1") {
                           toastr.success(jsonData);
                               setTimeout(function(){window.location ="<?php echo base_url()?>ProjectDashboard"},1000);

                           }

                           else {
                               toastr.error(falsedata);
                                   $('#editproject [type=submit]').attr('disabled',false);
                           }
                       });
                   });



               // Edit Job Validation
           $(document).ready(function() {
               $("#editjobbutton").click(function(e) {
                   e.stopPropagation();
                   var errorCount = 0;
                   if ($("#editbranchid").val() == null) {
                       $("#editbranchid").focus();
                       $("#editbranchvalidate").text("Please select a branch.");
                       errorCount++;
                   } else {
                       $("#editbranchvalidate").text("");
                   }
                   if ($("#editdepartmentid").val() == null) {
                       $("#editdepartmentid").focus();
                       $("#editdepartmentvalidate").text("Please select a department.");
                       errorCount++;
                   } else {
                       $("#editdepartmentvalidate").text("");
                   }
                   if ($("#editprojectleader").val() == null) {
                       $("#editprojectleadervalit").text("This field can't be empty.");
                       errorCount++;
                   }
                   else {
                       $("#editprojectleadervalit").text("");
                   }

                   if ($("#editprojectname").val().trim() == '') {
                       $("#editprojectname").focus();
                       $("#editprojectnamevalit").text("This field can't be empty.");
                       errorCount++;
                   }
                   else {
                       $("#editprojectnamevalit").text("");
                   }
                   if ($("#editclientname").val().trim() == '') {
                       $("#editclientnamevalit").text("This field can't be empty.");
                       errorCount++;
                   }
                   else {
                       $("#editclientnamevalit").text("");
                   }
                   if ($("#editstartdate").val().trim() == '') {
                       $("#editstartdatevalit").text("This field can't be empty.");
                       errorCount++;
                   }
                   else {
                       $("#editstartdatevalit").text("");
                   }
                   if ($("#editenddate").val().trim() == '') {
                       $("#editenddatevalidate").text("This field can't be empty.");
                       errorCount++;
                   }
                   else {
                       $("#editenddatevalidate").text("");
                   }

                   if ($("#editprojectlanguage").val().trim() == '') {
                       $("#editprojectlanguagevalidate").text("This field can't be empty.");
                       errorCount++;
                   }
                   else {
                       $("#editprojectlanguagevalidate").text("");
                   }
                   if ($("#editdescription").val().trim() == '') {
                       $("#editdescriptionvalidate").text("This field can't be empty.");
                       errorCount++;
                   }
                   else {
                       $("#editdescriptionvalidate").text("");
                   }

                  
                   if(errorCount > 0){
                       return false;
                   }
               });
           });
</script>
