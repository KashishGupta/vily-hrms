            
            <div class="section-body">
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                Copyright © 2021 <a href="https://ezdatechnology.com" target="_blank">Ezdat Technology Private Limited | HRMS</a>.
                            </div>
                            <!-- <div class="col-md-6 col-sm-12 text-md-right">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item"><a href="javascript:void(0)">Documentation</a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0)">FAQ</a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </footer>
            </div> 
        </div>
    </div>
    
    <script src="<?php echo base_url(); ?>assets/js/lib.vendor.bundle.js"></script>

    <!-- Text Editor  -->
    <script src="<?php echo base_url(); ?>assets/js/summernote.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/summernote.js"></script>

    <!-- Event Calendar -->
    <script src="<?php echo base_url(); ?>assets/js/fullcalendarscripts.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fullcalendar.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar.js"></script>
    
    <!-- DataTable -->

    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap4.min.js"></script>

    <!-- For Pdf Excel CSv -->
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
    
    <!-- Datetimepicker JS -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/core.js"></script>

    <!-- Toastr JS -->
    <script src="<?php echo base_url(); ?>assets/js/toastr.min.js"></script>   

    <script src="<?php echo base_url(); ?>assets/js/dropify.min.js"></script> 

    <!-- Search Select -->
    <!-- <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>

    <script type="text/javascript">
        $('.custom-select').select2();
    </script> -->

    <script>

    // For charchar value
        $(document).ready(function() {
            $('.name-valid').on('keypress', function(e) {
                var regex = new RegExp("^[a-zA-Z ]*$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            });
        });


    // For Number only
        function onlyNumberKey(evt) { 
            // Only ASCII charactar in that range allowed 
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
                return false; 
            return true; 
        }
        
    $(document).ready(function() {
        // Date Time Picker
        if($('.datetimepicker').length > 0) {
            $('.datetimepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                icons: {
                    up: "fa fa-angle-up",
                    down: "fa fa-angle-down",
                    next: 'fa fa-angle-right',
                    previous: 'fa fa-angle-left'
                }
            });
        }
    });

    $('.metismenu ul li a').each(function(){
        var eachHref = $(this).attr('href');
        var windowUrl = window.location.href;
        if(eachHref == windowUrl){
            $(this).parents('li').addClass('active');
            $(this).parents('ul:not(.metismenu)').addClass('collapse in');
            $(this).parents('ul:not(.metismenu)').siblings('a.has-arrow').attr('aria-expanded','true');
        }
    });

    $(function() {
        "use strict";
        
        $('.dropify').dropify();

        var drEvent = $('#dropify-event').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-dÃ©posez un fichier ici ou cliquez',
                replace: 'Glissez-dÃ©posez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'DÃ©solÃ©, le fichier trop volumineux'
            }
        });
    });
    </script>
        
</body>

</html>