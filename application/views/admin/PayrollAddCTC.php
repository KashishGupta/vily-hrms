<style type="text/css">
    a.btn.btn-circle.btn-default.btn-primary {
        border-bottom: 3px solid #28a745 !important;
    }
</style>
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>PayrollAddSalary">Payroll Salary</a></li>
                <li class="breadcrumb-item active">Add CTC</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <div class="row claerfix">
                    <div class="col-sm-12 col-lg-12">
                        <div class="stepwizard">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn btn-primary btn-circle">Salary Earn</a>
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">Salary Deduction</a>
                                </div>
                            </div>
                        </div>
                        <form role="form" method="POST" action="#" id="addpayroll">
                            <div class="row setup-content" id="step-1">
                                <div class="form-group row text-center" style="width: 100%">
                                    <label for="staticEmail" class="col-md-4 col-form-label">CTC <span class="text-danger"> *</span></label>
                                    <div class="col-md-4 ">
                                        <input type="text" name="ctc" id="ctc" class="form-control" placeholder="Please enter ctc." oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"/>
                                        <input type="hidden" name="userId" id="userId" class="form-control" value="<?php echo$userId ?>">
                                    </div>
                                    <span id="addctcvalid" class="text-danger"></span>
                                </div>
                                <table class="table" align="center">    
                                    <thead>
                                        <tr>    
                                            <th style="font-weight: 600;">SALARY EARN COMPONENTS</th>
                                            <th style="font-weight: 600;">CALCULATION TYPE (in %)</th>
                                            <th style="font-weight: 600;">AMOUNT MONTHLY</th>
                                            <th style="font-weight: 600;" class="text-right">AMOUNT ANNUALLY</th>
                                        </tr>   
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: 600;">Basic (INR)<span class="text-danger"> *</span></td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                    <input type="text" name="basicsalary" id="basicsalary" class="form-control" placeholder="Please enter basic salary." oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="0" >
                                                        <span class="input-group-append btn-primary" style="padding: 6px 15px;">(% of CTC)</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="basicmonthly" id="basicmonthly" class="form-control text-right" value="0" readonly="" >
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="basicyearly" id="basicyearly" class="form-control text-right" value="0" readonly="" >
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="font-weight: 600;">House Rent Allowance (INR)<span class="text-danger"> *</span></td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                    <input type="text" name="hra" id="hra" class="form-control" min="10" max="40" placeholder="Please enter HRA in %." oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="0"  />
                                                        <span class="input-group-append btn-primary" style="padding: 6px 10px;"> (% of Basic)</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="hramonthly" id="hramonthly" class="form-control text-right" placeholder="Please enter hra amount." value="0" readonly="" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="hrayearly" id="hrayearly" class="form-control text-right" placeholder="Please enter hra amount." value="0" readonly="" />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="font-weight: 600;">Conveyance Allowance (INR)<span class="text-danger"> *</span></td>
                                            <td>Fixed amount</td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="conveyanceamount" id="conveyanceamount" class="form-control text-right" min="10" max="40" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" placeholder="Please enter conveyance." value="0" />
                                                </div>
                                            </td>
                                            <td class="text-right">
                                            <div class="form-group">
                                                    <input type="text" name="conveyancefixed" id="conveyancefixed" class="form-control text-right" min="10" max="40" placeholder="Please enter conveyance." value="0" readonly=""/>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>

                                <div id="allowance" class="col-md-12">
                                    <div class="row claerfix">
                                    	<div class="form-group col-md-12">
                                            <button type="button" class="btn btn-primary btn-sm btn-icon icon-left" onClick="add_allowance()"><i class="fa fa-plus mr-2"></i> Add New Allowance </button>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <p><strong>Allowance Name</strong></p>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control" id="allowance_type" name="allowance_type[]" placeholder="Type" />

                                            <input type="hidden" class="form-control" name="netx_salary_month" id="netx_salary_month" value="0"/>
                                            <input type="hidden" class="form-control" name="netx_salary" id="netx_salary" value="0"/>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control text-right" id="percent_amount_1" name="percent[]" placeholder="Pleas enter amount" onkeyup="amt_all(this)" />
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control text-right" name="allowance_amount[]" value="0" id="allowance_amount_1" readonly="" />
                                        </div>
                                    </div>
                                </div>

                                <div id="allowance_input" class="col-md-12">
                                    <div class="row claerfix">
                                        <div class="form-group col-md-3 col-sm-12">
                                            <p><strong>Allowance Name</strong></p>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control" name="allowance_type[]" placeholder="Type" />
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control text-right" id="percent_amount" name="percent[]" placeholder="Pleas enter amount" onkeyup="amt_all(this)" />
                                        </div>
                                        <div class="form-group col-md-2 col-sm-12">
                                            <input type="number" class="form-control text-right" name="allowance_amount[]" value="0" id="allowance_amount" readonly="" />
                                        </div>
                                        <div class="form-group col-md-1 col-sm-12">
                                            <button type="button" class="btn btn-danger" id="allowance_amount_delete" onclick="deleteAllowanceParentElement(this)"> <i class="fa fa-trash-alt"></i> </button>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="kt-section__content kt-section__content--solid" style="">
                                    <button type="button" class="btn btn-info btn-sm mb-3" onClick="calculate_total_allowance()"> Calculate Total Allowance</button>
                                </div>

                                <table class="table" align="center">    
                                	<tbody>
                                        <tr style="background: #eee" >
                                            <td style="font-weight: 600;">Cost to Company</td>
                                            <td></td>
                                            <td></td>
                                            <td style="font-weight: 600;" class="text-center">₹ <span id="net_salaryt_month"></span> </td>
                                            <input type="hidden" name="conutsamount1" id="conutsamount1">
                                            <input type="hidden" name="net_salaryt_momnth1" id="net_salaryt_momnth1">
                                            <td style="font-weight: 600;" class="text-right">₹ <span id="netx_salary_year"></span> </td>
                                            <input type="hidden" name="conutsamountYearly1" id="conutsamountYearly1">
                                            <input type="hidden" name="netx_salary_year1" id="netx_salary_year1">
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <div class="form-group col-sm-12 text-right">
                                    <button class="btn btn-primary nextBtn submit-btn" id="addsalarybuttontest" type="button">Next</button>

                                    <button type="reset" class="btn btn-secondary" >Reset</button>
                                </div>
                            </div>

                            <div class="row setup-content" id="step-2">
                                <table class="table" align="center">    
                                    <thead>
                                        <tr>    
                                            <th style="font-weight: 600;">SALARY DEDUCTION COMPONENTS</th>
                                            <th style="font-weight: 600;">CALCULATION TYPE (in %)</th>
                                            <th style="font-weight: 600;">AMOUNT MONTHLY</th>
                                            <th style="font-weight: 600;" class="text-right">AMOUNT ANNUALLY</th>
                                        </tr>   
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="font-weight: 600;">PF</td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                    <input type="text" name="pf" id="pf" class="form-control" placeholder="Please enter pf." oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="0" >
                                                        <span class="input-group-append btn-primary" style="padding: 6px 10px;">(% of Basic)</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="pfmonthly" id="pfmonthly" class="form-control text-right" value="0" readonly="" >
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="pfyearly" id="pfyearly" class="form-control text-right" value="0" readonly="" >
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="font-weight: 600;">ESI</td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                    <input type="text" name="esi" id="esi" class="form-control" min="10" max="40" placeholder="Please enter hra." oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" value="0"  />
                                                        <span class="input-group-append btn-primary" style="padding: 6px 10px;"> (% of Basic)</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="esimonthly" id="esimonthly" class="form-control text-right" placeholder="Please enter esi." value="0" readonly="" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="esiyearly" id="esiyearly" class="form-control text-right" placeholder="Please enter esi." value="0" readonly="" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td>OR</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: 600;">Medical Insurance</td>
                                            <td>
                                                <p>Fixed</p>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="medicalmonthly" id="medicalmonthly" class="form-control text-right" min="10" max="40" placeholder="Please enter insurance amount." value="0" />
                                                </div>
                                            </td>
                                            <td class="text-right">
                                            <div class="form-group">
                                                    <input type="text" name="medicalyearly" id="medicalyearly" class="form-control text-right" min="10" max="40" placeholder="Please enter insurance amount." value="0" readonly=""/>
                                                </div>
                                            </td>
                                        </tr>                                 
                                    </tbody>
                                </table>

                                <div id="deduction" class="col-md-12">
                                    <div class="row claerfix">
                                    	<div class="form-group col-md-12">
                                            <button type="button" class="btn btn-primary btn-sm btn-icon icon-left" onClick="add_deduction()"><i class="fa fa-plus mr-2"></i> Add New Deduction </button>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <p><strong>Deduction Name</strong></p>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control" id="deduction_type" name="deduction_type[]" placeholder="Type" />
                                            <input type="hidden" class="form-control" id="total_deduction_percent"  />
                                        <input type="hidden" class="form-control" id="total_deduction"  />
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control text-right" id="percent_deduction_1" value="0" name="percentdeduction[]" placeholder="Pleas enter amount" onkeyup="amt_de(this)" />
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control text-right" name="deduction_amount[]" value="0" id="deduction_amount_1" readonly="" />
                                        </div>
                                    </div>
                                </div>

                                <div id="deduction_input" class="col-md-12">
                                    <div class="row claerfix">
                                        <div class="form-group col-md-3 col-sm-12">
                                            <p><strong>Deduction Name</strong></p>
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                            <input type="text" class="form-control" name="deduction_type[]" placeholder="Type" />
                                        </div>
                                        <div class="form-group col-md-3 col-sm-12">
                                        
                                       
                                            <input type="text" class="form-control text-right" id="percent_deduction" name="percentdeduction[]" placeholder="&#037;" onkeyup="amt_de(this)" />
                                        </div>
                                        <div class="form-group col-md-2 col-sm-12">
                                            <input type="number" class="form-control text-right" name="deduction_amount[]" value="0"  id="deduction_amount" readonly="" />
                                        </div>
                                        <div class="form-group col-md-1 col-sm-12">
                                            <button type="button" class="btn btn-danger" id="deduction_amount_delete" onclick="deleteDeductionParentElement(this)"> <i class="fa fa-trash-alt"></i> </button>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="kt-section__content kt-section__content--solid" style="">
                                    <button type="button" class="btn btn-info btn-sm mb-3" onClick="calculate_total_deduction()"> Calculate Total Deduction</button>
                                </div>

                                <table class="table" align="center">
                                	<tbody>
                                		<tr style="background: #eee" >
                                            <td style="font-weight: 600;">Deduction</td>
                                            <td></td>
                                            <td style="font-weight: 600;" class="text-center">₹ <span id="net_deduction_month1"></span></td>
                                            <input type="hidden" name="deductionmonthly1" id="deductionmonthly1">
                                            <input type="hidden" name="net_deduction_month" id="net_deduction_month">
                                            
                                            <td style="font-weight: 600;" class="text-right">₹ <span id="net_deduction_year1"></span></td>
                                            <input type="hidden" name="deductionyearly1" id="deductionyearly1">
                                            <input type="hidden" name="net_deduction_year" id="net_deduction_year">
                                            
                                        </tr>

                                        <tr style="background: #eee; margin-top: 30px "  >
                                            <td style="font-weight: 600;">Total Final CTC</td>
                                            <td></td>
                                            <td style="font-weight: 600;" class="text-center">₹ <span id="final_net_deduction_month1"></span></td>
                                            <input type="hidden" name="final_net_deduction_month" id="final_net_deduction_month">
                                            <td style="font-weight: 600;" class="text-right">₹ <span id="final_net_deduction_year1"></span></td>
                                            <input type="hidden" name="final_net_deduction_year" id="final_net_deduction_year">
                                        </tr>
                                	</tbody>
                                </table>

                                <div class="form-group col-sm-12 text-right">
                                    <button id="addsalarybutton" class="btn btn-success submit-btn">Submit</button> 
                                    <button type="reset" class="btn btn-secondary" id="test">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
var allowance_count     = 1;
var deduction_count     = 1;
var total_allowance     = 0;
var total_deduction     = 0;
var total_allowance_month      = 0;
var total_deduction_percent      = 0;
var deleted_allowances  = [];
var deleted_deductions  = [];


$('#allowance_input').hide();
    
    // CREATING BLANK ALLOWANCE INPUT
    var blank_allowance = '';
    $(document).ready(function () {
        blank_allowance = $('#allowance_input').html();
    });
function add_allowance()
{
    allowance_count++;
    $("#allowance").append(blank_allowance);
    $('#allowance_amount').attr('id', 'allowance_amount_' + allowance_count);
    $('#percent_amount').attr('id', 'percent_amount_' + allowance_count);
    $('#allowance_amount_delete').attr('id', 'allowance_amount_delete_' + allowance_count);
    $('#allowance_amount_delete_' + allowance_count).attr('onclick', 'deleteAllowanceParentElement(this, ' + allowance_count + ')');
}

// REMOVING ALLOWANCE INPUT
function deleteAllowanceParentElement(n, allowance_count) {
    n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
    deleted_allowances.push(allowance_count);
}





function amt_all(elem){
    var p = $(elem).val();
    var a = parseInt(p) *12;
    $(elem).parent().parent().find($('[name^=allowance_amo]')).attr('value',a);
}


function calculate_total_allowance()
{
    var amount;
    var percent_amount;
    for(i = 1; i <= allowance_count; i++) {
        if(jQuery.inArray(i, deleted_allowances) == -1)
        {
            amount = $('#allowance_amount_' + i).val();
             percent_amount = $('#percent_amount_' + i).val(); 
             
            if(amount != '') {
                amount = parseFloat(amount).toFixed(2);
                amount = parseFloat(amount);
                total_allowance = amount + total_allowance;
               // alert(total_allowance);
                $('#netx_salary').attr('value', total_allowance);
            }
            if(percent_amount != '') {
                percent_amount = parseFloat(percent_amount).toFixed(2);
                percent_amount = parseFloat(percent_amount);
                 //alert(percent_amount);
                total_allowance_month = percent_amount + total_allowance_month;
              
                $('#netx_salary_month').attr('value', total_allowance_month);
            }

        }
    }
 // ttt = $('#netx_salary').val();
 net_salary_month = parseInt($('#conutsamount1').val()) + parseInt($('#netx_salary_month').val());
 //alert(net_salary_month);
    $('#net_salaryt_momnth1').attr('value', net_salary_month);
    $('#net_salaryt_month').html(net_salary_month);
    total_allowance_month = 0;

   // ttt = $('#netx_salary').val();
   net_salary_year = parseInt($('#conutsamountYearly1').val()) + parseInt($('#netx_salary').val());
    $('#netx_salary_year1').attr('value', net_salary_year);
    $('#netx_salary_year').html(net_salary_year);
    total_allowance = 0;
}


// deductions

$('#deduction_input').hide();
    
    // CREATING BLANK DEDUCTION INPUT
    var blank_deduction = '';
    $(document).ready(function () {
        blank_deduction = $('#deduction_input').html();
    });

    function add_deduction()
    {
        deduction_count++;
        $("#deduction").append(blank_deduction);
        $('#deduction_amount').attr('id', 'deduction_amount_' + deduction_count);
        $('#percent_deduction').attr('id', 'percent_deduction_' + deduction_count);
        $('#deduction_amount_delete').attr('id', 'deduction_amount_delete_' + deduction_count);
        $('#deduction_amount_delete_' + deduction_count).attr('onclick', 'deleteDeductionParentElement(this, ' + deduction_count + ')');
    }

    // REMOVING DEDUCTION INPUT
    function deleteDeductionParentElement(n, deduction_count) {
        n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
        deleted_deductions.push(deduction_count);
    }
    
    function amt_de(elem){
        var p = $(elem).val();
        var a = parseInt(p) *12;
        $(elem).parent().parent().find($('[name^=deduction_amo]')).attr('value',a);
    }

    function calculate_total_deduction()
    {
        var amount;
        var percent_deduction;
        for(i = 1; i <= deduction_count; i++) {
            if(jQuery.inArray(i, deleted_deductions) == -1)
            {
                amount = $('#deduction_amount_' + i).val();
                percent_deduction =  $('#percent_deduction_' + i).val();
               
                if(amount != '') {
                    amount = parseFloat(amount).toFixed(2);
                    amount = parseFloat(amount);
                      //alert(amount);
                    total_deduction = amount + total_deduction;
                  
                    $('#total_deduction').attr('value', total_deduction);
                }
                if(percent_deduction != '') {
                    percent_deduction = parseFloat(percent_deduction).toFixed(2);
                    percent_deduction = parseFloat(percent_deduction);
                   // alert(percent_deduction);
                    total_deduction_percent = percent_deduction + total_deduction_percent;
                   
                    $('#total_deduction_percent').attr('value', total_deduction_percent);
                }
            }
        }
        net_deduction_month =parseInt($('#total_deduction_percent').val()) + parseInt($('#deductionmonthly1').val());
        $('#net_deduction_month').attr('value', net_deduction_month);
        $('#net_deduction_month1').html(net_deduction_month);
        total_deduction_percent = 0;

        net_deduction_year =parseInt($('#total_deduction').val()) + parseInt($('#deductionyearly1').val());
        $('#net_deduction_year').attr('value', net_deduction_year);
        $('#net_deduction_year1').html(net_deduction_year);
        total_deduction = 0;

        final_monthly_amount =parseInt($('#net_salaryt_momnth1').val()) - parseInt($('#net_deduction_month').val());
        $('#final_net_deduction_month').attr('value', final_monthly_amount);
        $('#final_net_deduction_month1').html(final_monthly_amount);
        final_monthly_amount = 0;

        final_year_amount =parseInt($('#netx_salary_year1').val()) - parseInt($('#net_deduction_year').val());
        $('#final_net_deduction_year').attr('value', final_year_amount);
        $('#final_net_deduction_year1').html(final_year_amount);
        final_year_amount = 0;
        
    }

    $(document).ready(function () {
        var navListItems = $("div.setup-panel div a"),
            allWells = $(".setup-content"),
            allNextBtn = $(".nextBtn");
        allWells.hide();
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr("href")),
                $item = $(this);

            if (!$item.hasClass("disabled")) {
                navListItems.removeClass("btn-primary").addClass("btn-default");
                $item.addClass("btn-primary");
                allWells.hide();
                $target.show();
                $target.find("input:eq(0)").focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]')
                    .parent()
                    .next()
                    .children("a"),
                curInputs = curStep.find("input[type='text'], select ,input[type='url']"),// Commented
                isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {// Commented
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }// Commented

            if (isValid) nextStepWizard.removeAttr("disabled").trigger("click");
        });

        $("div.setup-panel div a.btn-primary").trigger("click");
    });

    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#addctcvalid").text("");
        });
    });

$(document).ready(function () {
    $("#ctc,#basicsalary,#hra,#conveyanceamount,#pf,#esi,#medicalmonthly").keyup(function (e) {
        var This_id = $('#basicsalary').val();
        var This_idis = $('#ctc').val();
        var This_hra = $('#hra').val();
        var This_convayance = $('#conveyanceamount').val();
        //var This_monthaly = $('#monthlyallowance').val();

        net_salary = parseInt(This_idis) /100 * parseInt(This_id);
        $('#basicyearly').attr('value', net_salary);

        var arrnetdata = parseInt(net_salary) /12;
        $('#basicmonthly').attr('value', arrnetdata);

        arrhraamount = parseInt(net_salary) /100 * parseInt(This_hra);
        $('#hrayearly').attr('value', arrhraamount);

        var arrmonthdata = parseInt(arrhraamount) /12;
        $('#hramonthly').attr('value', arrmonthdata);

        var arrconvayance = parseInt(This_convayance) * 12;
        $('#conveyancefixed').attr('value', arrconvayance);

        //var arrmonthaly = parseInt(This_monthaly) * 12;
        //$('#yearlyallowance').attr('value', arrmonthaly);

        // total counts 
        var countsTotal = parseInt(arrnetdata) + parseInt(arrmonthdata) + parseInt(This_convayance);
        $('#conutsamount').html(countsTotal);
        $('#conutsamount1').val(countsTotal);
        var countsTotalYearly = parseInt(net_salary) + parseInt(arrhraamount) + parseInt(arrconvayance);
        $('#conutsamountYearly').html(countsTotalYearly);
        $('#conutsamountYearly1').val(countsTotalYearly);

        // deduction data
        var This_pf = $('#pf').val();
       // alert(This_pf);
        net_pf = parseFloat(net_salary /100 * This_pf);
        //alert(net_pf);
        $('#pfyearly').attr('value', net_pf);
        netmonth = parseInt(net_pf) / 12;
        $('#pfmonthly').attr('value', netmonth);
        
        //esi
        var This_esi = $('#esi').val();
        net_esi = parseFloat(net_salary /100 * This_esi);
        $('#esiyearly').attr('value', net_esi);
        netesi_month = parseInt(net_esi) / 12;
        $('#esimonthly').attr('value', netesi_month);
        
        // medical 
        var This_medical = $('#medicalmonthly').val();
        var medical_month = parseFloat(This_medical * 12);
        $('#medicalyearly').attr('value', medical_month);

        var countsdeductionTotal = parseInt(netmonth) + parseInt(netesi_month) + parseInt(This_medical);
        $('#deductionmonthly').html(countsdeductionTotal);
        $('#deductionmonthly1').val(countsdeductionTotal);
        var countsdeductionTotalyearly = parseInt(net_pf) + parseInt(net_esi) + parseInt(medical_month);
        $('#deductionyearly').html(countsdeductionTotalyearly);
        $('#deductionyearly1').val(countsdeductionTotalyearly);

        var totalpayableamountmonthly = parseInt($('#net_salaryt_momnth1').val()) - parseInt(countsdeductionTotal);
        totala1rrData2 = parseInt(totalpayableamountmonthly) + parseInt($('#netx_salary_month').val()) ;
        $('#totalearningpayablemonthly').val(totala1rrData2);
        $('#totalearningpayablemonthly1').html(totala1rrData2);
        var totalpayableamountyearly = parseInt($('#netx_salary').val()) - parseInt(countsdeductionTotalyearly);
        totala1rrData = parseInt(totalpayableamountyearly) + parseInt($('#netx_salary').val()) ;
        $('#totalearningpayableyearly').val(totala1rrData);
        $('#totalearningpayableyearly1').html(totala1rrData);
       
    });
});



   $("#addpayroll").submit(function (e) {
    var allowance_type = $('input[name="allowance_type[]"]').map(function(){ 
                    return this.value; 
                }).get();
    var percent = $('input[name="percent[]"]').map(function(){ 
    return this.value; 
     }).get();

    var allowance_amount = $('input[name="allowance_amount[]"]').map(function(){ 
    return this.value; 
     }).get();

     // deductions
     var deduction_type = $('input[name="deduction_type[]"]').map(function(){ 
                    return this.value; 
                }).get();
    var deduction_percent = $('input[name="percentdeduction[]"]').map(function(){ 
    return this.value; 
     }).get();

    var deduction_amount = $('input[name="deduction_amount[]"]').map(function(){ 
    return this.value; 
     }).get();
    var formData = {
        allowance_type: allowance_type,
        percent: percent,
        allowance_amount: allowance_amount,

        deduction_type: deduction_type,
        deduction_percent: deduction_percent,
        deduction_amount: deduction_amount,

        user_id: $("input[name=userId]").val(),
        
        ctc: $("input[name=ctc]").val(),
        basic_percent: $("input[name=basicsalary]").val(),
        basic_monthly: $("input[name=basicmonthly]").val(),
        basic_yearly: $("input[name=basicyearly]").val(),

        hra_percent: $("input[name=hra]").val(),
        hra_monthly: $("input[name=hramonthly]").val(),
        hra_yearly: $("input[name=hrayearly]").val(),

        conveyance_monthly: $("input[name=conveyanceamount]").val(),
        yearly_conveyance: $("input[name=conveyancefixed]").val(),

        total_earning_monthly: $("input[name=net_salaryt_momnth1]").val(),
        total_earning_yearly: $("input[name=netx_salary_year1]").val(),


        pf: $("input[name=pf]").val(),
        pf_monthly: $("input[name=pfmonthly]").val(),
        pf_yearly: $("input[name=pfyearly]").val(),

        esi: $("input[name=esi]").val(),
        esi_monthly: $("input[name=esimonthly]").val(),
        esi_yearly: $("input[name=esiyearly]").val(),

        medical_monthly: $("input[name=medicalmonthly]").val(),
        medical_yearly: $("input[name=medicalyearly]").val(),

        

        total_deduction_monthly: $("input[name=net_deduction_month]").val(),
        total_deduction_yearly: $("input[name=net_deduction_year]").val(),

        total_amount_monthly: $("input[name=final_net_deduction_month]").val(),
        total_amount_yearly: $("input[name=final_net_deduction_year]").val(),
    };
   
                
    e.preventDefault();
    $.ajax({
        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
        url: base_url + "addPayrollCtc", // the url where we want to POST
        data: formData, // our data object
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
        // using the done promise callback
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>PayrollAddSalary";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addsalarybutton [type=submit]").attr("disabled", false);
            }
        });
});
</script>