<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Chat</li>
            </ul>
        </div>
    </div>
</div>

<div class="section-body mt-3">
    <div class="chat_app">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                	<div class="card" id="users">
			            <a href="javascript:void(0)" class="chat_list_btn float-right"><i class="fa fa-align-right"></i></a>
			            <ul class="nav nav-tabs" role="tablist">
			                <li class="nav-item">
			                    <a class="nav-link active" id="users-tab" data-toggle="tab" href="#users-list" role="tab" aria-controls="users-list" aria-selected="true">Users</a>
			                </li>
			                <li class="nav-item">
			                    <a class="nav-link" id="groups-tab" data-toggle="tab" href="#groups" role="tab" aria-controls="groups" aria-selected="false">Groups</a>
			                </li>                    
			            </ul>
			            <div class="input-group mt-3 mb-2 pl-2 pr-2">
			                <input type="text" class="form-control search " placeholder="Search...">
			            </div>
			            <div class="tab-content">
			                <div class="tab-pane fade show active" id="users-list" role="tabpanel" aria-labelledby="users-tab">
			                    <ul class="right_chat list-unstyled list Kt-users">			                        
			                    </ul>
			                </div>
			                <div class="tab-pane fade" id="groups" role="tabpanel" aria-labelledby="groups-tab">
			                    <ul class="right_chat list-unstyled list">
			                        <li class="online">
			                            <a href="javascript:void(0);">
			                                <div class="media">
			                                    <img class="media-object" src="https://nsdbytes.com/template/epic/assets/images/xs/avatar2.jpg" alt="">
			                                    <div class="media-body">
			                                        <span class="name">PHP Groups</span>
			                                        <span class="message">How is the project coming</span>
			                                        <span class="badge badge-outline status"></span>
			                                    </div>
			                                </div>
			                            </a>
			                        </li>
			                        <li class="online">
			                            <a href="javascript:void(0);">
			                                <div class="media">
			                                    <img class="media-object" src="https://nsdbytes.com/template/epic/assets/images/xs/avatar2.jpg" alt="">
			                                    <div class="media-body">
			                                        <span class="name">Family Groups</span>
			                                        <span class="message">Update Code</span>
			                                        <span class="badge badge-outline status"></span>
			                                    </div>
			                                </div>
			                            </a>                            
			                        </li>
			                        <li class="offline">
			                            <a href="javascript:void(0);">
			                                <div class="media">
			                                    <img class="media-object" src="https://nsdbytes.com/template/epic/assets/images/xs/avatar2.jpg" alt="">
			                                    <div class="media-body">
			                                        <span class="name">Harry McCall</span>
			                                        <span class="message">3 New design bug</span>
			                                        <span class="badge badge-outline status"></span>
			                                    </div>
			                                </div>
			                            </a>                            
			                        </li>
			                        <li class="offline">
			                            <a href="javascript:void(0);">
			                                <div class="media">
			                                    <img class="media-object" src="https://nsdbytes.com/template/epic/assets/images/xs/avatar2.jpg" alt="">
			                                    <div class="media-body">
			                                        <span class="name">Friends holic</span>
			                                        <span class="message">Hello All!</span>
			                                        <span class="badge badge-outline status"></span>
			                                    </div>
			                                </div>
			                            </a>                            
			                        </li>
			                        <li class="online">
			                            <a href="javascript:void(0);">
			                                <div class="media">
			                                    <img class="media-object" src="https://nsdbytes.com/template/epic/assets/images/xs/avatar2.jpg" alt="">
			                                    <div class="media-body">
			                                        <span class="name">CL City 2</span>
			                                        <span class="message">Add new contact</span>
			                                        <span class="badge badge-outline status"></span>
			                                    </div>
			                                </div>
			                            </a>                            
			                        </li>
			                    </ul>
			                </div>
			            </div>
			        </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="card">
                        <div class="card-header bline ">
                        	<h3 class="card-title"><strong>Friends Group <small>Last seen: 2 hours ago</small></strong></h3>
                            <div class="card-options">
                                <a href="javascript:void(0)" class="p-1 chat_list_btn"><i class="fa fa-align-right"></i></a>
                                <a href="javascript:void(0)" class="p-1"><i class="fa fa-plus"></i></a>
                                <a href="javascript:void(0)" class="p-1"><i class="fa fa-cog"></i></a>
                                <a href="javascript:void(0)" class="p-1"><i class="fa fa-sync-alt"></i></a>
                            </div>
                        </div>                        
                        <div class="chat_windows">
                            <ul class="mb-0 Kt-userschat">
                                                            
                            </ul>
							<form id="chatusers" method="POST" action="#">
                                <div class="chat-message clearfix">
                                    <div class="input-group mb-0">
    								<input value="<?php echo $userid; ?>" id="userid" name="userid" class="form-control" type="hidden" />
                                        <textarea type="text" class="form-control" name="chatmessage" id="chatmessage" placeholder="Enter text here..."></textarea>
                                    </div>
                                    <div class="submit-section pull-right">
                                        <button id="reply"  type="submit" class="btn btn-primary">Send</button>
                                    </div>
                                </div>
							</form>
                           
                        </div>                            
                    </div>
                </div>
            </div>
        </div>
        
    </div>        
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.js"></script>
<script src="<?php echo base_url();?>assets/app.js"></script>
<script src="<?php echo base_url();?>assets/js/contactService.js"></script>
<script src="<?php echo base_url();?>assets/js/contactController.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
    var input = document.getElementById("chatmessage");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
           event.preventDefault();
           document.getElementById("reply").click();
        }
    });

    $(document).ready(function () {
        $('.btn-primary').click(function (e) {
            e.preventDefault();
            var message = $('#chatmessage').val();
            var user_id = $('#userid').val();
        
            $.ajax({
                type: "POST",
                url: base_url + "userConversation",
                data: { "user_id": user_id, "message": message },
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
                success: function (response) { 
                    var jsonData = response["data"]["message"];
                    toastr.success(jsonData);
                    $('#chatusers')[0].reset();   
                    $('html, body').animate({
                        scrollTop: $(".chat_windows").offset().top
                    }, 10);         
                }
            });
        });
     });

    // View Notification Public
    $.ajax({
        url: base_url + "viewconversationUser",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-xl-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Reimbursement</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-OpentestNotification").append(html);
        } 
        else {
            for (i in response.data) {
                var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } 
                    else {
                        UserImage = response.data[i].user_image;
                    }
                    var html ='<li class="online"><a href="<?php echo base_url(); ?>Chat/'+response.data[i].user_id+'"><div class="media"><img class="media-object" src=" ' + UserImage  + ' " alt=""><div class="media-body"><span class="name">' +  response.data[i].first_name + '</span><span class="badge badge-outline status"></span></div></div></a></li>';
                $(".Kt-users").append(html);
            }
        }
    });

    var users_id = <?php echo $userid; ?>;
    $.ajax({
        url: base_url + "userConversationclick",
        data: {users_id: users_id},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
    });

     // View Offerletter
     var userid =  localStorage.userid;
     var user_id = <?php echo $userid; ?>;
     $.ajax({
        url: base_url + "viewconversationSingleUser",
        data: {user_id: user_id },
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        for (i in response.data) {
    		if(response.data[i].user_id_fk == userid ){
                var html ='<li class="other-message pl-2"><span class="avatar avatar-blue mr-3">NG</span><div class="message"><p class="bg-light-pink">'+response.data[i].message+'</p><span class="time" >'+timeago(response.data[i].time)+'</span></div></li>';
    	}
        else{
            var html = '<li class="my-message pr-2"><div class="message"><p class="bg-light-gray">'+response.data[i].message+'</p><span class="time">'+timeago(response.data[i].time)+'</span></div></li>';
    	}
            $(".Kt-userschat").append(html);
        }
       
    });
   
    // time ago function
    function timeago(stringDate){
        var currDate = new Date();
        var diffMs=currDate.getTime() - new Date(stringDate).getTime();
        var sec=diffMs/1000;
        if(sec<=60)
            return '<span style = "color:red;">just now</span>';
        var min=sec/60;
        if(min<60)
            return parseInt(min)+' minute'+(parseInt(min)>1?'s ago':' ago');
        var h=min/60;
        if(h<=24)
            return (parseInt(h)>1? parseInt(h) +' hrs ago': ' an hour ago');
        var d=h/24;
        if(d<=7)
            return parseInt(d)+' day'+(parseInt(d)>1?'s ago':' ago');
        var w=d/7;
        if(w<=4.3)
          	 return (parseInt(w)>1? parseInt(w)+' weeks ago': ' a week ago');
        var m=d/30;
        if(m<=12)
            return parseInt(m)+' month'+(parseInt(m)>1?'s ago':' ago');
        var y=m/12;
        return parseInt(y)+' year'+(parseInt(y)>1?'s ago':' ago');
    }                 
</script>