<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Assets Category</li>
            </ul>
        </div>

        <!-- Add Assets Category Form -->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Asset Category</strong></h3>
            </div>
            <div class="card-body">
                <form id="addassetcategory" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Asset Category Name<span class="text-danger"> *</span></label><br />
                            <span id="assetcategoryvalid" class="text-danger change-pos"></span>
                            <input type="text" name="assetcategory" id="assetcategory" class="form-control" placeholder="Please enter asset category name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-5 col-md-6 col-sm-12">
                            <label>Asset Category Description <span class="text-danger">*</span></label><br />
                            <span id="assetcategorydescriptionvalid" class="text-danger change-pos"></span>
                            <textarea
                                rows="2"
                                name="assetcategorydescription"
                                id="assetcategorydescription"
                                class="form-control"
                                type="text"
                                placeholder="Please enter asset category description."
                                autocomplete="off"
                                style="resize: none;"
                            ></textarea>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                            <button id="assetcategorybutton" class="btn btn-success submit-btn">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Search Asset Category Result Table -->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Asset Category Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="assetcategorytable">
                    <thead>
                        <tr>
                            <th class="text-center">Asset Category</th>
                            <th class="text-center">Asset Category Description</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyassetcategory"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Edit Asset Category Status Modal -->
<div id="assetcategory_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Asset Category Status</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form id="editstatus" method="POST" action="#">
                <div class="modal-body">
                    <div class="form-header">
                        <p>Are you sure want to Active Or Inactive for this Asset Category?</p>
                    </div>

                    <div class="form-group">
                        <label>Asset Category Status<span class="text-danger">*</span></label>
                        <input value="" id="editassetcategoryidstatus" name="editassetcategoryidstatus" class="form-control" type="hidden" />
                        <select value="" id="editassetcategorystatus" name="editassetcategorystatus" class="form-control custom-select">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success submit-btn">Yes Change It!</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Edit Asset Category Status Modal -->

<!-- Edit Asset Category Modal -->
<div id="edit_assetcategory" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Asset Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editassetform" method="POST">
                    <div class="form-group col-md-12">
                        <label>Asset Category Name<span class="text-danger"> *</span></label><br />
                        <span id="editassetcategoryvalid" class="text-danger change-pos"></span>
                        <input type="text" value="" name="editassetcategory" id="editassetcategory" class="form-control" placeholder="Please enter asset category name." autocomplete="off" />
                        <input value="" id="editassetcategoryid" name="editassetcategoryid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Asset Category Description <span class="text-danger">*</span></label><br />
                        <span id="editassetcategrydescriptionvalid" class="text-danger change-pos"></span>
                        <textarea type="text" value="" name="editassetcategorydescription" id="editassetcategorydescription" class="form-control" placeholder="Please enter asset category description." autocomplete="off" /></textarea>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editassetcategorybutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Asset Category Modal -->


<!-- Delete Asset Issue Modal -->
<div class="modal custom-modal fade" id="delete_assetcategory" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button class="btn btn-danger continue-btn delete_assetcategory_button">Yes delete it!</button>
                    <button data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Asset Issue Modal -->


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#assetcategory").val("");
            $("textarea#assetcategorydescription").val("");
            $("span#assetcategoryvalid").prop("");
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#assetcategoryvalid").text("");
            jQuery("#assetcategorydescriptionvalid").text("");

            jQuery("#editassetcategoryvalid").text("");
            jQuery("#editassetcategrydescriptionvalid").text("");
        });
    });

    // Add Asset Category Form
    $("#addassetcategory").submit(function (e) {
        var formData = {
            category_assets: $("input[name=assetcategory]").val(),
            description: $("textarea[name=assetcategorydescription]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addAssetsCategory",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>AssetsCategory";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addassetcategory [type=submit]").attr("disabled", false);
            }
        });
    });

    // View Assets Category
    $(function () {
        filterCompanies("4");
        $("#assetsstatus").on("change", function () {
            assetsstatus = $(this).val();
            filterCompanies(assetsstatus);
        });
    });

    function filterCompanies(assetsstatus) {
        req = {};
        req.status = assetsstatus;
        $.ajax({
            url: base_url + "viewAssetsCategory",
            data: req,
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            timeout: 6000,
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            $("#assetcategorytable").DataTable().clear().destroy();
            $("#assetcategorytable tbody").empty();
            var table = document.getElementById("tablebodyassetcategory");
            for (i in response.data) {
                var testcheck = "";
                if (response.data[i].status == 1) {
                    testcheck = "checked";
                }
                if (response.data[i].status == 0) {
                    testcheck = "";
                }
                

                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +
                    response.data[i].category_assets +
                    "</td>" +
                    '<td class="text-center" style="white-space: break-spaces;">' +
                    response.data[i].description +
                    "</td>" +
                    '<td class="text-center"> <div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox" ' +
                    testcheck +
                    ' id="' +
                    response.data[i].id +
                    '" value="' +
                    response.data[i].status +
                    '" onclick="getstatus(' +
                    response.data[i].id +
                    ')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>' +
                    '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_assetcategory" aria-expanded="false" id="' +
                    response.data[i].id +
                    '" class="btn btn-info edit_data" title="Edit Assets Category" ><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_assetcategory" aria-expanded="false" id="' +
                    response.data[i].id +
                    '" class="btn btn-danger delete_data" title="Delete Assets Category" ><i class="fa fa-trash-alt"></i></button></td>';
                table.appendChild(tr);
            }
            var currentDate = new Date()
            var day = currentDate.getDate()
            var month = currentDate.getMonth() + 1
            var year = currentDate.getFullYear()
            var d = day + "-" + month + "-" + year;
            $("#assetcategorytable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: d+ ' Assets Category Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: d+ ' Assets Category Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    }
                ]
            });
        });
    }


    function getstatus(id) {
        var category_id = id;
        var status = $("#" + id).val();
        console.log(status);
        //alert(status);
        if (status == 0) {
            var status = 1;
            $.ajax({
                type: "POST",
                url: base_url + "editAssetsCategoryStatus",
                data: "category_id=" + category_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>AssetsCategory";
                }, 1000);
            });
        } else {
            var status = 0;
            $.ajax({
                type: "POST",
                url: base_url + "editAssetsCategoryStatus",
                data: "category_id=" + category_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>AssetsCategory";
                }, 1000);
            });
        }
    }
      

    //Edit  Assets Category Form Fill
    $(document).on("click", ".edit_data", function () {
        var category_id = $(this).attr("id");

        $.ajax({
            url: base_url + "viewAssetsCategory",
            method: "POST",
            data: {
                category_id: category_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editassetcategoryid").val(response["data"][0]["id"]);
                $("#editassetcategory").val(response["data"][0]["category_assets"]);
                $("#editassetcategorydescription").val(response["data"][0]["description"]);
                $("#edit_assetcategory").modal("show");
            },
        });
    });

    //Edit Branch Form
    $("#editassetform").submit(function (e) {
        var formData = {
            category_id: $("input[name=editassetcategoryid]").val(),
            category_assets: $("input[name=editassetcategory]").val(),
            description: $("textarea[name=editassetcategorydescription]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editAssetsCategory",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>AssetsCategory";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editbranch [type=submit]").attr("disabled", false);
            }
        });
    });

    //Delete Branch
    $(document).on("click", ".delete_data", function () {
        var category_id = $(this).attr("id");
        $(".delete_assetcategory_button").attr("id", category_id);
        $("#delete_assetcategory").modal("show");
    });

    $(document).on("click", ".delete_assetcategory_button", function () {
        var category_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deletAssetsCategory",
            method: "POST",
            data: {
                category_id: category_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>AssetsCategory";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_assetcategory_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    //Add Asset Category Validation
    $(document).ready(function () {
        $("#assetcategorybutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#assetcategory").val() == "") {
                $("#assetcatogory").focus();
                $("#assetcategoryvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#assetcategoryvalid").text("");
            }

            if ($("#assetcategorydescription").val().trim() == "") {
                $("#assetcategorydescriptionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#assetcategorydescriptionvalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Edit Asset Category Validation
    $(document).ready(function () {
        $("#editassetcategorybutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editassetcategory").val() == "") {
                $("#editassetcategory").focus();
                $("#editassetcategoryvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editassetcategoryvalid").text("");
            }

            if ($("#editassetcategorydescription").val().trim() == "") {
                $("#editassetcategrydescriptionvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editassetcategrydescriptionvalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>

