<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Reimbursement Policy</li>
            </ul>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs b-none">
                                <li class="nav-item">
                                    <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list"><i class="fa fa-list-ul"></i> Reimbursement Policy</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addnew"><i class="fa fa-plus"></i> Add Travell Policy</a>
                                </li>
                                 <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addhotelnew"><i class="fa fa-plus"></i> Add Hotel/Food Policy</a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="list" role="tabpanel">
                 <div class="card">
                    <div class="card-body">
                        <div class="table-responsive" id="users">
                            <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="ReimbursementPolicyh">
                                <thead>
                                    <tr>
                                        <th class="text-center">Policy Name</th>
                                        <th class="text-center">Limit</th>
                                        <th class="text-center">Duration</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="ReimbursementHoteltable"></tbody>
                            </table>
                        </div>
                    </div>
                </div> 

                <div class="card mt-3">
                    <div class="card-body">
                        <div class="table-responsive" id="users">
                            <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-md" id="ReimbursementPolicytrav">
                                <thead>
                                    <tr>
                                        <th class="text-center">Policy Name</th>
                                        <th class="text-center">Travel Type</th>
                                        <th class="text-center">Amount Per KM Price </th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="ReimbursementTraveltable"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
               
            <!-- Delete Reimbursement Modal -->
            <div class="modal custom-modal fade" id="delete_Reimbursementpolicy" role="dialog">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-header text-center">
                                <i class="fa fa-ban"></i>
                                <h3>Are you sure want to delete?</h3>
                            </div>
                            <div class="modal-btn delete-action pull-right">
                                <button type="submit" id="" class="btn btn-danger continue-btn delete_reimbursementpolicy_button">Yes delete it!</button>
                                <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Delete Reimbursement Modal -->
            <div class="tab-pane fade" id="addnew" role="tabpanel">
                <div class="card">
                    <div class="card-body">
                        <form id="addReimbursementPloicy" method="POST" action="#">
                            <div class="row clearfix">
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Policy Name<span class="text-danger"> *</span></label><br />
                                    <span id="addpolicynamevalid" class="text-danger change-pos"></span>
                                    <input name="addpolicyname" id="addpolicyname" class="form-control" type="text" placeholder="Name of the policy that will be displayed." autocomplete="off" />
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Travelling Type<span class="text-danger"> *</span></label><br />
                                    <span id="addtravellingtypevalid" class="text-danger change-pos"></span>
                                    
                                    <input name="addtravellingtype" id="addtravellingtype"  class="form-control" type="text" placeholder="Enter travelling type." autocomplete="off" />
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Amount Per KM Price<span class="text-danger"> *</span></label><br />
                                    <span id="addkmpricevalid" class="text-danger change-pos"></span>
                                    <input type="text" name="addkmprice" id="addkmprice" class="form-control" placeholder="Please enter amount" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" />
                                    
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                                    <button type="submit" id="addpolicybutton" class="btn btn-primary">Add Policy</button>
                                    <button type="reset" id="test" class="btn btn-default">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
             <div class="tab-pane fade" id="addhotelnew" role="tabpanel">
                <div class="card">
                    <div class="card-body">
                        <form id="addReimbursementPloicyhotel" method="POST">
                        <div class="row clearfix">
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Policy Name<span class="text-danger"> *</span></label><br />
                                    <span id="addpolicynamevalid" class="text-danger change-pos"></span>
                                    <input name="addpolicynamefood" id="addpolicynamefood" class="form-control" type="text" placeholder="Name of the policy that will be displayed." autocomplete="off" />
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Type<span class="text-danger"> *</span></label><br />
                                    <span id="addtravellingtypevalid" class="text-danger change-pos"></span>
                                    
                                    <select class="form-control" name="foodtype" id="foodtype">
                                    <option selected="true" disabled="">Select Type</option>
                                    <option value="Day">Day</option>
                                    <option value="Night">Night</option>
                                   </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Price<span class="text-danger"> *</span></label><br />
                                    <span id="addkmpricevalid" class="text-danger change-pos"></span>
                                    <input type="text" name="food_price" id="food_price" class="form-control" placeholder="Please enter amount" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" />
                                    
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                                    <button type="submit" id="addpolicyfoodbutton" class="btn btn-primary">Add Policy</button>
                                    <button type="reset" id="test" class="btn btn-default">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edit Department Modal -->
<div id="edit_policy" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Travelling Policy</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editpolicyform" method="POST">
                    <div class="form-group col-md-12">
                        <label>Policy Name<span class="text-danger"> </span></label>
                        <span id="editpolicynamevalid" class="text-danger change-pos"></span>
                        <input type="text" value="" id="editpolicyname" name="editpolicyname" class="form-control" placeholder="Please enter policy name." autocomplete="off" />
                        <input value="" id="editpolicyid" name="editpolicyid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                    <span id="edittravellingtypevalid" class="text-danger change-pos"></span>
                        <label>Travelling Type<span class="text-danger"> </span></label>
                        <input type="text" value="" id="edittravellingtype" name="edittravellingtype" class="form-control" placeholder="Please enter travelling type." autocomplete="off" />
                    </div>
                    <div class="form-group col-md-12">
                    <span id="editamountvalid" class="text-danger change-pos"></span>
                        <label>Amount Per KM Price<span class="text-danger"> </span></label>
                        <input type="text" value="" id="editamount" name="editamount" class="form-control" placeholder="Please enter amount." autocomplete="off" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="addpolicyeditbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Department Modal -->

<!-- Delete Travelling Policy Modal -->
<div class="modal custom-modal fade" id="delete_travelling" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button class="btn btn-danger continue-btn delete_travelling_button">Yes delete it!</button>
                    <button data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Travelling Policy Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {
    $("#test").click(function () {
        jQuery("#addpolicynamevalid").text("");
        jQuery("#addtravellingtypevalid").text("");
        jQuery("#addkmpricevalid").text("");
    });
});



// View reimbursement_policy_travel 
$.ajax({
        url: base_url + "viewReimbursementTravelPolicy",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        //var j = 1;
        var table = document.getElementById("ReimbursementTraveltable");
        for (i in response.data) {
            const d = new Date(response.data[0].created_at);
            const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
            const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
            const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
            endDate = `${da} ${mo} ${ye}`;
            var tr = document.createElement("tr");
            tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
                '<td class="text-center">' +
                response.data[i].policy_name +
                "</td>" +
                '<td class="text-center" style="white-space: break-spaces;">' +
                response.data[i].type +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].per_km +
                "</td>" +
                '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_policy" aria-expanded="false" id="' + response.data[i].id + '"  class="btn btn-primary edit_data" title="Edit Travelling Policy"><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_travelling" aria-expanded="false" id="' + response.data[i].id + '"  class="btn btn-danger delete_data" title="Delete Travelling Policy"><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr);
        }
        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#ReimbursementPolicytrav").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Reimbursement Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Reimbursement Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2 ]
                    }
                }
            ]
        });
    });

    // View reimbursement food Policy 
    $.ajax({
        url: base_url + "viewReimbursementHotel",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        //var j = 1;
        var table = document.getElementById("ReimbursementHoteltable");
        for (i in response.data) {
            const d = new Date(response.data[0].created_at);
            const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
            const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
            const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
            endDate = `${da} ${mo} ${ye}`;
            var tr = document.createElement("tr");
            tr.innerHTML = 
                '<td class="text-center">' +
                response.data[i].policy_name +
                "</td>" +
                '<td class="text-center" style="white-space: break-spaces;">' +
                response.data[i].type +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].price +
                "</td>" +
                '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_policy_food" aria-expanded="false" id="' + response.data[i].id + '"  class="btn btn-primary edit_data_food" title="Edit Travelling Policy"><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_food" aria-expanded="false" id="' + response.data[i].id + '"  class="btn btn-danger delete_data" title="Delete Travelling Policy"><i class="fa fa-trash-alt"></i></button></td>';
            table.appendChild(tr);
        }
        $("#ReimbursementPolicyh").DataTable();
    });


    //Edit  Department Form Fill
    $(document).on("click", ".edit_data", function () {
        var travel_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewReimbursementTravelPolicy",
            method: "POST",
            data: {
                travel_id: travel_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editpolicyid").val(response["data"][0]["id"]);
                $("#editpolicyname").val(response["data"][0]["policy_name"]);
                $("#edittravellingtype").val(response["data"][0]["type"]);
                $("#editamount").val(response["data"][0]["per_km"]);
                $("#edit_policy").modal("show");
            },
        });
    });

    //Edit Department form
    $("#editpolicyform").submit(function (e) {
        var formData = {
            travel_id: $("input[name=editpolicyid]").val(),
            policy_name: $("input[name=editpolicyname]").val(),
            type: $("input[name=edittravellingtype]").val(),
            per_km: $("input[name=editamount]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editReimbursementTravel",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonData = response;
            var falseData = response["data"];
            var successData = response["data"]["message"];
            if (jsonData["data"]["status"] == "1") {
                toastr.success(successData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>ReimbursementPolicy";
                }, 1000);
            } else {
                toastr.error(falseData);
                $("#editpolicyform [type=submit]").attr("disabled", false);
            }
        });
    });

    // Delete Leave Type
    $(document).on("click", ".delete_data", function () {
        var travel_id = $(this).attr("id");
        $(".delete_travelling_button").attr("id", travel_id);
        $("#delete_travelling").modal("show");
    });
    $(document).on("click", ".delete_travelling_button", function () {
        var travel_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteReimbursementTravel",
            method: "POST",
            data: {
                travel_id: travel_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>ReimbursementPolicy";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_travelling_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    $(document).ready(function () {
        $("#addpolicybutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addpolicyname").val().trim() == "") {
                $("#addpolicyname").focus();
                $("#addpolicynamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addpolicynamevalid").text("");
            }

            if ($("#addtravellingtype").val().trim() == "") {
                $("#addtravellingtype").focus();
                $("#addtravellingtypevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addtravellingtypevalid").text("");
            }

            if ($("#addkmprice").val().trim() == "") {
                $("#addkmprice").focus();
                $("#addkmpricevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#addkmpricevalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });


    $(document).ready(function () {
        $("#addpolicyeditbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editpolicyname").val().trim() == "") {
                $("#editpolicyname").focus();
                $("#editpolicynamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editpolicynamevalid").text("");
            }

            if ($("#edittravellingtype").val().trim() == "") {
                $("#edittravellingtype").focus();
                $("#edittravellingtypevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#edittravellingtypevalid").text("");
            }
            if ($("#editamount").val().trim() == "") {
                $("#editamount").focus();
                $("#editamountvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editamountvalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
    
    // Add Hotel Policy
    $("#addReimbursementPloicyhotel").submit(function (e) {
        var formData = {
            policy_name: $("input[name=addpolicynamefood]").val(),
            type: $("select[name=foodtype]").val(),
            price: $("input[name=food_price]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addReimbursementhotelPolicy",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>ReimbursementPolicy";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addReimbursementPloicyhotel [type=submit]").attr("disabled", false);
            }
        });
    });
    // Add Tarvel Policy
    $("#addReimbursementPloicy").submit(function (e) {
        var formData = {
            type: $("input[name=addtravellingtype]").val(),
            policy_name: $("input[name=addpolicyname]").val(),
            per_km: $("input[name=addkmprice]").val(),
        };

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addReimbursementTravelPolicy",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];

            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>ReimbursementPolicy";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addReimbursementPloicy [type=submit]").attr("disabled", false);
            }
        });
    });
</script>
