<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Department</li>
            </ul>
        </div>
      
        <!-- Add Department Form -->
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Department</strong></h3>
            </div>
            <div class="card-body">
                <form id="adddepartment" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Branch Name<span class="text-danger"> *</span></label><br />
                            <span id="addbranchvalidate" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addbranchid" id="addbranchid"> </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Department Name<span class="text-danger"> *</span></label><br />
                            <span id="adddepartmentvalidate" class="text-danger change-pos"></span>
                            <input type="text" name="departmentname" id="departmentname" class="form-control" placeholder="Please enter department name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12 button-open">
                            <button id="adddepartmentbutton" class="btn btn-success submit-btn">Submit</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
    

        <!-- Search Department Result Table -->
            <div class="card-header">
                <h3 class="card-title"><strong>Search By Branch Result</strong></h3>
            </div>
            <div class="card-body">
                <form id="filter-form" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Select Branch<span class="text-danger"> *</span> </label>
                            <span id="findbranchvalidate" class="text-danger change-pos-open"></span>
                            <select name="findbranch" id="findbranch" class="form-control custom-select"> </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12 button-open">
                            <button id="filterdepartmentbutton" class="btn btn-success">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style table-responsive table-responsive-md table-responsive-sm" id="departmenttable">
                    <thead>
                        <tr>
                            <th class="text-center">Department</th>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Branch Address</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodydepartment"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Edit Department Modal -->
<div id="edit_department" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Department</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editdepartment" method="POST">
                    <div class="form-group col-md-12">
                        <label>Branch Name<span class="text-danger"> *</span></label>
                        <br />
                        <span id="editbranchvalidate" class="text-danger change-pos"></span>
                        <select value="" class="custom-select form-control" name="editbranchid" id="editbranchid"> </select>
                        <input value="" id="editdepartmentid" name="editdepartmentid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Department Name<span class="text-danger"> *</span></label>
                        <br />
                        <span id="editdepartmentvalidate" class="text-danger change-pos"></span>
                        <input type="text" value="" id="editdepartmentname" name="editdepartmentname" class="form-control" placeholder="Please enter department name." autocomplete="off" />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editdepartmentbutton" class="btn btn-success submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Department Modal -->

<!-- Delete Department Modal -->
<div class="modal custom-modal fade" id="delete_department" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button class="btn btn-danger continue-btn delete_department_button">Yes delete it!</button>
                    <button data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Department Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script>

    //for remove validation and empty input field
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#departmentname").val("");
            $("textarea#remark").val("");
            $("span#addbranchvalidate").prop("");
            $("#addbranchid option:eq(0)").prop("selected", true);
        });

        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });

        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addbranchvalidate").text("");
            jQuery("#adddepartmentvalidate").text("");
            jQuery("#findbranchvalidate").text("");
            jQuery("#editbranchvalidate").text("");
            jQuery("#editdepartmentvalidate").text("");
        });
    });

    $(function () {
        filterDepartment("all");
        $("#filter-form").submit();
    });

    function filterDepartment() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            branch_id = $("#findbranch").val();
            //alert(branch_id);
            if (branch_id == null) {
                branch_id = "all";
            }
            req = {};
            req.branch_id = branch_id;
            $.ajax({
                url: base_url + "viewDepartment",
                data: req,
                type: "POST",
                dataType: "json", // what type of data do we expect back from the server
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                $("#departmenttable").DataTable().clear().destroy();
                var table = document.getElementById("tablebodydepartment");
                for (i in response.data) {
                    var tr = document.createElement("tr");
                    tr.innerHTML =
                        '<td class="text-center">' +
                        response.data[i].department_name +
                        "</td>" +
                        '<td class="text-center">' +
                        response.data[i].branch_name +
                        "</td>" +
                        '<td class="text-center" style="white-space:break-spaces;">' +
                        response.data[i].branch_location +
                        "</td>" +
                        '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_department" aria-expanded="false" class="btn btn-info edit_data" title="Edit Department" id="' +
                        response.data[i].department_id +
                        '"><i class="fa fa-edit"></i></button> <button type="button" data-toggle="modal" data-target="#delete_department" aria-expanded="false" class="btn btn-danger delete_data" id="' +
                        response.data[i].department_id +
                        '" title="Delete Department"><i class="fa fa-trash-alt"></i></button></td>';
                    table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#departmenttable").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Department Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Department Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                });
            });
        });
    }

    
    //Select branch by api
      $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Add  Department list
    $("#adddepartment").submit(function (e) {
        var formData = {
            branch_id: $("select[name=addbranchid]").val(),
            department_name: $("input[name=departmentname]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "addDepartment",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Department";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#adddepartment [type=submit]").attr("disabled", false);
            }
        });
    });

    //Select Filter branch by api
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#findbranch");

        dropdown.empty();
        dropdown.append('<option selected="true" value="all">All Branch</option>');
        dropdown.prop("selectedIndex", 0);

        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    //Branch Fetch for department edit
    function fetchBranch(branch_id) {
        $.ajax({
            url: base_url + "viewactiveBranch",
            method: "POST",
            data: {},
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#editbranchid");

            dropdown.empty();

            dropdown.append("<option disabled>Choose Branch</option>");
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                if (branch_id == entry.id) {
                    dropdown.append($('<option selected="true"></option>').attr("value", entry.id).text(entry.branch_name));
                } else {
                    dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
                }
            });
        });
    }

    //Edit  Department Form Fill
    $(document).on("click", ".edit_data", function () {
        var department_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewDepartment",
            method: "POST",
            data: {
                department_id: department_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                var branch_id = response["data"][0]["branch_id"];
                fetchBranch(branch_id);
                $("#editdepartmentid").val(response["data"][0]["department_id"]);
                $("#editdepartmentname").val(response["data"][0]["department_name"]);
                $("#edit_department").modal("show");
            },
        });
    });

    //Edit Department form
    $("#editdepartment").submit(function (e) {
        var formData = {
            branch_id: $("select[name=editbranchid]").val(),
            department_id: $("input[name=editdepartmentid]").val(),
            department_name: $("input[name=editdepartmentname]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editDepartment",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonData = response;
            var falseData = response["data"];
            var successData = response["data"]["message"];
            if (jsonData["data"]["status"] == "1") {
                toastr.success(successData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Department";
                }, 1000);
            } else {
                toastr.error(falseData);
                $("#editdepartment [type=submit]").attr("disabled", false);
            }
        });
    });

    //Delete Department
    $(document).on("click", ".delete_data", function () {
        var department_id = $(this).attr("id");
        $(".delete_department_button").attr("id", department_id);
        $("#delete_department").modal("show");
    });

    $(document).on("click", ".delete_department_button", function () {
        var department_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteDepartment",
            method: "POST",
            data: {
                department_id: department_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>Department";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_department_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    //Add Department Validation
    $(document).ready(function () {
        $("#adddepartmentbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addbranchid").val() == null) {
                $("#addbranchid").focus();
                $("#addbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#addbranchvalidate").text("");
            }

            if ($("#departmentname").val().trim() == "") {
                $("#adddepartmentvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#adddepartmentvalidate").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Edit Department Validation
    $(document).ready(function () {
        $("#editdepartmentbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editbranchid").val() == null) {
                $("#editbranchid").focus();
                $("#editbranchvalidate").text("Please select a branch name.");
                errorCount++;
            } else {
                $("#editbranchvalidate").text("");
            }
            if ($("#editdepartmentname").val().trim() == "") {
                $("#editdepartmentvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editdepartmentvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Filter Department Validation
    $(document).ready(function () {
        $("#filterdepartmentbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#findbranch").val() == null) {
                $("#findbranch").focus();
                $("#findbranchvalidate").text("Please select a branch.");
                errorCount++;
            } else {
                $("#findbranchvalidate").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
