<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Branch</li>
            </ul>
            <div class="header-action mt-3">
                <a class="btn btn-primary" href="<?php echo base_url(); ?>BranchAdd"><i class="fa fa-plus mr-2"></i>Add Branch</a>
            </div>
        </div>

        <!-- Search Branch Result in Table-->
        <div class="card mt-3 box_shadow">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Branch Result</strong></h3>
                <div class="card-options">
                    <button type="button" data-toggle="modal" data-target="#branchbulk_upload" aria-expanded="false" class="btn btn-info" title="Bulk Upload">Bulk Upload</button>
                </div>
            </div>

            <div class="card-body">
                <form id="filter-form">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Select Branch Status<span class="text-danger"> *</span></label>
                            <select class="form-control custom-select" id="status">
                                <option selected value="4">All</option>
                                <option value="1">Active</option>
                                <option value="o">Inactive</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>

            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style table-responsive table-responsive-md" id="branchtable">
                    <thead>
                        <tr>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Branch Address</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodybranch"></tbody>
                </table>
            </div>
        </div>
        <!-- /Search Branch Result in Table-->
    </div>
</div>

<!-- Edit Branch Modal -->
<div class="modal custom-modal fade" id="edit_branch" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Branch</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editbranch" method="POST">
                    <div class="form-group col-md-12">
                        <label>Branch Name <span class="text-danger">*</span></label>
                        <br />
                        <span class="text-danger change-pos" id="editbranchnamevalidate"></span>
                        <input type="text" value="" id="editbranchname" name="editbranchname" class="form-control" placeholder="Please enter branch name." autocomplete="off" />
                        <input value="" id="editbranchid" name="editbranchid" class="form-control" type="hidden" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Branch Location <span class="text-danger">*</span></label>
                        <br />
                        <span class="text-danger change-pos" id="editbranchlocationvalidate"></span>
                        <input type="text" value="" id="editbranchlocation" name="editbranchlocation" class="form-control" placeholder="Please enter branch location." autocomplete="off" />
                    </div>
                    <div class="form-group col-md-6 col-md-12">
                        <label>Branch Latitude <span id="editbranchlatvalidate" class="text-danger"></span></label>
                        <input type="text" value="" id="lat" name="lat" class="form-control" placeholder="Please enter branch name." />
                    </div>
                    <div class="form-group col-md-6 col-md-12">
                        <label>Branch Longitude <span id="editbranchlngvalidate" class="text-danger"></span></label>
                        <input type="text" value="" id="lng" name="lng" class="form-control" placeholder="Please enter branch name." />
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editbranchbutton" class="btn btn-success submit-btn box_shadow">Update Changes</button>
                        <button type="button" class="btn btn-secondary box_shadow" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Branch Modal -->

<!-- Bulk Upload Modal -->
<div id="branchbulk_upload" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bulk Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addBulkBranch" method="POST" action="#">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Csv<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="imagevalid"></span>
                            <input type="file" class="dropify form-control" name="image" id="image" onchange="load_file();" data-allowed-file-extensions="csv" data-max-file-size="5MB" />
                            <input type="hidden" class="form-control text-ellipsis" id="12arrdata" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success submit-btn">Yes!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Bulk Upload Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    function load_file() {
        if (!window.FileReader) {
            return alert("FileReader API is not supported by your browser.");
        }
        var $i = $("#image"), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
        if (input.files && input.files[0]) {
            file = input.files[0]; // The file
            fr = new FileReader(); // FileReader instance
            fr.onload = function () {
                // Do stuff on onload, use fr.result for contents of file
                $("#12arrdata").val(fr.result);
            };
            fr.readAsDataURL(file);
        }
        else {
            alert("File not selected or browser incompatible.");
        }
    }

    // Add Branch Bulk Uopoad
    $("#addBulkBranch").submit(function (e) {
        var formData = {
            csvFile: $("#12arrdata").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST", // define the type of HTTP verb we want to use (POST for our form)
            url: base_url + "addBranchBulk", // the url where we want to POST
            data: formData, // our data object
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonData = response;
            var falseData = response["data"];
            var successData = "Branch Bulk Uploaded Successfully.";
            if (jsonData["data"] == null) {
                toastr.success(successData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>Branch";
                }, 1000);
            }
            else {
                toastr.error(falseData);
                $("#addBulkBranch [type=submit]").attr("disabled", false);
            }
        });
    });

    //for remove validation 
    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#editbranchnamevalidate").text("");
            jQuery("#editbranchlocationvalidate").text("");
        });
    });

    $(function () {
        filterCompanies("4");
        $("#status").on("change", function () {
            status = $(this).val();
            filterCompanies(status);
        });
    });

    function filterCompanies(status) {
        req = {};
        req.status = status;
        $.ajax({
            url: base_url + "viewBranch",
            data: req,
            type: "POST",
            dataType: "json",
            timeout: 6000,
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            $("#branchtable").DataTable().clear().destroy();
            $("#branchtable tbody").empty();
            var table = document.getElementById("tablebodybranch");
            for (i in response.data) {
                var testcheck = "";
                if (response.data[i].is_active == 1) {
                    testcheck = "checked";
                }
                if (response.data[i].status == 0) {
                    testcheck = "";
                }
                var status = "";
                if (response.data[i].is_active == 0) {
                    status = "Inactive";
                }
                if (response.data[i].is_active == 1) {
                    status = "Active";
                }
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td class="text-center">' +response.data[i].branch_name + '</td>' +

                    '<td class="text-center" style="white-space:break-spaces;">' + response.data[i].branch_location + '</td>' +

                    '<td class="text-center"> <div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox" ' + testcheck + ' id="' + response.data[i].id + '" value="' + response.data[i].is_active + '" onclick="getstatus(' + response.data[i].id + ')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>' +
                    
                    '<td class="text-center"><button type="button" data-toggle="modal" data-target="#edit_branch" aria-expanded="false" class="btn btn-info edit_data box_shadow" title="Edit Branch" id="' + response.data[i].id + '"><i class="fa fa-edit"></i></button> </td>';
                table.appendChild(tr);
            }

            var currentDate = new Date()
            var day = currentDate.getDate()
            var month = currentDate.getMonth() + 1
            var year = currentDate.getFullYear()
            var d = day + "-" + month + "-" + year;
            $("#branchtable").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: d+ ' Branch Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: d+ ' Branch Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    }
                ]
            });
        });
    }
    
    /*-----------------------------Inactive Button------------------------------------*/
    function getstatus(id) {
        var branch_id = id;
        var is_active = $("#" + id).val();
        console.log(status);
        if (is_active == 0) {
            var is_active = 1;
            $.ajax({
                type: "POST",
                url: base_url + "editBranchActive",
                data: "branch_id=" + branch_id + "&is_active=" + is_active,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Branch";
                }, 1000);
            });
        } 
        else {
            var is_active = 0;
            $.ajax({
                type: "POST",
                url: base_url + "editBranchActive",
                data: "branch_id=" + branch_id + "&is_active=" + is_active,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Branch";
                }, 1000);
            });
        }
    }


    //Edit  Branch Form Fill
    $(document).on("click", ".edit_data", function () {
        var branch_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewBranch",
            method: "POST",
            data: {
                branch_id: branch_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editbranchname").val(response["data"][0]["branch_name"]);
                $("#editbranchid").val(response["data"][0]["id"]);
                $("#editbranchlocation").val(response["data"][0]["branch_location"]);
                $("#lat").val(response["data"][0]["branch_lat"]);
                $("#lng").val(response["data"][0]["branch_long"]);
                $("#edit_branch").modal("show");
            },
        });
    });

    //Edit Branch Form
    $("#editbranch").submit(function (e) {
        var formData = {
            branch_id: $("input[name=editbranchid]").val(),
            branch_name: $("input[name=editbranchname]").val(),
            branch_location: $("input[name=editbranchlocation]").val(),
            branch_lat: $("input[name=lat]").val(),
            branch_long: $("input[name=lng]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editBranch",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Branch";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editbranch [type=submit]").attr("disabled", false);
            }
        });
    });

    //Delete Branch
    $(document).on("click", ".delete_data", function () {
        var branch_id = $(this).attr("id");
        $(".delete_branch_button").attr("id", branch_id);
        $("#delete_branch").modal("show");
    });
    $(document).on("click", ".delete_branch_button", function () {
        var branch_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteBranch",
            method: "POST",
            data: {
                branch_id: branch_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                window.location.replace("<?php echo base_url()?>Branch");
            },
        });
    });

    //Edit Branch Validation form
    $(document).ready(function () {
        $("#editbranchbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editbranchname").val().trim() == "") {
                $("#editbranchname").focus();
                $("#editbranchnamevalidate").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#editbranchnamevalidate").text("");
            }
            if ($("#editbranchlocation").val().trim() == "") {
                $("#editbranchlocation").focus();
                $("#editbranchlocationvalidate").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#editbranchlocationvalidate").text("");
            }
            if ($("#lat").val().trim() == "") {
                $("#lat").focus();
                $("#editbranchlatvalidate").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#editbranchlatvalidate").text("");
            }
            if ($("#lng").val().trim() == "") {
                $("#lng").focus();
                $("#editbranchlngvalidate").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#editbranchlngvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=loadMap"></script>
<script>
    $(document).ready(function () {
        $("#editbranchlocation").keyup(function () {
            var address = $(this).val();
            var url = "https://maps.googleapis.com/maps/api/geocode/json";
            var key = "AIzaSyB6SeEWVn2z0Phkp5gnlxAe-wSs2lzvAck"; // not real
            jQuery.ajax({
                type: "GET",
                dataType: "json",
                url: url + "?address=" + address + "&key=" + key,
                success: function (data) {
                    if (data.results.length) {
                        jQuery("#lat").val(data.results[0].geometry.location.lat);
                        jQuery("#lng").val(data.results[0].geometry.location.lng);
                    } else {
                        jQuery("#lat").val("");
                        jQuery("#lng").val("");
                    }
                },
            });
        });
    });
    var today = new Date();
    var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
</script>
