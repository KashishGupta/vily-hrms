<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Notification</li>
            </ul>
        </div>

        <!-- Search Notification -->
        <div class="card mt-3 ">
            <div class="card-header">
                <h3 class="card-title"><strong>Notification</strong></h3>
            </div>
            <div class="card-body-open">
                <ul class="right_chat list-unstyled Kt-OpentestNotification">
                                                
                </ul>
            </div>
        </div>
        <!-- /Search Payroll Salary -->
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">

    // View Notification Public
    $.ajax({
        url: base_url + "viewNotificationRaead",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-xl-12"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div>';
            $(".kt-OpentestNotification").append(html);
        } else {
        for (i in response.data) {
            var html ='<li class="online"> <a href="javascript:void(0);"> <div class="media"> <img class="media-object " src="<?php echo base_url(); ?>assets/images/avatar1.jpg" alt=""> <div class="media-body"> <span class="name">' +  response.data[i].sender_name + ' <small class="float-right">'+timeago(response.data[i].created_at)+'</small></span> <span class="message">' +
            response.data[i].message + '</span> <span class="badge badge-outline status"></span> </div> </div> </a> </li>';
            $(".Kt-OpentestNotification").append(html);
        }
    }
    });

    function timeago(stringDate)
    {
      
          var currDate = new Date();
          var diffMs=currDate.getTime() - new Date(stringDate).getTime();
        
          var sec=diffMs/1000;
         // alert(sec);
          if(sec<=60)
              return '<span style = "color:red;">just now</span>';
          var min=sec/60;
          if(min<60)
              return parseInt(min)+' minute'+(parseInt(min)>1?'s ago':' ago');
          var h=min/60;
          if(h<=24)
              return (parseInt(h)>1? parseInt(h) +' hrs ago': ' an hour ago');
          var d=h/24;
          if(d<=7)
              return parseInt(d)+' day'+(parseInt(d)>1?'s ago':' ago');
          var w=d/7;
          if(w<=4.3)
          	  return (parseInt(w)>1? parseInt(w)+' weeks ago': ' a week ago');
          var m=d/30;
          if(m<=12)
              return parseInt(m)+' month'+(parseInt(m)>1?'s ago':' ago');
          var y=m/12;
          return parseInt(y)+' year'+(parseInt(y)>1?'s ago':' ago');
   }                 
    
</script>