<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Leave Type</li>
            </ul>
        </div>
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Edit Leave Policy</strong></h3>
            </div>
            <div class="card-body">
                <form id="editleavePloicy"  method="POST">
                    <div class="row clearfix">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Policy Name<span class="text-danger"> *</span></label><br>
                            <span id="addpolicynamevalid" class="text-danger change-pos"></span>
                            
                            <input name="editPolicyId" id="editPolicyId" class="form-control" type="hidden">
                            <input name="editPolicyName" id="editPolicyName" class="form-control" type="text" placeholder="Name of the policy that will be displayed." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Leave Type<span class="text-danger"> *</span></label><br>
                            <span id="addleavetypevalid" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="editleaveType" id="editleaveType"></select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-label">Is Earned Leave</div>
                                    <div class="custom-controls-stacked" style="margin-top: 19px;">
                                        <label class="custom-control custom-radio custom-control-inline">
                                        <?php   $query56 = $this->db->query("SELECT * FROM `leave_policy` WHERE id ='".$policy_id."'");
                                $arrProjectsData56 = $query56->row();
                                $test = $arrProjectsData56->check_leave_type;
                                if($test == 1){
                                $checkreset56= 'checked';
                                }else{
                                $checkreset56= 'uncheck';
                                } 
                                ?>
                                            <input type="checkbox" id="isEarned" name="isEarned" <?php echo $checkreset56;?> class="custom-switch-input" />
                                            <span class="custom-switch-indicator"></span>
                                        </label>
                                    </div>
                                    <p class="font-12">Can leaves be Earned Leave employee.</p>
                                </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Total Per Year<span class="text-danger"> *</span></label><br>
                            <span id="addtotalperyearvalid" class="text-danger change-pos"></span>
                            <input name="edittotalperyear" id="edittotalperyear" onkeypress="return onlyNumberKey(event)" class="form-control" type="text" placeholder="Number of leaves allotted per year according to policy." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Description<span class="text-danger"> </span></label><br>
                            <span id="addtotalperyearvalid" class="text-danger change-pos"></span>
                            <input name="editdescription" id="editdescription" class="form-control" type="text" placeholder="Add Description (optional)" autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Apply before days<span class="text-danger"> </span></label><br>
                            <span id="addapplybeforevalid" class="text-danger change-pos"></span>
                            <input name="editapplyBeforeDays" id="editapplyBeforeDays" class="form-control" type="text" onkeypress="return onlyNumberKey(event)" placeholder="Min. no of days of gap from the date of leave application to the first date of applicable leave." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Accrual Method <span class="text-danger"> *</span></label><br>
                            <span id="addaccuralvalid" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="editaccrualname" id="editaccrualname"></select>
                        </div>

                      <!--  <div class="form-group col-lg-4 col-md-6 col-sm-12" id="hideshow">
                            <label>Accrual Period<span class="text-danger"> </span></label><br />
                            <span id="addaccuralperiodvalid" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addaccuralperiod" id="addaccuralperiod">
                            <option value="">Select Accrual Period</option>
                                <option value="Weekly">Weekly</option>
                                <option value="Monthly">Monthly</option>
                            </select>
                        </div>-->
                       
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Is Accrual Fixed</div>
                            <div class="custom-controls-stacked" style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                    <input type="checkbox" id="editAccuralFixed" name="editAccuralFixed" checked onclick="return false;" onkeydown="return false;"  class="custom-switch-input" readonly="" checked="">
                                <span class="custom-switch-indicator"></span>
                                </label>
                            </div>
                        </div>
                        <style>
                        
                        </style>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Is Accrual reset</div>
                            <div class="custom-controls-stacked"  style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                <?php   $query = $this->db->query("SELECT * FROM `leave_policy` WHERE id ='".$policy_id."'");
                                $arrProjectsData = $query->row();
                                $test = $arrProjectsData->is_accrual_reset;
                                if($test == 1){
                                $checkreset= 'checked';
                                }else{
                                $checkreset= 'uncheck';
                                } 
                                ?>
                                <input type="checkbox" id="isAccuralreset" name="isAccuralreset"<?php echo $checkreset?>  class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                </label>
                                
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Is Accrual at beginning</div>
                            <div class="custom-controls-stacked"  style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                <?php   $query2 = $this->db->query("SELECT * FROM `leave_policy` WHERE id ='".$policy_id."'");
                                $arrpolicy = $query2->row();
                                $testbegining = $arrpolicy->is_accrual_beginning;
                                if($testbegining == 1){
                                $checkbegining= 'checked';
                                }else{
                                $checkbegining= 'uncheck';
                                } 
                                ?>
                                <input type="checkbox" id="isAccrualbeginning" name="isAccrualbeginning" <?php echo $checkbegining?>  class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                </label>
                                
                            </div>
                            <p class="font-12">Does accrual happened at the beginning or at the end of the period.</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Max. carry days<span class="text-danger"> </span></label><br>
                            <span id="addapplybeforevalid" class="text-danger change-pos"></span>
                            <input name="editmaxcarrydays" id="editmaxcarrydays" class="form-control" onkeypress="return onlyNumberKey(event)" type="text" placeholder="How many days of leave can carry forward year on year." autocomplete="off">
                        </div>
                       
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Is Encashable</div>
                            <div class="custom-controls-stacked"  style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                <?php   $query3 = $this->db->query("SELECT * FROM `leave_policy` WHERE id ='".$policy_id."'");
                                $arrpolicy1 = $query3->row();
                                $testbegining1 = $arrpolicy1->is_encashable;
                                if($testbegining1 == 1){
                                $checkbegining1= 'checked';
                                }else{
                                $checkbegining1= 'uncheck';
                                } 
                                $backdate1 = $arrpolicy1->back_date;
                                if($backdate1 == 1){
                                $checkbackdate1= 'checked';
                                }else{
                                $checkbackdate1= 'uncheck';
                                } 
                                ?>
                                <input type="checkbox" id="isEncashable" name="isEncashable" <?php echo $checkbegining1?>  class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                </label>
                            </div>
                            <p class="font-12">Can leaves be encashed at end of year or on employee relieving.</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-label">Allow Back Date</div>
                                    <div class="custom-controls-stacked" style="margin-top: 19px;">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input type="checkbox" id="isBackDate" name="isBackDate" <?php echo $checkbackdate1?>  class="custom-switch-input" />
                                            <span class="custom-switch-indicator"></span>
                                        </label>
                                    </div>
                                    <p class="font-12">Can leaves be allow to back Date.</p>
                                </div>
                       <!-- <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Waiting Period Unit<span class="text-danger"></span></label><br>
                            <span id="addaccuralperiodvalid" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="editaccuralperiod" id="editaccuralperiod">
                            
                            </select>
                            <p class="font-12">If there is a wiating period then is that unit in days, weeks or months</p>
                        </div>-->
                   
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Waiting Period length<span class="text-danger"> </span></label><br>
                            <span id="addwaitingperiodvalid" class="text-danger change-pos"></span>
                            <input name="addwaitingperiod" id="addwaitingperiod" class="form-control" type="text" placeholder="Is there a waiting time before leaves start accruing" autocomplete="off">
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Is holiday excluded</div>
                            <div class="custom-controls-stacked"  style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                <?php  
                                       $query4 = $this->db->query("SELECT * FROM `leave_policy` WHERE id ='".$policy_id."'");
                                        $arrpolicy2 = $query4->row();
                                        $testbegining2 = $arrpolicy1->is_holiday_excluded;
                                        $testweekecnd = $arrpolicy1->is_weekend_excluded;
                                        $testDoj = $arrpolicy1->is_doj_considered;
                                        $testnagative = $arrpolicy1->negative_balance_allowed;
                                        if($testbegining2 == 1){
                                        $checkbegining2= 'checked';
                                        }else{
                                        $checkbegining2= 'uncheck';
                                        } 
                                        if($testweekecnd == 1){
                                        $checkweekend2= 'checked';
                                        }else{
                                        $checkweekend2= 'uncheck';
                                        } 
                                        if($testDoj == 1){
                                        $checkweekend3= 'checked';
                                        }else{
                                        $checkweekend3= 'uncheck';
                                        } 
                                        if($testnagative == 1){
                                        $checkweekend4= 'checked';
                                        }else{
                                        $checkweekend4= 'uncheck';
                                        } 
                             ?>
                                <input type="checkbox" id="isholidayexcluded" name="isholidayexcluded" <?php echo $checkbegining2?>  class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                </label>
                            </div>
                            <p class="font-12">If any company holidays fall between leave apply days, will that be counted?</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Is weekend excluded</div>
                            <div class="custom-controls-stacked"  style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                <input type="checkbox" id="isweekendexcluded" name="isweekendexcluded" <?php echo $checkweekend2?>  class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                </label>
                            </div>
                            <p class="font-12">If any weekends (as defined in attendance policy) fall between leave apply days, will that be counted?</p>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Limit Inclusion<span class="text-danger"> </span></label><br>
                            <span id="addapplybeforevalid" class="text-danger change-pos"></span>
                            <input name="editInclusion" id="editInclusion" class="form-control" onkeypress="return onlyNumberKey(event)" type="text" placeholder="Add limit inclusion." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Max. Consecutive<span class="text-danger"> </span></label><br>
                            <span id="addapplybeforevalid" class="text-danger change-pos"></span>
                            <input name="editConsecutive" id="editConsecutive" class="form-control" onkeypress="return onlyNumberKey(event)" type="text" placeholder="Maximum no. of leaves of same type that may be applied at one go." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Max. Balance Days<span class="text-danger"> </span></label><br>
                            <span id="addapplybeforevalid" class="text-danger change-pos"></span>
                            <input name="ediBalance" id="ediBalance" class="form-control" onkeypress="return onlyNumberKey(event)" type="text" placeholder="How many days of leaves can accrue in total." autocomplete="off">
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Is DOJ considered</div>
                            <div class="custom-controls-stacked"  style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                <input type="checkbox" id="isDOJconsidered" name="isDOJconsidered" <?php echo $checkweekend3?>  class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <div class="form-label">Negative Balance Allowed</div>
                            <div class="custom-controls-stacked"  style="margin-top: 19px;">
                                <label class="custom-control custom-radio custom-control-inline">
                                <input type="checkbox" id="NegativeBalanceAllowed" name="NegativeBalanceAllowed" <?php echo $checkweekend4?>  class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                </label>
                            </div>
                            <p class="font-12">Can the balance become negative (i.e. whether policy is enforced in a strict way)</p>
                        </div>
                        <div class="col-lg-12 mt-3 text-right">
                            <button type="submit" id="addleavepolicybutton" class="btn btn-primary">Update Policy</button>
                            <button type="reset" class="btn btn-default">Cancel</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #hideshow, #hideshow1{
        display: none;
    }


</style>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

	 $(function () {
        $("#editaccrualname").change(function () {
            if ($(this).val() == "Throughout Every Period") {
                $("#hideshow").show();
            } else {
                $("#hideshow").hide();
            }
        });
    });

    $(function () {
        $("#editaccuralperiod").change(function () {
            if ($(this).val() == "None") {
                $("#hideshow1").hide();
            } else {
                $("#hideshow1").show();
            }
        });
    });

//_viewaccrual_period          
function fetchAccuralperiod(accrualperiod) {
            $.ajax({  
                url: base_url+"viewaccrual_period",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
              let dropdown = $('#editaccuralperiod');
    
                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Waiting Period</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(accrualperiod == entry.period_name)
                        {
                            dropdown.append($('<option selected="true"></option>').attr('value', entry.period_name).text(entry.period_name));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.period_name).text(entry.period_name));
                        }
                    })
            }); 
        }
//Fetech Accural Type              
function fetchAccuralType(accrualname) {
            $.ajax({  
                url: base_url+"viewAccuralForLeave",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
              let dropdown = $('#editaccrualname');
    
                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Leave Type</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(accrualname == entry.id)
                        {
                            dropdown.append($('<option selected="true"></option>').attr('value', entry.id).text(entry.accrual_name));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.accrual_name));
                        }
                    })
            }); 
        }
 //Fetech Leave Type              
 function fetchLeaveType(leave_type_id) {
            $.ajax({  
                url: base_url+"viewLeaveType",  
                method:"POST",  
                data:{},  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                }
            })
            .done(function(response){
              let dropdown = $('#editleaveType');
    
                dropdown.empty();
                
                dropdown.append('<option disabled>Choose Leave Type</option>');
                dropdown.prop('selectedIndex', 0);
                
                // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        if(leave_type_id == entry.id)
                        {
                            dropdown.append($('<option selected="true"></option>').attr('value', entry.id).text(entry.leave_type));
                        }
                        else
                        {
                            dropdown.append($('<option></option>').attr('value', entry.id).text(entry.leave_type));
                        }
                    })
            }); 
        }

        
// Edit  Leave Policy Form Fill */
var leave_policy_id = <?php echo $policy_id; ?>;
        //alert(job_id);
            $.ajax({
                url: base_url+"viewLeavePloicies",
                data:{leave_policy_id:leave_policy_id}, 
                type: "POST",
                dataType    : 'json', 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                },
          
               success:function(response){
              
                var accrualperiod = response["data"][0]["waiting_period"];
                var leave_type_id = response["data"][0]["leave_type_id"];
                var accrualname = response["data"][0]["accural_method"];
                fetchLeaveType(leave_type_id); 
                fetchAccuralType(accrualname);
                fetchAccuralperiod(accrualperiod);
                $('#editPolicyId').val(response["data"][0]["id"]); 
                
                $('#addwaitingperiod').val(response["data"][0]["waiting_period_length"]); 
                        $('#editPolicyName').val(response["data"][0]["policy_name"]);
                     
                        
                        $('#edittotalperyear').val(response["data"][0]["total_per_year"]); 
                        $('#editdescription').val(response["data"][0]["description"]); 
                        $('#editapplyBeforeDays').val(response["data"][0]["apply_before_days"]); 
                        $('#editmaxcarrydays').val(response["data"][0]["max_carry_days"]); 
                        $('#editInclusion').val(response["data"][0]["limit_inclusion"]);
                        $('#editConsecutive').val(response["data"][0]["max_consecutive"]); 
                        $('#ediBalance').val(response["data"][0]["max_balance_days"]); 
                        
                    }  
                });  

 // Edit Policy form*/
 $("#editleavePloicy").submit(function(e) {
    if($('#isAccrualbeginning').prop("checked") == true){
       var accural = 1;
    }else{
        var accural = 0;
    }
    if($('#editAccuralFixed').prop("checked") == true){
        var accuralFixed = 1;
    }else{
        var accuralFixed = 0;
    }
    if($('#isAccuralreset').prop("checked") == true){
        var accuralreset = 1;
    }else{
        var accuralreset = 0;
    }
    if($('#isEncashable').prop("checked") == true){
        var Encashable = 1;
    }else{
        var Encashable = 0;
    }
    if($('#isholidayexcluded').prop("checked") == true){
        var holidayexcluded = 1;
    }else{
        var holidayexcluded = 0;
    }
    if($('#isweekendexcluded').prop("checked") == true){
        var weekendexcluded = 1;
    }else{
        var weekendexcluded = 0;
    }
    if($('#isDOJconsidered').prop("checked") == true){
        var DOJconsidered = 1;
    }else{
        var DOJconsidered = 0;
    }
    if($('#NegativeBalanceAllowed').prop("checked") == true){
        var NegativeBalance = 1;
    }else{
        var NegativeBalance = 0;
    }
    if ($("#isBackDate").prop("checked") == true) {
            var BackDate = 1;
        } else {
            var BackDate = 0;
        }
        if ($("#isEarned").prop("checked") == true) {
            var EarnedLeave = 1;
        } else {
            var EarnedLeave = 0;
        }
                       var formData = {
                           'leave_policy_id'          :  $('input[name=editPolicyId]').val(),
                           'total_per_year'           :  $('input[name=edittotalperyear]').val(),
                          
                           'waiting_period_length'    :  $('input[name=addwaitingperiod]').val(),
                           'leave_type_id'            :  $('select[name=editleaveType]').val(),
                           'accural_method'           :  $('select[name=editaccrualname]').val(),
                           'policy_name'              :  $('input[name=editPolicyName]').val(),
                           'description'              :  $('input[name=editdescription]').val(),
                           'apply_before_days'        :  $('input[name=editapplyBeforeDays]').val(),
                           'max_carry_days'           :  $('input[name=editmaxcarrydays]').val(),
                           'limit_inclusion'          :  $('input[name=editInclusion]').val(),
                           'max_consecutive'          :  $('input[name=editConsecutive]').val(),
                           'max_balance_days'         :  $('input[name=ediBalance]').val(),
                           is_accrual_fixed: accuralFixed,
                           is_accrual_reset: accuralreset,
                            is_accrual_beginning: accural,
                            is_encashable: Encashable,
                           is_holiday_excluded: holidayexcluded,
                           is_weekend_excluded: weekendexcluded,
                           is_doj_considered: DOJconsidered,
                          negative_balance_allowed: NegativeBalance,
                          back_date: BackDate,
                          check_leave_type :EarnedLeave,
                       };

                       e.preventDefault();
                       $.ajax({
                           type        : 'POST',
                           url         : base_url+'editLeavePolicy',
                           data        : formData, // our data object
                           dataType    : 'json',
                           encode      : true,
                           beforeSend: function(xhr){
                               xhr.setRequestHeader('Token', localStorage.token);
                           }
                       })
                       .done(function(response) {
                           console.log(response);

                           var jsonDa = response
                           var jsonData = response["data"]["message"];
                           var falsedata = response["data"];
                           if (jsonDa["data"]["status"] == "1") {
                           toastr.success(jsonData);
                               setTimeout(function(){window.location ="<?php echo base_url()?>LeavePolicy"},1000);
                           }

                           else {
                               toastr.error(falsedata);
                                   $('#editleavePloicy [type=submit]').attr('disabled',false);
                           }
                       });
                   });

           
                </script> 