<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Social </li>
            </ul>
        </div>

        <div class="row mt-3">
            <div class="col-lg-2 col-md-4 col-6">
				<div class="card w_social_state2">
					<div class="card-body">
						<div class="icon facebook"><i class="fab fa-facebook fa-2x"></i></div>
						<div class="content">
							<div class="text">Like</div>
							<h5 class="mb-0">123</h5>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-4 col-6">
			    <div class="card w_social_state2">
			        <div class="card-body">
			            <div class="icon instagram"><i class="fab fa-instagram fa-2x"></i></div>
			            <div class="content">
			                <div class="text">Followers</div>
			                <h5 class="mb-0">231</h5>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="col-lg-2 col-md-4 col-6">
			    <div class="card w_social_state2">
			        <div class="card-body">
			            <div class="icon twitter"><i class="fab fa-twitter fa-2x"></i></div>
			            <div class="content">
			                <div class="text">Followers</div>
			                <h5 class="mb-0">31</h5>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="col-lg-2 col-md-4 col-6">
			    <div class="card w_social_state2">
			        <div class="card-body">
			            <div class="icon google"><i class="fab fa-google fa-2x"></i></div>
			            <div class="content">
			                <div class="text">Like</div>
			                <h5 class="mb-0">254</h5>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="col-lg-2 col-md-4 col-6">
			    <div class="card w_social_state2">
			        <div class="card-body">
			            <div class="icon linkedin"><i class="fab fa-linkedin fa-2x"></i></div>
			            <div class="content">
			                <div class="text">Followers</div>
			                <h5 class="mb-0">2510</h5>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="col-lg-2 col-md-4 col-6">
			    <div class="card w_social_state2">
			        <div class="card-body">
			            <div class="icon behance"><i class="fab fa-behance fa-2x"></i></div>
			            <div class="content">
			                <div class="text">Project</div>
			                <h5 class="mb-0">121</h5>
			            </div>
			        </div>
			    </div>
			</div>

        </div>

        <div class="row clearfix">
            <div class="col-md-12 col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Social Media</strong></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive social_media_table">
                            <table class="table table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Media</th>
                                        <th>Name</th>
                                        <th>Like</th>                                                                                
                                        <th>Comments</th>
                                        <th>Share</th>
                                        <th>Members</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span class="social_icon linkedin"><i class="fab fa-linkedin"></i></span>
                                        </td>
                                        <td><span class="list-name">Linked In</span>
                                            <span class="text-muted">Florida, United States</span>
                                        </td>
                                        <td class="text-center">19K</td>
                                        <td class="text-center">14K</td>
                                        <td class="text-center">10K</td>
                                        <td class="text-center">
                                            <span class="badge badge-success">2341</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="social_icon twitter-table"><i class="fab fa-twitter"></i></span>
                                        </td>
                                        <td><span class="list-name">Twitter</span>
                                            <span class="text-muted">Arkansas, United States</span>
                                        </td>
                                        <td class="text-center">7K</td>
                                        <td class="text-center">11K</td>
                                        <td class="text-center">21K</td>
                                        <td class="text-center">
                                            <span class="badge badge-success">952</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="social_icon facebook"><i class="fab fa-facebook"></i></span>
                                        </td>
                                        <td><span class="list-name">Facebook</span>
                                            <span class="text-muted">Illunois, United States</span>
                                        </td>
                                        <td class="text-center">15K</td>
                                        <td class="text-center">18K</td>
                                        <td class="text-center">8K</td>
                                        <td class="text-center">
                                            <span class="badge badge-success">6127</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="social_icon google"><i class="fab fa-google-plus"></i></span>
                                        </td>
                                        <td><span class="list-name">Google Plus</span>
                                            <span class="text-muted">Arizona, United States</span>
                                        </td>
                                        <td class="text-center">15K</td>
                                        <td class="text-center">18K</td>
                                        <td class="text-center">154</td>
                                        <td class="text-center">
                                            <span class="badge badge-success">325</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="card google w_social">
                    <div class="carousel slide twitter w_feed pointer-event" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <i class="fa fa-twitter fa-2x"></i>
                                <p>23th Feb</p>
                                <h4>Now Get <span>Up to 70% Off</span><br>on buy</h4>
                                <div class="m-t-20"><i>- post form ThemeMakker</i></div>
                            </div>
                            <div class="carousel-item">
                                <i class="fab fa-twitter fa-2x"></i>
                                <p>25th Jan</p>
                                <h4>Now Get <span>50% Off</span><br>on buy</h4>
                                <div class="m-t-20"><i>- post form WrapTheme</i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card google w_social">
                    <div class="carousel slide facebook w_feed pointer-event" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item">
                                <i class="fab fa-facebook fa-2x"></i>
                                <p>20th Jan</p>
                                <h4>Now Get <span>50% Off</span><br>on buy</h4>
                                <div class="m-t-20"><i>- post form Theme</i></div>
                            </div>
                            <div class="carousel-item active">
                                <i class="fab fa-facebook fa-2x"></i>
                                <p>23th Feb</p>
                                <h4>Now Get <span>Up to 70% Off</span><br>on buy</h4>
                                <div class="m-t-20"><i>- post form Theme</i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

