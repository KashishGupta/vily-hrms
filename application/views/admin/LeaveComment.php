<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Leave">Leave</a></li>
                <li class="breadcrumb-item active">Leave Comments</li>
            </ul>
        </div>
        <div class="row clearfix mt-3">
            <div class="col-lg-3 col-md-12">
                <div class="card">
                    <div class="card-body text-center">
                        <div class="mb-3" id="fgfimage"></div>
                        <h6 class="mt-3 mb-0" id="profilenames"></h6>
                        <span id="useremail"></span>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Leave Info</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <small class="text-muted">Policy Name: </small>
                                <p class="mb-0 text-uppercase" id="policy_name"></p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">Start Date : </small>
                                <p class="mb-0" id="start_date"></p>
                            </li>
                            <li class="list-group-item">
                                <small class="text-muted">End Date: </small>
                                <p class="mb-0" id="end_date"></p>
                            </li>
                          
                            <li class="list-group-item">
                                <small class="text-muted">Days: </small>
                                <p class="mb-0" id="totalPercent_days"></p>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Leave Reason</h3>
                    </div>
                    <div class="card-body">
                        <span id="leaveReason"> </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Leave Replies</h3>
                    </div>
                    <div class="card-body ticket_comment_replay Kt-ticketComments"></div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form id="addLeaveComments" method="POST">
                            <input type="hidden" id="leave_id" name="leave_id" class="form-control" value="" />
                            <div class="form-group col-md-12">
                                <label>Comments<span class="text-danger"> *</span></label>
                                <br />
                                <span id="addcommentValidate" class="text-danger change-pos"></span>
                                <textarea class="form-control summernote" cols="3" name="addcomments" id="addcomments" placeholder="Enter text here..."></textarea>
                            </div>
                           
                            <div class="submit-section pull-right">
                                <button id="addLeaveCommentbutton" class="btn btn-success submit-btn">Add Comment</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /Edit Ticket Modal -->
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
 //Add Job Validation
 $(document).ready(function() {
            $("#addLeaveCommentbutton").click(function(e) {
                e.stopPropagation();
                var errorCount = 0;
               
                if ($("#addcomments").val().trim() == '') {
                    $("#addcommentValidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addcommentValidate").text("");
                }
                
               
                if(errorCount > 0){
                    return false;
                }
            });
        });
    var leave_id = <?php echo $leave_id; ?>;
    $('#leave_id').val(leave_id);
   // alert(leave_id);
    $.ajax({
        url: base_url + "viewLeaveComments",
        data: {leave_id: leave_id},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-xl-12 text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div>';
            $(".Kt-ticketComments").append(html);
        } 
        else {
            for (i in response.data) {
                const d = new Date(response.data[i].created_at);
                const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
                const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
                const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
                endDate = `${da} ${mo} ${ye}`;
                
                var html ='<div class="timeline_item"><img class="tl_avatar" src="'+response.data[i].user_image+'" alt="" /><span><a href="javascript:void(0);" title="">'+response.data[i].first_name+'</a> <small class="float-right text-right">'+endDate+' - '+timeago(response.data[i].created_at)+'</small></span><div class="msg">'+response.data[i].comments+'</div></div></div>';
                $(".Kt-ticketComments").append(html);
            }
        }
    });
// Add Leave Comments

       $("#addLeaveComments").submit(function (e) {
            var formData = {
                comments: $("textarea[name=addcomments]").val(),
                leave_id: $("input[name=leave_id]").val(),
            };

            e.preventDefault();
            $.ajax({
                type: "POST",
                url: base_url + "addLeaveComments",
                data: formData, // our data object
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];

                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url();?>LeaveComment/"+leave_id;
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#addLeaveComments [type=submit]").attr("disabled", false);
                }
            });
        });
//vew Leave Info

    $.ajax({
        url: base_url + "viewLeave_Where",
        method: "POST",
        data: {
            leave_id: leave_id,
        },
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            if (!$.trim(response.data[0].user_image)) {
            $("#fgfimage").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
        } 
        else {
                $("#fgfimage").append('<img class="img-thumbnail" src="' + response.data[0].user_image + '" alt="" style="width:200px; border-radius: 15px;" />');

                }
                if(response.data[0].status = 0)
                {
                    $("#ticketstatus").css({"color":"red"}).html('Inactive');
                }else{
                    $("#ticketstatus").css({"color":"green"}).html('Active');
                }
                if(response.data[0].priority  = 0)
                {
                    $("#ticketpriority").css({"color":"blue"}).html('Low');
                }else if(response.data[0].priority  = 1){
                    $("#ticketpriority").css({"color":"green"}).html('Medium');
                }else{
                    $("#ticketpriority").css({"color":"red"}).html('High');
                }

                var ticketdate = new Date(response.data[0].date);
                    var dd = String(ticketdate.getDate()).padStart(2, '0');
                    var mm = String(ticketdate.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = ticketdate.getFullYear();

                    ticketdate = dd + '-' + mm + '-' + yyyy;
                    $("#ticketDate").html(ticketdate);
                $("#profilenames").html(response.data[0].first_name);
                $("#policy_name").html(response.data[0].policy_name);
                $("#useremail").html(response.data[0].email);

                var startDate = new Date(response.data[0].start_date);
            var dd = String(startDate.getDate()).padStart(2, '0');
            var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = startDate.getFullYear();

            startDate = dd + '-' + mm + '-' + yyyy;


            var endDate = new Date(response.data[0].end_date);
            var dd = String(endDate.getDate()).padStart(2, '0');
            var mm = String(endDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = endDate.getFullYear();

            endDate = dd + '-' + mm + '-' + yyyy;
                $("#start_date").html(startDate);
                $("#end_date").html(endDate);

            var totalPercent = "";
           
           if(response.data[0].allow_short == 1)
           {
               totalPercent = response.data[0].days;
           }else if(response.data[0].allow_half == 1)
           {
               totalPercent = response.data[0].days;
           }
           else{
               var number = response.data[0].days;
               var percentToGet = 1;
               var percent =number + percentToGet;
               totalPercent = percent;
           }

                $("#totalPercent_days").html(totalPercent);
                $("#leaveReason").html(response.data[0].reason);

            },
            });
</script>