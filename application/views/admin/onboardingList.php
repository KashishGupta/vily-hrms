<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Onboarding</li>
            </ul>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs b-none">
                                <li class="nav-item">
                                    <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list"><i class="fa fa-hand-holding-water"></i> Onboarding Pending</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addnew"><i class="fa fa-check-circle"></i>Onboarding Complete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="list" role="tabpanel">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Search Onboarding Result</strong></h3>
                        <div class="card-options">
                            <a href="<?php echo base_url(); ?>onBoarding" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form id="filter-form" method="POST" action="#">
                            <div class="row">
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Branch <span class="text-danger"> *</span></label><br>
                                    <span id="addbranchvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="addbranchid" id="addbranchid" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Department<span class="text-danger"> *</span></label><br>
                                    <span id="adddepartmentidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Designation<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="designation_id" id="designation_id" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Users<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="user_id" id="user_id" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="filteremployeebutton" class="btn btn-success submit-btn">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style table-responsive table-responsive-xl list" id="onboardingtable">
                            <thead>
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-center">Gender</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodyonboarding">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="addnew" role="tabpanel">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"><strong>Search Onboarding Result</strong></h3>
                    <div class="card-options">
                        <a href="<?php echo base_url(); ?>onBoardingComplete" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                    </div>
                </div>

                <div class="card-body">
                    <form id="filter-form1" method="POST" action="#">
                        <div class="row">
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Branch <span class="text-danger"> *</span></label><br>
                                <span id="addbranchvalidate1" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="addbranchid1" id="addbranchid1" />
                                    
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Department<span class="text-danger"> *</span></label><br>
                                <span id="adddepartmentidvalidate1" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="adddepartmentid1" id="adddepartmentid1"/>
                                    
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Designation<span class="text-danger"> *</span></label><br>
                                <span id="designationidvalidate1" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="designation_id1" id="designation_id1" />
                                    
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                <label>Users<span class="text-danger"> *</span></label><br>
                                <span id="designationidvalidate1" class="text-danger change-pos"></span>
                                <select class="custom-select form-control" name="user_id1" id="user_id1" />
                                    
                                </select>
                            </div>
                            <div class="form-group text-right col-sm-12">
                                <button id="filteremployeebutton1" class="btn btn-success submit-btn">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-vcenter text-nowrap table_custom border-style table-responsive table-responsive-md list" id="onboardingtable1">
                        <thead>
                            <tr>
                                <th class="text-left">Employee Name</th>
                                <th class="text-center">Mobile</th>
                                <th class="text-center">Gender</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody id="tablebodyonboarding1">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>            
</div>

<div id="onboarding_status" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit On Boarding Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editstatus" method="POST" action="#">
                <div class="modal-body">
                    <div class="form-header">
                        <p>Are you sure want to change?</p>
                    </div>

                    <div class="form-group">
                        <input value="1" id="editstesttatus" name="editstesttatus" class="form-control" type="hidden">
                        <input value="" id="editstatusidt" name="editstatusidt" class="form-control" type="hidden">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success submit-btn">Yes Change It!</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    // Select branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select department by api*/
    $("#addbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddepartmentid");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#designation_id");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Select Designation by api*/
    $("#designation_id").change(function () {
        $.ajax({
            url: base_url + "viewUserDashboard",
            data: { designation_id: $("select[name=designation_id]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#user_id");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose User</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
            });
        });
    });

    $(function () {
        filterEmployeeDash(4);
            $("#filter-form").submit();
        });
        function filterEmployeeDash() {
            $("#filter-form").off("submit");
            $("#filter-form").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#user_id").val();
                if (user_id == null) {
                    user_id = 4;
                }
            req = {};
            req.user_id = user_id;
            //Show Employee In table
            $.ajax({
                url: base_url + "viewOnboardingUserFilter",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                $("#onboardingtable").DataTable().clear().destroy();
                    $("#onboardingtable tbody").empty();
                var table = document.getElementById("tablebodyonboarding");
                for (i in response.data) {

                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                    var status = 1;
                    var tr = document.createElement("tr");
                    tr.innerHTML =

                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                            UserImage +
                            '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                            response.data[i].user_code +
                            '" title="">' +
                        response.data[i].emp_id +
                        ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +

                        '<td class="text-center">' +
                        response.data[i].phone +
                        "</td>" +
                        '<td class="text-center">' +
                        response.data[i].gender +
                        "</td>" +
                        '<td class="text-center"><div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox"  id="' + response.data[i].id + '" value="' + status + '" onclick="getstatus(' + response.data[i].id + ')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>' +
                        '<td class="text-center"><a href="onBoardingAddDocument/' + response.data[i].id +'" class="btn btn-info" title="Add Employee Document"><i class="fa fa-folder-plus"></i></a> <a href="onBoardingAddDetails/' + response.data[i].id +'" class="btn btn-primary" title="Add Employee Details"><i class="fa fa-plus"></i></a> <a href="onBoardingView/' + response.data[i].id +'" class="btn btn-info" title="OnBoarding View"><i class="fa fa-eye"></i></a> </td>';
                    table.appendChild(tr);
                }

                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#onboardingtable").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Onboarding list Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Onboarding list Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }
                        }
                    ]
                });
            });
        });
    }
    function getstatus(id) {
        var user_id = id;
        console.log(status);

            var status = 1;
            $.ajax({
                type: "POST",
                url: base_url + "editStatusdoc",
                data: "user_id=" + user_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                console.log(response);

                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>onboardingList";
                }, 1000);
                } else {
                toastr.error(falsedata);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>onboardingList";
                }, 1000);
                }
            });
         
   
    }
   
    
</script>

<script>
    // Select branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid1");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select department by api*/
    $("#addbranchid1").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid1]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddepartmentid1");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid1").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid1]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#designation_id1");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Select Designation by api*/
    $("#designation_id1").change(function () {
        $.ajax({
            url: base_url + "viewUserDashboard",
            data: { designation_id: $("select[name=designation_id1]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#user_id1");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose User</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
            });
        });
    });

    $(function () {
        filterEmployeeDash1(4);
            $("#filter-form1").submit();
        });
        function filterEmployeeDash1() {
            $("#filter-form1").off("submit");
            $("#filter-form1").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#user_id1").val();
                if (user_id == null) {
                    user_id = 4;
                }
            req = {};
            req.user_id = user_id;
        //Show Employee In table
        $.ajax({
            url: base_url + "viewOnboardingCompleteFilter",
            data: req,
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            $("#onboardingtable1").DataTable().clear().destroy();
                $("#onboardingtable1 tbody").empty();
            var table = document.getElementById("tablebodyonboarding1");
            for (i in response.data) {

                var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
                var status = 0;
                var statusactive = "checked";
                var tr = document.createElement("tr");
                tr.innerHTML =

                    '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                        UserImage +
                        '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                        response.data[i].user_code +
                        '" title="">' +
                        response.data[i].emp_id +
                        ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +
                    
                    '<td class="text-center">' +
                    response.data[i].phone +
                    "</td>" +
                    '<td class="text-center">' +
                    response.data[i].gender +
                    "</td>" +
                    '<td class="text-center"><div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox" '+statusactive+'  id="' + response.data[i].id + '" value="' + status + '" onclick="getstatusclick(' + response.data[i].id + ')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>' +
                    '<td class="text-center"><a href="onBoardingView/' + response.data[i].id +'" class="btn btn-info" title="Add OnBoarding Document"><i class="fa fa-eye"></i></a> </td>';
                table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#onboardingtable1").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Onboarding list Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Onboarding list Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2, 3 ]
                            }
                        }
                    ]
                });
            });
        });
    }


    function getstatusclick(id) {
        var user_id = id;
        console.log(status);

            var status = 0;
            $.ajax({
                type: "POST",
                url: base_url + "editStatusdoc",
                data: "user_id=" + user_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>onboardingList";
                }, 1000);
                } else {
                toastr.error(falsedata);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>onboardingList";
                }, 1000);
                }
            });
         
   
    }
</script>