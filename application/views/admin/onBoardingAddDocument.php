<style type="text/css">
	button#onboardingStatus, button#onboardingPersonal, button#onboardingExperience, button#onboardingOther {
	    position: absolute;
	    right: 30px;
	    border: 0;
	    border-radius: 25px;
	    cursor: none;
	}
</style>

<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>onboardingList">On Boarding</a></li>
                <li class="breadcrumb-item active">Add Employee Document</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h5 class="modal-title">Select Document To Add</h5>
            </div>
            <div class="card-body">
                <div class="sc-kfGgVZ ceDrlv">
                    <div class="sc-eXEjpC jSRNhT" data-toggle="modal" data-target="#add_Offer">
                        <div class="sc-kPVwWT kQKRxC">
                            <div class="sc-dnqmqq nucGq">
                                <svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc">
                                    <path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path>
                                    <path
                                        d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z"
                                        fill="#dadcdf"
                                    ></path>
                                    <path
                                        d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z"
                                        fill="#dadcdf"
                                    ></path>
                                </svg>
                                <div>
                                    <strong>Offer letter/ Agreement</strong>
                                    <div class="sc-gzVnrw hywHFG">Send offer letters to new hires to review and sign.</div>
                                </div>
                                <button id="onboardingStatus" class="btn btn-secondary"></button>
                            </div>
                        </div>
                    </div>

                    <div class="sc-eXEjpC jSRNhT" data-toggle="modal" data-target="#add_agreements">
                        <div class="sc-kPVwWT kQKRxC">
                            <div class="sc-dnqmqq nucGq">
                                <svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc">
                                    <path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path>
                                    <path
                                        d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z"
                                        fill="#dadcdf"
                                    ></path>
                                    <path
                                        d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z"
                                        fill="#dadcdf"
                                    ></path>
                                </svg>
                                <div>
                                    <strong>Personal Documents</strong>
                                    <div class="sc-gzVnrw hywHFG">Send personal letters to new hires to review and sign.</div>
                                </div>
                                <button id="onboardingPersonal" class="btn btn-secondary"></button>
                            </div>
                        </div>
                    </div>
                    <div data-test="performance-review-doc" class="sc-eXEjpC jSRNhT" data-toggle="modal" data-target="#add_performance">
                        <div class="sc-kPVwWT kQKRxC">
                            <div class="sc-dnqmqq nucGq">
                                <svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc">
                                    <path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path>
                                    <path
                                        d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z"
                                        fill="#dadcdf"
                                    ></path>
                                    <path
                                        d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z"
                                        fill="#dadcdf"
                                    ></path>
                                </svg>
                                <div>
                                    <strong>Experience and Education</strong>
                                    <div class="sc-gzVnrw hywHFG">Send experience letters to new hires to review and sign.</div>
                                </div>
                                <button id="onboardingExperience" class="btn btn-secondary"></button>
                            </div>
                        </div>
                    </div>
                    <div class="sc-eXEjpC jSRNhT" data-toggle="modal" data-target="#add_collect">
                        <div class="sc-kPVwWT kQKRxC">
                            <div class="sc-dnqmqq nucGq">
                                <svg data-name="Layer 1" viewBox="0 0 384.04 512" class="Icon__StyledBrandIcon-k7uw4s-0 ewKPMc">
                                    <path d="M32 0A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0H32z" fill="#ededee"></path>
                                    <path
                                        d="M249.4 16L368 134.63V480a16 16 0 01-16 16H32a16 16 0 01-16-16V32a16 16 0 0116-16h217.4M256 0H32A32.1 32.1 0 000 32v448a32.1 32.1 0 0032 32h320a32.1 32.1 0 0032-32V128L256 0z"
                                        fill="#dadcdf"
                                    ></path>
                                    <path
                                        d="M288 128h96L256 0v96a32.1 32.1 0 0032 32zM304 240H80a16 16 0 110-32h224a16 16 0 110 32zM208 176H80a16 16 0 110-32h128a16 16 0 110 32zM208 112H80a16 16 0 110-32h128a16 16 0 110 32zM304 304H80a16 16 0 110-32h224a16 16 0 110 32zM304 368H80a16 16 0 110-32h224a16 16 0 110 32zM304 432H80a16 16 0 110-32h224a16 16 0 110 32z"
                                        fill="#dadcdf"
                                    ></path>
                                </svg>
                                <div>
                                    <strong>Other Document</strong>
                                    <div class="sc-gzVnrw hywHFG">Send other letters to new hires to review and sign.</div>
                                </div>
                                <button id="onboardingOther" class="btn btn-secondary"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Offer letter/Agreement Modal -->
<div id="add_Offer" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Offer letter/ Agreement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addofferletter" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-sm-12">
                            <label>Document Name <span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="offerdocumentnamevalid"></span>
                            <input type="text" id="offerdocumentname" name="offerdocumentname" class="form-control" placeholder="Please enter document name." />
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Document File <span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="resumefilevalid"></span>
                            <input type="hidden" id="editrelativeuserid" name="editrelativeuserid" />
                            <input type="file" class="dropify form-control" name="resumefile" id="resumefile" onchange="loading_file();" data-allowed-file-extensions="pdf jpg jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="23arrdata" />
                        </div>

                        <div class="col-lg-12 mt-3 text-right">
                            <button id="addofferletterbutton" class="btn btn-primary submit-btn">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Offer letter/Agreement Modal -->

<!-- Add Experience and Education Document Modal -->
<div id="add_performance" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Experience and Education</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addexperienceletter" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-12">
                            <label>Document Name<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="addexperiencedocumentnamevalid"></span>
                            <input class="form-control" type="text" id="addexperiencedocumentname" name="addexperiencedocumentname" placeholder="Please enter document name." />
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Document File<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="experirencefilevalid"></span>
                            <input type="hidden" id="editexperienceuserid" name="editexperienceuserid" />
                            <input type="file" class="dropify form-control" name="experirencefile" id="experirencefile" onchange="loadingexperience_file();" data-allowed-file-extensions="pdf jpg jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="21arrdata" />
                        </div>
                        <div class="col-lg-12 mt-3 text-right">
                            <button id="addexperienceletterbutton" class="btn btn-primary submit-btn">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Experience and Education Document Modal -->

<!-- Add Personal Documents Modal -->
<div id="add_agreements" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Personal Documents</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="personalform" method="POST" action="#">
                    <div class="row clearfix">
                        <div class="form-group col-lg-12">
                            <label>Document Name<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="documentnamevalid"></span>
                            <input class="form-control" type="text" id="documentname" name="documentname" placeholder="Please enter document name." />
                        </div>
                        <div class="form-group col-lg-12">
                            <label>Document File<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="personalfilealid"></span>
                            <input type="hidden" id="editpersonaluserid" name="editpersonaluserid" />
                            <input type="file" class="dropify form-control" name="personalfile" id="personalfile" onchange="loadingpersonal_file();" data-allowed-file-extensions="pdf jpg jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="22arrdata" />
                        </div>
                        <div class="col-lg-12 mt-3 text-right">
                            <button id="addpersonalbutton" class="btn btn-primary submit-btn">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Add Personal Documents Modal -->

<!-- Add Other Document Modal -->
<div id="add_collect" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Other Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="otherformtest" method="POST" action="">
                    <div class="row clearfix">
                        <div class="form-group col-sm-12 col-md-12">
                            <label>Document Name<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="adddocumentnamealid"></span>
                            <input class="form-control" type="text" id="adddocumentname" name="adddocumentname" placeholder="Please enter document name." />
                        </div>

                        <div class="form-group col-sm-12 col-md-12">
                            <label>Description<span class="text-danger"> *</span></label>
                            <br />
                            <span class="text-danger change-pos" id="addinstructiondocumentalid"></span>
                            <textarea
                                class="form-control"
                                type="text"
                                id="addinstructiondocument"
                                rows="2"
                                name="addinstructiondocument"
                                placeholder="Describe any requirements for this document that will be shown to team members."
                            ></textarea>
                        </div>
                        <div class="form-group col-sm-12 col-md-12">
                            <label>Upload a file<span class="text-danger"> *</span></label><br />
                            <span class="text-danger change-pos" id="addotherfilealid"></span>
                            <input type="hidden" id="addotheruserid" name="addotheruserid" />
                            <input type="file" class="dropify form-control" name="otherfile" id="otherfile" onchange="loadingother_file();" data-allowed-file-extensions="pdf jpg jpeg" data-max-file-size="10MB" />
                            <input type="hidden" id="otherarrdata" />
                        </div>
                        <div class="col-md-12 mt-10 text-right">
                            <button id="addotherletterbutton" class="btn btn-success submit-btn">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
//Select Family Relation by api
$.ajax({
    url: base_url + "viewRelationnamePublic",
    data: {},
    type: "POST",
    dataType: 'json',
    encode: true,
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Token', localStorage.token);
    }
})

.done(function(response) {
    let dropdown = $('#familyrelation');
    dropdown.empty();
    dropdown.append('<option selected="true" disabled>Select family relation</option>');
    dropdown.prop('selectedIndex', 0);

    // Populate dropdown with list of provinces
    $.each(response.data, function(key, entry) {
        dropdown.append($('<option></option>').attr('value', entry.id).text(entry.relation));
    })
});


//for remove validation and empty input field
$(document).ready(function() {
    $(".modal").click(function() {
        $("input#offerdocumentname").val("");
        $("input#resumefile").val("");

        $("input#addexperiencedocumentname").val("");
        $("input#experirencefile").val("");

        $("input#offerdocumentname").val("");
        $("input#resumefile").val("");

        $("input#documentname").val("");
        $("input#personalfile").val("");

        $("input#adddocumentname").val("");
        $("textarea#addinstructiondocument").val("");
        $("input#otherfile").val("");
    });

    $(".modal .modal-dialog .modal-body > form").click(function(e) {
        e.stopPropagation();
    });

    $("form button[data-dismiss]").click(function() {
        $(".modal").click();
    });
});

$(document).ready(function() {
    $(".modal").click(function() {
        jQuery("#offerdocumentnamevalid").text("");
        jQuery("#resumefilevalid").text("");

        jQuery("#addexperiencedocumentnamevalid").text("");
        jQuery("#experirencefilevalid").text("");

        jQuery("#documentnamevalid").text("");
        jQuery("#personalfilealid").text("");

        jQuery("#adddocumentnamealid").text("");
        jQuery("#addinstructiondocumentalid").text("");
        jQuery("#addotherfilealid").text("");

        jQuery("#familynamevalid").text("");
        jQuery("#addinstructiondocumentalid").text("");
        jQuery("#addotherfilealid").text("");
    });
});


// offer  Letter upload
function loading_file() {
    if (!window.FileReader) {
        return alert("FileReader API is not supported by your browser.");
    }
    var $i = $("#resumefile"), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
    if (input.files && input.files[0]) {
        file = input.files[0]; // The file
        fr = new FileReader(); // FileReader instance
        fr.onload = function() {
            // Do stuff on onload, use fr.result for contents of file
            $("#23arrdata").val(fr.result);
        };
        //fr.readAsText( file );
        fr.readAsDataURL(file);
    } else {
        // Handle errors here
        alert("File not selected or browser incompatible.");
    }
}

// Experience letter upload
function loadingexperience_file() {
    if (!window.FileReader) {
        return alert("FileReader API is not supported by your browser.");
    }
    var $i = $("#experirencefile"), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
    if (input.files && input.files[0]) {
        file = input.files[0]; // The file
        fr = new FileReader(); // FileReader instance
        fr.onload = function() {
            // Do stuff on onload, use fr.result for contents of file
            $("#21arrdata").val(fr.result);
        };
        //fr.readAsText( file );
        fr.readAsDataURL(file);
    } else {
        // Handle errors here
        alert("File not selected or browser incompatible.");
    }
}

// Personal letter upload
function loadingpersonal_file() {
    if (!window.FileReader) {
        return alert("FileReader API is not supported by your browser.");
    }
    var $i = $("#personalfile"), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
    if (input.files && input.files[0]) {
        file = input.files[0]; // The file
        fr = new FileReader(); // FileReader instance
        fr.onload = function() {
            // Do stuff on onload, use fr.result for contents of file
            $("#22arrdata").val(fr.result);
        };
        //fr.readAsText( file );
        fr.readAsDataURL(file);
    } else {
        // Handle errors here
        alert("File not selected or browser incompatible.");
    }
}

// Other letter upload
function loadingother_file() {
    if (!window.FileReader) {
        return alert("FileReader API is not supported by your browser.");
    }
    var $i = $("#otherfile"), // Put file input ID here
        input = $i[0]; // Getting the element from jQuery
    if (input.files && input.files[0]) {
        file = input.files[0]; // The file
        fr = new FileReader(); // FileReader instance
        fr.onload = function() {
            // Do stuff on onload, use fr.result for contents of file
            $("#otherarrdata").val(fr.result);
        };
        //fr.readAsText( file );
        fr.readAsDataURL(file);
    } else {
        // Handle errors here
        alert("File not selected or browser incompatible.");
    }
}

// View Offerletter
var user_id = <?php echo $userid; ?>;
//alert(user_id);
$.ajax({
    url: base_url + "checkDocumentUploaded",
    data: {
        user_id: user_id
    },
    method: "POST",
    dataType: "json",
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Token', localStorage.token);
    }
})

.done(function(response) {
    if (response["data"]["status"] == 1) {
        $('#onboardingStatus').css({"background-color": "#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingStatus').css({"background-color": "#dc3545"}).text("Pending");
    }
});
$.ajax({
    url: base_url + "checkPersonalDocument",
    data: {
        user_id: user_id
    },
    method: "POST",
    dataType: "json",
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Token', localStorage.token);
    }
})

.done(function(response) {

    if (response["data"]["status"] == 1) {
        $('#onboardingPersonal').css({"background-color": "#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingPersonal').css({"background-color": "#dc3545"}).text("Pending");
    }
})
// Expierience table
$.ajax({
    url: base_url + "checkExperienceDocument",
    data: {
        user_id: user_id
    },
    method: "POST",
    dataType: "json",
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Token', localStorage.token);
    }
})

.done(function(response) {
    if (response["data"]["status"] == 1) {
        $('#onboardingExperience').css({"background-color": "#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingExperience').css({"background-color": "#dc3545"}).text("Pending");
    }

});
// Expierience table
$.ajax({
        url: base_url + "checkOtherDocument",
        data: {
            user_id: user_id
        },
        method: "POST",
        dataType: "json",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Token', localStorage.token);
        }
    })
    .done(function(response) {

        if (response["data"]["status"] == 1) {
            $('#onboardingOther').css({
                "background-color": "#28a745"
            }).text("Complete");
        }
        if (response["data"]["status"] == 0) {
            $('#onboardingOther').css({
                "background-color": "#dc3545"
            }).text("Pending");
        }

    });
// Edit Reimbursement
var user_id = <?php echo $userid; ?>;
//alert(user_id);
$.ajax({
    url: base_url + "viewUserId",
    data: {
        user_id: user_id
    },
    type: "POST",
    dataType: 'json',
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Token', localStorage.token);
    },

    success: function(response) {

        $('#addotheruserid').val(response["data"][0]["id"]);
        $('#editpersonaluserid').val(response["data"][0]["id"]);
        $('#editexperienceuserid').val(response["data"][0]["id"]);
        $('#editrelativeuserid').val(response["data"][0]["id"]);
        localStorage.removeItem('$reimbursementid');
    }
});
//Add Other letter form
$("#otherformtest").submit(function(e) {

    var formData = {
        'user_id': $('input[name=addotheruserid]').val(),
        'document_name': $('input[name=adddocumentname]').val(),
        'instruction_document': $('textarea[name=addinstructiondocument]').val(),
        'other': $('#otherarrdata').val()
    };
    //alert(formData);
    e.preventDefault();
    $.ajax({
            type: 'POST',
            url: base_url + 'addOtherdocument',
            data: formData,
            dataType: 'json',
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            }
        })
        .done(function(response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function() {
                    window.location = "<?php echo base_url()?>onBoardingAddDocument/" + user_id
                }, 1000);

            } else {
                toastr.error(falsedata);
                $('#otherformtest [type=submit]').attr('disabled', false);
            }

        });

});
//Add Document offer letter form
$("#addofferletter").submit(function(e) {

    var formData = {
        'user_id': $('input[name=editrelativeuserid]').val(),
        'document_name': $('input[name=offerdocumentname]').val(),
        'offer_letter': $('#23arrdata').val()
    };
    e.preventDefault();
    $.ajax({
            type: 'POST',
            url: base_url + 'addOnboarding',
            data: formData,
            dataType: 'json',
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            }
        })
        .done(function(response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function() {
                    window.location = "<?php echo base_url()?>onBoardingAddDocument/" + user_id
                }, 1000);

            } else {
                toastr.error(falsedata);
                $('#addofferletter [type=submit]').attr('disabled', false);
            }

        });

});

//Add Experience letter form
$("#addexperienceletter").submit(function(e) {

    var formData = {
        'user_id': $('input[name=editexperienceuserid]').val(),
        'document_name': $('input[name=addexperiencedocumentname]').val(),
        'experience_letter': $('#21arrdata').val()
    };
    e.preventDefault();
    $.ajax({
            type: 'POST',
            url: base_url + 'addExperience',
            data: formData,
            dataType: 'json',
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            }
        })
        .done(function(response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function() {
                    window.location = "<?php echo base_url()?>onBoardingAddDocument/" + user_id
                }, 1000);

            } else {
                toastr.error(falsedata);
                $('#addexperienceletter [type=submit]').attr('disabled', false);
            }
        });

});

//Add Personal letter form
$("#personalform").submit(function(e) {

    var formData = {
        'user_id': $('input[name=editpersonaluserid]').val(),
        'document_name': $('input[name=documentname]').val(),
        'personaldocument': $('#22arrdata').val()
    };
    e.preventDefault();
    $.ajax({
            type: 'POST',
            url: base_url + 'addPersonalDocument',
            data: formData,
            dataType: 'json',
            encode: true,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Token', localStorage.token);
            }
        })
        .done(function(response) {
            //console.log(response);
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function() {
                    window.location = "<?php echo base_url()?>onBoardingAddDocument/" + user_id
                }, 1000);

            } else {
                toastr.error(falsedata);
                $('#personalform [type=submit]').attr('disabled', false);
            }

        });

});


//Add Offer Letter Validation
$(document).ready(function() {
    $("#addofferletterbutton").click(function(e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#offerdocumentname").val().trim() == "") {
            $("#offerdocumentname").focus();
            $("#offerdocumentnamevalid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#offerdocumentnamevalid").text("");
        }

        if ($("#resumefile").val().trim() == "") {
            $("#resumefilevalid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#resumefilevalid").text("");
        }

        if (errorCount > 0) {
            return false
        }
    });
});


//Add Experience Letter Validation
$(document).ready(function() {
    $("#addexperienceletterbutton").click(function(e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#addexperiencedocumentname").val().trim() == "") {
            $("#addexperiencedocumentname").focus();
            $("#addexperiencedocumentnamevalid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addexperiencedocumentnamevalid").text("");
        }

        if ($("#experirencefile").val().trim() == "") {
            $("#experirencefilevalid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#experirencefilevalid").text("");
        }

        if (errorCount > 0) {
            return false
        }
    });
});

//Add Personal Letter Validation
$(document).ready(function() {
    $("#addpersonalbutton").click(function(e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#documentname").val().trim() == "") {
            $("#documentname").focus();
            $("#documentnamevalid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#documentnamevalid").text("");
        }

        if ($("#personalfile").val().trim() == "") {
            $("#personalfilealid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#personalfilealid").text("");
        }

        if (errorCount > 0) {
            return false
        }
    });
});

//Add Experience Letter Validation
$(document).ready(function() {
    $("#addotherletterbutton").click(function(e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#adddocumentname").val().trim() == "") {
            $("#adddocumentname").focus();
            $("#adddocumentnamealid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#adddocumentnamealid").text("");
        }

        if ($("#addinstructiondocument").val().trim() == "") {
            $("#addinstructiondocument").focus();
            $("#addinstructiondocumentalid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addinstructiondocumentalid").text("");
        }

        if ($("#otherfile").val().trim() == "") {
            $("#addotherfilealid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#addotherfilealid").text("");
        }

        if (errorCount > 0) {
            return false
        }
    });
});

// For Family Details Form Validation
$(document).ready(function() {
    $("#familydetailsbutton").click(function(e) {
        e.stopPropagation();
        var errorCount = 0;
        if ($("#familyname").val().trim() == "") {
            $("#familyname").focus();
            $("#familynamevalid").text("This field can't be empty.");
            errorCount++;
        } else {
            $("#familynamevalid").text("");
        }

        if ($("#familyrelation").val() == null) {
            $("#familyrelation").focus();
            $("#familyrelationvalid").text("Please select a family relation.");
            errorCount++;
        } else {
            $("#familyrelationvalid").text("");
        }

        if ($("#familycontactno").val().length < 10) {
            $("#familycontactno").focus();
            $("#familycontactnovalid").text("Enter valid conatct no.");
            errorCount++;
        } else {
            $("#familycontactnovalid").text("");
        }

        if (errorCount > 0) {
            return false;
        }
    });
});
</script>
