
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">App Setting</li>
            </ul>
        </div>
    </div>
</div>

<style type="text/css">
    .form-group {
        border-bottom: 1px solid #eee;
        display: flex;
    }

    .fa-question-circle {
        z-index: 1;
        position: absolute;
        right: 0px;
        top: 7px;
    }

</style>

<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="d-lg-flex justify-content-between">
            <ul class="nav nav-tabs page-header-tab">
                <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#team_details">Team Details</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#policies">Policies</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activities">Activities </a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#subscription">Subscription </a></li>
            </ul>
        </div>
        
        <div class="tab-content">
            <div class="tab-pane active show" id="team_details">
                <div class="card">
                    <div class="card-body">
                        <form action="#" method="POST">
                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="teamname">Team Name</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="This could be the registered company or organisation name, team name, workplace name or location" data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <input class="form-control" type="text" id="teamname" name="teamname" value="IOS Development">
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="fname">Time Zone</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="This could be the registered company or organisation name, team name, workplace name or location" data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <input class="form-control" type="text" id="timezone" name="timezone" value="(UTC+05:30) Mumbai, Delhi, Bengaluru">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="fname">Start Day for the Week</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="This is set to Monday by default.  Timesheet start days are updated if this option is changed" data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <select class="form-control">
                                        <option value="" disabled="">Select Start Day for the Week</option>
                                        <option selected="">Monday</option>
                                        <option>Tuesday</option>
                                        <option>Wednesday</option>
                                        <option>Thuresday</option>
                                        <option>Friday</option>
                                        <option>Saturday</option>
                                        <option>Sunday</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="fname">Starting Month</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="This is set to January by default but some teams may choose to set this to align with the start of their financial year. It's applicable when it comes to reports and viewing team member stats" data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <select class="form-control">
                                        <option selected="">January</option>
                                        <option>February</option>
                                        <option>March</option>
                                        <option>April</option>
                                        <option>May</option>
                                        <option>June</option>
                                        <option>July</option>
                                        <option>August</option>
                                        <option>September</option>
                                        <option>October</option>
                                        <option>November</option>
                                        <option>December</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="fname">Working Hours in a Day</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="This is set to Monday by default.  Timesheet start days are updated if this option is changed" data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <select class="form-control">
                                        <option>0.5</option>
                                        <option>1</option>
                                        <option>1.5</option>
                                        <option>2</option>
                                        <option>2.5</option>
                                        <option>3</option>
                                        <option>3.5</option>
                                        <option>4</option>
                                        <option>4.5</option>
                                        <option>5</option>
                                        <option>5.5</option>
                                        <option>6</option>
                                        <option>6.5</option>
                                        <option>7</option>
                                        <option>7.5</option>
                                        <option selected="">8</option>
                                        <option>8.5</option>
                                        <option>9</option>
                                        <option>9.5</option>
                                        <option>10</option>
                                        <option>10.5</option>
                                        <option>11</option>
                                        <option>11.5</option>
                                        <option>12</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="fname">Automatic Jibble Out</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="Generally teams should encourage their members to jibble out themselves for accurate time tracking and attendance.  But there could be a number of reasons a team may use this automatic jibble out feature since a team may only be concerned with jibbled in time only." data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <select class="form-control">
                                        <option selected="">0.5</option>
                                        <option>1</option>
                                        <option>1.5</option>
                                        <option>2</option>
                                        <option>2.5</option>
                                        <option>3</option>
                                        <option>3.5</option>
                                        <option>4</option>
                                        <option>4.5</option>
                                        <option>5</option>
                                        <option>5.5</option>
                                        <option>6</option>
                                        <option>6.5</option>
                                        <option>7</option>
                                        <option>7.5</option>
                                        <option>8</option>
                                        <option>8.5</option>
                                        <option>9</option>
                                        <option>9.5</option>
                                        <option>10</option>
                                        <option>10.5</option>
                                        <option>11</option>
                                        <option>11.5</option>
                                        <option>12</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="fname">Reminder</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="If reminder is enabled, members of the team will get the reminder notification 5 minutes before they get automatic jibbled out." data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <!-- Default inline 1-->
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="defaultInline1">
                                        <label class="custom-control-label" for="defaultInline1">Email</label>
                                    </div>

                                    <!-- Default inline 2-->
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="defaultInline2">
                                        <label class="custom-control-label" for="defaultInline2">Bot</label>
                                    </div>

                                    <!-- Default inline 3-->
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="defaultInline3">
                                        <label class="custom-control-label" for="defaultInline3">Mobile</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 float-left">
                                    <label for="fname">Team Members Sorted By</label>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-title="By default the list of team members that appear in all the views within the app are sorted by first name" data-original-title="" title="" aria-describedby="tooltip277564"></i>
                                </div>
                                <div class="col-md-4 float-right">
                                    <select class="form-control">
                                        <option selected="">First Name</option>
                                        <option>Last Name</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" style="float: right;">
                                <button id="addjobbutton" class="btn btn-success submit-btn mr-3">Submit</button>
                                <button type="reset" id="test" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>                        
            </div>
            <div class="tab-pane" id="policies">
                <div class="card">
                    <div class="card-body">
                        <div class="policies-powerup-disabled">
                            <div class="banner row">
                                <div class="promo-text col-md-6">
                                    <h2>Policies</h2>
                                    <p class="m-b">Configure Jibble to suit your business needs. You can tighten up security by restricting employees to clock in using specific devices/channels and enforcing a server clock, ensuring no one can cheat the system. Upgrade your subscription today to use the Policies Power-Up</p>
                                    
                                        <a href="#" class="btn btn-primary">Upgrade Now</a>
                                    
                                </div>
                                <div class="promo-image col-md-6">
                                    <img src="https://app.jibble.io/images/powerUps/promo-policies-banner-img.png" alt="Policies">
                                </div>
                            </div>

                            <div class="promo-container row mt-5">
                                
                                <div class="text-container col-md-4">
                                    <h6 class="text-center">Set Channels</h6>
                                    <p>Configure the channel you want employees to Jibble In/Out of. You can enforce or restrict your team from using the Web Browser, Slack or their Mobile Phone (Android/iPhone).</p>
                                </div>
                                <div class="text-container col-md-4">
                                    <h6 class="text-center">Prevent Buddy Punching</h6>
                                    <p>Add extra layers of security by requiring staff members to Jibble In/Out with activities, a selfie and/or their phone's geolocation. Using facial recognition means the chances for fraudulent entries and 'buddy punching' are almost non-existent. You can also enforce a server clock that prevents anyone from manually adjusting time entries.</p>
                                </div>
                                <div class="text-container col-md-4">
                                    <h6 class="text-center">Secured Timesheets &amp; Logs</h6>
                                    <p>Restrict access to sensitive data to just managers or admins. You can set up access rights to team timesheets and decide if team members can edit their own time entries.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                    
            </div>
            <div class="tab-pane" id="activities">
                <div class="card">
                    <div class="card-body">
                        <div class="policies-powerup-disabled">
                            <div class="banner row">
                                <div class="promo-text col-md-6">
                                    <h2>Activity Tracking</h2>
                                    <p class="m-b">Activity Tracking allows you to create activities and projects that you can track time against, enabling you to generate useful client, payroll or productivity reports. Upgrade your subscription today to use the Activity Tracking Power-Up</p>
                                    
                                        <a href="#" class="btn btn-primary">Upgrade Now</a>
                                </div>
                                <div class="promo-image col-md-6">
                                    <img src="https://app.jibble.io/images/powerUps/promo-policies-banner-img.png" alt="Policies">
                                </div>
                            </div>

                            <div class="promo-container row mt-5">
                                
                                <div class="text-container col-md-6">
                                    <h6 class="text-center">Set up your activities and track time against them</h6>
                                    <p>Add an activity and then your staff can track time against them. They simply select the activity when they clock in or out.</p>
                                </div>
                                <div class="text-container col-md-6">
                                    <h6 class="text-center">Get accurate reports and timesheets</h6>
                                    <p>When your staff Jibbles in or out against activities, you will be able to break your reports down into more detail.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                    
            </div>
            <div class="tab-pane" id="subscription">
                <div class="card">
                    <div class="js-subscription panel-body panel-body-large" style="overflow: visible;">
                        <div class="subscription-tab text-left p-xs">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <div class="plan-name m-b-xs"><span class="m-r-sm">Attendance Pro</span></div>
                                    <div class="m-b">
                                        <div class="estimate-info">Your first bill for <strong>$6</strong> is due on <strong>13 Apr 2020</strong></div>
                                        <div class="warning-text">Free trial expires in: <strong>12 days</strong></div>
                                    </div>
                                    <button class="btn btn-primary btn-lg js-add-payment-details-btn m-r">Add Payment Details</button>
                                    <button class="btn btn-outline btn-lg js-change-plan-btn">Change Plan</button>
                                </div>
                                <img src="https://app.jibble.io/images/subscription-rocket.png" class="pull-right m-b-lg" height="240" alt="Subscription">
                            </div>
                            <hr>
                            
                            <div>
                                <div class="row">
                                    <div class="col-md-3 subscription-property-name">Billing cycle</div>
                                    <div class="col-md-9 subscription-property-value">Monthly</div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 subscription-property-name">Price</div>
                                    <div class="col-md-9 subscription-property-value">$2 per user/month</div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 subscription-property-name">Monthly active users
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" data-container="body" data-html="true" data-delay="5000" data-title="A Monthly Active User (MAU) is a user that is registered as active in a specific month. Active users are those who:<div style=&quot;text-align: left;&quot;><br>- Jibble in/out<br>- Log in to the web app<br>- Use of any Jibble Bot or API functions</div>" data-original-title="" title="" aria-describedby="tooltip111279"></i></div>
                                    <div class="col-md-9 subscription-property-value">0</div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3 subscription-property-name">Payment method</div>
                                    <div class="col-md-9 subscription-property-value"><span class="disabled">No payment method set up yet</span></div>
                                </div>
                                <hr>
                                <div class="m-b">
                                    <a class="js-customer-portal-btn">Payment &amp; Billing Details</a>
                                </div>
                                <div>
                                    <a class="js-cancel-subscription">Cancel Subscription</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!------------ Modal  Add Location------------>
<div class="modal custom-modal fade" id="add_location" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Location</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form id="addbranch" method="POST" action="#" >
                    <div class="form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <br>
                        <span id="branch_error" class="text-danger"></span>
                        <input type="text" name="addbranchname" id="addbranchname" class="form-control"  placeholder="Enter the name of your Location">
                    </div>
                    <fieldset name="Address" class="col-md-12 form-group">
                        <legend>Address</legend>
                        <input type="text" class="form-control mb-1" name="street1" placeholder="Street Address 1" value="">
                        <input type="text" class="form-control mb-1" name="street2" placeholder="Address Line 2" value="">
                        <input type="text" class="form-control mb-1" name="city" placeholder="City" value="">
                        <input type="text" class="form-control mb-1" name="postcode" placeholder="Postcode" value="">
                        <input type="text" class="form-control mb-1" name="country" placeholder="Country" value="">
                        <button class="btn btn-primary" type="submit">Search</button>
                    </fieldset>
                    
                    <div class="submit-section pull-right">
                        <button id="addbranchbutton" class="btn btn-primary submit-btn">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete Location Modal -->
<div class="modal custom-modal fade" id="delete_branch" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" id="" class="btn btn-danger continue-btn delete_branch_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Location Modal -->



<!------------ Modal    Add Holiday------------>
<div class="modal custom-modal fade" id="add_holiday" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Holiday</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addholiday" method="POST" action="#">
                    <div class="form-group">
                        <label>Holiday Name <span class="text-danger">*</span></label><br>
                        <span id="addholidaynamevalidate" class="text-danger"></span>
                        <input id="addholidayname" name="addholidayname" placeholder="Please enter holiday name." class="form-control" type="text" >
                    </div>
                    
                    <div class="form-group">
                        <label>Holiday Date <span class="text-danger">*</span></label><br>
                        <span id="addholidaydatevalidate" class="text-danger"></span>   
                        <input data-provide="datepicker" data-date-autoclose="true" id="addholidaydate" name="addholidaydate" class="form-control" type="text" placeholder="yyyy/mm/dd" readonly>
                    
                    </div>
                    <div class="submit-section pull-right">
                        <button id="addholidaybutton" class="btn btn-primary submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Holiday Modal -->
<div class="modal custom-modal fade" id="edit_holiday" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Holiday</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editholiday" method="POST">
                    <div class="form-group">
                        <label>Holiday Name <span class="text-danger">*</span></label><br>
                        <span id="editholidaynamevalidate" class="text-danger"></span>
                        <input id="editholidayname" name="editholidayname" class="form-control" value="" type="text">
                    </div>
                    <div class="form-group">
                        <label>Holiday Date <span class="text-danger">*</span></label><br>
                        <span id="editholidaydatevalidate" class="text-danger"></span>
                        <input data-provide="datepicker" data-date-autoclose="true" id="editholidaydate" name="editholidaydate" class="form-control " value="" type="text">
                    </div>
                    <div class="submit-section pull-right">
                        <button id="editholidaybutton" class="btn btn-primary submit-btn">Update Changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Holiday Modal -->

<!-- Delete Holiday Modal -->
<div class="modal custom-modal fade" id="delete_holiday" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <a href="#" class="btn btn-danger continue-btn delete_holiday_button">Yes delete it!</a>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Delete Holiday Modal -->