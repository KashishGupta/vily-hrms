<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>onboardingList">On Boarding</a></li>
                <li class="breadcrumb-item active">Add Emp Details</li>
            </ul>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                <h5 class="modal-title">Employee Details</h5>
            </div>
            <style>
                .bs-example{
                    margin: 20px;
                }
                .accordion .fa{
                    margin-right: 0.5rem;
                    font-size: 23px;
                    font-weight: bold;
                    position: relative;
                    top: 2px;
                }
                button#onboardingStatusoffice, button#onboardingStatusedu, button#onboardingStatusexp, button#onboardingStatusBank, button#onboardingStatusparent, button#onboardingStatusLeave {
				    position: absolute;
				    right: 15px;
				    top: 7px;
				    cursor: context-menu;
				    border: 0;
				    border-radius: 25px;
				}
			</style>

            <div class="bs-example">
                <div class="accordion" id="accordionExample">
                    
                    <div class="card">
                        <div class="card-header collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo"><i class="fa fa-angle-right"></i> Education Details 
                        	<button id="onboardingStatusedu" class="btn btn-secondary"></button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <form id="addEducationdetails" method="POST" action="#">
                                    <div class="row clearfix">
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Highest Qualification <span class="text-danger"> *</span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="highestdegreevalid"></span>
                                            <input class="form-control" type="hidden" value="editdescription" name="editdescription" id="editdescription" />
                                            <input class="form-control" type="hidden" value="userEducationtest" name="userEducationtest" id="userEducationtest" />
                                            <input class="form-control" name="highestdegree" id="highestdegree" placeholder="Please enter highest qualification." >
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Institution Name<span class="text-danger"> *</span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="institutionnamevalid"></span>
                                            <input type="text" class="form-control" name="institutionname" id="institutionname" placeholder="Please enter institution name." />
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Specialization</label>
                                            <!-- <br />
                                            <span class="text-danger change-pos" id="subjectvalid"></span> -->
                                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Please enter specialization." >
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Grade</label>
                                            <!-- <br />
                                            <span class="text-danger change-pos" id="gradevalid"></span> -->
                                            <input type="text" class="form-control" name="grade" id="grade" placeholder="Please enter grade." >
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>End Year<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="eduenddatevalid"></span>
                                            <input type="date" class="form-control" name="eduenddate" id="eduenddate">
                                        </div>
                                        <!-- <div class="form-group col-md-1 col-sm-12 button-open">
                                            <button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="fa fa-plus" aria-hidden="true"></span> </button>
                                        </div>

                                        <div id="education_fields" style="width: 100%">
                                        </div> -->

                                        <div class="col-lg-12 mt-3 text-right">
                                            <button id="addeducationbutton" class="btn btn-primary submit-btn">Submit</button>
                                            <button type="reset" class="btn btn-secondary" id="test1">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree"><i class="fa fa-angle-right"></i> Experience Details 
                        	<button id="onboardingStatusexp" class="btn btn-secondary"></button>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <form id="addExperiencedetails" method="POST" action="#">
                                    <div class="row clearfix">
                                        
                                        <div class="form-group col-sm-12">
                                            <div class="form-label">Choose </div>
                                            <div class="custom-controls-stacked">
                                                <label class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" id="interm" name="example-inline-radios" value="0" onclick="myFunction()">
                                                    <span class="custom-control-label">Intern / Fresher</span>
                                                </label>
                                                <label class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" id="experience" name="example-inline-radios" value="" onclick="myFunction1()">
                                                    <span class="custom-control-label">Experience</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Previous Company Name<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="previouscompanynamevalid"></span>
                                            <input class="form-control" type="hidden" value="editde" name="editde" id="editde"  />
                                            <input class="form-control" type="hidden" value="useeexperience" name="useeexperience" id="useeexperience"  />
                                            <input type="text" class="form-control" name="previouscompanyname" id="previouscompanyname" placeholder="Please enter previous company name."/>
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Designation<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="positionvalid"></span>
                                            <input type="text" class="form-control" name="position" id="position" placeholder="Please enter designation." />
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Location<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="locationvalid"></span>
                                            <input type="text" class="form-control" name="location" id="location" placeholder="Please enter location." />
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Start Date<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="startdatevalid"></span>
                                            <input type="date" class="form-control" name="startdate" id="startdate" />
                                        </div>

                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>End Date<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="enddatevalid"></span>
                                            <input type="date" class="form-control" name="enddate" id="enddate" />
                                        </div>

                                        <div class="col-lg-12 mt-3 text-right">
                                            <button id="addexperiencebutton" class="btn btn-primary submit-btn">Submit</button>
                                            <button type="reset" class="btn btn-secondary" id="test2">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour"><i class="fa fa-angle-right"></i> Bank Details 
                        	<button id="onboardingStatusBank" class="btn btn-secondary"></button>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                            <div class="card-body">
                                <form id="employeeBank" method="POST" action="#">
                                    <div class="row clearfix">
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Account Holder Name <span class="text-danger">*</span></label>
                                            <br>
                                            <span class="text-danger change-pos" id="accountholdernamevalid"></span>
                                            <input class="form-control name-valid" type="text" name="accountholdername" id="accountholdername"  placeholder="Please enter account holder name." />
                                        </div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Bank Account No. <span class="text-danger">*</span></label>
                                            <br>
                                            <span class="text-danger change-pos" id="bankaccountnovalid"></span>
                                            <input class="form-control" type="text" name="bankaccountno" id="bankaccountno" placeholder="Please enter bank account no." maxlength="16" onkeypress="return onlyNumberKey(event)" />
                                        </div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Bank Name <span class="text-danger">*</span></label>
                                            <br>
                                            <span class="text-danger change-pos" id="banknamevalid"></span>
                                            <input class="form-control" type="text" name="bankname" id="bankname" placeholder="Please enter bank name" />
                                        </div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Bank Branch Address <span class="text-danger">*</span></label>
                                            <br>
                                            <span class="text-danger change-pos" id="bankbranchnamevalid"></span>
                                            <input class="form-control" type="text" name="bankbranchname" id="bankbranchname" placeholder="Please enter Bank Branch Address name." />
                                        </div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>IFSC Code <span class="text-danger">*</span></label>
                                            <br>
                                            <span class="text-danger change-pos" id="ifsccodevalid"></span>
                                            <input class="form-control" type="text" name="ifsccode" id="ifsccode" maxlength="11" placeholder="Please enter bank ifsc code." />
                                        </div>
                                        
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Pan Card No.<span class="text-danger">*</span></label>
                                            <br>
                                            <span class="text-danger change-pos" id="pancardvalid"></span>
                                            <input class="form-control" type="text" name="pancard" id="pancard" placeholder="Please enter pancard no." maxlength="10" />
                                        </div>
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Aadhar Card No.<span class="text-danger">*</span></label>
                                            <br>
                                            <span class="text-danger change-pos" id="aadharcardvalid"></span>
                                            <input class="form-control" type="hidden" value="user_id" name="user_id" id="user_id" />
                                            <input class="form-control" type="text" name="aadharcard" id="aadharcard" maxlength="16" placeholder="Please enter aadhar card no." onkeypress="return onlyNumberKey(event)"  />
                                        </div>
                                        <div class="col-lg-12 mt-3 text-right">
                                            <button id="addbankdetailsbutton" class="btn btn-primary submit-btn">Submit</button>
                                            <button type="reset" class="btn btn-secondary" id="test3">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive"><i class="fa fa-angle-right"></i>Leave Policy <button id="onboardingStatusLeave" class="btn btn-secondary"></button>  
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                            <div class="card-body">
                                <form id="leavePolicyform" method="POST" action="#">
                                    <div class="row clearfix">
                                    
                                        <div class="form-group col-md-4 col-sm-12">
                                            <label>Leave Policy<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="policynamevalid"></span>
                                            <input class="form-control" type="hidden" value="userparent" name="userparent" id="userparent" />
                                            <?php 
                                             $query1 = $this->db->query("SELECT leave_policy_id FROM `users` WHERE id='".$userid."'");
                                             $arrPolicy = $query1->row();
                                             $arrTest1 = $arrPolicy->leave_policy_id;
                                            
                                             $query = $this->db->query("SELECT policy_name FROM `leave_policy` WHERE id IN ($arrTest1)");
                                             $arrProjectsData1 = $query->result();
                                            $arrtempData =  array();
                                             foreach($arrProjectsData1 as $team1) {
                                                $arrtempData[] = $team1->policy_name;
                                              } 
                                            $arrattet = implode(",",$arrtempData);
                                            
                                            ?>
                                            <input class="form-control" type="hidden" value="<?php echo $arrattet;  ?>" name="SelectName" id="SelectName" />
                                            <select class="form-control chosen-select" multiple name="leavePolicyname"  id="UserRole"/>
                                            <?php
                                            $query = $this->db->query("SELECT id,policy_name FROM `leave_policy`");
                                            $arrProjectsData = $query->result();
                                            foreach($arrProjectsData as $team) { ?>                                                        
                                            <option value="<?php echo $team->policy_name; ?>"><?php echo $team->policy_name; ?></option>
                                            <?php } ?>

                                            </select>
                                           
                                        </div>
                                        <div class="col-lg-12 mt-3 text-right">
                                            <button id="addleavePolicybutton" class="btn btn-primary submit-btn">Submit</button>
                                            <button type="reset" class="btn btn-secondary" id="test3">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix" data-toggle="collapse" data-target="#collapseSix"><i class="fa fa-angle-right"></i>Reporting Manager 
                        	<button id="onboardingStatusparent" class="btn btn-secondary"></button>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                            <div class="card-body">
                                <form id="addParentdetails" method="POST" action="#">
                               
                                    <div class="row clearfix">
                                        <div class="form-group col-md-4 col-sm-12">
                                        <label>Reporting Manager OR HR<span class="text-danger"> </span></label>
                                            <br />
                                            <span class="text-danger change-pos" id="policynamevalid"></span>
                                            <input class="form-control" type="hidden" value="userparent_parent_id" name="userparent_parent_id" id="userparent_parent_id" />
                                            <?php 
                                             $query1 = $this->db->query("SELECT  parent_id FROM users  WHERE id='".$userid."'");
                                             $arrPolicy = $query1->row();
                                             $arrTest1 = $arrPolicy->parent_id;
                                            
                                             $query = $this->db->query("SELECT id,designation_name FROM `company_designations` WHERE id IN ($arrTest1)");
                                             $arrProjectsData1 = $query->result();
                                            $arrtempData1 =  array();
                                             foreach($arrProjectsData1 as $team1) {
                                                $arrtempData1[] = $team1->id;
                                              } 
                                            $arrattet1 = implode(",",$arrtempData1);
                                           
                                            ?>
                                            <input class="form-control" type="hidden" value="<?php echo $arrattet1;?>" name="SelectNameDesignation" id="SelectNameDesignation" />
                                            <select class="form-control chosen-select" multiple name="leavePolicyname1"  id="UserRole_designation"/>
                                            <?php
                                            
                                           $query8 = $this->db->query("SELECT id,designation_name FROM `company_designations`");
                                            $arrreportManager7 = $query8->result();
                                         
                                            foreach($arrreportManager7 as $manager4)
                                            { 
                                            ?>                                                        
                                            <option value="<?php echo $manager4->id; ?>"><?php echo $manager4->designation_name; ?></option>
                                            <?php } ?>

                                            </select>
                                        </div>

                                       
                                        <div class="col-lg-12 mt-3 text-right">
                                            <button id="addbankdetailsbutton" class="btn btn-primary submit-btn">Submit</button>
                                            <button type="reset" class="btn btn-secondary" id="test">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    div#UserRole_chosen, div#UserRole_designation_chosen {
        width: 100% !important;
    }
</style>

<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>

<script>
$(".chosen-select").chosen({
  no_results_text: "Oops, nothing found!"
});

    var selectedUserRole = document.getElementById('SelectName').value;
  
    var str_array = selectedUserRole.split(',');
   console.log(str_array);
    for (var i = 0; i < str_array.length; i++) {
        str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
    }

    $("#UserRole").val(str_array).trigger("chosen:updated");


    var selectedUserDesignation = document.getElementById('SelectNameDesignation').value;
  
  var str_array_designation = selectedUserDesignation.split(',');
 console.log(str_array_designation);
  for (var i = 0; i < str_array_designation.length; i++) {
    str_array_designation[i] = str_array_designation[i].replace(/^\s*/, "").replace(/\s*$/, "");
  }

  $("#UserRole_designation").val(str_array_designation).trigger("chosen:updated");

</script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>

    $(document).ready(function(){
        // Add down arrow icon for collapse element which is open by default
        $(".collapse.show").each(function(){
            $(this).prev(".card-header").find(".fa").addClass("fa-angle-down").removeClass("fa-angle-right");
        });
        
        // Toggle right and down arrow icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).prev(".card-header").find(".fa").removeClass("fa-angle-right").addClass("fa-angle-down");
        }).on('hide.bs.collapse', function(){
            $(this).prev(".card-header").find(".fa").removeClass("fa-angle-down").addClass("fa-angle-right");
        });
    });
</script>


<script>
 // View Offerletter
 var userds_id = <?php echo $userid; ?>;
    //alert(user_id);
    $.ajax({
        url: base_url+"checkOfficeDetails",
        data: {user_id: userds_id },
        method:"POST",  
        dataType:"json", 
        beforeSend: function(xhr){
             xhr.setRequestHeader('Token', localStorage.token);}
    })
    
    .done(function(response) { 
       
   
    if (response["data"]["status"] == 1) {
        $('#onboardingStatusoffice').css({"background-color":"#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingStatusoffice').css({"background-color":"#dc3545"}).text("Pending");
    }
   
    });

    $.ajax({
        url: base_url+"checkEducationDetails",
        data: {user_id: userds_id },
        method:"POST",  
        dataType:"json", 
        beforeSend: function(xhr){
             xhr.setRequestHeader('Token', localStorage.token);}
    })
    
    .done(function(response) { 
       
   
    if (response["data"]["status"] == 1) {
        $('#onboardingStatusedu').css({"background-color":"#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingStatusedu').css({"background-color":"#dc3545"}).text("Pending");
    }
   
    });

    $.ajax({
        url: base_url+"checkExperienceDetails",
        data: {user_id: userds_id },
        method:"POST",  
        dataType:"json", 
        beforeSend: function(xhr){
             xhr.setRequestHeader('Token', localStorage.token);}
    })
    
    .done(function(response) { 
       
   
    if (response["data"]["status"] == 1) {
        $('#onboardingStatusexp').css({"background-color":"#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingStatusexp').css({"background-color":"#dc3545"}).text("Pending");
    }
   
    });
    $.ajax({
        url: base_url+"checkBankDetails",
        data: {user_id: userds_id },
        method:"POST",  
        dataType:"json", 
        beforeSend: function(xhr){
             xhr.setRequestHeader('Token', localStorage.token);}
    })
    
    .done(function(response) { 
       
   
    if (response["data"]["status"] == 1) {
        $('#onboardingStatusBank').css({"background-color":"#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingStatusBank').css({"background-color":"#dc3545"}).text("Pending");
    }
   
    });

    $.ajax({
        url: base_url+"checkParentDetails",
        data: {user_id: userds_id },
        method:"POST",  
        dataType:"json", 
        beforeSend: function(xhr){
             xhr.setRequestHeader('Token', localStorage.token);}
    })
    
    .done(function(response) { 
       
   
    if (response["data"]["status"] == 1) {
        $('#onboardingStatusparent').css({"background-color":"#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingStatusparent').css({"background-color":"#dc3545"}).text("Pending");
    }
   
    });
   //check Leave Policy Details
    $.ajax({
        url: base_url+"checkLeavePolicyDetails",
        data: {user_id: userds_id },
        method:"POST",  
        dataType:"json", 
        beforeSend: function(xhr){
             xhr.setRequestHeader('Token', localStorage.token);}
    })
    
    .done(function(response) { 
       
   
    if (response["data"]["status"] == 1) {
        $('#onboardingStatusLeave').css({"background-color":"#28a745"}).text("Complete");
    }
    if (response["data"]["status"] == 0) {
        $('#onboardingStatusLeave').css({"background-color":"#dc3545"}).text("Pending");
    }
   
    });
    
      

	$(function () {
	    $("#maritalstatus").change(function () {
	        if ($(this).val() == "Single/Unmarried") {
	            $("#hideshow").hide();
	        } else {
	            $("#hideshow").show();
	        }
	    });
	});
	function myFunction() {
	  document.getElementById("previouscompanyname").disabled = true;
	  document.getElementById("position").disabled = true;
	  document.getElementById("location").disabled = true;
	  document.getElementById("startdate").disabled = true;
	  document.getElementById("enddate").disabled = true;
	  document.getElementById("experience_fields_button").disabled = true;
	}

	function myFunction1() {
	  document.getElementById("previouscompanyname").disabled = false;
	  document.getElementById("position").disabled = false;
	  document.getElementById("location").disabled = false;
	  document.getElementById("startdate").disabled = false;
	  document.getElementById("enddate").disabled = false;
	  document.getElementById("experience_fields_button").disabled = false;
	}
	//Select leave  Policy by api
	$.ajax({
            url: base_url+"viewLeavePloicies",
            data: { },
            type: "POST",
            dataType    : 'json',
            encode      : true,
            beforeSend: function(xhr){xhr.setRequestHeader('Token', localStorage.token);}
        })

        .done(function(response){ 
            let dropdown = $('#leavePolicyname');
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Select Leave Policy</option>');
            dropdown.prop('selectedIndex', 0);
                    
            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($('<option></option>').attr('value', entry.id).text(entry.policy_name));
            })
        });

 	// Edit' Form Fill
 	var user_id = '<?php echo $userid; ?>';
        $.ajax({
            url: base_url+"viewOnUsercode",
            data: {user_id: user_id },
            type: "POST",
            dataType    : 'json',
            beforeSend: function(xhr){
                xhr.setRequestHeader('Token', localStorage.token);
            },

            success:function(response){
                
                $('#editde').val(response["data"][0]["user_code"]);
                $('#editdescription').val(response["data"][0]["user_code"]);
            }
        });

    

	
	

// Edit' Form Fill
var user_code1 = "<?php echo $userid; ?>";
$.ajax({
    url: base_url + "viewOnboardingBank",
    data: { user_id: user_code1 },
    type: "POST",
    dataType: "json",
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.token);
    },

    success: function (response) {
        $("#accountholdername").val(response["data"][0]["holder_name"]);
        $("#bankaccountno").val(response["data"][0]["account_no"]);
        $("#bankname").val(response["data"][0]["bank_name"]);
        $("#bankbranchname").val(response["data"][0]["branch"]);
        $("#ifsccode").val(response["data"][0]["ifsc"]);
        $("#pancard").val(response["data"][0]["pan_card"]);
        $("#aadharcard").val(response["data"][0]["aadhar_card"]);
       
    },
});
	
	

    //for remove validation and empty input field
    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#passportnovalid").text("");
            jQuery("#passportnoexpirevalid").text("");
            jQuery("#nationalityvalid").text("");
            jQuery("#religionvalid").text("");
            jQuery("#maritalstatusvalid").text("");
            jQuery("#childvalid").text("");
            jQuery("#familynamevalid").text("");
            jQuery("#familyrelationvalid").text("");
            jQuery("#familycontactnovalid").text("");
        });

        $("#test1").click(function () {
            jQuery("#highestdegreevalid").text("");
            jQuery("#educationboardvalid").text("");
            jQuery("#highestdegreevalidd").text("");
            jQuery("#educationboardvalidd").text("");
            jQuery("#institutionnamevalid").text("");
            jQuery("#institutionnamevalidd").text("");
            jQuery("#subjectvalid").text("");
            jQuery("#gradevalid").text("");
            jQuery("#edustartdatevalid").text("");
            jQuery("#eduenddatevalid").text("");
        });

        $("#test2").click(function () {
            jQuery("#previouscompanynamevalid").text("");
            jQuery("#positionvalid").text("");
            jQuery("#locationvalid").text("");
            jQuery("#totalexperiencevalid").text("");
            jQuery("#startdatevalid").text("");
            jQuery("#enddatevalid").text("");
        });

        $("#test3").click(function () {
            jQuery("#accountholdernamevalid").text("");
            jQuery("#bankaccountnovalid").text("");
            jQuery("#banknamevalid").text("");
            jQuery("#bankbranchnamevalid").text("");
            jQuery("#ifsccodevalid").text("");
            jQuery("#pancardvalid").text("");
            jQuery("#aadharcardvalid").text("");
        });
    });
   

    

    $(document).ready(function () {
        $("#addeducationbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#highestdegree").val().trim() == "") {
                $("#highestdegree").focus();
                $("#highestdegreevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#highestdegreevalid").text("");
            }


            if ($("#institutionname").val().trim() == "") {
                $("#institutionname").focus();
                $("#institutionnamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#institutionnamevalid").text("");
            }

            if ($("#highestdegree").val().trim() == "") {
                $("#highestdegree").focus();
                $("#highestdegreevalidd").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#highestdegreevalidd").text("");
            }


            if ($("#institutionname").val().trim() == "") {
                $("#institutionname").focus();
                $("#institutionnamevalidd").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#institutionnamevalidd").text("");
            }

            /*if ($("#educationboard").val().trim() == "") {
                $("#educationboard").focus();
                $("#educationboardvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#educationboardvalid").text("");
            }

            if ($("#edustartdate").val().trim() == "") {
                $("#edustartdatevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#edustartdatevalid").text("");
            }

            if ($("#eduenddate").val().trim() == "") {
                $("#eduenddatevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#eduenddatevalid").text("");
            }*/

            if (errorCount > 0) {
                return false;
            }
        });
    });

  

    $(document).ready(function () {
        $("#addbankdetailsbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#accountholdername").val().trim() == "") {
                $("#accountholdername").focus();
                $("#accountholdernamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#accountholdernamevalid").text("");
            }

            if ($("#bankaccountno").val().trim() == "") {
                $("#bankaccountno").focus();
                $("#bankaccountnovalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#bankaccountnovalid").text("");
            }

            if ($("#bankname").val().trim() == "") {
                $("#bankname").focus();
                $("#banknamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#banknamevalid").text("");
            }

            if ($("#bankbranchname").val().trim() == "") {
                $("#bankbranchnamevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#bankbranchnamevalid").text("");
            }

            if ($("#ifsccode").val().trim() == "") {
                $("#ifsccodevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#ifsccodevalid").text("");
            }

            if ($("#pancard").val().trim() == "") {
                $("#pancardvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#pancardvalid").text("");
            }
            if ($("#aadharcard").val().trim() == "") {
                $("#aadharcardvalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#aadharcardvalid").text("");
            }

            if (errorCount > 0) {
                return false;
            }
        });
    });

    
    // Add Experience Details
	var user_id = "<?php echo $userid; ?>";
	$("#useeexperience").val(user_id);
	// Add Education Degree Details
	$("#addExperiencedetails").submit(function (e) {
	    if ($("#interm").is(":checked") == true) {
	        var test = 1;
	    } else {
	        var test = 0;
	    }
	    var formData = {
	        user_code: $("input[name=editde]").val(),
	        user_id: $("input[name=useeexperience]").val(),
	        company_name: $("input[name=previouscompanyname]").val(),
	        location: $("input[name=location]").val(),
	        job_position: $("input[name=position]").val(),
	        period_from: $("input[name=startdate]").val(),
	        period_to: $("input[name=enddate]").val(),
	        intern: test,
	    };

	    e.preventDefault();
	    $.ajax({
	        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
	        url: base_url + "addExperienceOnboarding", // the url where we want to POST
	        data: formData, // our data object
	        dataType: "json", // what type of data do we expect back from the server
	        encode: true,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader("Token", localStorage.token);
	        },
	    }).done(function (response) {
	        console.log(response);

	        var jsonData = response;
	        var falseData = response["data"];
	        var successData = response["data"]["message"];
	        if (jsonData["data"]["status"] == "1") {
	            toastr.success(successData);
	            setTimeout(function () {
	                window.location = "<?php echo base_url(); ?>onBoardingAddDetails/" + user_id;
	            }, 1000);
	        } else {
	            toastr.error(falseData);
	            $("#addExperiencedetails [type=submit]").attr("disabled", false);
	        }
	    });
	});

	// Add Leave Policy Details
	$("#leavePolicyform").submit(function (e) {
        
	    var formData = {
	        user_id: $("input[name=userparent]").val(),
	        leave_policy_id: $("select[name=leavePolicyname]").val(),
	    };

	    e.preventDefault();
	    $.ajax({
	        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
	        url: base_url + "assignLeavePolicy", // the url where we want to POST
	        data: formData, // our data object
	        dataType: "json", // what type of data do we expect back from the server
	        encode: true,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader("Token", localStorage.token);
	        },
	    }).done(function (response) {
	        console.log(response);

	        var jsonData = response;
	        var falseData = response["data"];
	        var successData = response["data"]["message"];
	        if (jsonData["data"]["status"] == "1") {
	            toastr.success(successData);
	            setTimeout(function () {
	                window.location = "<?php echo base_url(); ?>onBoardingAddDetails/" + user_id;
	            }, 1000);
	        } else {
	            toastr.error(falseData);
	            $("#addParentdetails [type=submit]").attr("disabled", false);
	        }
	    });
	});

	// Add Parent id Details
	var user_id = "<?php echo $userid; ?>";
	$("#userparent").val(user_id);
    $("#userparent_parent_id").val(user_id);
	// Add Education Degree Details
	$("#addParentdetails").submit(function (e) {
	    var formData = {
	        user_id: $("input[name=userparent_parent_id]").val(),
            parent_id: $("select[name=leavePolicyname1]").val(),
	        
	    };

	    e.preventDefault();
	    $.ajax({
	        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
	        url: base_url + "addParentUser", // the url where we want to POST
	        data: formData, // our data object
	        dataType: "json", // what type of data do we expect back from the server
	        encode: true,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader("Token", localStorage.token);
	        },
	    }).done(function (response) {
	        console.log(response);

	        var jsonData = response;
	        var falseData = response["data"];
	        var successData = response["data"]["message"];
	        if (jsonData["data"]["status"] == "1") {
	            toastr.success(successData);
	            setTimeout(function () {
	                window.location = "<?php echo base_url(); ?>onBoardingAddDetails/" + user_id;
	            }, 1000);
	        } else {
	            toastr.error(falseData);
	            $("#addParentdetails [type=submit]").attr("disabled", false);
	        }
	    });
	});
	// Add Education Details
	var user_id = "<?php echo $userid; ?>";
	$("#userEducationtest").val(user_id);
	// Add Education Degree Details
	$("#addEducationdetails").submit(function (e) {
	    var formData = {
	        user_code: $("input[name=editdescription]").val(),
	        user_id: $("input[name=userEducationtest]").val(),
	        institution: $("input[name=institutionname]").val(),
	        subject: $("input[name=subject]").val(),
	        end_date: $("input[name=eduenddate]").val(),
	        grade: $("input[name=grade]").val(),
	        education_degree: $("input[name=highestdegree]").val(),
	    };

	    e.preventDefault();
	    $.ajax({
	        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
	        url: base_url + "addEducationonboarding", // the url where we want to POST
	        data: formData, // our data object
	        dataType: "json", // what type of data do we expect back from the server
	        encode: true,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader("Token", localStorage.token);
	        },
	    }).done(function (response) {
	        console.log(response);

	        var jsonData = response;
	        var falseData = response["data"];
	        var successData = response["data"]["message"];
	        if (jsonData["data"]["status"] == "1") {
	            toastr.success(successData);
	            setTimeout(function () {
	                window.location = "<?php echo base_url(); ?>onBoardingAddDetails/" + user_id;
	            }, 1000);
	        } else {
	            toastr.error(falseData);
	            $("#addEducationdetails [type=submit]").attr("disabled", false);
	        }
	    });
	});
	var user_id = "<?php echo $userid; ?>";
	
	

	var user_id = "<?php echo $userid; ?>";
	$("#user_id").val(user_id);
	// Add Bank Details
	$("#employeeBank").submit(function (e) {
	    var formData = {
	        user_id: $("input[name=user_id]").val(),
	        holder_name: $("input[name=accountholdername]").val(),
	        account_no: $("input[name=bankaccountno]").val(),
	        bank_name: $("input[name=bankname]").val(),
	        branch: $("input[name=bankbranchname]").val(),
	        ifsc: $("input[name=ifsccode]").val(),
	        pan_card: $("input[name=pancard]").val(),
	        aadhar_card: $("input[name=aadharcard]").val(),
	    };

	    e.preventDefault();
	    $.ajax({
	        type: "POST", // define the type of HTTP verb we want to use (POST for our form)
	        url: base_url + "onboardingBankDetails", // the url where we want to POST
	        data: formData, // our data object
	        dataType: "json", // what type of data do we expect back from the server
	        encode: true,
	        beforeSend: function (xhr) {
	            xhr.setRequestHeader("Token", localStorage.token);
	        },
	    }).done(function (response) {
	        console.log(response);

	        var jsonData = response;
	        var falseData = response["data"];
	        var successData = response["data"]["message"];
	        if (jsonData["data"]["status"] == "1") {
	            toastr.success(successData);
	            setTimeout(function () {
	                window.location = "<?php echo base_url(); ?>onBoardingAddDetails/" + user_id;
	            }, 1000);
	        } else {
	            toastr.error(falseData);
	            $("#addemployee [type=submit]").attr("disabled", false);
	        }
	    });
	});
</script>