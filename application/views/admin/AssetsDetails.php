<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Assets">Assets</a></li>
                <li class="breadcrumb-item active">Asset Details</li>
            </ul>
        </div>
        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Assets Info</h3>
                    </div>
                    <div class="row ">
                        <div class="col-lg-6 col-md-12">
                            <div class="card-body">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Assets ID : </strong></small>
                                        <p class="mb-0 text-uppercase" id="assetid"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Asset Category : </strong></small>
                                        <p class="mb-0" id="assetcategory"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Asset Name :</strong> </small>
                                        <p class="mb-0" id="assetname"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Purchase Date : </strong></small>
                                        <p class="mb-0" id="assetpurchasedate"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Purchase From :</strong> </small>
                                        <p class="mb-0" id="purchasefrom"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Purchase By : </strong></small>
                                        <p class="mb-0" id="purchaseby"></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="card-body">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Serial No : </strong></small>
                                        <p class="mb-0 text-uppercase" id="seialno"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Asset Condition : </strong></small>
                                        <p class="mb-0" id="assetcondition"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Asset Warranty : </strong></small>
                                        <p class="mb-0" id="assetwarranty"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted"><strong>Asset Amount : </strong></small>
                                        <p class="mb-0" id="assetamount"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted text-uppercase"><strong>Asset Bill No : </strong></small>
                                        <p class="mb-0 text-uppercase"  id="assetbillno"></p>
                                    </li>
                                    <li class="list-group-item">
                                        <small class="text-muted text-uppercase"><strong>Description : </strong></small>
                                        <p class="mb-0" id="description"></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
    var assets_code = '<?php echo $assetcode; ?>';
    $('#assetcode').val(assets_code);
    $.ajax({
        url: base_url + "viewAssetsCompany",
        method: "POST",
        data: {
            assets_code: assets_code,
        },
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {

            var purchasedate = new Date(response.data[0].purchage_date);
            var dd = String(purchasedate.getDate()).padStart(2, '0');
            var mm = String(purchasedate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = purchasedate.getFullYear();
            purchasedate = dd + '-' + mm + '-' + yyyy;

            $("#assetid").html(response.data[0].assets_code);
            $("#assetcategory").html(response.data[0].category_assets);
            $("#assetname").html(response.data[0].assets_name);
            $("#assetpurchasedate").html(purchasedate);
            $("#purchasefrom").html(response.data[0].purchage_from);
            $("#purchaseby").html(response.data[0].purchage_by);
            $("#seialno").html(response.data[0].serial_no);
            $("#assetcondition").html(response.data[0].assets_cond);
            $("#assetwarranty").html(response.data[0].assets_warranty);
            $("#assetamount").html(response.data[0].assets_amount);
            $("#assetbillno").html(response.data[0].assets_billno);
            $("#description").html(response.data[0].description);
        },
    });

</script>
                        