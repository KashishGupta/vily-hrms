
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
    <?php 

$query1 = $this->db->query("SELECT company_logo FROM `company`");
$arrPolicy = $query1->row();
$arrTest1 = $arrPolicy->company_logo;
$arrTest = $arrPolicy->company_logo;
?>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
        <link rel="icon" href="<?php echo $arrTest;?>" type="image/x-icon" />
        <title><?php echo $title; ?></title>

        <!-- Bootstrap v4.3.1 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />

        <!-- Bootstrap DataTable -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap4.min.css" />

        <!-- FullCalendar v3.9.0 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.min.css" />

        <!-- Datepicker for Bootstrap v1.6.4 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css">

        <!-- Toastr -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/toastr.min.css" />

        <!-- Fontawesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />

        <!-- Custom Style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css" />

        <!-- Text Editor Summernote  -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/summernote.css" />

        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dropify.min.css" />

        <!-- Search Select -->
        <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet" />

        <script>
            var base_url = "<?php echo base_url(); ?>api/callApi/";
            //alert(base_url);
            var base_urlEvents = "<?php echo base_url(); ?>";
            var logIndesignation = localStorage.designationid;
            var logInrole = localStorage.roleid;

            // alert(logInrole);
        </script>
    </head>

    <body class="font-montserrat">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader"></div>
        </div>

        <div id="main_content">
            <div id="header_top" class="header_top">
                <div class="container">
                    <div class="hleft">
                        <a class="header-brand" href="<?php echo base_url(); ?>Dashboard">
                            <img src="<?php echo $arrTest1;?>" />
                        </a>
                        <div class="dropdown">
                            <a href="<?php echo base_url(); ?>Calendar" class="nav-link icon app_inbox" title="Ezdat Calendar-alt"><i class="fa fa-calendar-week"></i></a>
                        </div>

                        <div class="dropdown">
                            <a href="<?php echo base_url(); ?>YellowPageList" class="nav-link icon" title="Employee Contact"><i class="fa fa-id-card"></i></a>
                        </div>

                        <div class="dropdown">
                            <a href="<?php echo base_url(); ?>Holiday" class="nav-link icon" title="Holiday list"><i class="fa fa-plane-departure"></i></a>
                        </div>

                        <div class="dropdown">
                            <a href="<?php echo base_url(); ?>Notification" class="nav-link icon" title="Notification list"><i class="fa fa-bell"></i></a>
                        </div>

                        <div class="dropdown">
                            <a href="<?php echo base_url(); ?>Chat/1" class="nav-link icon" title="Notification list"><i class="fa fa-comments"></i></a>
                        </div>
                    </div>
                    <div class="hright">
                        <div class="dropdown">
                            <a href="javascript:void(0)" class="nav-link icon menu_toggle">
                                <i class="fa fa-align-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="left-sidebar" class="sidebar">
                <h5 class="brand-name text-center">Ezdat Admin</h5>
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul class="metismenu">
                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"> <i class="fa fa-home"></i><span>Home</span> </a>

                            <ul>
                                <li><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                                <li><a href="<?php echo base_url(); ?>Branch">Branch</a></li>
                                <li><a href="<?php echo base_url(); ?>Department">Department</a></li>
                                <li><a href="<?php echo base_url(); ?>Designation">Designation</a></li>
                                <li><a href="<?php echo base_url(); ?>EmployeeList">Employee</a></li>
                                <li><a href="<?php echo base_url(); ?>Leave">Leave</a></li>
                                <li>
                                    <a href="<?php echo base_url(); ?>Attendance"><span>Attendance</span></a>
                                </li>
                               
                            </ul>
                        </li>
                        <li class="metismenu_item">
                            <a href="<?php echo base_url(); ?>Tickets">
                                <i class="fa fa-ticket-alt"></i>
                                <span>Tickets</span>
                            </a>
                        </li>

                        <li class="metismenu_item">
                            <a href="<?php echo base_url(); ?>ProjectDashboard">
                                <i class="fa fa-list-ul"></i>
                                <span>Projects</span>
                            </a>
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c">
                                <i class="fa fa-suitcase"></i>
                                <span>Job Portal</span>
                            </a>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>JobDashboard">Dashboard</a></li>
                                <li><a href="<?php echo base_url(); ?>JobType">Job Type</a></li>
                                <li><a href="<?php echo base_url(); ?>JobStatus">Job Status</a></li>
                                <li><a href="<?php echo base_url(); ?>Jobs">Jobs</a></li>
                            </ul>
                        </li>

                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c">
                                <i class="fa fa-briefcase"></i>
                                <span>Payroll</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>PayrollAddSalary"><span>Add Payroll</span> </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>PayrollSalary"><span>Payroll Salary</span> </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>PayrollView"><span>Payroll View</span> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"> <i class="fa fa-object-ungroup"></i><span>Assets Portal</span> </a>

                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>AssetsCategory"><span>Assets Category</span></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>Assets"><span>Company Assets</span></a>
                                </li>
                                <!-- <li><a href="<?php echo base_url(); ?>AssetsIssue" ><span>Issue Assets</span></a></li> -->
                                <li>
                                    <a href="<?php echo base_url(); ?>AssetsManagement"><span>Assets Management</span></a>
                                </li>
                            </ul>
                        </li>

                        <li class="metismenu_item"><a href="<?php echo base_url(); ?>ReimbursementList"><i class="fa fa-rupee-sign"></i><span>Reimbursement</span></a>
                        </li>

                        <li class="metismenu_item">
                            <a href="<?php echo base_url(); ?>onboardingList"><i class="fa fa-tasks"></i><span>Onboarding</span></a>
                        </li>

                        <li class="metismenu_item">
                            <a href="<?php echo base_url(); ?>offBoardingList"><i class="fa fa-tasks"></i><span>Offboarding</span></a>
                        </li>

                      	<!-- <li class="metismenu_item"><a href="<?php echo base_url(); ?>moduleAddon" ><i class="fa fa-id-badge"></i><span>Module Addon</span></a></li>  -->
                        <li class="metismenu_item">
                            <a href="javascript:void(0)" class="has-arrow arrow-c"><i class="fa fa-cog"></i><span>Settings</span></a>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>Role">Role Base & Permission</a></li>
                                <li><a href="<?php echo base_url(); ?>LeaveType">Leave Type</a></li>
                                <li><a href="<?php echo base_url(); ?>LeavePolicy">Leave Policy</a></li>
                                <li><a href="<?php echo base_url(); ?>ReimbursementPolicy">Reimbursement Policy</a></li>
                                <li><a href="<?php echo base_url(); ?>ChangePassword">Change password </a></li>
                                <li><a href="<?php echo base_url(); ?>Visitor">Visitor </a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="page">
                <div id="page_top" class="section-body sticky-top">
                    <div class="container-fluid">
                        <div class="page-header">
                            <div class="left">
                                <h1 class="page-title">HRMS Dashboard</h1>
                            </div>
                            <div class="right">
                                <div class="notification d-flex">
                                    <div class="dropdown d-flex">
                                        <a class="nav-link icon d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown">
                                            <i class="fa fa-bell"></i>
                                            <span class="badge badge-success nav-unread" id="totalnotification"></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <ul class="right_chat list-unstyled Kt-OpenNotification w250 p-1"></ul>
                                            <div class="dropdown-divider"></div>
                                            <a href="<?php echo base_url()?>Notification" class="dropdown-item text-center text-muted-dark readall"></a>
                                        </div>
                                    </div>
                                    <div class="dropdown d-flex">
                                        <a class="nav-link icon d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown">
                                            <i class="fa fa-user"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="<?php echo base_url(); ?>ProfileAdmin"><i class="dropdown-icon fe fe-user"></i> Admin Profile</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="<?php echo base_url(); ?>CompanySetting"><i class="dropdown-icon fe fe-settings"></i> Company Setting</a>
                                        </div>
                                    </div>
                                    <div class="dropdown d-flex">
                                        <a class="nav-link icon d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown">
                                            <i class="fa fa-power-off"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <form id="logoutdata" method="POST">
                                                <button class="dropdown-item submit-btn" style="border: none; background: none; padding-right: 100px;"><i class="dropdown-icon fe fe-log-out"></i> Log out</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    button.dropdown-item.submit-btn:hover {
                        background: #e8e9e9 !important;
                    }
                </style>

                <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
                <script>
                    //Show Project

                    // Log Out form
                    $("#logoutdata").submit(function (e) {
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: base_url + "logout",
                            data: {},
                            dataType: "json",
                            encode: true,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Token", localStorage.token);
                            },
                        }).done(function (response) {
                            console.log(response);
                            localStorage.clear();
                            window.location.replace("<?php echo base_url();?>");
                        });
                    });

                    // Token Expire API
                    $.ajax({
                        type: "POST",
                        url: base_url + "viewHoliday",
                        data: {},
                        dataType: "json",
                        encode: true,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr);
                            console.log(thrownError);
                            console.log(ajaxOptions);
                            if (xhr.status === 401) {
                                window.location = "<?php echo base_url()?>";
                            }
                        },
                    }).done(function (response) {
                        console.log(response);
                        var resData = response["data"];
                        if (resData == "Unauthorized Token!") {
                            window.location = "<?php echo base_url()?>";
                        } else if (resData == "Token Time Expired.") {
                            window.location = "<?php echo base_url()?>";
                        } else {
                        }
                    });
                    // notification tag get id
                    function getValue(id) { 
                        var notification_id = id;
                       // alert(users_id); 
                            var status = 1;
                            $.ajax({
                            type: "POST",
                            url: base_url + "notificationRead",
                            data: "notification_id=" + notification_id + "&status=" + status,
                            dataType: "json",
                            encode: true,
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Token", localStorage.token);
                            },
                            }).done(function (response) {
                            var jsonDa = response;
                            var jsonData = response["data"]["message"];
                        
                                toastr.success(jsonData);
                                setTimeout(function () {
                                    window.location = "<?php echo base_url()?>Notification";
                                }, 1000);
                            
                        });
                    } 
                    // View Notification Public
                    $.ajax({
                        url: base_url + "viewNotification",
                        data: {},
                        type: "POST",
                        dataType: "json",
                        encode: true,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },
                    }).done(function (response) {
                        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-lg-12"><div class="card"><div class="card-header"></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! There is No Notification</p></div></div></div>';
            $(".Kt-OpenNotification").append(html);
        } else {
                        for (i in response.data) {
                           
                            var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                            var html =
                                '<li class="online"> <a href="#" onclick="getValue('+response.data[i].id+');"> <div class="media"> <img class="media-object " src="' +
                            UserImage +
                            '" alt=""> <div class="media-body"> <span class="name">' +
                                response.data[i].sender_name +
                                ' <small class="float-right">' +
                                timeago(response.data[i].created_at) +
                                '</small></span> <span class="message">' +
                                response.data[i].message +
                                '</span> <span class="badge badge-outline status"></span> </div> </div> </a> </li>';
                            $(".Kt-OpenNotification").append(html);
                        }
                    }
                    });
                    function timeago(stringDate) {
                        // alert(stringDate);
                        var currDate = new Date();
                        var diffMs = currDate.getTime() - new Date(stringDate).getTime();

                        var sec = diffMs / 1000;
                        if (sec <= 60) return '<span style = "color:red;">just now</span>';
                        var min = sec / 60;
                        if (min < 60) return parseInt(min) + " minute" + (parseInt(min) > 1 ? "s ago" : " ago");
                        var h = min / 60;
                        if (h <= 24) return parseInt(h) > 1 ? parseInt(h) + " hrs ago" : " an hour ago";
                        var d = h / 24;
                        if (d <= 7) return parseInt(d) + " day" + (parseInt(d) > 1 ? "s ago" : " ago");
                        var w = d / 7;
                        if (w <= 4.3) return parseInt(w) > 1 ? parseInt(w) + " weeks ago" : " a week ago";
                        var m = d / 30;
                        if (m <= 12) return parseInt(m) + " month" + (parseInt(m) > 1 ? "s ago" : " ago");
                        var y = m / 12;
                        return parseInt(y) + " year" + (parseInt(y) > 1 ? "s ago" : " ago");
                    }

                    // all new totalnotification count
                    $.ajax({
                        url: base_url + "viewTotalNotification",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#totalnotification").html(response.data[0].Notification);
                        },
                    });
                    // all new Employe count
                    /*$.ajax({
                        url: base_url + "countAllNewEmployee",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#totalnewemployees").html(response.data[0].NewEmployee);
                        },
                    });*/

                    //Total Tickets
                    /*$.ajax({
                        url: base_url + "viewTicketsTotal",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#totalticketsheader").html(response.data[0].id);
                            var number = response.data[0].id;
                            var percentToGet = 100;
                            var percent = (percentToGet / 100) * number;
                            $("#totalpercent").html(percent);
                        },
                    });*/
                    //Total Approved Jobs
                    /*$.ajax({
                        url: base_url + "viewApprovedDateJob",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#totalapprovedjob").html(response.data[0].total_approved);
                        },
                    });*/
                    // Real Time
                    /*function timedMsg() {
                        var t = setInterval("change_time();", 1000);
                    }*/
                    // Change Time
                  /*  function change_time() {
                        var de = new Date();
                        var dateObj = new Date("May 18, 89 08:50:05");

                        var difference = de.getTime() - dateObj.getTime();
                        var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                        difference -= daysDifference * 1000 * 60 * 60 * 24;

                        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
                        difference -= hoursDifference * 1000 * 60 * 60;

                        var minutesDifference = Math.floor(difference / 1000 / 60);
                        difference -= minutesDifference * 1000 * 60;

                        var secondsDifference = Math.floor(difference / 1000);

                        document.getElementById("Hour1853").innerHTML = hoursDifference + ":" + minutesDifference + ":" + secondsDifference + " (Realtime Hours)";
                    }
                    timedMsg();*/

                  /*  $.ajax({
                        url: base_url + "checkUserBirthday",
                        method: "POST",
                        dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Token", localStorage.token);
                        },

                        success: function (response) {
                            $("#name").html(response.data[0].first_name);
                        },
                    });*/
                </script>