<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Events</li>
            </ul>
        </div>

        <div class="row mt-3">

            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addNewEvent">Add New Event</button>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header"><h3 class="card-title">Today Events</h3></div>
                    <div class="card-body">
                        <div class="event-name">
                            <div class="row Kt-events"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add New Event popup -->
<div class="modal custom-modal fade" id="addNewEvent" role="dialog" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-header">
                <h5 class="modal-title">Add an event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="addevents" method="POST" action="#">
                    <div class="row">

                        <div class="form-group col-lg-6 col-md-12">
                            <label class="control-label">Event Name<span class="text-danger"> *</span></label><br />
                            <span id="addeventnamevalid" class="text-danger change-pos"></span>
                            <input class="form-control" placeholder="Enter event name." type="text" name="addsubject" id="addsubject" />
                        </div>

                        <div class="form-group col-lg-6 col-md-12">
                            <label class="control-label">Event Place<span class="text-danger"> *</span></label><br />
                            <span id="addeventplacevalid" class="text-danger change-pos"></span>
                            <input class="form-control" placeholder="Enter event place." type="text" name="addeventlocation" id="addeventlocation" />
                        </div>

                        <div class="form-group col-lg-6 col-md-12">
                            <label class="control-label">Event Date<span class="text-danger"> *</span></label><br />
                            <span id="addeventdatevalid" class="text-danger change-pos"></span>
                            <input class="form-control" placeholder="Select event date." type="date" name="addeventdate" id="addeventdate" />
                        </div>

                        <div class="form-group col-lg-6 col-md-12">
                            <label class="control-label">Event Time<span class="text-danger"> *</span></label><br />
                            <span id="addeventtimevalid" class="text-danger change-pos"></span>
                            <input class="form-control" placeholder="Enter event time." type="time" name="addeventtime" id="addeventtime" />
                        </div>

                        <div class="form-group col-md-12">
                            <label>Event Description<span class="text-danger"> </span></label><br />
                            <span id="addjobtypedescriptionvalidate" class="text-danger change-pos"></span>
                            <textarea name="eventdescription" id="eventdescription" class="form-control" type="text" placeholder="Please enter Event description." autocomplete="off"></textarea>
                        </div>
                    </div>

                    <div class="form-group col-md-12 text-right">
                        <button id="addeventbutton" class="btn btn-success submit-btn">Submit</button>
                        <button type="reset" class="btn btn-secondary test" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
	//For remove validation and empty input field
    $(document).ready(function () {
        $(".test").click(function () {
            jQuery("#addeventnamevalid").text("");
            jQuery("#addeventplacevalid").text("");
            jQuery("#addeventdatevalid").text("");
            jQuery("#addeventtimevalid").text("");
        });
    });
    $("form button[data-dismiss]").click(function () {
        $(".modal").click();
    });

    // Show Today events
    $.ajax({
        url: base_url + "viewTodayEvent",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            console.log(response);
            var UserTime = "";
            if (!$.trim(response.data[0])) {
                var html =
                    '<div class="col-lg-12"><div class="card"><div class="card-header"><h3 class="card-title"></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! Event Not available Today</p></div></div></div>';
                $(".Kt-events").append(html);
            } 
            else {
                for (i in response.data) {
                    const d = new Date(response.data[i].event_date);
                    const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
                    const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
                    const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
                    endDate = `${da} ${mo} ${ye}`;
                    var html =
                        '<div class="col-md-12"><strong>Event Name:</strong></div><div class="col-md-12">' + response.data[i].subject  + '</div><div class="col-md-12"><strong>Time:</strong></div><div class="col-md-12">' + response.data[i].event_time  + '</div><div class="col-md-12"><strong>Location:</strong></div><div class="col-md-12">' + response.data[i].event_location  + '</div><div class="col-md-12"><strong>Description:</strong></div><div class="col-md-12" style="border-bottom: 1px solid #eee; margin-bottom: 14px; padding-bottom: 15px;">' + response.data[i].description  + '</div>' ;
                    $(".Kt-events").append(html);
                }
            }
            $("#userLastLogin").html(UserTime);
        },
    });

    // Add  Events
    $("#addevents").submit(function (e) {
        var formData = {
            event_date: $("input[name=addeventdate]").val(),
            event_time: $("input[name=addeventtime]").val(),
            event_location: $("input[name=addeventlocation]").val(),
            subject: $("input[name=addsubject]").val(),
            description: $("textarea[name=eventdescription]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST", 
            url: base_url + "addEvent", 
            data: formData, 
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Calendar";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addevents [type=submit]").attr("disabled", false);
            }
        });
    });

    //Add Event Validation
	$(document).ready(function () {
	    $("#addeventbutton").click(function (e) {
	        e.stopPropagation();
	        var errorCount = 0;
	        var test = $("#addeventlocation").val();
	        if ($("#addsubject").val().trim() == "") {
	            $("#addeventnamevalid").text("This field can't be empty.");
	            errorCount++;
	        } else {
	            $("#addeventnamevalid").text("");
	        }
	        if ($("#addeventlocation").val().trim() == "") {
	            $("#addeventplacevalid").text("This field can't be empty.");
	            errorCount++;
	        } else {
	            $("#addeventplacevalid").text("");
	        }
	        if ($("#addeventlocation").val().trim() == "") {
	            $("#addeventdatevalid").text("This field can't be empty.");
	            errorCount++;
	        } else {
	            $("#addeventdatevalid").text("");
	        }
	         if ($("#addeventtime").val().trim() == "") {
	            $("#addeventtimevalid").text("This field can't be empty.");
	            errorCount++;
	        } else {
	            $("#addeventtimevalid").text("");
	        }
	        if (errorCount > 0) {
	            return false;
	        }
	    });
	});

</script>
