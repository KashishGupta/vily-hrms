<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
				<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>ProjectDashboard">Project</a></li>
				<li class="breadcrumb-item active">Add Project</li>
			</ul>
        </div>
        
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Add Project</strong></h3>
            </div>
            <div class="card-body">
                <form id="addjob" method="POST">
					<div class="row">
						<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Branch<span class="text-danger"> *</span></label><br>
							<span id="addbranchvalidate" class="text-danger change-pos"></span>
                             <input type="hidden" name="projectid" id="projectid">
							<select class="custom-select form-control" name="addbranchid" id="addbranchid" />
							    
							</select>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12">
							<label>Department<span class="text-danger"> *</span></label><br>
							<span id="adddepartmentvalidate" class="text-danger change-pos"></span>
							<select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/>
							    
							</select>
						</div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Project Name<span class="text-danger"> *</span></label><br>
                            <span id="addprojectnamevalit" class="text-danger change-pos"></span>
                            <input type="text" class="form-control " name="addprojectname" id="addprojectname" placeholder="Please enter project name" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Client Name<span class="text-danger"> *</span></label><br>
                            <span id="addclientnamevalit" class="text-danger change-pos"></span>
                            <input type="text" class="form-control" name="addclientname" id="addclientname" placeholder="Please enter client name." autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Created Date<span class="text-danger"> *</span></label><br>
                            <span id="addstartdatevalit" class="text-danger change-pos"></span>
                            <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="addstartdate" id="addstartdate" placeholder="YYYY/MM/DD" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Deadline Date<span class="text-danger"> *</span></label><br>
                            <span id="addenddatevalidate" class="text-danger change-pos"></span>
                            <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="addenddate" id="addenddate" placeholder="YYYY/MM/DD" readonly=""   />
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Project Language<span class="text-danger"> *</span></label>
                            <br>
                            <span id="addprojectlanguagevalidate" class="text-danger change-pos"></span>
                            <textarea class="form-control" id="addprojectlanguage" cols="5" rows="4" name="addprojectlanguage" placeholder="Please enter your project Language eg. HTML, CSS." autocomplete="off"></textarea>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Project File<span class="text-danger"> *</span></label>
                            <br>
                            <span id="addfilevalidate" class="text-danger change-pos"></span>
                            <input type="file" class="dropify form-control" name="file" id="file" onchange="load_file();" data-allowed-file-extensions="pdf xlsx xls doc docx" data-max-file-size="5mb">                 
                            <input type="hidden" id="12arrdata">
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <label>Description<span class="text-danger"> *</span></label><br>
                            <span id="adddescriptionvalidate" class="text-danger change-pos"></span>
                            <textarea class="form-control" name="adddescription" cols="5" rows="4" id="adddescription" autocomplete="off"  placeholder="Please enter project description." /></textarea>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Project Leader<span class="text-danger"> *</span></label><br>
                            <span id="addprojectleadervalit" class="text-danger change-pos"></span>
                            <select class="custom-select form-control" name="addprojectleader" id="addprojectleader" />
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Project Team<span class="text-danger"> *</span></label><br>
                            <span id="addprojectteamvalit" class="text-danger change-pos"></span>
                          
                            <select class="form-control chosen-select" multiple name="addprojectTeam" id="addprojectTeam"  />
                            <?php
                                $query = $this->db->query("SELECT id,first_name FROM `users`");
                                $arrProjectsData = $query->result();
                                foreach($arrProjectsData as $team) { ?>                                                        
                                <option value="<?php echo $team->id; ?>"><?php echo $team->first_name; ?></option>
                            <?php } ?>
                           
                            </select>
                            
                        </div>
                        <div class="col-xs-12 text-right">
                            <span id="maxContentPost"></span>
                        </div>
                        <div class="form-group col-md-12 col-sm-12 text-right">
                            <button id="addjobbutton" class="btn btn-success submit-btn">Submit</button>
                            <button type="reset" id="test" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                        </div>
					</div>
				</form>
            </div>
            
        </div>
    </div>            
</div>  

<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<script>

$(".chosen-select").chosen({
  no_results_text: "Oops, nothing found!"
});
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">


    

    $(document).ready(function () {
        $("#test").click(function () {
            $("#addbranchid option:eq(0)").prop("selected", true);
            $("#adddepartmentid option:eq(0)").prop("selected", true);  

            $("#addprojectleadervalit option:eq(0)").prop("selected", true);       
        });
    });

    //for remove validation and empty input field
        $(document).ready(function() {
            $('#test').click(function(){
                jQuery('#addbranchvalidate').text('');
                jQuery('#adddepartmentvalidate').text('');
                jQuery('#addprojectnamevalit').text('');
                jQuery('#addclientnamevalit').text('');
                jQuery('#addstartdatevalit').text('');
                jQuery('#addenddatevalidate').text('');
                jQuery('#addprojectamountvalit').text('');
                jQuery('#addadvanceamountvalit').text('');
                
                jQuery('#addprojectleadervalit').text('');

                jQuery('#addprojectlanguagevalidate').text('');
                jQuery('#addfilevalidate').text('');
                jQuery('#adddescriptionvalidate').text('');
            });
        });
    function load_file() {
        if (!window.FileReader) {
            return alert("FileReader API is not supported by your browser.");
        }
        var $i = $("#file"), // Put file input ID here
            input = $i[0]; // Getting the element from jQuery
        if (input.files && input.files[0]) {
            file = input.files[0]; // The file
            fr = new FileReader(); // FileReader instance
            fr.onload = function () {
                // Do stuff on onload, use fr.result for contents of file
                $("#12arrdata").val(fr.result);
            };
            //fr.readAsText( file );
            fr.readAsDataURL(file);
        } else {
            // Handle errors here
            alert("File not selected or browser incompatible.");
        }
    }
    
    //Select branch For Filter
        $.ajax({
            url: base_url + "viewactiveBranch",
            data: {},
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#addbranchid");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Branch</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
            });
        });


    // Select Department by select branch For Add Designation Filter
        $("#addbranchid").change(function () {
            $.ajax({
                url: base_url + "viewDepartment",
                data: { branch_id: $("select[name=addbranchid]").val() },
                type: "POST",
                dataType: "json", // what type of data do we expect back from the server
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                let dropdown = $("#adddepartmentid");

                dropdown.empty();

                dropdown.append('<option selected="true" disabled required="true">Choose Department</option>');
                dropdown.prop("selectedIndex", 0);

                // Populate dropdown with list of provinces
                $.each(response.data, function (key, entry) {
                    dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
                });
            });
        });
   //Select Project Lead
   $("#adddepartmentid").change(function () {
     
            $.ajax({
                url: base_url + "viewUserName",
                data: { department_id: $("select[name=adddepartmentid]").val() },
                type: "POST",
                dataType: "json", // what type of data do we expect back from the server
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                let dropdown = $("#addprojectleader");
                

                dropdown.empty();

                dropdown.append('<option selected="true" disabled required="true">Choose Lead</option>');
                dropdown.prop("selectedIndex", 0);

                // Populate dropdown with list of provinces
                $.each(response.data, function (key, entry) {
                    dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
                });
            });
        });
 

    // Add Designation
        $("#addjob").submit(function (e) {
            var formData = {
                'department_id': $("select[name=adddepartmentid]").val(),
                'user_id': $("input[name=projectid]").val(),
                'project_name': $("input[name=addprojectname]").val(),
                'client_name': $("input[name=addclientname]").val(),
                'start_date': $("input[name=addstartdate]").val(),
                'end_date': $("input[name=addenddate]").val(),
                'project_team': $("select[name=addprojectTeam]").val(),
                'project_language': $("textarea[name=addprojectlanguage]").val(),
                'project_leader_id': $("select[name=addprojectleader]").val(),
                'project_description'     :  $('textarea[name=adddescription]').val(),
                'project_file'                    :  $('#12arrdata').val()
                
            };
            e.preventDefault();
            $.ajax({
                xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
                type: "POST",
                url: base_url + "addProject",
                data: formData,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })

            .done(function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url() ?>ProjectDashboard";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#addjob [type=submit]").attr("disabled", false);
                }
            });
        });

    //Add Job Validation
        $(document).ready(function() {
            $("#addjobbutton").click(function(e) {
                e.stopPropagation();
                var errorCount = 0;
                if ($("#addbranchid").val() == null) {
                    $("#addbranchid").focus();
                    $("#addbranchvalidate").text("Please select a branch.");
                    errorCount++;
                } else {
                    $("#addbranchvalidate").text("");
                }
                if ($("#adddepartmentid").val() == null) {
                    $("#adddepartmentid").focus();
                    $("#adddepartmentvalidate").text("Please select a department.");
                    errorCount++;
                } else {
                    $("#adddepartmentvalidate").text("");
                }
                if ($("#addprojectleader").val() == null) {
                    $("#addprojectleadervalit").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addprojectleadervalit").text("");
                }

                if ($("#addprojectTeam").val() == null) {
                    $("#addprojectteamvalit").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addprojectteamvalit").text("");
                }
                
                if ($("#addprojectname").val().trim() == '') {
                    $("#addprojectname").focus();
                    $("#addprojectnamevalit").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addprojectnamevalit").text("");
                }
                if ($("#addclientname").val().trim() == '') {
                    $("#addclientnamevalit").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addclientnamevalit").text("");
                }
                if ($("#addstartdate").val().trim() == '') {
                    $("#addstartdatevalit").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addstartdatevalit").text("");
                }
                if ($("#addenddate").val().trim() == '') {
                    $("#addenddatevalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addenddatevalidate").text("");
                }

                if ($("#addprojectlanguage").val().trim() == '') {
                    $("#addprojectlanguagevalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addprojectlanguagevalidate").text("");
                }
                if ($("#adddescription").val().trim() == '') {
                    $("#adddescriptionvalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#adddescriptionvalidate").text("");
                }
                
                if ($("#file").val().trim() == '') {
                    $("#addfilevalidate").text("This field can't be empty.");
                    errorCount++;
                } 
                else {
                    $("#addfilevalidate").text("");
                }
                if(errorCount > 0){
                    return false;
                }
            });
        });
</script>
