<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Hr/Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Payroll View</li>
            </ul>
        </div>

        <div class="card mt-3" style="width: 600px; margin: 0 auto;">
            <div class="card-header">
                <h3 class="card-title"><strong>Payslip</strong></h3>
            </div>
            
            <div class="card-body" id="dvData">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl " >
                    <thead>
                    <tr>
                        <th>Net Salary</th>
                        <th>PROVIDENT FUND</th>
                        <th>TOTAL</th>
                    </tr>
                        
                    </thead>
                    <tbody id="tablebodypublicjob">
                    <?php 
                             $query = $this->db->query("SELECT basic_salary,pf FROM `user_payroll` WHERE month = '".$montha."' AND year = '".$yeara."'");
                             $arrpayrollData = $query->result();
                             $totalsalary=0;
                             $totalpf=0;
                             foreach($arrpayrollData as $arrpayrollDatas)
                             {
                                $totalsalary+= $arrpayrollDatas->basic_salary;
                                $totalpf+= $arrpayrollDatas->pf;
                             }
                              $totalcountsoverall = $totalsalary+$totalpf;
                            ?>
                    <tr>
                        <td>₹ <?php echo $totalsalary;?></td>
                        <td>₹ <?php echo $totalpf;?></td>
                        <td>₹ <?php echo $totalcountsoverall;?> </td>
                    
                    </tbody>
                </table>
                <br>
                <br>
               
                <a href="#" id ="export" role='button' class="btn btn-primary">Download Payslip CSV
                </a>
          

                
            </div>
        </div>
    </div>
</div>

 <!-- Scripts ----------------------------------------------------------- -->
 <script type='text/javascript' src='https://code.jquery.com/jquery-1.11.0.min.js'></script>
        <!-- If you want to use jquery 2+: https://code.jquery.com/jquery-2.1.0.min.js -->
        <script type='text/javascript'>
        $(document).ready(function () {

            console.log("HELLO")
            function exportTableToCSV($table, filename) {
                var $headers = $table.find('tr:has(th)')
                    ,$rows = $table.find('tr:has(td)')

                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    ,tmpColDelim = String.fromCharCode(11) // vertical tab character
                    ,tmpRowDelim = String.fromCharCode(0) // null character

                    // actual delimiter characters for CSV format
                    ,colDelim = '","'
                    ,rowDelim = '"\r\n"';

                    // Grab text from table into CSV formatted string
                    var csv = '"';
                    csv += formatRows($headers.map(grabRow));
                    csv += rowDelim;
                    csv += formatRows($rows.map(grabRow)) + '"';

                    // Data URI
                    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                $(this)
                    .attr({
                    'download': filename
                        ,'href': csvData
                        //,'target' : '_blank' //if you want it to open in a new window
                });

                //------------------------------------------------------------
                // Helper Functions 
                //------------------------------------------------------------
                // Format the output so it has the appropriate delimiters
                function formatRows(rows){
                    return rows.get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim);
                }
                // Grab and format a row from the table
                function grabRow(i,row){
                     
                    var $row = $(row);
                    //for some reason $cols = $row.find('td') || $row.find('th') won't work...
                    var $cols = $row.find('td'); 
                    if(!$cols.length) $cols = $row.find('th');  

                    return $cols.map(grabCol)
                                .get().join(tmpColDelim);
                }
                // Grab and format a column from the table 
                function grabCol(j,col){
                    var $col = $(col),
                        $text = $col.text();

                    return $text.replace('"', '""'); // escape double quotes

                }
            }


            // This must be a hyperlink
            $("#export").click(function (event) {
                 var outputFile = 'payslip'
               // var outputFile = window.prompt("What do you want to name your output file (Note: This won't have any effect on Safari)") || 'export';
                outputFile = outputFile.replace('.csv','') + '.csv'
                 
                // CSV
                exportTableToCSV.apply(this, [$('#dvData>table'), outputFile]);
                
                // IF CSV, don't do event.preventDefault() or return false
                // We actually need this to be a typical hyperlink
            });
        });
    </script>