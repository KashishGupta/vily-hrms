<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Payroll Salary</li>
            </ul>
           
        </div>

        <!-- Search Payroll Salary -->
       

        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Payroll Salary</strong></h3>
            </div>
            <div class="card-body">
                <form role="form" method="POST"  id="addpayroll">
               
                    <table class="table table-vcenter text-nowrap table_custom border-style table-responsive-xxl list" align="center" id="tablepayroll">    
                        <thead>
                            <tr>    
                                <th class="text-left" style="font-weight: 600;">Employee Name</th>
                                <th class="text-center" style="font-weight: 600;">LOP Days</th>
                                <th class="text-center" style="font-weight: 600;">Bonus</th>
                                <th class="text-center" style="font-weight: 600;">Commission</th>
                                <th class="text-center" style="font-weight: 600;">Reimbursements</th>
                                <th class="text-center" style="font-weight: 600;">LTA</th>
                                <th class="text-center" style="font-weight: 600;">Adjustments</th>
                                <th class="text-center" style="font-weight: 600;">Net Salary</th>
                            </tr>   
                        </thead>
                        <tbody id="tablebodypayroll"></tbody>
                    </table>
                    <br>
                    <br>
                    <br>
                    <div class="form-group text-right" style="width: 100%">
                    <button id="addbranchbutton" class="btn btn-success submit-btn">Submit</button>
                       
                        <button type="reset" class="btn btn-secondary" id="test">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /Search Payroll Salary -->

       

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>


         $.ajax({
            url: base_url + "viewpayrollCreateSearch",
            data: {},
            type: "POST",
            dataType: "json",
            timeout: 6000,
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            $("#tablepayroll").DataTable().clear().destroy();
            $("#tablepayroll tbody").empty();
            var table = document.getElementById("tablebodypayroll");
            for (i in response.data) {
               // alert(response.data[i]);
                var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "http://52.86.51.162/ezdatechnology/assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                var tr = document.createElement("tr");
                tr.innerHTML =
                '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage +'" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +response.data[i].user_code +'" title="">' +response.data[i].emp_id +' - ' + response.data[i].first_name + '</a> <p class="mb-0">₹' + response.data[i].total_amount_monthly + '.00</p> </div> </div></td>' +
                '<td class="text-center"><div class="form-group"><input style="width: 150px" type="text" name="lopdays[]" id="lopdays" class="form-control text-right" value="0" ><input style="width: 150px" type="hidden" name="user_id[]" id="user_id" class="form-control text-right" value="' + response.data[i].id + '" > </div></td>' +
                '<td class="text-center"><div class="form-group"><input style="width: 150px" type="text" name="bonus[]" id="bonus" class="form-control text-right" value="0" ></div></td>' +
                '<td class="text-center"><div class="form-group"><input style="width: 150px" type="text" name="commission[]" id="commission" class="form-control text-right" value="0" ></div></td>' +
                '<td class="text-center"><div class="form-group"><input style="width: 150px" type="text" name="reimbursements[]" id="reimbursements" class="form-control text-right" value="0" > </div></td>' +
                '<td class="text-center"><div class="form-group"><input style="width: 150px" type="text" name="lta[]" id="lta" class="form-control text-right" value="0" ></div></td>' +
                '<td class="text-center"><div class="form-group"><input style="width: 150px" type="text" name="adjestments[]" id="adjestments" class="form-control text-right" value="0" ></div></td>' +
                '<td class="text-center"><div class="form-group"><input style="width: 150px" type="text" name="net_salary[]" id="net_salary" class="form-control text-right" value="' + response.data[i].total_amount_monthly + '.00" ><input style="width: 150px" type="hidden" name="month[]" id="month" class="form-control text-right" value="<?php echo $montha?>" ><input style="width: 150px" type="hidden" name="year[]" id="year" class="form-control text-right" value="<?php echo $yeara?>"><input style="width: 150px" type="hidden" name="pf[]" id="pf" class="form-control text-right" value="' + response.data[i].pf_monthly + '" ></div> </td>';
               
                table.appendChild(tr);
            }

            $("#tablepayroll").DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: ' Branch Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: ' Branch Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1 ]
                        }
                    }
                ]
            });
        });
     var month = <?php echo $montha;?>;
     var year = <?php echo $yeara;?>;
    //Declined Leave
    $("#addpayroll").on('submit',function(){
        event.preventDefault();
      var formdata =  $( this ).serialize();
      $.ajax({
            type: "POST",
            url: base_url + "addPayroll",
            data: formdata,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonData = response;
            var falseData = response["data"];
            var successData = "Payroll Created Successfully.";
            if (jsonData["data"] == null) {
                toastr.success(successData);
                setTimeout(function () {
                    window.location = "<?php echo base_url(); ?>PayrollView2/"+month+ "/"+year;
                }, 1000);
            }
            else {
                toastr.error(falseData);
                $("#addpayroll [type=submit]").attr("disabled", false);
            }
        });
      });
   
</script>
