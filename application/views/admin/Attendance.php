<link rel="stylesheet" href="<?php echo base_url()?>assets/css/c3.min.css" />
<style type="text/css">
    .leave-name {
        font-size: 16px;
    }
</style>
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Attendance</li>
            </ul>
        </div>
        <div class="row mt-3">
            <div class="col-md-4 col-sm-12">
                <div class="card">
                	<div class="card-header">
                        <h3 class="card-title"><strong>Employment Growth</strong></h3>
                    </div>
                    <div class="card-body">
                        <div id="chart-donut" style="height: 17rem"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="leave-name">Full Day Leave</div>
                                <div class="py-4 m-0 text-center h1" id="fulldayleave"></div>
                                <div class="d-flex">
                                    <small class="text-muted">This Month</small>
                                    <div class="ml-auto" id="totalLeaveMonth"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="leave-name">Half Day Leave</div>
                                <div class="py-4 m-0 text-center h1" id="halfdayleave"></div>
                                <div class="d-flex">
                                    <small class="text-muted">This Month</small>
                                    <div class="ml-auto" id="totalHalfMonth"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="leave-name">Short Leave</div>
                                <div class="py-4 m-0 text-center h1" id="shortdayleave"></div>
                                <div class="d-flex">
                                    <small class="text-muted">This Month</small>
                                    <div class="ml-auto" id="totalshortMonth"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="leave-name">Total Absent</div>
                                <div class="py-4 m-0 text-center h1 text-danger" id="totalonabsent"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="leave-name">Latecomers</div>
                                <div class="py-4 m-0 text-center h1 text-blue" id="latecomers"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="leave-name">On-Time</div>
                                <div class="py-4 m-0 text-center h1 text-success" id="ontime"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-status bg-orange"></div>
            <div class="card-header">
                <h3 class="card-title"><strong>Employee's Attendance List Today</strong></h3>
                <div class="card-options">
                    <p id="time"></p>
                    <a href="<?php echo base_url(); ?>AttendanceReport" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xl" id="employeetablebody">
                    <thead>
                        <tr>
                            <th class="text-left">Employee Name</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Punch IN / OUT</th>                           
                            <th class="text-center">Status</th>
                        </tr>
                    </thead>
                    <tbody id="employeetable"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Attendance Modal -->
<div class="modal custom-modal fade" id="attendance_info" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document" id="content">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Attendance Info</h5>
                <button type="button" class="close" onclick="location.href='<?php echo base_url()?>Attendance'" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card punch-status">
                            <div class="card-body">
                                <h5 class="card-title">Timesheet <small class="text-muted" id="totalVal"></small></h5>
                                <div class="punch-det">
                                    <h6>Punch In at</h6>
                                    <p id="datevalpunch"></p><p id="datevalpunchTime"></p>
                                </div>
                                <div class="punch-info">
                                <div class="punch-hours"><span id="datetime"></span></div>
                                </div>
                                <div class="punch-det">
                                    <h6>Punch Out at</h6>
                                    <p id="datevalpunchOut"></p><p id="datevalpunchTimeOut"></p>
                                </div>
                                <div class="statistics">
                                    <div class="row">
                                        <div class="col-md-6 col-6 text-center">
                                            <div class="stats-box">
                                                <p>Break</p>
                                                <h6>1.21 hrs</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-6 text-center">
                                            <div class="stats-box">
                                                <p>Overtime</p>
                                                <h6>3 hrs</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card recent-activity">
                            <div class="card-body">
                                <h5 class="card-title">Activity</h5>
                                <ul class="new_timeline mt-3 kt-widget__items">
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="elementH"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /Attendance Modal -->
<script src="<?php echo base_url()?>assets/js/jquery-3.2.1.min.js"></script>

<script src="<?php echo base_url()?>assets/js/c3.bundle.js"></script>
<script>

    // Half Day Leave
	$.ajax({
        url: base_url + "totalHalfLeaveShort",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
           // alert(response.data[0].totalshortToday);
           // alert(response.data[1].totalHalfToday);
           // alert(response.data[2].totalLeaveToday);
            $("#shortdayleave").html(response.data[0].totalshortToday);
            $("#halfdayleave").html(response.data[1].totalHalfToday);
            $("#fulldayleave").html(response.data[2].totalLeaveToday);
            $("#totalLeaveMonth").html(response.data[3].totalLeaveMonth);
            $("#totalshortMonth").html(response.data[4].totalshortMonth);
            $("#totalHalfMonth").html(response.data[5].totalHalfMonth);
            $("#totalonabsent").html(response.data[6].totalonabsent);
            
            
            
            
        },
    });



	function GetAttendanceData(){
	    return $.ajax({
	        url: base_url+"totalAbsentGraph",
	        // url: http://pratap.ezdatechnology.com/index.php/api/callApi/viewEvent",
	        method:"POST",
	        dataType:"json",
	        beforeSend: function(xhr){
	            xhr.setRequestHeader('Token', localStorage.token);
	        },
	        success:function(response){
	            loadAttendanceGraph(response.data);
	        }

	    });
	}
	GetAttendanceData();
	function loadAttendanceGraph(data){
	    var chart = c3.generate({
	        bindto: '#chart-donut', // id of chart wrapper
	        data: {
	            columns: [
	                // each columns data
	                ['data1', data[0]['data1']],
	                ['data2', data[1]['data2']],
	                ['data3', data[2]['data3']]
	            ],
	            type: 'donut', // default type of chart
	            colors: {
	                'data1': '#ff6384',
	                'data2': '#36a2eb',
	                'data3': '#ffcd56'
	            },
	            names: {
	                // name of each serie
	                'data1': 'Leave',
	                'data2': 'Present',
	                'data3': 'Absent'
	            }
	        },
	        axis: {
	        },
	        legend: {
	            show: true, //hide legend
	        },
	        padding: {
	            bottom: 0,
	            top: 0
	        },
	    });
	}

    $(document).ready(function(){
        var pad = function(num) { return ("0"+num).slice(-2); }
        var totalSeconds = 0;
        $("li").each(function(){
            var currentDuration = $(this).text();
            currentDuration = currentDuration.split(":");
            var hrs = parseInt(currentDuration[0],10);
            var min = parseInt(currentDuration[1],10);
            var sec = parseInt(currentDuration[2],10);
            var currDurationSec = sec + (60*min) + (60*60*hrs); 
            totalSeconds +=currDurationSec;
        });
        var hours = Math.floor(totalSeconds / 3600);
    	totalSeconds %= 3600;
    	var minutes = Math.floor(totalSeconds / 60);
    	var seconds = totalSeconds % 60;
        $(".totalVal").text(pad(hours)+":"+pad(minutes)+":"+pad(seconds));
    });

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        // add a zero in front of numbers<10
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function() {
            startTime()
        }, 500);
    }

    startTime();

    //Show Employee In table
    $.ajax({
        url: base_url + "viewAttendancePublic",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })

    .done(function (response) {
        $("#employeetablebody").DataTable().clear().destroy();
            $("#employeetablebody tbody").empty();
            var table = document.getElementById("employeetable");
            for (i in response.data) {
            var de = new Date();
            var dateObj = new Date('May 18, 89 '+ response.data[i].in_time  +'');
            var difference = de.getTime() - dateObj.getTime();
            var daysDifference = Math.floor(difference/1000/60/60/24);
            difference -= daysDifference*1000*60*60*24;

            var hoursDifference = Math.floor(difference/1000/60/60);
            difference -= hoursDifference*1000*60*60;

            var minutesDifference = Math.floor(difference/1000/60);
            difference -= minutesDifference*1000*60;

            var secondsDifference = Math.floor(difference/1000); 

            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date; 
            var attendanceDate = new Date(dateTime);
            var dd = String(attendanceDate.getDate()).padStart(2, '0');
            var mm = String(attendanceDate.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = attendanceDate.getFullYear();
            attendanceDate = dd + '-' + mm + '-' + yyyy; 

            var finalstatus = "";
            if(response.data[i].status == 0)
            {
                finalstatus = 'Present';
            }
            if(response.data[i].status == null)
            {
                finalstatus = 'Absent'; 
            }
            var status_color = "";
            if (response.data[i].status == 0) {
                status_color = "green";
            }
            if (response.data[i].status == null) {
                status_color = "red";
            }
            var UserImage = "";
            if (!$.trim(response.data[i].user_image)) {
                UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
            } else {
                UserImage = response.data[i].user_image;
            }
           
            var tr = document.createElement("tr");
            tr.innerHTML =

            '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage + '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' + response.data[i].user_code + '" title="">' + response.data[i].emp_id + ' - ' + response.data[i].first_name + '</a> <p>' + response.data[i].email + '</p> </div> </div></td>' +

            '<td class="text-center">' + attendanceDate + '</td>' +


            '<td class="text-center"><a href="#" class="btn btn-info edit_data" data-toggle="modal" data-target="#attendance_info" id="'+response.data[i].id + '">IN / OUT</a></td>' +
               
            '<td class="text-center" style="color:' + status_color + '">' +finalstatus + '</td>';

            table.appendChild(tr);
        }

        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#employeetablebody").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Employee Attendance Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Employee Attendance Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3 ]
                    }
                }
            ]
        });
    });

    //View Attendance List
    $(document).on("click", ".edit_data", function () {
        var attendance_id = $(this).attr("id");

        $.ajax({
            url: base_url + "viewAttendancePerDay",
            method: "POST",
            data: {
                attendance_id: attendance_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                const d = new Date(response.data[0].date);
                const ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
                const mo = new Intl.DateTimeFormat("en", { month: "short" }).format(d);
                const da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
                endDate = `${da} ${mo} ${ye}`;
                $('#dateval').html(endDate);
                $('#datevalpunch').html(endDate);
                $('#datevalpunchOut').html(endDate);
                if(response.data[0].out_time == null)
                {
                    var Timeend = '00:00'; 
                }
                else{
                    var Timeend = response.data[0].out_time;
                }
                var Timestart = response.data[0].in_time;
                $('#datevalpunchTime').html(Timestart);
                for (i in response.data) {
                    if(response.data[i].out_time == null)
                    {
                        var Timeend = '00:00:00'; 
                    }
                    else{
                        var Timeend = response.data[i].out_time;
                    }
                    $('#datevalpunchTimeOut').html(Timeend);
                    var Timestart = response.data[i].in_time;
                    start = Timestart.split(":");
                    end = Timeend.split(":");
                    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
                    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
                    var diff = endDate.getTime() - startDate.getTime();
                    var hours = Math.floor(diff / 1000 / 60 / 60);
                    diff -= hours * 1000 * 60 * 60;
                    var minutes = Math.floor(diff / 1000 / 60);
                    var test =  (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes;
                    console.log(test);
                    $('#datetime').html(test);
                    var html = '<li><div class="bullet pink"></div><div class="desc"><h3>Punch In at</h3></div><div class="time">'+response.data[i].in_time+'</div></li><li><div class="bullet pink"></div><div class="desc"><h3>Punch Out at</h3></div><div class="time">'+Timeend+'</div></li>';
                    $(".kt-widget__items").append(html);
                }
                $("#edit_branch").modal("show");
            },
        });
    });
</script>