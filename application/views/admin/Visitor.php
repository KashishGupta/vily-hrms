<style type="text/css">
	.avatar:hover {
		transform: scale(2);
	}
</style>
<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Visitor</li>
            </ul>
        </div>
        <!-- Search Visitor Result -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Visitor Result</strong></h3>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl" id="visitortable">
                    <thead>
                        <tr>
                            <th class="text-left">Visitor Name</th>                            
                            <th class="text-center">Visitor Address</th>
                            <th class="text-center">Purpose Visit</th>
                            <th class="text-center">Visit Time</th>
                            <th class="text-left">Employee Name</th>
                            <th class="text-left">Branch / Department / Designation</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyvisitor"></tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap4.min.js"></script>

    <!-- For Pdf Excel CSv -->
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
<script>


    //Show Leave list
    $.ajax({
        url: base_url + "viewVisitos",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        $("#visitortable").DataTable().clear().destroy();
        var table = document.getElementById("tablebodyvisitor");
        for (i in response.data) {

            var UserImage = "";
            if (!$.trim(response.data[i].visitors_image_url)) {
                UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
            } else {
                UserImage = response.data[i].visitors_image_url;
            }

            var UserImage1 = "";
            if (!$.trim(response.data[i].user_image)) {
                UserImage1 = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
            } else {
                UserImage1 = response.data[i].user_image;
            }

            var tr = document.createElement("tr");
            tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
                '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                    UserImage +
                    '" alt=""></span><div class="ml-3">  <a href="#" title="">' + response.data[i].visitor_name + '</a> <p class="mb-0">'+response.data[i].visitor_email+'</p> <p class="mb-0">'+response.data[i].visitorphone+'</p> </div> </div></td>' +

                    '<td class="text-center" >' + response.data[i].purpose_visit + '</td>' +

                    '<td class="text-center" >' + response.data[i].visitor_address + '</td>' +

                    '<td class="text-center" >' + response.data[i].creted_at + '</td>' +

                    '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                    UserImage1 + '" alt=""></span><div class="ml-3">  <a href="#" title="">' + response.data[i].first_name + '</a> <p class="mb-0">'+response.data[i].email+'</p> <p class="mb-0">'+response.data[i].phone+'</p> </div> </div></td>' +


                    '<td ><div class="d-flex align-items-center"><div class="ml-0"> <p class="mb-0"><strong>Branch</strong> - ' +
                        response.data[i].branch_name +
                        '</p> <p class="mb-0"><strong>Department</strong> - ' +
                        response.data[i].department_name +
                        '</p> <p class="mb-0"><strong>Designation</strong> - ' +
                        response.data[i].designation_name +
                        "</p> </div> </div></td>" ;

            table.appendChild(tr);
        }

        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#visitortable").DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: d+ ' Visitor Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5 ]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: d+ ' Visitor Details',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: [ 0, 1, 2, 3, 4, 5 ]
                    }
                }
            ]
        });
    });
    
</script>
