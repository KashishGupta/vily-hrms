<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Change Password</li>
            </ul>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-body">
                <form id="editchangepassword" method="POST" action="#">

                    <div class="form-group col-md-6 col-sm-12">
                        <label>Old Password<span class="text-danger"> *</span></label><br />
                        <span id="addeditoldpassword" class="text-danger change-pos"></span>
                        <input type="password" name="editoldpassword" id="editoldpassword" class="form-control" placeholder="Please Enter Old Password" />
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        <label>New Password<span class="text-danger"> *</span></label><br />
                        <span id="addeditcurrentpassword" class="text-danger change-pos"></span>
                        <input type="password" name="editcurrentpassword" id="editcurrentpassword" class="form-control" placeholder="Please Enter New Password" />
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        <label>Confirm Password<span class="text-danger"> *</span></label><br />
                        <span id="addConfirmPassword" class="text-danger change-pos"></span>
                        <input type="password" id="ConfirmPassword" name="ConfirmPassword" class="form-control" placeholder="Please Enter Confirm Password" />
                        <span id="result" class="text-danger"></span>
                    </div>

                    <span id="msg" class="text-danger"></span>
                    <span id="message" class="text-danger"></span>
                    <div class="submit-section col-sm-12">
                        <button class="btn btn-success submit-btn" id="editchange">Update Password</button>
                        <button type="reset" id="test" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //for remove validation and empty input field
    $(document).ready(function () {
        $("#test").click(function () {
            jQuery("#addeditoldpassword").text("");
            jQuery("#addeditcurrentpassword").text("");
            jQuery("#addConfirmPassword").text("");
            jQuery("#msg").html("");
            jQuery("#message").html("");
            jQuery("#result").html("");
        });
    });

    $("#editchangepassword").submit(function (e) {
        var formData = {
            user_id: localStorage.userid,
            email: localStorage.emailid,
            password: $("input[name=editcurrentpassword]").val(),
            confirm_password: $("input[name=ConfirmPassword]").val(),
            old_password: $("input[name=editoldpassword]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "changePassword",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>changePassword";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editchangepassword [type=submit]").attr("disabled", false);
            }
        });
    });

    //Change Password Validation
    $(document).ready(function () {
        $("#editchange").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#editoldpassword").val().trim() == "") {
                $("#editoldpassword").focus();
                $("#addeditoldpassword").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#addeditoldpassword").text("");
            }
            if ($("#editcurrentpassword").val().trim() == "") {
                $("#editcurrentpassword").focus();
                $("#addeditcurrentpassword").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#addeditcurrentpassword").text("");
            }
            if ($("#ConfirmPassword").val().trim() == "") {
                $("#ConfirmPassword").focus();
                $("#addConfirmPassword").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#addConfirmPassword").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });
</script>
<script>
    // at least one number, one lowercase and one uppercase letter
    // at least six characters
    $(document).ready(function () {
        $("#editchange").click(function (e) {
            $("#result").html(checkStrength($("#editcurrentpassword").val()));
        });
        function checkStrength(password) {
            var strength = 0;
            if (password.length < 6) {
                $("#result").removeClass();
                $("#result").addClass("short");
                return "Password must contain at least 6 characters";
            }
            if (password.length > 7) strength += 1;
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1;
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1;
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
            if (strength < 2) {
                $("#result").removeClass();
                $("#result").addClass("weak");
                return "Weak";
            } else if (strength == 2) {
                $("#result").removeClass();
                $("#result").addClass("good");
                return "Good";
            } else {
                $("#result").removeClass();
                $("#result").addClass("strong");
                return "Strong";
            }
        }
    });
</script>
