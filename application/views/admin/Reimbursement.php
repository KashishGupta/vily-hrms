<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Pending Reimbursement</li>
            </ul>
            <div class="header-action mt-3">
                <a href="<?php echo base_url(); ?>ReimbursementList" class="btn btn-info grid-system "><i class="fe fe-list"></i></a>
            </div>
        </div>

        <div class="row mt-3 clearfix kt-widget__items"></div>
    </div>
</div>

<form id="filter-form" method="POST" action="#">
    
</form>


<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
   $(function () {
        filterEmployeeDash('all');
            $("#filter-form").submit();
        });
        function filterEmployeeDash() {
            $("#filter-form").off("submit");
            $("#filter-form").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#user_id").val();
                if (user_id == null) {
                    user_id = 'all';
                }
            req = {};
            req.user_id = user_id;

    $.ajax({
        url: base_url + "viewReimbursementBranch",
        data: req,
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        if (!$.trim(response.data[0])) {
            var html =
                '<div class="col-xl-12"><div class="card"><div class="card-header"><h3 class="card-title"><strong>Reimbursement</strong></h3></div><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800;    margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div></div>';
            $(".kt-widget__items").append(html);
        } else {
            for (i in response.data) {
                var startDate = new Date(response.data[i].start_date);
                var dd = String(startDate.getDate()).padStart(2, "0");
                var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = startDate.getFullYear();

                startDate = dd + "-" + mm + "-" + yyyy;
                var endDate = new Date(response.data[i].end_date);
                var dd = String(endDate.getDate()).padStart(2, "0");
                var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = endDate.getFullYear();

                endDate = dd + "-" + mm + "-" + yyyy;
                var arrivactiondate = "";
                if (response.data[i].start_date == "0000-00-00") {
                    arrivactiondate = "Nil";
                } else {
                    var start = startDate;
                    arrivactiondate = start;
                }
                var arrivalEnddate = "";
                if (response.data[i].end_date == "0000-00-00") {
                    arrivalEnddate = "Nil";
                } else {
                    var start = endDate;
                    arrivalEnddate = start;
                }
                var touramount = response.data[i].amount;
                var hotelamount = response.data[i].hotel_amount;
                var otheramount = response.data[i].employee_amount;
                var foodamount = response.data[i].food_amount;
                var totalamount = Number(touramount) + Number(hotelamount) + Number(otheramount) + Number(foodamount);
                var html =
                    '<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"><div class="card"><div class="card-body text-center ribbon"><div class="ribbon-box cyan edit_data">Pending</div><h5 class="mt-3 mb-0">' +
                    response.data[i].first_name +
                    "</h5><p>" +
                    response.data[i].email +
                    '</p><ul class="mb-2 list-unstyled d-flex justify-content-center"><li>' +
                    response.data[i].emp_id +
                    "</li><li>" +
                    response.data[i].branch_name +
                    "</li><li>₹ " +
                    totalamount +
                    '</li></ul><a href="<?php echo base_url(); ?>ReimbursementView/' +
                    response.data[i].id +
                    '" class="btn btn-info">View Details</a><div class="row text-center mt-4"><div class="col-lg-6 col-md-6 col-sm-6 border-right"><label class="mb-0">Start Date</label><h4 class="font-18">' +
                    arrivactiondate +
                    '</h4></div><div class="col-lg-6 col-md-6 col-sm-6"><label class="mb-0">End Date</label><h4 class="font-18">' +
                    arrivalEnddate +
                    "</h4></div></div></div></div></div>";
                $(".kt-widget__items").append(html);
            }
        }
    });
});
}
    // Edit StatusReimbursement
    $(document).on("click", ".edit_data", function () {
        var reimbursement_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewReimbursementeditBranch",
            method: "POST",
            data: {
                reimbursement_id: reimbursement_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editstatid").val(response["data"][0]["id"]);
                $("#editstatuschange").val(response["data"][0]["status"]);
                $("#reimbursement_status").modal("show");
            },
        });
    });

    // Edit Reimbursement Active form
    $("#editstatus").submit(function (e) {
        var formData = {
            reimbursement_id: $("input[name=editstatid]").val(),
            status: $("select[name=editstatuschange]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "editchangeStatus",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url() ?>Reimbursement";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editstatus [type=submit]").attr("disabled", false);
            }
        });
    });
</script>
