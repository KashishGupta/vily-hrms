<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Payroll</li>
            </ul>
        </div>

        <div class="row clearfix mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex justify-content-between">
                            <ul class="nav nav-tabs b-none">
                                <li class="nav-item">
                                    <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list"><i class="fa fa-hand-holding-water"></i>Pending Payroll</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="addnew-tab" data-toggle="tab" href="#addnew"><i class="fa fa-thumbs-up"></i>Complete Payroll</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="list" role="tabpanel">

                <!-- Search Employee List Result Table -->
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Payroll Pending List</strong></h3>
                    </div>
                    <div class="card-body">
                        <form id="filter-form" method="POST" action="#">
                            <div class="row">
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Branch <span class="text-danger"> *</span></label><br>
                                    <span id="addbranchvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="addbranchid" id="addbranchid" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Department<span class="text-danger"> *</span></label><br>
                                    <!-- <span id="adddepartmentidvalidate" class="text-danger change-pos"></span> -->
                                    <select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Designation<span class="text-danger"> *</span></label><br>
                                    <!-- <span id="designationidvalidate" class="text-danger change-pos"></span> -->
                                    <select class="custom-select form-control" name="designation_id" id="designation_id" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Users<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="user_id" id="user_id" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="filteremployeebutton" class="btn btn-success submit-btn">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl" id="employeetablebody">
                            <thead>
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-left">Branch / Department / Designation</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="employeedatatable"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="addnew" role="tabpanel">

                <!-- Search Employee List Result Table -->
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Payroll Complete List</strong></h3>
                    </div>

                    <div class="card-body">
                        <form id="filter-form1" method="POST" action="#">
                            <div class="row">
                                 <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Branch <span class="text-danger"> *</span></label><br>
                                    <span id="addbranchvalidate1" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="addbranchid1" id="addbranchid1" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Department<span class="text-danger"> *</span></label><br>
                                    <!-- <span id="adddepartmentidvalidate" class="text-danger change-pos"></span> -->
                                    <select class="custom-select form-control" name="adddepartmentid1" id="adddepartmentid1"/>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Designation<span class="text-danger"> *</span></label><br>
                                    <!-- <span id="designationidvalidate" class="text-danger change-pos"></span> -->
                                    <select class="custom-select form-control" name="designation_id1" id="designation_id1" />
                                        
                                    </select>
                                </div>

                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Users<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate1" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="user_id1" id="user_id1" />
                                        
                                    </select>
                                </div>
                                <div class="form-group col-sm-12 text-right">
                                    <button id="filteremployeebutton1" class="btn btn-success submit-btn">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style list table-responsive table-responsive-xxl" id="employeetablebody1">
                            <thead>
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-left">Branch / Department / Designation</th>
                                </tr>
                            </thead>
                            <tbody id="employeedatatable1"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    function load_file() {
        if (!window.FileReader) {
            return alert("FileReader API is not supported by your browser.");
        }
        var $i = $("#image"), // Put file input ID here
            input = $i[0]; // Getting the element from jQuery
        if (input.files && input.files[0]) {
            file = input.files[0]; // The file
            fr = new FileReader(); // FileReader instance
            fr.onload = function () {
                // Do stuff on onload, use fr.result for contents of file
                $("#12arrdata").val(fr.result);
            };
            //fr.readAsText( file );
            fr.readAsDataURL(file);
        } else {
            // Handle errors here
            alert("File not selected or browser incompatible.");
        }
    }   
   
    // Select branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select department by api*/
    $("#addbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddepartmentid");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#designation_id");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Select Employee by api*/
    $("#designation_id").change(function () {
        $.ajax({
            url: base_url + "viewUserDashboard",
            data: { designation_id: $("select[name=designation_id]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#user_id");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose User</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
            });
        });
    });

    $(function () {
        filterEmployeeDash(4);
            $("#filter-form").submit();
        });
        function filterEmployeeDash() {
            $("#filter-form").off("submit");
            $("#filter-form").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#user_id").val();
                if (user_id == null) {
                    user_id = 4;
                }
            req = {};
            req.user_id = user_id;
            //Show Employee In table
            $.ajax({
                url: base_url + "viewpayrollPending",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                $("#employeetablebody").DataTable().clear().destroy();
                    $("#employeetablebody tbody").empty();
                var table = document.getElementById("employeedatatable");
                for (i in response.data) {

                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                    var testcheck = "";
                    if (response.data[i].is_active == 1) {
                        testcheck = "checked";
                    }
                    if (response.data[i].is_active == 0) {
                        testcheck = "";
                    }

                    var tr = document.createElement("tr");
                    tr.innerHTML =

                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                            UserImage +
                            '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                            response.data[i].user_code +
                            '" title="">' +
                        response.data[i].emp_id +
                        ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +
                        
                        '<td class="text-center">' +
                        response.data[i].phone +
                        "</td>" +
                        '<td ><div class="d-flex align-items-center"><div class="ml-3"> <p class="mb-0"><strong>Branch</strong> - ' +
                        response.data[i].branch_name +
                        '</p> <p class="mb-0"><strong>Department</strong> - ' +
                        response.data[i].department_name +
                        '</p> <p class="mb-0"><strong>Designation</strong> - ' +
                        response.data[i].designation_name +
                        "</p> </div> </div></td>" +

                        '<td class="text-center"><a href="PayrollAddCTC/' + response.data[i].id +'" class="btn btn-info" title="Add CTC" ><i class="fa fa-plus mr-2"></i>Add CTC</a></td>';
                    table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#employeetablebody").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Employee Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Employee Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                });
            });
        });
    }

    //Add Employee Validation
        $(document).ready(function () {
            $("#filteremployeebutton").click(function (e) {
                e.stopPropagation();
                var errorCount = 0;
                if ($("#addbranchid").val() == null) {
                    $("#addbranchid").focus();
                    $("#addbranchvalidate").text("Please select a branch name.");
                    errorCount++;
                } else {
                    $("#addbranchvalidate").text("");
                }

                if (errorCount > 0) {
                    return false;
                }
            });
        });    
</script>

<script type="text/javascript">
    // Select branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid1");

        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Select department by api*/
    $("#addbranchid1").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid1]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddepartmentid1");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Select Designation by api*/
    $("#adddepartmentid1").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid1]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#designation_id1");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Select Employee by api*/
    $("#designation_id1").change(function () {
        $.ajax({
            url: base_url + "viewUserDashboard",
            data: { designation_id: $("select[name=designation_id]").val() },
            type: "POST",
            dataType: "json", // what type of data do we expect back from the server
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#user_id1");

            dropdown.empty();

            dropdown.append('<option selected="true" disabled>Choose User</option>');
            dropdown.prop("selectedIndex", 0);

            // Populate dropdown with list of provinces
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
            });
        });
    });

    $(function () {
        filterEmployeeDash1(4);
            $("#filter-form1").submit();
        });
        function filterEmployeeDash1() {
            $("#filter-form1").off("submit");
            $("#filter-form1").on("submit", function (e) {
                e.preventDefault();
                user_id = $("#user_id1").val();
                if (user_id == null) {
                    user_id = 4;
                }
            req = {};
            req.user_id = user_id;
            //Show Employee In table
            $.ajax({
                url: base_url + "viewpayrollComplete",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                $("#employeetablebody1").DataTable().clear().destroy();
                    $("#employeetablebody1 tbody").empty();
                var table = document.getElementById("employeedatatable1");
                for (i in response.data) {

                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                    var testcheck = "";
                    if (response.data[i].is_active == 1) {
                        testcheck = "checked";
                    }
                    if (response.data[i].is_active == 0) {
                        testcheck = "";
                    }

                    var tr = document.createElement("tr");
                    tr.innerHTML =

                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                            UserImage +
                            '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                            response.data[i].user_code +
                            '" title="">' +
                        response.data[i].emp_id +
                        ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +
                        
                        '<td class="text-center">' +
                        response.data[i].phone +
                        "</td>" +
                        '<td ><div class="d-flex align-items-center"><div class="ml-3"> <p class="mb-0"><strong>Branch</strong> - ' +
                        response.data[i].branch_name +
                        '</p> <p class="mb-0"><strong>Department</strong> - ' +
                        response.data[i].department_name +
                        '</p> <p class="mb-0"><strong>Designation</strong> - ' +
                        response.data[i].designation_name +
                        "</p> </div> </div></td>";
                    table.appendChild(tr);
                }
                var currentDate = new Date()
                var day = currentDate.getDate()
                var month = currentDate.getMonth() + 1
                var year = currentDate.getFullYear()
                var d = day + "-" + month + "-" + year;
                $("#employeetablebody1").DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'excelHtml5',
                            title: d+ ' Employee Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            title: d+ ' Employee Details',
                            pageSize: 'LEGAL',
                            exportOptions: {
                                columns: [ 0, 1, 2 ]
                            }
                        }
                    ]
                });
            });
        });
    }
</script>