<div class="section-body">
    <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center">
            <ul class="breadcrumb mt-3 mb-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Attendance List</li>
            </ul>
        </div>
        <div class="card mt-3">
            <div class="card-status bg-orange"></div>
            <div class="card-header">
                <h3 class="card-title"><strong>Search Employee's Attendance</strong></h3>
            </div>
            <div class="card-body">
                <form id="filter-form" method="POST" action="#">
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>Start Date <span class="text-danger"> *</span></label><br>
                            <span id="addbranchvalidate" class="text-danger change-pos"></span>
                            <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="startDate" id="startDate" placeholder="YYYY/MM/DD" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-12">
                            <label>End Date<span class="text-danger"> *</span></label><br>
                            <span id="adddepartmentidvalidate" class="text-danger change-pos"></span>
                            <input type="text" data-provide="datepicker" data-date-autoclose="true" class="form-control" name="enddate" id="enddate" placeholder="YYYY/MM/DD" readonly="" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-3 col-sm-12 button-open">
                            <button id="filterdesignationbutton" type="submit" class="btn btn-success submit-btn">Search</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title"><strong>Search Attendance Result</strong></h3>
                <div class="card-options">
                    <a href="<?php echo base_url(); ?>Attendance" class="btn btn-info grid-system"><i class="fe fe-grid"></i></a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-hover table-vcenter text-nowrap table_custom border-style table-responsive table-responsive-xxl list" id="onboardingtable">
                    <thead>
                        <tr>
                            <th class="text-left">Emp ID</th>
                            <th class="text-left">Employee Name</th>
                            <th class="text-center">Branch</th>
                            <th class="text-center">Designation</th>
                            <th class="text-center">Status</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tablebodyonboarding">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>            
</div>
<!-- Attendance Modal -->
<div class="modal custom-modal fade" id="attendance_info" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Attendance Info</h5>
                <button type="button" class="close" onclick="location.href='<?php echo base_url()?>AttendanceReport'" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card punch-status">
                            <div class="card-body">
                                <h5 class="card-title">Timesheet <small class="text-muted">11 Mar 2019</small></h5>
                                <div class="punch-det">
                                    <h6>Punch In at</h6>
                                    <p>Wed, 11th Mar 2019 10.00 AM</p>
                                </div>
                                <div class="punch-info">
                                    <div class="punch-hours">
                                        <span>3.45 hrs</span>
                                    </div>
                                </div>
                                <div class="punch-det">
                                    <h6>Punch Out at</h6>
                                    <p>Wed, 20th Feb 2019 9.00 PM</p>
                                </div>
                                <div class="statistics">
                                    <div class="row">
                                        <div class="col-md-6 col-6 text-center">
                                            <div class="stats-box">
                                                <p>Break</p>
                                                <h6>1.21 hrs</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-6 text-center">
                                            <div class="stats-box">
                                                <p>Overtime</p>
                                                <h6>3 hrs</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card recent-activity">
                            <div class="card-body">
                                <h5 class="card-title">Activity</h5>
                                <ul class="new_timeline mt-3 kt-widget__items">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Attendance Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    //Fetch branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    //Fetch department by api*/
    $.ajax({
        url: base_url + "viewDepartmentSelect",
        data: { },
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#adddepartmentid");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Department</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
        });
    });

    //Fetch Designation by api*/
    $.ajax({
        url: base_url + "viewDesignationSelect",
        data: { },
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#designation_id");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Designation</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
        });
    });

    // Fetch Employee Name by api*/
    $.ajax({
        url: base_url + "viewUserName",
        data: { },
        type: "POST",
        dataType: "json", 
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#user_id");
        dropdown.empty();

        dropdown.append('<option selected="true" disabled>Choose User</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.first_name));
        });
    });


    $(function () {
        filterDepartment("all");
        $("#filter-form").submit();
    });
    function filterDepartment() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            start_date = $("#startDate").val();
            end_date = $("#enddate").val();
           //alert(start_date);
            if (start_date == '') {
                start_date = "all";
            }
            req = {};
            req.start_date = start_date; 
            req.end_date = end_date; 
        
            //Show Employee In table
            $.ajax({
                url: base_url + "viewAttendanceFilter",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                $("#onboardingtable").DataTable().clear().destroy();
                    $("#onboardingtable tbody").empty();
                var table = document.getElementById("tablebodyonboarding");
                for (i in response.data) {
                    var attendanceDate = new Date(response.data[i].date);
                    var dd = String(attendanceDate.getDate()).padStart(2, '0');
                    var mm = String(attendanceDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = attendanceDate.getFullYear();
                    attendanceDate = dd + '-' + mm + '-' + yyyy; 
                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }
                    var finalstatus = "";
                    if(response.data[i].status == 0)
                    {
                        finalstatus = 'Present';
                    }
                    if(response.data[i].status == 1)
                    {
                        finalstatus = 'On A Leave'; 
                    }
                    var tr = document.createElement("tr");
                    tr.innerHTML =
                        '<td class="text-center">' + response.data[i].emp_id + '</td>' +

                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage + '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' + response.data[i].user_code + '" title="">' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].email + '</p> </div> </div></td>' +

                        '<td class="text-center">' + response.data[i].branch_name + '</td>' +

                        '<td class="text-center">' + response.data[i].designation_name + '</td>' +

                        '<td class="text-center">' + finalstatus + '</td>' +

                        '<td class="text-center"><a href="#" class="btn btn-info edit_data" data-toggle="modal" data-target="#attendance_info" id="'+response.data[i].id + '">' +attendanceDate + "</a></td>";
                    table.appendChild(tr);
                }
                $("#onboardingtable").DataTable({
                    "dom": "Bfrtip",
                    "buttons": ["excelHtml5", "pdfHtml5"],
                });
            });
        });
    }

    //View Attendance List
    $(document).on("click", ".edit_data", function () {
        var attendance_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewAttendancePerDay",
            method: "POST",
            data: {
                attendance_id: attendance_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                for (i in response.data) {
                    var html = '<li><div class="bullet pink"></div><div class="desc"><h3>Punch In at</h3></div><div class="time">'+response.data[i].in_time+'</div></li><li><div class="bullet pink"></div><div class="desc"><h3>Punch Out at</h3></div><div class="time">'+response.data[i].out_time+'</div></li>';
                    $(".kt-widget__items").append(html);
                }
                $("#attendance_info").modal("show");
            },
        });
    });
</script>