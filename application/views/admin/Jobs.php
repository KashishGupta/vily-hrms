        <div class="section-body">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center">
                    <ul class="breadcrumb mt-3 mb-0">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Jobs</li>
                    </ul>
                    <div class="header-action mt-3">
                        <a href="<?php echo base_url(); ?>JobAdd" class="btn btn-primary"><i class="fe fe-plus mr-2"></i>Add Job</a>
                        
                    </div>
                </div>
                
                <div class="card mt-3">
                    <div class="card-header">
                        <h3 class="card-title"><strong>Search Jobs Result</strong></h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-vcenter text-nowrap table_custom border-style table-responsive table-responsive-xxl list" id="jobtable">
                            <thead>
                                <tr>
                                    <th class="text-center">Job ID</th>
                                    <th class="text-center">Job Title</th>
                                    <th class="text-center">Branch</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">Expire Date</th>
                                    <th class="text-center">Job Type</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Applicants</th>
                                    <th class="text-center">Approvals</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodyjob">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>            
        </div>

        <!-- Status Job Modal -->
        <div id="status_job" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Status Job</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="statusjobs" method="POST">
                            <div class="form-group">
                                <label>Status</label>
                                <select value="" class="custom-select form-control" name="editstatus" id="editstatus">
                                    
                                </select>
                                <input type="hidden" id="statusjobsid" name="statusjobsid" class="form-control">

                            </div>
                            
                            <div class="submit-section pull-right">
                                <button id="editTicketbutton" class="btn btn-success submit-btn">Update Changes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Delete Job Modal -->
        <div class="modal custom-modal fade" id="delete_managejob" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header text-center">
                            <i class="fa fa-ban"></i>
                            <h3>Are you sure want to delete?</h3>
                        </div>
                        <div class="modal-btn delete-action pull-right">
                            <a href="#" class="btn btn-danger continue-btn delete_managejob_button">Delete</a>
                            <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete Job Modal -->

        <!-- Approved Leave Modal -->
        <div id="approved_job" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header">
                            <h3>Job Approve</h3>
                            <p>Are you sure want to approve for this job?</p>
                        </div>
                        <form id="editjob" method="POST" action="#">

                            <input value="" id="editaprrovejobid" name="editaprrovejobid" class="form-control" type="hidden" />

                            <input value="1" id="editaprrovejob" name="editaprrovejob" class="form-control" type="hidden" />

                            <div class="submit-section pull-right">
                                <button id="" class="btn btn-success submit-btn">Yes Approved It!</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Approved Leave Modal -->
        
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        
        <script>

            //Show Job  list
                $.ajax({
                    url: base_url+"viewJob",
                    data: { },
                    type: "POST",
                    dataType    : 'json', // what type of data do we expect back from the server
                    encode      : true,
                    beforeSend: function(xhr){xhr.setRequestHeader('Token', localStorage.token);}
                })
                .done(function(response) { 
                    var j = 1;
                    var table = document.getElementById('tablebodyjob');
                    for (i in response.data) {
                        var startDate = new Date(response.data[i].start_date);
                        var dd = String(startDate.getDate()).padStart(2, '0');
                        var mm = String(startDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = startDate.getFullYear();
                        
                        startDate = dd + '-' + mm + '-' + yyyy;
                        var endDate = new Date(response.data[i].end_date);
                        var dd = String(endDate.getDate()).padStart(2, '0');
                        var mm = String(endDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                        var yyyy = endDate.getFullYear();

                        endDate = dd + '-' + mm + '-' + yyyy;


                      var approval = "";
                      if (response.data[i].approval == 0) {
                        approval =
                            '<div class="dropdown action-label"><a class="btn btn-primary btn-sm btn-rounded  dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-purple"></i> Pending</a><div class="dropdown-menu dropdown-menu-right" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-17px, 31px, 0px); z-index:9999;"> <a class="dropdown-item approved_data" href="#" data-toggle="modal" id="' +
                            response.data[i].job_id +
                            '" data-target="#approved_job"><i class="fa fa-dot-circle-o text-success"></i> Approved</a> </div></div>';
                        }
                        
                        if (response.data[i].approval == 1) {
                            approval = '<div class="dropdown action-label"><a class="btn btn-success btn-sm btn-rounded " href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-success"></i> Approved</a></div>';
                        }

                        var tr = document.createElement('tr');
                        tr.innerHTML = 
                        '<td class="text-center text-uppercase">' + response.data[i].job_number + '</td>' +

                        '<td class="text-center">' + response.data[i].job_title + '</td>' +

                        '<td class="text-center">' + response.data[i].branch_name + '</td>' +

                        '<td class="text-center">' + response.data[i].department_name + '</td>' +

                        '<td class="text-center">' + startDate + '</td>' +

                        '<td class="text-center text-danger">' + endDate + '</td>' +

                        '<td class="text-center" style="color:'+ response.data[i].type_color +'">' + response.data[i].job_type + '</td>' +

                        '<td class="text-center" style="color:'+ response.data[i].status_color +'">' + response.data[i].job_applicant_status + '</td>' +

                        '<td class="text-center"><a href="jobApplicants/'+ response.data[i].job_id +'" class="viewbutton btn-primary btn">'+response.data[i].total_candidate+'</a></td>' +

                        '<td class="text-center">' + approval + '</td>' +
                       
                       '<td class="text-center"><button type="button" data-toggle="modal" data-target="#status_job" aria-expanded="false" class="btn btn-info edit_status" id="'+ response.data[i].job_id +'" title="Edit Status"><i class="fa fa-eye"></i></button> <a href="JobEdit/' + response.data[i].job_id +'" class="btn btn-info edit_data" title="Edit Job" id="'+ response.data[i].job_id +'"><i class="fa fa-edit"></i></a> <button type="button" data-toggle="modal" data-target="#delete_job" aria-expanded="false" class="btn btn-danger delete_data" id="'+ response.data[i].job_id +'" title="Delete Job" ><i class="fa fa-trash-alt"></i></button></td>';
                        table.appendChild(tr)
                    }
                    var currentDate = new Date()
                    var day = currentDate.getDate()
                    var month = currentDate.getMonth() + 1
                    var year = currentDate.getFullYear()
                    var d = day + "-" + month + "-" + year;
                    $("#jobtable").DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend: 'excelHtml5',
                                title: d+ ' Jobs Details',
                                pageSize: 'LEGAL',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                }
                            },
                            {
                                extend: 'pdfHtml5',
                                title: d+ ' Jobs Details',
                                pageSize: 'LEGAL',
                                exportOptions: {
                                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                }
                            }
                        ]
                    });
                });


            //Approved Leave Form Fill
            $(document).on("click", ".approved_data", function () {
                var job_id = $(this).attr("id");
                $.ajax({
                    url: base_url + "viewJob",
                    method: "POST",
                    data: {
                        job_id: job_id,
                    },
                    dataType: "json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Token", localStorage.token);
                    },
                    success: function (response) {
                        
                        $('#editaprrovejobid').val(response["data"][0]["job_id"]);
                      
                        $("#approved_job").modal("show");
                    },
                });
            });

            //Approved Job form
            $("#editjob").submit(function (e) {
                var formData = {
                    job_id: $("input[name=editaprrovejobid]").val(),
                    approval: $("input[name=editaprrovejob]").val(),
                };
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: base_url + "changeJobStatus",
                    data: formData, // our data object
                    dataType: "json",
                    encode: true,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Token", localStorage.token);
                    },
                }).done(function (response) {
                    console.log(response);
                    var jsonDa = response;
                    var jsonData = response["data"]["message"];
                    var falsedata = response["data"];
                    if (jsonDa["data"]["status"] == "1") {
                        toastr.success(jsonData);
                        setTimeout(function () {
                            window.location = "<?php echo base_url()?>Jobs";
                        }, 1000);
                    } else {
                        toastr.error(falsedata);
                        $("#editjob [type=submit]").attr("disabled", false);
                    }
                });
            });


            //Select Job Status by api
                $.ajax({
                    url: base_url+"viewJobApplicantStatus",
                    data: { },
                    type: "POST",
                    dataType    : 'json',
                    encode      : true,
                    beforeSend: function(xhr){xhr.setRequestHeader('Token', localStorage.token);}
                })

                .done(function(response){ 
                    let dropdown = $('#editstatus');
                    dropdown.empty();
                    dropdown.append('<option selected="true" disabled>Select Job Status</option>');
                    dropdown.prop('selectedIndex', 0);
                            
                    // Populate dropdown with list of provinces
                    $.each(response.data, function (key, entry) {
                        dropdown.append($('<option></option>').attr('value', entry.id).text(entry.job_applicant_status ));
                    })
                });
            // Active or inactive ticket form fill
                $(document).on('click', '.edit_status', function() {
                    var job_id = $(this).attr("id");
                    $.ajax({
                        url: base_url + "viewJob",
                        method: "POST",
                        data: {
                            job_id: job_id
                        },
                        dataType: "json",
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success: function(response) {
                        
                            $('#statusjobsid').val(response["data"][0]["job_id"]);
                            $('#editstatus').val(response["data"][0]["job_status"]);
                        
                           
                            $('#status_job').modal('show');
                        }
                    });
                });


             // Active or inactive ticket form edit
                $("#statusjobs").submit(function(e) {
                    var formData = {
                        'job_id'             : $('input[name=statusjobsid]').val(),
                        'job_status'         : $('select[name=editstatus]').val()
                    };

                    e.preventDefault();
                    $.ajax({
                            type: 'POST',
                            url: base_url + 'changeJobStatus',
                            data: formData, // our data object
                            dataType: 'json',
                            encode: true,
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('Token', localStorage.token);
                            }
                        })
                        .done(function(response) {
                            console.log(response);

                            var jsonDa = response;
                            var jsonData = response["data"]["message"]; 
                            var falsedata = response["data"];
                            if (jsonDa["data"]["status"] == "1") {
                                toastr.success(jsonData);
                                setTimeout(function () {
                                    window.location = "<?php echo base_url(); ?>Jobs";
                                }, 1000);
                            } else {
                                toastr.error(falsedata);
                                $("#statusjobs [type=submit]").attr("disabled", false);
                            }
                        });
                });

                
            // Delete Designation */
                $(document).on('click', '.delete_data', function(){  
                    var job_id = $(this).attr("id");  
                    $('.delete_managejob_button').attr('id', job_id);
                    $('#delete_managejob').modal('show');  
                });
                
                
                $(document).on('click', '.delete_managejob_button', function(){  
                    var job_id = $(this).attr("id");  
                    $.ajax({  
                        url:base_url+"deleteJob",  
                        method:"POST",  
                        data:{job_id:job_id},  
                        dataType:"json", 
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success:function(response){
                            console.log(response);
                            var jsonDa = response;                
                            var jsonData = response["data"]["message"];
                            var falsedata = response["data"];
                            
                            if (jsonDa["data"]["status"] == "1") {
                                toastr.success(jsonData);
                                setTimeout(function(){window.location ="<?php echo base_url(); ?>Jobs"},1000);
                              
                            } 

                            else {
                                toastr.error(falsedata);
                                $('#delete_task_button [type=submit]').attr('disabled',false);
                            } 
                        }  
                   });  
                });
                
                
            //add Job validation form
                $(document).ready(function(){
                    $("#addjobbutton").click(function(){
                        
                        if($("#addbranchid").val() == 0)
                        {
                            $("#addbranchid").focus();
                            $("#addbranchvalidate").text("Please select a valid branch.");
                            return;
                        }
                        else
                        {
                            $("#addbranchvalidate").text("");
                        }
                        
                        if($("#adddepartmentid").val() == 0)
                        {
                            $("#adddepartmentid").focus();
                            $("#adddepartmentvalidate").text("Please select a valid department.");
                            return;
                        }
                        else
                        {
                            $("#adddepartmentvalidate").text("");
                        }
                        
                        if($("#adddesignationid").val() == 0)
                        {
                            $("#adddesignationid").focus();
                            $("#adddesignationvalidate").text("Please select a valid designation.");
                            return;
                        }
                        else
                        {
                            $("#adddesignationvalidate").text("");
                        }
                        
                        if($("#addjobtypeid").val() == 0)
                        {
                            $("#addjobtypeid").focus();
                            $("#addjobtypevalidate").text("Please select a valid job type.");
                            return;
                        }
                        else
                        {
                            $("#addjobtypevalidate").text("");
                        }
                        
                        if($("#addstartdate").val() == 0)
                        {
                            $("#addstartdate").focus();
                            $("#addstartdatevalidate").text("Please select a valid start date.");
                            return;
                        }
                        else
                        {
                            $("#addstartdatevalidate").text("");
                        }
                        
                        if($("#addenddate").val() == 0)
                        {
                            $("#addenddate").focus();
                            $("#addenddatevalidate").text("Please select a valid end date.");
                            return;
                        }
                        else
                        {
                            $("#addenddatevalidate").text("");
                        }
                        
                        if($("#addnoofvacancies").val() == 0)
                        {
                            $("#addnoofvacancies").focus();
                            $("#addnoofvacanciesvalidate").text("Please select a valid no of vacancies.");
                            return;
                        }
                        else
                        {
                            $("#addnoofvacanciesvalidate").text("");
                        }
                        
                        if($("#adddescription").val() == 0)
                        {
                            $("#adddescription").focus();
                            $("#adddescriptionvalidate").text("Please enter a valid description.");
                            return;
                        }
                        else
                        {
                            $("#adddescriptionvalidate").text("");
                        }
            
                        $("#showhide").show();
                    });
                });
        </script>
        
        