<!-- Plugins chart css -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/c3.min.css" />
<style type="text/css">
    button.fc-agendaDay-button.btn.btn-secondary.fc-state-default, button.fc-listWeek-button.btn.btn-secondary.fc-state-default.fc-corner-right {
        display: none;
    }
</style>
<div class="section-body mt-3">
    <div class="container-fluid">
        <div class="row clearfix row-deck">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-2 text-center">
                                <div class="welcome-img" id="fgfimage">
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <h4 class="user-name m-t-0 mb-0">Welcome, <span id="profilenamess"></span> </h4>
                                <div class="staff-id" > <span id="joiningdate"></span> </div>
                            </div>
                            <div class="col-sm-3 lastLogin">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">

            <div class="col-6 col-md-4 col-xl-2">
                <div class="card">
                    <div class="card-body ribbon">
                        <div class="ribbon-box cyan" id="totalemployeess"></div>
                        <a href="<?php echo base_url(); ?>EmployeeList" class="my_sort_cut text-muted">
                            <i class="fa fa-users"></i>
                            <span>Employees</span>
                        </a>
                    </div>
                </div>
            </div>   

            <div class="col-6 col-md-4 col-xl-2">
                <div class="card">
                    <div class="card-body ribbon">
                        <div class="ribbon-box cyan" id="presentemployee"></div>
                        <a href="<?php echo base_url(); ?>Attendance" class="my_sort_cut text-muted">
                            <i class="fa fa-address-card"></i>
                            <span>Today Present</span>
                        </a>
                    </div>
                </div>
            </div>   

            <div class="col-6 col-md-4 col-xl-2">
                <div class="card">
                    <div class="card-body ribbon">
                        <div class="ribbon-box cyan" id="totalholiday"></div>
                        <a href="<?php echo base_url(); ?>Holiday" class="my_sort_cut text-muted">
                            <i class="fa fa-plane-departure"></i>
                            <span>Holiday</span>
                        </a>
                    </div>
                </div>
            </div>   

            <div class="col-6 col-md-4 col-xl-2">
                <div class="card">
                    <div class="card-body ribbon">
                        <div class="ribbon-box cyan" id="todayjobs"></div>
                        <a href="<?php echo base_url(); ?>Jobs" class="my_sort_cut text-muted">
                            <i class="fa fa-suitcase"></i>
                            <span>Jobs</span>
                        </a>
                    </div>
                </div>
            </div>   

            <div class="col-6 col-md-4 col-xl-2">
                <div class="card">
                    <div class="card-body ribbon">
                        <div class="ribbon-box cyan" id="totalproject"></div>
                        <a href="<?php echo base_url(); ?>ProjectDashboard" class="my_sort_cut text-muted">
                            <i class="fa fa-list-ul"></i>
                            <span>Projects</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 col-xl-2">
                <div class="card">
                    <div class="card-body ribbon">
                        <div class="ribbon-box cyan" id="totaltickets"></div>
                        <a href="<?php echo base_url(); ?>Tickets" class="my_sort_cut text-muted">
                            <i class="fa fa-ticket-alt"></i>
                            <span>Tickets</span>
                        </a>
                    </div>
                </div>
            </div>   

            <div class="col-xl-6 col-lg-6 col-md-12 d-flex">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Event Calendar</strong></h3>
                    </div>
                    <div class="card-body">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-12 d-flex">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Employment Growth</strong></h3>
                    </div>
                    <div class="card-body" style="height: 343px;">
                        <div id="chart-donut"></div>
                    </div>
                </div>
            </div>      

            <div class="col-xl-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Task List</strong></h3>
                        <div class="card-options">
                           <!-- <a href="#" data-target="#sticky_notes" data-toggle="modal" class="btn btn-info">Sticky Notes</a>-->
                        </div>
                    </div>
                    <div class="card-body todo_list ">
                        <form id="taskChange" method="POST">
                            <table class="table table-hover table-bordered table-responsive text-nowrap mb-0 table-vcenter" id="headerfixed">
                                <thead class="thead-dark" >
                                    <tr>
                                        <th  class="text-left">Task Name</th>
                                        <th class="text-left">Employee Name</th>
                                        <th class="text-center">Due</th>
                                        <th class="text-center">Priority</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="taskdatatable"></tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>      

            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Employee's</strong></h3>
                    </div>
                    <div class="card-body">
                        <form id="filter-form" method="POST" action="#">
                            <div class="row">

                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Branch <span class="text-danger"> *</span></label><br>
                                    <span id="addbranchvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="addbranchid" id="addbranchid" />
                                        
                                    </select>
                                </div>

                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Department<span class="text-danger"> *</span></label><br>
                                    <span id="adddepartmentidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="adddepartmentid" id="adddepartmentid"/>
                                        
                                    </select>
                                </div>

                                <div class="form-group col-lg-3 col-md-6 col-sm-12">
                                    <label>Designation<span class="text-danger"> *</span></label><br>
                                    <span id="designationidvalidate" class="text-danger change-pos"></span>
                                    <select class="custom-select form-control" name="designation_id" id="designation_id" />
                                        
                                    </select>
                                </div>

                                <div class="form-group col-lg-3 col-md-6 col-sm-12 button-open">
                                    <button id="filteremployeebutton" class="btn btn-success submit-btn">Search</button>
                                </div>

                            </div>
                        </form>
                    </div>
                    
                    <div class="card-body">
                        <table class="table table-bordered table-hover mb-0 table-vcenter text-nowrap table-responsive table-responsive-xxl dataTable" id="employeetablebody">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-left">Branch / Department / Designation</th>
                                    <th class="text-center">Assign Task</th>
                                </tr>
                            </thead>
                            <tbody id="employeedatatable"></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-status bg-orange"></div>
                    <div class="card-header">
                        <h3 class="card-title"><strong>Leave Ticket Status</strong></h3>
                        <div class="card-options">
                            <select class="custom-select form-control" id="leavetickettabledata">
                                <option value="leaveselect">Leave</option>
                                <option value="ticketselect">Ticket</option>
                            </select>
                        </div>
                    </div>

                    <div class="card-body data" id="leaveselect">                       
                        <table class="table table-bordered table-hover mb-0 table-vcenter table-responsive table-responsive-xxl text-nowrap dataTable" id="leavetable">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-left">Branch / Department</th>
                                    <th class="text-center">Leave Type</th>
                                    <th class="text-center">From</th>
                                    <th class="text-center">To</th>
                                    <th class="text-center">Total Days</th>
                                    <th class="text-center">Reason</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody id="tablebodyleave"></tbody>
                        </table>
                    </div>

                    <div class="card-body data" id="ticketselect">
                        <table class="table table-bordered table-hover mb-0 table-vcenter table-responsive table-responsive-xl table-responsive-l text-nowrap dataTable" id="mergeleaveticket">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">Ticket Id</th>
                                    <th class="text-left">Employee Name</th>
                                    <th class="text-center">Date</th>
                                    <th class="text-center">Ticket Type</th>
                                    <th class="text-center">Priority</th>
                                    <th class="text-center">Ticket Status</th>                                   
                                </tr>
                            </thead>
                            <tbody id="tablebodymergeleave"></tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--<div id="sticky_notes" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sticky Notes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addtask1" method="POST">
                    <div class="form-group col-md-12">
                        <label>Description<span class="text-danger"> </span></label>
                        </br>
                        <span id="adddescriptionvalidate1" class="text-danger change-pos"></span>
                        <textarea id="adddescription1" name="adddescription1" class="form-control" placeholder="Enter Description" autocomplete="off"></textarea>
                    </div>
                    <div class="submit-section text-right">
                        <button id="addtasknamebutton1" class="btn btn-success submit-btn">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>-->

<!-- Assign Task Modal -->
<div id="add_task" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assign Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="addtask" method="POST">

                    <div class="form-group col-md-12">
                        <label>Task Name<span class="text-danger"> *</span></label>
                        <br />
                        <span id="addtasknamevalidate" class="text-danger change-pos"></span>
                        <input type="text" id="addtaskname" name="addtaskname" class="form-control" placeholder="Please enter Task" autocomplete="off" />
                    </div>

                    <div class="form-group col-md-12">
                        <label>Priority<span class="text-danger"> *</span></label>
                        <br />
                        <span id="addpriorityvalidate" class="text-danger change-pos"></span>
                        <select class="custom-select form-control" name="addpriority" id="addpriority">
                        </select>
                        <input value="" id="adduserid" name="adduserid" class="form-control" type="hidden" />
                    </div>

                    <div class="form-group col-md-12">
                        <label>Due Date<span class="text-danger"> *</span></label>
                        <br />
                        <span id="addduedatevalidate" class="text-danger change-pos"></span>
                        <input id="addduedate" name="addduedate" class="form-control" type="date" autocomplete="off" />
                    </div>

                    <div class="form-group col-md-12">
                        <label>Description<span class="text-danger"> </span></label>
                        </br>
                        <span id="adddescriptionvalidate" class="text-danger change-pos"></span>
                        <textarea id="adddescription" name="adddescription" class="form-control" placeholder="Enter Description" autocomplete="off"></textarea>
                    </div>

                    <div class="submit-section text-right">
                        <button id="addtasknamebutton" class="btn btn-success submit-btn">Add task</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete Assign Task Modal -->
<div class="modal custom-modal fade" id="delete_task" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h4>Are you sure want to delete Task?</h4>
                </div>
                <div class="modal-btn delete-action pull-right">
                <button type="submit" id="" class="btn btn-danger continue-btn delete_task_button box_shadown">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Delete Assign Task Modal -->

<!-- Delete Ticket Modal -->
<div class="modal custom-modal fade" id="delete_ticket" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header text-center">
                    <i class="fa fa-ban" style="font-size: 130px; color: #ff8800;"></i>
                    <h3>Are you sure want to delete?</h3>
                </div>
                <div class="modal-btn delete-action pull-right">
                    <button type="submit" id="" class="btn btn-danger continue-btn delete_ticket_button">Yes delete it!</button>
                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-secondary cancel-btn">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Delete Ticket Modal -->

<!-- Approved Leave Modal -->
<div id="approved_leave" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header">
                    <h3>Leave Approve</h3>
                    <p>Are you sure want to approve for this leave?</p>
                </div>
                <form id="editleave" method="POST" action="#">
               
                <input value="" id="editUserId" name="editUserId" class="form-control" type="hidden" />
                    <input value="" id="Leaveapprovedtype" name="Leaveapprovedtype" class="form-control" type="hidden" />
                    <input value="" id="Leaveapprovedapply" name="Leaveapprovedapply" class="form-control" type="hidden" />
                    <input value="" id="Leaveapprovedfrom" name="Leaveapprovedfrom" class="form-control" type="hidden" />
                    <input value="" id="Leavedays" name="Leavedays" class="form-control" type="hidden" />
                    <input value="" id="editleavetypeid" name="editleavetypeid" class="form-control" type="hidden" />
                    <input value="1" id="editleaveaction" name="editleaveaction" class="form-control" type="hidden" />
                    <div class="submit-section pull-right">
                        <button id="ediapprovedbutton" class="btn btn-success submit-btn">Yes Approved It!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Approved Leave Modal -->

<!-- Denied Leave Modal -->
<div class="modal custom-modal fade" id="denied_leave" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-header">
                    <h3>Leave Denied</h3>
                    <p>Are you sure want to denied for this leave?</p>
                </div>
                <form id="declinedleave" method="POST" action="#">
                    <input value="" id="Leavedelclinedleavetype" name="Leavedelclinedleavetype" class="form-control" type="hidden" />
                    <input value="" id="Leavedelclinedfrom" name="Leavedelclinedfrom" class="form-control" type="hidden" />
                    <input value="" id="Leavedelclinedapply" name="Leavedelclinedapply" class="form-control" type="hidden" />
                    <input value="" id="editLeavedelclinedid" name="editLeavedelclinedid" class="form-control" type="hidden" />
                    <input value="" id="editLeavedelclinedemail" name="editLeavedelclinedemail" class="form-control" type="hidden" />
                    <input value="2" id="editLeavedeclined" name="editLeavedeclined" class="form-control" type="hidden" />
                    <div class="form-group">
                        <label>Leave Declined Reason <span class="text-danger">*</span></label><br>
                        <span id="declineleavevalid" class="text-danger"></span>
                        <textarea rows="3" class="form-control" name="declineleave" id="declineleave" placeholder="Please enter denied reason."></textarea>
                    </div>
                    <div class="submit-section pull-right">
                        <button id="leavedeniedbutton" class="btn btn-success submit-btn">Yes denied It!</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<p><tt id="results"></tt></p>
<!-- /AssignTask Modal -->

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>

<!-- DataTable -->
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap4.min.js"></script>

<!-- For Pdf Excel CSv -->
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
<script>
    $(document).ready(function () {
        $(".modal").click(function () {
            $("input#addtaskname").val("");
            $("input#addduedate").val("");
            $("input#addtodo").val("");
            $("textarea#adddescription").val("");

            $("textarea#addcomments").val("");
            $("#addpriority option:eq(0)").prop("selected", true);
        });
        $(".modal .modal-dialog .modal-body > form").click(function (e) {
            e.stopPropagation();
        });
        $("form button[data-dismiss]").click(function () {
            $(".modal").click();
        });
    });

    $(document).ready(function () {
        $(".modal").click(function () {
            jQuery("#addtasknamevalidate").text("");
            jQuery("#addpriorityvalidate").text("");
            jQuery("#addduedatevalidate").text("");
            jQuery("#editsubjectvalidate").text("");
            jQuery("#addtodovalidate").text("");
        });
    });

    $(document).ready(function () {
        $('.data').hide();
        $('#leaveselect').show();
            $('#leavetickettabledata').change(function () {
            $('.data').hide();
            $('#'+$(this).val()).show();
        })
    });
</script>

<!-- Charts Homepage -->
<script src="<?php echo base_url(); ?>assets/js/c3.bundle.js"></script>
<script src="<?php echo base_url(); ?>assets/js/c3.js"></script>
<script type="text/javascript">
    //View Profile
    $.ajax({
        url: base_url + "viewprofilePublic",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            if (!$.trim(response.data[0].user_image)) {
                $("#fgfimage").append('<img src="<?php echo base_url();?>assets/images/dummy/person-dummy.jpg" />');
            } else {
                $("#fgfimage").append('<img src="' + response.data[0].user_image + '" />');
            }
            $("#profilenamess").html(response.data[0].first_name);
            $("#joiningdate").html(response.data[0].email);
        },
    });

    // Show User Last Login
    $.ajax({
        url: base_url + "viewLastLogin",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            console.log(response);
            if (!$.trim(response.data[0])) {
            } else {
                var html = '<p class="login-status">Last Login- <span>' + response.data[0].time + "</span></p>";
                $(".lastLogin").append(html);
            }
        },
    });

    //Show Total Employee in 2nd row Section
    $.ajax({
        url: base_url + "totalEmployee",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalemployeess").html(response.data[0].id);
        },
    });

    // Total Present Employee in 2nd row Section
    $.ajax({
        url: base_url + "viewAttendanceTodayPresent",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            var presentData = response.data[0].presentEmployee;
          //alert(presentData);
            $("#presentemployee").html(presentData);
            
        },
    });

    // Total Holiday Employee in 2nd row Section
    $.ajax({
        url: base_url + "viewTotalHoliday",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            var presentData = response.data[0].Present;
            $("#totalholiday").html(response.data[0].totalHoliday);
        },
    });

    //Show Total Today Job
    $.ajax({
        url: base_url + "totalJobs",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#todayjobs").html(response.data[0].totalJobs);
        },
    });

    //Show Total Project
    $.ajax({
        url: base_url + "totalProject",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totalproject").html(response.data[0].totalProjects);
        },
    });

    //Total Tickets
    $.ajax({
        url: base_url + "viewTicketsTotal",
        method: "POST",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
        success: function (response) {
            $("#totaltickets").html(response.data[0].id);
            var number = response.data[0].id;
            var percentToGet = 100;
            var percent = (percentToGet / 100) * number;
            $("#totalpercent").html(percent);
        },
    });

    //Show To Do Task List In Table
    $.ajax({
        url: base_url + "userViewTask",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        var table = document.getElementById("taskdatatable");
        if (!$.trim(response.data[0])) {
            var html = '<div class="card"><div class="card-body text-center"><p style="font-size: 40px; color: #ff8800; margin: 0;"><i class="fa fa-frown"></i></p><p>Sorry! No data available</p></div></div>';
            $(".kt-widget__items").append(html);
        } 
        else {
            for (i in response.data) {
                var testcheck = "";
                if (response.data[i].status == 1) {
                    testcheck = "checked";
                }
                if (response.data[i].status == 0) {
                    testcheck = "";
                }
                var colour = "";
                if (response.data[i].priority == "High") {
                    colour = "#db2828";
                }
                if (response.data[i].priority == "Low") {
                    colour = "#fbbd08";
                }
                if (response.data[i].priority == "Medium") {
                    colour = "#5a5278";
                }
                var dueDate = new Date(response.data[i].due_date);
                var dd = String(dueDate.getDate()).padStart(2, "0");
                var mm = String(dueDate.getMonth() + 1).padStart(2, "0"); //January is 0!
                var yyyy = dueDate.getFullYear();
                dueDate = dd + "-" + mm + "-" + yyyy;
                var tr = document.createElement("tr");
                tr.innerHTML =
                    '<td><label class="custom-control custom-checkbox"><input  type="checkbox" ' + testcheck + ' class="custom-control-input" name="editstatus"  ><span class="custom-control-label">' + response.data[i].task_name + '</span><p style="font-weight: 100; white-space:break-spaces;">' + response.data[i].description + '</p></label></td>' +
                    
                    '<td ><div class="d-flex align-items-center"><div class="ml-0">  <a title="">' + response.data[i].first_name + '</a> <p class="mb-0">' +  response.data[i].emp_id + '</p> </div> </div></td>' +
                    
                    '<td class="text-center text-nowrap">' + dueDate + '</td>' +

                    '<td class="text-center"><span class="tag tag-warning ml-0 mr-0" style="background-color:' + colour + '">' + response.data[i].priority + '</span></td>' +

                    '<td class="text-center"><button type="button" data-toggle="modal" data-target="#delete_task" aria-expanded="false" id="' + response.data[i].id + '" class="btn btn-danger delete_data"><i class="fa fa-trash-alt"></i></button></td>';
                table.appendChild(tr);
            }

            var currentDate = new Date()
            var day = currentDate.getDate()
            var month = currentDate.getMonth() + 1
            var year = currentDate.getFullYear()
            var d = day + "-" + month + "-" + year;

            $('#headerfixed').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: d+ ' Task List Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1, 2, 4],
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: d+ ' Task List Details',
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [ 0, 1, 2, 4]
                        }
                    }
                ]
            } );
        }
    });

    //Delete To Do Task List In Table
    $(document).on("click", ".delete_data", function () {
        var task_id = $(this).attr("id");
        $(".delete_task_button").attr("id", task_id);
        $("#delete_task").modal("show");
    });
    $(document).on("click", ".delete_task_button", function () {
        var task_id = $(this).attr("id");
        $.ajax({
            url: base_url + "deleteTask",
            method: "POST",
            data: {
                task_id: task_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                console.log(response);
                var jsonDa = response;
                var jsonData = response["data"]["message"];
                var falsedata = response["data"];
                if (jsonDa["data"]["status"] == "1") {
                    toastr.success(jsonData);
                    setTimeout(function () {
                        window.location = "<?php echo base_url()?>Dashboard";
                    }, 1000);
                } else {
                    toastr.error(falsedata);
                    $("#delete_task_button [type=submit]").attr("disabled", false);
                }
            },
        });
    });

    // Fetch Branch by api*/
    $.ajax({
        url: base_url + "viewactiveBranch",
        data: {},
        type: "POST",
        dataType: "json", 
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        let dropdown = $("#addbranchid");
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Branch</option>');
        dropdown.prop("selectedIndex", 0);
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.id).text(entry.branch_name));
        });
    });

    // Fetch Department by api*/
    $("#addbranchid").change(function () {
        $.ajax({
            url: base_url + "viewDepartment",
            data: { branch_id: $("select[name=addbranchid]").val() },
            type: "POST",
            dataType: "json", 
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            let dropdown = $("#adddepartmentid");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Department</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.department_id).text(entry.department_name));
            });
        });
    });

    // Fetch Designation by api*/
    $("#adddepartmentid").change(function () {
        $.ajax({
            url: base_url + "viewDesignation",
            data: { department_id: $("select[name=adddepartmentid]").val() },
            type: "POST",
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            let dropdown = $("#designation_id");
            dropdown.empty();
            dropdown.append('<option selected="true" disabled>Choose Designation</option>');
            dropdown.prop("selectedIndex", 0);
            $.each(response.data, function (key, entry) {
                dropdown.append($("<option></option>").attr("value", entry.designation_id).text(entry.designation_name));
            });
        });
    });

    // Show Employee in table using Filter
    $(function () {
        filterEmployeeDash('all');
        $("#filter-form").submit();
    });
    function filterEmployeeDash() {
        $("#filter-form").off("submit");
        $("#filter-form").on("submit", function (e) {
            e.preventDefault();
            designation_id = $("#designation_id").val();
            if (designation_id == null) {
                designation_id = 'all';
            }
            req = {};
            req.designation_id = designation_id;
            //Show Employee In table
            $.ajax({
                url: base_url + "viewUserDashboard",
                data: req,
                type: "POST",
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            })
            .done(function (response) {
                $("#employeetablebody").DataTable().clear().destroy();
                $("#employeetablebody tbody").empty();
                var table = document.getElementById("employeedatatable");
                for (i in response.data) {
                    var UserImage = "";
                    if (!$.trim(response.data[i].user_image)) {
                        UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                    } else {
                        UserImage = response.data[i].user_image;
                    }

                    var tr = document.createElement("tr");
                    tr.innerHTML =
                        '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage + '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' + response.data[i].user_code + '" title="">' + response.data[i].emp_id + ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' +  response.data[i].email + '</p> </div> </div></td>' +
                        
                        '<td class="text-center">' + response.data[i].phone + '</td>' +

                        '<td ><div class="align-items-center"><div class="ml-0"> <p class="mb-0"><strong>Branch</strong> - ' +  response.data[i].branch_name + '</p> <p class="mb-0"><strong>Department</strong> - ' +  response.data[i].department_name +  '</p> <p class="mb-0"><strong>Designation</strong> - ' +  response.data[i].designation_name + '</p> </div> </div></td>' +

                        '<td class="text-center"><button type="button" data-toggle="modal" data-target="#add_task" aria-expanded="false" class="btn btn-info edit_data" title="Assign Task" id="' + response.data[i].id + '">Assign Task</button></td>';
                    table.appendChild(tr);
                }

                var currentDate = new Date()
		        var day = currentDate.getDate()
		        var month = currentDate.getMonth() + 1
		        var year = currentDate.getFullYear()
		        var d = day + "-" + month + "-" + year;

                $('#employeetablebody').DataTable( {
			        dom: 'Bfrtip',
			        buttons: [
			            {
			                extend: 'excelHtml5',
			                title: d+ ' Employee Details',
			                pageSize: 'LEGAL',
			                exportOptions: {
			                    columns: [ 0, 1, 2],
			                }
						},
			            {
			                extend: 'pdfHtml5',
			                title: d+ ' Employee Details',
			                pageSize: 'LEGAL',
			                exportOptions: {
			                    columns: [ 0, 1, 2]
			                }
			            }
			        ]
			    } );
            });
        });
    }

    // Add Task Using Employee Table
    $("#addtask").submit(function (e) {
        var formData = {
            task_name: $("input[name=addtaskname]").val(),
            priority: $("select[name=addpriority]").val(),
            due_date: $("input[name=addduedate]").val(),
            description: $("textarea[name=adddescription]").val(),
            user_id: $("input[name=adduserid]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "taskAssign",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        }).done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Dashboard";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#addtask [type=submit]").attr("disabled", false);
            }
        });
    });

    //Add Tickets Validation
    $(document).ready(function () {
        $("#addtasknamebutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#addtaskname").val().trim() == "") {
                $("#addtasknamevalidate").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#addtasknamevalidate").text("");
            }
            if ($("#addpriority").val() == null) {
                $("#addpriority").focus();
                $("#addpriorityvalidate").text("Please select a ticket priority.");
                errorCount++;
            } 
            else {
                $("#addpriorityvalidate").text("");
            }
            if ($("#addduedate").val().trim() == "") {
                $("#addduedatevalidate").text("This field can't be empty.");
                errorCount++;
            } 
            else {
                $("#addtiaddduedatevalidatetlevalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Show Leave list
    $.ajax({
        url: base_url + "viewAllLeave",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        $("#leavetable").DataTable().clear().destroy();
        var table = document.getElementById("tablebodyleave");
        for (i in response.data) {
            var startDate = new Date(response.data[i].start_date);
            var dd = String(startDate.getDate()).padStart(2, "0");
            var mm = String(startDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = startDate.getFullYear();

            startDate = dd + "-" + mm + "-" + yyyy;

            var endDate = new Date(response.data[i].end_date);
            var dd = String(endDate.getDate()).padStart(2, "0");
            var mm = String(endDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = endDate.getFullYear();

            endDate = dd + "-" + mm + "-" + yyyy;

            var UserImage = "";
            if (!$.trim(response.data[i].user_image)) {
                UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
            } else {
                UserImage = response.data[i].user_image;
            }

            var status = "";
            if (response.data[i].status == 0) {
                status =
                    '<div class="dropdown action-label"><a class="btn btn-primary btn-sm btn-rounded  dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-purple"></i> Pending</a><div class="dropdown-menu dropdown-menu-right" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-17px, 31px, 0px); z-index:9999;"> <a class="dropdown-item approved_data" href="#" data-toggle="modal" id="' +
                    response.data[i].leave_id +
                    '" data-target="#approved_leave"><i class="fa fa-dot-circle-o text-success"></i> Approved</a> <a class="dropdown-item declined_data" href="#" data-toggle="modal" id="' +
                    response.data[i].leave_id +
                    '" data-target="#denied_leave"><i class="fa fa-dot-circle-o text-danger"></i> Declined</a></div></div>';
            }

            if (response.data[i].status == 1) {
                status = '<div class="dropdown action-label"><a class="btn btn-success btn-sm btn-rounded" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-success"></i> Approved</a></div>';
            }
            if (response.data[i].status == 2) {
                status = '<div class="dropdown action-label"><a class="btn btn-danger btn-sm btn-rounded" href="#" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-dot-circle-o text-danger"></i> Declined</a></div>';
            }

            var totalPercent = "";

            if (response.data[i].allow_short == 1) {
                totalPercent = response.data[i].days;
            } else if (response.data[i].allow_half == 1) {
                totalPercent = response.data[i].days;
            } else {
                var number = response.data[i].days;
                var percentToGet = 1;
                var percent = number + percentToGet;
                totalPercent = percent;
            }

            var tr = document.createElement("tr");
            tr.innerHTML = //'<td class="text-center">' + j++ + '</td>' +
                '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' + UserImage + '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' + response.data[i].user_code + '" title="">' + response.data[i].emp_id + ' - ' + response.data[i].first_name + '</a> <p class="mb-0">' + response.data[i].designation_name + '</p> </div> </div></td>' +

                '<td ><div class="d-flex align-items-center"><div class="ml-0"> <p class="mb-0"><strong>Branch</strong> - ' +
                        response.data[i].branch_name +
                        '</p> <p class="mb-0"><strong>Department</strong> - ' +
                        response.data[i].department_name +
                        "</p>  </div> </div></td>" +

                '<td class="text-center">' + response.data[i].policy_name + '</td>' +

                '<td class="text-center">' + startDate + '</td>' +
                
                '<td class="text-center text-danger">' + endDate + '</td>' +
                
                '<td class="text-center">' + totalPercent + '</td>' +

                '<td class="text-center"  style=" white-space: break-spaces;">' + response.data[i].reason + '</td>' +

                '<td class="text-center">' + status + '</td>';
            table.appendChild(tr);
        }

        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#leavetable").DataTable({
            dom: 'Bfrtip',
	        buttons: [
	            {
	                extend: 'excelHtml5',
	                title: d+ ' Employee Leave Details',
	                pageSize: 'LEGAL',
	                exportOptions: {
	                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
	                }
	            },
	            {
	                extend: 'pdfHtml5',
	                title: d+ ' Employee Leave Details',
	                pageSize: 'LEGAL',
	                exportOptions: {
	                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
	                }
	            }
	        ]
        });
    });

    // Denied Leave Form File
    $(document).on("click", ".declined_data", function () {
        var leave_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewAllLeave_Where",
            method: "POST",
            data: {
                leave_id: leave_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#Leavedelclinedleavetype").val(response["data"][0]["leave_type"]);
                $("#editLeavedelclinedid").val(response["data"][0]["leave_id"]);
                $("#editLeavedelclinedemail").val(response["data"][0]["email"]);
                $("#Leavedelclinedapply").val(response["data"][0]["start_date"]);
                $("#Leavedelclinedfrom").val(response["data"][0]["end_date"]);
                $("#denied_leave").modal("show");
            },
        });
    });

    //Declined Leave
    $("#declinedleave").submit(function (e) {
        var formData = {
            leave_type: $("input[name=Leavedelclinedleavetype]").val(),
            start_date: $("input[name=Leavedelclinedapply]").val(),
            end_date: $("input[name=Leavedelclinedfrom]").val(),
            email: $("input[name=editLeavedelclinedemail]").val(),
            reason: $("textarea[name=declineleave]").val(),
            leave_id: $("input[name=editLeavedelclinedid]").val(),
            action: $("input[name=editLeavedeclined]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "leaveAction",
            data: formData,
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);

            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Leave";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#declinedleave [type=submit]").attr("disabled", false);
            }
        });
    });

    //Approved Leave Form Fill
    $(document).on("click", ".approved_data", function () {
        var leave_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewAllLeave_Where",
            method: "POST",
            data: {
                leave_id: leave_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#editleavetypeid").val(response["data"][0]["leave_id"]);
                $("#editUserId").val(response["data"][0]["user_id"]);
                $("#Leavedays").val(response["data"][0]["days"]);
                $("#Leaveapprovedapply").val(response["data"][0]["start_date"]);
                $("#Leaveapprovedfrom").val(response["data"][0]["end_date"]);
                $("#Leaveapprovedtype").val(response["data"][0]["leave_type"]);
                $("#approved_leave").modal("show");
            },
        });
    });

    //Approved Leave form
    $("#editleave").submit(function (e) {
        var formData = {
            leave_type: $("input[name=Leaveapprovedtype]").val(),
            days: $("input[name=Leavedays]").val(),
            start_date: $("input[name=Leaveapprovedapply]").val(),
            end_date: $("input[name=Leaveapprovedfrom]").val(),
            leave_id: $("input[name=editleavetypeid]").val(),
            user_id: $("input[name=editUserId]").val(),
            action: $("input[name=editleaveaction]").val(),
        };
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: base_url + "leaveAction",
            data: formData, // our data object
            dataType: "json",
            encode: true,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
        })
        .done(function (response) {
            console.log(response);
            var jsonDa = response;
            var jsonData = response["data"]["message"];
            var falsedata = response["data"];
            if (jsonDa["data"]["status"] == "1") {
                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Leave";
                }, 1000);
            } else {
                toastr.error(falsedata);
                $("#editleave [type=submit]").attr("disabled", false);
            }
        });
    });

    // Leave Denied  Validation
    $(document).ready(function () {
        $("#leavedeniedbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;
            if ($("#declineleave").val().trim() == "") {
                $("#declineleavevalid").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#declineleavevalid").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    //Show  Tikets
    $.ajax({
        url: base_url + "viewTickets",
        data: {},
        type: "POST",
        dataType: "json",
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    })
    .done(function (response) {
        var table = document.getElementById("tablebodymergeleave");
        for (i in response.data) {
            var priority_color = "";
            if (response.data[i].priority == 0) {
                priority_color = "blue";
            }
            if (response.data[i].priority == 1) {
                priority_color = "green";
            }
            if (response.data[i].priority == 2) {
                priority_color = "red";
            }
            var priority = "";
            if (response.data[i].priority == 0) {
                priority = "Low";
            }
            if (response.data[i].priority == 1) {
                priority = "Medium";
            }
            if (response.data[i].priority == 2) {
                priority = "High";
            }

            var testcheck = "";
            if (response.data[i].status == 0) {
                testcheck = "checked";
            }
            if (response.data[i].status == 1) {
                testcheck = "";
            }
            var UserImage = "";
                if (!$.trim(response.data[i].user_image)) {
                    UserImage = "<?php echo base_url();?>assets/images/dummy/person-dummy.jpg";
                } else {
                    UserImage = response.data[i].user_image;
                }
            var sDate = new Date(response.data[i].date);
            var dd = String(sDate.getDate()).padStart(2, "0");
            var mm = String(sDate.getMonth() + 1).padStart(2, "0"); //January is 0!
            var yyyy = sDate.getFullYear();
            sDate = dd + "-" + mm + "-" + yyyy;

            var tr = document.createElement("tr");
            tr.innerHTML =
                '<td class="text-center text-uppercase"><a href="TicketsCommnets/' +
                response.data[i].id +
                '" class="viewbutton">' +
                response.data[i].ticket_no +
                " </a></td>" +
                '<td ><div class="d-flex align-items-center"><span class="avatar avatar-pink"><img class="avatar w100 h100 mb-1" src="' +
                UserImage +
                '" alt=""></span><div class="ml-3">  <a href="EmployeeView/' +
                response.data[i].user_code +
                '" title="">' +
                response.data[i].emp_id +
                ' - ' +
                response.data[i].first_name +
                '</a> <p class="mb-0">' +
                response.data[i].branch_name +
                "</p> </div> </div></td>" +

                '<td class="text-center">' +
                sDate +
                "</td>" +
                '<td class="text-center">' +
                response.data[i].ticket_type +
                "</td>" +
                '<td class="text-center"style="color:' +
                priority_color +
                '">' +
                priority +
                "</td>" +
                '<td class="text-center"><div class="custom-controls-stacked"><label class="custom-control custom-radio custom-control-inline"><input type="checkbox" '+testcheck+' id="'+response.data[i].id+'" value="'+ response.data[i].status +'" onclick="getstatus('+response.data[i].id+')" class="custom-switch-input"> <span class="custom-switch-indicator"></span></label></div></td>';

            table.appendChild(tr);
        }
        var currentDate = new Date()
        var day = currentDate.getDate()
        var month = currentDate.getMonth() + 1
        var year = currentDate.getFullYear()
        var d = day + "-" + month + "-" + year;
        $("#mergeleaveticket").DataTable({
            dom: 'Bfrtip',
	        buttons: [
	            {
	                extend: 'excelHtml5',
	                title: d+ ' Employee Ticket Details',
	                pageSize: 'LEGAL',
	                exportOptions: {
	                    columns: [ 0, 1, 2, 3, 4, 5]
	                }
	            },
	            {
	                extend: 'pdfHtml5',
	                title: d+ ' Employee Ticket Details',
	                pageSize: 'LEGAL',
	                exportOptions: {
	                    columns: [ 0, 1, 2, 3, 4, 5]
	                }
	            }
	        ]
        });
    });
    
    function getstatus(id) {
        var ticket_id = id;
        var status = $("#" + id).val();
        console.log(status);
        //alert(status);
        if (status == 0) {
            var status = 1;
            $.ajax({
                type: "POST",
                url: base_url + "editTicketsActive",
                data: "ticket_id=" + ticket_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Dashboard";
                }, 1000);
            });
        } else {
            var status = 0;
            $.ajax({
                type: "POST",
                url: base_url + "editTicketsActive",
                data: "ticket_id=" + ticket_id + "&status=" + status,
                dataType: "json",
                encode: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Token", localStorage.token);
                },
            }).done(function (response) {
                var jsonDa = response;
                var jsonData = response["data"]["message"];

                toastr.success(jsonData);
                setTimeout(function () {
                    window.location = "<?php echo base_url()?>Dashboard";
                }, 1000);
            });
        }
    }
   

    //Select Priority
    $.ajax({
        url: base_url + "viewTicketsPriority",
        data: {},
        type: "POST",
        dataType: "json", // what type of data do we expect back from the server
        encode: true,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Token", localStorage.token);
        },
    }).done(function (response) {
        let dropdown = $("#addpriority");

        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Choose Priority</option>');
        dropdown.prop("selectedIndex", 0);

        // Populate dropdown with list of provinces
        $.each(response.data, function (key, entry) {
            dropdown.append($("<option></option>").attr("value", entry.priority).text(entry.priority));
        });
    });
    //Edit  Department Form Fill
    $(document).on("click", ".edit_data", function () {
        var user_id = $(this).attr("id");
        $.ajax({
            url: base_url + "viewUserId",
            method: "POST",
            data: {
                user_id: user_id,
            },
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Token", localStorage.token);
            },
            success: function (response) {
                $("#adduserid").val(response["data"][0]["id"]);
                $("#add_task").modal("show");
            },
        });
    });

    //Add Tickets Validation
    $(document).ready(function () {
        $("#editTicketbutton").click(function (e) {
            e.stopPropagation();
            var errorCount = 0;

            if ($("#addcomments").val().trim() == "") {
                $("#editsubjectvalidate").text("This field can't be empty.");
                errorCount++;
            } else {
                $("#editsubjectvalidate").text("");
            }
            if (errorCount > 0) {
                return false;
            }
        });
    });

    
</script>