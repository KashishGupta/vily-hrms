
<!doctype html>
<html lang="en" dir="ltr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" href="<?php echo base_url(); ?>assets/images/Favicon.ico" type="image/x-icon"/>

<title>Ezdat : HRMS</title>

<!-- Bootstrap Core and vandor -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />

<!-- Core css -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme2.css"/>

</head>
<body class="font-montserrat">

<div class="auth">
    <div class="auth_left">
        <div class="card">
            <div class="text-center mb-5">
                <a class="header-brand" href="#"><img src="<?php echo base_url(); ?>assets/images/ezdat-logo.png" alt="Ezdat Logo"></a>
            </div>
            <form action="#" id="forgetpassword" method="POST">
                <div class="card-body">
                    <div class="card-title">Forgot password</div>
                    <p class="text-muted">Enter your email address and your password will be reset and emailed to you.</p>
                    <span id="message"></span>
                     <span id="thnakyou"></span>
                    <div class="form-group">
                        <label class="form-label" for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="forgetemail" name="forgetemail" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary btn-block">Send me new password</button>
                    </div>
                </div>
                <div class="text-center text-muted">
                    Forget it, <a href="<?php echo base_url(); ?>login">Send me Back</a> to the Sign in screen.
                </div>
            </form>
        </div>      
    </div>
    <div class="auth_right">
        <div class="carousel slide" data-ride="carousel" data-interval="3000">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/login/slider1.svg" class="img-fluid" alt="login page" />
                    <div class="px-4 mt-4">
                        <h4>Fully Responsive</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/login/slider2.svg" class="img-fluid" alt="login page" />
                    <div class="px-4 mt-4">
                        <h4>Quality Code and Easy Customizability</h4>
                        <p>There are many variations of passages of Lorem Ipsum available.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/login/slider3.svg" class="img-fluid" alt="login page" />
                    <div class="px-4 mt-4">
                        <h4>Cross Browser Compatibility</h4>
                        <p>Overview We're a group of women who want to learn JavaScript.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
// Add' Holiday 
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>assets/js/lib.vendor.bundle.js"></script>
<script src="<?php echo base_url(); ?>assets/js/core.js"></script>
 <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
<script>
    
                $("#forgetpassword").submit(function(e) {
                    var formData = {
                        'email'         :  $('input[name=forgetemail]').val()
                       
                    };
                     $('#message').css('color', 'rgba(255, 165, 0, 0.98)').text('Please Wait...').show("slow");
                     e.preventDefault();
                    $.ajax({
                        type        : 'POST', 
                        url         : '<?php echo base_url(); ?>api/callApi/forgetPassword',
                        data        : formData, 
                   
                        success: function(response)
            {
             var falsedata = response["data"];
                var jsonData = response;
 
                // user is logged in successfully in the back-end
                // let's redirect
            
                 if (jsonData["data"]["id"])
                {
                localStorage.emailid = jsonData["data"]["email"];
                localStorage.userid = jsonData["data"]["id"];
                var thankyou = "Your Reset password Link Has Been Send your Email Id...";
                 $('#message').css('color', 'rgb(0,128,0').text(thankyou).show("slow");
             
                // window.location.replace("http://neeraj.ezdatechnology.com/index.php/forgotPassword");   
                }
               
                else{
                     $('#message').css('color', 'rgba(255, 0, 0, 0.87)').text(falsedata).show("slow");
                }
          }
                });
                });
</script>
</body>
</html>
 