<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <?php 
			$query1 = $this->db->query("SELECT company_logo FROM `company`");
			$arrPolicy = $query1->row();
			$arrTest1 = $arrPolicy->company_logo;
			$arrTest = $arrPolicy->company_logo;
		?>
        <link rel="icon" href="<?php echo $arrTest1; ?>" type="image/x-icon" />

        <title>Ezdat : HRMS</title>
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
        <!-- Bootstrap Core and vandor -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />

        <!-- Core css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css" />

        <style type="text/css">
        	span#emailvalid {
			    position: absolute;
			    top: 38%;
			}

			span#passwordvalid {
			    position: absolute;
			    top: 65%;
			}
			span#message {
			    position: absolute;
			    top: 34%;
			    width: 89%;
			}
        </style>
    </head>

    <body class="font-montserrat">
        <div id="page_top" class="section-body top_dark sticky-top">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="left">
                        <h1 class="page-title text-white">HRMS Dashboard</h1>
                    </div>
                    <div class="right text-white">
                        <div class="notification d-flex">
                            <div class="dropdown d-flex">
                                <a href="#" class="btn btn-primary">
                                    Jobs
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="auth">
            <div class="auth_left">
                <div class="card">
                    <div class="text-center mb-2">
                        <a class="header-brand" href="<?php echo base_url(); ?>">
                        	<img src="<?php echo $arrTest; ?>" alt="Ezdat Logo" style="max-width: 160px;" />
                        </a>
                    </div>
                    <span id="message"></span>
                    <form action="#" id="login" method="POST">
                        <!-- <div class="card-body"> -->
                            <p class="mt-2">Welcome back! Please Login to continue.</p>
                            
                            <div class="input-icon mb-3">
                                <span class="input-icon-addon"><i class="fe fe-user"></i></span>
                                <input type="email" class="form-control text-lowercase" id="email" name="email" aria-describedby="emailHelp" placeholder="yourname@yourmail.com" />
                            </div>
                            <div class="input-icon mb-3">
                                <span class="input-icon-addon"><i class="fe fe-lock"></i></span>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password" />
                                
                            </div><p class="icon-show-hide" style="position: absolute; right: 33px; top: 58%;"><i class="far fa-eye" id="togglePassword"></i></p>
                            <br>
                            <span id="emailvalid" class="text-danger"></span>
                            <span id="passwordvalid" class="text-danger"></span>
                            <div class="form-group">
			                    <label class="custom-control custom-checkbox">
			                    	<input type="checkbox" class="custom-control-input">
			                    	<span class="custom-control-label">Remember me</span>
			                    	
			                    </label>
			                    <a href="<?php echo base_url(); ?>login/forgotPassword" class="float-right">Forgot password?</a>
			                </div>
                            <div class="form-footer">
                                <button class="btn btn-primary btn-block" type="submit" id="loginuser">Login</button>
                            </div>
                        <!-- </div> -->
                    </form>
                </div>
            </div>
            <div class="auth_right">
                <div class="carousel slide" data-ride="carousel" data-interval="3000">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="<?php echo base_url(); ?>assets/images/login/slider1.svg" class="img-fluid" alt="login page" />
                            <div class="px-4 mt-4">
                                <h4>Fully Responsive</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="<?php echo base_url(); ?>assets/images/login/slider2.svg" class="img-fluid" alt="login page" />
                            <div class="px-4 mt-4">
                                <h4>Quality Code and Easy Customizability</h4>
                                <p>There are many variations of passages of Lorem Ipsum available.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="<?php echo base_url(); ?>assets/images/login/slider3.svg" class="img-fluid" alt="login page" />
                            <div class="px-4 mt-4">
                                <h4>Cross Browser Compatibility</h4>
                                <p>Overview We're a group of women who want to learn JavaScript.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/lib.vendor.bundle.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/core.js"></script>
        <script type="text/javascript">
        	const togglePassword = document.querySelector('#togglePassword');
			const password = document.querySelector('#password');

			togglePassword.addEventListener('click', function (e) {

			    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
			    password.setAttribute('type', type);

			    this.classList.toggle('fa-eye-slash');
			});



            $(document).ready(function () {
                $("#login").submit(function (e) {
                    var formData = {
                        email: $("input[name=email]").val(),
                        password: $("input[name=password]").val(),
                    };
                    $("#message").css({"color":"#fff", "background-color":"#6f6fd2", "border-radius":"0.25rem", "padding":"5px", "text-align":"center"}).text("Please Wait...").show("slow");
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>api/callApi/userLogin",
                        data: formData,
                        success: function (response) {
                            var jsonData = response;
                            var falsedata = response["data"]["message"];
                            //alert(test);
                            if (jsonData["data"]["role_id"] == "1") {
                                localStorage.token = jsonData["data"]["Token"];
                                localStorage.userid = jsonData["data"]["id"];
                                localStorage.roleid = jsonData["data"]["role_id"];
                                localStorage.emailid = jsonData["data"]["email"];
                                localStorage.designationid = jsonData["data"]["designation_id"];
                                $("#message").css({"color":"#fff", "background-color":"green", "border-radius":"0.25rem", "padding":"5px", "text-align":"center"}).text("LogIn Successfully...").show("slow");
                                setTimeout(function () {
                                    window.location = "<?php echo base_url()?>Dashboard";
                                }, 700);
                            } 
                            else if (jsonData["data"]["role_id"] == "0") {
                                localStorage.token = jsonData["data"]["Token"];
                                localStorage.userid = jsonData["data"]["id"];
                                localStorage.roleid = jsonData["data"]["role_id"];
                                localStorage.emailid = jsonData["data"]["email"];
                                localStorage.designationid = jsonData["data"]["designation_id"];
                                $("#message").css({"color":"#fff", "background-color":"green", "border-radius":"0.25rem", "padding":"5px", "text-align":"center"}).text("LogIn Successfully...").show("slow");
                                setTimeout(function () {
                                    window.location = "<?php echo base_url()?>Home";
                                }, 700);
                            }
                               
                            else {
                                $("#message").css({"color":"#fff", "background-color":"rgba(255, 0, 0, 0.87)", "border-radius":"0.25rem", "padding":"5px", "text-align":"center"}).text(falsedata).show("slow");
                            }
                        },
                    });
                });
            });

            // Validation
            $(document).ready(function () {
                $("#loginuser").click(function (e) {
                    e.stopPropagation();
                    var errorCount = 0;

                    if ($("#email").val().trim() == "") {
                        $("#email").focus();
                        $("#emailvalid").text("Required Email  *");
                        errorCount++;
                    } else {
                        $("#emailvalid").text("");
                    }

                    if ($("#password").val().trim() == "") {
                        $("#password").focus();
                        $("#passwordvalid").text("Required Password *");
                        errorCount++;
                    } else {
                        $("#passwordvalid").text("");
                    }

                    if (errorCount > 0) {
                        return false;
                    }
                });
            });
        </script>
    </body>
</html>
