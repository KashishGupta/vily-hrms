<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
  Project: Willy HRMS
  Author: Pratap Singh
  File Name: OnboardingModel.php
  Date: March 12, 2020
  Info: This is the main class which is hold all the Models of the Onboarding.
*/

class OnboardingModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function checkOfficeDetails($credential) {
        // Check The Credentials
        $query = $this->db->get_where('users' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
             
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('users.id', $row->id);
            $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
      // check Bank details If Allready Exist
 function checkBankDetailsAvailability($where_basic)
 {
     $query = $this->db->get_where('users_bank_details',$where_basic);
     if ($query->num_rows() > 0)
         return TRUE;
     else
         return FALSE;
 }
     // check Basic details If Allready Exist
 function checBasicDetailsAvailability($where_basic)
 {
     $query = $this->db->get_where('user_personal',$where_basic);
     if ($query->num_rows() > 0)
         return TRUE;
     else
         return FALSE;
 }
     // check Experience details If Allready Exist
 function checExperienceDetailsAvailability($where_education)
 {
     $query = $this->db->get_where('user_previouscompany',$where_education);
     if ($query->num_rows() > 0)
         return TRUE;
     else
         return FALSE;
 }
    // check Education details If Allready Exist
 function checEducationDetailsAvailability($where_education)
 {
     $query = $this->db->get_where('user_education',$where_education);
     if ($query->num_rows() > 0)
         return TRUE;
     else
         return FALSE;
 }
    //Add  Experience  Document
    public function addOnexperience($data)
    {
        $this->db->insert('user_experience',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     // add Experience Details
     public function addExperience($data)
     {
       
      $this->db->insert('user_previouscompany',$data);
         $insert_id = $this->db->insert_id();
         if($insert_id != 0)
         {
             return TRUE;
         }
         else    
         {
             return FALSE;
         }
     }
    // add Education Dertails
    public function addEducation($data)
    {
    $this->db->insert('user_education',$data);
    $inser_id = $this->db->insert_id();
    if($inser_id != 0)
    {
      return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     //Add  Personal  Document
    public function addPersonal($data)
    {
        $this->db->insert('user_personaldocument',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    //Add  Other Document
    public function addOther($data)
    {
        $this->db->insert('user_other',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    // Add Api Query
 public function addOnboarding($data)
    {
        $this->db->insert('user_onboarding',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    
    public function viewOnUsercode($where2)
    {
    
    $this->db->select('users.user_code');
        $this->db->from('users');
          $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
    
    // view Users
public function viewOnboardingBankWhere($where)
{
    $this->db->select('*');
    $this->db->from('users_bank_details');
     $this->db->where($where);
    $query = $this->db->get();
    return $query->result();
}
public function viewOnboardingBank()
{
$this->db->select('*');
    $this->db->from('users_bank_details');
    $query = $this->db->get();
    return $query->result();
}
// view Users
public function viewOnboardingOfficeWhere($where)
{
    $this->db->select('*');
    $this->db->from('users');
     $this->db->where($where);
    $query = $this->db->get();
    return $query->result();
}
public function viewOnboardingOffice()
{
$this->db->select('*');
    $this->db->from('users');
    $query = $this->db->get();
    return $query->result();
}




     public function viewUserOnboarding($where2)
    {
    
    $this->db->select('users.id,users.first_name,user_details.user_image,users.emp_id,users.email,users.phone,user_details.gender');
        $this->db->from('onboarding_status');
        $this->db->join('users','users.id = onboarding_status.user_id');
        $this->db->join('user_details','user_details.user_id = users.id');
          $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
     public function viewUserOnboardingwhere($where,$where2)
    {
        $this->db->select('users.id ,users.first_name,user_details.user_image,users.emp_id,users.email,users.phone,user_details.gender');
     $this->db->from('onboarding_status');
     $this->db->join('users','users.id = onboarding_status.user_id');
     $this->db->join('user_details','user_details.user_id = users.id');
     $this->db->where($where);
     $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
    
    // Delete Other Document
    public function deleteOther($where_onboarding)
    {
        $this->db->where($where_onboarding);
        $this->db->delete('user_other');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // DeleteExperience Id
   public function deleteExperience($where_onboarding)
    {
        $this->db->where($where_onboarding);
        $this->db->delete('user_experience');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Delete Onboarding Id
   public function deleteOnboarding($where_onboarding)
    {
        $this->db->where($where_onboarding);
        $this->db->delete('user_onboarding');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Delete personal Id
   public function deletePersoanl($where_persoanl)
    {
        $this->db->where($where_persoanl);
        $this->db->delete('user_personaldocument');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
     // Edit change Status
     public function addParent($data,$where)
     {
         $this->db->where($where);
         $this->db->update('users',$data);
         
         if($this->db->affected_rows() != 1) 
         {
            return FALSE;
         }
         else 
         {
             return TRUE;
         }
     }
    // Edit change Status
    public function editchangeStatus($data,$where)
    {
        $this->db->where($where);
        $this->db->update('onboarding_status',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Edit onboarding
     public function editOnboarding($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_onboarding',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Edit Experience
    public function editExperience($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_experience',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Edit Experience
    public function editPersonal($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_personaldocument',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Edit editOther
    public function editOther($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_other',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
     // View Persoanl Document
    public function viewOnboardingLoggedWhere($onboarding_where)
    {
        $this->db->select('users.id as user_id,user_onboarding.offer_letter,user_onboarding.id,user_onboarding.document_name');
        $this->db->from('user_onboarding');
        $this->db->join('users','user_onboarding.user_id = users.id');
     
          $this->db->where($onboarding_where);
        $query = $this->db->get();
        return $query->result();
    }
    
    // View Persoanl Document
    public function viewPesonalWhere($personal_where)
    {
         $this->db->select('users.id as user_id,user_personaldocument.id,user_personaldocument.personaldocument,user_personaldocument.document_name,user_personaldocument.status');
        $this->db->from('user_personaldocument');
        $this->db->join('users','user_personaldocument.user_id = users.id');
          $this->db->where($personal_where);
        $query = $this->db->get();
        return $query->result();
    }
     // View Persoanl Document
    public function viewPesonal()
    {
         $this->db->select('users.id as user_id,user_personaldocument.id,user_personaldocument.personaldocument,user_personaldocument.document_name');
        $this->db->from('user_personaldocument');
        $this->db->join('users','user_personaldocument.user_id = users.id');
        $query = $this->db->get();
        return $query->result();
    }
    //View Other 
     public function viewOtherWhere($where_other)
    {
        $this->db->select('users.id as user_id,user_other.other,user_other.id,user_other.document_name,user_other.instruction_document,user_other.status');
        $this->db->from('user_other');
        $this->db->join('users','user_other.user_id = users.id');
     
          $this->db->where($where_other);
        $query = $this->db->get();
        return $query->result();
    }
      public function viewOther()
    {
        $this->db->select('users.id as user_id,user_other.other,user_other.id,user_other.document_name,user_other.instruction_document');
        $this->db->from('user_other');
        $this->db->join('users','user_other.user_id = users.id');
      
        $query = $this->db->get();
        return $query->result();
    }
    //View Experience 
     public function viewExperienceWhere($where_experience)
    {
        $this->db->select('users.id as user_id,user_experience.document_name,user_experience.experience_letter,user_experience.id,user_experience.status');
        $this->db->from('user_experience');
        $this->db->join('users','user_experience.user_id = users.id');
     
          $this->db->where($where_experience);
        $query = $this->db->get();
        return $query->result();
    }
      public function viewExperience()
    {
        $this->db->select('users.id as user_id,user_experience.document_name,user_experience.experience_letter,user_experience.id');
        $this->db->from('user_experience');
        $this->db->join('users','user_experience.user_id = users.id');
      
        $query = $this->db->get();
        return $query->result();
    }
    // View Onboarding Id And All Ids
    
    public function viewOnboardingWhere($where_onboarding)
    {
        $this->db->select('users.id as user_id,user_onboarding.offer_letter,user_onboarding.status,user_onboarding.id,user_onboarding.document_name');
        $this->db->from('user_onboarding');
        $this->db->join('users','user_onboarding.user_id = users.id');
     
          $this->db->where($where_onboarding);
        $query = $this->db->get();
        return $query->result();
    }
      public function viewOnboarding()
    {
        $this->db->select('users.id as user_id,user_onboarding.offer_letter,user_onboarding.id,user_onboarding.document_name');
        $this->db->from('user_onboarding');
        $this->db->join('users','user_onboarding.user_id = users.id');
      
        $query = $this->db->get();
        return $query->result();
    }
// check All Documents Uploade Or Not
    public function checkOnboardingDoc($credential)
    {
       // Check The Credentials
       $query = $this->db->get_where('user_onboarding' , $credential);
       // If User Exists
       if ($query->num_rows() > 0) {
           $row = $query->row();
           $this->db->select('*');
           $this->db->from('user_onboarding');

           $this->db->where('user_onboarding.id', $row->id);
       
          $user = $this->db->get()->row();

           return $user;
       }
       else{
           return FALSE;
       }
    }

     // check Personal Documents Uploade Or Not
     public function checkOnboardingPersonal($credential)
     {
        // Check The Credentials
        $query = $this->db->get_where('user_personaldocument' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->db->select('*');
            $this->db->from('user_personaldocument');
 
            $this->db->where('user_personaldocument.id', $row->id);
        
           $user = $this->db->get()->row();
 
            return $user;
        }
        else{
            return FALSE;
        }
     }
     
      // check Other Letter Documents Uploade Or Not
      public function checkOnboardingOther($credential)
      {
         // Check The Credentials
         $query = $this->db->get_where('user_other' , $credential);
         // If User Exists
         if ($query->num_rows() > 0) {
             $row = $query->row();
             $this->db->select('*');
             $this->db->from('user_other');
  
             $this->db->where('user_other.id', $row->id);
         
            $user = $this->db->get()->row();
  
             return $user;
         }
         else{
             return FALSE;
         }
      }
      // check Experience Letter Documents Uploade Or Not
      public function checkOnboardingExperience($credential)
      {
         // Check The Credentials
         $query = $this->db->get_where('user_experience' , $credential);
         // If User Exists
         if ($query->num_rows() > 0) {
             $row = $query->row();
             $this->db->select('*');
             $this->db->from('user_experience');
  
             $this->db->where('user_experience.id', $row->id);
         
            $user = $this->db->get()->row();
  
             return $user;
         }
         else{
             return FALSE;
         }
      }
}