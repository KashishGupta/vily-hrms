<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Onboarding.php
	Date: March 12, 2020
	Info: This is the main class which is hold all the Functions of the users.
*/

class Onboarding extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('onboardingModel');
	}

    public function _viewOnboardingUser($request)
	{		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}	
		if($permission->can_view == 'Y')
		{			
			/* Here we Add the userss.. If User has the permission for that. */
      if(TRUE)
			{
				
					if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->users->_getUserId();
       
					if(!empty($user_ids))
					$where = 'onboarding_status.user_id IN ('.implode(',',$user_ids).') AND onboarding_status.status = 0 ';
				else
					$where = 'onboarding_status.user_id = 0 AND onboarding_status.status = 0';

				if(!empty($users_where))
				{
					$result = $this->onboardingModel->viewUserOnboardingwhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewUserOnboarding($where);
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}

	public function _viewOnboardingUserFilter($request)
	{		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}	
		if($permission->can_view == 'Y')
		{			
			/* Here we Add the userss.. If User has the permission for that. */
      if(TRUE)
			{
				if(!empty($request->user_id == 4))
				{
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
					$where = 'onboarding_status.user_id IN ('.implode(',',$user_ids).') AND onboarding_status.status = 0 ';
				else
					$where = 'onboarding_status.user_id = 0 AND onboarding_status.status = 0';
						$result = $this->onboardingModel->viewUserOnboarding($where);
					return $result;
				}else{
				
					if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->users->_getUserId();
       
					if(!empty($user_ids))
					$where = 'onboarding_status.user_id IN ('.implode(',',$user_ids).') AND onboarding_status.status = 0 ';
				else
					$where = 'onboarding_status.user_id = 0 AND onboarding_status.status = 0';

				if(!empty($users_where))
				{
					$result = $this->onboardingModel->viewUserOnboardingwhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewUserOnboarding($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}

	// add Experience details usercode and id
	public function _viewOnUsercode($request)
	{		
				
			/* Here we Add the userss.. If User has the permission for that. */
      if(TRUE)
			{
				
					if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;
					$result = $this->onboardingModel->viewOnUsercode($users_where);
					return $result;
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}
	// Add Experience Details Onboarding  
	public function _addExperienceOnboarding($request)
	{
		
	$module_name = 'onboarding';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_add == 'Y')
	{
		if(isset($request->user_id) AND isset($request->user_code))
	 
	 {
		$experienceData = array(
			'user_id' => $request->user_id,
			'user_code' => $request->user_code
		);
		if(isset($request->company_name))
		$experienceData['company_name'] = $request->company_name;
		if(isset($request->location))
		$experienceData['location'] = $request->location;
		if(isset($request->job_position))
		$experienceData['job_position'] = $request->job_position;
		if(isset($request->period_from))
		$experienceData['period_from'] = $request->period_from;
		if(isset($request->period_to))
		$experienceData['period_to'] = $request->period_to;
		if(isset($request->intern))
		$experienceData['intern'] = $request->intern;
		/* Date Conversion and Other Things */
	$leaveStartDate=strtotime($request->period_to);
	$dayOfStartLeave = date("l", $leaveStartDate);

	$leaveEndDate=strtotime($request->period_from);
	$dayOfEndLeave = date("l", $leaveEndDate);

	$currentDate = date('d-m-Y');
	$currentDate=strtotime($currentDate);
/* End Date Conversion */

if($leaveStartDate > $currentDate){
	$this->api->_sendError('Future Date is Not Allowed.');
}
	 /* Weekend and Date Ahead Check Start */
		 if($leaveStartDate < $leaveEndDate){
			 $this->api->_sendError('End Date Ahead From Start Date.');
		 }
	
				  
				  
				   $result = $this->onboardingModel->addExperience($experienceData);
			  if($result)
				   {
					   return array(
						   'status' => '1',
						   'message' => 'Experience Submitted Successfully.'
					   );
				   }
				   else
				   {
					   $this->api->_sendError("Sorry, Experience Not Submitted. Please Try Again!!");
				   }
	 }
	 else{
		 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	 }
	}
	else
	{
	$this->api->_sendError("Sorry, You Have No Permission To Add Experience.");
	}
	}
	// Onboarding Add Emolyee Education Details
	public function _addEducationonboarding($request)
	{
	$module_name = 'onboarding';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_add == 'Y')
	{
		if(isset($request->institution) AND isset($request->subject) AND isset($request->end_date) AND isset($request->grade) AND isset($request->education_degree) AND isset($request->user_code) AND isset($request->user_id))
	 
	 {
	 $educationData = array(
					   'institution' => $request->institution,
					   'subject' => $request->subject,
					   'end_date' => $request->end_date,
					   'grade' => $request->grade,
					   'user_code' => $request->user_code,
					   'education_degree' => $request->education_degree,
					   'user_id' => $request->user_id
				   );
			
				   $result = $this->onboardingModel->addEducation($educationData);
			  if($result)
				   {
					   return array(
						   'status' => '1',
						   'message' => 'Education Details submitted Successfully.'
					   );
				   }
				   else
				   {
					   $this->api->_sendError("Sorry, Education Details Not submitted. Please Try Again!!");
				   }
	 }
	 else{
		 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	 }
	}
	else
	{
	$this->api->_sendError("Sorry, You Have No Permission To submitted Education Details.");
	}
	}
	// View Onboarding Complete
	public function _viewOnboardingCompleteFilter($request)
	{		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}	
		if($permission->can_view == 'Y')
		{			
			/* Here we Add the userss.. If User has the permission for that. */
      if(TRUE)
			{
				if(!empty($request->user_id == 4))
				{
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
					$where = 'onboarding_status.user_id IN ('.implode(',',$user_ids).') AND onboarding_status.status = 1';
				else
					$where = 'onboarding_status.user_id = 0 AND onboarding_status.status = 1';
						$result = $this->onboardingModel->viewUserOnboarding($where);
					return $result;
				}else{
				
					if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->users->_getUserId();
       
				if(!empty($user_ids))
				$where = 'onboarding_status.user_id IN ('.implode(',',$user_ids).') AND onboarding_status.status = 1';
			else
				$where = 'onboarding_status.user_id = 0 AND onboarding_status.status = 1';

				if(!empty($users_where))
				{
					$result = $this->onboardingModel->viewUserOnboardingwhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewUserOnboarding($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}
	public function _viewOnboardingComplete($request)
	{		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}	
		if($permission->can_view == 'Y')
		{			
			/* Here we Add the userss.. If User has the permission for that. */
      if(TRUE)
			{
				
					if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->users->_getUserId();
       
					if(!empty($user_ids))
					$where = 'onboarding_status.user_id IN ('.implode(',',$user_ids).') AND onboarding_status.status = 1';
				else
					$where = 'onboarding_status.user_id = 0 AND onboarding_status.status = 1';

				if(!empty($users_where))
				{
					$result = $this->onboardingModel->viewUserOnboardingwhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewUserOnboarding($where);
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}

	// Add Parent Id
	public function _addParentUser($request)
    {
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Onboarding. If User has the permission for that. */
			if(isset($request->user_id))
			{	
			    	$user_where = array(
				    'id' => $request->user_id,
				);
				if(isset($request->parent_id))
				
		       $parenrt_wherename = implode(",",$request->parent_id);
			  
				$user_parent['parent_id'] = $parenrt_wherename;
				$result = $this->onboardingModel->addParent($user_parent,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reporting Manager Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reporting Manager Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Parent Document.");
		}
    }

   
   
    // Edit Onboarding
    public function _editOnboarding($request)
    {
		$onboarding = array();
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the offer letter.. If User has the permission for that. */

			if(isset($request->user_id))
			{
			    $user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$onboarding_where = array(
					'user_id' => $request->user_id
				     
				);
                $onboarding = array(
					'modify_by' => $user_id,
		   'status' =>1
		  
				);
				$onboarding_status = array(
		          'onboarding_status' =>1
				);
				if(isset($request->offer_letter))
					$onboarding['offer_letter'] = $request->offer_letter;
       if(isset($request->document_name))
					$onboarding['document_name'] = $request->document_name;
			

				$result = $this->onboardingModel->editOnboarding($onboarding,$onboarding_where);
			
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Offer Letter Uploaded Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Offer Letter Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Offer Letter.");
		}
    }
     // Edit Experience
    public function _editExperience($request)
    {
		$onboarding = array();
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Experience letter.. If User has the permission for that. */

			if(isset($request->user_id))
			{
			    $user = $this->api->_GetTokenData();
				$user_id = $user['id'];	
				$experience_where = array(
					'user_id' => $request->user_id
				
				);

                  $experience = array(
					'modify_by' => $user_id
         
					);
				if(isset($request->experience_letter))
					$experience['experience_letter'] = $request->experience_letter;
			if(isset($request->document_name))
					$experience['document_name'] = $request->document_name;

				$result = $this->onboardingModel->editExperience($experience,$experience_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Experience Letter Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Experience Letter Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Experience Letter.");
		}
    }
    // Edit Personal Document
    public function _editPersonaldocument($request)
    {
		$personal = array();
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Personal letter.. If User has the permission for that. */

			if(isset($request->user_id))
			{
			    $user = $this->api->_GetTokenData();    
				$user_id = $user['id'];	
		        	$personal_where = array(
					'user_id' => $request->user_id
					
			     	);
                   $personal = array(
					'modify_by' => $user_id,
          'status' =>1
					);
					
				if(isset($request->document_name))
					$personal['document_name'] = $request->document_name;
				if(isset($request->personaldocument))
					$personal['personaldocument'] = $request->personaldocument;

				$result = $this->onboardingModel->editPersonal($personal,$personal_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Personal Letter Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Personal Letter Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Personal Letter.");
		}
	}
// check Experience Details If Allready Exist In DataBase
public function _checkBasicDetailsAvailability($request)
{		
		$module_name = 'onboarding';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	
	if($permission->can_add == 'Y')
	{
		/* Here we check the Experience Location. If User has the permission for that. */

		if(isset($request->user_id))
		{
			
			$where_basic = array(
				'user_id' => $request->user_id
			);

			$result = $this->onboardingModel->checBasicDetailsAvailability($where_basic);
			if($result)
				return array("message" => $request->user_id." is Available.");
			else
				$this->api->_sendError("Basic Details is Still Pending. Please Fill The Details.");
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Add Onboarding.");
	}
}
	// check Experience Details If Allready Exist In DataBase
	public function _checkExperienceDetailsAvailability($request)
	{		
			$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the Experience Location. If User has the permission for that. */

			if(isset($request->user_id))
			{
				
				$where_education = array(
					'user_id' => $request->user_id
				);

				$result = $this->onboardingModel->checExperienceDetailsAvailability($where_education);
				if($result)
					return array("message" => $request->user_id." is Available.");
				else
					$this->api->_sendError("Experience Details is Still Pending. Please Fill The Details.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Onboarding.");
		}
	}
		// check Education Details If Allready Exist In DataBase
		public function _checkEducationDetailsAvailability($request)
		{		
				$module_name = 'onboarding';
			$permissions = $this->users->_checkPermission($module_name);
			foreach($permissions as $permission){}
			
			if($permission->can_add == 'Y')
			{
				/* Here we check the Branch Location. If User has the permission for that. */
	
				if(isset($request->user_id))
				{
					
					$where_education = array(
						'user_id' => $request->user_id
					);
	
					$result = $this->onboardingModel->checEducationDetailsAvailability($where_education);
					if($result)
						return array("message" => $request->user_id." is Available.");
					else
						$this->api->_sendError("Education Details is Still Pending. Please Fill The Details.");
				}
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, You Have No Permission To Add Onboarding.");
			}
		}

		// check Education Details If Allready Exist In DataBase
		public function _checkBankDetailsAvailability($request)
		{		
				$module_name = 'onboarding';
			$permissions = $this->users->_checkPermission($module_name);
			foreach($permissions as $permission){}
			
			if($permission->can_add == 'Y')
			{
				/* Here we check the Branch Location. If User has the permission for that. */
	
				if(isset($request->user_id))
				{
					
					$where_education = array(
						'user_id' => $request->user_id
					);
	
					$result = $this->onboardingModel->checkBankDetailsAvailability($where_education);
					if($result)
						return array("message" => $request->user_id." is Available.");
					else
						$this->api->_sendError("Bank Details is Still Pending. Please Fill The Details.");
				}
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, You Have No Permission To Add Onboarding.");
			}
		}
	 // Edit Personal Document
	 public function _editStatusdoc($request)
	 {
		 $personal = array();
		 $module_name = 'onboarding';
		 $permissions = $this->users->_checkPermission($module_name);
		 foreach($permissions as $permission){}
		 
		 if($permission->can_edit == 'Y')
		 {
			 /* Here we Update/Edit the Personal letter.. If User has the permission for that. */
 
			 if(isset($request->user_id) AND isset($request->status))
			 {
				
				$status = $this->_checkEducationDetailsAvailability($request);
				$status2 = $this->_checkExperienceDetailsAvailability($request);
				$status3 = $this->_checkBasicDetailsAvailability($request);
				$status4 = $this->_checkBankDetailsAvailability($request);
				
				 $user = $this->api->_GetTokenData();    
				 $user_id = $user['id'];
				 $user_name = $user['first_name'];	
					 $personal_where = array(
					 'user_id' => $request->user_id
					 
					  );
					$personal = array(
						'status_change_by' =>$user_name,
		                'status' =>$request->status
					 );
 
					 $result = $this->onboardingModel->editchangeStatus($personal,$personal_where);
				 if($result)
				 {
					 return array(
						 'status' => '1',
						 'message' => 'Onboarding Status is changed Successfully.'
					 );
				 }
				 else
				 {
					 $this->api->_sendError("Sorry, Personal Letter Not Updated. Please Try Again!!");
				 }
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		 }
		 else
		 {
			 $this->api->_sendError("Sorry, You Have No Permission To Edit Onboarding Status.");
		 }
	 }
     // Edit Personal Document
    public function _editOtherdocument($request)
    {
		$other = array();
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Personal letter.. If User has the permission for that. */

			if(isset($request->user_id))
			{
			    $user = $this->api->_GetTokenData();    
				$user_id = $user['id'];	
			$other_where = array(
					'user_id' => $request->user_id
				
				);
                  $other = array(
					'modify_by' => $user_id,
           'status' =>1
					);
				if(isset($request->other))
					$other['other'] = $request->other;
					if(isset($request->document_name))
					$other['document_name'] = $request->document_name;
					if(isset($request->instruction_document))
					$other['instruction_document'] = $request->instruction_document;
			

				$result = $this->onboardingModel->editOther($other,$other_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Document Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Document Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Document.");
		}
    }
    //  Delete The Onboarding
    public function _deleteOnboarding($request)
    {
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Onboarding. If User has the permission for that. */
			if(isset($request->onboarding_id))
			{
				$where_onboarding = array(
					'id' => $request->onboarding_id,
				);
				$result = $this->onboardingModel->deleteOnboarding($where_onboarding);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Onboarding Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Onboarding Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Onboarding.");
		}
    }
    //  Delete The Other
    public function _deleteOther($request)
    {
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Onboarding. If User has the permission for that. */
			if(isset($request->other_id))
			{
				$where_other = array(
					'id' => $request->other_id,
				);
				$result = $this->onboardingModel->deleteOther($where_other);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Document Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Documents Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Document.");
		}
    }
     //  Delete The Experience
    public function _deleteExperience($request)
    {
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Experience. If User has the permission for that. */
			if(isset($request->experience_id))
			{
				$where_experience = array(
					'id' => $request->experience_id,
				);
				$result = $this->onboardingModel->deleteExperience($where_experience);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Experience Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Experience Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Experience.");
		}
    }
//  Delete personal Detatil
    public function _deletePersonal($request)
    {
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Onboarding. If User has the permission for that. */
			if(isset($request->persoanl_id))
			{
				$where_onboarding = array(
					'id' => $request->persoanl_id,
				);
				$result = $this->onboardingModel->deletePersoanl($where_onboarding);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Personal Document Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Personal Document Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Personal Document.");
		}
    }

    // view Logged In Onboarding
  public function _viewLoggedinUser($request)
    {		
		
			/* Here we View the Onboarding.. If User has the permission for that. */  
			if(TRUE)
			{
				  $user = $this->api->_GetTokenData();    
		      $user_id = $user['id'];	
					$onboarding_where['user_onboarding.user_id'] = $user_id;
					$result = $this->onboardingModel->viewOnboardingLoggedWhere($onboarding_where);
					return $result;		
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
   
    // view Logged In Onboarding
  public function _viewLoggedinPersonal($request)
    {		
		
			/* Here we View the Onboarding.. If User has the permission for that. */  
			if(TRUE)
			{
				  $user = $this->api->_GetTokenData();    
		      $user_id = $user['id'];	
					$onboarding_where['user_personaldocument.user_id'] = $user_id;
					$result = $this->onboardingModel->viewPesonalWhere($onboarding_where);
					return $result;		
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
    
    // view Logged In Onboarding
  public function _viewLoggedinExperience($request)
    {		
		
			/* Here we View the Onboarding.. If User has the permission for that. */  
			if(TRUE)
			{
				  $user = $this->api->_GetTokenData();    
		      $user_id = $user['id'];	
					$onboarding_where['user_experience.user_id'] = $user_id;
					$result = $this->onboardingModel->viewExperienceWhere($onboarding_where);
					return $result;		
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	
    }
    
      // view Logged In Onboarding
  public function _viewLoggedinOther($request)
    {		
		
			/* Here we View the Onboarding.. If User has the permission for that. */  
			if(TRUE)
			{
				  $user = $this->api->_GetTokenData();    
		      $user_id = $user['id'];	
					$onboarding_where['user_other.user_id'] = $user_id;
					$result = $this->onboardingModel->viewOtherWhere($onboarding_where);
					return $result;	
          
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
    
     // view Onboarding
  public function _viewOnboarding($request)
    {		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$onboarding_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Onboarding.. If User has the permission for that. */  
			if(TRUE)
			{
				if(isset($request->user_id))
				{
					$onboarding_where['user_onboarding.user_id'] = $request->user_id;
					$result = $this->onboardingModel->viewOnboardingWhere($onboarding_where);
					return $result;
				}
				else if(isset($request->onboarding_id)){
				    $onboarding_where['user_onboarding.id'] = $request->onboarding_id;
					$result = $this->onboardingModel->viewOnboardingWhere($onboarding_where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewOnboarding();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Onboarding.");
		}
    }
    // view Personal Documemt
  public function _viewPersonalDocument($request)
    {		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$onboarding_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Personal Documents.. If User has the permission for that. */  
			if(TRUE)
			{
				if(isset($request->user_id))
				{
					$onboarding_where['user_personaldocument.user_id'] = $request->user_id;
					$result = $this->onboardingModel->viewPesonalWhere($onboarding_where);
					return $result;
				}
			   else if(isset($request->personal_id))
				{
					$onboarding_where['user_personaldocument.id'] = $request->personal_id;
					$result = $this->onboardingModel->viewPesonalWhere($onboarding_where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewPesonal();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Personal Document.");
		}
    }
     // view Experience Documemt
    public function _viewExperience($request)
    {		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$experience_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Onboarding.. If User has the permission for that. */  
			if(TRUE)
			{
				if(isset($request->user_id))
				{
					$experience_where['user_experience.user_id'] = $request->user_id;
					$result = $this->onboardingModel->viewExperienceWhere($experience_where);
					return $result;
				}
				else if(isset($request->experience_id)){
				    	$Experience_where['user_experience.id'] = $request->experience_id;
					$result = $this->onboardingModel->viewExperienceWhere($Experience_where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewExperience();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Experience Letter.");
		}
    }
     // view other Documemt
    public function _viewOtherDocument($request)
    {		
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$other_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Other Document. If User has the permission for that. */  
			if(TRUE)
			{
				if(isset($request->user_id))
				{
					$other_where['user_other.user_id'] = $request->user_id;
					$result = $this->onboardingModel->viewOtherWhere($other_where);
					return $result;
				}
				else if(isset($request->other_id))
				{
					$other_where['user_other.id'] = $request->other_id;
					$result = $this->onboardingModel->viewOtherWhere($other_where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewOther();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Documents.");
		}
	}

	// Check  Paren id Details 
	public function _viewOnboardingOffice($request)
	{		
	   
			/* Here we View the Other Document. If User has the permission for that. */ 

			 if(TRUE)
			{
				
				if(isset($request->user_id))
				{
					$office_where['users.id'] = $request->user_id;
					$result = $this->onboardingModel->viewOnboardingOfficeWhere($office_where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewOnboardingOffice();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}
	// Check  BAnk id Details 
	public function _viewOnboardingBank($request)
	{		
	   
			/* Here we View the Other Document. If User has the permission for that. */ 

			 if(TRUE)
			{
				
				if(isset($request->user_id))
				{
					$bank_where['users_bank_details.user_id'] = $request->user_id;
					$result = $this->onboardingModel->viewOnboardingBankWhere($bank_where);
					return $result;
				}
				else
				{
					$result = $this->onboardingModel->viewOnboardingBank();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}
	// Check  Office Details 
	public function _checkOfficeDetails($request)
	{		
	   
			/* Here we Check the Office Details. If User has the permission for that. */  
			if(isset($request->user_id))
			{
			   $credential = array(
				   'id' => $request->user_id,
					);
					$user = $this->onboardingModel->checkOfficeDetails($credential);
				
				   if($user->branch_id == 0)
				   {
					   return array(
						   'status' => '0',
						   'message' => 'Office Details  not Submitted.'
					   );
				   }else{
					   return array(
						   'status' => '1',
						   'message' => 'Office Details Submitted.'
					   );
				   }

			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}

	// Check  Paren id Details 
	public function _checkParentDetails($request)
	{		
	   
			/* Here we View the Other Document. If User has the permission for that. */  
			if(isset($request->user_id))
			{
			   $credential = array(
				   'id' => $request->user_id,
					);
					$user = $this->onboardingModel->checkOfficeDetails($credential);
				
				   if($user->parent_id == 0)
				   {
					   return array(
						   'status' => '0',
						   'message' => 'Parent Details not submitted .'
					   );
				   }else{
					   return array(
						   'status' => '1',
						   'message' => 'Parent Details sumitted .'
					   );
				   }

			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}
	// Check  Paren id Details 
	public function _checkLeavePolicyDetails($request)
	{		
	   
			/* Here we View the Other Document. If User has the permission for that. */  
			if(isset($request->user_id))
			{
			   $credential = array(
				   'id' => $request->user_id,
					);
					$user = $this->onboardingModel->checkOfficeDetails($credential);
				
				   if($user->leave_policy_id == 0)
				   {
					   return array(
						   'status' => '0',
						   'message' => 'Parent Details  Not Done.'
					   );
				   }else{
					   return array(
						   'status' => '1',
						   'message' => 'Parent Details Done .'
					   );
				   }

			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}
	// Check  Office Details 
	public function _checkEducationDetails($request)
	{		
	   
			/* Here we View the Other Document. If User has the permission for that. */  
			if(isset($request->user_id))
			{
			   $credential = array(
				   'user_id' => $request->user_id,
					);
					$result = $this->onboardingModel->checEducationDetailsAvailability($credential);
				
					if($result==TRUE)
					{
					   return array(
						   'status' => '1',
						   'message' => 'Education Details Submitted.'
					   );
				   }else{
					   return array(
						   'status' => '0',
						   'message' => 'Education Details not Submitted.'
					   );
				   }

			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}
	// Check  Experience Details 
	public function _checkExperienceDetails($request)
	{		
	   
			/* Here we View the Other Document. If User has the permission for that. */  
			if(isset($request->user_id))
			{
			   $credential = array(
				   'user_id' => $request->user_id,
					);
					$result = $this->onboardingModel->checExperienceDetailsAvailability($credential);
				
					if($result==TRUE)
					{
					   return array(
						   'status' => '1',
						   'message' => 'Experience Details Submitted.'
					   );
				   }else{
					   return array(
						   'status' => '0',
						   'message' => 'Experience Details Not Submitted.'
					   );
				   }

			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}
	// Check  Bank Details 
	public function _checkBankDetails($request)
	{		
	   
			/* Here we View the Other Document. If User has the permission for that. */  
			if(isset($request->user_id))
			{
			   $credential = array(
				   'user_id' => $request->user_id,
					);
					$result = $this->onboardingModel->checkBankDetailsAvailability($credential);
				
					if($result==TRUE)
					{
					   return array(
						   'status' => '1',
						   'message' => 'Bank Details Submitted.'
					   );
				   }else{
					   return array(
						   'status' => '0',
						   'message' => 'Bank Details Not Submitted.'
					   );
				   }

			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	   
	}
	 // Check  Offer Letter Documemt
	 public function _checkDocumentUploaded($request)
	 {		
		
			 /* Here we View the Other Document. If User has the permission for that. */  
			 if(isset($request->user_id))
			 {
				$credential = array(
					'user_id' => $request->user_id,
					 );
					 $result = $this->onboardingModel->checkOnboardingDoc($credential);
					if($result==TRUE)
					{
						return array(
							'status' => '1',
							'message' => 'Offer Letter Uploaded.'
						);
					}else{
						return array(
							'status' => '0',
							'message' => 'Offer Letter Not Uploaded.'
						);
					}

			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		
	 }
	 
	 // check Experience document 
	 public function _checkExperienceDocument($request)
	 {		
			 /* Here we View the Other Document. If User has the permission for that. */  
			 if(isset($request->user_id))
			 {
				$credential = array(
					'user_id' => $request->user_id,
					 );
					 $result = $this->onboardingModel->checkOnboardingExperience($credential);
					if($result==TRUE)
					{
						return array(
							'status' => '1',
							'message' => 'Experience Letter Uploaded.'
						);
					}else{
						return array(
							'status' => '0',
							'message' => 'Experience Letter Not Document Uploaded.'
						);
					}
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		
	 }

	 // check Other Letter  document 
	 public function _checkOtherDocument($request)
	 {		
			 /* Here we View the Other Document. If User has the permission for that. */  
			 if(isset($request->user_id))
			 {
				$credential = array(
					'user_id' => $request->user_id,
					 );
					 $result = $this->onboardingModel->checkOnboardingOther($credential);
					if($result==TRUE)
					{
						return array(
							'status' => '1',
							'message' => 'Other Letter Uploaded.'
						);
					}else{
						return array(
							'status' => '0',
							'message' => 'Other Letter Not Document Uploaded.'
						);
					}
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		
	 }
// check personal document 
	 public function _checkPersonalDocument($request)
	 {		
			 /* Here we View the Other Document. If User has the permission for that. */  
			 if(isset($request->user_id))
			 {
				$credential = array(
					'user_id' => $request->user_id,
					 );
					 $result = $this->onboardingModel->checOnboardingPersonal($credential);
					if($result==TRUE)
					{
						return array(
							'status' => '1',
							'message' => 'Personal Letter Uploaded.'
						);
					}else{
						return array(
							'status' => '0',
							'message' => 'Personal Letter Not Document Uploaded.'
						);
					}
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		
	 }
}
