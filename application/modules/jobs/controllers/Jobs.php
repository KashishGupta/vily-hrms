<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Jobs.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the Jobs.
*/

class Jobs extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('jobsModel');
	}

	/* -------------------------------------------------------------------------- */
	/*       Add New Job Post Function Where a Unique 6 Digit Code Generated      */
	/* -------------------------------------------------------------------------- */

	public function _addJob($request)
    {	
			
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Jobs. If User has the permission for that. */

			if(isset($request->department_id) AND isset($request->job_title) AND isset($request->start_date) AND isset($request->key_skill) AND isset($request->end_date) AND isset($request->job_type_id))
			{
      
             
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$user_Roleid = $user['role_id'];
				if($user_Roleid == 1)
				{
					$date = new DateTime("now");
 
				$curr_date = $date->format('Y-m-d ');
				
					$job = array(
						'job_number' => $this->api->_getRandomString(6),
						'department_id' => $request->department_id,
						'start_date' => $request->start_date,
						'end_date' => $request->end_date,
						'job_type_id' => $request->job_type_id,
						'key_skill' => $request->key_skill,
						'job_title' => $request->job_title,
						'approval'	=>	1,
						'added_by'	=> $user_id,
						'approved_at'	=> $curr_date,
						'modify_by'	=>	$user_id
	
					);
	                
					if(isset($request->status))
						$job['status'] = $request->status;
					else
						$job['status'] = 0;				// By Default Open
	
					if(isset($request->no_of_post))
						$job['no_of_post'] = $request->no_of_post;
					else
						$job['no_of_post'] = 1;				// By Default Open
	
					if(isset($request->job_description))
						$job['job_description'] = $request->job_description;

						$jobStartDate=strtotime($request->start_date);
						$jobEndDate=strtotime($request->end_date);
						$currentDate = date('d-m-Y');
						$currentDate=strtotime($currentDate);
					/* End Date Conversion */
			   if($jobStartDate < $currentDate){
				   $this->api->_sendError('Job Cannot Be Applied On Past Date.');
			   }
			   if($jobStartDate > $jobEndDate)
			   {
				$this->api->_sendError('End Date Ahead From Start Date.');
			   }
				if($jobStartDate < $currentDate)
				{
				 $this->api->_sendError('Start Date Ahead From End Date.');
				}
					$result = $this->jobsModel->addJob($job);
				}
				else{

				
				$job = array(
					'job_number' => $this->api->_getRandomString(6),
					'department_id' => $request->department_id,
					'start_date' => $request->start_date,
					'key_skill' => $request->key_skill,
						'job_title' => $request->job_title,
					'end_date' => $request->end_date,
					'job_type_id' => $request->job_type_id,
					'approval'	=>	0,
					'added_by'	=> $user_id,
					'modify_by'	=>	$user_id

				);

				if(isset($request->status))
					$job['status'] = $request->status;
				else
					$job['status'] = 0;				// By Default Open

				if(isset($request->no_of_post))
					$job['no_of_post'] = $request->no_of_post;
				else
					$job['no_of_post'] = 1;				// By Default Open

				if(isset($request->job_description))
					$job['job_description'] = $request->job_description;

						$jobStartDate=strtotime($request->start_date);
						$jobEndDate=strtotime($request->end_date);
						$currentDate = date('d-m-Y');
						$currentDate=strtotime($currentDate);
				/* End Date Conversion */
           if($jobStartDate < $currentDate){
           	$this->api->_sendError('Job Cannot Be Applied On Past Date.');
           }
           if($jobStartDate > $jobEndDate)
		   {
			 $this->api->_sendError('End Date Ahead From Start Date.');
		   }
		   if($jobStartDate < $currentDate)
		   {
			$this->api->_sendError('Start Date Ahead From End Date.');
			}
			 $result = $this->jobsModel->addJob($job);
			}
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Job.");
		}
    }

    public function _deleteJob($request)
    {		
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Jobs. If User has the permission for that. */

			if(isset($request->job_id))
			{
				$where_job = array(
					'id' => $request->job_id,
				);
				$result = $this->jobsModel->deleteJob($where_job);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Job.");
		}
    }

    public function _editJob($request)
    {		
		$job = array();
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$job_where = array();
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Edit the Jobs.. If User has the permission for that. */

			if(isset($request->job_id))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$job_where = array(
					'id' => $request->job_id
				);

				$job['modify_by'] = $user_id;
				if(isset($request->job_description))
					$job['job_description'] = $request->job_description;
				if(isset($request->department_id))
					$job['department_id'] = $request->department_id;
    	  if(isset($request->key_skill))
					$job['key_skill'] = $request->key_skill;
        if(isset($request->job_title))
					$job['job_title'] = $request->job_title;
				if(isset($request->status))
					$job['status'] = $request->status;
				if(isset($request->start_date))
					$job['start_date'] = $request->start_date;
				if(isset($request->end_date))
					$job['end_date'] = $request->end_date;
				if(isset($request->job_type_id))
					$job['job_type_id'] = $request->job_type_id;
				if(isset($request->no_of_post))
					$job['no_of_post'] = $request->no_of_post;
				
				$result = $this->jobsModel->editJob($job,$job_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Job.");
		}
	}
	 // view Total Last Month Jobs
	 public function _totalLastMonthJobs($request)
	 {
	 $module_name = 'jobs';
	 $permissions = $this->users->_checkPermission($module_name);
	 foreach($permissions as $permission){}
	 if($permission->can_view == 'Y')
	 {
	 if(TRUE)
	 {
		
	 $result = $this->jobsModel->totalLastMonthJobs();
	 return $result;
	 }
	 else
	 {
	 $this->api->_sendError("Sorry, There Is Some Pereameter Missing. Please Try Again!!");
	 }
	 }
	 else
	 {
	 $this->api->_sendError("Sorry, You Have No Permission To View Total Close Jobs");
	 }
	 }
    // view Total Close Jobs
    public function _totalCloseJobs($request)
    {
    $module_name = 'jobs';
    $permissions = $this->users->_checkPermission($module_name);
    foreach($permissions as $permission){}
    if($permission->can_view == 'Y')
    {
    if(TRUE)
    {
	
    $result = $this->jobsModel->totalCloseJobs();
    return $result;
    }
    else
    {
    $this->api->_sendError("Sorry, There Is Some Pereameter Missing. Please Try Again!!");
    }
    }
    else
    {
    $this->api->_sendError("Sorry, You Have No Permission To View Total Close Jobs");
    }
    }
    // view totl Hold Jobs
    public function _totalHoldJobs($request)
    {
    $module_name = 'jobs';
    $permissions = $this->users->_checkPermission($module_name);
    foreach($permissions as $permission){}
    if($permission->can_view == 'Y')
    {
    if(TRUE)
    {
	
    $result = $this->jobsModel->totalHoldJobs();
    return $result;
    }
    else
    {
    $this->api->_sendError("Sorry, There is Some Perameter Misssing. Please Try Again!!");
    }
    }
    else
    {
    $this->api->_sendError("Sorry, You Have No Permission To View Hild Jobs!!");
    }
    }
    // View Total Jobs
    public function _totalJobs($request)
    {
    $module_name = 'jobs';
    $permissions = $this->users->_checkPermission($module_name);
    foreach($permissions as $permission){}
    if($permission->can_view == 'Y')
    {
    if(TRUE)
 {
	
 $result = $this->jobsModel->totalJobs();
 return $result;
 }
 else
 {
$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
 }
 }
 else
 {
	$this->api->_sendError("Sorry, You Have No Permission To View Open Jobs.");
 }
 }
// View Open Jobs...
 public function _totalOpenJobs($request)
 {
	$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
 	if($permission->can_view == 'Y')
		{
 if(TRUE)
 {
	
 $result = $this->jobsModel->totalOpenJobs();
 return $result;
 }
 else
 {
$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
 }
 }
 else
 {
	$this->api->_sendError("Sorry, You Have No Permission To View Open Jobs.");
 }
 }
 // view Today Jobs Opening
    public function _viewTodayJob($request)
    {		
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$job_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Jobs.. If User has the permission for that. */

			if(TRUE)
			{
					
					$result = $this->jobsModel->ViewTodayJobs();
					return $result;
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Jobs.");
		}
	}
	// view Current Month Jobs Opening
    public function _viewCurrentMonthJob($request)
    {		
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$job_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Jobs.. If User has the permission for that. */

			if(TRUE)
			{
					
					$result = $this->jobsModel->ViewCurrentMonthJobs();
					return $result;
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Jobs.");
		}
	}

		// view Current Month Jobs Opening
		public function _viewApplicatiionMonth($request)
		{		
			$module_name = 'jobs';
			$permissions = $this->users->_checkPermission($module_name);
			foreach($permissions as $permission){}
			$job_where = array();

			  if($permission->can_view == 'Y')
			  {
				/* Here we View the Jobs.. If User has the permission for that. */

				if(TRUE)
				{

					$result = $this->jobsModel->ViewApplicationMonth();
					return $result;
				
				}
				else
				{
				    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			  }
				else
				{
				    $this->api->_sendError("Sorry, You Have No Permission To View Jobs.");
				}
		}
		// view Current Month Jobs Opening
		public function _viewLastSevenApplication($request)
		{		
			$module_name = 'jobs';
			$permissions = $this->users->_checkPermission($module_name);
			foreach($permissions as $permission){}
			$job_where = array();

			  if($permission->can_view == 'Y')
			  {
				/* Here we View the Jobs.. If User has the permission for that. */

				if(TRUE)
				{

					$result = $this->jobsModel->ViewLastSeven();
					return $result;
				
				}
				else
				{
				    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			  }
				else
				{
				    $this->api->_sendError("Sorry, You Have No Permission To View Jobs.");
				}
		}
		// view Current Month Jobs Opening
		public function _viewChartApplication($request)
		{		
			$module_name = 'jobs';
			$permissions = $this->users->_checkPermission($module_name);
			foreach($permissions as $permission){}
			$job_where = array();

			  if($permission->can_view == 'Y')
			  {
				/* Here we View the Jobs.. If User has the permission for that. */

				if(TRUE)
				{

					$result = $this->jobsModel->ViewChartData();
					return $result;
				
				}
				else
				{
				    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			  }
				else
				{
				    $this->api->_sendError("Sorry, You Have No Permission To View Jobs.");
				}
		}
	// view View Approved Date Jobs Count
    public function _viewApprovedDateJob($request)
    {		
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$job_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Jobs.. If User has the permission for that. */

			if(TRUE)
			{
					
					$result = $this->jobsModel->viewApprovedJob();
					return $result;
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Jobs.");
		}
	}
 // View Jobs
    public function _viewJob($request)
    {		
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$job_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Jobs.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->department_id))
					$job_where['jobs.department_id'] = $request->department_id;
				if(isset($request->job_id))
					$job_where['jobs.id'] = $request->job_id;
				if(isset($request->job_type_id))
					$job_where['jobs.job_type_id'] = $request->job_type_id;
				if(isset($request->status))
					$job_where['jobs.status'] = $request->status;
				
				if(!empty($job_where))
				{
					$result = $this->jobsModel->viewJobWhere($job_where);
					return $result;
				}
				else
				{
					$result = $this->jobsModel->viewJob();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Jobs.");
		}
	}
	
	public function _viewJobPublic($request)		// This is a Public API No Need To Check Token
    {
		$job_where = array();
		if(isset($request->department_id))
			$job_where['jobs.department_id'] = $request->department_id;
		if(isset($request->job_id))
			$job_where['jobs.id'] = $request->job_id;
		if(isset($request->job_type_id))
			$job_where['jobs.job_type_id'] = $request->job_type_id;
		if(isset($request->status))
			$job_where['jobs.status'] = $request->status;

		if(!empty($job_where))
		{
			$result = $this->jobsModel->viewPublicJob($job_where);
			return $result;
		}
		else
		{
			$result = $this->jobsModel->viewPublicJobs();
			return $result;
		}
	}
	
	public function _changeJobStatus($request)
    {
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Edit the Jobs.. If User has the permission for that. */

			if(isset($request->job_id))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$job_where = array(
					'id' => $request->job_id
				);

			
				$date = new DateTime("now");
 
				$curr_date = $date->format('Y-m-d ');
				$job['approved_at'] = $curr_date;
			    $job['modify_by'] = $user_id;
				if(isset($request->approval))
		      	    $job['approval'] = $request->approval;
        	    if(isset($request->job_status))
		      	    $job['status'] = $request->job_status;
			
				$result = $this->jobsModel->editJob($job,$job_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Status Changed Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Status Not Changed. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Jobs Status.");
		}
	}
	
	//job Education Type Applicants
    public function _viewJobEducation($request)
    {		
			if(TRUE)
			{	
					$result = $this->jobsModel->viewJobEducation();
					return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	}
}
