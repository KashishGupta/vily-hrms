<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: JobsModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Jobs.
*/

class JobsModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
   
    public function GetData()
    {
      $this->db->select('*');
      $this->db->from('Get_data');
      $query= $this->Get_result();
      return $query;
    }
    public function addJob($data)
    {
        $this->db->insert('jobs',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteJob($where_job)
    {
        $this->db->where($where_job);
        $this->db->delete('jobs');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editJob($data,$where)
    {
        $this->db->where($where);
        $this->db->update('jobs',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function totalLastMonthJobs()
    {
       
       $date = new DateTime("now");
       $curr_date = $date->format('Y-m-d ');
       $this->db->select('count(*) as LastMonth');
       $this->db->from('jobs');
       $this->db->where('YEAR(start_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)');
       $this->db->where('MONTH(start_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)');
     
       $query = $this->db->get();
       return $query->result();
    }
    // view Today Jobs Opening
    public function ViewLastSeven()
    {
    //SELECT * FROM `job_applicants` WHERE `apply_date` BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()
        $date = new DateTime("now");
 
        $curr_date = $date->format('Y-m-d ');
        $this->db->select('job_applicants.first_name,company_departments.department_name');
        $this->db->from('job_applicants');
        $this->db->join('jobs', 'jobs.id = job_applicants.job_id');

        $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
        $this->db->where('apply_date BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()');
       
        $query = $this->db->get();
        return $query->result();
    }

    public function viewJobEducation()
    {
  
        $this->db->select('*');
        $this->db->from('education_type');
        $query = $this->db->get();
        return $query->result();
    }
    
    // view Today Jobs Opening
    public function ViewChartData()
    {
        $query = $this->db->query("SELECT YEAR(`apply_date`) as applicantsDate , Count(*) as ApplicantsCount FROM `job_applicants` GROUP BY YEAR(`apply_date`) ORDER BY 1
        ");
        $arrApplicantsData = $query->result();
        $i=0;
        foreach($arrApplicantsData as $key => $arrtest)
        {
        if($i>0){
          $applicantsDate['Applicants'] = $applicantsDate['Applicants']." , ".$arrtest->applicantsDate;
         $applicantsDate['data1']= $applicantsDate['data1']." , ".$arrtest->ApplicantsCount;
        }
        else{
        $applicantsDate['Applicants'] = $arrtest->applicantsDate;
         $applicantsDate['data1']= $arrtest->ApplicantsCount;
        }
         
         $arApplicants[0] = $applicantsDate;
         $i++;
        }
        //Applied By Year
        $query = $this->db->query("SELECT YEAR(`apply_date`) as AppliedDate , Count(*) as AppliedCount FROM `job_applicants` WHERE `status` = 0 GROUP BY YEAR(`apply_date`) ORDER BY 1
        ");
        $AppliedData = $query->result();
        $i=0;
        foreach($AppliedData as $key => $value)
        {
        
        if($i>0){
   
         $appliedData['data2'] =  $appliedData['data2'].",".$value->AppliedCount;
        
         }
        else{
        
        
         $appliedData['data2'] = $value->AppliedCount;
        }
         $arrApplied[0] = $appliedData;
          $i++;
        } 
        //Shortlisted by Year
        $query = $this->db->query("SELECT YEAR(`apply_date`) as ShortlistedDate , Count(*) as ShortlistedCount FROM `job_applicants` WHERE `status` = 1 GROUP BY YEAR(`apply_date`) ORDER BY 1");
        $ShortlistedData = $query->result();
         $i=0;
        foreach($ShortlistedData as $key => $row)
        {
         if($i>0){
      
         $ShortlistedDate['data3'] =$ShortlistedDate['data3'].",". $row->ShortlistedCount;
          }
        else{
         
         $ShortlistedDate['data3'] = $row->ShortlistedCount;
        }
         $arrShortlisted[0]= $ShortlistedDate;
         $i++;
        }
         //interviewed  by Eaar
         $query = $this->db->query("SELECT YEAR(`apply_date`) as interviewedDate , Count(*) as interviewedCount FROM `job_applicants` WHERE `status` = 2 GROUP BY YEAR(`apply_date`) ORDER BY 1");
         $interviewedData = $query->result();
          $i=0;
         foreach($interviewedData as $key => $row)
         {
          if($i>0){
         
          $interviewedDate['data4'] =$interviewedDate['data4'].",". $row->interviewedCount;
           }
         else{
          
          $interviewedDate['data4'] = $row->interviewedCount;
         }
          $arrinterviewed[0]= $interviewedDate;
          $i++;
         }
          //rejected  by Eaar
          $query = $this->db->query("SELECT YEAR(`apply_date`) as interviewedDate , Count(*) as rejectedCount FROM `job_applicants` WHERE `status` = 3 GROUP BY YEAR(`apply_date`) ORDER BY 1");
          $rejectedData = $query->result();
           $i=0;
          foreach($rejectedData as $key => $row)
          {
           if($i>0){
          
           $rejectedDate['data5'] =$rejectedDate['data5'].",". $row->rejectedCount;
            }
          else{
           
           $rejectedDate['data5'] = $row->rejectedCount;
          }
           $arrrejected[0]= $rejectedDate;
           $i++;
          }
        $arrMerge = array_merge($arApplicants,$arrApplied,$arrShortlisted,$arrinterviewed,$arrrejected);
        return $arrMerge;
    }
    // view Today Jobs Opening
    public function ViewTodayJobs()
    {
        $date = new DateTime("now");
 
        $curr_date = $date->format('Y-m-d ');
        $this->db->select('count(*) as today');
        $this->db->from('jobs');
        $this->db->where('start_date', $curr_date);
       
        $query = $this->db->get();
        return $query->result();
    }
     // view Approved DAte Jobs Opening
     public function viewApprovedJob()
     {
         $date = new DateTime("now");
  
         $curr_date = $date->format('Y-m-d ');
         $this->db->select('count(*) as total_approved');
         $this->db->from('jobs');
         $this->db->where('approved_at', $curr_date);
         
         $query = $this->db->get();
         return $query->result();
     }
   public function totalCloseJobs()
   {
   $this->db->select('count(*) as totalCloseJobs');
   $this->db->from('jobs');
   $this->db->where('status', 1);
 
   $query = $this->db->get();
   return $query->result();
   }
    // view Total Hold Jobs
    public function totalHoldJobs()
    {
    $this->db->select('count(*) as totalHoldJobs');
    $this->db->from('jobs');
    $this->db->where('status', 2);
   
     $query = $this->db->get();
        return $query->result();
    }
// total Open Jobs
 public function totalOpenJobs()
 {
         $this->db->select('count(*) as totalOpenJobs');
        $this->db->from('jobs');
        $this->db->where('status', 0);
     
        $query = $this->db->get();
        return $query->result();
 }
 
 // view Current Month Jobs Opening
 public function ViewApplicationMonth()
 {
 
    
     $this->db->select('count(*) as totalJobsMonth');
     $this->db->from('job_applicants');
   $this->db->where('MONTH(apply_date)', date('m')); //For current month
   $this->db->where('YEAR(apply_date)', date('Y')); // For current year
  
   $this->db->order_by("apply_date","ASC");
  
   
   
   $arrTotal = $this->db->get()->result();
   //  applied
   $this->db->select('count(*) as Applied');
   $this->db->from('job_applicants');
 $this->db->where('MONTH(apply_date)', date('m')); //For current month
 $this->db->where('YEAR(apply_date)', date('Y')); // For current year

 $this->db->order_by("apply_date","ASC");
 $this->db->where('status',0);
 
 
 $arrApplied = $this->db->get()->result();
 //shortlisted
 $this->db->select('count(*) as Shortlisted');
 $this->db->from('job_applicants');
$this->db->where('MONTH(apply_date)', date('m')); //For current month
$this->db->where('YEAR(apply_date)', date('Y')); // For current year

$this->db->order_by("apply_date","ASC");
$this->db->where('status',1);


$arrshortlisted = $this->db->get()->result();
  
//interviewd
$this->db->select('count(*) as interviewd');
$this->db->from('job_applicants');
$this->db->where('MONTH(apply_date)', date('m')); //For current month
$this->db->where('YEAR(apply_date)', date('Y')); // For current year

$this->db->order_by("apply_date","ASC");
$this->db->where('status',2);


$arrinterviewd = $this->db->get()->result();

   //rejcted
$this->db->select('count(*) as rejcted');
$this->db->from('job_applicants');
$this->db->where('MONTH(apply_date)', date('m')); //For current month
$this->db->where('YEAR(apply_date)', date('Y')); // For current year

$this->db->order_by("apply_date","ASC");
$this->db->where('status',3);


$arrrejcted = $this->db->get()->result();

 $merge = array_merge($arrTotal, $arrApplied,$arrshortlisted,$arrinterviewd,$arrrejcted);
 return $merge;
 }
  // view Current Month Jobs Opening
    public function ViewCurrentMonthJobs()
    {
    
       
        $this->db->select('count(*) as totalCurrentMonthJobs');
        $this->db->from('jobs');
      $this->db->where('MONTH(start_date)', date('m')); //For current month
      $this->db->where('YEAR(start_date)', date('Y')); // For current year
     
      $this->db->order_by("start_date","ASC");
     
      $query = $this->db->get();
     
      return $query->result();
    }
 // Total Jobs
 public function totalJobs()
  {
        $this->db->select('count(*) as totalJobs');
        $this->db->from('jobs');
       
        $query = $this->db->get();
        return $query->result();
  }
  public function viewPublicJob($where2)
  {
      $this->db->select('jobs.id as job_id, jobs.job_number, jobs.key_skill, jobs.job_title, jobs.status as job_status, company_departments.id as department_id, company_departments.department_name, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location, jobs.start_date, jobs.end_date, jobs.no_of_post, job_types.id as job_type_id, job_types.job_type, job_types.type_color, jobs.job_description, COUNT(job_applicants.id) AS total_candidate,job_applicant_status.status_color,job_applicant_status.job_applicant_status');
      $this->db->from('jobs');
   
        $this->db->join('job_applicant_status', 'job_applicant_status.id = jobs.status');
      $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
      $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
      $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
      $this->db->join('job_applicants', 'jobs.id = job_applicants.job_id','left');
      $this->db->group_by('jobs.id');
      $this->db->where($where2);
      $this->db->where('approval',1);
      $query = $this->db->get();
      return $query->result();
  }
  public function viewPublicJobs()
  {
      $this->db->select('jobs.id as job_id, jobs.job_number, jobs.key_skill, jobs.job_title, jobs.status as job_status, company_departments.id as department_id, company_departments.department_name, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location, jobs.start_date, jobs.end_date, jobs.no_of_post, job_types.id as job_type_id, job_types.job_type, job_types.type_color, jobs.job_description, COUNT(job_applicants.id) AS total_candidate,job_applicant_status.status_color,job_applicant_status.job_applicant_status');
      $this->db->from('jobs');
   
        $this->db->join('job_applicant_status', 'job_applicant_status.id = jobs.status');
      $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
      $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
      $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
      $this->db->join('job_applicants', 'jobs.id = job_applicants.job_id','left');
      $this->db->group_by('jobs.id');
      $this->db->where('approval',1);
      $query = $this->db->get();
      return $query->result();
  }
    public function viewJob()
    {
      	$user = $this->api->_GetTokenData();
				$user_Roleid = $user['role_id'];
				if($user_Roleid == 1)
				{
        $this->db->select('jobs.id as job_id, jobs.job_number, jobs.key_skill, jobs.job_title, jobs.approval, jobs.status as job_status, company_departments.id as department_id, company_departments.department_name, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location, jobs.start_date, jobs.end_date, jobs.no_of_post, job_types.id as job_type_id, job_types.job_type, job_types.type_color, jobs.job_description, COUNT(job_applicants.id) AS total_candidate,job_applicant_status.status_color,job_applicant_status.job_applicant_status');
        $this->db->from('jobs');
        $this->db->join('users','jobs.added_by = users.id');
          $this->db->join('job_applicant_status', 'job_applicant_status.id = jobs.status');
        $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
        $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
        $this->db->join('job_applicants', 'jobs.id = job_applicants.job_id','left');
        $this->db->group_by('jobs.id');
        $query = $this->db->get();
        return $query->result();
        }else{
          $this->db->select('jobs.id as job_id, jobs.job_number, jobs.approval, jobs.key_skill, jobs.job_title, jobs.status as job_status, company_departments.id as department_id, company_departments.department_name, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location, jobs.start_date, jobs.end_date, jobs.no_of_post, job_types.id as job_type_id, job_types.job_type, job_types.type_color, jobs.job_description, COUNT(job_applicants.id) AS total_candidate,job_applicant_status.status_color,job_applicant_status.job_applicant_status');
          $this->db->from('jobs');
          $this->db->join('users','jobs.added_by = users.id');
            $this->db->join('job_applicant_status', 'job_applicant_status.id = jobs.status');
          $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
          $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
          $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
          $this->db->join('job_applicants', 'jobs.id = job_applicants.job_id','left');
        $this->db->group_by('jobs.id');
         $this->db->where('approval',1);
        $query = $this->db->get();
        return $query->result();
        }
    }

    public function viewJobWhere($where)
    {
    $user = $this->api->_GetTokenData();
				$user_Roleid = $user['role_id'];
				if($user_Roleid == 1)
				{
          $this->db->select('jobs.id as job_id, jobs.job_number, jobs.approval, jobs.key_skill, jobs.job_title, jobs.status as job_status, company_departments.id as department_id, company_departments.department_name, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location, jobs.start_date, jobs.end_date, jobs.no_of_post, job_types.id as job_type_id, job_types.job_type, job_types.type_color, jobs.job_description, COUNT(job_applicants.id) AS total_candidate,job_applicant_status.status_color,job_applicant_status.job_applicant_status');
          $this->db->from('jobs');
          $this->db->join('users','jobs.added_by = users.id');
            $this->db->join('job_applicant_status', 'job_applicant_status.id = jobs.status');
          $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
          $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
          $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
          $this->db->join('job_applicants', 'jobs.id = job_applicants.job_id','left');
        $this->db->group_by('jobs.id');
        $this->db->where($where);
       
        $query = $this->db->get();
        return $query->result();
        }
        else
        {
          $this->db->select('jobs.id as job_id, jobs.job_number, jobs.approval, jobs.key_skill, jobs.job_title, jobs.status as job_status, company_departments.id as department_id, company_departments.department_name, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location, jobs.start_date, jobs.end_date, jobs.no_of_post, job_types.id as job_type_id, job_types.job_type, job_types.type_color, jobs.job_description, COUNT(job_applicants.id) AS total_candidate,job_applicant_status.status_color,job_applicant_status.job_applicant_status');
          $this->db->from('jobs');
          $this->db->join('users','jobs.added_by = users.id');
            $this->db->join('job_applicant_status', 'job_applicant_status.id = jobs.status');
          $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
          $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
          $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
          $this->db->join('job_applicants', 'jobs.id = job_applicants.job_id','left');
        $this->db->group_by('jobs.id');
        $this->db->where($where);
         $this->db->where('approval',1);
       
        $query = $this->db->get();
        return $query->result();
        }
    }
    
}