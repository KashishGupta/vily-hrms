<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: BranchModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Branchs.
*/

class BranchModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function addBranch($data)
    {
        $this->db->insert('company_branches',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
// check Branch Location If Allready Exist
 function checkBranchLocationAvailability($where_branch)
    {
        $query = $this->db->get_where('company_branches',$where_branch);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }
    // Delete Branch
    public function deleteBranch($where_Branch)
    {
        $this->db->where($where_Branch);
        $this->db->delete('company_branches');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editBranch($data,$where)
    {
        $this->db->where($where);
        $this->db->update('company_branches',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
 
         //is active branch
     public function viewactiveBranch()
    {
        $this->db->select('*');
        $this->db->from('company_branches');
         $this->db->order_by("id", "DESC");
            $this->db->where('is_active' , 1);
        $query = $this->db->get();
        return $query->result();
    }
    // view branch
    public function viewBranch()
    {
        $this->db->select('*');
        $this->db->from('company_branches');
         $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->result();
    }

    public function viewBranchWhere($where)
    {
        $this->db->select('*');
        $this->db->from('company_branches');
         $this->db->order_by("id", "DESC");
             $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function chatsAddUser($data)
    {
        $this->db->insert('conversation_reply',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    // select data convestation user
    public function getUserConversation($where)
    {
       
        $this->db->select('*');
        $this->db->from('conversation'); 
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->row();
       
        
    }
    public function updateUserConversation($data,$where)
    {
   
        $this->db->where($where);
        $this->db->update('conversation',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
 // insert Data 
 public function insertData($data)
 {
    
      //$lastLoggedData = date('d-m-Y h:i:s');
      $where=array('user_one'=>$data['user_one'],'user_two'=>$data['user_two']);
     // $where2=array('user_two'=>$data['user_two'],'user_one'=>$data['user_one']);
      $arrUserConversationData=$this->getUserConversation($where);
     // $arrUserConversationData1=$this->getUserConversation($where2);
     
     
      if(!empty($arrUserConversationData->c_id)){
        $lastLogin= date('d-m-Y h:i:s');
        $updateData['time']=$lastLogin;
        $updateWhere['c_id']=$arrUserConversationData->c_id;
       
        $success = $this->updateUserConversation($updateData,$updateWhere);
        
     
    }
      else{
        $this->db->insert('conversation',$data);
        $insert_id = $this->db->insert_id();
       if($insert_id != 0)
       {
           return TRUE;
       }
       else    
       {
           return FALSE;
       }
     }  
 }
 // view Conversation Single User
 
 public function viewCon()
 {
     $user = $this->api->_GetTokenData();
     $user_id = $user['id'];
    
     $this->db->select('*');
     $this->db->from('conversation');
     $this->db->join('conversation_reply','conversation.c_id = conversation_reply.c_id_fk');
     $this->db->join('users', 'users.id = conversation_reply.user_id_fk');
     $query = $this->db->get();
     return $query->result();
 }

 public function viewconWhere($where2)
 {
     $user = $this->api->_GetTokenData();
     $user_id = $user['id'];
     $this->db->select('*');
     $this->db->from('conversation');
     $this->db->join('conversation_reply','conversation.c_id = conversation_reply.c_id_fk');
     $this->db->join('users', 'users.id = conversation_reply.user_id_fk');
     // $this->db->join('user_bank_details','user_details.user_id = user_bank_details.user_id');
     // $this->db->join('user_social_details','user_social_details.user_id = user_details.user_id');
       
    // $this->db->where($where);
     $this->db->where($where2);
     $query = $this->db->get();
     return $query->result();
 }
    public function viewUser()
    {
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];
        $this->db->select('users.first_name,users.id as user_id');
        $this->db->from('users');
        $this->db->where("users.id NOT IN (".$user_id.")");
        $query = $this->db->get();
        return $query->result();
    }

    public function viewUserWhere($where2)
    {
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];
        $this->db->select('users.first_name,users.id as user_id');
        $this->db->from('users');
        $this->db->where("users.id NOT IN (".$user_id.")");
        $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
}