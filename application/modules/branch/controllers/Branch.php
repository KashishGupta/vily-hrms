<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Branch.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the Branch.
*/

class Branch extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('branchModel');
	}
	// check Branch If Allready Exist In DataBase
 public function _checkBranchAvailability($request)
	{		
			$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the Branch Location. If User has the permission for that. */

			if(isset($request->branch_location))
			{
				$where_branch = array(
					'branch_location' => $request->branch_location
				);

				$result = $this->branchModel->checkBranchLocationAvailability($where_branch);
				if($result)
					return array("message" => $request->branch_location." is Available.");
				else
					$this->api->_sendError("Branch Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Branch.");
		}
    }
	public function _addBranch($request)
    {
		$branch = array();
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Branches.. If User has the permission for that. */

			if(isset($request->branch_name) AND isset($request->branch_location)AND isset($request->branch_lat)AND isset($request->branch_long))
			{
                   $status = $this->_checkBranchAvailability($request);
				   $branch = array(
					'branch_name' => $request->branch_name,
					'branch_location' => $request->branch_location,
					'branch_lat' => $request->branch_lat,
					'branch_long' => $request->branch_long,
					'is_active' => 1
				);

				$result = $this->branchModel->addBranch($branch);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Branch Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Branch Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Branch.");
		}
	}
	
	public function _addBranchBulk($request)
    {
		$branch = array();
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Branches.. If User has the permission for that. */
			if(isset($request->csvFile))
			{
			   $baseimage = $request->csvFile;
				$source_image = str_replace('data:application/vnd.ms-excel;base64,','',$baseimage);
				$decoded = base64_decode($source_image);
				$arrCsvData=explode("\n",$decoded);
				array_pop($arrCsvData);
				array_shift($arrCsvData);
				$full_stats = array();
				foreach($arrCsvData as $line) {
					$full_stats[] = str_getcsv($line, "\t");
					foreach($full_stats as $Csv => $arrvalue)
					{
					 foreach($arrvalue as $arrCsv => $arrvaluebreak)
					{
						if (!empty($arrvaluebreak))
						{
							
						 $explode=explode(',',$arrvaluebreak);	
						 $branch = array(
							'branch_name' => $explode[0],
							'branch_location' => $explode[1],
							'branch_lat' => $explode[2],
							'branch_long' => $explode[3],
							'is_active' => $explode[4]
						);
						}					
					}
					}
					//print_r($explode);
				 $this->db->insert('company_branches',$branch);
					
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Branch.");
		}
    }

    public function _deleteBranch($request)
    {		
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Branchs.. If User has the permission for that. */

			if(isset($request->branch_id))
			{
				$where_branch = array(
					'id' => $request->branch_id,
				);
				$result = $this->branchModel->deleteBranch($where_branch);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Branch Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Branch Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Branch.");
		}
    }

    public function _editBranch($request)
    {
		$branch = array();
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Branches.. If User has the permission for that. */

			if(isset($request->branch_id))
			{
				$branch_where = array(
					'id' => $request->branch_id
				);
         
				if(isset($request->branch_name))
					$branch['branch_name'] = $request->branch_name;
				if(isset($request->branch_location))
					$branch['branch_location'] = $request->branch_location;
					//$status = $this->_checkBranchAvailability($request);
				if(isset($request->branch_lat))
					$branch['branch_lat'] = $request->branch_lat;
				if(isset($request->branch_long))
					$branch['branch_long'] = $request->branch_long;

				$result = $this->branchModel->editBranch($branch,$branch_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Branch Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Branch Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Branch.");
		}
	}
	// Active branch
    public function _viewJobBranch($request)
    {		
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		if($permission->can_view == 'Y')
		{
			/* Here we View the Active Branches.. If User has the permission for that. */

			if(TRUE)
			{
				
					$result = $this->branchModel->viewBranch();
					return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Branch.");
		}
	}
	// Active branch
    public function _viewTokenBranch($request)
    {		
		
			/* Here we View the Active Branches.. If User has the permission for that. */

			if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$userbranch_id = $user['branch_id'];
				$where_branch = array('id'=>$userbranch_id,'is_active'=>1);
					$result = $this->branchModel->viewBranchWhere($where_branch);
					return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
// Active branch
    public function _viewactiveBranch($request)
    {		
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		if($permission->can_view == 'Y')
		{
			/* Here we View the Active Branches.. If User has the permission for that. */

			if(TRUE)
			{
					$result = $this->branchModel->viewactiveBranch();
					return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Branch.");
		}
    }
    // view Branch
    public function _viewBranch($request)
    {		
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$branch_where = array();
		//print_r($request);
		if($permission->can_view == 'Y')
		{
			/* Here we View the Branches.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->branch_id))
				{
					$branch_where['id'] = $request->branch_id;
					$result = $this->branchModel->viewBranchWhere($branch_where);
					return $result;
				}
         else	if($request->status === '4')
				{
				$result = $this->branchModel->viewBranch();
				return $result;
				}
         else	if(isset($request->status))
				{
					$branch_where['is_active'] = $request->status;
					$result = $this->branchModel->viewBranchWhere($branch_where);
					return $result;
				}
				else
				{
					$result = $this->branchModel->viewBranch();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Branch.");
		}
    }
     // view Branch
	 public function _viewTokenExpired($request)
	 {		
		
			/* Here we View the Branches.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->branch_id))
				{
					$branch_where['id'] = $request->branch_id;
					$result = $this->branchModel->viewBranchWhere($branch_where);
					return $result;
				}
                else if($request->status === '4')
				{
				$result = $this->branchModel->viewBranch();
				return $result;
				}
				else
				{
					$result = $this->branchModel->viewBranch();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
    // active or inactive branch
     public function _editBranchActive($request)
    {
		$branch = array();
		$module_name = 'branch';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Active Branches.. If User has the permission for that. */

			if(isset($request->branch_id))
			{
				$branch_where = array(
					'id' => $request->branch_id
				);

				if(isset($request->is_active))
					$branch['is_active'] = $request->is_active;
				$result = $this->branchModel->editBranch($branch,$branch_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Branch Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Branch Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Active Branch.");
		}
	}
	
	// chat conversation api with out permission
	public function _viewconversationUser($request)
    {		
		$users_where = array();
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->user_id)){
					$users_where['user_details.user_id'] = $request->user_id;			
					$result = $this->branchModel->viewUserWhere($users_where);
					return $result;
				}
				else
				{
					$result = $this->branchModel->viewUser();
					return $result;
				}
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	}

	// chat conversation api with single user chats
	public function _viewconversationSingleUser($request)
    {		
		$users_where = array();
			/* Here we View the userss.. If User has the permission for that. */

			
				if(isset($request->user_id)){
					$user = $this->api->_GetTokenData();
					
					  $user_name = $user['id'];
					   $timeData = date('d-m-Y h:i:s');
					$userConversation = 
					array(
					'user_one' => $user_name,
					'time' => $timeData,
					'user_two'=>$request->user_id
					);
					 //check if the Conversation between those 2 users exists, if not create new Conversation Id
					 $num1 = $this->db->get_where('conversation', array('user_one' => $user_name, 'user_two' => $request->user_id))->num_rows();
					 $num2 = $this->db->get_where('conversation', array('user_one' => $request->user_id, 'user_two' => $user_name))->num_rows();
					 if ($num1 == 0 && $num2 == 0) {
						 $test = 0;
						 $data_message['c_id_fk'] = $test;
						 $result = $this->branchModel->viewconWhere($data_message);
				   return $result;
					  }
					 if ($num1 > 0)
					 $cid = $this->db->get_where('conversation', array('user_one' => $user_name, 'user_two' => $request->user_id))->row()->c_id;
					 if ($num2 > 0)
					 $cid = $this->db->get_where('conversation', array('user_one' => $request->user_id, 'user_two' => $user_name))->row()->c_id;
			   
			   
				   $data_message['c_id_fk'] = $cid;
				   $result = $this->branchModel->viewconWhere($data_message);
				   return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	}
 // Add User After Click
 // Add Notifications For Particular user
 public function _userConversationclick($request)
 {
	 if(isset($request->users_id)){
  $user = $this->api->_GetTokenData();
  
	$user_name = $user['id'];
	 $timeData = date('d-m-Y h:i:s');
  $userConversation = 
  array(
  'user_one' => $user_name,
  'created_at' => $timeData,
  'user_two'=>$request->users_id
  );
   //check if the Conversation between those 2 users exists, if not create new Conversation Id
   $num1 = $this->db->get_where('conversation', array('user_one' => $user_name, 'user_two' => $request->users_id))->num_rows();
   $num2 = $this->db->get_where('conversation', array('user_one' => $request->users_id, 'user_two' => $user_name))->num_rows();
   if ($num1 == 0 && $num2 == 0) {
	 $message_thread_code = substr(md5(rand(100000000, 20000000000)), 0, 15);
	 $data_message_thread['message_thread_code'] = $message_thread_code;
	 $data_message_thread['user_one'] = $user_name;
	 $data_message_thread['user_two'] = $request->users_id;
	 $data_message_thread['created_at'] = $timeData;
	 $this->db->insert('conversation', $data_message_thread);
   }
   
  }
  else
  {
  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
 
 
}
	// Add Notifications For Particular user
	public function _userConversation($request)
	{
		
		if(isset($request->user_id) AND isset($request->message)){
	 $user = $this->api->_GetTokenData();
	 
	   $user_name = $user['id'];
	    $timeData = date('d-m-Y h:i:s');
	 $userConversation = 
	 array(
	 'user_one' => $user_name,
	 'created_at' => $timeData,
	 'user_two'=>$request->user_id
	 );
      //check if the Conversation between those 2 users exists, if not create new Conversation Id
	  $num1 = $this->db->get_where('conversation', array('user_one' => $user_name, 'user_two' => $request->user_id))->num_rows();
	  $num2 = $this->db->get_where('conversation', array('user_one' => $request->user_id, 'user_two' => $user_name))->num_rows();
	  if ($num1 == 0 && $num2 == 0) {
		$message_thread_code = substr(md5(rand(100000000, 20000000000)), 0, 15);
		$data_message_thread['message_thread_code'] = $message_thread_code;
		$data_message_thread['user_one'] = $user_name;
		$data_message_thread['user_two'] = $request->user_id;
		$data_message_thread['created_at'] = $timeData;
		$this->db->insert('conversation', $data_message_thread);
	  }
	  if ($num1 > 0)
	  $cid = $this->db->get_where('conversation', array('user_one' => $user_name, 'user_two' => $request->user_id))->row()->c_id;
	  if ($num2 > 0)
	  $cid = $this->db->get_where('conversation', array('user_one' => $request->user_id, 'user_two' => $user_name))->row()->c_id;
	  $arrtimeData = date('Y-m-d h:i:s');

	$data_message['c_id_fk'] = $cid;
	//$data_message['time'] = $arrtimeData;
	$data_message['user_id_fk'] = $request->user_id;
	$data_message['message'] = $request->message;	
	$result = $this->branchModel->chatsAddUser($data_message);
	if($result)
		{
		 return array(
				'status' => '1',
				'message' => ' Message Send Successfully.'
				);
		}
		else
			{
				$this->api->_sendError("Sorry, Conversation Not Added. Please Try Again!!");
			}
	 }
	 else
	 {
	 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	 }
	
	}
}
