<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: LeaveSettingModel.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Models of the Leave Types.
*/

class LeaveSettingModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
// check leave type avialability
   public function checkLeaveTYpeNameAvailability($where_type)
   {
    $query = $this->db->get_where('company_leave_types',$where_type);
  
     // If User Exists
       if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
   }

   
   public function addLeavePolicy($data)
   {
       $this->db->insert('leave_policy',$data);
       $insert_id = $this->db->insert_id();
       if($insert_id != 0)
       {
           return TRUE;
       }
       else    
       {
           return FALSE;
       }
   }
    public function addLeaveType($data)
    {
        $this->db->insert('company_leave_types',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteLeaveType($where_leave_type)
    {
        $this->db->where($where_leave_type);
        $this->db->delete('company_leave_types');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editLeaveType($data,$where)
    {
        $this->db->where($where);
        $this->db->update('company_leave_types',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewLeaveType()
    {
        $this->db->select('*');
        $this->db->from('company_leave_types');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewLeaveTypeWhere($where)
    {
        $query = $this->db->get_where('company_leave_types', $where);
        return $query->result();
    }

    public function viewLeavePolicy()
    {
        $this->db->select('leave_policy.id,leave_policy.leave_type_id,company_leave_types.leave_type,leave_policy.check_leave_type,leave_policy.maximum_leaves,leave_policy.policy_name,leave_policy.waiting_period,leave_policy.total_per_year,leave_policy.description,leave_policy.apply_before_days,leave_policy.accural_method,leave_policy.is_accrual_fixed,leave_policy.is_accrual_reset,leave_policy.is_accrual_beginning,leave_policy.max_carry_days,leave_policy.is_encashable,leave_policy.waiting_period_length,leave_policy.is_holiday_excluded,leave_policy.is_weekend_excluded,leave_policy.limit_inclusion,leave_policy.max_consecutive,leave_policy.max_balance_days,leave_policy.is_doj_considered,leave_policy.negative_balance_allowed,leave_policy.created_at');
        $this->db->from('leave_policy');
        $this->db->join('company_leave_types', 'company_leave_types.id = leave_policy.leave_type_id');
        $query = $this->db->get();
        return $query->result();
    }
    //viewaccrual_period
    public function viewaccrual_period()
    {
        $this->db->select('*');
        $this->db->from('waiting_period_policy');
        $query = $this->db->get();
        return $query->result();
    }
    public function viewLeavePolicyWhere($where)
    {
        $this->db->select('leave_policy.id,leave_policy.leave_type_id,company_leave_types.leave_type,leave_policy.maximum_leaves,leave_policy.check_leave_type,leave_policy.policy_name,leave_policy.waiting_period,leave_policy.total_per_year,leave_policy.description,leave_policy.apply_before_days,leave_policy.accural_method,leave_policy.is_accrual_fixed,leave_policy.is_accrual_reset,leave_policy.is_accrual_beginning,leave_policy.max_carry_days,leave_policy.is_encashable,leave_policy.waiting_period_length,leave_policy.is_holiday_excluded,leave_policy.is_weekend_excluded,leave_policy.limit_inclusion,leave_policy.max_consecutive,leave_policy.max_balance_days,leave_policy.is_doj_considered,leave_policy.negative_balance_allowed,leave_policy.created_at');
        $this->db->from('leave_policy');
        $this->db->join('company_leave_types', 'company_leave_types.id = leave_policy.leave_type_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function deleteLeavePolicy($where_leave_policy)
    {
        $this->db->where($where_leave_policy);
        $this->db->delete('leave_policy');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function editLeavePolicy($data,$where)
    {
        $this->db->where($where);
        $this->db->update('leave_policy',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    
}