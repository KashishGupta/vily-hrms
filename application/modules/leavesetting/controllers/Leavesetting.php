<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: LeaveSetting.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Functions of the users.
*/

class Leavesetting extends MY_Controller{

    public function __construct(){
		parent::__construct();
		$this->load->model('leaveSettingModel');
	}
	
	public function _addLeaveType($request)
    {
		
		$leaveType = array();
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Leave Settings.. If User has the permission for that. */

			if(isset($request->leave_type)  AND isset($request->description))
			{
            $status2 = $this->_checkLeaveType_nameAvailability($request);
				$leaveType = array(
					'leave_type' => $request->leave_type,
                    'description' => $request->description
				);
				if(isset($request->status))
					$leaveType['status'] = $request->status;
                  
				$result = $this->leaveSettingModel->addLeaveType($leaveType);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Type Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Type Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Leave Type.");
		}
    }
// check if allready add Leave types
public function _checkLeaveType_nameAvailability($request)
	{		
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the email. If User has the permission for that. */

			if(isset($request->leave_type))
			{
				$where_type = array(
					'leave_type' => $request->leave_type
				);

				$result = $this->leaveSettingModel->checkLeaveTYpeNameAvailability($where_type);
				if($result)
					return array("message" => $request->leave_type." is Available.");
				else
					$this->api->_sendError("Leave Type Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Add Leave Type.");
		}
    }
    // delete leave type
    public function _deleteLeaveType($request)
    {		
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the leave Settings.. If User has the permission for that. */

			if(isset($request->leave_type_id))
			{
				$where_leave_type = array(
					'id' => $request->leave_type_id,
				);
				$result = $this->leaveSettingModel->deleteLeaveType($where_leave_type);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Type Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Type Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Leave Type.");
		}
    }

    public function _editLeaveType($request)
    {
		$leaveType = array();
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Leave Type.. If User has the permission for that. */

			if(isset($request->leave_type_id))
			{
     
			
				$where_leave_type = array(
					'id' => $request->leave_type_id
				);
	
				if(isset($request->leave_type))
					$leaveType['leave_type'] = $request->leave_type;
				
					
        if(isset($request->description))
					$leaveType['description'] = $request->description;

				$result = $this->leaveSettingModel->editLeaveType($leaveType,$where_leave_type);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Type Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Type Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Leave Type.");
		}
    }

    public function _viewLeaveType($request)
    {		
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_leave_type = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leave Type.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->leave_type_id))
				{
					$where_leave_type['id'] = $request->leave_type_id;
					$result = $this->leaveSettingModel->viewLeaveTypeWhere($where_leave_type);
					return $result;
				}
				else
				{
					$result = $this->leaveSettingModel->viewLeaveType();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Leave Type.");
		}
	}
	
	// view Leave Type For Seelcted Index For Users
	public function _viewLeaveTypeSelected($request)
    {		
		
		$where_leave_type = array();
		
	
			/* Here we View the Leave Type.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->leave_type_id))
				{
					$where_leave_type['id'] = $request->leave_type_id;
					$result = $this->leaveSettingModel->viewLeaveTypeWhere($where_leave_type);
					return $result;
				}
				else
				{
					$result = $this->leaveSettingModel->viewLeaveType();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	}
	

// Add Leave Policy
	public function _addLeavePolicy($request)
    {

		$leaveType = array();
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Leave Settings.. If User has the permission for that. */

			if(isset($request->leave_type_id) AND isset($request->policy_name) AND isset($request->total_per_year) AND isset($request->description) AND isset($request->apply_before_days) AND isset($request->accural_method) 
			AND isset($request->is_accrual_fixed) AND isset($request->is_accrual_reset) AND isset($request->is_accrual_beginning) AND isset($request->max_carry_days) AND isset($request->is_encashable) AND isset($request->waiting_period_length) AND isset($request->is_weekend_excluded) AND isset($request->limit_inclusion)
			AND isset($request->max_balance_days) AND isset($request->is_holiday_excluded) AND isset($request->is_doj_considered) AND isset($request->negative_balance_allowed))
			{
				$createDate = date("Y-m-d h:i:s");
				$datearr = date("Y-m-d");
				$leavePolicy = array(
					'leave_type_id' => $request->leave_type_id,
                     'policy_name' => $request->policy_name,
					'total_per_year' => $request->total_per_year,
					'description' => $request->description,
                     'apply_before_days' => $request->apply_before_days,
					'accural_method' => $request->accural_method,
					//'accrual_period'=> $request->accrual_period,
                     'is_accrual_fixed' => $request->is_accrual_fixed,
					'is_accrual_reset' => $request->is_accrual_reset,
					'apply_before_date' => $datearr,
					'is_accrual_beginning' => $request->is_accrual_beginning,
                     'max_carry_days' => $request->max_carry_days,
					'is_encashable' => $request->is_encashable,
					'is_holiday_excluded' => $request->is_holiday_excluded,
					'waiting_period_length' => $request->waiting_period_length,
                     'is_weekend_excluded' => $request->is_weekend_excluded,
					'limit_inclusion' => $request->limit_inclusion,
					'is_accrual_beginning' => $request->is_accrual_beginning,
                     'max_carry_days' => $request->max_carry_days,
					'is_encashable' => $request->is_encashable,
					'max_balance_days' => $request->max_balance_days,
					 'is_doj_considered' => $request->is_doj_considered,
					 'created_at' => $createDate,
					'negative_balance_allowed' => $request->negative_balance_allowed
				); 
				if(isset($request->back_date))
				$leavePolicy['back_date'] = $request->back_date;
				if(isset($request->check_leave_type))
				$leavePolicy['check_leave_type'] = $request->check_leave_type;
				if(isset($request->max_consecutive))
					$leavePolicy['max_consecutive'] = $request->max_consecutive;

				$result = $this->leaveSettingModel->addLeavePolicy($leavePolicy);
				
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Policy Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Policy Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Leave Policy.");
		}
    }
// view leave Policies 
	public function _viewLeavePloicies($request)
    {		
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_leave_type = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leave Type.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->leave_policy_id))
				{
					$where_leave_policy['leave_policy.id'] = $request->leave_policy_id;
					$result = $this->leaveSettingModel->viewLeavePolicyWhere($where_leave_policy);
					return $result;
				}
				else
				{
					$result = $this->leaveSettingModel->viewLeavePolicy();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Leave Policy.");
		}
	}

	// view leave Policies 
	public function _viewLgggedUserPloicies($request)
    {		
		
			/* Here we View the Leave Type.. If User has the permission for that. */

			if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$test = $user['leave_policy_id'];
				$where = 'leave_policy.id IN ('.$test.') ';
				$result = $this->leaveSettingModel->viewLeavePolicyWhere($where);
			    return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	}

	
	// Edit Leave Policy
	public function _editLeavePolicy($request)
    {
		$leaveType = array();
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Leave Policy.. If User has the permission for that. */
			if(isset($request->leave_policy_id))
			{
				$where_leave_policy = array(
					'id' => $request->leave_policy_id
				);
				if(isset($request->leave_type_id))
					$leavePolicy['leave_type_id'] = $request->leave_type_id;
                if(isset($request->policy_name))
					$leavePolicy['policy_name'] = $request->policy_name;
				if(isset($request->total_per_year))
					$leavePolicy['total_per_year'] = $request->total_per_year;
                if(isset($request->description))
					$leavePolicy['description'] = $request->description;
				if(isset($request->apply_before_days))
					$leavePolicy['apply_before_days'] = $request->apply_before_days;
                if(isset($request->accural_method))
					$leavePolicy['accural_method'] = $request->accural_method;
                if(isset($request->is_accrual_fixed))
					$leavePolicy['is_accrual_fixed'] = $request->is_accrual_fixed;
				if(isset($request->is_accrual_reset))
					$leavePolicy['is_accrual_reset'] = $request->is_accrual_reset;
                if(isset($request->is_accrual_beginning))
					$leavePolicy['is_accrual_beginning'] = $request->is_accrual_beginning;
				//if(isset($request->waiting_period))
					//$leavePolicy['waiting_period'] = $request->waiting_period;
				if(isset($request->max_carry_days))
					$leavePolicy['max_carry_days'] = $request->max_carry_days;
                if(isset($request->is_encashable))
					$leavePolicy['is_encashable'] = $request->is_encashable;
				if(isset($request->waiting_period_length))
					$leavePolicy['waiting_period_length'] = $request->waiting_period_length;
                if(isset($request->is_holiday_excluded))
					$leavePolicy['is_holiday_excluded'] = $request->is_holiday_excluded;
				if(isset($request->is_weekend_excluded))
					$leavePolicy['is_weekend_excluded'] = $request->is_weekend_excluded;
                if(isset($request->limit_inclusion))
					$leavePolicy['limit_inclusion'] = $request->limit_inclusion;
				if(isset($request->max_consecutive))
					$leavePolicy['max_consecutive'] = $request->max_consecutive;
				if(isset($request->max_balance_days))
					$leavePolicy['max_balance_days'] = $request->max_balance_days;
				if(isset($request->is_doj_considered))
					$leavePolicy['is_doj_considered'] = $request->is_doj_considered;
				if(isset($request->negative_balance_allowed))
					$leavePolicy['negative_balance_allowed'] = $request->negative_balance_allowed;
					if(isset($request->back_date))
					$leavePolicy['back_date'] = $request->back_date;
					if(isset($request->check_leave_type))
					$leavePolicy['check_leave_type'] = $request->check_leave_type;
					
					
				$result = $this->leaveSettingModel->editLeavePolicy($leavePolicy,$where_leave_policy);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Policy Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Policy Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Leave Policy.");
		}
    }
	// delete leave Policy
    public function _deleteLeavePolicy($request)
    {		
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the leave Settings.. If User has the permission for that. */

			if(isset($request->leave_policy_id))
			{
				$where_leave_policy = array(
					'id' => $request->leave_policy_id,
				);
				$result = $this->leaveSettingModel->deleteLeavePolicy($where_leave_policy);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Policy Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Policy Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Leave Policy.");
		}
	}
	
	//viewaccrual_period
	public function _viewaccrual_period($request)
    {		
		$module_name = 'leave_setting';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_leave_type = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leave Type.. If User has the permission for that. */

			if(TRUE)
			{
				
					$result = $this->leaveSettingModel->viewaccrual_period();
					return $result;
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Leave Type.");
		}
	}

}
