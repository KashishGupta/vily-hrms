<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: Attendancemanagement.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Functions of the Attendance Management.
*/

class Attendancemanagement extends MY_Controller{

    public function __construct(){
		parent::__construct();
		$this->load->model('attendanceManagementModel');
	}

    public function _viewAllAttendance($request)
    {		
		$module_name = 'attendance_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_attendance = array();
		$where3 = "";
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leave Type.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->user_id))
					$where_attendance['user_details.user_id'] = $request->user_id;
				if(isset($request->attendance_date))
					$where_attendance['attendances.date'] = $request->attendance_date;
				if((isset($request->from_date)) AND (isset($request->to_date)))
					$where3 = "attendances.date BETWEEN ".$request->from_date." AND ".$request->to_date."";
				
				$where = 'user_details.user_id IN ('.implode(',',$this->users->_getUserId()).')';

				if(!empty($where_attendance))
				{
					$result = $this->attendanceManagementModel->viewAttendanceWhere($where_attendance,$where,$where3);
					return $result;
				}
				else
				{
					$result = $this->attendanceManagementModel->viewAttendance($where,$where3);
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Attendance.");
		}
	}
}
