<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: AttendanceManagementModel.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Models of the Attendance Management.
*/

class AttendanceManagementModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function viewAttendanceWhere($where,$where2,$where3)
    {
        $this->db->select('attendances.id as attendance_id, attendances.attendance_number, attendances.date, attendances.in_time, attendances.out_time, attendances.final_status, user_details.first_name, user_details.last_name, user_details.emp_id, user_details.email');
        $this->db->from('attendances');
        $this->db->join('user_details', 'user_details.user_id = attendances.user_id');
        $this->db->where($where);
        $this->db->where($where2);
        if($where3 != "")
        {
            $this->db->where($where3);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function viewAttendance($where2,$where3)
    {
        $this->db->select('attendances.id as attendance_id, attendances.attendance_number, attendances.date, attendances.in_time, attendances.out_time, attendances.final_status, user_details.first_name, user_details.last_name, user_details.emp_id, user_details.email');
        $this->db->from('attendances');
        $this->db->join('user_details', 'user_details.user_id = attendances.user_id');
        $this->db->where($where2);
        if($where3 != "")
        {
            $this->db->where($where3);
        }
        $query = $this->db->get();
        return $query->result();
    }
}