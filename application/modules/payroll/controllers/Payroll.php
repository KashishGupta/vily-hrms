<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Payroll.php
	Date: August 12, 2020
	Info: This is the main class which is hold all the Functions of the users.
*/

class Payroll extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('payrollModel');
		$this->load->model('userModel');
	}
	
	// check Payslip If Already Exist In DataBase month And Year
	public function _checkPayrollAvailability($request)
	{	
		
			$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the Payroll Month. If User has the permission for that. */

			if(isset($request->month) AND isset($request->year))
			{
				$where_payroll = array(
					'month' => $request->month,
					'year' => $request->year
				);

				$result = $this->payrollModel->checkPayrollAvailability($where_payroll);
				if($result)
					return array(
							'status' => '1',	
						     "message" =>"Payroll is Available."
					       );
				else
					$this->api->_sendError("Payroll Already Present In This Month.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Payroll.");
		}
	}
	// check Payslip If Not Available Exist In DataBase month And Year
	public function _checkPayrollNotAvailability($request)
	{		
			$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the Payroll Month. If User has the permission for that. */

			if(isset($request->user_id) AND isset($request->month) AND isset($request->year))
			{
				$where_payroll = array(
					'user_id' => $request->user_id,
					'month' => $request->month,
					'year' => $request->year
				);

				$result = $this->payrollModel->checkPayrollNotAvailability($where_payroll);
				if($result == TRUE){
					return array(
							'status' => '1',	
						     "message" =>"Payroll is Available."
						   );
						}
				else{
					$this->api->_sendError("Payroll Is Not Present In This Month And Year.");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Payroll.");
		}
    }

	// View Payroll Pending
public function _viewpayrollCreateSearch($request)
{		
	$module_name = 'payroll';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}	
	if($permission->can_view == 'Y')
	{			
		/* Here we Add the userss.. If User has the permission for that. */
		if(TRUE)
		{
			if(isset($request->user_id))
					$users_where['payroll_status.user_id'] = $request->user_id;

					$user_ids = $this->users->_getUserId();
				if(!empty($user_ids))
				$where = 'payroll_status.user_id IN ('.implode(',',$user_ids).') AND payroll_status.status = 1';
				else
				$where = 'payroll_status.user_id = 0 AND payroll_status.status = 1';

				if(!empty($users_where))
				{
					$result = $this->payrollModel->viewPayrollWhereCtcComplete($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->payrollModel->viewPayrollCtcComplete($where);
					return $result;
				}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Edit Payroll.");
	}
}
// A User Payroll Can view 
      public function _viewPayroll($request)
    {		
		$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$payroll_where = array();
		
		
		if($permission->can_view == 'Y')
		{	
			/* Here we View the  Payroll... If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->payroll_id))
				$payroll_where['user_payroll.id'] = $request->payroll_id;
				if(isset($request->user_id) AND isset($request->month) AND isset($request->year))
				$payroll_where['users.id'] = $request->user_id;
				$payroll_where['user_payroll.month'] = $request->month;
				$payroll_where['user_payroll.year'] = $request->year;
			  $user_ids = $this->users->_getUserId();
			  if(!empty($user_ids))
				$where = 'user_payroll.user_id IN ('.implode(',',$user_ids).')';
			  else
				$where = 'user_payroll.user_id  = 0';
			
			if(!empty($payroll_where))
			  {
				$result = $this->payrollModel->viewPayrollWhere($payroll_where,$where);
				return $result;
			  }
			  else
			  {
				$result = $this->payrollModel->viewPayroll($where);
				return $result;
			  }
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Payroll.");
		}
    }

	// A User Payroll Can view 
	public function _viewPayrollother($request)
    {		
		
			/* Here we View the  Payroll... If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->payroll_id))
				$where['user_payroll.id'] = $request->payroll_id;
				if(isset($request->user_id) AND isset($request->month) AND isset($request->year))
				$where['users.id'] = $request->user_id;
				$where['user_payroll.month'] = $request->month;
				$where['user_payroll.year'] = $request->year;
			 
			if(!empty($where))
			  {
				$result = $this->payrollModel->viewPayroll($where);
				return $result;
			  }
			  else
			  {
				$result = $this->payrollModel->viewPayrolltest();
				return $result;
			  }
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
// View Payroll Pending
	public function _viewpayrollPending($request)
	{		
		$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}	
		if($permission->can_view == 'Y')
		{			
			/* Here we Add the userss.. If User has the permission for that. */
			if(TRUE)
			{
				
				if(!empty($request->user_id == 4))
				{
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
					$where = 'payroll_status.user_id IN ('.implode(',',$user_ids).') AND payroll_status.status = 0';
					else
					$where = 'payroll_status.user_id = 0 AND payroll_status.status = 0';
					$result = $this->payrollModel->viewPayrollCtc($where);
					return $result;
				}else{
					if(isset($request->user_id))
					$users_where['payroll_status.user_id'] = $request->user_id;
					$user_ids = $this->users->_getUserId();
				if(!empty($user_ids))
				$where = 'payroll_status.user_id IN ('.implode(',',$user_ids).') AND payroll_status.status = 0';
				else
				$where = 'payroll_status.user_id = 0 AND payroll_status.status = 0';

				if(!empty($users_where))
				{
					$result = $this->payrollModel->viewPayrollWhereCtc($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->payrollModel->viewPayrollCtc($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Payroll.");
		}
	}

	// View Payroll Pending
	public function _viewpayrolleditcase($request)
	{		
		$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}	
		if($permission->can_view == 'Y')
		{			
			/* Here we Add the userss.. If User has the permission for that. */
			
			
			if(isset($request->user_id))
			{
				$where = array('users_payslip.user_id' =>$request->user_id);
				$result = $this->payrollModel->viewPayrollCtcedit($where);
				return $result;
			}
			
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Payroll.");
		}
	}

	// View Payroll Complete
	public function _viewpayrollComplete($request)
	{		
		$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}	
		if($permission->can_view == 'Y')
		{			
			/* Here we Add the userss.. If User has the permission for that. */
			if(TRUE)
			{
				
				if(!empty($request->user_id == 4))
				{
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
					$where = 'payroll_status.user_id IN ('.implode(',',$user_ids).') AND payroll_status.status = 1';
					else
					$where = 'payroll_status.user_id = 0 AND payroll_status.status = 1';
					$result = $this->payrollModel->viewPayrollCtc($where);
					return $result;
				}else{
				
				if(isset($request->user_id))
					$users_where['payroll_status.user_id'] = $request->user_id;

					$user_ids = $this->users->_getUserId();
				if(!empty($user_ids))
				$where = 'payroll_status.user_id IN ('.implode(',',$user_ids).') AND payroll_status.status = 1';
				else
				$where = 'payroll_status.user_id = 0 AND payroll_status.status = 1';

				if(!empty($users_where))
				{
					$result = $this->payrollModel->viewPayrollWhereCtcComplete($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->payrollModel->viewPayrollCtcComplete($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Payroll.");
		}
	}

	// Edit ctc
	public function _editPayrollCtc($request)
    {
		$branch = array();
		$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Active Branches.. If User has the permission for that. */

			if(isset($request->user_id))
			{
				
				$payslip_where = array('user_id' => $request->user_id);

			if(isset($request->ctc))
				$payrollDatactc['ctc'] = $request->ctc;
			if(isset($request->basic_monthaly))
				$payrollDatactc['basic_monthaly'] = $request->basic_monthaly;
			if(isset($request->percent_basic))
				$payrollDatactc['percent_basic'] = $request->percent_basic;

			if(isset($request->basic_yearly))
				$payrollDatactc['basic_yearly'] = $request->basic_yearly;
			if(isset($request->hra_monthaly))
				$payrollDatactc['hra_monthaly'] = $request->hra_monthaly;
			if(isset($request->percent_hra	))
				$payrollDatactc['percent_hra'] = $request->percent_hra;

			if(isset($request->hra_yearly))
				$payrollDatactc['hra_yearly'] = $request->hra_yearly;	

			if(isset($request->convance_percent))
				$payrollDatactc['convance_percent'] = $request->convance_percent;
			if(isset($request->yearly_conveyance))
				$payrollDatactc['yearly_conveyance'] = $request->yearly_conveyance;
			if(isset($request->total_amount_monthly))
				$payrollDatactc['total_amount_monthly'] = $request->total_amount_monthly;

			if(isset($request->total_amount_components))
				$payrollDatactc['total_amount_components'] = $request->total_amount_components;
			if(isset($request->total_components_yearly))
				$payrollDatactc['total_components_yearly'] = $request->total_components_yearly;
			if(isset($request->total_amount_percent	))
				$payrollDatactc['total_amount_percent	'] = $request->total_amount_percent;
			if(isset($request->total_amount_yearly))
				$payrollDatactc['total_amount_yearly'] = $request->total_amount_yearly;
				$result = $this->payrollModel->editctcuseersemp($payrollDatactc,$payslip_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Payroll Ctc  Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Payroll Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit  Payroll.");
		}
	}
	// Add payroll Ctc
public function _addPayrollCtc($request)
{

	$payrollData = array();
	$module_name = 'payroll';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	
	if($permission->can_add == 'Y')
	{
		/* Here we Add the payroll. If User has the permission for that. */

		if(isset($request->user_id) AND isset($request->ctc) AND isset($request->basic_monthly) AND isset($request->basic_percent) AND isset($request->basic_yearly)
		AND isset($request->hra_monthly) AND isset($request->hra_percent) AND isset($request->hra_yearly))
		{ 
			
			
			

			// earning data
			$payrollDatactc = array(
				'user_id' => $request->user_id,
				'ctc' => $request->ctc,
				'basic_percent' => $request->basic_percent,
				'basic_monthly' => $request->basic_monthly,
				'basic_yearly' => $request->basic_yearly,
				'hra_monthly' => $request->hra_monthly,
				'hra_percent' => $request->hra_percent,
				'hra_yearly' => $request->hra_yearly
			);
			
	
			$payslip_where = array('user_id' => $request->user_id);
			if(isset($request->allowance_type))
			$allowances             = array();
			$allowance_types        = $request->allowance_type;
			$percents        = $request->percent;
			$allowance_amounts      = $request->allowance_amount;
			$number_of_entries      = sizeof($allowance_types);
	
			for($i = 0; $i < $number_of_entries; $i++)
			{
				if($allowance_types[$i] != "" && $percents[$i] != "" && $allowance_amounts[$i] != "")
				{
					$new_entry = array('type' => $allowance_types[$i], 'percent' => $percents[$i] , 'allowance_amount' => $allowance_amounts[$i]);
					array_push($allowances, $new_entry);
				}
			}
			
			$payrollDatactc['allowance_type']     = json_encode($allowances);

			// deduction 
            if(isset($request->deduction_type))
			$deduction             = array();
			$deduction_types        = $request->deduction_type;
			$deduction_percent        = $request->deduction_percent;
			$deduction_amount      = $request->deduction_amount;
			$number_of_entries1      = sizeof($deduction_types);
	
			for($i = 0; $i < $number_of_entries1; $i++)
			{
				if($deduction_types[$i] != "" && $deduction_percent[$i] != "" && $deduction_amount[$i] != "")
				{
					$new_entry1 = array('type' => $deduction_types[$i], 'deduction_percent' => $deduction_percent[$i] , 'deduction_amount' => $deduction_amount[$i]);
					array_push($deduction, $new_entry1);
				}
			}
			
			$payrollDatactc['deduction_type']     = json_encode($deduction);

			if(isset($request->conveyance_monthly))
				$payrollDatactc['conveyance_monthly'] = $request->conveyance_monthly;
			if(isset($request->yearly_conveyance))
				$payrollDatactc['yearly_conveyance'] = $request->yearly_conveyance;
			
            // total earning data
			if(isset($request->total_earning_monthly))
				$payrollDatactc['total_earning_monthly'] = $request->total_earning_monthly;
			if(isset($request->total_earning_yearly))
				$payrollDatactc['total_earning_yearly'] = $request->total_earning_yearly;
				
           // deduction data 

		   if(isset($request->pf))
				$payrollDatactc['pf'] = $request->pf;
			if(isset($request->pf_monthly))
				$payrollDatactc['pf_monthly'] = $request->pf_monthly;
			if(isset($request->pf_yearly))
				$payrollDatactc['pf_yearly'] = $request->pf_yearly;
			if(isset($request->esi))
				$payrollDatactc['esi'] = $request->esi;
			if(isset($request->esi_monthly))
				$payrollDatactc['esi_monthly'] = $request->esi_monthly;
			if(isset($request->esi_yearly))
				$payrollDatactc['esi_yearly'] = $request->esi_yearly;
			
			if(isset($request->medical_monthly))
				$payrollDatactc['medical_monthly'] = $request->medical_monthly;
			if(isset($request->medical_yearly))
				$payrollDatactc['medical_yearly'] = $request->medical_yearly;

       // Total deduction data
			if(isset($request->total_deduction_monthly))
				$payrollDatactc['total_deduction_monthly'] = $request->total_deduction_monthly;
			if(isset($request->total_deduction_yearly))
				$payrollDatactc['total_deduction_yearly'] = $request->total_deduction_yearly;



       // Total Amount of all over data
			if(isset($request->total_amount_monthly))
				$payrollDatactc['total_amount_monthly'] = $request->total_amount_monthly;
			if(isset($request->total_amount_yearly))
				$payrollDatactc['total_amount_yearly'] = $request->total_amount_yearly;
				$payslip_data['status'] = 1;
				$result = $this->payrollModel->editpayrollStatus($payslip_data,$payslip_where);
			$result = $this->payrollModel->addPayrollctc($payrollDatactc);
			if($result)
			{
				$result1 = $this->payrollModel->editpayrollStatus($payslip_data,$payslip_where);
				return array(
					'status' => '1',
					'message' => 'Payroll Ctc Added Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, Payroll Ctc Not Added. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Add Payroll.");
	}
}

// Here we Add the Payroll.
public function _addPayroll($request)
    {
	
					foreach($request->lopdays as $key=>$value )
					{
						$val=$request->user_id[$key];
						$val1=$request->bonus[$key];
						$val2=$request->commission[$key];
						$val3=$request->reimbursements[$key];
						$val4=$request->lta[$key];
						$val5=$request->adjestments[$key];
						$val6=$request->net_salary[$key];
						$val7=$request->month[$key];
						$val8=$request->year[$key];
						$val9=$request->pf[$key];
						$result[$key]=array($value,$val,$val1,$val2,$val3,$val4,$val5,$val6,$val7,$val8,$val9);
						
					
					}
							foreach($result as $results)
							{
									/*$user_pay = array('month' => $results[8],'year' =>$results[9]);
									$bankdata1 = $this->payrollModel->getdatabankDetails($user_pay);
									if($bankdata1 == TRUE)
									{
									$this->api->_sendError("Payroll Already Present In This Month.");

									}*/
								$user = $this->api->_GetTokenData();
										$userbranch_id = $user['first_name'];
										$codepayroll =  $this->api->_getRandomString(6);
										
								$branch = array(
									'lop_days' => $results[0],
									'user_id' => $results[1],
									'bonus_yearly' => $results[2],
									'commission' => $results[3],
									'reimbursement' => $results[4],
									'lta' => $results[5],
									'adujestment' => $results[6],
									'basic_salary' => $results[7],
									'month'=>$results[8],
									'year'=>$results[9],
									'pf'=>$results[10],
									'created_by' => $userbranch_id,
									'payroll_code'=>$codepayroll
								);
							//	print_r($branch);
							

			
					
				$this->db->insert('user_payroll',$branch);
								
			
							
								
					}
	 
	
		
    }

//  Delete The Payroll
    public function _deletePayroll($request)
    {
		$module_name = 'payroll';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Payroll. If User has the permission for that. */

			if(isset($request->payroll_id))
			{
				$where_payroll = array(
					'user_payroll.id' => $request->payroll_id,
				);
				$result = $this->payrollModel->deletePayroll($where_payroll);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Payroll Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Payroll Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Payroll.");
		}
    }

}

















