<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: BranchModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Branchs.
*/

class PayrollModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
// check Payroll Year Month And User_id If Already Exist
// Get Data where  
public function getdatabankDetails($user_pay) {
    // Check The Credentials
    $query = $this->db->get_where('user_payroll' , $user_pay);
    // If User Exists
    if ($query->num_rows() > 0) {

        $row = $query->row();
        
        $this->db->select('*');
        $this->db->from('user_payroll');
        $this->db->where('user_payroll.id', $row->id);
        $user = $this->db->get()->row();

        return $user;
    }
    else{
        return FALSE;
    }
}
function checkPayrollAvailability($where_payroll)
{
    $query = $this->db->get_where('user_payroll',$where_payroll);
    if ($query->num_rows() > 0)
        return FALSE;
    else
        return TRUE;
}
// check Payroll Year Month And User_id If Already Exist
function checkPayrollNotAvailability($where_payroll)
{
      // Check The Credentials
    $query = $this->db->get_where('user_payroll' , $where_payroll);
    // If User Exists
    if ($query->num_rows() > 0) {


        return TRUE;
    }
    else{
        return FALSE;
    }

}


public function editctcuseersemp($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users_payslip',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

public function editpayrollStatus($data,$where)
    {
        $this->db->where($where);
        $this->db->update('payroll_status',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function viewPayrollCtcedit($where)
{ 
    $this->db->select('*');
    $this->db->from('users_payslip');
    $this->db->where($where);
    $query = $this->db->get();
    return $query->result();
}

public function viewPayrollWhereCtcComplete($where,$where2)
{
   $this->db->select('users.id,users.first_name,users.emp_id,users.is_active,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email,users_payslip.hra_monthly,users_payslip.pf_monthly,users_payslip.basic_monthly,users_payslip.conveyance_monthly,users_payslip.total_earning_monthly,users_payslip.pf_monthly,users_payslip.esi_monthly,users_payslip.medical_monthly,
        users_payslip.total_deduction_monthly,users_payslip.total_amount_monthly,users_payslip.allowance_type');
    $this->db->from('payroll_status');
    $this->db->join('users_payslip','users_payslip.user_id = payroll_status.user_id');
    $this->db->join('users','users.id = payroll_status.user_id');
    $this->db->join('user_details','user_details.user_id = users.id');
    $this->db->join('company_designations','company_designations.id = users.designation_id');
    $this->db->join('company_departments','company_departments.id = users.department_id');
    $this->db->join('company_branches','company_branches.id = users.branch_id');
  
    $this->db->where($where);
    $this->db->where($where2);
     $query = $this->db->get();
     return $query->result();
}
   
public function viewPayrollCtcComplete($where)
{ 
    $this->db->select('users.id,users.first_name,users.emp_id,users.is_active,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email,users_payslip.total_amount_monthly,users_payslip.pf_monthly,users_payslip.hra_monthly,users_payslip.basic_monthly,users_payslip.conveyance_monthly,users_payslip.basic_percent,users_payslip.hra_percent');
    $this->db->from('payroll_status');
    $this->db->join('users_payslip','users_payslip.user_id = payroll_status.user_id');
    $this->db->join('users','users.id = payroll_status.user_id');
    $this->db->join('user_details','user_details.user_id = users.id');
    $this->db->join('company_designations','company_designations.id = users.designation_id');
    $this->db->join('company_departments','company_departments.id = users.department_id');
    $this->db->join('company_branches','company_branches.id = users.branch_id');
  
   
    $this->db->where($where);
    $query = $this->db->get();
    return $query->result();
}

public function viewPayrollCtc($where)
{ 
    $this->db->select('users.id,users.first_name,users.emp_id,users.is_active,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email');
    $this->db->from('payroll_status');
    $this->db->join('users','users.id = payroll_status.user_id');
    $this->db->join('user_details','user_details.user_id = users.id');
    $this->db->join('company_designations','company_designations.id = users.designation_id');
    $this->db->join('company_departments','company_departments.id = users.department_id');
    $this->db->join('company_branches','company_branches.id = users.branch_id');
  
   
    $this->db->where($where);
    $query = $this->db->get();
    return $query->result();
}

public function viewPayrollWhereCtc($where,$where2)
{
   $this->db->select('users.id,users.first_name,users.emp_id,users.is_active,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email');
    $this->db->from('payroll_status');
    $this->db->join('users','users.id = payroll_status.user_id');
    $this->db->join('user_details','user_details.user_id = users.id');
    $this->db->join('company_designations','company_designations.id = users.designation_id');
    $this->db->join('company_departments','company_departments.id = users.department_id');
    $this->db->join('company_branches','company_branches.id = users.branch_id');
  
    $this->db->where($where);
    $this->db->where($where2);
     $query = $this->db->get();
     return $query->result();
}
  

    public function viewPayrolltest()
    { 
        $this->db->select('user_payroll.id,user_payroll.payroll_code,user_payroll.status,user_payroll.month,user_payroll.year,user_payroll.pf,user_payroll.esi,user_payroll.hra,user_payroll.hra,user_payroll.medical,user_payroll.conveyance,user_payroll.basic_salary,user_payroll.user_id,user_payroll.created_by,company_branches.branch_name,
        company_departments.department_name,company_designations.designation_name,users.first_name,users.user_code,user_payroll.total_salary,user_payroll.pf,user_payroll.esi,user_payroll.total_earning,user_payroll.allowance_type,user_payroll.hra,user_payroll.total_deduction,user_payroll.medical,user_payroll.conveyance,user_payroll.medical_group,user_payroll.total_salary');
        $this->db->from('user_payroll');
        $this->db->join('users','users.id = user_payroll.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
      
       
        $query = $this->db->get();
        return $query->result();
    }

    public function viewPayroll($where)
    { 
        $this->db->select('user_payroll.id,user_payroll.payroll_code,user_payroll.month,user_payroll.year,user_payroll.pf,user_payroll.basic_salary,user_payroll.user_id,user_payroll.created_by,company_branches.branch_name,
        company_departments.department_name,company_designations.designation_name,users.first_name,users.emp_id,user_payroll.pf,users_payslip.hra_monthly,user_payroll.bonus_yearly,user_payroll.reimbursement,
        user_payroll.lta,user_payroll.commission,user_payroll.adujestment,users_payslip.esi_monthly,user_details.joining_date,company_branches.branch_name,company_branches.branch_location,users_payslip.basic_monthly,users_payslip.basic_yearly,
        users_payslip.hra_yearly,users_payslip.pf_monthly,users_payslip.pf_yearly,users_payslip.esi_monthly,users_payslip.esi_yearly,users_payslip.conveyance_monthly,users_payslip.yearly_conveyance,users_payslip.medical_monthly,users_payslip.medical_yearly');
        $this->db->from('user_payroll');
        $this->db->join('users','users.id = user_payroll.user_id');
        $this->db->join('users_payslip','users_payslip.user_id = users.id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
      
       
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewPayrollWhere($where,$where2)
    {
        $this->db->select('user_payroll.id,user_payroll.payroll_code,user_payroll.month,user_payroll.year,user_payroll.pf,user_payroll.basic_salary,user_payroll.user_id,user_payroll.created_by,company_branches.branch_name,
        company_departments.department_name,company_designations.designation_name,users.first_name,users.emp_id,user_payroll.pf,users_payslip.hra_monthly,user_payroll.bonus_yearly,user_payroll.reimbursement,
        user_payroll.lta,user_payroll.commission,user_payroll.adujestment,users_payslip.esi_monthly,user_details.joining_date,company_branches.branch_name,company_branches.branch_location,users_payslip.basic_monthly,users_payslip.basic_yearly,
        users_payslip.hra_yearly,users_payslip.pf_monthly,users_payslip.pf_yearly,users_payslip.esi_monthly,users_payslip.esi_yearly,users_payslip.conveyance_monthly,users_payslip.yearly_conveyance,users_payslip.medical_monthly,users_payslip.medical_yearly');
        $this->db->from('user_payroll');
        $this->db->join('users','users.id = user_payroll.user_id');
        $this->db->join('users_payslip','users_payslip.user_id = users.id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
      
        $this->db->where($where);
        $this->db->where($where2);
         $query = $this->db->get();
         return $query->result();
    }
    
  public function addPayroll($data)
    {
        $this->db->insert('user_payroll',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function addPayrollctc($data)
    {
        $this->db->insert('users_payslip',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function deletePayroll($where_payroll)
    {
        $this->db->where($where_payroll);
        $this->db->delete('user_payroll');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
}