<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: JobApplicantStatusModel.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Models of the Job Applicant Status.
*/

class JobApplicantStatusModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
//  check job Status if allready exist in data base 
   public function checkJobStatus($where_status)
   {
   $query = $this->db->get_where('job_applicant_status',$where_status);
    if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }
    public function addJobApplicantStatus($data)
    {
        $this->db->insert('job_applicant_status',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteJobApplicantStatus($where_leave_type)
    {
        $this->db->where($where_leave_type);
        $this->db->delete('job_applicant_status');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editJobApplicantStatus($data,$where)
    {
        $this->db->where($where);
        $this->db->update('job_applicant_status',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewJobApplicantStatus()
    {
        $this->db->select('*');
        $this->db->from('job_applicant_status');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewJobApplicantStatusWhere($where)
    {
        $query = $this->db->get_where('job_applicant_status', $where);
        return $query->result();
    }
}