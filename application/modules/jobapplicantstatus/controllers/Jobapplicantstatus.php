<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: jobApplicantStatus.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Functions of the Job Applicant Status.
*/

class Jobapplicantstatus extends MY_Controller{

    public function __construct(){
		parent::__construct();
		$this->load->model('jobApplicantStatusModel');
	}
	// check job status Availability
	public function _chechJobStatusAvailablity($request)
  {
  $module_name = 'job_applicant_status';
  $permissions = $this->users->_checkPermission($module_name);
  foreach($permissions as $permission){}
  	if($permission->can_add == 'Y')
		{
			/* Here we check the Job Applicants Status. If User has the permission for that. */

			if(isset($request->job_applicant_status))
			{
				$where_type = array(
					'job_applicant_status' => $request->job_applicant_status
				);

				$result = $this->jobApplicantStatusModel->checkJobStatus($where_type);
				if($result)
					return array("message" => $request->job_applicant_status." is Available.");
				else
					$this->api->_sendError("Job Status Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Job Status.");
		}
    }	
	public function _addJobApplicantStatus($request)
    {
		$jobApplicantStatus = array();
		$module_name = 'job_applicant_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Job Applicant Status. If User has the permission for that. */

			if(isset($request->job_applicant_status))
			{
        $status = $this->_chechJobStatusAvailablity($request);
				$jobApplicantStatus = array(
					'job_applicant_status' => $request->job_applicant_status
				);

				if(isset($request->status_color))
					$jobApplicantStatus['status_color'] = $request->status_color;
				else	
					$jobApplicantStatus['status_color'] = "#009efb"; 		// Primary Blue Color
				
				$result = $this->jobApplicantStatusModel->addJobApplicantStatus($jobApplicantStatus);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Applicant Status Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Applicant Status Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Job Applicant Status.");
		}
    }

    public function _deleteJobApplicantStatus($request)
    {		
		$module_name = 'job_applicant_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Job Applicant Status. If User has the permission for that. */

			if(isset($request->job_applicant_status_id))
			{
				$where_job_applicant_status = array(
					'id' => $request->job_applicant_status_id,
				);
				$result = $this->jobApplicantStatusModel->deleteJobApplicantStatus($where_job_applicant_status);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Applicant Status Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Applicant Status Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Job Applicant Status.");
		}
    }

    public function _editJobApplicantStatus($request)
    {
		$jobApplicantStatus = array();
		$module_name = 'job_applicant_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Job Status.. If User has the permission for that. */

			if(isset($request->job_applicant_status_id))
			{
				$where_job_applicant_status = array(
					'id' => $request->job_applicant_status_id
				);

				if(isset($request->job_applicant_status))
					$jobApplicantStatus['job_applicant_status'] = $request->job_applicant_status;
					//$status = $this->_chechJobStatusAvailablity($request);
				if(isset($request->status_color))
					$jobApplicantStatus['status_color'] = $request->status_color;

				$result = $this->jobApplicantStatusModel->editJobApplicantStatus($jobApplicantStatus,$where_job_applicant_status);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Applicant Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Applicant Status Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Job Applicant Status.");
		}
    }

    public function _viewJobApplicantStatus($request)
    {		
		$module_name = 'job_applicant_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_job_applicant_status = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Job Applicant Status.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->job_applicant_status_id))
				{
					$where_job_applicant_status['id'] = $request->job_applicant_status_id;
					$result = $this->jobApplicantStatusModel->viewJobApplicantStatusWhere($where_job_applicant_status);
					return $result;
				}
				else
				{
					$result = $this->jobApplicantStatusModel->viewJobApplicantStatus();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Job Applicant Status.");
		}
    }
}
