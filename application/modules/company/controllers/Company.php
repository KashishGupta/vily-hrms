<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Company.php
	Date: August 04, 2020
	Info: This is the main class which is hold all the Functions of the Events.
*/

class Company extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('companyModel');
	}
    // view country
    public function _viewCountries($request)
    {
		$module_name = 'company';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Countries. If User has the permission for that. */

			if(TRUE)
            {	if(isset($request->country_id))
                {
                    $coutry_where['countries.id'] = $request->country_id;
                    $result = $this->companyModel->viewCountrieswhere($coutry_where);
					return $result;
                }else{
					$result = $this->companyModel->viewCountries();
                    return $result;	
                }
        }
        else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    
   }

     else
    {
    $this->api->_sendError("Sorry, You Have No Permission To View company.");
    }
  }
  // view States
  public function _viewStates($request)
    {
		$module_name = 'company';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Countries. If User has the permission for that. */

			if(TRUE){

			if(isset($request->country_id))
					$state_where['states.country_id'] = $request->country_id;
				if(isset($request->states_id))
					$state_where['states.id'] = $request->states_id;
            
				if(!empty($state_where))
				{
					$result = $this->companyModel->viewstateswhere($state_where);
					return $result;
				}
				else{
					$result = $this->companyModel->viewstates();
                    return $result;	
                }
        }
        else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    
   }

     else
    {
    $this->api->_sendError("Sorry, You Have No Permission To View company.");
    }
  }

   // view Cities
   public function _viewCities($request)
   {
	   
		   /* Here we View the Countries. If User has the permission for that. */

		   if(TRUE){

		   if(isset($request->state_id)){
				   $state_where['cities.state_id'] = $request->state_id;
				   $result = $this->companyModel->viewCitywhere($state_where);
				   return $result;
		   }
			   else{
				   $result = $this->companyModel->viewCity();
				   return $result;	
			   }
	   }
	   else
		   {
			   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		   }
	   
   
  
 }

  // view Company
  public function _viewCompany($request)
    {
		$module_name = 'company';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Coumpany. If User has the permission for that. */

			if(TRUE){

			if(isset($request->company_id))
					$company_where['company.id'] = $request->company_id;
            
				if(!empty($company_where))
				{
					$result = $this->companyModel->viewCompanywhere($company_where);
					return $result;
				}
				else{
					$result = $this->companyModel->viewCompany();
                    return $result;	
                }
        }
        else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    
   }

     else
    {
    $this->api->_sendError("Sorry, You Have No Permission To View company.");
    }
  }


	// check company Email Availability
	public function _checkCompanyEmailAvailability($request)
	{		
		$module_name = 'company';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the email. If User has the permission for that. */

			if(isset($request->company_email))
			{
				$where_email = array(
					'company_email' => $request->company_email
				);

				$result = $this->companyModel->checkEmailAvailability($where_email);
				if($result)
					return array("message" => $request->email." is Available.");
				else
					$this->api->_sendError("Company Already Present In Our Records.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add company.");
		}
	}
}
