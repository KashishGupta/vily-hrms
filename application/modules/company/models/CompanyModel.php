<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
  Project: Willy HRMS
  Author: Pratap Singh
  File Name: CompanyModel.php
  Date: August 04, 2020
  Info: This is the main class which is hold all the Models of the Events.
*/

class CompanyModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function viewCountries()
    {
        $this->db->select('*');
        $this->db->from('countries');
      
        $query = $this->db->get();
        return $query->result();
    }
    public function viewCountrieswhere($where)
    {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    function checkEmailAvailability($where_email)
    {
        $query = $this->db->get_where('company' , $where_email);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }
    public function viewstates()
    {
        $this->db->select('*');
        $this->db->from('states');
      
        $query = $this->db->get();
        return $query->result();
    }
    public function viewstateswhere($where)
    {
        $this->db->select('*');
        $this->db->from('states');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewCity()
    {
        $this->db->select('*');
        $this->db->from('cities');
      
        $query = $this->db->get();
        return $query->result();
    }
    public function viewCitywhere($where)
    {
        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function addcompany($data)
    {
        $this->db->insert('company',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deletecompany($where_company)
    {
        $this->db->where($where_company);
        $this->db->delete('company');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function viewCompany()
    {
        $this->db->select('company.company_name,company.id,company.contactperson,company.company_address,company.owner_name,company.company_email,company.phone_no,
        company.password,company.app_email,company.app_password,countries.name as country_name,states.name as state_name,cities.name as city,company.postal_code,company.fax_no,
        company.database_name,company.api_link,company.device_id,company.folder_name,company.company_logo,company.status,states.id as state_id,countries.id as countries_id,cities.id as city_id');
        $this->db->from('company');
        $this->db->join('states', 'states.id = company.state');
        $this->db->join('cities', 'cities.id = company.city');
        $this->db->join('countries', 'countries.id = company.country_name');
        $query = $this->db->get();
        return $query->result();
    }
    public function viewCompanywhere($where)
    {
        $this->db->select('company.company_name,company.id,company.contactperson,company.company_address,company.owner_name,company.company_email,company.phone_no,
        company.password,company.app_email,company.app_password,countries.name as country_name,states.name as state_name,cities.name as city,company.postal_code,company.fax_no,
        company.database_name,company.api_link,company.device_id,company.folder_name,company.company_logo,company.status,states.id as state_id,countries.id as countries_id,cities.id as city_id');
        $this->db->from('company');
        $this->db->join('states', 'states.id = company.state');
        $this->db->join('cities', 'cities.id = company.city');
        $this->db->join('countries', 'countries.id = company.country_name');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    // company Edit
    public function editCompany($data,$where)
    {
        $this->db->where($where);
        $this->db->update('company',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

      // Result : SUCCESS BUT WHEN FILE PERMISSION IS 777 
      public function unzip_company_seed($company_name)
      {
          $zip = new ZipArchive;
          $res = $zip->open("seed/company_seed.zip");
          if ($res === TRUE) 
          {
              // Unzip path
              $extractpath = "".$company_name."/";
              
              // Extract file
              $zip->extractTo($extractpath);
              $zip->close();
  
              //$this->session->set_flashdata('msg','Upload & Extract successfully.');
              //echo "Success";
          } 
          else 
          {
              //$this->session->set_flashdata('msg','Failed to extract.');
              //echo "fail";
          }
      } 

       // Result : SUCCESS
    public function create_company_db($company_name)
    {
        $this->load->dbforge();

        if ($this->dbforge->create_database($company_name))
        {
                //echo 'Database created!';
        }
    }

     // Result : SUCCESS
     public function company_sql_import($company_name,$adminData)
     {
         $config['hostname'] = "localhost";
         $config['username'] = "root";
         $config['password'] = "Redhat@123!@#";
         $config['database'] = $company_name;
         $config['dbdriver'] = "mysqli";
         $config['dbprefix'] = "";
         $config['pconnect'] = FALSE;
         $config['db_debug'] = TRUE;
         $config['cache_on'] = FALSE;
         $config['cachedir'] = "";
         $config['char_set'] = "utf8";
         $config['dbcollat'] = "utf8_general_ci";
 
         $db2 = $this->load->database($config,TRUE);
 
         // Temporary variable, used to store current query
         $templine = '';
         // Read in entire file
         $lines = file('seed/company_seed.sql');
         // Loop through each line
         foreach ($lines as $line)
         {
             // Skip it if it's a comment
             if (substr($line, 0, 2) == '--' || $line == '')
                 continue;
 
             // Add this line to the current segment
             $templine .= $line;
             // If it has a semicolon at the end, it's the end of the query
             if (substr(trim($line), -1, 1) == ';')
             {
                 // Perform the query
                 $db2->query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $db2->_error_message() . '<br /><br />');
                 // Reset temp variable to empty
                 $templine = '';
             }            
         }
         $config['dbcollat'] = "latin1_swedish_ci";
         $db2 = $this->load->database($config,TRUE);
         $db2->insert('admin',$adminData);        
         //echo "Tables imported successfully";
     }

      // Result : SUCCESS BUT WHEN FILE PERMISSION IS 777 
    public function company_config_write($company_name)
    {
        $this->load->helper('file');
        
        $filename = getcwd() . "/".$company_name."/application/config/config.php";

        if( chmod($filename, 0777) ) 
        {
            chmod($filename, 0755);
        }
            
        $line_i_am_looking_for = 25;
        $lines = file( $filename , FILE_IGNORE_NEW_LINES );
        $master_url = base_url();
        $lines[$line_i_am_looking_for] = "$"."config['base_url'] = '".$master_url."".$company_name."/';";
        if(file_put_contents( $filename , implode( "\n", $lines ) ))
        {
           // echo "Config File Changed.";
        }
        else
        {
           // echo "error Occured.";
        }
    }

    // Result : SUCCESS BUT WHEN FILE PERMISSION IS 777 
    public function company_database_write($company_name)
    {
        $this->load->helper('file');
        
        $filename = getcwd() . "/".$company_name."/application/config/database.php";

        if( chmod($filename, 0777) ) 
        {
            chmod($filename, 0755);
        }
            
        $line_i_am_looking_for = 80;
        $lines = file( $filename , FILE_IGNORE_NEW_LINES );
        $master_url = base_url();
        $lines[$line_i_am_looking_for] = "'database' => '".$company_name."',";
        if(file_put_contents( $filename , implode( "\n", $lines ) ))
        {
            //echo "Database File Changed.";
        }
        else
        {
            //echo "error Occured.";
        }
    }

     // Result : SUCCESS BUT WHEN FILE PERMISSION IS 777 
     public function file_write_test()
     {
         $this->load->helper('file');
 
         $data = 'Insert Data Using Codeigniter.';
         if ( ! write_file('test_company2/application/config/test.php', $data))
         {
             echo 'Unable to write the file';
         }
         else
         {
             echo 'File written! <br>';
             $string = read_file('test_company2/application/config/test.php');
             echo $string;
         }
     }

     public function postCURL($_url, $_param){

        $postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v) 
        { 
          $postData .= $k . '='.$v.'&'; 
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }
    
}