<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: Jobstatus.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Functions of the Jobstatus.
*/

class Jobstatus extends MY_Controller{

    public function __construct(){
		parent::__construct();
		$this->load->model('jobStatusModel');
	}
	
	public function _addJobStatus($request)
    {
		$jobStatus = array();
		$module_name = 'job_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Job Status.. If User has the permission for that. */

			if(isset($request->job_status))
			{
				$jobStatus = array(
					'job_status' => $request->job_status
				);

				if(isset($request->status_color))
					$jobStatus['status_color'] = $request->status_color;
				else	
					$jobStatus['status_color'] = "#009efb"; 		// Primary Blue Color
				
				$result = $this->jobStatusModel->addJobStatus($jobStatus);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Status Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Status Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Job Status.");
		}
    }

    public function _deleteJobStatus($request)
    {		
		$module_name = 'job_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Job Status.. If User has the permission for that. */

			if(isset($request->job_status_id))
			{
				$where_job_status = array(
					'id' => $request->job_status_id,
				);
				$result = $this->jobStatusModel->deleteJobStatus($where_job_status);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Status Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Status Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Job Status.");
		}
    }

    public function _editJobStatus($request)
    {
		$jobStatus = array();
		$module_name = 'job_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Job Status.. If User has the permission for that. */

			if(isset($request->job_status_id))
			{
				$where_job_status = array(
					'id' => $request->job_status_id
				);

				if(isset($request->job_status))
					$jobStatus['job_status'] = $request->job_status;
				if(isset($request->status_color))
					$jobStatus['status_color'] = $request->status_color;

				$result = $this->jobStatusModel->editJobStatus($jobStatus,$where_job_status);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Status Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Job Status.");
		}
    }

    public function _viewJobStatus($request)
    {		
		$module_name = 'job_status';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_job_status = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Job Status. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->job_status_id))
				{
					$where_job_status['id'] = $request->job_status_id;
					$result = $this->jobStatusModel->viewJobStatusWhere($where_job_status);
					return $result;
				}
				else
				{
					$result = $this->jobStatusModel->viewJobStatus();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Job Status.");
		}
    }
}