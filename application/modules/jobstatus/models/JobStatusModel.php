<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: JobStatusModel.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Models of the Job Status.
*/

class JobStatusModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function addJobStatus($data)
    {
        $this->db->insert('job_status',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteJobStatus($where_leave_type)
    {
        $this->db->where($where_leave_type);
        $this->db->delete('job_status');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editJobStatus($data,$where)
    {
        $this->db->where($where);
        $this->db->update('job_status',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewJobStatus()
    {
        $this->db->select('*');
        $this->db->from('job_status');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewJobStatusWhere($where)
    {
        $query = $this->db->get_where('job_status', $where);
        return $query->result();
    }
}