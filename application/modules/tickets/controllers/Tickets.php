<?php
require APPPATH . '/libraries/S3Document.php';
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Tickets.php
	Date: March 23, 2020
	Info: This is the main class which is hold all the Functions of the Tickets.
*/

class Tickets extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->objdocumentS3 = new s3Document();
		$this->load->model('ticketsModel');
		$this->load->model('projectsModel');
		$this->load->model('onboardingModel');
		$this->load->model('offboardingModel');
		$this->load->model('reimbursementModel');
		$this->load->model('jobApplicantsModel');
	}
	// add Tickets
	public function _addTickets($request)
    {
		
			if(isset($request->title) AND isset($request->priority) AND isset($request->send_branch) AND isset($request->description) AND isset($request->send_department) AND isset($request->type))
			{ 
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$ticketsData = array(
					'ticket_no' => $this->api->_getRandomString(6),
					'title' => $request->title,
					'send_department'=>$request->send_department,
					'send_branch'=>$request->send_branch,
					'created_by' => $user_id,
					'description' => $request->description,
					'priority' => $request->priority,
				     'status' => 0,
					'user_id' => $user_id,
					'ticket_type'=>$request->type
				); 
				// Add Query In ticket Model
				$result = $this->ticketsModel->addTickets($ticketsData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Tickets Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Tickets Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}	
	
	}
		
		// view Tickets Comments
		public function _viewTicketsComments($request)
		{
			
				/* Here we Add the Tickets. If User has the permission for that. */
				if(TRUE)
				{
				if(isset($request->ticket_id))
		     	{
					$where_tickets = array(
						'ticket_id' => $request->ticket_id,
					);			
			    $result = $this->ticketsModel->viewTicketsCommentWhere($where_tickets);
				return $result;
				}
				else{
					$result = $this->ticketsModel->viewTicketsComment();
					return $result;
				}
			}
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			
		}
		// add Tickets Priority
		public function _viewTicketsPriority($request)
		{
			
				/* Here we Add the Tickets. If User has the permission for that. */
	
				if(TRUE)
				{ 
								
			    $result = $this->ticketsModel->viewTicketsPriority();
				return $result;
				}
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			
		}
//  Delete The Tickets
    public function _deleteTickets($request)
    {
		$module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Tickets. If User has the permission for that. */

			if(isset($request->tickets_id))
			{
				$where_tickets = array(
					'id' => $request->tickets_id,
				);
				$result = $this->ticketsModel->deleteTickets($where_tickets);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Tickets Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Tickets Not Deleted. Please Try -!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Tickets.");
		}
    }

// Active or inactive status
 public function _editTicketsActive($request)
    {
		$ticketStatus = array();
		$module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the tickes status. If User has the permission for that. */

			if(isset($request->ticket_id))
			{
				$where_ticket_type = array(
					'id' => $request->ticket_id
				);

				if(isset($request->status))
					$ticketStatus['status'] = $request->status;
					
				if(isset($request->ticket_solution))
					$ticketStatus['ticket_solution'] = $request->ticket_solution;
				$result = $this->ticketsModel->editTickets($ticketStatus,$where_ticket_type);
				if($request->status == 0){
					if($result)
					{
						return array(
							'status' => '1',
							'message' => 'Ticket is Active Successfully.'
						);
					}
					else
					{
						$this->api->_sendError("Sorry, Ticket Status Not Updated. Please Try Again!!");
					}  
				}else{
					if($result)
					{
						return array(
							'status' => '1',
							'message' => 'Ticket is Inactive Successfully.'
						);
					}
					else
					{
						$this->api->_sendError("Sorry, Ticket Status Not Updated. Please Try Again!!");
					}  
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Ticket Status.");
		}
	}
	// Job Applicants
	public function _addJobApplicant($request)
    {	
			
		if(isset($request->job_id) AND isset($request->first_name) AND isset($request->job_applicant_status_id) AND isset($request->experience) AND isset($request->current_location) AND isset($request->email) AND isset($request->phone) AND isset($request->current_designation) AND isset($request->hieghest_education) AND isset($request->education) AND isset($request->skills) AND isset($request->current_company) AND isset($request->resume))
		{
		
     $jobstatus = $request->job_applicant_status_id;
      if($jobstatus == 1){
      $this->api->_sendError("This Job  is Currently Closed.");
      }
       else if($jobstatus == 2){
      $this->api->_sendError("This Job  is Currently Hold.");
      }
       else if($jobstatus == 3){
      $this->api->_sendError("This Job  is Currently call Off.");
      }else{

		$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
			$jobApplicant = array(
				'job_id' => $request->job_id,
				'first_name' => $request->first_name,
				'education' => $request->education,
				'phone' => $request->phone,
				'email'	=> $request->email,
                'experience' => $request->experience,
				'current_location' => $request->current_location,
				'current_designation' => $request->current_designation,
				'highest_education' => $request->hieghest_education,
				'skills'	=> $request->skills,
   	            'current_company'	=> $request->current_company,
				'referral_by'=> $user_id,
                'job_applicant_status_id'=>$request->job_applicant_status_id,
            	'status' => 0,
				'is_refferal' => 0
			);
	  if(isset($request->resume))
	  $baseimage = $request->resume;
				 if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
				 {
					
				 $pdf = 'data:application/pdf;base64,';
				 $wordImage = 'data:image/jpeg;base64,';
				 $wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
				 if(strpos($baseimage, $pdf) !== false){
				 $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
				 $image_type = 'pdf';
				 } else if(strpos($baseimage, $wordImage) !== false){
				 $source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
				 $image_type = 'jpeg';  
				 }else if(strpos($baseimage, $wordexcelword) !== false){
				 $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
				 $image_type = 'docx';
				 }else{
				// echo "Word  Found!";
				 }
				
				 // Set Image Type To "JPEG"
				 $t=time();                  // Get Current Time
				 $nametime = $t.'.'.$image_type;      // Set Image Nam
				  $name = 'ezdat_documents/' . $nametime;
				 $file = base64_decode($source_image);       // Set File Using Decode Base64String
				 $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
				 $source_image = $name;          // Set Source Image Name
				 $source_image_url123 = $this->objdocumentS3->url($name);      // Set Source Image URL
				 $jobApplicant['resume'] = $source_image_url123;
			 }else{
				$jobApplicant['resume'] = 0;
			}
		    	
       
			$result = $this->jobApplicantsModel->addJobApplicant($jobApplicant);
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'Job Applied Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, Job Not Applied. Please Try Again!!");
			}
		}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
    }
	// add Reimbursement
	public function _addReimbursement($request)
    {

    	 
		
			/* Here we Add the Reimbursement. If User has the permission for that. */

			if(isset($request->end_city) AND isset($request->start_date) AND isset($request->end_date) AND isset($request->start_time)  AND isset($request->end_time) AND isset($request->mode_of_journey) AND isset($request->amount))
			{
				$leaveStartDate=strtotime($request->start_date);
				 $dayOfStartLeave = date("l", $leaveStartDate);

				 $leaveEndDate=strtotime($request->end_date);
				 $dayOfEndLeave = date("l", $leaveEndDate);

				 $currentDate = date('d-m-Y');
				 $currentDate=strtotime($currentDate);
				 /* End Date Conversion */
				 if($leaveStartDate > $currentDate){
				 $this->api->_sendError('Reimbursement Cannot Be Applied On Future Date.');
				 }
				 /* Weekend and Date Ahead Check Start */
				 if($leaveStartDate > $leaveEndDate){
				 $this->api->_sendError('End Date Ahead From Start Date.');
				 }
				 // reimbursement status
				 $user = $this->api->_GetTokenData();
				 $user_id = $user['id'];
				 $user_name = $user['first_name'];
				 $status_data['status'] = 0;
				  $status_data['user_id']= $user_id;
				 // $status_data['approved_by']= $user_name;
					 $otherResult = $this->reimbursementModel->addstatus($status_data); 
					 $last_id = $this->db->insert_id();
					 $arrModeJourney = $request->mode_of_journey;
					 $query = $this->db->query("SELECT * FROM reimbursement_policy_travel WHERE id ='".$arrModeJourney."'");
				    $arrjourneyData = $query->row();
					 $tourData = array(
						'end_city' => $request->end_city,
						'start_date' => $request->start_date,
						'end_date' => $request->end_date,
						'start_time' => $request->start_time,
						  'end_time' => $request->end_time,
						   'mode_of_journey' => $arrjourneyData->policy_name,
						'amount' => $request->amount,
						 'status' => 0,
						 'reimbursement_id'=>$last_id,
						'user_id' => $user_id
					);
					 if(isset($request->image))
		  $baseimage = $request->image;
                     if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
                     {
						
					 $pdf = 'data:application/pdf;base64,';
					 $wordImage = 'data:image/jpeg;base64,';
					 $wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
					 if(strpos($baseimage, $pdf) !== false){
					 $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
					 $image_type = 'pdf';
					 } else if(strpos($baseimage, $wordImage) !== false){
					 $source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					 $image_type = 'jpeg';  
					 }else if(strpos($baseimage, $wordexcelword) !== false){
					 $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
					 $image_type = 'docx';
					 }else{
					// echo "Word  Found!";
					 }
					
					 // Set Image Type To "JPEG"
					 $t=time();                  // Get Current Time
					 $nametime = $t.'.'.$image_type;      // Set Image Nam
				      $name = 'ezdat_documents/' . $nametime;
					 $file = base64_decode($source_image);       // Set File Using Decode Base64String
					 $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
					 $source_image = $name;          // Set Source Image Name
					 $source_image_url12 = $this->objdocumentS3->url($name);      // Set Source Image URL
					 $tourData['image'] = $source_image_url12;
				 }else{
					$tourData['image'] = 0;
				}        // Run When Image Successfully Upload On AWS S3
				
				
				if(isset($request->starting_city))
				$tourData['starting_city'] = $request->starting_city;
				//print_r($tourData);
			
				$tourresult = $this->reimbursementModel->addTour($tourData);

        // hotel Reimbursement
        
         
          
		  if(isset($request->hotel_bill))
		  $baseimagehotel = $request->hotel_bill;
          if($baseimagehotel != NULL)     // Run When source_image is Not NULL | source_image is Valid
        {
		  $pdf = 'data:application/pdf;base64,';
		  $wordImage = 'data:image/jpeg;base64,';
		  $wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
		  if(strpos($baseimagehotel, $pdf) !== false){
		  $source_image = str_replace('data:application/pdf;base64,','',$baseimagehotel);
		  $image_type = 'pdf';
		  } else if(strpos($baseimagehotel, $wordImage) !== false){
		  $source_image = str_replace('data:image/jpeg;base64,','',$baseimagehotel);
		  $image_type = 'jpeg';  
		  }else if(strpos($baseimagehotel, $wordexcelword) !== false){
		  $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimagehotel);
		  $image_type = 'docx';
		  }else{
		 // echo "Word  Found!";
		  }
		  // Set Image Type To "JPEG"
		  $t=time();                  // Get Current Time
		  $nametime = $t.'.'.$image_type;      // Set Image Nam
		 $name = 'ezdat_documents/' . $nametime;
		  $file = base64_decode($source_image);       // Set File Using Decode Base64String
		  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
		  $source_image = $name;          // Set Source Image Name
		  $source_image_urlhotel = $this->objdocumentS3->url($name);      // Set Source Image URL
		  $reimbursement_data = array( 
			'hotel_bill' => $source_image_urlhotel
		  );
		}else{
			$reimbursement_data['hotel_bill'] = 0;
			}
			if(isset($request->departure_date))
					$reimbursement_data['departure_date'] = $request->departure_date;
           if(isset($request->arrival_date))
					$reimbursement_data['arrival_date'] = $request->arrival_date;
          if(isset($request->hotel_amount))
					$reimbursement_data['hotel_amount'] = $request->hotel_amount;		
            if(isset($request->hotel_name))
					$reimbursement_data['hotel_name'] = $request->hotel_name;
            
             $reimbursement_data['status'] = 0;
              $reimbursement_data['user_id']= $user_id;
			  $reimbursement_data['reimbursement_id'] = $last_id;
			 
         	$reimbursementResult = $this->reimbursementModel->addReimbursement($reimbursement_data);

          // other Detals
          
            $other_data['reimbursement_id'] = $last_id;
            if(isset($request->employee_amount))
					$other_data['employee_amount'] = $request->employee_amount;
				
          
          if(isset($request->bill_name))
					$other_data['bill_name'] = $request->bill_name;
         
         if(isset($request->other_details))
					$other_data['other_details'] = $request->other_details;
          
          if(isset($request->other_date))
					$other_data['other_date'] = $request->other_date;
		  if(isset($request->other_image))
		  $baseimageother = $request->other_image;
		  if($baseimageother != NULL)     // Run When source_image is Not NULL | source_image is Valid
		  {
		  $pdf = 'data:application/pdf;base64,';
		  $wordImage = 'data:image/jpeg;base64,';
		  $wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
		  if(strpos($baseimageother, $pdf) !== false){
		  $source_image = str_replace('data:application/pdf;base64,','',$baseimageother);
		  $image_type = 'pdf';
		  } else if(strpos($baseimageother, $wordImage) !== false){
		  $source_image = str_replace('data:image/jpeg;base64,','',$baseimageother);
		  $image_type = 'jpeg';  
		  }else if(strpos($baseimageother, $wordexcelword) !== false){
		  $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimageother);
		  $image_type = 'docx';
		  }else{
		 // echo "Word  Found!";
		  }
		  // Set Image Type To "JPEG"
		  $t=time();                  // Get Current Time
		  $nametime = $t.'.'.$image_type;      // Set Image Nam
				      $name = 'ezdat_documents/' . $nametime;
		  $file = base64_decode($source_image);       // Set File Using Decode Base64String
		  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
		  $source_image = $name;          // Set Source Image Name
		  $source_image_urlother = $this->objdocumentS3->url($name);      // Set Source Image URL
		  $other_data['other_image'] = $source_image_urlother;
		}else{
			$other_data['other_image'] = 0;
		}
				
           $other_data['status'] = 0;
            $other_data['user_id']= $user_id;
			   $otherResult = $this->reimbursementModel->addOther($other_data);
			   
          // food Reimbursement
         
         $food_data['reimbursement_id'] = $last_id;
         if(isset($request->food_name))
					$food_data['food_name'] = $request->food_name;
				
           if(isset($request->city))
					$food_data['city'] = $request->city;
          
          if(isset($request->food_date))
					$food_data['food_date'] = $request->food_date;
           
		  if(isset($request->food_bill))
		  $baseimagebill = $request->food_bill;
		  if($baseimagebill != NULL)     // Run When source_image is Not NULL | source_image is Valid
		  {
		  $pdf = 'data:application/pdf;base64,';
		  $wordImage = 'data:image/jpeg;base64,';
		  $wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
		  if(strpos($baseimagebill, $pdf) !== false){
		  $source_image = str_replace('data:application/pdf;base64,','',$baseimagebill);
		  $image_type = 'pdf';
		  } else if(strpos($baseimagebill, $wordImage) !== false){
		  $source_image = str_replace('data:image/jpeg;base64,','',$baseimagebill);
		  $image_type = 'jpeg';  
		  }else if(strpos($baseimagebill, $wordexcelword) !== false){
		  $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimagebill);
		  $image_type = 'docx';
		  }else{
		  //echo "Word  Found!";
		  }
		  // Set Image Type To "JPEG"
		  $t=time();                  // Get Current Time
		  $nametime = $t.'.'.$image_type;      // Set Image Nam
		  $name = 'ezdat_documents/' . $nametime;
		  $file = base64_decode($source_image);       // Set File Using Decode Base64String
		  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
		  $source_image = $name;          // Set Source Image Name
		  $source_image_urlbill = $this->objdocumentS3->url($name);      // Set Source Image URL
		  $food_data['food_bill'] = $source_image_urlbill;
		  }else{
			$food_data['food_bill'] = 0;
		  }
		  
          
            if(isset($request->food_amount))
					$food_data['food_amount'] = $request->food_amount;
          
           $food_data['status'] = 0;
           $food_data['user_id']= $user_id;
          $result = $this->reimbursementModel->addFood($food_data);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
	// Add tickets comments
	public function _addTicketsComments($request)
    {
		
			/* Here we Update/Edit the tickes status. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($request->ticket_id) AND isset($request->comment))
			{
				
		if(isset($request->image_name))
     $baseimage = $request->image_name;
    
     if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
     {
		 
		$pdf = 'data:application/pdf;base64,';
		$wordImage = 'data:image/jpeg;base64,';
		$wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
		if(strpos($baseimage, $pdf) !== false){
		   $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
			$image_type = 'pdf';
	   } else if(strpos($baseimage, $wordImage) !== false){
			$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
			$image_type = 'jpeg';  
	   }else if(strpos($baseimage, $wordexcelword) !== false){
		   $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
		   $image_type = 'docx';
	   }else{
		 echo "Word  Found!";
	   }
           // Set Image Type To "JPEG"
       $t=time();                  // Get Current Time
       $nametime = $t.'.'.$image_type;      // Set Image Nam
				      $name = 'ezdat_documents/' . $nametime;
       $file = base64_decode($source_image);       // Set File Using Decode Base64String
       $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
       $source_image = $name;          // Set Source Image Name
       $source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
	   $addcomments = array( 
		'image_name' => $source_image_url
	  ); 
	 
	}else{
		$addcomments = array( 
			'image_name' => 0
		  ); 
	}
	$addcomments['ticket_id'] = $request->ticket_id;
	$addcomments['user_id'] = $user_id;
	$addcomments['comment'] = $request->comment;
                 
		 $result = $this->ticketsModel->addTicketComments($addcomments);  // Update users Table

         if($result)
         {
            return array(
           'status' => '1',
           'message' => 'Tickets Comment Added Successfully.'
           );
         }
         else
         {
 $attendance = array('message' => 'Sorry, Some Error Occured. Please uploadAgain Again!!.');
     $this->api->_sendError($attendance);  
          
         }
                                     
     
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	 // add Projects
	 public function _addProject($request)
	 {
		 
	 $module_name = 'projects';
	 $permissions = $this->users->_checkPermission($module_name);
	 foreach($permissions as $permission){}
	 if($permission->can_add == 'Y')
	 {
	 if(isset($request->project_name) AND isset($request->client_name) AND isset($request->project_team) AND isset($request->start_date) AND isset($request->end_date)  AND isset($request->project_language) AND isset($request->project_leader_id) AND isset($request->project_description) AND isset($request->project_file) AND isset($request->department_id)){
	 
	 $user = $this->api->_GetTokenData();
		   $user_id = $user['first_name'];
		   $user_code = $user['user_code'];
		   $teamUser = $request->project_team;
		   $whereuser = implode(',',$teamUser);
		   $where = 'users.id IN ('.implode(',',$teamUser).') ';
		 
			 $results = $this->projectsModel->viewLeaveticketwhere($where);
			 foreach($results as $result)
				{
					$user_names[] = $result->first_name;
				}
			   
			 $arrProjectTeamName = implode(',',$user_names);
				   
				  
				  $baseimage = $request->project_file;
				 
				  $pdf = 'data:application/pdf;base64,';
				  $wordImage = 'data:image/jpeg;base64,';
				  $wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
				  if(strpos($baseimage, $pdf) !== false){
					 $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
					  $image_type = 'pdf';
				 } else if(strpos($baseimage, $wordImage) !== false){
					  $source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					  $image_type = 'jpeg';  
				 }else if(strpos($baseimage, $wordexcelword) !== false){
					 $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
					 $image_type = 'docx';
				 }else{
				   echo "Word  Found!";
				 }
				 
				  if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
				  {
						// Set Image Type To "JPEG"
					$t=time();                  // Get Current Time
					$nametime = $t.'.'.$image_type;      // Set Image Nam
					$name = 'ezdat_documents/' . $nametime;
					$file = base64_decode($source_image);       // Set File Using Decode Base64String
					$uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
					$source_image = $name;          // Set Source Image Name
					$source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
					if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
					{
						$arrproject1leade = array('users.id' =>$request->project_leader_id);
						$arrleader = $this->projectsModel->viewLeaderProjectwhere($arrproject1leade);
						
					  // Set User Data
					  $project = array(
						'project_name' => $request->project_name,
						'creator_usercode' => $user_code,
								  'client_name' => $request->client_name,
								  'start_date' => $request->start_date,
								  'end_date' => $request->end_date,
								  'project_language' => $request->project_language,
								  'project_leader_id' => $arrleader->first_name,
								  'project_description' => $request->project_description,
								  'project_file' => $source_image_url,
						'department_id' => $request->department_id,
						'project_team'=>$whereuser,
						'project_team_name'=>$arrProjectTeamName,
						  'status'	=> 0,
								  'creator_name'	=> $user_id
							  
							  );   
							  
					  $result = $this->projectsModel->addProject($project); // Update users Table
			 
					  if($result)
					  {
						 return array(
						'status' => '1',
						'message' => 'projects Added Successfully.'
						);
					  }
					  else
					  {
			  $attendance = array('message' => 'Sorry, Some Error Occured. Please uploadAgain Again!!.');
				  $this->api->_sendError($attendance);  
					   
					  }
					}
					else                                    // Run When Image Not Upload On AWS S3
					{
			  $attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
				  $this->api->_sendError($attendance);  
					}                                 
				  }
				  
	 }
	 else
	 {
	 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	 }
	 }
	 else
	 {
	 $this->api->_sendError("Sorry, You Have No Permission To Add Project.");
	 }
	 }
	  // edit Projects
	  public function _editProject($request)
	  {
		  
		  $module_name = 'projects';
			$permissions = $this->users->_checkPermission($module_name);
			foreach($permissions as $permission){}
			
		if($permission->can_edit == 'Y')
			{
				/* Here we Update/Edit the Project.. If User has the permission for that. */
	
				if(isset($request->project_id))
				{
					$whereProject = array(
						'id' => $request->project_id
					);
					if(isset($request->project_file))
					$baseimage = $request->project_file;
				 
					
				   
					if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
					{
						$pdf = 'data:application/pdf;base64,';
					$wordImage = 'data:image/jpeg;base64,';
					$wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
					if(strpos($baseimage, $pdf) !== false){
					   $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
						$image_type = 'pdf';
				   } else if(strpos($baseimage, $wordImage) !== false){
						$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
						$image_type = 'jpeg';  
				   }else if(strpos($baseimage, $wordexcelword) !== false){
					   $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
					   $image_type = 'docx';
				   }else{
					 echo "Word  Found!";
				   }
						  // Set Image Type To "JPEG"
					  $t=time();                  // Get Current Time
					  $nametime = $t.'.'.$image_type;      // Set Image Nam
				      $name = 'ezdat_documents/' . $nametime;
					  $file = base64_decode($source_image);       // Set File Using Decode Base64String
					  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
					  $source_image = $name;          // Set Source Image Name
					  $source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
					// Set User Data
					$project = array( 
						'project_file' => $source_image_url
					  ); 
				  }else{
					$usertr_where = $request->project_id;
			  $query = $this->db->query("SELECT * FROM user_project WHERE id ='".$usertr_where."'");
				$arrTicketsData = $query->row();
			   // print_r($arrTicketsData);
					$userDetails = array( 
					  'project_file' => $arrTicketsData->project_file
					); 
				   // print_r($userDetails);
				   }
					  
						if(isset($request->project_name))
						$project['project_name'] = $request->project_name;
						if(isset($request->client_name))
						$project['client_name'] = $request->client_name;
						if(isset($request->end_date))
						$project['end_date'] = $request->end_date;
						if(isset($request->start_date))
						$project['start_date'] = $request->start_date;
						if(isset($request->department_id))
						$project['department_id'] = $request->department_id;
						if(isset($request->project_language))
						$project['project_language'] = $request->project_language;
						if(isset($request->project_leader_id))
						//$arrproject1leade = array('users.id' =>$request->project_leader_id);
						//$arrleader = $this->projectsModel->viewLeaderProjectwhere($arrproject1leade);
						$project['project_leader_id'] = $request->project_leader_id;
						if(isset($request->project_description))
						$project['project_description'] = $request->project_description;
						if(isset($request->project_team))
						$teamUser =$request->project_team; 
						
						$whereuser = implode("','",$request->project_team);
					
							$query = $this->db->query("SELECT * FROM `users` WHERE `first_name` IN ('$whereuser')");
							$arrProjectsData1 = $query->result();
							$arrtempData =  array();
							foreach($arrProjectsData1 as $team1) {
							$arrtempData[] = $team1->id;
							} 
							$project_teamUser = implode(",",$arrtempData);
						
						$project['project_team'] = $project_teamUser;
						
					
						$project['project_team_name'] = $whereuser;
									  
		$result = $this->projectsModel->editProject($project,$whereProject);
		if($result)
		{
		return array(
							'status' => '1',
							'message' => 'Project Updated Successfully.'
						);
		}
						else
						{
				$attendance = array('message' => 'Sorry, Some Error Occured. Please uploadAgain Again!!.');
					$this->api->_sendError($attendance);  
						 
						}
					                                 
					
					
		
		
	   }
	   else
	   {
		$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	   }
	   }
	   else
	   {
	   $this->api->_sendError("Sorry, You Have No Permission To Edit Project.");
	   }
	  }

	  // add Offboarding Document
	  public function _addOffboarding($request)
	  {
		   $module_name = 'offboarding';
		   $permissions = $this->users->_checkPermission($module_name);
		   foreach($permissions as $permission){}
		   $offboarding_add = array();
		   
		   if($permission->can_add == 'Y')
		   {
		if(isset($request->user_id) AND isset($request->description) AND isset($request->offboarding_doc))
		{

		   $offboarding_where = array(
			   'user_id'=>$request->user_id
			   ); 
		   $offboarding_Status = array(
			   'status'=> 1
		   
			   ); 
			   
	   $results = $this->offboardingModel->EditoffboardingStatus($offboarding_Status,$offboarding_where);
				$baseimage = $request->offboarding_doc;
			   
				$pdf = 'data:application/pdf;base64,';
				$wordImage = 'data:image/jpeg;base64,';
				$wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
				if(strpos($baseimage, $pdf) !== false){
				   $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
					$image_type = 'pdf';
			   } else if(strpos($baseimage, $wordImage) !== false){
					$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					$image_type = 'jpeg';  
			   }else if(strpos($baseimage, $wordexcelword) !== false){
				   $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
				   $image_type = 'docx';
			   }else{
				 echo "Word  Found!";
			   }
			   
				if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
				{
					  // Set Image Type To "JPEG"
				  $t=time();                  // Get Current Time
				  $nametime = $t.'.'.$image_type;      // Set Image Nam
				  $name = 'ezdat_documents/' . $nametime;
				  $file = base64_decode($source_image);       // Set File Using Decode Base64String
				  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
				  //$source_image = $name;          // Set Source Image Name
				  $source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
				  if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
				  {
					$offboarding_add = array(
						'user_id'=>$request->user_id,
						'description'=>$request->description,
						'status'=> 0,
						'offboarding_doc'=>$source_image_url
						); 
						
						$result = $this->offboardingModel->Insertoffboarding($offboarding_add);
						
						if($result)
						{
						return array(
									   'status' => '1',
									   'message' => 'Offboarding Document Added Successfully.'
								   );
						}
						else
						{
						$this->api->_sendError("Sorry, Offboarding Document Not Added. Please Try Again!!");
						}
				  }
				  else                                    // Run When Image Not Upload On AWS S3
				  {
			$attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
				$this->api->_sendError($attendance);  
				  }                                 
				}
				else
				{
					$this->api->_sendError("Sorry, some perameter missing!!");
				} 
		
		}
		else
		{
		$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
		}
		else
		{
		$this->api->_sendError("Sorry, Sorry You Have No Permission To Add Offboarding");
		}
	   
	  } 
	  // Add Onboarding User documents
	public function _addOnboarding($request)
    {
		$reimbursementData = array();
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Onboarding. If User has the permission for that. */
			if(isset($request->user_id))
			{
				if(isset($request->offer_letter))
				
				$baseimage = $request->offer_letter;
			   
				$pdf = 'data:application/pdf;base64,';
				$wordImage = 'data:image/jpeg;base64,';
				$wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
				if(strpos($baseimage, $pdf) !== false){
				   $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
					$image_type = 'pdf';
			   } else if(strpos($baseimage, $wordImage) !== false){
					$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					$image_type = 'jpeg';  
			   }else if(strpos($baseimage, $wordexcelword) !== false){
				   $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
				   $image_type = 'docx';
			   }else{
				 echo "Word  Found!";
			   }
			   
				if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
				{
					  // Set Image Type To "JPEG"
				  $t=time();                  // Get Current Time
				  $nametime = $t.'.'.$image_type;      // Set Image Nam
				      $name = 'ezdat_documents/' . $nametime;
				  $file = base64_decode($source_image);       // Set File Using Decode Base64String
				  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
				  $source_image = $name;          // Set Source Image Name
				  $source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
				  if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
				  {
					$user = $this->api->_GetTokenData();
				$user_id = $user['id'];	
				$user_personal = array(
				    'user_id' => $request->user_id,
					'status' => 1, 
					'offer_letter' =>$source_image_url,             
					'created_by' => $user_id
				);
				
			    
               if(isset($request->document_name))
					$user_personal['document_name'] = $request->document_name;
				$result = $this->onboardingModel->addOnboarding($user_personal);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Offer Letter Uploaded Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Offer Letter Not Added. Please Try Again!!");
				}
				  }
				  else                                    // Run When Image Not Upload On AWS S3
				  {
			$attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
				$this->api->_sendError($attendance);  
				  }                                 
				}
				else
				{
					$this->api->_sendError("Sorry, some perameter missing!!");
				} 
			    	
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Onboarding.");
		}
    }
	   // Add Personal documents
	public function _addExperience($request)
    {
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Onboarding. If User has the permission for that. */
			if(isset($request->user_id))
			{
				if(isset($request->experience_letter))
				
				$baseimage = $request->experience_letter;
			   
				$pdf = 'data:application/pdf;base64,';
				$wordImage = 'data:image/jpeg;base64,';
				$wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
				if(strpos($baseimage, $pdf) !== false){
				   $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
					$image_type = 'pdf';
			   } else if(strpos($baseimage, $wordImage) !== false){
					$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					$image_type = 'jpeg';  
			   }else if(strpos($baseimage, $wordexcelword) !== false){
				   $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
				   $image_type = 'docx';
			   }else{
				 echo "Word  Found!";
			   }
			   
				if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
				{
					  // Set Image Type To "JPEG"
				  $t=time();                  // Get Current Time
				  $nametime = $t.'.'.$image_type;      // Set Image Nam
				      $name = 'ezdat_documents/' . $nametime;
				  $file = base64_decode($source_image);       // Set File Using Decode Base64String
				  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
				  $source_image = $name;          // Set Source Image Name
				  $source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
				  if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
				  {
					$user = $this->api->_GetTokenData();
				$user_id = $user['id'];	
				$user_experienceletter = array(
				    'user_id' => $request->user_id,
					'status' => 1,
					'experience_letter'=>$source_image_url,
					'created_by' => $user_id
				);
				  if(isset($request->document_name))
					$user_experienceletter['document_name'] = $request->document_name;
			   
				$result = $this->onboardingModel->addOnexperience($user_experienceletter);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Experience Document Uploaded Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Experience Document Not Added. Please Try Again!!");
				}
				  }
				  else                                    // Run When Image Not Upload On AWS S3
				  {
			$attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
				$this->api->_sendError($attendance);  
				  }                                 
				}
				else
				{
					$this->api->_sendError("Sorry, some perameter missing!!");
				} 
			    	
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Experience Document.");
		}
    }
	  // Add Personal documents
	public function _addPersonalDocument($request)
    {
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Onboarding. If User has the permission for that. */
			if(isset($request->user_id))
			{
				if(isset($request->personaldocument))
				$baseimage = $request->personaldocument;
			   
				$pdf = 'data:application/pdf;base64,';
				$wordImage = 'data:image/jpeg;base64,';
				$wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
				if(strpos($baseimage, $pdf) !== false){
				   $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
					$image_type = 'pdf';
			   } else if(strpos($baseimage, $wordImage) !== false){
					$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					$image_type = 'jpeg';  
			   }else if(strpos($baseimage, $wordexcelword) !== false){
				   $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
				   $image_type = 'docx';
			   }else{
				 echo "Word  Found!";
			   }
			   
				if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
				{
					  // Set Image Type To "JPEG"
				  $t=time();                  // Get Current Time
				  $nametime = $t.'.'.$image_type;      // Set Image Nam
				  $name = 'ezdat_documents/' . $nametime;
				  $file = base64_decode($source_image);       // Set File Using Decode Base64String
				  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
				  $source_image = $name;          // Set Source Image Name
				  $source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
				  if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
				  {
					$user = $this->api->_GetTokenData();
			     	$user_id = $user['id'];	
			    	$user_aadharcard = array(
				    'user_id' => $request->user_id,
					'status' => 1,
					'personaldocument'=>$source_image_url,
					'created_by' => $user_id
				); 
				
                if(isset($request->document_name))
				$user_aadharcard['document_name'] = $request->document_name;
			$result = $this->onboardingModel->addPersonal($user_aadharcard);
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'Personal Document Uploaded Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, Personal Document Not Added. Please Try Again!!");
			}
				  }
				  else                                    // Run When Image Not Upload On AWS S3
				  {
			$attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
				$this->api->_sendError($attendance);  
				  }                                 
				}
				else
				{
					$this->api->_sendError("Sorry, some perameter missing!!");
				} 	
				
			   
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Personal Document.");
		}
    }
	   // Add Personal documents onboarding
	public function _addOtherdocument($request)
    {
		$reimbursementData = array();
		$module_name = 'onboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Onboarding. If User has the permission for that. */
			if(isset($request->user_id))
			{
			    if(isset($request->other))	
				$baseimage = $request->other;
			   
				$pdf = 'data:application/pdf;base64,';
				$wordImage = 'data:image/jpeg;base64,';
				$wordexcelword = 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
				if(strpos($baseimage, $pdf) !== false){
				   $source_image = str_replace('data:application/pdf;base64,','',$baseimage);
					$image_type = 'pdf';
			   } else if(strpos($baseimage, $wordImage) !== false){
					$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					$image_type = 'jpeg';  
			   }else if(strpos($baseimage, $wordexcelword) !== false){
				   $source_image = str_replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,','',$baseimage);
				   $image_type = 'docx';
			   }else{
				 echo "Word  Found!";
			   }
			   
				if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
				{
					  // Set Image Type To "JPEG"
				  $t=time();                  // Get Current Time
				  $nametime = $t.'.'.$image_type;      // Set Image Nam
				      $name = 'ezdat_documents/' . $nametime;
				  $file = base64_decode($source_image);       // Set File Using Decode Base64String
				  $uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
				  $source_image = $name;          // Set Source Image Name
				  $source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
				  if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
				  {
					$user = $this->api->_GetTokenData();
				$user_id = $user['id'];	
				$user_other = array(
				    'user_id' => $request->user_id,
					'status' => 1,
					'other'=>$source_image_url,
					'created_by' => $user_id
				);       
							if(isset($request->document_name))
							$user_other['document_name'] = $request->document_name;
							 if(isset($request->instruction_document))
							$user_other['instruction_document'] = $request->instruction_document;
						$result = $this->onboardingModel->addOther($user_other);
		   
					if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Other Document Uploaded Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Other Document Not Uploaded. Please Try Again!!");
				}
				  }
				  else                                    // Run When Image Not Upload On AWS S3
				  {
			$attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
				$this->api->_sendError($attendance);  
				  }                                 
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Uploaded Other Document.");
		}
    }
// edit Tickets
 public function _editTickets($request)
    {
		$ticketData = array();
		$module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Ticket. If User has the permission for that. */

			if(isset($request->ticket_id))
			{
				
                $currentdate=date('Y-m-d');
				$ticket_where = array(
					'id' => $request->ticket_id
				
				);
	    
				if(isset($request->title))
					$ticketData['title'] = $request->title;
				if(isset($request->description))
					$ticketData['description'] = $request->description;
				if(isset($request->send_department))
					$ticketData['send_department'] = $request->send_department;
				if(isset($request->send_branch))
					$ticketData['send_branch'] = $request->send_branch;
				if(isset($request->priority))
					$ticketData['priority'] = $request->priority;
				if(isset($request->ticket_type))
					$ticketData['ticket_type'] = $request->ticket_type;

				$result = $this->ticketsModel->editTickets($ticketData,$ticket_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Tickets Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Tickets Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Tickets.");
		}
    }
    // view Tickets Logged in User
    
    public function _viewTicketsLoggedInuser($request){
   $module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$profile_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Tickets.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$ticket_where['user_tickets.user_id'] = $user_id;				
			$result = $this->ticketsModel->viewTicketsWhere($ticket_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		
		
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Profile.");
		}
    }
  // view Tickets
  
  public function _viewTicketsSelf($request)
  {		
  
		  /* Here we View the userss.. If User has the permission for that. */

		  if(TRUE)
		  {
		  
	 if(isset($request->ticket_id)){
			 $ticket_where['user_tickets.id'] = $request->ticket_id;
              $result = $this->ticketsModel->viewTicketsUser($ticket_where);
				  return $result;
	
			  }
	  else{
	  $result = $this->ticketsModel->viewTicketsSelf();
				  return $result;
	  }
		  }
		  else
		  {
			  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		  }
	 
  }

  // view all Reimnursment Approved by Branch 
  public function _viewTicketsEdit($request)
  {		

		  /* Here we View the userss.. If User has the permission for that. */

		  if(TRUE)
		  {
		  
	           if(isset($request->ticket_id)){
				  $ticket_where['user_tickets.id'] = $request->ticket_id;
				  $result = $this->ticketsModel->viewTicketsWhereEdit($ticket_where);
				  return $result;
				}
	           else{
	               $result = $this->ticketsModel->viewTicketsEdit();
				  return $result;
	              }
		  }
		  else
		  {
			  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		  }
	 
  }
   // view all Reimnursment Approved by Branch 
    public function _viewTickets($request)
    {		
	
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['role_id'];
				//print_r($user_id);
				if($user_id == 1){
               if(isset($request->ticket_id)){
					$ticket_where['user_tickets.id'] = $request->ticket_id;
					$result = $this->ticketsModel->viewTicketsWhereBranch($ticket_where);
					return $result;
				}
               else{
                   $result = $this->ticketsModel->viewTickets();
					return $result;
				   }
				}else{
					if(isset($request->ticket_id)){
						$ticket_where['user_tickets.id'] = $request->ticket_id;
						$result = $this->ticketsModel->viewTicketsWheredepartment($ticket_where);
						return $result;
					}
				   else{
					   $result = $this->ticketsModel->viewTicketsdepartment();
						return $result;
					   }
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}

    // view Total Tickets
     public function _viewTicketsTotal($request)
    {		
			
			/* Here we View the Total Tickets.. If User has the permission for that. */

			if(TRUE)
			{
     
					$user = $this->api->_GetTokenData();
				$user_id = $user['role_id'];
				//print_r($user_id);
				if($user_id == 1){
               
                   $result = $this->ticketsModel->viewTotalTickets();
					return $result;
				   }
				else{
					
					   $result = $this->ticketsModel->viewTicketemployee();
						return $result;
					 
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	
	}
	// view Total Tickets
	public function _viewTicketsTotalSelf($request)
    {		
		
			/* Here we View the Total Tickets.. If User has the permission for that. */

			if(TRUE)
			{
				     $user = $this->api->_GetTokenData();
					 $user_id = $user['id'];
					 $where_user = array('user_tickets.user_id'=>$user_id);
					$result = $this->ticketsModel->viewTotalTickets($where_user);
					return $result;
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
  // view total leave tickets
      public function _viewTicketsleaveTotal($request)
    {		
			$module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Total Leaves Tickets.. If User has the permission for that. */

			if(TRUE)
			{
				
        	$user_ids = $this->users->_getUserId();
				if(!empty($user_ids))
					$where = 'user_tickets.user_id IN ('.implode(',',$user_ids).')';
				else
					$where = 'user_tickets.user_id = 0';
					$result = $this->ticketsModel->viewTotalLeaveTickets($where);
					return $result;
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Total Leave Tickets.");
		}
	}
    // view total leave self tickets
	public function _viewTicketsleaveSelf($request)
    {		
		
			/* Here we View the Total Leaves Tickets.. If User has the permission for that. */

			if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$where_user = array('user_tickets.user_id'=>$user_id);
			   $result = $this->ticketsModel->viewTotalLeaveTickets($where_user);
			   return $result;
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	 // view total leave self tickets
	 public function _viewTicketsPayrollSelf($request)
	 {		
		
			 /* Here we View the Total Leaves Tickets.. If User has the permission for that. */
 
			 if(TRUE)
			 {
				 $user = $this->api->_GetTokenData();
				 $user_id = $user['id'];
				 $where_user = array('user_tickets.user_id'=>$user_id);
				$result = $this->ticketsModel->viewTotalPayrollTickets($where_user);
				return $result;
				 
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		
	 }

	  // view total Others self tickets
	  public function _viewTotalmediumTicket($request)
	  {		
		 
			  /* Here we View the Total Leaves Tickets.. If User has the permission for that. */
  
			  if(TRUE)
			  {
				$user = $this->api->_GetTokenData();
				$user_id = $user['role_id'];
				//print_r($user_id);
				if($user_id == 1){
				
					$result = $this->ticketsModel->viewMediumPriority();
					return $result;
				}else{
					$result = $this->ticketsModel->viewMediumPriorityDepartment();
					return $result;
				}
				  
			  }
			  else
			  {
				  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			  }
		 
	  }
    // view total Payroll Tickets
    
    public function _viewTotalHighTicket($request){
   
    if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['role_id'];
				//print_r($user_id);
				if($user_id == 1){
				
					$result = $this->ticketsModel->viewHighPriority();
					return $result;
				}else{
					$result = $this->ticketsModel->viewHighPriorityDepartment();
					return $result;
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
   
    }
     // view total Attendance Tickets
    
    public function _viewTotalLowPriority($request){
    
    if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['role_id'];
				//print_r($user_id);
				if($user_id == 1){
				
					$result = $this->ticketsModel->viewLowPriority();
					return $result;
				}else{
					$result = $this->ticketsModel->viewLowPriorityDepartment();
					return $result;
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
   
    }
	
	// view Tickets Type
	public function _viewTicketsType($request)
	{
			/* Here we Add the Tickets. If User has the permission for that. */

			if(TRUE)
			{ 
				if(isset($request->type_id)){
				$ticket_where['ticket_type.id'] = $request->type_id;
				$result = $this->ticketsModel->viewTicketsTypewhere($ticket_where);
				return $result;
			}
			
	       else{
		        $result = $this->ticketsModel->viewTicketsType();
		        return $result;
	         }				
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}

	// add Tickets Type
	public function _addTicketsType($request)
    {
		$ticketsData = array();
		$module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			if(isset($request->type_name) AND isset($request->description))
			{ 
				$ticketsData = array(
					'type_name' => $request->type_name,
					'description'=>$request->description
				); 
				// Add Query In ticket Model
				$result = $this->ticketsModel->addTicketsType($ticketsData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Tickets Type Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Tickets Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}	
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Tickets.");
		}
	}
	//  Delete The Tickets type
    public function _deleteTicketsType($request)
    {
		$module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Tickets Type. If User has the permission for that. */

			if(isset($request->type_id))
			{
				$where_type = array(
					'id' => $request->type_id,
				);
				$result = $this->ticketsModel->deleteTicketsType($where_type);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Tickets Type Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Tickets Type Not Deleted. Please Try -!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Tickets.");
		}
    }
	// edit Tickets Type
	public function _editTicketsType($request)
    {
		$ticketsData = array();
		$module_name = 'tickets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			if(isset($request->type_id))
			{ 
				$type_where = array(
					'id' => $request->type_id
				
				);
				if(isset($request->type_name))
					$typeData['type_name'] = $request->type_name;
				if(isset($request->description))
					$typeData['description'] = $request->description;
				// edit Query In ticket Model
				$result = $this->ticketsModel->editTicketsType($typeData,$type_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Tickets Type Edit Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Tickets Not Edit. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}	
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Tickets.");
		}
	}
	
	// update profile image
	public function _updateProfileImage($request)
    {
			/* Here we Update/Edit the tickes status. If User has the permission for that. */
			
			if(isset($request->user_image))
			{
				$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			$whrerDAta = array('user_id' =>$user_id);
			$checkattendance = $this->ticketsModel->getLastRegisterData($whrerDAta);
			//print_r($checkattendance);
			if(!empty($checkattendance->id)){
				
				$baseimage = $request->user_image;
			
				if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
				{
			 
			
					$wordImage = 'data:image/jpeg;base64,';
	
					if(strpos($baseimage, $wordImage) !== false){
					$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
					$image_type = 'jpeg';  
					}else{
					echo "Word  Found!";
					}
			   // Set Image Type To "JPEG"
					$t=time();                  // Get Current Time
					$nametime = $t.'.'.$image_type;      // Set Image Nam
							$name = 'ezdat_documents/' . $nametime;
					$file = base64_decode($source_image);       // Set File Using Decode Base64String
					$uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
					$source_image = $name;          // Set Source Image Name
					$source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
					$updateimage = array( 
					'user_image' => $source_image_url
					); 
	
					}else{
					$addcomments = array( 
					'user_image' => 0
					); 
					}
					$update_where = array( 
						'user_id' => $user_id
						); 
					 
						$result = $this->ticketsModel->editUser_image($updateimage,$update_where);
	
						if($result)
						{
						return array(
						'status' => '1',
						'message' => 'Profile Update Successfully.'
						);
						}
						else
						{
						$attendance = array('message' => 'Sorry, Some Error Occured. Please uploadAgain Again!!.');
						$this->api->_sendError($attendance);  
						
						}
			}else
                                
			$baseimage = $request->user_image;
			
			if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
			{
		 
		
				$wordImage = 'data:image/jpeg;base64,';

				if(strpos($baseimage, $wordImage) !== false){
				$source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
				$image_type = 'jpeg';  
				}else{
				echo "Word  Found!";
				}
           // Set Image Type To "JPEG"
				$t=time();                  // Get Current Time
				$nametime = $t.'.'.$image_type;      // Set Image Nam
						$name = 'ezdat_documents/' . $nametime;
				$file = base64_decode($source_image);       // Set File Using Decode Base64String
				$uploaded_image = $this->objdocumentS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
				$source_image = $name;          // Set Source Image Name
				$source_image_url = $this->objdocumentS3->url($name);      // Set Source Image URL
				$updateimage = array( 
				'user_image' => $source_image_url
				); 

				}else{
				$updateimage = array( 
				'user_image' => 0
				); 
				}
					
                    $updateimage['user_id'] = $user_id;
					$result = $this->ticketsModel->insertDatauser($updateimage);
					if($result)
					{
					return array(
					'status' => '1',
					'message' => 'Profile Update Successfully.'
					);
					}
					else
					{
					$attendance = array('message' => 'Sorry, Some Error Occured. Please uploadAgain Again!!.');
					$this->api->_sendError($attendance);  
					
					}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}


    }