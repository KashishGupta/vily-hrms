<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: TicketsModel.php
	Date: March 23, 2020
	Info: This is the main class which is hold all the Models of the Tickets.
*/

class TicketsModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function getLastRegisterData($where)
    {
     
        $this->db->select('*');
        $this->db->from('user_details'); 
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->row();
       
        
    }
    
    public function insertDatauser($data)
    {
        $this->db->insert('user_details',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function editUser_image($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_details',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }


    public function addTicketComments($data)
    {
        $this->db->insert('ticket_comments',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function addTickets($data)
    {
        $this->db->insert('user_tickets',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

     public function deleteTickets($where_tickets)
    {
        $this->db->where($where_tickets);
        $this->db->delete('user_tickets');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
 public function editTickets($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_tickets',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
// count all total tickects
    public function viewTotalTickets()
    {
        
         $this->db->select('count(*) as id' );
        $this->db->from('user_tickets');
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $query = $this->db->get();
        return $query->result();
    }

    // Add Tickets Priority
    public function viewTicketsPriority()
    {
     $this->db->select('*');
     $this->db->from('priority');
     $query = $this->db->get();
     return $query->result();
    }
    // Delete ticket type
    public function deleteTicketsType($where_type)
    {
        $this->db->where($where_type);
        $this->db->delete('ticket_type');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // edit ticket type
    public function editTicketsType($data,$where)
    {
        $this->db->where($where);
        $this->db->update('ticket_type',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
   // add Tickets Type
    public function addTicketsType($data)
    {
        $this->db->insert('ticket_type',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     // view Tickets Priority
     public function viewTicketsType()
     {
      $this->db->select('*');
      $this->db->from('ticket_type');
      $query = $this->db->get();
      return $query->result();
     }

     // view Tickets Priority
     public function viewTicketsTypewhere($where)
     {
      $this->db->select('*');
      $this->db->from('ticket_type');
      $this->db->where($where);
      $query = $this->db->get();
     
      return $query->result();
     }
//view Tickets
  public function viewTicketsWhereBranch($where)
    {
       $this->db->select('user_tickets.id,users.first_name,user_tickets.description,users.email,company_departments.department_name as send_department,user_details.user_image,user_tickets.title,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets');
        $this->db->join('company_departments','company_departments.id = user_tickets.send_department');
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
       
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->where($where);
        
        $query = $this->db->get();
        return $query->result();
    }

    //view Tickets by department Id
  public function viewTicketsWheredepartment($where)
  {
    $user = $this->api->_GetTokenData();
    $user_department = $user['department_id'];
    $where2 = array('user_tickets.send_department'=>$user_department);
     $this->db->select('user_tickets.id,users.first_name,user_tickets.description,users.email,company_departments.department_name as send_department,user_details.user_image,user_tickets.title,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,company_branches.branch_name,users.emp_id');
      $this->db->from('user_tickets');
      $this->db->join('company_departments','company_departments.id = user_tickets.send_department');
      $this->db->join('users','user_tickets.user_id = users.id');
      $this->db->join('company_designations','company_designations.id = users.designation_id');
     
      $this->db->join('company_branches','company_branches.id = users.branch_id');
      $this->db->join('user_details','user_details.user_id = users.id');
      $this->db->where($where);
      $this->db->where($where2);
      $query = $this->db->get();
      return $query->result();
  }
  public function viewTicketemployee()
  {
    $user = $this->api->_GetTokenData();
    $user_department = $user['department_id'];
    $where2 = array('user_tickets.send_department'=>$user_department);
       $this->db->select('count(*) as id' );
      $this->db->from('user_tickets');
      $this->db->join('users','user_tickets.user_id = users.id');
      $this->db->join('company_designations','company_designations.id = users.designation_id');
      $this->db->join('company_departments','company_departments.id = users.department_id');
      $this->db->join('company_branches','company_branches.id = users.branch_id');
      $this->db->where($where2);
      $query = $this->db->get();
      return $query->result();
  }
    //view Tickets by department Id
    public function viewTicketsdepartment()
    {
        $user = $this->api->_GetTokenData();
        $user_department = $user['department_id'];
        $where = array('user_tickets.send_department'=>$user_department);
       $this->db->select('user_tickets.id,users.first_name,user_tickets.description,users.email,company_departments.department_name as send_department,user_details.user_image,user_tickets.title,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets');
        $this->db->join('company_departments','company_departments.id = user_tickets.send_department');
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
       
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewTicketsSelf()
    {
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];
        $where_user = array('user_tickets.user_id'=>$user_id);
        $this->db->select('user_tickets.id,user_tickets.title,user_tickets.description,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,users.first_name,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets');
       
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->where($where_user);
        
        $query = $this->db->get();
        return $query->result();
    }
    public function viewTicketsUser($where2)
    {
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];
        $where_user = array('user_tickets.user_id'=>$user_id);
        $this->db->select('user_tickets.id,user_tickets.title,user_tickets.description,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,users.first_name,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets');
         
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->where($where_user);
        $this->db->where($where2);
        
        $query = $this->db->get();
        return $query->result();
    }
     public function viewTickets()
    {
        $this->db->select('user_tickets.id,user_details.user_image,user_tickets.description,user_tickets.title,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,users.first_name,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets');
         
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $query = $this->db->get();
        return $query->result();
    }
   public function viewTicketsWhere($where2)
    {
        $this->db->select('user_tickets.id,user_tickets.title,user_tickets.description,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,users.first_name,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets');
        
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
       
        $this->db->where($where2);
        
        $query = $this->db->get();
        return $query->result();
    }
    public function viewTicketsWhereEdit($where)
    {
        $this->db->select('user_tickets.id,user_tickets.title,user_tickets.description,user_tickets.send_branch,user_tickets.date,user_tickets.send_department,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,users.first_name,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets'); 
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->where($where);
        
        $query = $this->db->get();
        return $query->result();
    }
    public function viewTicketsEdit()
    {
        $this->db->select('user_tickets.id,user_tickets.title,user_tickets.description,user_tickets.send_branch,user_tickets.send_department,user_tickets.date,user_tickets.status,user_tickets.ticket_no,user_tickets.priority,user_tickets.ticket_type,user_tickets.modify_at,users.first_name,company_branches.branch_name,users.emp_id');
        $this->db->from('user_tickets'); 
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
       
        
        $query = $this->db->get();
        return $query->result();
    }
   public function viewTotalLeaveTickets($where2)
    {
        $this->db->select('count(*) as ticket_typeleave' );
        $this->db->from('user_tickets');
       
         
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
           $this->db->where('ticket_type', 'leave');
            $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
    
     public function viewTotalPayrollTickets($where2)
    {
        $this->db->select('count(*) as ticket_typePayroll' );
        $this->db->from('user_tickets');
       
          
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
           $this->db->where('ticket_type', 'Payroll');
             $this->db->where($where2);
        $query = $this->db->get();
  
       
        return $query->result();
    }
       // view priority 
       public function viewHighPriority()
       {
           $this->db->select('count(*) as highPriority');
           $this->db->from('user_tickets');
          
           $this->db->join('users','user_tickets.user_id = users.id');
           $this->db->join('company_designations','company_designations.id = users.designation_id');
           $this->db->join('company_departments','company_departments.id = users.department_id');
           $this->db->join('company_branches','company_branches.id = users.branch_id');
           $this->db->where('priority', 2);
           $query = $this->db->get();
       
           return $query->result();
       }
       public function viewHighPriorityDepartment()
       {
           
           $user = $this->api->_GetTokenData();
           $user_department = $user['department_id'];
           $where = array('user_tickets.send_department'=>$user_department);
           $this->db->select('count(*) as highPriority');
           $this->db->from('user_tickets');
          
           $this->db->join('users','user_tickets.user_id = users.id');
           $this->db->join('company_designations','company_designations.id = users.designation_id');
           $this->db->join('company_departments','company_departments.id = users.department_id');
           $this->db->join('company_branches','company_branches.id = users.branch_id');
           $this->db->where('priority', 2);
           $this->db->where($where);
           $query = $this->db->get();
       
           return $query->result();
       }

       // view priority 
       public function viewMediumPriority()
       {
           $this->db->select('count(*) as mediumPriority');
           $this->db->from('user_tickets');
          
           $this->db->join('users','user_tickets.user_id = users.id');
           $this->db->join('company_designations','company_designations.id = users.designation_id');
           $this->db->join('company_departments','company_departments.id = users.department_id');
           $this->db->join('company_branches','company_branches.id = users.branch_id');
           $this->db->where('priority', 1);
           $query = $this->db->get();
       
           return $query->result();
       }
       public function viewMediumPriorityDepartment()
       {
           $user = $this->api->_GetTokenData();
           $user_department = $user['department_id'];
           $where = array('user_tickets.send_department'=>$user_department);
           $this->db->select('count(*) as mediumPriority');
           $this->db->from('user_tickets');
          
           $this->db->join('users','user_tickets.user_id = users.id');
           $this->db->join('company_designations','company_designations.id = users.designation_id');
           $this->db->join('company_departments','company_departments.id = users.department_id');
           $this->db->join('company_branches','company_branches.id = users.branch_id');
           $this->db->where('priority', 1);
           $this->db->where($where);
           $query = $this->db->get();
       
           return $query->result();
       }


    // view priority 
    public function viewLowPriority()
    {
        $this->db->select('count(*) as lowPriority');
        $this->db->from('user_tickets');
       
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->where('priority', 0);
        $query = $this->db->get();
    
        return $query->result();
    }
    public function viewLowPriorityDepartment()
    {
        $user = $this->api->_GetTokenData();
        $user_department = $user['department_id'];
        $where = array('user_tickets.send_department'=>$user_department);
        $this->db->select('count(*) as lowPriority');
        $this->db->from('user_tickets');
       
        $this->db->join('users','user_tickets.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->where('priority', 0);
        $this->db->where($where);
        $query = $this->db->get();
    
        return $query->result();
    }
    
    
  
    // view Ticket Comments
  public function viewTicketsCommentWhere($where)
  {
     $this->db->select('*');
     $this->db->from('ticket_comments');
     $this->db->join('users','ticket_comments.user_id = users.id');
      $this->db->where($where);
      $query = $this->db->get();
      return $query->result();
  }
  public function viewTicketsComment()
  {
    $this->db->select('*');
    $this->db->from('ticket_comments');
    $this->db->join('users','ticket_comments.user_id = users.id');
    $this->db->join('user_details','user_details.user_id = users.id'); 
      $query = $this->db->get();
      return $query->result();
  }
}