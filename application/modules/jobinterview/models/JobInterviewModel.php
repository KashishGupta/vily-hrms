<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: JobInterviewModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Job Interviews.
*/

class JobInterviewModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function addJobInterview($data)
    {
        $this->db->insert('job_interviews',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteJobInterview($where_job)
    {
        $this->db->where($where_job);
        $this->db->delete('job_interviews');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editJobInterview($data,$where)
    {
        $this->db->where($where);
        $this->db->update('job_interviews',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewJobInterview()
    {
        $this->db->select('job_interviews.id as job_interview_id, job_interviews.interview_date, job_interviews.interview_time, job_interviews.remark, job_interviews.status as interview_status ,job_applicants.id as job_applicant_id, job_applicants.first_name, job_applicants.last_name, job_applicant_status.job_applicant_status as applicant_status, job_applicant_status.status_color as applicant_status_color ,jobs.id as job_id, jobs.job_number, company_designations.designation_name, company_departments.department_name, company_branches.branch_name, company_branches.branch_location, job_types.job_type, job_types.type_color');
        $this->db->from('job_interviews');
        $this->db->join('job_applicants', 'job_applicants.id = job_interviews.applicant_id');
        $this->db->join('jobs', 'job_applicants.job_id = jobs.id');
        $this->db->join('job_applicant_status', 'job_applicants.job_applicant_status_id = job_applicant_status.id');
        $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
        $this->db->join('company_designations', 'company_designations.id = jobs.designation_id');
        $this->db->join('company_departments', 'company_departments.id = company_designations.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
        //$this->db->group_by('jobs.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewJobInterviewWhere($where)
    {
        $this->db->select('job_interviews.id as job_interview_id, job_interviews.interview_date, job_interviews.interview_time, job_interviews.remark, job_interviews.status as interview_status ,job_applicants.id as job_applicant_id, job_applicants.first_name, job_applicants.last_name, job_applicant_status.job_applicant_status as applicant_status, job_applicant_status.status_color as applicant_status_color ,jobs.id as job_id, jobs.job_number, company_designations.designation_name, company_departments.department_name, company_branches.branch_name, company_branches.branch_location, job_types.job_type, job_types.type_color');
        $this->db->from('job_interviews');
        $this->db->join('job_applicants', 'job_applicants.id = job_interviews.applicant_id');
        $this->db->join('jobs', 'job_applicants.job_id = jobs.id');
        $this->db->join('job_applicant_status', 'job_applicants.job_applicant_status_id = job_applicant_status.id');
        $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
        $this->db->join('company_designations', 'company_designations.id = jobs.designation_id');
        $this->db->join('company_departments', 'company_departments.id = company_designations.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
        $this->db->where($where);
        //$this->db->group_by('jobs.id');
        $query = $this->db->get();
        return $query->result();
    }
}