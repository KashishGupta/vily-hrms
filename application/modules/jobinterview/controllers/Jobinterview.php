<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: Jobinterview.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the Job Interview.
*/

class Jobinterview extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('jobInterviewModel');
	}
	
/* -------------------------------------------------------------------------- */
/*          Used To Add Interview Or Schedule Interview Of Applicant          */
/* -------------------------------------------------------------------------- */

	public function _addJobInterview($request)
    {
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Add the Jobs Interview. If User has the permission for that. */

			if(isset($request->applicant_id) AND isset($request->interview_date) AND isset($request->interview_time))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				
				$jobInterview = array(
					'applicant_id' => $request->applicant_id,
					'interview_date' => $request->interview_date,
					'interview_time' => $request->interview_time,
					'status' => 0,
					'created_by' => $user_id
				);

				if(isset($request->remark))
					$jobInterview['remark'] = $request->remark;

				$result = $this->jobInterviewModel->addJobInterview($jobInterview);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Interview Scheduled Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Interview Not Scheduled. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Job Interview.");
		}
    }

/* -------------------------------------------------------------------------- */
/*             Used To Change Interview Status (Open & Cancelled)             */
/* -------------------------------------------------------------------------- */

    public function _changeJobInterviewStatus($request)
    {
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Edit the Jobs.. If User has the permission for that. */

			if(isset($request->job_interview_id) AND isset($request->status))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$job_interiew_where = array(
					'id' => $request->job_interview_id
				);

				if(!(in_array($request->status,['0','1'])))		// 0: Open | 1: Cancelled
				{
					$this->api->_sendError("Sorry, Wrong Status Choose. Please Try Again!!");
				}

				$jobInterview = array(
					'status' => $request->status,
					'modify_by' => $user_id
				);
				
				$result = $this->jobInterviewModel->editJobInterview($jobInterview,$job_interiew_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Interview Status Changed Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Interview Status Not Changed. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Change Job Interview Status.");
		}
	}

/* -------------------------------------------------------------------------- */
/*             This Function Used To Update The Job Interview Data            */
/* -------------------------------------------------------------------------- */

	public function _editJobInterview($request)
    {
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Edit the Jobs.. If User has the permission for that. */

			if(isset($request->job_interview_id))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$job_interiew_where = array(
					'id' => $request->job_interview_id
				);

				$jobInterview = array(
					'modify_by' => $user_id
				);
				if(isset($request->interview_date))
					$jobInterview['interview_date'] = $request->interview_date;
				if(isset($request->interview_time))
					$jobInterview['interview_time'] = $request->interview_time;
				if(isset($request->remark))
					$jobInterview['remark'] = $request->remark;
				
				$result = $this->jobInterviewModel->editJobInterview($jobInterview,$job_interiew_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Interview Update Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Interview Not Update. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Job Interview.");
		}
    }

/* -------------------------------------------------------------------------- */
/*                This is used To View Interviews Of Applicant                */
/* -------------------------------------------------------------------------- */
    public function _viewJobInterview($request)
    {		
		$module_name = 'jobs';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$job_interview_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Job Applicants.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->job_applicant_id))
					$job_interview_where['job_interviews.applicant_id'] = $request->job_applicant_id;
				if(isset($request->job_id))
					$job_interview_where['job_applicants.job_id'] = $request->job_id;
				if(isset($request->job_interviews_id))
					$job_interview_where['job_interviews.id'] = $request->job_interviews_id;
				
				if(!empty($job_interview_where))
				{
					$result = $this->jobInterviewModel->viewJobInterviewWhere($job_interview_where);
					return $result;
				}
				else
				{
					$result = $this->jobInterviewModel->viewJobInterview();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Job Interviews.");
		}
	}
}