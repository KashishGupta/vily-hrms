<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/S3.php';
//require APPPATH . '/libraries/Slack.php';

/* 
  Project: Willy HRMS
  Author: Pratap Singh
  File Name: Attendance.php
  Date: November 12, 2020
  Info: This is the main class which is hold all the Functions of the Attendance.
*/

class Attendance extends MY_Controller {

    public function __construct(){
    parent::__construct();
      $this->objOfS3 = new s3();
     //$this->objOfSlack = new Slack();
    $this->load->model('attendanceModel');
    $this->load->model('companyModel');
  
  }
   // 1. Enroll User Face After He Verified Successfully : Function Name = faceEnroll | Get User Face In base64 String And Save This To AWS Bucket
   public function _registerUserTab($request)
   {
 
    if(isset($request->phone) AND isset($request->emp_id) AND isset($request->name) AND isset($request->id)){
               
         $credential = array(
           'phone' => $request->phone,
           'emp_id' => $request->emp_id,
        );
        $credential2 = array('id' => $request->id);
           // $checkimei = $request->imei;
          $user = $this->attendanceModel->getUserwhere($credential);
          if($user == TRUE) { 
         
             $checkattendance = $this->attendanceModel->getUserLastRegisterData($credential);
             
         if(!empty($checkattendance->id)){
            
           $user_where = array(
             'emp_id' => $request->emp_id,
             'id' => $user->id
           ); 
           $users_where = array(
             'emp_id' => $request->emp_id,
             'user_id' => $user->id
           ); 
                 // $result = $this->attendanceModel->editRegister($user_data,$user_where); // Update users Table
                  $attendance = $this->attendanceModel->getUserLastRegisterData($users_where);
                  if($checkattendance->is_verified==0)
                  {
                    $contact_no = $request->phone;
                    if(preg_match('/^\d{10}$/',$contact_no)) // phone number is valid
                    {
                    $api_key = "45d11d6d-cde0-11e7-94da-0200cd936042";
                    $url = "https://2factor.in/API/V1/{$api_key}/SMS/+91{$contact_no}/AUTOGEN";
                    header('Content-type: application/json');
                    $api_data = json_decode(file_get_contents($url),true);
                    $data = array('otp_session_id'=>$api_data['Details']);
                    $otp_session_details = $api_data['Details'];

                    }else{
                    $attendance1 = array(
                    'status' => '0',
                    'message' => 'Sorry, Enter valid phone number!!');
                    $this->api->_sendError($attendance1);

                    }
                    $user_data = array( 
                      'otp_session_id' => $otp_session_details,
                      'is_register' => 1
                         );   
                  $user_where = array(
                      'emp_id' => $request->emp_id,
                      'id' => $user->id
                      ); 
                    $result = $this->attendanceModel->editRegister($user_data,$user_where); // Update users Table

                    $attendance = array(
                      'status' => '2',
                      'is_register' => '1',
                      'message' => 'Sorry, Please Verify Otp First!!');
                        $this->api->_sendError($attendance);
                   }
                 
                 else if($attendance->is_face_enroll == 0)
                  {
                   $attendance = array(
                     'status' => '3',
                     'is_register' => '1',
                     'message' => 'Sorry, Please Enroll Your Face First!!');
                       $this->api->_sendError($attendance);
                  }
                  else{
                            
                   return array(
                     'status' => '1',
                     'is_register' => '1',
                     'source_image'=>$attendance->source_image,
                     'source_image_url'=>$attendance->source_image_url,
                     'message' => 'User Registered Successfully.'
                   );
                  }
                 
              
           }
          
     
            else {
           
          // When User Not Register With Any Device
         $set_Data = array(
           'user_id'=>$user->id,
           'emp_id' => $user->emp_id,
           'phone' => $request->phone
         );
       
         //$template = 'Ezdat';
           $result = $this->attendanceModel->addAttendance($set_Data); 
                             // Set User Data
                            
                             $user_where = array(
                                 'emp_id' => $request->emp_id,
                                 'id' => $user->id
                                 ); 
                             $users_where = array(
                               'emp_id' => $request->emp_id,
                               'user_id' => $user->id
                           ); 
        
         $attendance = $this->attendanceModel->getUserLastRegisterData($users_where);
         $attendanceUserOtp = $this->attendanceModel->getUserwhere($user_where);
         if($attendance->is_verified == 0)
         {
          $contact_no = $request->phone;
          if(preg_match('/^\d{10}$/',$contact_no)) // phone number is valid
          {
          $api_key = "45d11d6d-cde0-11e7-94da-0200cd936042";
          $url = "https://2factor.in/API/V1/{$api_key}/SMS/+91{$contact_no}/AUTOGEN";
          header('Content-type: application/json');
          $api_data = json_decode(file_get_contents($url),true);
          $data = array('otp_session_id'=>$api_data['Details']);
          $otp_session_details = $api_data['Details'];

          }else{
          $attendance1 = array(
          'status' => '0',
          'message' => 'Sorry, Enter valid phone number!!');
          $this->api->_sendError($attendance1);

          }
          $user_data = array( 
            'otp_session_id' => $otp_session_details,
            'is_register' => 1
               );   
        $user_where = array(
            'emp_id' => $request->emp_id,
            'id' => $user->id
            ); 
          $result = $this->attendanceModel->editRegister($user_data,$user_where); // Update users Table
          $attendance = array(
            'status' => '2',
            'is_register' => '1',
            'message' => 'User Registered Successfully Please Verify Otp.');
              $this->api->_sendError($attendance);
         }
        else if($attendance->is_face_enroll == 0)
         {
           
          $attendance = array(
            'status' => '3',
            'is_register' => '1',
            'message' => 'Sorry, Please Enroll Your Face First!!');
              $this->api->_sendError($attendance);
         }
       
         
         else {
          $attendance =  array(
            'status' => '1',
            'is_register' => '1',
            'source_image'=>$attendance->source_image,
            'source_image_url'=>$attendance->source_image_url,
            'message' => 'User Registered Successfully.'
          );
          $this->api->_sendError($attendance); 
                  }
         
           }
        }
   else {
     $attendance = array('message' => 'Sorry, User Not Found In Our Records!!.');
     $this->api->_sendError($attendance);
   }
       
     }
     else
     {
       $attendance = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!');
     $this->api->_sendError($attendance);
       
     }
   
    }

  
  // 1. Enroll User Face After He Verified Successfully : Function Name = faceEnroll | Get User Face In base64 String And Save This To AWS Bucket
  public function _registerUser($request)
  {

   if(isset($request->phone) AND isset($request->emp_id) AND isset($request->name) AND isset($request->id)){
              
        $credential = array(
          'phone' => $request->phone,
          'emp_id' => $request->emp_id,
		   );
		   $checkimei = $request->imei;
		   $credential2 = array('id' => $request->id);
          // $checkimei = $request->imei;
         $user = $this->attendanceModel->getUserwhere($credential);
       if(isset($request->imei))
         $imei_query = $this->attendanceModel->getdataUserwhere($credential2);
         if($imei_query->imei_number != $checkimei)
					 {
						$attendance = array( 'status' => '1','message' => 'Sorry, New Device Detected! Please Contact Admin!!');
				        $this->api->_sendError($attendance);
           }
          
         if($user == TRUE) { 
        
         
        
            $checkattendance = $this->attendanceModel->getUserLastRegisterData($credential);
            
        if(!empty($checkattendance->id)){
          $otp = rand(100000,999999);     // Generate A 6 Digit OTP Number
          // Set User Data
          $user_data = array( 
            'otp' => $otp
          
          );   
          $user_where = array(
            'emp_id' => $request->emp_id,
            'id' => $user->id
          ); 
          $users_where = array(
            'emp_id' => $request->emp_id,
            'user_id' => $user->id
          ); 
                 $result = $this->attendanceModel->editRegister($user_data,$user_where); // Update users Table
                 $attendance = $this->attendanceModel->getUserLastRegisterData($users_where);
                 if($attendance->is_face_enroll == 0)
                 {
                  $attendance = array(
                    'status' => '3',
                    'is_register' => '1',
                    'otp'=>$otp,
                    'message' => 'Sorry, Please Enroll Your Face First!!');
                      $this->api->_sendError($attendance);
                 }
                 else{
                  return array(
                    'status' => '1',
                    'is_register' => '1',
                    'source_image'=>$attendance->source_image,
                    'source_image_url'=>$attendance->source_image_url,
                    'otp'=>$otp,
                    'message' => 'User Registered Successfully Please Verify Otp.'
                  );
                 }
                
             
          }
         
    
           else {
          
         // When User Not Register With Any Device
        $set_Data = array(
          'user_id'=>$user->id,
          'emp_id' => $user->emp_id,
          'phone' => $request->phone,
          'imei_number' => $request->imei
        );
          $result = $this->attendanceModel->addAttendance($set_Data); 
          $otp = rand(100000,999999);     // Generate A 6 Digit OTP Number
                            // Set User Data
                            $user_data = array( 
                                'otp' => $otp,
                                'is_register' => 1
              );   
              $user_where = array(
                                'emp_id' => $request->emp_id,
                                'id' => $user->id
                            ); 
                            $users_where = array(
                              'emp_id' => $request->emp_id,
                              'user_id' => $user->id
                          ); 
        $result = $this->attendanceModel->editRegister($user_data,$user_where); // Update users Table
        $attendance = $this->attendanceModel->getUserLastRegisterData($users_where);
                 if($attendance->is_face_enroll == 0)
                 {
                  $attendance = array(
                    'status' => '3',
                    'is_register' => '1',
                    'otp'=>$otp,
                    'message' => 'Sorry, Please Enroll Your Face First!!');
                      $this->api->_sendError($attendance);
                 }else{
                
                    return array(
                      'status' => '1',
                      'is_register' => '1',
                      'source_image'=>$attendance->source_image,
                      'source_image_url'=>$attendance->source_image_url,
                      'otp'=>$otp,
                      'message' => 'User Registered Successfully Please Verify Otp.'
                    );
                    
                 }
        
          }
       }
  else {
    $attendance = array('message' => 'Sorry, User Not Found In Our Records!!.');
    $this->api->_sendError($attendance);
  }
      
    }
    else
    {
      $attendance = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!');
    $this->api->_sendError($attendance);
      
    }
  
   }
    // resend otp for users when expire
    public function _resend_otp($request)
 {
        if(isset($request->user_id) AND isset($request->phone))
           {
            $credential = array('id' => $request->user_id);
            $user = $this->attendanceModel->getUserwhere($credential);
        
           if($user == TRUE){
            $contact_no = $request->phone;
          if(preg_match('/^\d{10}$/',$contact_no)) // phone number is valid
          {
          $api_key = "45d11d6d-cde0-11e7-94da-0200cd936042";
          $url = "https://2factor.in/API/V1/{$api_key}/SMS/+91{$contact_no}/AUTOGEN";
          header('Content-type: application/json');
          $api_data = json_decode(file_get_contents($url),true);
          $data = array('otp_session_id'=>$api_data['Details']);
          $otp_session_details = $api_data['Details'];

          }else{
          $attendance1 = array(
          'status' => '0',
          'message' => 'Sorry, Enter valid phone number!!');
          $this->api->_sendError($attendance1);

          }

          $user_data = array( 
            'otp_session_id' => $otp_session_details
               );   
        $user_where = array(
            'id' => $request->user_id
            ); 
          $result = $this->attendanceModel->editRegister($user_data,$user_where); // Update users Table
          if($result){  
            return array(
                    'status' => '1',
                      'message' => 'Otp Send  Successfully.'
                     );
                  }
              else{
              $attendance = array('message' => 'Sorry, Otp Not Sent. Please Try Again!!.');
              $this->api->_sendError($attendance);     
                          
              }
            }else{
              $attendance = array('message' => 'Sorry,  user Not Match!!.');
                    $this->api->_sendError($attendance); 
            }
                 
            }
        else
        {
          $attendance = array('message' => 'Sorry,  There is Some Parameter Missing. Please Try Again!!.');
          $this->api->_sendError($attendance);  
                  
        }
     }
   // Otp Verify Api
 public function _verify_otp($request)
 {
             if(isset($request->user_id) AND isset($request->otp))
           {
            $credential = array('id' => $request->user_id);
            $user = $this->attendanceModel->getUserwhere($credential);
        
           if($user == TRUE){
            $usercredetial= array('user_id'=>$user->id);
             
            $userenroll = $this->attendanceModel->getdataEnrollwhere($usercredetial);
           
            if($userenroll->is_verified == 1)
            {
        $attendance = array('message' => 'Sorry, You Have Already Verify otp!!.');
            $this->api->_sendError($attendance);  
            }
            $otp_session_id = $user->otp_session_id;
            $otp = $request->otp;
           
            $api_key = "45d11d6d-cde0-11e7-94da-0200cd936042";
           
            $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://2factor.in/API/V1/{$api_key}/SMS/VERIFY/{$otp_session_id}/{$otp}",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/x-www-form-urlencoded"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  $api_data = json_decode($err);
  
} else {
  $api_data = json_decode($response);
 
}
    
            if($api_data->Status=='Success'){ 
              if($api_data->Details=='OTP Expired')
              {
                $attendance = array('message' => 'Sorry, Otp Expired!!.');
                $this->api->_sendError($attendance);  
              }
              $user_where = array(
                'user_id' => $userenroll->user_id
                ); 
               $user_data = array( 
                                  'is_verified' => 1
                                 );   
                 
                               $resultset = $this->attendanceModel->editUserAttendance($user_data,$user_where); // Update users Table
                                 if($resultset){  
                                  return array(
                                          'status' => '1',
                                          'is_verified' => '1',
                                            'message' => 'Your Otp Verified  Successfully.'
                                           );
                                        }
                       else{
                    $attendance = array('message' => 'Sorry, Otp Not Verified. Please Try Again!!.');
                    $this->api->_sendError($attendance);     
                                  
                     }

            }else{
              $attendance = array('message' => 'Sorry,  Otp Not Match!!.');
                    $this->api->_sendError($attendance); 
            }
                  }
                  else
                  {
                    $attendance = array('message' => 'Sorry,  User Not Match!!.');
                    $this->api->_sendError($attendance);  
                                     
                  }
                     }
                  else
                 {
            $attendance = array('message' => 'Sorry,  There is Some Parameter Missing. Please Try Again!!.');
            $this->api->_sendError($attendance);  
                            
                  }
     }
 // face Enroll Api
  public function _userFaceEnroll($request)
  {
    if(isset($request->phone) AND isset($request->emp_id) AND isset($request->source_image) AND isset($request->status) AND isset($request->imei))
    {
      $phone = $request->phone;
      $empid = $request->emp_id;
      $checkimei = $request->imei;
      $source_image = $request->source_image;
      // Set Credential
      $credential = array('phone' => $phone,'emp_id' => $empid);
      $user = $this->attendanceModel->getdatawhere($credential);
      
                if($user == TRUE)
                {
                if($user->is_verified == 1)
                {   
                  if($request->status == 1) {
                   // Run When User Is Verified
                if($user->is_face_enroll == 1)  // Check Is There User Is Already Enroll His Face Or Not
                {
               $arrError =  array(
                                        'is_face_enroll' =>$user->is_face_enroll,
                                        'status' =>$user->status,
                                        'is_login' => 1,
                                        'first_register' => 1,
                                        'source_image' =>$user->source_image,
                                        'source_image_url' =>$user->source_image_url,
                                        'message' => 'You Are Already Enroll Your Face. Please Exit, And Try Again.!!.'
                                        );
                                        $this->api->_sendError($arrError);  
         
                                }
            if($user->imei_number != $checkimei)
					 {
						$attendance = array( 'status' => '1','message' => 'Sorry, New Device Detected! Please Contact Admin!!');
				      $this->api->_sendError($attendance);
           }
                      if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
                            {
                                $image_type = 'jpeg';       // Set Image Type To "JPEG"

                                $t=time();                  // Get Current Time
                                $name = $t.'.'.$image_type;      // Set Image Name

                                $file = base64_decode($source_image);       // Set File Using Decode Base64String
                                
                                $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function

                                $source_image = $name;          // Set Source Image Name
                                $source_image_url = $this->objOfS3->url($name);      // Set Source Image URL
                                
                                
                                if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
                                {
                                    // Set User Data
                                    $user_data = array( 
                                        'is_face_enroll' => 1,
                                        'is_login' => 1,
                                        'status' => $request->status,
                                        'first_register' => 1,
                                        'source_image' => $source_image,
                                        'source_image_url' => $source_image_url
                                    );             
                                    
                                    $user_where = array(
                                        'phone' => $phone
                                       
                                    );
                                    $result = $this->attendanceModel->editUserAttendance($user_data,$user_where);  // Update users Table
    
                                    if($result)
                                    {
                                       return array(
                                        'is_face_enroll' => 1,
                                        'is_login' => 1,
                                        'first_register' => 1,
                                        'source_image' => $source_image,
                                        'source_image_url' => $source_image_url,
                                        'status' => '1',
                                        'message' => 'EnrollMent Added Successfully.'
                                        );
                                    }
                                    else
                                    {
                    $attendance = array('message' => 'Sorry, Some Error Occured. Please Enroll Your Face Again!!.');
                            $this->api->_sendError($attendance);  
                                   
                                    }
                                }
                                else                                    // Run When Image Not Upload On AWS S3
                                {
                  $attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
                            $this->api->_sendError($attendance);  
                                }                                 
                            }
                            else                            // Run When source_image is NULL | source_image is Invalid
                            {
                $attendance = array('message' => 'Sorry, Source Image Is Invalid, Please Try Again!!.');
                $this->api->_sendError($attendance);  
                          
                            }
                      
                        }else{
                          $attendance = array('message' => 'Sorry, status is empty!!.');
                          $this->api->_sendError($attendance); 
                        }
                 }
      else
      {
    $attendance = array('message' => 'Sorry, You Are Not Verified Yet. Illegal Access, Please Try Again!!.');
    $this->api->_sendError($attendance); 
      }
      }
      else
      {
        $attendance = array('message' => 'Sorry, User Not Exists, Please Try Again And Register Again!!.');
            $this->api->_sendError($attendance);
           
      }
      }
      else
      {
      $attendance = array('message' => 'Sorry,  There is Some Parameter Missing. Please Try Again!!.');
      $this->api->_sendError($attendance);
     
    //  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
      }
  }
// userface Enroll Tab
// face Enroll Api
public function _userFaceEnrollTab($request)
{
  if(isset($request->phone) AND isset($request->emp_id) AND isset($request->source_image) AND isset($request->status))
  {
    $phone = $request->phone;
    $empid = $request->emp_id;
    $source_image = $request->source_image;
    // Set Credential
    $credential = array('phone' => $phone,'emp_id' => $empid);
    $user = $this->attendanceModel->getdatawhere($credential);
    
              if($user == TRUE)
              {
              if($user->is_verified == 1)
              {   
                if($request->status == 1) {
                 // Run When User Is Verified
              if($user->is_face_enroll == 1)  // Check Is There User Is Already Enroll His Face Or Not
              {
             $arrError =  array(
                                      'is_face_enroll' =>$user->is_face_enroll,
                                      'status' =>$user->status,
                                      'is_login' => 1,
                                      'first_register' => 1,
                                      'source_image' =>$user->source_image,
                                      'source_image_url' =>$user->source_image_url,
                                      'message' => 'You Are Already Enroll Your Face. Please Exit, And Try Again.!!.'
                                      );
                                      $this->api->_sendError($arrError);  
       
                              }
         
                    if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
                          {
                              $image_type = 'jpeg';       // Set Image Type To "JPEG"

                              $t=time();                  // Get Current Time
                              $name = $t.'.'.$image_type;      // Set Image Name

                              $file = base64_decode($source_image);       // Set File Using Decode Base64String
                              
                              $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function

                              $source_image = $name;          // Set Source Image Name
                              $source_image_url = $this->objOfS3->url($name);      // Set Source Image URL
                              
                              
                              if($source_image_url != '' OR $source_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
                              {
                                  // Set User Data
                                  $user_data = array( 
                                      'is_face_enroll' => 1,
                                      'is_login' => 1,
                                      'status' => $request->status,
                                      'first_register' => 1,
                                      'source_image' => $source_image,
                                      'source_image_url' => $source_image_url
                                  );             
                                  
                                  $user_where = array(
                                      'phone' => $phone
                                     
                                  );
                                  $result = $this->attendanceModel->editUserAttendance($user_data,$user_where);  // Update users Table
  
                                  if($result)
                                  {
                                     return array(
                                      'is_face_enroll' => 1,
                                      'is_login' => 1,
                                      'first_register' => 1,
                                      'source_image' => $source_image,
                                      'source_image_url' => $source_image_url,
                                      'status' => '1',
                                      'message' => 'EnrollMent Added Successfully.'
                                      );
                                  }
                                  else
                                  {
                  $attendance = array('message' => 'Sorry, Some Error Occured. Please Enroll Your Face Again!!.');
                          $this->api->_sendError($attendance);  
                                 
                                  }
                              }
                              else                                    // Run When Image Not Upload On AWS S3
                              {
                $attendance = array('message' => 'Source Image Not Uploaded, Please Try Again.!!.');
                          $this->api->_sendError($attendance);  
                              }                                 
                          }
                          else                            // Run When source_image is NULL | source_image is Invalid
                          {
              $attendance = array('message' => 'Sorry, Source Image Is Invalid, Please Try Again!!.');
              $this->api->_sendError($attendance);  
                        
                          }
                    
                      }else{
                        $attendance = array('message' => 'Sorry, status is empty!!.');
                        $this->api->_sendError($attendance); 
                      }
               }
    else
    {
  $attendance = array('message' => 'Sorry, You Are Not Verified Yet. Illegal Access, Please Try Again!!.');
  $this->api->_sendError($attendance); 
    }
    }
    else
    {
      $attendance = array('message' => 'Sorry, User Not Exists, Please Try Again And Register Again!!.');
          $this->api->_sendError($attendance);
         
    }
    }
    else
    {
    $attendance = array('message' => 'Sorry,  There is Some Parameter Missing. Please Try Again!!.');
    $this->api->_sendError($attendance);
   
  //  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
    }
}
// view Attendance Self user

public function _viewSelfAttendance($request)
{		
// In This Method Only Logged In User Is View His Attendance 
// There is No Functionality Like To View Other User

  /* Here we View the Leaves.. If User has the permission for that. */
  
  if(TRUE)
  {
    $user = $this->api->_GetTokenData();
    $user_id = $user['id'];
    $checkdate = date('Y-m-d');
    $where = array('multiple_markIn.date'=>$checkdate);
    $where2 = array('multiple_markIn.user_id'=>$user_id);
   // print_r($where2);
    $result = $this->attendanceModel->viewAttendanceWhereSelf($where,$where2);
    return $result;
  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }

}
 // total pesent today 
 
 public function _viewAttendanceTodayPresent($request)
 {		
 
 
   /* Here we View the Jobs.. If User has the permission for that. */

   if(TRUE)
   {
    
       $result = $this->attendanceModel->AttendancepresentToday();
       return $result;
    
   }
   else
   {
     $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
 
}
 // This is a Public API No Need To Check Token
 public function _viewAttendancePublic($request)
 {		
 $module_name = 'attendance';
 $permissions = $this->users->_checkPermission($module_name);
 foreach($permissions as $permission){}
 $attendance_where = array();
 
 if($permission->can_view == 'Y')
 {
   /* Here we View the Jobs.. If User has the permission for that. */

   if(TRUE)
   {
    $checkdate = date('Y-m-d');
    $where = array('attendance_mark.date'=>$checkdate);
       $result = $this->attendanceModel->viewAttendance();
       return $result;
    
   }
   else
   {
     $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
 }
 else
 {
   $this->api->_sendError("Sorry, You Have No Permission To View Attendance.");
 }
}

// view Attendance Filter
// This is a Public API No Need To Check Token
public function _viewAttendanceFilter($request)
{	
   
$module_name = 'attendance';
$permissions = $this->users->_checkPermission($module_name);
foreach($permissions as $permission){}
$attendance_where = array();

if($permission->can_view == 'Y')
{
  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
    if(!empty($request->start_date == 'all'))
				{
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
					$where = 'attendance_mark.user_id IN ('.implode(',',$user_ids).')';
				else
					$where = 'attendance_mark.user_id = 0';
						$result = $this->attendanceModel->viewAttendancefilterWhere($where);
					return $result;
        }
else{
    if(isset($request->start_date) AND isset($request->end_date))
    $startData = $request->start_date;
    $endData = $request->end_date;
   // $attendance_where['attendance_mark.user_id'] = $request->user_id;
    //SELECT * FROM attendance_mark WHERE date >='".$startData."' '2021-01-01' AND date <= '2021-01-14'
    //$attendance_where = 'date >= '".$startData."' AND date <= '".$endData."'';
    $query = $this->db->query("SELECT users.first_name,user_details.user_image,attendance_mark.date,attendance_mark.status,users.emp_id,users.email,attendance_mark.id,company_branches.branch_name,company_designations.designation_name FROM attendance_mark INNER join users on users.id = attendance_mark.user_id INNER JOIN company_branches on company_branches.id = users.branch_id INNER JOIN company_designations on company_designations.id = users.designation_id INNER join user_details on user_details.user_id = attendance_mark.user_id WHERE date >='".$startData."' AND date <= '".$endData."'");
      $arrTicketsData = $query->result();
    //$result = $this->attendanceModel->viewAttendancefilterWhere($attendance_where);
    return $arrTicketsData;

  
}
  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
}
else
{
  $this->api->_sendError("Sorry, You Have No Permission To View Attendance.");
}
}
public function getSundays($y,$m){ 
  $date = "$y-$m-01";
  $first_day = date('N',strtotime($date));
  $first_day = 7 - $first_day + 1;
  $last_day =  date('t',strtotime($date));
  $days = array();
  for($i=$first_day; $i<=$last_day; $i=$i+7 ){
      $days[$y."-".$m."-".$i] = 'Sunday Holiday';
  }
  return  $days;
}
public function getHolidays(){ 
  $query1 = $this->db->query("SELECT * FROM company_holidays WHERE MONTH(`holiday_date`) = MONTH(CURRENT_DATE()) AND YEAR(`holiday_date`) = YEAR(CURRENT_DATE())");
  $HolidayData = $query1->result();
$holida = array();
  foreach($HolidayData as $holiday)
  {
    $holida[$holiday->holiday_date] = 'Holiday';
    //$holida[]=$holiday->holiday_date;
  }
  return  $holida;
}
// view Attendance Filter
// This is a Public API No Need To Check Token
public function _viewAttendanceCalandar($request)
{	
   
  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
    $user = $this->api->_GetTokenData();
    $user_id = $user['id'];
    $query = $this->db->query("SELECT users.first_name,users.id,users.emp_id FROM users WHERE `role_id` NOT IN ('1')");
    $arrTicketsData = $query->result();
    $result = array();
//print_r($result);
    foreach($arrTicketsData as $employee) {
      $userdata['id']=$employee->id;
      $userdata['name']=$employee->first_name.', '.$employee->emp_id;
      $result[] = $userdata;
      
    }

   
  
   return $result;
     

  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
}

// view Attendance Filter
// This is a Public API No Need To Check Token
public function _viewAttendanceUserCalandar($request)
{	
   
  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
    $query = $this->db->query("SELECT * FROM `user_leave`");
    $arrTicketsData = $query->result();
   // $query2 = $this->db->query("SELECT * FROM `attendance_mark`");
    //$arrAttendanceData = $query2->result();
    $result = array();
    foreach($arrTicketsData as $row) {
    
      $userLeave['id'] = $row->id;
      $userLeave['text'] =  $row->reason;
      $userLeave['start'] = $row->start_date;
      $userLeave['end'] =  $row->end_date;
      $userLeave['resource'] = $row->user_id;
    $result[] = $userLeave;
      
    }
   
   return $result;
     

  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
}
// This is a Public API No Need To Check Token
public function _viewAttendanceSelfUser($request)
{	
   
$module_name = 'attendance';
$permissions = $this->users->_checkPermission($module_name);
foreach($permissions as $permission){}
$attendance_where = array();

if($permission->can_view == 'Y')
{
  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
    $user = $this->api->_GetTokenData();
       $user_id = $user['id'];
    $attendance_where['attendance_mark.user_id'] = $user_id;
    $result = $this->attendanceModel->viewAttendancefilterWhere($attendance_where);
    return $result;

  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
}
else
{
  $this->api->_sendError("Sorry, You Have No Permission To View Attendance.");
}
}

// view Attendance Self User Filrer 
// This is a Public API No Need To Check Token
public function _viewAttendanceSelfUserFilter($request)
{	
   
$module_name = 'attendance';
$permissions = $this->users->_checkPermission($module_name);
foreach($permissions as $permission){}
$attendance_where = array();

if($permission->can_view == 'Y')
{
  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
    if(!empty($request->start_date == 'all'))
				{
          $user = $this->api->_GetTokenData();
          $user_id = $user['id'];
          $query = $this->db->query("SELECT attendance_mark.id,users.first_name,user_details.user_image,attendance_mark.date,attendance_mark.status,users.emp_id,users.email,attendance_mark.id,company_branches.branch_name,company_designations.designation_name FROM attendance_mark INNER join users on users.id = attendance_mark.user_id INNER JOIN company_branches on company_branches.id = users.branch_id INNER JOIN company_designations on company_designations.id = users.designation_id INNER join user_details on user_details.user_id = attendance_mark.user_id WHERE attendance_mark.user_id = '".$user_id."' ORDER BY attendance_mark.id DESC");
          $result = $query->result();
          return $result;
        }
else{
   
    if(isset($request->start_date) AND isset($request->end_date))
    $startData = $request->start_date;
    $endData = $request->end_date;
    $user = $this->api->_GetTokenData();
    $user_id = $user['id'];
    $query = $this->db->query("SELECT attendance_mark.id,users.first_name,user_details.user_image,attendance_mark.date,attendance_mark.status,users.emp_id,users.email,attendance_mark.id,company_branches.branch_name,company_designations.designation_name FROM attendance_mark INNER join users on users.id = attendance_mark.user_id INNER JOIN company_branches on company_branches.id = users.branch_id INNER JOIN company_designations on company_designations.id = users.designation_id INNER join user_details on user_details.user_id = attendance_mark.user_id WHERE date >='".$startData."' AND date <= '".$endData."' AND attendance_mark.user_id = '".$user_id."' ORDER BY attendance_mark.id DESC");
      $result = $query->result();
    return $result;
   }
  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
}
else
{
  $this->api->_sendError("Sorry, You Have No Permission To View Attendance.");
}
}
// This is a Admin API Need To check Token
public function _viewAttendancePerDay($request)
{		
$module_name = 'attendance';
$permissions = $this->users->_checkPermission($module_name);
foreach($permissions as $permission){}
$attendance_where = array();

if($permission->can_view == 'Y')
{
  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
   
    if(isset($request->attendance_id))
      $attendance_where['multiple_markIn.attendance_mark_id'] = $request->attendance_id;
   
    if(!empty($attendance_where))
    {
      $result = $this->attendanceModel->viewAttendancePerDayWhere($attendance_where);
      return $result;
    }
    else
    {
      $result = $this->attendanceModel->viewAttendancePerDay();
      return $result;
    }
  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
}
else
{
  $this->api->_sendError("Sorry, You Have No Permission To View Attendance.");
}
}
// This is a Admin API Need To check Token
public function _viewAttendanceOnUser($request)
{		

  
  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
    $query = $this->db->query("SELECT attendance_mark.id,attendance_mark.user_id,users.first_name,attendance_mark.date FROM attendance_mark INNER JOIN users on attendance_mark.user_id=users.id  ORDER BY attendance_mark.id DESC");
      $result = $query->result();
      $attendaceDetails = array();
          foreach($result as $key => $attendance)
          {
              if(!isset($attendaceDetails[$attendance->user_id])){
                  $attendaceDetails[$attendance->date][$attendance->user_id] = 'p';
              }else{
                $attendaceDetails[$attendance->date][$attendance->user_id] = 'p';
              }
          }
  // for leave details in attendance
      $this->db->select('user_leave.id as leave_id,user_leave.user_id,user_leave.start_date, user_leave.end_date,leave_policy.policy_name');
      $this->db->from('user_leave');
      $this->db->join('leave_policy','leave_policy.id = user_leave.leave_policy_id');
      $query = $this->db->get()->result();
      foreach ($query as $key => $value)
       {
          $data_leave['leave_id'] = $value->leave_id;
          $data_leave['user_id'] = $value->user_id;
          $data_leave['start_date'] = $value->start_date;
          $data_leave['end_date'] = $value->end_date;
          $data_leave['policy_name'] = $value->policy_name;
          $result_holiday[] = $data_leave;
       }
       $query1 = $this->db->query("SELECT * FROM company_holidays WHERE MONTH(`holiday_date`) = MONTH(CURRENT_DATE()) AND YEAR(`holiday_date`) = YEAR(CURRENT_DATE())");
       $HolidayData = $query1->result();
     $holida = array();
       foreach($HolidayData as $holiday)
       {
         $holida[$holiday->holiday_date] = 'Holiday';
         //$holida[]=$holiday->holiday_date;
       }
       
      $merge = array_merge($attendaceDetails,$result_holiday,$holida);
      return $merge;
     
  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }

  
}
// view Visitors

// View Assets User
public function _viewVisitos($request)
{		

		/* Here we View the Assets.. If User has the permission for that. */

		if(TRUE)
		{
			if(isset($request->visitor_id))
				{
					$visitor_where['visitors.id'] = $request->visitor_id;
					$result = $this->attendanceModel->viewVisitorWhere($visitor_where);
					return $result;
				}
				else
				{
					$result = $this->attendanceModel->viewvisitor();
					return $result;
				}
				
	   }
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}

}
// This visitor meet form
public function _addVisitorsPerDay($request)
{		

  /* Here we View the Jobs.. If User has the permission for that. */
   
    if(isset($request->visitor_name) AND isset($request->phone) AND isset($request->email) AND isset($request->address) AND isset($request->purpose_visit) AND isset($request->person_id) AND isset($request->visitor_image))
   {

      $visitor_data = array(
        'visitor_name'=>$request->visitor_name,
        'phone'=>$request->phone,
        'address'=>$request->address,
        'purpose_visit'=>$request->purpose_visit,
        'email'=>$request->email,
        'person_id'=>$request->person_id
      );
      $visitor_dataNotification= array('user_id'=>$request->person_id,
                                       'sender_name'=>$request->visitor_name,
                                       'message'=>$request->visitor_name.''." wants To Meet You"
                                    );
      $resulttest = $this->attendanceModel->addVisitorsNotification($visitor_dataNotification);
      $source_image = $request->visitor_image;
      $image_type = 'jpeg';       // Set Image Type To "JPEG"
      $t=time();                  // Get Current Time
      $name = $t.'.'.$image_type;      // Set Image Nam
      $file = base64_decode($source_image);       // Set File Using Decode Base64String
      $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
      $source_image = $name;          // Set Source Image Name
      $source_image_url = $this->objOfS3->url($name);      // Set Source Image URL
        // Set User Data
        $visitor_data['visitors_image_url'] = $source_image_url;            
      $result = $this->attendanceModel->addVisitors($visitor_data);
      
      if($result){
        return array(
          'status' => '1',
          'message' => 'Thank You For visitiong Our office.'
        );
      }
      else
      {
        $attendance = array('message' => 'Sorry, Visitors Not Added. Please Try Again!!.');
        $this->api->_sendError($attendance);
      }
   }
  else
  {
    $attendance = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!.');
    $this->api->_sendError($attendance);
   
  }
}

 
  // face coamparision Attendance
  public function _faceComparision($request)
  {
    if(isset($request->source_image))
 
  {
  $source_image = $request->source_image;
    if($source_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
                            {
                                $image_type = 'jpeg';       // Set Image Type To "JPEG"

                                $t=time();                  // Get Current Time
                                $name = $t.'.'.$image_type;      // Set Image Name

                                $file = base64_decode($source_image);       // Set File Using Decode Base64String
                                
                                $exist_image = $this->objOfS3->exist($source_image);   // Call AWS Exist Image Function

                               
                                
                                
                                if($exist_image != '' OR $exist_image != NULL)        // Run When Image Successfully Upload On AWS S3
                                {
                //return = array('message' => 'Source Image Is Match Successfully.');
                return array(
                  'status' => '1',
                  'message' => 'Source Image Is Match Successfully.'
                ); 
                                }
                                else                                    // Run When Image Not Upload On AWS S3
                                {
                  $attendance = array('message' => 'Source Image Not Match, Please Try Again.!!.');
                            $this->api->_sendError($attendance);  
                                }                                 
                            }
                            else                            // Run When source_image is NULL | source_image is Invalid
                            {
                $attendance = array('message' => 'Sorry, Source Image Is Invalid, Please Try Again!!.');
                $this->api->_sendError($attendance);  
                          
                            }
    
  }
  else
  {
  $attendance = array('message' => 'Sorry, There is Some Perameter Missing. Please Try Again!!.');
  $this->api->_sendError($attendance);
 
  }
  }
   // mark Multiple Entry Attendance
   public function _markMultipleAttendance($request)
   {
   
     /* Here we Add the Attendance. If User has the permission for that. */
     if(isset($request->type) AND isset($request->long) AND isset($request->lat) AND isset($request->current_location) AND isset($request->branch_name))
     {
       $lat = $request->lat;
       $long = $request->long;
       $userBranch = $this->api->_GetTokenData();
     /*  $useridBranch = $userBranch['branch_id'];
      
       $office_where = array(  
         'id' => $useridBranch
         );
       $userbranch = $this->attendanceModel->getdatawhereBranch('company_branches',$office_where)->result();// check branch longitude And Latitude 
       foreach($userbranch as $office)
       {

           $point_check = $this->attendanceModel->distanceCalculation($office->branch_lat, $office->branch_long, $lat, $long);      // Function To Check Point Is In The Area 
           if($point_check)
           {
               break;
           }
           else
           {
               continue;
           }
       }*/
      /* if($point_check){*/
       $user = $this->api->_GetTokenData();
       $user_id = $user['id'];
       $emp_id = $user['emp_id'];
       $branch_id = $user['branch_id'];
       $designation_id = $user['designation_id'];
       $department_id = $user['department_id'];
       $attendanceData = array(
         'user_id' => $user_id,
         'modify_by' => $user_id
       );
       $enrollData = array(
         'user_id' => $user_id
       );
       $result2 = $this->attendanceModel->getUserLastRegisterData($enrollData);
       if($result2->is_face_enroll == 0)
       {
         $attendance = array('message' => 'Sorry,Please Enroll First!!.');
                     $this->api->_sendError($attendance); 
       }
       $checkTodayDate = date('Y-m-d');
       $checkTodayData = array(
        'user_id' => $user_id,
        'date' => $checkTodayDate
      );
       $checkDataDate = $this->attendanceModel->getUserDateData($checkTodayData);
      
       if(!empty($checkDataDate->id)){
       $arrDate=$checkDataDate->date;
      
       $checkTodayDate = date('Y-m-d');
       if($arrDate ==$checkTodayDate)
       {
        if($request->type == 0)       // If User Want To Mark IN
        {
          $attendanceData = array( 
                                'attendance_mark_id'=>$checkDataDate->id,
                                'user_id' => $user_id,
                                'final_status' => 0,
                                'emp_id' => $emp_id,
                                'in_lat' => $request->lat,
                                'in_long' => $request->long,
                                'in_time' => date('H:i:s'),
                                'current_location'=>$request->current_location,
                                'branch_name'=>$request->branch_name,
                                'date' => date('Y-m-d'),
                            );  
                          $result = $this->attendanceModel->addAttendanceMultiple($attendanceData);
                           if($result)
                           {
                            return array(
                            'status' => '1',
                            'message' => 'Attendance Marked Successfully.'
                               );
                           }
                         else
                           {
                            $attendance = array('message' => 'Sorry,  Attendance Not Marked. Please Try Again!!.');
                            $this->api->_sendError($attendance); 
                           }
                                
        }
        elseif($request->type == 1)     // If User Want To Mark OUT
        {  
         $user_where = array(  
           'final_status' => 0,
           'date' => date('Y-m-d'),
           'user_id'=>$user_id
           );
          // print_r($user_where);
         $userattendance = $this->attendanceModel->getdatawhereBranch('multiple_markIn',$user_where)->result();
       // print_r($userattendance);
        foreach($userattendance as $markinData){}
        $attendanceUpdate = array('id' => $markinData->id);
        $dataUpdate = array('final_status' =>1);
       
        $resultUpdate =  $this->attendanceModel->editAttendanceMarkIn($dataUpdate,$attendanceUpdate);
         $attendanceData = array( 
           'multiple_markin_id' => $markinData->id,
           'user_id' => $user_id,
           'emp_id' => $emp_id,
           'out_lat' => $request->lat,
           'current_location'=>$request->current_location,
           'branch_name'=>$request->branch_name,
           'out_long' => $request->long,
           'out_time' => date('H:i:s'),
           'date' => date('Y-m-d'),
       );  
     $result = $this->attendanceModel->addAttendanceMultipleOut($attendanceData);
      if($result)
      {
       return array(
       'status' => '1',
       'message' => 'Attendance Marked Out Successfully.'
          );
      }
    else
      {
       $attendance = array('message' => 'Sorry,  Attendance Not Marked. Please Try Again!!.');
       $this->api->_sendError($attendance); 
      }
        }
        else
        {
          $attendance = array('message' => 'Sorry, You Are Trying Worng Number, Please Try Again!!.');
          $this->api->_sendError($attendance); 
        }
       }else{
        $attendanceData = array( 
          'user_id' => $user_id,
          'emp_id' => $emp_id,
          'date' => date('Y-m-d'),
          'branch_id' => $branch_id,
          'designation_id' => $designation_id,
          'department_id' => $department_id,
      );  
        $result = $this->attendanceModel->addAttendanceDateData($attendanceData);
        $last_id = $this->db->insert_id();
       // print_r($last_id);
        if($request->type == 0)       // If User Want To Mark IN
        {
          $attendanceData = array( 
                                'attendance_mark_id'=>$last_id,
                                'user_id' => $user_id,
                                'final_status' => 0,
                                'emp_id' => $emp_id,
                                'in_lat' => $request->lat,
                                'current_location'=>$request->current_location,
                                'branch_name'=>$request->branch_name,
                                'in_long' => $request->long,
                                'in_time' => date('H:i:s'),
                                'date' => date('Y-m-d'),
                            );  
                          $result = $this->attendanceModel->addAttendanceMultiple($attendanceData);
                           if($result)
                           {
                            return array(
                            'status' => '1',
                            'message' => 'Attendance Marked Successfully.'
                               );
                           }
                         else
                           {
                            $attendance = array('message' => 'Sorry,  Attendance Not Marked. Please Try Again!!.');
                            $this->api->_sendError($attendance); 
                           }
                                
        }
        elseif($request->type == 1)     // If User Want To Mark OUT
        {  
         $user_where = array(  
           'final_status' => 0,
           'user_id'=>$user_id
           );
         $userattendance = $this->attendanceModel->getdatawhereBranch('multiple_markIn',$user_where)->result();
        foreach($userattendance as $markinData){}
        $attendanceUpdate = array('id' => $markinData->id);
        $dataUpdate = array('final_status' =>1);
       
        $resultUpdate =  $this->attendanceModel->editAttendanceMarkIn($dataUpdate,$attendanceUpdate);
         $attendanceData = array( 
           'multiple_markin_id' => $markinData->id,
           'user_id' => $user_id,
           'emp_id' => $emp_id,
           'out_lat' => $request->lat,
           'current_location'=>$request->current_location,
           'branch_name'=>$request->branch_name,
           'out_long' => $request->long,
           'out_time' => date('H:i:s'),
           'date' => date('Y-m-d'),
       );  
     $result = $this->attendanceModel->addAttendanceMultipleOut($attendanceData);
      if($result)
      {
       return array(
       'status' => '1',
       'message' => 'Attendance Marked Out Successfully.'
          );
      }
    else
      {
       $attendance = array('message' => 'Sorry,  Attendance Not Marked. Please Try Again!!.');
       $this->api->_sendError($attendance); 
      }
        }
        else
        {
          $attendance = array('message' => 'Sorry, You Are Trying Worng Number, Please Try Again!!.');
          $this->api->_sendError($attendance); 
        }
       }

       }else
       {
         $attendanceData = array( 
          'user_id' => $user_id,
          'emp_id' => $emp_id,
          'date' => date('Y-m-d'),
          'branch_id' => $branch_id,
          'designation_id' => $designation_id,
          'department_id' => $department_id,
      );  
        $result = $this->attendanceModel->addAttendanceDateData($attendanceData);
        $last_id = $this->db->insert_id();
        if($request->type == 0)       // If User Want To Mark IN
        {
          $attendanceData = array( 
                                'attendance_mark_id'=>$last_id,
                                'user_id' => $user_id,
                                'final_status' => 0,
                                'emp_id' => $emp_id,
                                'in_lat' => $request->lat,
                                'current_location'=>$request->current_location,
                                'branch_name'=>$request->branch_name,
                                'in_long' => $request->long,
                                'in_time' => date('H:i:s'),
                                'date' => date('Y-m-d'),
                            );  
                          $result = $this->attendanceModel->addAttendanceMultiple($attendanceData);
                           if($result)
                           {
                            return array(
                            'status' => '1',
                            'message' => 'Attendance Marked Successfully.'
                               );
                           }
                         else
                           {
                            $attendance = array('message' => 'Sorry,  Attendance Not Marked. Please Try Again!!.');
                            $this->api->_sendError($attendance); 
                           }
                                
        }
        elseif($request->type == 1)     // If User Want To Mark OUT
        {  
         $user_where = array(  
           'final_status' => 0,
           'user_id'=>$user_id
           );
         $userattendance = $this->attendanceModel->getdatawhereBranch('multiple_markIn',$user_where)->result();
        foreach($userattendance as $markinData){}
        $attendanceUpdate = array('id' => $markinData->id);
        $dataUpdate = array('final_status' =>1);
       
        $resultUpdate =  $this->attendanceModel->editAttendanceMarkIn($dataUpdate,$attendanceUpdate);
         $attendanceData = array( 
           'multiple_markin_id' => $markinData->id,
           'user_id' => $user_id,
           'emp_id' => $emp_id,
           'current_location'=>$request->current_location,
           'branch_name'=>$request->branch_name,
           'out_lat' => $request->lat,
           'out_long' => $request->long,
           'out_time' => date('H:i:s'),
           'date' => date('Y-m-d'),
       );  
     $result = $this->attendanceModel->addAttendanceMultipleOut($attendanceData);
      if($result)
      {
       return array(
       'status' => '1',
       'message' => 'Attendance Marked Out Successfully.'
          );
      }
    else
      {
       $attendance = array('message' => 'Sorry,  Attendance Not Marked. Please Try Again!!.');
       $this->api->_sendError($attendance); 
      }
        }
        else
        {
          $attendance = array('message' => 'Sorry, You Are Trying Worng Number, Please Try Again!!.');
          $this->api->_sendError($attendance); 
        }
       }
      

    /* }
     else{
      $userid = $this->api->_GetTokenData();
      $useridBranch = $userid['id'];
         $sendDataError = array( 
          'user_id' => $useridBranch,
          'current_lat' => $request->lat,
          'current_long' => $request->long,
          'current_location' => $request->current_location
      );  
    $result = $this->attendanceModel->addAttendancNotification($sendDataError);  
    $attendance = array('message' => 'Sorry, Location detected does not match with the prescribed location.!!.');
    $this->api->_sendError($attendance); 
     } */
     }
     else
     {
       $attendance = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!.');
       $this->api->_sendError($attendance); 
      
     }
   
   }
  // For Mark In and OUT Attendance 
  public function _markAttendance($request)
    {
    
      /* Here we Add the Attendance. If User has the permission for that. */
      if(isset($request->type) AND isset($request->long) AND isset($request->lat))
      {
        $lat = $request->lat;
        $long = $request->long;
        $userBranch = $this->api->_GetTokenData();
        $useridBranch = $userBranch['branch_id'];
       
        $office_where = array(  
          'id' => $useridBranch
          );
        $userbranch = $this->attendanceModel->getdatawhereBranch('company_branches',$office_where)->result();
        
        foreach($userbranch as $office)
        {

            $point_check = $this->attendanceModel->distanceCalculation($office->branch_lat, $office->branch_long, $lat, $long);      // Function To Check Point Is In The Area 
            if($point_check)
            {
                break;
            }
            else
            {
                continue;
            }
        }
        if($point_check){
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];
        $attendanceData = array(
          'user_id' => $user_id,
          'modify_by' => $user_id
        );
        $enrollData = array(
          'user_id' => $user_id
        );
        
          
          $credential = array('branch_lat'=> $request->lat,
                              'branch_long'=> $request->long
        );
        
        $result2 = $this->attendanceModel->getUserLastRegisterData($enrollData);
        if($result2->is_face_enroll == 0)
        {
          $attendance = array('message' => 'Sorry, Sorrry Please Enroll First!!.');
                      $this->api->_sendError($attendance);    
          
        }
        if(isset($request->in_image))
        {
            $in_image = $request->in_image;  
        }
        else if(isset($request->out_image))
        {
            $out_image = $request->out_image; 
        }
        else{
            $test1 = array($request->in_image );
        }
        
        $attendance_where = array(
          'user_id' => $user_id,
          'date' => date('Y-m-d')
        );
        $query2 = $this->attendanceModel->getAttendanceWhere($attendance_where);
  
        if($request->type == 0)       // If User Want To Mark IN
        {
          if($query2->num_rows() > 0)
          {
            // Show Error  
            $attendance = array( 'status' => '0','message' => 'Sorry, Attendance Already Marked On That Day. Or Please Try Again!!.');
            $this->api->_sendError($attendance);                     
          }
          else
          {
            if($in_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
            {
              $image_type = 'jpeg';       // Set Image Type To "JPEG"

              $t=time();                  // Get Current Time
              $name = $t.'.'.$image_type;      // Set Image Name

              $file = base64_decode($in_image);       // Set File Using Decode Base64String
              
              $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function

              $in_image = $name;          // Set Source Image Name
              $in_image_url = $this->objOfS3->url($name);      // Set Source Image URL
              if($in_image_url != '' OR $in_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
                                {
                    // Then It Will Mark IN || Insert Query RUN
                  $attendanceData = array( 
                    'user_id' => $user_id,
                                        'final_status' => $request->type,
                                        'in_lat' => $request->lat,
                                        'in_long' => $request->long,
                    'in_time' => date('H:i:s'),
                    'date' => date('Y-m-d'),
                                        'in_image' => $in_image,
                    'in_image_url' => $in_image_url,
                    'attendance_number' => $this->api->_getRandomString(8),
                                    );    
                }
                                else                                    // Run When Image Not Upload On AWS S3
                                {
                  // Show Error Dialog | Invalid source_image And Open Face Enroll Screen       
                  $attendance = array('message' => 'Source Image Not Uploaded, Please Try Again!!.');
                      $this->api->_sendError($attendance);              
                                }       

            }
            else                            // Run When source_image is NULL | source_image is Invalid
            {
              $attendance = array('message' => 'Sorry, Source Image Is Invalid, Please Try Again!!.');
              $this->api->_sendError($attendance);  
            
            }
          } 
        }
        elseif($request->type == 1)     // If User Want To Mark OUT
        {         
          if($query2->num_rows() > 0)
          {
            $row = $query2->row();
            if($row->final_status == 0)
            {
              if($out_image != NULL)     // Run When source_image is Not NULL | source_image is Valid
              {
                $image_type = 'jpeg';       // Set Image Type To "JPEG"
  
                $t=time();                  // Get Current Time
                $name = $t.'.'.$image_type;      // Set Image Name
  
                $file = base64_decode($out_image);       // Set File Using Decode Base64String
                
                $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
  
                $out_image = $name;          // Set Source Image Name
                $out_image_url = $this->objOfS3->url($name);      // Set Source Image URL
                if($out_image_url != '' OR $out_image_url != NULL)        // Run When Image Successfully Upload On AWS S3
                  {
                      // Then It Will Mark IN || Insert Query RUN
                    $attendanceData = array( 
                      'final_status' => $request->type,
                      'out_lat' => $request->lat,
                      'out_long' => $request->long,
                      'out_time' => date('H:i:s'),
                      'out_image' => $out_image,
                      'out_image_url' => $out_image_url,
                    );    
                  }
                  else                                    // Run When Image Not Upload On AWS S3
                  {
                    // Show Error Dialog | Invalid source_image And Open Face Enroll Screen       
                    $attendance = array('message' => 'Out Image Not Uploaded, Please Try Again!!.');
                    $this->api->_sendError($attendance);              
                  }       
              }
              else                            // Run When source_image is NULL | source_image is Invalid
              {
                $attendance = array('message' => 'Sorry, Source Image Is Invalid, Please Try Again!!.');
                $this->api->_sendError($attendance);  
              
              }    
            }
            else
            {
              $attendance = array( 'status' => '0','message' => 'Sorry, You Are Already Marked OUT, Please Try Again!!.');
              $this->api->_sendError($attendance); 
            
            }           
          }
          else
          {
            $attendance = array('message' => 'Sorry, You Have To Be Marked IN First, Please Try Again!!.');
            $this->api->_sendError($attendance); 

          }
        }
        else
        {
          $attendance = array('message' => 'Sorry, You Are Trying Worng Number, Please Try Again!!.');
          $this->api->_sendError($attendance); 
        }
        
        if($request->type == 0)
        {
          $result = $this->attendanceModel->addEnroll($attendanceData);
          if($result)
        {
          return array(
            'status' => '1',
            'message' => 'Attendance Marked Successfully.'
          );
        }
        else
        {
          $attendance = array('message' => 'Sorry,  Attendance Not Marked. Please Try Again!!.');
          $this->api->_sendError($attendance); 
        }
        }
        if(($request->type == 1))
        {
          $result = $this->attendanceModel->editAttendance($attendanceData,$attendance_where);
          if($result)
        {
          return array(
            'status' => '1',
            'message' => 'Attendance Marked Out Successfully.'
          );
        }
        else
        {
          $attendance = array('message' => 'Sorry, Attendance Not Marked. Please Try Again!!.');
          $this->api->_sendError($attendance); 
         
        }
        }

      }else{
        $attendance = array('message' => 'Sorry,   Location detected does not match with the prescribed location.!!.');
          $this->api->_sendError($attendance); 
       
      } 
      }
      else
      {
        $attendance = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!.');
        $this->api->_sendError($attendance); 
       
      }
    
    }
    // check attendance 

    public function _checkAttendance($request)
    {
      $module_name = 'attendance';
    $permissions = $this->users->_checkPermission($module_name);
    foreach($permissions as $permission){}
    
    if($permission->can_add == 'Y')
    {
      $user = $this->api->_GetTokenData();
        $user_id = $user['id'];
      $attendance_where = array(
        'user_id' => $user_id,
        'date' => date('Y-m-d')
      );
      $query2 = $this->attendanceModel->getAttendanceWhere($attendance_where);
     
        if($query2->num_rows() > 0)
        {
          // Show Error 
          $row = $query2->row();
          if($row->final_status==0){
          $attendance = array( 'final_status'=>$row->final_status,'message' => 'Sorry, Attendance Already Marked On That Day. Or Please Try Again!!.');
          $this->api->_sendError($attendance);  
        }
        else if($row->final_status == 1) 
        {
          $attendance = array( 'final_status'=>$row->final_status,'message' => 'Sorry, Attendance Already Marked On That Day. Or Please Try Again!!.');
          $this->api->_sendError($attendance);
        } 
        else{
          
        }                 
        }
      else{
      
        return array(
          'message' => 'Attendance Not Marked In.'
        );
    }
 
  }else{
      $attendance = array('message' => 'Sorry, You Have No Permission To check Attendance!!.');
      $this->api->_sendError($attendance); 
    }
    }
    // View Attendance Api
  public function _viewAttendance($request)
    {
    
  
      /* Here we Add the Branches.. If User has the permission for that. */
      $this->objOfSlack->username = 'CodeIgniter Test Bot';
      $this->objOfSlack->channel  = ['general', 'random'];
      if ($this->objOfSlack->send('This is a test notification from the CI Slack Bot.')) {
        print_r($this->objOfSlack->output); // Print the response from Slack if you want.
      } else {
        print_r($this->objOfSlack->error); // This will output the error.
      }
  }
  // check imei number
  public function _checkImeiAvailability($request)
  {   
    $module_name = 'attendance';
    $permissions = $this->users->_checkPermission($module_name);
    foreach($permissions as $permission){}
    
    if($permission->can_add == 'Y')
    {
      /* Here we check the email. If User has the permission for that. */

      if(isset($request->imei) AND isset($request->phone))
      {
        $where_Imei = array(
          'imei_number' => $request->imei,
          'phone' => $request->phone
        );

        $result = $this->attendanceModel->checkImeiAvailability($where_Imei);
        if($result){
          $attendance = array('message' => 'Sorry, Please Contact To Admin!!.');
                $this->api->_sendError($attendance);  
            }
        else{
          
          return array(
            'message' => 'Imei Match Success Fully.'
          );
            
        }
      }
      else
      {
        $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
      }
    }
    else
    {
      $this->api->_sendError("Sorry, You Have No Permission To Add Attendance.");
    }
  }
  // Edit Company
  public function _editCompany($request)
  {		
    
	  $company = array();
	  $module_name = 'company';
	  $permissions = $this->users->_checkPermission($module_name);
	  foreach($permissions as $permission){}
	  $company_where = array();
	  
	  if($permission->can_edit == 'Y')
	  {
		  /* Here we Edit the company.. If User has the permission for that. */

		  if(isset($request->company_id))
		  {
			  $company_where = array(
				  'id' => $request->company_id
			  );
        if(isset($request->company_logo))

        $baseimage = $request->company_logo;
        if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
       {
        $source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
        
          
          $image_type = 'jpeg';       // Set Image Type To "JPEG"
          $t=time();                  // Get Current Time
          $name = $t.'.'.$image_type;      // Set Image Nam
          $file = base64_decode($source_image);       // Set File Using Decode Base64String
          $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
          $source_image = $name;          // Set Source Image Name
          $source_image_url = $this->objOfS3->url($name);      // Set Source Image URL
           // Set User Data
           $company = array( 
             'company_logo' => $source_image_url
           ); 
         }
            else{
       $query = $this->db->query("SELECT * FROM company");
         $arrTicketsData = $query->row();
        // print_r($arrTicketsData);
             $company = array( 
               'company_logo' => $arrTicketsData->company_logo
             ); 
            // print_r($company);
            }
			  if(isset($request->company_name))
				  $company['company_name'] = $request->company_name;
				if(isset($request->contactperson))
				  $company['contactperson'] = $request->contactperson;
			   if(isset($request->owner_name))
				  $company['owner_name'] = $request->owner_name;
			  if(isset($request->phone_no))
				  $company['phone_no'] = $request->phone_no;
			  if(isset($request->company_email))
				  $company['company_email'] = $request->company_email;
			 if(isset($request->company_address))
				  $company['company_address'] = $request->company_address;
			  if(isset($request->company_url))
				  $company['api_link'] = $request->api_link;
			  if(isset($request->state))
				  $company['state'] = $request->state;
			  if(isset($request->country_name))
				  $company['country_name'] = $request->country_name;
			  if(isset($request->city))
				  $company['city'] = $request->city;
			  if(isset($request->postal_code))
				  $company['postal_code'] = $request->postal_code;

         

			  $result = $this->companyModel->editcompany($company,$company_where);
			  if($result)
			  {
				  return array(
					  'status' => '1',
					  'message' => 'company Updated Successfully.'
				  );
			  }
			  else
			  {
				  $this->api->_sendError("Sorry, Company Not Updated. Please Try Again!!");
			  }
		  }
		  else
		  {
			  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		  }
	  }
	  else
	  {
		  $this->api->_sendError("Sorry, You Have No Permission To Edit company.");
	  }
  }

  // Edit Employee Details
 public function _editDetails($request)
 {
  //print_r($request);
 $userDetails = array();
 $module_name = 'attendance';
 $permissions = $this->users->_checkPermission($module_name);
 foreach($permissions as $permission){}
 $user_where = array();
 if($permission->can_edit == 'Y')
 {
   /* Here we Update/Edit the Employee.. If User has the permission for that. */
       
   if(isset($request->user_id))
   {
     $user_where = array(
       'user_details.user_id' => $request->user_id
     );
     
     $where_user = array(
      'users.id' => $request->user_id
    );

     //  $status = $this->users->_checkfirst_nameAvailability($request);
     if(isset($request->first_name))
     $user['first_name'] = $request->first_name;
     if(isset($request->phone))
     $user['phone'] = $request->phone;
     if(isset($request->department_id))
     $user['department_id'] = $request->department_id;
    if(isset($request->designation_id))
    $user['designation_id'] = $request->designation_id;
    if(isset($request->branch_id))
     $user['branch_id'] = $request->branch_id;
  
$resulttest = $this->attendanceModel->editUserPersonal($user,$where_user);  // Update users Table
    
     if(isset($request->user_image))

     $baseimage = $request->user_image;
     if($baseimage != NULL)     // Run When source_image is Not NULL | source_image is Valid
    {
     $source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
     
       
       $image_type = 'jpeg';       // Set Image Type To "JPEG"
       $t=time();                  // Get Current Time
       $name = $t.'.'.$image_type;      // Set Image Nam
       $file = base64_decode($source_image);       // Set File Using Decode Base64String
       $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
       $source_image = $name;          // Set Source Image Name
       $source_image_url = $this->objOfS3->url($name);      // Set Source Image URL
        // Set User Data
        $userDetails = array( 
          'user_image' => $source_image_url
        ); 
      }
         else{
          $usertr_where = $request->user_id;
    $query = $this->db->query("SELECT * FROM user_details WHERE user_id ='".$usertr_where."'");
      $arrTicketsData = $query->row();
     // print_r($arrTicketsData);
          $userDetails = array( 
            'user_image' => $arrTicketsData->user_image
          ); 
         // print_r($userDetails);
         }
        
          // user Table Edit Details
     if(isset($request->father_name))
     $userDetails['father_name'] = $request->father_name;
     
    if(isset($request->dob))
    $userDetails['dob'] = $request->dob;
    if(isset($request->joining_date))
     $userDetails['joining_date'] = $request->joining_date;
   if(isset($request->personal_email))
     $userDetails['personal_email'] = $request->personal_email;
   if(isset($request->address))
     $userDetails['address'] = $request->address;
   if(isset($request->gender))
     $userDetails['gender'] = $request->gender;           
         $result = $this->attendanceModel->editUserDetails($userDetails,$user_where);  // Update users Table

         if($result)
         {
            return array(
           'status' => '1',
           'message' => 'Employee Edit SuccessFully  Successfully.'
           );
         }
         else if($resulttest)
         {
          return array(
            'status' => '1',
            'message' => 'Employee Edit SuccessFully  Successfully.'
            );

          
         } else{
          $this->api->_sendError("Sorry, Employee Not Edit. Please Try Again!!");
        }
                                      
    
     
   }
   else
   {
     $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
 }
 else
 {
   $this->api->_sendError("Sorry, You Have No Permission To Edit Employee.");
 }
 }


 public function _addUser($request)
{
	
	/* Here we Add the users.. If User has the permission for that. */

		if(isset($request->first_name) AND isset($request->branch_id) AND isset($request->designation_id) AND isset($request->department_id) AND isset($request->email) AND isset($request->joining_date) AND isset($request->dob) AND isset($request->password) AND isset($request->confirm_password) AND isset($request->joining_date))
		{
			$passcheck = $request->password;
				// Validate password strength
		  $uppercase = preg_match('@[A-Z]@', $passcheck);
		  $lowercase = preg_match('@[a-z]@', $passcheck);
		  $number    = preg_match('@[0-9]@', $passcheck);
		  $specialChars = preg_match('@[^\w]@', $passcheck);
		  
		  if(!$uppercase || !$lowercase || !$number || !$specialChars) {
			
				  $this->api->_sendError(" Sorry, include at least one upper case letter, one number, and one special character in Password.");
		  }else if(strlen($passcheck) < 6){
			  $this->api->_sendError("Sorry, Password should be at least 6 characters in length!!");
		  }
			if($request->password != $request->confirm_password)
			{
				$this->api->_sendError("Password Mismatch.");
			}
			$leaveStartDate=strtotime($request->joining_date);
			$dayOfStartLeave = date("l", $leaveStartDate);
		
			$leaveEndDate=strtotime($request->dob);
			$dayOfEndLeave = date("l", $leaveEndDate);
		
			$currentDate = date('d-m-Y');
			$currentDate=strtotime($currentDate);
			if($leaveEndDate > $currentDate){
				$this->api->_sendError('Dob Cannot Be Add On Future Date.');
			}
			if($leaveStartDate > $currentDate){
				$this->api->_sendError('Joining Date Cannot Be Add On Future Date.');
			}
            $status = $this->users->_checkfirst_nameAvailability($request);
            $status2 = $this->users->_checkEmailAvailability($request);
		
		//--------- No Need To Add Anything in this Array
		 $userCode=  $this->api->_getRandomString(6);
			$users = array(
				'first_name' => $request->first_name,
				'email' => $request->email,
        'leave_policy_id'=>0,
        'department_id' => $request->department_id,
				'designation_id' => $request->designation_id,
        'branch_id'=>$request->branch_id,		
        'parent_id'=>0,
        'device_token'=>'web',
        'phone'=>$request->phone,
				'user_code' => $userCode,
				'password' => md5($request->email.''.md5($request->password)),
				'is_active' => 1
			);
			
        $user_id = $this->userModel->insertData('users',$users);
        	
			//$last_id = $this->db->insert_id();
			$empId = 'EMP'.$user_id;
			$data2['emp_id'] = $empId;
      $this->db->where('id',$user_id);
       
			$this->db->update('users',$data2);
			
			if($user_id == 0)
				$this->api->_sendError("Something Went Wrong. Please Try Again!!");
				$users_details = array(
					'user_id' => $user_id,
					'dob'=> $request->dob,
					'joining_date'=>$request->joining_date
				);
			if(isset($request->father_name))
				$users_details['father_name'] = $request->father_name;
			if(isset($request->personal_email))
				$users_details['personal_email'] = $request->personal_email;
			if(isset($request->gender))
				$users_details['gender'] = $request->gender;
			if(isset($request->address))
        $users_details['address'] = $request->address;
        // user Image Uoload S3 Bucket
        if(isset($request->user_image))
          $baseimage = $request->user_image;
          $source_image = str_replace('data:image/jpeg;base64,','',$baseimage);
          $image_type = 'jpeg';       // Set Image Type To "JPEG"
          $t=time();                  // Get Current Time
          $name = $t.'.'.$image_type;      // Set Image Nam
          $file = base64_decode($source_image);       // Set File Using Decode Base64String
          $uploaded_image = $this->objOfS3->upload($name, $file, $image_type);   // Call AWS Upload Image Function
          $source_image = $name;          // Set Source Image Name
          $source_image_url = $this->objOfS3->url($name);      // Set Source Image URL
            // Set User Data
          $users_details['user_image'] = $source_image_url;            
          $userExperience_result = $this->userModel->insertData('user_details',$users_details);
          
                                        
    
       //------- user onboarding status Document
      $onboardingstatus = array(
        'user_id' => $user_id,
        'status' =>0
      	);
	
	 $userExperience_result = $this->userModel->insertData('onboarding_status',$onboardingstatus);

   $payrollstatus = array(
		'user_id' => $user_id,
		'status' =>0
	);
  $payroll_result = $this->userModel->insertData('payroll_status',$payrollstatus);
	  //------- user Offboarding status Document
	  $onffboardingstatus = array(
		'user_id' => $user_id,
		'status' =>0
	);
	 $userofffboarding_result = $this->userModel->insertData('offboarding_status',$onboardingstatus);
   $user_personal['user_id'] = $user_id;
   if(isset($request->passport_no))
						$user_personal['passport_no'] = $request->passport_no;
					if(isset($request->passport_expiry))
						$user_personal['passport_expiry'] = $request->passport_expiry;
					if(isset($request->nationality))
						$user_personal['nationality'] = $request->nationality;
					if(isset($request->religion))
						$user_personal['religion'] = $request->religion;
					if(isset($request->marital_status)) 
						$user_personal['marital_status'] = $request->marital_status;
					if(isset($request->no_of_children))
						$user_personal['no_of_children'] = $request->no_of_children;
					if(isset($request->contactperson_name))
						$user_personal['contactperson_name'] = $request->contactperson_name;
					if(isset($request->contactperson_no))
						$user_personal['contactperson_no'] = $request->contactperson_no;
					if(isset($request->relationship))
						$user_personal['relationship'] = $request->relationship;
					$resultp = $this->userModel->insertData('user_personal',$user_personal);
			if($resultp != 0)
			{
				return array(
					'status' => '1',
					'message' => 'Employee Added Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, User Not Added. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Required Parameter Missing. Please Try Again!!");
		}
}


// view Attendance For Users Enroll Data & This is a Admin API Need To check Token
public function _viewEnrollAttendance($request)
{		

  /* Here we View the Jobs.. If User has the permission for that. */

  if(TRUE)
  {
   
    
      $result = $this->attendanceModel->getUserallRegister();
      return $result;
    
  }
  else
  {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }

}





}
