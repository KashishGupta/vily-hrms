<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: AttendanceModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Attendance.
*/

class AttendanceModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function addAttendancNotification($data)
    {
        $this->db->insert('attendance_notification',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function addAttendance($data)
    {
        $this->db->insert('enroll_attendance',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function addVisitorsNotification($data)
    {
        $this->db->insert('user_notification',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    
    public function addVisitors($data)
    {
        $this->db->insert('visitors',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    // add atrtendance Multiple In
    public function addAttendanceMultiple($data)
    {
        $this->db->insert('multiple_markIn',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    // Edit Mark In Attendance
    public function editAttendanceMarkIn($data,$where)
    {
        $this->db->where($where);
        $this->db->update('multiple_markIn',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

     // add atrtendance Multiple Out
     public function addAttendanceMultipleOut($data)
     {
         $this->db->insert('multiple_markOut',$data);
         $insert_id = $this->db->insert_id();
         if($insert_id != 0)
         {
             return TRUE;
         }
         else    
         {
             return FALSE;
         }
     }
    
     public function insertData($table,$data)
     {
         $this->db->insert($table,$data);
         $insert_id = $this->db->insert_id();
         if($insert_id != 0)
         {
             return $insert_id;
         }
         else    
         {
             return 0;
         }
     }
    public function editUserPersonal($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function editUserDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_details',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function addEnroll($data)
    {
        $this->db->insert('attendances',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function addAttendanceData($data)
    {
        $this->db->insert('users',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
  
    public function getUserwhere($credential) {
        // Check The Credentials
        $query = $this->db->get_where('users' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('users');
          
            $this->db->where('users.id', $row->id);
        
           $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
   
     //  check Imei Number Available Or Not
     function checkImeiAvailability($where_Imei)
    {
        $query = $this->db->get_where('enroll_attendance',$where_Imei);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }
 
     function getdataImeiwhere($table_name,$where){
        $this->db->order_by("id", "desc");
        $query = $this->db->get_where($table_name , $where);
        return $query;
    }
// Get Data Attendance
public function getdataUserwhere($credential2) {
    // Check The Credentials
    $query = $this->db->get_where('users' , $credential2);
    // If User Exists
    if ($query->num_rows() > 0) {

        $row = $query->row();
        
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users.id', $row->id);
        $user = $this->db->get()->row();

        return $user;
    }
    else{
        return FALSE;
    }
}
// Get Data Attendance
public function getdataEnrollwhere($credential) {
    // Check The Credentials
    $query = $this->db->get_where('enroll_attendance' , $credential);
    // If User Exists
    if ($query->num_rows() > 0) {

        $row = $query->row();
        
        $this->db->select('*');
        $this->db->from('enroll_attendance');
        $this->db->where('enroll_attendance.id', $row->id);
        $user = $this->db->get()->row();

        return $user;
    }
    else{
        return FALSE;
    }
}
       // Get Data where  
    public function getdatawhere($credential) {
        // Check The Credentials
        $query = $this->db->get_where('enroll_attendance' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('enroll_attendance');
            $this->db->where('enroll_attendance.id', $row->id);
            $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }

      // Get Data where  Branch
      function getdatawhereBranch($table_name,$where){
        $this->db->order_by("id", "desc");
        $query = $this->db->get_where($table_name , $where);
        return $query;
    }
     
    public function getUserLastRegisterData($where)
    {
       
        $this->db->select('*');
        $this->db->from('enroll_attendance'); 
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->row();
       
        
    }


    public function getUserallRegister()
    {
       
        $this->db->select('enroll_attendance.source_image,enroll_attendance.source_image_url');
        $this->db->from('enroll_attendance'); 
       // $this->db->where($where); 
        $query = $this->db->get();
        return $query->result();
       
        
    }
// check attendance date 

public function addAttendanceDateData($data)
{
    $this->db->insert('attendance_mark',$data);
    $insert_id = $this->db->insert_id();
    if($insert_id != 0)
    {
        return TRUE;
    }
    else    
    {
        return FALSE;
    }
}
    public function getUserDateData($where)
    {
       
        $this->db->select('*');
        $this->db->from('attendance_mark'); 
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->row();
       
        
    }
    public function editUserAttendance($data,$where)
    {
        $this->db->where($where);
        $this->db->update('enroll_attendance',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function editRegister($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function deleteAttendance($where_attendance)
    {
        $this->db->where($where_leave);
        $this->db->delete('user_leave');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editAttendance($data,$where)
    {
        $this->db->where($where);
        $this->db->update('attendances',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function AttendancepresentToday()
    {
        
        $query = $this->db->query("SELECT COUNT(*) as presentEmployee FROM attendance_mark WHERE attendance_mark.date=date(now()) AND attendance_mark.status = 0

        ");
        $arrAttendanceData = $query->result();
        
        return $arrAttendanceData;
    }
    public function viewAttendance()
    {
        
        $query = $this->db->query("SELECT users.emp_id,users.first_name,users.email,attendance_mark.id,attendance_mark.status,user_details.user_image FROM users INNER JOIN user_details on user_details.user_id = users.id LEFT JOIN attendance_mark on users.id = attendance_mark.user_id and attendance_mark.date=date(now()) WHERE `role_id` NOT IN ('1')

        ");
        $arrAttendanceData = $query->result();
        
        return $arrAttendanceData;
    }

    //view Visitor Where
    public function viewVisitorWhere($where)
    {
        $this->db->select('visitors.visitor_name,visitors.phone as visitorphone,visitors.email as visitor_email,visitors.address as visitor_address,
        visitors.purpose_visit,visitors.person_id,visitors.visitors_image_url,user_details.user_image,users.email,users.first_name,company_designations.designation_name,
        company_departments.department_name,company_branches.branch_name,users.phone,visitors.creted_at');
        $this->db->from('visitors');
        $this->db->join('users', 'users.id = visitors.person_id');
        $this->db->join('user_details', 'user_details.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->where($where);
        $query = $this->db->get(); 
        return $query->result();
    }

     //view Visitor
     public function viewvisitor()
     {
        $this->db->select('visitors.visitor_name,visitors.phone as visitorphone,visitors.email as visitor_email,visitors.address as visitor_address,
        visitors.purpose_visit,visitors.person_id,visitors.visitors_image_url,user_details.user_image,users.email,users.first_name,company_designations.designation_name,
        company_departments.department_name,company_branches.branch_name,users.phone,visitors.creted_at');
         $this->db->from('visitors');
         $this->db->join('users', 'users.id = visitors.person_id');
         $this->db->join('user_details', 'user_details.user_id = users.id');
         $this->db->join('company_designations','company_designations.id = users.designation_id');
         $this->db->join('company_departments','company_departments.id = users.department_id');
         $this->db->join('company_branches','company_branches.id = users.branch_id');
         $query = $this->db->get(); 
         return $query->result();
     }

    public function viewAttendancefilter()
    {
       
        $this->db->select('users.first_name,user_details.user_image,attendance_mark.date,attendance_mark.status,users.emp_id,users.email,attendance_mark.id,company_branches.branch_name,company_designations.designation_name');
        $this->db->from('users');
        $this->db->join('attendance_mark', 'attendance_mark.user_id = users.id', 'left');
        $this->db->join('company_branches', 'company_branches.id = users.branch_id');
        $this->db->join('company_designations', 'company_designations.id = users.designation_id');
        $this->db->join('user_details', 'user_details.user_id = users.id');
        $this->db->where();
        $query = $this->db->get(); 
        return $query->result();
    }
    public function viewAttendancefilterWhere($where)
    {
      
        $this->db->select('users.first_name,user_details.user_image,attendance_mark.date,attendance_mark.status,users.emp_id,users.email,attendance_mark.id,company_branches.branch_name,company_designations.designation_name');
        $this->db->from('attendance_mark');
        $this->db->join('users', 'users.id = attendance_mark.user_id');
        $this->db->join('company_branches', 'company_branches.id = users.branch_id');
        $this->db->join('company_designations', 'company_designations.id = users.designation_id');
        $this->db->join('user_details', 'user_details.user_id = attendance_mark.user_id');
        $this->db->where($where);
        $query = $this->db->get(); 
        return $query->result();
    }

    public function viewAttendanceWhere($where)
    {
        
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('company_branches', 'company_branches.id = users.branch_id');
        $this->db->join('company_designations', 'company_designations.id = users.designation_id');
     
        $this->db->where($where);
        $query = $this->db->get(); 
        return $query->result();
    }
    
    public function viewAttendancePerDayWhere($where)
    {
       $this->db->select('users.first_name,user_details.user_image,users.email,users.emp_id,multiple_markIn.in_time,multiple_markIn.date,multiple_markOut.out_time');
       $this->db->from('multiple_markIn');
       $this->db->join('multiple_markOut', 'multiple_markOut.multiple_markin_id = multiple_markIn.id', 'left');
       $this->db->join('users', 'users.id = multiple_markIn.user_id');
       $this->db->join('user_details', 'user_details.user_id = multiple_markIn.user_id');
       $this->db->where($where);
       $query = $this->db->get(); 
       return $query->result();
    }
    public function viewAttendancePerDay()
    {
       $this->db->select('users.first_name,user_details.user_image,users.email,users.emp_id,multiple_markIn.in_time,multiple_markIn.date,multiple_markOut.out_time');
       $this->db->from('multiple_markIn');
       $this->db->join('multiple_markOut', 'multiple_markOut.multiple_markin_id = multiple_markIn.id', 'left');
       $this->db->join('users', 'users.id = multiple_markIn.user_id');
       $this->db->join('user_details', 'user_details.user_id = multiple_markIn.user_id');
       $query = $this->db->get(); 
       return $query->result();
    }

    public function viewAttendanceWhereSelf($where,$where2)
    {
       $this->db->select('users.first_name,multiple_markIn.in_time,multiple_markIn.date,multiple_markOut.out_time');
       $this->db->from('multiple_markIn');
       $this->db->join('multiple_markOut', 'multiple_markOut.multiple_markin_id = multiple_markIn.id', 'left');
       $this->db->join('users', 'users.id = multiple_markIn.user_id');
       $this->db->where($where);
       $this->db->where($where2);
       $query = $this->db->get(); 
       return $query->result();
    }

    public function userHasLeaveType($where)
    {
        $query = $this->db->get_where('user_leave_details',$where);
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function getNumberOfLeaveByType($where)
    {
        $this->db->select('no_of_leave');
        $this->db->from('company_leave_types');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function insertUserLeaveType($data)
    {
        $this->db->insert('user_leave_details',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function getAttendanceWhere($where)
    {
        $query = $this->db->get_where('attendances', $where);
        return $query;
    }

    // distance Calculation
    function distanceCalculation($lat1, $lon1, $lat2, $lon2, $unit = 'K', $decimals = 3) 
    {
        $oddNodes = false;

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") 
        {
            $final_dis = ($miles * 1.609344);
        } 
        else if ($unit == "N") 
        {
            $final_dis = ($miles * 0.8684);
        } 
        else 
        {
            $final_dis = $miles;
        }

        $final_dis = $final_dis * 1000;
        //echo $final_dis."|";

        if($final_dis <= 500.000)
        {
            $oddNodes = !$oddNodes;
            return $oddNodes;
        }
        else
        {
            return $oddNodes;
        }
      
    }

}