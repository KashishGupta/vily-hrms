<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
  Project: Willy HRMS
  Author:Pratap Singh
  File Name: EventsModel.php
  Date: November 29, 2019
  Info: This is the main class which is hold all the Models of the Events.
*/

class EventsModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function addEvent($data)
    {
        $this->db->insert('events',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteEvent($where_event)
    {
        $this->db->where($where_event);
        $this->db->delete('events');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editEvent($data,$where)
    {
        $this->db->where($where);
        $this->db->update('events',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewEvent()
    {
        $usercompany = $this->api->_GetTokenData();
		
      $this->db->select('company_holidays.id as holiday_id, company_holidays.holiday_title, company_holidays.holiday_date, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location');
        $this->db->from('company_holidays');
        $this->db->join('company_branches', 'company_branches.id = company_holidays.branch_id');
       $company_holidays = $this->db->get()->result();
       

       $this->db->select('users.id,users.first_name,user_details.dob' );
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
      $userData = $this->db->get()->result();

      $get['result'] = $this->db->get("events")->result();
      
      foreach ( $userData as $key => $row) {
            $d = date('d', strtotime($row->dob));
            $m =  date("m", strtotime($row->dob));
            $yearBegin = date("Y");
            $yearEnd = $yearBegin + 10; 
            $years = range($yearBegin, $yearEnd, 1);

           foreach($years as $year){
            $date = $year.'-'.$m.'-'.$d;
            $user_data['title'] = $row->first_name."'s Birthday";
            $user_data['start'] = $date;
            $user_data['className'] = 'bg-info';
            $uData[] = $user_data;
    }
            
    
      }
      
      $class = array("bg-warning", "bg-success", "bg-indigo", "bg-gray", "bg-red", "bg-orange", "bg-pink");

      foreach ($get['result'] as $key => $value) {
             $my_date = str_replace("/", ".",$value->event_date);
             $my_date = strtotime($value->event_date);
              $arrdata = 'current + ';
             $data['title'] = $value->subject;
             $data['start'] = $value->event_date;
             $data['end'] = $value->event_date;
             $data['className'] = $class[array_rand($class)];
             $result[] = $data;
            
      }
       $classname = array("bg-warning", "bg-success", "bg-indigo", "bg-gray", "bg-red", "bg-orange", "bg-pink");
      foreach ($company_holidays as $key => $value) {
            
             $data_holiday['title'] = $value->holiday_title;
             $data_holiday['start'] = $value->holiday_date;
               $data_holiday['className'] = $classname[array_rand($classname)];
             
             $result_holiday[] = $data_holiday;
            
      }
      $merge = array_merge($uData, $result,$result_holiday);
            return $merge;
    }

    public function viewEventWhere($where)
    {
        $this->db->select('*');
        $this->db->from('events');
     
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewTodayEvent()
    {
        $date = new DateTime("now");
 
        $curr_date = $date->format('Y-m-d ');
        $this->db->select('*');
        $this->db->from('events');
        $this->db->where('event_date', $curr_date);
       
        $query = $this->db->get();
        return $query->result();
    }
     public function viewEventtotal()
    {
        $this->db->select('*');
        $this->db->from('events');
      
        $query = $this->db->get();
        return $query->result();
    }
}