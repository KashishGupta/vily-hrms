<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Events.php
	Date: November 29, 2019
	Info: This is the main class which is hold all the Functions of the Events.
*/

class Events extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('eventsModel');
	}
	
	public function _addEvent($request)
    {
		$eventData = array();
		$module_name = 'events';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Events. If User has the permission for that. */

			if(isset($request->event_date) AND isset($request->event_time) AND isset($request->event_location) AND isset($request->subject))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$eventData = array(
					'event_number' => $this->api->_getRandomString(6),
					'event_date' => $request->event_date,
					'event_time' => $request->event_time,
					'event_location' => $request->event_location,
					'subject' => $request->subject,
					
					'created_by' => $user_id,
					'modify_by' => $user_id
				);

				if(isset($request->description))
					$eventData['description'] = $request->description;
				$result = $this->eventsModel->addEvent($eventData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Event Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Event Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Events.");
		}
    }

    public function _deleteEvent($request)
    {
		$module_name = 'events';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Events. If User has the permission for that. */

			if(isset($request->event_id))
			{
				$where_event = array(
					'events.id' => $request->event_id,
				);
				$result = $this->eventsModel->deleteEvent($where_event);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Event Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Event Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Event.");
		}
    }

    public function _editEvent($request)
    {
		$eventData = array();
		$module_name = 'events';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Events.. If User has the permission for that. */

			if(isset($request->event_id))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];

				$event_where = array(
					'events.id' => $request->event_id
				);

				$eventData['modify_by'] = $user_id;
				if(isset($request->event_date))
					$eventData['event_date'] = $request->event_date;
				if(isset($request->event_time))
					$eventData['event_time'] = $request->event_time;
				if(isset($request->event_location))
					$eventData['event_location'] = $request->event_location;
				if(isset($request->event_image))
					$eventData['event_image'] = $request->event_image;
				if(isset($request->subject))
					$eventData['subject'] = $request->subject;
				if(isset($request->description))
					$eventData['description'] = $request->description;

				$result = $this->eventsModel->editEvent($eventData,$event_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Event Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Event Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Event.");
		}
    }
   // view only Events All Events
   public function _allEvents($request){
 	$module_name = 'events';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Event. If User has the permission for that. */

			if(TRUE)
			{	
					$result = $this->eventsModel->viewEventtotal();
					return $result;	
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Event.");
		}
    
}
}
// view Events And birthday In This Api
    public function _viewEvent($request)
    {
		$module_name = 'events';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Event. If User has the permission for that. */

			if(TRUE)
			{	
					$result = $this->eventsModel->viewEvent();
					return $result;	
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Event.");
		}
    
}
else
{
	$this->api->_sendError("Sorry, You Have No Permission To View Event.");
}
}

// view Events And birthday In This Api
public function _viewTodayEvent($request)
{
	$module_name = 'events';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	
	if($permission->can_view == 'Y')
	{
		/* Here we View the Event. If User has the permission for that. */

		if(TRUE)
		{	
				$result = $this->eventsModel->viewTodayEvent();
				return $result;	
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To View Event.");
	}

}
else
{
$this->api->_sendError("Sorry, You Have No Permission To View Event.");
}
}
// view Event where
public function _viewevetWhere($request)
   {
		$module_name = 'events';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Event. If User has the permission for that. */

			if(TRUE)
			{	
					if(isset($request->event_id))
				{
					$where_event_id['id'] = $request->event_id;
					$result = $this->eventsModel->viewEventWhere($where_event_id);
					return $result;
				}
				else
				{
					$result = $this->eventsModel->viewEventtotal();
					return $result;
				}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Event.");
		}
    
}
}
}
