<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Reimbursement.php
	Date: February 23, 2020
	Info: This is the main class which is hold all the Functions of the Reimbursement.
*/

class Reimbursement extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('reimbursementModel');
	}
	// Add Hotel/Food Policy
	public function _addReimbursementhotelPolicy($request)
	{
		
		$reimbursementData = array();
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$reimbursement_where = array();
		if($permission->can_add == 'Y')
		{
			/* Here we Update/Edit the Reimbursement.. If User has the permission for that. */
   
			if(isset($request->policy_name) AND isset($request->type) AND isset($request->price))
			{
				
			$food_data = array(
				'policy_name'=>$request->policy_name,
				'type'=>$request->type,
				'price'=>$request->price
				
			);
				$result = $this->reimbursementModel->addReimbursementHotel($food_data);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Hotel/Food Policy Add Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Policy Not add. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To add Reimbursement Policy.");
		}
	}
	 // view Reimbursement Hotel/Food POlicy
	 public function _viewReimbursementHotel($request)
	 {		
		
			 /* Here we View the Reimbursement Food. If User has the permission for that. */
				
			 if(TRUE)
			 {
				 if(isset($request->hotel_id))
				 {
					 $hotel_where['reimbursement_policy_hotel.id'] = $request->hotel_id;
					 $result = $this->reimbursementModel->viewHotelPolicyWhere($hotel_where);
					 return $result;
				 }
				 else
				 {
					 $result = $this->reimbursementModel->viewHotelPolicy();
					 return $result;
				 }
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		 
	 }	
   // Add Trave Policy
 public function _addReimbursementTravelPolicy($request)
 {
	 $reimbursementData = array();
	 $module_name = 'reimbursement';
	 $permissions = $this->users->_checkPermission($module_name);
	 foreach($permissions as $permission){}
		 $reimbursement_where = array();
	 if($permission->can_add == 'Y')
	 {
		 /* Here we Update/Edit the Reimbursement.. If User has the permission for that. */

		 if(isset($request->policy_name) AND isset($request->type) AND isset($request->per_km))
		 {
		 $travel_data = array(
			 'policy_name'=>$request->policy_name,
			 'type'=>$request->type,
             'per_km'=>$request->per_km
		 );
			 $result = $this->reimbursementModel->addReimbursementTravel($travel_data);
			 if($result)
			 {
				 return array(
					 'status' => '1',
					 'message' => 'Reimbursement Travel Policy Add Successfully.'
				 );
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, Reimbursement Policy Not add. Please Try Again!!");
			 }
		 }
		 else
		 {
			 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		 }
	 }
	 else
	 {
		 $this->api->_sendError("Sorry, You Have No Permission To add Reimbursement Policy.");
	 }
 }	
 // View Reimbursement Travel Policy
 public function _viewReimbursementTravelPolicy($request)
 {
	
			 /* Here we View the Reimbursement Food. If User has the permission for that. */
				
			 if(TRUE)
			 {
				 if(isset($request->travel_id))
				 {
					 $travel_where['reimbursement_policy_travel.id'] = $request->travel_id;
					 $result = $this->reimbursementModel->viewTravelPolicyWhere($travel_where);
					if($result == null)
					{
						$arrResult = 0;
						return $arrResult;
					}else{
						return $result;
					}
				 }
				 else
				 {
					 $result = $this->reimbursementModel->viewTravelPolicy();
					 return $result;
				 }
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		
 }
 // edit Reimbursement Travel
 public function _editReimbursementTravel($request)
    {
		$reimbursementData = array();
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$reimbursement_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Reimbursement.. If User has the permission for that. */

			if(isset($request->travel_id))
			{
			

				$reimbursement_where = array(
					'id' => $request->travel_id
				);

				if(isset($request->policy_name))
					$reimbursementData['policy_name'] = $request->policy_name;
				if(isset($request->type))
					$reimbursementData['type'] = $request->type;
					if(isset($request->per_km))
					$reimbursementData['per_km'] = $request->per_km;
				$result = $this->reimbursementModel->editReimbursementTravel1($reimbursementData,$reimbursement_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Travel Policy Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Reimbursement.");
		}
    }
 //  Delete Reimbursement
 public function _deleteReimbursementTravel($request)
    {		
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Reimbursement. If User has the permission for that. */

			if(isset($request->travel_id))
			{
				$where_reimbursement = array(
					'id' => $request->travel_id,
				);
				$result = $this->reimbursementModel->deleteReimbursementTravel($where_reimbursement);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Travel Policy Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Reimbursement.");
		}
    }
//  Delete Reimbursement
 public function _deleteReimbursement($request)
    {		
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Reimbursement. If User has the permission for that. */

			if(isset($request->reimbursement_id))
			{
				$where_reimbursement = array(
					'id' => $request->reimbursement_id,
				);
				$result = $this->reimbursementModel->deleteReimbursement($where_reimbursement);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Reimbursement.");
		}
    }

   
// edit Reimbursement Hotel
 public function _editReimbursement($request)
    {
		$reimbursementData = array();
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$reimbursement_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Reimbursement.. If User has the permission for that. */

			if(isset($request->reimbursement_id))
			{
			

				$reimbursement_where = array(
					'id' => $request->reimbursement_id
				);

				if(isset($request->departure_date))
					$reimbursementData['departure_date'] = $request->departure_date;
				if(isset($request->arrival_date))
					$reimbursementData['arrival_date'] = $request->arrival_date;
				if(isset($request->hotel_amount))
					$reimbursementData['hotel_amount'] = $request->hotel_amount;
				if(isset($request->hotel_name))
					$reimbursementData['hotel_name'] = $request->hotel_name;
				$result = $this->reimbursementModel->editReimbursement($reimbursementData,$reimbursement_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Reimbursement.");
		}
    }
    // edit Reimbursement Food
 public function _editFood($request)
    {
		$reimbursementData = array();
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$food_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Reimbursement.. If User has the permission for that. */

			if(isset($request->food_id))
			{
			

				$food_where = array(
					'id' => $request->food_id
				);

				if(isset($request->food_name))
					$FoodData['food_name'] = $request->food_name;
				if(isset($request->city))
					$FoodData['city'] = $request->city;
				if(isset($request->food_date))
					$FoodData['food_date'] = $request->food_date;
			
         if(isset($request->food_amount))
					$FoodData['food_amount'] = $request->food_amount;
        
				$result = $this->reimbursementModel->editReimbursementFood($FoodData,$food_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Food Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Reimbursement Food.");
		}
    }
     // edit Reimbursement Tour
   public function _editReimbursementTour($request)
    {
		$reimbursementData = array();
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$food_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Reimbursement.. If User has the permission for that. */

			if(isset($request->tour_id))
			{
			

				$tour_where = array(
					'reimbursement_id' => $request->tour_id
				);
 
				  if(isset($request->starting_city))
					$tourData['starting_city'] = $request->starting_city;
				  if(isset($request->end_city))
					$tourData['end_city'] = $request->end_city;
				  if(isset($request->start_date))
					$tourData['start_date'] = $request->start_date;
				  if(isset($request->end_date))
					$tourData['end_date'] = $request->end_date;
                  if(isset($request->start_time))
					$tourData['start_time'] = $request->start_time;
                 if(isset($request->end_time))
					$tourData['end_time'] = $request->end_time;
                 if(isset($request->mode_of_journey))
					$tourData['mode_of_journey'] = $request->mode_of_journey;
                 if(isset($request->amount))
					$tourData['amount'] = $request->amount;
        
				$result = $this->reimbursementModel->editReimbursementTour($tourData,$tour_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Tour Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Tour Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Reimbursement Tour.");
		}
    }
    // Edit Other ReimbursementTour
    public function _editReimbursementOther($request)
    {
		$otherData = array();
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$food_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Reimbursement.. If User has the permission for that. */

			if(isset($request->other_id))
			{
			

				$other_where = array(
					'id' => $request->other_id
				);

				if(isset($request->other_details))
					$otherData['other_details'] = $request->other_details;
				if(isset($request->other_date))
					$otherData['other_date'] = $request->other_date;
				if(isset($request->bill_name))
					$otherData['bill_name'] = $request->bill_name;
				if(isset($request->employee_amount))
					$otherData['employee_amount'] = $request->employee_amount;
         
        
				$result = $this->reimbursementModel->editOther($otherData,$other_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement other Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement other Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Reimbursement other.");
		}
    }
     // view Reimbursement Other Amonts
  public function _viewOtherReimbursement($request)
    {		
		
			/* Here we View the Reimbursement Food. If User has the permission for that. */
               
			if(TRUE)
			{

				if(isset($request->reimbursement_id))
				{
					$tour_where['other_amount.reimbursement_id'] = $request->reimbursement_id;
					$result = $this->reimbursementModel->viewOtherWhere($tour_where);
					return $result;
				}
				else
				{
					$result = $this->reimbursementModel->viewOther();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		 
    }
    
     // Reimbursement view reimbursement employee 
    public function _viewHotelEmployee($request)
    {		
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$profile_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Reimbursement.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
      if(isset($request->user_id)){
      	$hotel_where['reimbursement.reimbursement_id'] = $request->user_id;				
				$result = $this->reimbursementModel->viewReimbursementWhere2($hotel_where);
				return $result;
      }
      else if(isset($request->hotel_id)){
      	$hotel_where['reimbursement.id'] = $request->hotel_id;				
				$result = $this->reimbursementModel->viewReimbursementWhere2($hotel_where);
				return $result;
      }else{
				$hotel_where['reimbursement.user_id'] = $user_id;				
				$result = $this->reimbursementModel->viewReimbursementWhere2($hotel_where);
				return $result;
        }
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		
		
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Reimbursement Hotel.");
		}
    }
     // Reimbursement view tour employee 
    public function _viewOtherEmployee($request)
    {		
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$profile_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Reimbursement.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
       if(isset($request->user_id)){
      	$other_where['other_amount.tour_id'] = $request->user_id;				
				$result = $this->reimbursementModel->viewOtherWhere($other_where);
				return $result;
      }
      if(isset($request->other_id)){
      	$other_where['other_amount.id'] = $request->other_id;				
				$result = $this->reimbursementModel->viewOtherWhere($other_where);
				return $result;
      }else{
				$other_where['other_amount.user_id'] = $user_id;				
				$result = $this->reimbursementModel->viewOtherWhere($other_where);
				return $result;
        }
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		
		
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Reimbursement Other.");
		}
    }
     // Reimbursement view Food employees 
    public function _viewFoodEmployee($request)
    {		
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$profile_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Reimbursement.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
      if(isset($request->user_id)){
      	$food_where['user_food.tour_id'] = $request->user_id;				
				$result = $this->reimbursementModel->viewFoodWhere($food_where);
				return $result;
      }
      else if(isset($request->food_id)){
      	$food_where['user_food.id'] = $request->food_id;				
				$result = $this->reimbursementModel->viewFoodWhere($food_where);
				return $result;
      }else{
				$food_where['user_food.user_id'] = $user_id;				
				$result = $this->reimbursementModel->viewFoodWhere($food_where);
				return $result;
        }
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		
		
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Reimbursement Food.");
		}
    }
    // Reimbursement view tour employee 
    public function _viewTourEmployee($request)
    {		
		
			/* Here we View the Reimbursement.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
      if(isset($request->user_id)){
            	$tour_where['reimbursement_status.reimbursement_id'] = $request->user_id;				
				$result = $this->reimbursementModel->viewReimbursementEmployee($tour_where);
				return $result;
	  }
	  else{
			$tour_where['reimbursement_status.user_id'] = $user_id;				
			$result = $this->reimbursementModel->viewReimbursementEmployee($tour_where);
			return $result;
        }
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
   // view Reimbursement Food
  public function _viewFoodReimbursement($request)
    {		
		
			/* Here we View the Reimbursement Food. If User has the permission for that. */
               
			if(TRUE)
			{

				if(isset($request->reimbursement_id))
				{
					$tour_where['user_food.reimbursement_id'] = $request->reimbursement_id;
					$result = $this->reimbursementModel->viewFoodWhere($tour_where);
					return $result;
				}
				else
				{
					$result = $this->reimbursementModel->viewFood();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
 // view Reimbursement Tour
  public function _viewTourReimbursement($request)
    {		
		
			/* Here we View the Reimbursement Tour. If User has the permission for that. */
               
			if(TRUE)
			{

				if(isset($request->reimbursement_id))
				{
					$tour_where['user_tour.reimbursement_id'] = $request->reimbursement_id;
					$result = $this->reimbursementModel->viewTourWhereusertour($tour_where);
					return $result;
				}
				if(isset($request->tour_id))
				{
					$tour_where['user_tour.id'] = $request->tour_id;
					$result = $this->reimbursementModel->viewTourWhere($tour_where);
					return $result;
				}
				else
				{
					$result = $this->reimbursementModel->viewTour();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
    // view Reimbursement Employee
  public function _viewEmployeeReimbursement($request)
    {		
		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$profile_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leaves.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$profile_where['user_tour.user_id'] = $user_id;				
				$result = $this->profileModel->viewUserWhere($profile_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		
		
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Profile.");
		}
    }
  // view Reimbursement
  public function _viewReimbursement($request)
    {		
		
		
			/* Here we View the Reimbursement. If User has the permission for that. */
               $user = $this->api->_GetTokenData();
               $user_id = $user['id'];
			if(TRUE)
			{

				if(isset($request->reimbursement_id))
				{
					$reimbursement_where['reimbursement.reimbursement_id'] = $request->reimbursement_id;
					$result = $this->reimbursementModel->viewReimbursementWhere2($reimbursement_where);
					return $result;
				}
				else
				{
					$result = $this->reimbursementModel->viewreimbursement();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
			
    }
   // view all Reimnursment Pending by Branch 
   public function _viewReimbursementeditBranch($request)
   {		
   $module_name = 'reimbursement';
	   $permissions = $this->users->_checkPermission($module_name);
	   foreach($permissions as $permission){}
   
	   
	   if($permission->can_view == 'Y')
	   {
		   /* Here we View the userss.. If User has the permission for that. */

		   if(TRUE)
		   {
		   if(isset($request->reimbursement_id))
				   $reimbursement_where['reimbursement_status.id'] = $request->reimbursement_id;
			   
				   $result = $this->reimbursementModel->viewReimbursementBranch($reimbursement_where);
				   return $result;
		 
			   
		   }
		   else
		   {
			   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		   }
	   }
	   else
	   {
		   $this->api->_sendError("Sorry, You Have No Permission To View Reimbursement users.");
	   }
   }
    public function _viewReimbursementBranch($request)
    {		
	$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(!empty($request->user_id == 'all'))
				{
					$user_ids = $this->users->_getUserId();
					
					if(!empty($user_ids))
						$where = 'reimbursement_status.user_id IN ('.implode(',',$user_ids).') AND reimbursement_status.status = 0';
					else
						$where = 'reimbursement_status.user_id = 0 AND reimbursement_status.status = 0';
						$result = $this->reimbursementModel->viewReimbursementBranch($where);
					return $result;
				}else{
			if(isset($request->reimbursement_id))
					$reimbursement_where['reimbursement_status.id'] = $request->reimbursement_id;
				if(isset($request->user_id))
					$reimbursement_where['reimbursement_status.user_id'] = $request->user_id;
				$user_ids = $this->users->_getUserId();
				if(!empty($user_ids))
					$where = 'reimbursement_status.user_id IN ('.implode(',',$user_ids).') AND reimbursement_status.status = 0';
				else
					$where = 'reimbursement_status.user_id = 0 AND reimbursement_status.status = 0';

			if(!empty($reimbursement_where))
				{
					$result = $this->reimbursementModel->viewReimbursementWhereBranch($reimbursement_where,$where);
					return $result;
				}
				else
				{
					$result = $this->reimbursementModel->viewReimbursementBranch($where);
					return $result;
				}
			}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Reimbursement users.");
		}
	}
    // view all Reimnursment Approved by Branch 
    public function _viewReimbursementApproved($request)
    {		
	$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(!empty($request->user_id == 'all'))
				{
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
					$where = 'reimbursement_status.user_id IN ('.implode(',',$user_ids).') AND reimbursement_status.status = 1';
				else
					$where = 'reimbursement_status.user_id = 0 AND reimbursement_status.status = 1';
					$result = $this->reimbursementModel->viewReimbursementBranch($where);
					return $result;
				}else{
					if(isset($request->user_id))
					$reimbursement_where['reimbursement_status.user_id'] = $request->user_id;
			if(isset($request->reimbursement_id))
					$reimbursement_where['reimbursement_status.id'] = $request->reimbursement_id;
				$user_ids = $this->users->_getUserId();
				if(!empty($user_ids))
					$where = 'reimbursement_status.user_id IN ('.implode(',',$user_ids).') AND reimbursement_status.status = 1';
				else
					$where = 'reimbursement_status.user_id = 0 AND reimbursement_status.status = 1';

			if(!empty($reimbursement_where))
				{
					$result = $this->reimbursementModel->viewReimbursementWhereBranch($reimbursement_where,$where);
					return $result;
				}
				else
				{
					$result = $this->reimbursementModel->viewReimbursementBranch($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Reimbursement Approved.");
		}
	}
  // view all Reimnursment Approved by Branch 
    public function _viewReimbursementDeclined($request)
    {		
	$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(!empty($request->user_id == 'all'))
				{
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
					$where = 'reimbursement_status.user_id IN ('.implode(',',$user_ids).') AND reimbursement_status.status = 2';
				else
					$where = 'reimbursement_status.user_id = 0 AND reimbursement_status.status = 2';
					$result = $this->reimbursementModel->viewReimbursementBranch($where);
					return $result;
				}else{
				if(isset($request->user_id))
					$reimbursement_where['reimbursement_status.user_id'] = $request->user_id;
              if(isset($request->reimbursement_id))
					$reimbursement_where['reimbursement_status.id'] = $request->reimbursement_id;
				$user_ids = $this->users->_getUserId();
				if(!empty($user_ids))
					$where = 'reimbursement_status.user_id IN ('.implode(',',$user_ids).') AND reimbursement_status.status = 2';
				else
					$where = 'reimbursement_status.user_id = 0 AND reimbursement_status.status = 2';

			if(!empty($reimbursement_where))
				{
					$result = $this->reimbursementModel->viewReimbursementWhereBranch($reimbursement_where,$where);
					return $result;
				}
				else
				{
					$result = $this->reimbursementModel->viewReimbursementBranch($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Reimbursement Approved.");
		}
	}
  // change food Reimbursement Other status
 public function _editTourStatus($request)
    {

		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Status reimbursement other.. If User has the permission for that. */

			if(isset($request->reimbursement_id))
			{
				$tour_where = array( 
					'reimbursement_id' => $request->reimbursement_id
				);

				if(isset($request->status))
					$tourData['status'] = $request->status;
          if(isset($request->fixed_amount))
					$tourData['fixed_amount'] = $request->fixed_amount;
				$result = $this->reimbursementModel->editTour($tourData,$tour_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Tour Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Other Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Satus Reimbursement Tour.");
		}
	}
	// change food Reimbursement Other status
	public function _editchangeStatus($request)
    {

		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Status reimbursement other.. If User has the permission for that. */

			if(isset($request->reimbursement_id) AND isset($request->status))
			{
				$reimbursement_where = array( 
					'id' => $request->reimbursement_id
				);
				if($request->status == 1)			// When User Want to Approve The Leave
					{
				

					$reimbursementData['status'] = 1;
				$result = $this->reimbursementModel->editStatus($reimbursementData,$reimbursement_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Approved Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Approved Action Not Performed. Please Try Again!!");
				}
			}
			else if($request->status == 2){
				

					$reimbursementData['status'] = 2;
				$result = $this->reimbursementModel->editStatus($reimbursementData,$reimbursement_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Declined  Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Disapproved Action Not Performed. Please Try Again!!");
				}
			}
			else{
				$this->api->_sendError("Sorry, You Are Performed Wrong Task. Please Try Again!!");
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Satus Reimbursement Tour.");
		}
    }
 // change food Reimbursement Other status
 public function _editOtherStatus($request)
    {

		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Status reimbursement other.. If User has the permission for that. */

			if(isset($request->reimbursement_id))
			{
				$other_where = array( 
					'reimbursement_id' => $request->reimbursement_id
				);

				if(isset($request->status))
					$otherData['status'] = $request->status;
          if(isset($request->admin_amount))
					$otherData['admin_amount'] = $request->admin_amount;
				$result = $this->reimbursementModel->editOther($otherData,$other_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Other Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Other Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Satus Reimbursement other.");
		}
    }
 // change food Reimbursement food status
 public function _editFoodStatus($request)
    {

		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Status reimbursement.. If User has the permission for that. */

			if(isset($request->reimbursement_id))
			{
				$Food_where = array(
					'reimbursement_id' => $request->reimbursement_id
				);

				if(isset($request->status))
					$FoodData['status'] = $request->status;
          if(isset($request->clear_amount))
					$FoodData['clear_amount'] = $request->clear_amount;
				$result = $this->reimbursementModel->editFood($FoodData,$Food_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Food Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Food Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Satus Reimbursement.");
		}
    }
  // Change status Reimbursement
     public function _editReimbursementStatus($request)
    {

		$module_name = 'reimbursement';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Status reimbursement.. If User has the permission for that. */

			if(isset($request->reimbursement_id))
			{
				$reimbursement_where = array(
					'reimbursement_id' => $request->reimbursement_id
				);

				if(isset($request->status))
					$reimbursementData['status'] = $request->status;
         	if(isset($request->remarked_amount))
					$reimbursementData['remarked_amount'] = $request->remarked_amount;
				$result = $this->reimbursementModel->editReimbursement($reimbursementData,$reimbursement_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Reimbursement Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Reimbursement Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Satus Reimbursement.");
		}
    }
    
    //chenge expense Stattus
   
    
   
}
