<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: ReiumbersemntModel.php
	Date: February 23, 2020
	Info: This is the main class which is hold all the Models of the Reiumbersemnt.
*/

class ReimbursementModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
     
   
     public function deleteReimbursement($where_reimbursement)
    {
        $this->db->where($where_reimbursement);
        $this->db->delete('user_tour');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function deleteReimbursementTravel($where_reimbursement)
    {
        $this->db->where($where_reimbursement);
        $this->db->delete('reimbursement_policy_travel');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
    public function viewHotelPolicyWhere($where)
    {
       
         $this->db->select('*');
       $this->db->from('reimbursement_policy_hotel');
         
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewHotelPolicy()
    {
       
         $this->db->select('*');
       $this->db->from('reimbursement_policy_hotel'); 
        //$this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function addReimbursementHotel($data)
    {
        $this->db->insert('reimbursement_policy_hotel',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function addReimbursementTravel($data)
    {
        $this->db->insert('reimbursement_policy_travel',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     public function addReimbursement($data)
    {
        $this->db->insert('reimbursement',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function addstatus($data)
    {
        $this->db->insert('reimbursement_status',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     public function addTour($data)
    {
        $this->db->insert('user_tour',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     public function addFood($data)
    {
        $this->db->insert('user_food',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    
     public function addOther($data)
    {
        $this->db->insert('other_amount',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
   
  public function editReimbursementFood($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_food',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    } 
    public function editReimbursementTravel1($data,$where)
    {
        $this->db->where($where);
        $this->db->update('reimbursement_policy_travel',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    } 
     
   public function editReimbursementTour($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_tour',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
 public function editReimbursement($data,$where)
    {
        $this->db->where($where);
        $this->db->update('reimbursement',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
     public function editFood($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_food',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
     public function editOther($data,$where)
    {
        $this->db->where($where);
        $this->db->update('other_amount',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function editStatus($data,$where)
    {
        $this->db->where($where);
        $this->db->update('reimbursement_status',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function editTour($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_tour',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
     // view Riembursment Other Amount
   public function viewOther()
    {
       $this->db->select('*');
        $this->db->from('other_amount');
       
         
       
        
         $query = $this->db->get();
        return $query->result();
    }
    
    public function viewTravelPolicyWhere($where)
    {
       
         $this->db->select('*');
       $this->db->from('reimbursement_policy_travel');
         
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewTravelPolicy()
    {
       
         $this->db->select('*');
       $this->db->from('reimbursement_policy_travel');
         
        //$this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewOtherWhere($where)
    {
       
         $this->db->select('*');
       $this->db->from('other_amount');
         
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
   // view Riembursment Food
   public function viewFood()
    {
       $this->db->select('*');
        $this->db->from('user_food');
       
         
       
        
         $query = $this->db->get();
        return $query->result();
    }

    public function viewFoodWhere($where)
    {
       
         $this->db->select('*');
       $this->db->from('user_food');
         
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

// view Riembursment Tour
   public function viewTour()
    {
       $this->db->select('*');
        $this->db->from('user_tour');
       
         
       
        
         $query = $this->db->get();
        return $query->result();
    }
    
    public function viewTourWhereusertour($where)
    {
       
        $this->db->select('*');
        $this->db->from('user_tour');
       
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewTourWhere($where)
    {
       
        $this->db->select('reimbursement_status.id,user_tour.starting_city,user_tour.end_city,user_tour.start_date,user_tour.end_date,user_tour.start_time,user_tour.end_time,user_tour.mode_of_journey,user_tour.amount,reimbursement_status.user_id,reimbursement_status.status,reimbursement.departure_date,reimbursement.arrival_date,reimbursement.hotel_amount,reimbursement.hotel_bill,reimbursement.hotel_name,other_amount.employee_amount,other_amount.bill_name,other_amount.other_details,other_amount.other_date,other_amount.other_image,user_food.food_name,user_food.city,user_food.food_date,user_food.food_bill,user_food.food_amount,users.emp_id,company_branches.branch_name,users.first_name,users.email,user_tour.fixed_amount,reimbursement.remarked_amount,user_food.clear_amount,other_amount.admin_amount');
        $this->db->from('reimbursement_status');
        $this->db->join('users','users.id = reimbursement_status.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
         $this->db->join('reimbursement','reimbursement_status.id = reimbursement.reimbursement_id');
          $this->db->join('other_amount','reimbursement_status.id = other_amount.reimbursement_id');
           $this->db->join('user_food','reimbursement_status.id = user_food.reimbursement_id');
           $this->db->join('user_tour','reimbursement_status.id = user_tour.reimbursement_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

// view Riembursment
   public function viewReimbursement()
    {
       $this->db->select('*');
        $this->db->from('reimbursement');
       
         
       
        
         $query = $this->db->get();
        return $query->result();
    }

    public function viewReimbursementWhere2($where)
    {
       
         $this->db->select('*');
       $this->db->from('reimbursement');
         
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
   

    public function viewReimbursementWhereBranch($where,$where2)
    {
        $this->db->select('reimbursement_status.id,reimbursement_status.status,users.user_code,user_details.user_image,user_tour.starting_city,user_tour.end_city,user_tour.start_date,user_tour.end_date,user_tour.start_time,user_tour.end_time,user_tour.mode_of_journey,user_tour.amount,user_tour.user_id,user_tour.status,reimbursement.departure_date,reimbursement.arrival_date,reimbursement.hotel_amount,reimbursement.hotel_bill,reimbursement.hotel_name,other_amount.employee_amount,other_amount.bill_name,other_amount.other_details,other_amount.other_date,other_amount.other_image,user_food.food_name,user_food.city,user_food.food_date,user_food.food_bill,user_food.food_amount,users.emp_id,company_branches.branch_name,users.first_name,users.email,user_tour.fixed_amount,reimbursement.remarked_amount,user_food.clear_amount,other_amount.admin_amount');
        $this->db->from('reimbursement_status');
        $this->db->join('users','users.id = reimbursement_status.user_id');
        $this->db->join('user_details','user_details.user_id = reimbursement_status.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
         $this->db->join('reimbursement','reimbursement_status.id = reimbursement.reimbursement_id');
          $this->db->join('other_amount','reimbursement_status.id = other_amount.reimbursement_id');
           $this->db->join('user_food','reimbursement_status.id = user_food.reimbursement_id');
           $this->db->join('user_tour','reimbursement_status.id = user_tour.reimbursement_id');
        $this->db->where($where);
          $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
     public function viewReimbursementBranch($where2)
    {
        $this->db->select('reimbursement_status.id,reimbursement_status.status,user_details.user_image,users.user_code,user_tour.starting_city,user_tour.end_city,user_tour.start_date,user_tour.end_date,user_tour.start_time,user_tour.end_time,user_tour.mode_of_journey,user_tour.amount,user_tour.user_id,user_tour.status as status_status,reimbursement.departure_date,reimbursement.arrival_date,reimbursement.hotel_amount,reimbursement.hotel_bill,reimbursement.hotel_name,other_amount.employee_amount,other_amount.bill_name,other_amount.other_details,other_amount.other_date,other_amount.other_image,user_food.food_name,user_food.city,user_food.food_date,user_food.food_bill,user_food.food_amount,users.emp_id,company_branches.branch_name,users.first_name,users.email,user_tour.fixed_amount,reimbursement.remarked_amount,user_food.clear_amount,other_amount.admin_amount');
        $this->db->from('reimbursement_status');
        $this->db->join('users','users.id = reimbursement_status.user_id');
        $this->db->join('user_details','user_details.user_id = reimbursement_status.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
         $this->db->join('reimbursement','reimbursement_status.id = reimbursement.reimbursement_id');
          $this->db->join('other_amount','reimbursement_status.id = other_amount.reimbursement_id');
           $this->db->join('user_food','reimbursement_status.id = user_food.reimbursement_id');
           $this->db->join('user_tour','reimbursement_status.id = user_tour.reimbursement_id');
        $this->db->where($where2);
        
        $query = $this->db->get();
        return $query->result();
    }
    public function viewReimbursementEmployee($where2)
    {
        $this->db->select('reimbursement_status.id,reimbursement_status.status as final_status,user_tour.starting_city,user_tour.end_city,user_tour.start_date,user_tour.end_date,user_tour.start_time,user_tour.end_time,user_tour.mode_of_journey,user_tour.amount,user_tour.user_id,user_tour.status,reimbursement.departure_date,reimbursement.arrival_date,reimbursement.hotel_amount,reimbursement.hotel_bill,reimbursement.hotel_name,other_amount.employee_amount,other_amount.bill_name,other_amount.other_details,other_amount.other_date,other_amount.other_image,user_food.food_name,user_food.city,user_food.food_date,user_food.food_bill,user_food.food_amount,users.emp_id,company_branches.branch_name,users.first_name,users.email,user_tour.fixed_amount,reimbursement.remarked_amount,user_food.clear_amount,other_amount.admin_amount');
        $this->db->from('reimbursement_status');
        $this->db->join('users','users.id = reimbursement_status.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
         $this->db->join('reimbursement','reimbursement_status.id = reimbursement.reimbursement_id');
          $this->db->join('other_amount','reimbursement_status.id = other_amount.reimbursement_id');
           $this->db->join('user_food','reimbursement_status.id = user_food.reimbursement_id');
           $this->db->join('user_tour','reimbursement_status.id = user_tour.reimbursement_id');
        $this->db->where($where2);
        
        $query = $this->db->get();
        return $query->result();
    }
   
}