<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Resource.php
	Date: June 27, 2020
	Info: This is the main class which is hold all the Functions of the users.
*/

class Resources extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('resourceModel');
	}
	
	
// A User Resources Can view 
   public function _viewResources($request)
    {		
		$module_name = 'resources';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$resource_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Roles.. If User has the permission for that. */

			if(TRUE)
			{
      	    $user = $this->api->_GetTokenData();
			$user_id = $user['role_id'];
            $role_where['resource_role.role_id'] = $user_id;
				if(isset($request->role_id))
				{
					$role_where['resource_role.role_id'] = $request->role_id;
				}
				$result = $this->resourceModel->viewResourceWhere($role_where);
				return $result;
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Roles.");
		}
	}
// User Can Add Resources
public function _addResources($request)
    {
		
		$resource = array();
		$module_name = 'resources';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{       
			//echo "<pre>";print_r($request);echo"<pre>";
			/* Here we Add the Resources. If User has the permission for that. */
				
				$arrRequest=(array)$request;
				//print_r($arrRequest);
				//die;
				foreach($arrRequest as $arrRes)
				{
					foreach($arrRes as $role_id=>$data)
					{
						$where_cond['designation_id'] = $role_id;
						$result = $this->resourceModel->deleteResources($where_cond);

						foreach($data as $resource_id=>$dataM)
						{
							$VL_read= "N";
							$VL_write= "N";
							$VL_create= "N";
							$VL_delete= "N";
							if(!empty($dataM->read))
							{
								$VL_read = 'Y';
							}
							if(!empty($dataM->write))
							{
								$VL_write = 'Y';
							}
							if(!empty($dataM->create))
							{
								$VL_create = 'Y';
							}
							if(!empty($dataM->delete))
							{
								$VL_delete = 'Y';
							}
							$resource = array(
								'resource_id' => $resource_id,
								'designation_id' => $role_id,
								'can_add' => $VL_create,
								'can_edit' => $VL_write,
								'can_view' => $VL_read,
								'can_delete' => $VL_delete	
								);
								$result = $this->resourceModel->addResources($resource);
							}
					}
					
				}
				
				
				if($result = 1)
				{
					$result1 = 1;
				}else{
					$result1 = 1;
				}
				
				if($result1)
				{
					return array(
						'status' => '1',
						'message' => 'Resource Added Successfully.'
					);
				}
			
				else
				{
					$this->api->_sendError("Sorry, Resource Not Added. Please Try Again!!");
				}
		
			
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Resource.");
		}
    }
	//  Delete The Resource
    public function _deleteResources($request)
    {
		$module_name = 'resources';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Resource. If User has the permission for that. */

			if(isset($request->resources_id))
			{
				$where_resources = array(
					'id' => $request->resources_id,
				);
				$result = $this->resourceModel->deleteResources($where_resources);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Resource Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Resource Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Resource.");
		}
    }
	// A User Can View Resources all Resources 
   public function _viewAllResources($request)
    {		
		$module_name = 'resources';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Resources.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->role_id))
			{
				//print_r($request->role_id);
				//die;
				$role_where = array('designation_id' => $request->role_id);
				$result = $this->resourceModel->viewtimeResourceWhere($role_where);
				return $result;
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Resources.");
		}
	}
  // A User Can View Role all Resources 
  public function _viewAllRoll($request)
  {		
	  
		  /* Here we View the Roles.. If User has the permission for that. */

		  if(TRUE)
		  {
			  $user = $this->api->_GetTokenData();
			  $user_id = $user['role_id'];
			  $userdsignation = $user['designation_id'];
			  if($user_id == 1)
			  {
				  $arrWhere = array('resource_role.role_id' =>$user_id);
				  $result = $this->resourceModel->viewallRollResource($arrWhere);
				  return $result;
			  }else{
				  $arrWhere = array('resource_role.designation_id' =>$userdsignation);
				  $result = $this->resourceModel->viewallRollResource($arrWhere);
				  return $result;
			  }
			 
			  
			  
		  }
		  else
		  {
			  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		  }
	  
  }


	 public function _editResource($request)
    {
		$resourceData = array();
		$module_name = 'resources';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Reimbursement.. If User has the permission for that. */

			if(isset($request->editResource_id))
			{
				$resource_where = array(
					'id' => $request->editResource_id
				);

				if(isset($request->resource_id))
					$resourceData['resource_id'] = $request->resource_id;
				if(isset($request->role_id))
					$resourceData['role_id'] = $request->role_id;
				if(isset($request->can_add))
					$resourceData['can_add'] = $request->can_add;
				if(isset($request->can_edit))
					$resourceData['can_edit'] = $request->can_edit;
				if(isset($request->can_view))
					$resourceData['can_view'] = $request->can_view;
				if(isset($request->can_delete))
					$resourceData['can_delete'] = $request->can_delete;
				

				$result = $this->resourceModel->editResource($resourceData,$resource_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Resource Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Resource Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Resource.");
		}
    }
}