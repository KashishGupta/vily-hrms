<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Resources.php
	Date: June 27, 2020
	Info: This is the main class which is hold all the Models of the Branchs.
*/

class ResourceModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

   public function viewResource($where)
    {
        $this->db->select('resource_role.id,resource_role.role_id,resource_role.resource_id,resource_role.can_add,resource_role.can_edit,resource_role.can_view,resource_role.can_delete,resource.resource_name,role.role_name');
        $this->db->from('resource_role');
         $this->db->join('resource', 'resource.id = resource_role.resource_id');
        $this->db->join('role', 'role.id = resource_role.role_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
   
    }
public function viewallRollResource($where)
    {
        $this->db->select('*');
        $this->db->from('resource_role');
        $this->db->join('resource', 'resource.id = resource_role.resource_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
   
    }
    public function viewResourceWhere($where)
    {
         $this->db->select('*');
        $this->db->from('resource_role');
         //$this->db->join('resource', 'resource.id = resource_role.resource_id');
        $this->db->join('role', 'role.id = resource_role.role_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewtimeResourceWhere($where)
    {
         $this->db->select('*');
        $this->db->from('resource_role');
         //$this->db->join('resource', 'resource.id = resource_role.resource_id','left');
       
        $this->db->where($where);
        $query = $this->db->get()->result();
        foreach ($query as $value) {
            
            $data_role['role_id'] = $value->role_id;
            $data_role['can_add'] = $value->can_add;
            $data_role['can_view'] = $value->can_view;
            $data_role['can_edit'] = $value->can_edit;
            $data_role['can_delete'] = $value->can_delete;
              
            $result_role[$value->resource_id] = $data_role;
           
           
     }
     
        $this->db->select('*');
        $this->db->from('resource');
       
        $query2 = $this->db->get()->result();
        foreach ($query2 as $key => $value) {
            
            $resource['id'] = $value->id;
            $resource['resource_name'] = $value->resource_name;
            $resource['display_name'] = $value->display_name;
            $result_resource[] = $resource;
           
     }
     
     $arrFinalData= array();
     foreach($result_resource as $data)
     {

        if(!empty($result_role[$data['id']]))
        {
            //$data['role']=$result_role[$data['id']];
            $data = array_merge($data,$result_role[$data['id']]);
        }
        else{
            //$data['role']=array();
        }
        $arrFinalData[]= $data;
     }
    
            return $arrFinalData;
       
    }
    public function viewtimeResource()
    {
        $this->db->select('resource.id,resource.resource_name');
        $this->db->from('resource');
       
        $query = $this->db->get();
        return $query->result();
   
    }
    public function addResources($data)
    {
       
        $this->db->insert('resource_role',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
   
    }

      public function deleteResources($where_resources)
    {
        $this->db->where($where_resources);
        $this->db->delete('resource_role');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

     public function viewallResource()
    {
        $this->db->select('resource.id,resource.resource_name');
        $this->db->from('resource');
         
        $query = $this->db->get();
        return $query->result();
   
    }

   
   
     public function viewallRoll()
    {
        $this->db->select('role.id,role.role_name');
        $this->db->from('role');
         
        $query = $this->db->get();
        return $query->result();
   
    }

    public function editResource($data,$where)
    {
        $this->db->where($where);
        $this->db->update('resource_role',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
}