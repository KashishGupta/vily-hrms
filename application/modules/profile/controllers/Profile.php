<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH . '/libraries/Slack.php';

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Profile.php
	Date: February 27, 2020
	Info: This is the main class which is hold all the Functions of the users.
*/

class Profile extends MY_Controller {

    public function __construct(){
		parent::__construct();
		// $this->objOfSlack = new slack();
		$this->load->model('profileModel');
	}
		// view Notification
		public function _viewNotification($request){

			/* Here we View the Tickets.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$notification_where['user_notification.user_id'] = $user_id;				
			    $result = $this->profileModel->viewNotificationWhere($notification_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}

		}

		// view Notification
		public function _viewNotificationRaead($request){

			/* Here we View the Tickets.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$notification_where['user_notification.user_id'] = $user_id;				
			    $result = $this->profileModel->viewNotificationWhereread($notification_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}

		}
		
		// Add Education Details 
 public function _notificationRead($request)
 {
 	if(isset($request->notification_id) AND isset($request->status))
     {
		$where_Date = array(
			'id' => $request->notification_id
		);
		if(isset($request->status))
		$notificationData['status'] = $request->status;
		$result = $this->profileModel->editNotification($notificationData,$where_Date); 
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'Notification Read SuccessFully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, Not Read Notification. Please Try Again!!");
			}
     }
    else
	{
  	$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
    }
 
   }
	// view Notification
	public function _viewTotalNotification($request){
		
				 /* Here we View the Tickets.. If User has the permission for that. */
				 $user = $this->api->_GetTokenData();
				 $user_id = $user['id'];
				 if(TRUE)
				 {
					 $notification_where['user_notification.user_id'] = $user_id;				
				 $result = $this->profileModel->viewNotificationTotatWhere($notification_where);
					 return $result;
				 }
				 else
				 {
					 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				 }
			
		 }
 // Add Notifications For Particular user
 public function _addNotfication($request)
 {
 
 	if(isset($request->user_id) AND isset($request->message)){
  $user = $this->api->_GetTokenData();
  
	$user_name = $user['first_name'];
  $userNotification = 
  array(
  'user_id' => $request->user_id,
  'message' =>$request->message,
  'sender_name'=>$user_name
  );
   $result = $this->profileModel->addNotification($userNotification);
   if($result)
  {
					return array(
						'status' => '1',
						'message' => 'Message Send Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Notification Not Added. Please Try Again!!");
				}    
  }
  else
  {
  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
 
 }
	// Add Education Details 
 public function _addEducationDetails($request)
 {
	 
 $module_name = 'profile';
 $permissions = $this->users->_checkPermission($module_name);
 foreach($permissions as $permission){}
 if($permission->can_add == 'Y')
 {
 	if(isset($request->institution) AND isset($request->subject) AND isset($request->end_date) AND isset($request->grade) AND isset($request->education_degree))
  
  {
  	$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$user_code = $user['user_code'];
  $educationData = array(
					'institution' => $request->institution,
					'subject' => $request->subject,
					'end_date' => $request->end_date,
					'grade' => $request->grade,
					'education_degree' => $request->education_degree,
					'user_id' => $user_id,
					'user_code'=>$user_code
				);
				if(isset($request->user_code))
				$educationData['user_code'] = $request->user_code;
				$result = $this->profileModel->addEducation($educationData);
       	if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Education Details Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Education Not Added. Please Try Again!!");
				}
  }
  else{
  	$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
 }
 else
 {
 $this->api->_sendError("Sorry, You Have No Permission To Add Education.");
 }
 }
 // edit Profile EducATION
 public function _editEducation($request)
    {
		$ticketData = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Ticket. If User has the permission for that. */

			if(isset($request->education_id))
			{
					
				$education_where = array(
					'id' => $request->education_id
				
				);
	      $user = $this->api->_GetTokenData();
				$user_id = $user['id'];
                 
				$educationData = array(
					'user_id' => 	$user_id
				);
				if(isset($request->institution))
					$educationData['institution'] = $request->institution;
				if(isset($request->subject))
					$educationData['subject'] = $request->subject;
				if(isset($request->end_date))
					$educationData['end_date'] = $request->end_date;
        	if(isset($request->grade))
					$educationData['grade'] = $request->grade;
          	if(isset($request->education_degree))
					$educationData['education_degree'] = $request->education_degree;

				$result = $this->profileModel->editEducationdeta($educationData,$education_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Education Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Education Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Education Details.");
		}
    }
 // View Experience Can view 
    public function _viewEducationbEmployee($request)
    {		
		
			/* Here we View the user profile. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
      if(isset($request->education_id))
					$education_where['user_education.id'] = $request->education_id;
       if(isset($request->user_id))
					$education_where['user_education.user_id'] = $request->user_id;
					if(isset($request->user_code))
					$education_where['user_education.user_code'] = $request->user_code;
       if(!empty($education_where))
				{
					$result = $this->profileModel->viewEducationWhere($education_where);
					return $result;
				}
				else
				{
				$edu_where['user_education.user_id'] = $user_id;				
				$result = $this->profileModel->viewEducationWhere($edu_where);
				return $result;
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
   
    }
    public function _deleteExperienceData($request)
    {
    $module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
    if(isset($request->experience_id))
    {
   $experience_where = array(
					'id' => $request->experience_id,
				);
    $result = $this->profileModel->deletedExperience($experience_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Experience Profile Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Experience Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Experience.");
		}
    }
    //  Delete Education Profile
   public function _deleteEducationData($request)
    {		
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the education Profile. If User has the permission for that. */

			if(isset($request->education_id))
			{
				$where_education = array(
					'id' => $request->education_id,
				);
				$result = $this->profileModel->deleteEducationData($where_education);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Education Profile Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Education Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Education.");
		}
    }
	// Add Education Details 
 public function _addExperienceDetails($request)
 {
 $module_name = 'profile';
 $permissions = $this->users->_checkPermission($module_name);
 foreach($permissions as $permission){}
 if($permission->can_add == 'Y')
 {
 	if(isset($request->company_name) AND isset($request->location) AND isset($request->job_position) AND isset($request->period_from) AND isset($request->period_to))
  
  {
  	$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$user_code = $user['user_code'];
  $experienceData = array(
					'company_name' => $request->company_name,
					'location' => $request->location,
					'job_position' => $request->job_position,
					'period_from' => $request->period_from,
					'period_to' => $request->period_to,
					'user_id' => $user_id,
					'user_code'=>$user_code
				);
         
				$result = $this->profileModel->addExperience($experienceData);
       	if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Experience Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Experience Not Added. Please Try Again!!");
				}
  }
  else{
  	$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
 }
 else
 {
 $this->api->_sendError("Sorry, You Have No Permission To Add Experience.");
 }
 }
 // edit Profile Experience
 public function _editExperienceDetails($request)
    {
		$ticketData = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Ticket. If User has the permission for that. */

			if(isset($request->experience_id))
			{
				
               
				$experience_where = array(
					'id' => $request->experience_id
				
				);
	     
				if(isset($request->company_name))
					$experienceData['company_name'] = $request->company_name;
				if(isset($request->location))
					$experienceData['location'] = $request->location;
				if(isset($request->job_position))
					$experienceData['job_position'] = $request->job_position;
        	if(isset($request->period_from))
					$experienceData['period_from'] = $request->period_from;
          	if(isset($request->period_to))
					$experienceData['period_to'] = $request->period_to;

				$result = $this->profileModel->editExperience($experienceData,$experience_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Experience Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Experience details not updated. please try again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Experience Details.");
		}
    }
 // View Experience Can view 
    public function _viewExperienceEmployee($request)
    {		
		
			/* Here we View the user profile. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
       if(isset($request->experience_id))
					$exp_where['user_previouscompany.id'] = $request->experience_id;
       if(isset($request->user_id))
					$exp_where['user_previouscompany.user_id'] = $request->user_id;
					if(isset($request->user_code))
					$exp_where['user_previouscompany.user_code'] = $request->user_code;
       if(!empty($exp_where))
				{
					$result = $this->profileModel->viewExperienceWhere($exp_where);
					return $result;
				}
 
				else
				{
				$experience_where['user_previouscompany.user_id'] = $user_id;				
				$result = $this->profileModel->viewExperienceWhere($experience_where);
				return $result;
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
// A User Profile Can view 
    public function _viewProfile($request)
    {		
    
		
			/* Here we View the user profile. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$profile_where['users.id'] = $user_id;				
				$result = $this->profileModel->viewUserWhere($profile_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }

	// A User Profile Can view 
    public function _viewProfileEmployee($request)
    {		
    
			/* Here we View the user profile. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$profile_where['users.id'] = $user_id;				
				$result = $this->profileModel->viewUserWhereEmployee($profile_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	
    }

    // This is a Public API No Need To Check Token
   public function _viewprofilePublic($request)		
    {
    	
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			
				$profile_where['users.id'] = $user_id;				
				$result = $this->profileModel->viewUseryz($profile_where);
				return $result;
		
	}
 
	// This is a editProfile API 
   public function _editProfile($request)
    {
		$profile = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$profile_where= array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the profile. If User has the permission for that. */

			if(isset($request->user_id))
			{
				$profile_where = array(
					'user_id' => $request->user_id
				);

				if(isset($request->father_name))
					$profile['father_name'] = $request->father_name;
	              if(isset($request->personal_email))
					$profile['personal_email'] = $request->personal_email;
				if(isset($request->phone))
					$profile['phone'] = $request->phone;
				if(isset($request->address))
					$profile['address'] = $request->address;
				if(isset($request->gender))
					$profile['gender'] = $request->gender;
				if(isset($request->dob))
					$profile['dob'] = $request->dob;

				$result = $this->profileModel->editProfile($profile,$profile_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Profile Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Profile Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Profile.");
		}
    }
    //Edit User Details On Log In user
    public function _editUsersDetails($request)
    {
		$userDetails = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$user_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the User Details. If User has the permission for that. */
           $user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($user_id))
			{
				$user_where = array(
					'users.id' => $user_id
				);
				if(isset($request->phone))
					$userDetails['phone'] = $request->phone;
				if(isset($request->father_name))
					$userDetails['father_name'] = $request->father_name;
	           if(isset($request->dob))
					$userDetails['dob'] = $request->dob;
				if(isset($request->personal_email))
					$userDetails['personal_email'] = $request->personal_email;
				if(isset($request->address))
					$userDetails['address'] = $request->address;
				if(isset($request->gender))
					$userDetails['gender'] = $request->gender;		
				$result = $this->profileModel->editUserDetails($userDetails,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, User Details Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users Details.");
		}
    }
  public function _editDetailsprofile($request)
    {
		$userDetails = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$user_where = array();
		if($permission->can_edit == 'Y')
		{ 
			/* Here we Update/Edit the Details. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($user_id))
			{
				$user_where = array(
					'user_id' => $user_id
				);
				/*$user_where2 = array(
					'id' => $user_id
				);
                if(isset($request->first_name))
					$userDetails2['first_name'] = $request->first_name;
				if(isset($request->phone))
					$userDetails2['phone'] = $request->phone;
					$result = $this->profileModel->editUserDetails($userDetails2,$user_where2);*/
				if(isset($request->father_name))
					$userDetails['father_name'] = $request->father_name;
	           if(isset($request->dob))
					$userDetails['dob'] = $request->dob;
				if(isset($request->personal_email))
					$userDetails['personal_email'] = $request->personal_email;
				if(isset($request->address))
					$userDetails['address'] = $request->address;
				if(isset($request->gender))
					$userDetails['gender'] = $request->gender;
				
				$result = $this->profileModel->editUserDetails2($userDetails,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Profile Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Branch.");
		}
    }  
    
//Edit Relative contact details
     public function _editRelativeDetails($request)
    {
		$branch = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Branchs.. If User has the permission for that. */
           $user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($user_id))
			{
				$user_where = array(
					'user_emergency_contact.user_id' => $user_id
				);

				if(isset($request->name))
					$bank['name'] = $request->name;
				if(isset($request->relationship))
					$bank['relationship'] = $request->relationship;
	           if(isset($request->emergency_phone))
					$bank['emergency_phone'] = $request->emergency_phone;
				$result = $this->profileModel->editRelativeDetails($bank,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Relative Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Relative Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Relative.");
		}
    }
    //Edit Relative EMPLOYEE details
    /* public function _editeEmployeeDetails($request)
    {
		$relative = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$user_where = array();
		if($permission->can_edit == 'Y')
		{
			Here we Update/Edit the Branchs.. If User has the permission for that.
           $user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($request->user_id))
			{
				$user_where = array(
					'user_id' => $request->user_id
				);

				if(isset($request->name))
					$relative['name'] = $request->name;
				if(isset($request->relationship))
					$relative['relationship'] = $request->relationship;
	           if(isset($request->emergency_phone))
					$relative['emergency_phone'] = $request->emergency_phone;
					 if(isset($request->date_of_birth))
					$relative['date_of_birth'] = $request->date_of_birth;
				$result = $this->profileModel->editRelativeDetails($relative,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Relative Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Relative Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Relative.");
		}
    } */
 //Edit Personal Details details
   /* public function _editPersonal($request)
    {
		$userPersonal = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$user_where = array();
		if($permission->can_edit == 'Y')
		{
			Here we Update/Edit the Branchs.. If User has the permission for that. 
          
			if(isset($request->user_id))
			{
				$user_where = array(
					'user_personal.user_id' =>$request->user_id
				);

				if(isset($request->passport_no))
					$userPersonal['passport_no'] = $request->passport_no;
				if(isset($request->passport_expiry))
					$userPersonal['passport_expiry'] = $request->passport_expiry;
	           if(isset($request->nationality))
					$userPersonal['nationality'] = $request->nationality;
				 if(isset($request->religion))
					$userPersonal['religion'] = $request->religion;
				 if(isset($request->marital_status))
					$userPersonal['marital_status'] = $request->marital_status;
				 if(isset($request->no_of_children))
					$userPersonal['no_of_children'] = $request->no_of_children;
				$result = $this->profileModel->editDetails($userPersonal,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Personal Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Personal Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Personal.");
		}
    }*/
    //Edit Employee Bank Details details
    /* public function _editBank($request)
    {
		$userBank = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$user_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Branchs.. If User has the permission for that. 
          
			if(isset($request->user_id))
			{
				$user_where = array(
					'users_bank_details.user_id' =>$request->user_id
				);

				if(isset($request->holder_name))
					$userBank['holder_name'] = $request->holder_name;
				if(isset($request->account_no))
					$userBank['account_no'] = $request->account_no;
	           if(isset($request->bank_name))
					$userBank['bank_name'] = $request->bank_name;
				 if(isset($request->branch))
					$userBank['branch'] = $request->branch;
				 if(isset($request->salary))
					$userBank['salary'] = $request->salary;
				 if(isset($request->ifsc))
					$userBank['ifsc'] = $request->ifsc;
					 if(isset($request->pan_card))
					$userBank['pan_card'] = $request->pan_card;
					 if(isset($request->aadhar_card))
					$userBank['aadhar_card'] = $request->aadhar_card;
				$result = $this->profileModel->edit_BankDetails($userBank,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Personal Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Personal Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Personal.");
		}
    }*/
    //Edit Personal Details details
     public function _editPersonalDetails($request)
    {
		$branch = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Personal Details. If User has the permission for that. */
           $user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($user_id))
			{
				$user_where = array(
					'user_personal.user_id' => $user_id
				);

				if(isset($request->passport_no))
					$bank['passport_no'] = $request->passport_no;
				if(isset($request->passport_expiry))
					$bank['passport_expiry'] = $request->passport_expiry;
	           if(isset($request->nationality))
					$bank['nationality'] = $request->nationality;
				 if(isset($request->religion))
					$bank['religion'] = $request->religion;
				 if(isset($request->marital_status))
					$bank['marital_status'] = $request->marital_status;
				 if(isset($request->no_of_children))
					$bank['no_of_children'] = $request->no_of_children;
					if(isset($request->contactperson_name))
					$bank['contactperson_name'] = $request->contactperson_name;
					if(isset($request->contactperson_no))
					$bank['contactperson_no'] = $request->contactperson_no;
					if(isset($request->relationship))
					$bank['relationship'] = $request->relationship;
				$result = $this->profileModel->editDetails($bank,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Personal Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Personal Details Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Personal Details.");
		}
    }

        //Edit User Education Details details
     public function _editEducationDetails($request)
    {
		$bank = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Education Details. If User has the permission for that. */
           $user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($user_id))
			{
				$user_where = array(
					'user_education.user_id' => $user_id
				);

				if(isset($request->education_degree))
					$bank['education_degree'] = $request->education_degree;
				if(isset($request->company_name))
					$bank['company_name'] = $request->company_name;
				if(isset($request->location))
					$bank['location'] = $request->location;
				if(isset($request->job_position))
					$bank['job_position'] = $request->job_position;
				if(isset($request->period_from))
					$bank['period_from'] = $request->period_from;
				if(isset($request->period_to))
					$bank['period_to'] = $request->period_to;
				if(isset($request->institution))
					$bank['institution'] = $request->institution;
				if(isset($request->subject))
					$bank['subject'] = $request->subject;
				if(isset($request->start_date))
					$bank['start_date'] = $request->start_date;
	           	if(isset($request->end_date))
					$bank['end_date'] = $request->end_date;
				if(isset($request->grade))
					$bank['grade'] = $request->grade;


				$result = $this->profileModel->editeducation($bank,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Education Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, User Education Details Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit User Education Details.");
		}
    }
 public function _editEmployeeEducation($request)
    {
		$bank = array();
		$module_name = 'profile';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Branchs.. If User has the permission for that. */
           $user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(isset($request->user_id))
			{
				$user_where = array(
					'user_education.user_id' => $request->user_id
				);

				if(isset($request->education_degree))
					$bank['education_degree'] = $request->education_degree;
				if(isset($request->company_name))
					$bank['company_name'] = $request->company_name;
				if(isset($request->location))
					$bank['location'] = $request->location;
				if(isset($request->job_position))
					$bank['job_position'] = $request->job_position;
				if(isset($request->period_from))
					$bank['period_from'] = $request->period_from;
				if(isset($request->period_to))
					$bank['period_to'] = $request->period_to;
				if(isset($request->institution))
					$bank['institution'] = $request->institution;
				if(isset($request->subject))
					$bank['subject'] = $request->subject;
				if(isset($request->start_date))
					$bank['start_date'] = $request->start_date;
	           	if(isset($request->end_date))
					$bank['end_date'] = $request->end_date;
				if(isset($request->grade))
					$bank['grade'] = $request->grade;


				$result = $this->profileModel->editeducation($bank,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Education Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, User Education Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit User Education.");
		}
    }

     
    
  // view  user some api's
  public function _viewGenderPublic($request)		
    {				
				$result = $this->profileModel->viewGender();
				return $result;
	}
  public function _viewReligionPublic($request)		
    {				
				$result = $this->profileModel->viewReligion();
				return $result;
	}
  public function _viewMartialPublic($request)		
    {				
				$result = $this->profileModel->viewMartialStatus();
				return $result;
	}
 public function _viewNationalityPublic($request)		
    {				
				$result = $this->profileModel->viewnationality();
				return $result;
	}
  public function _viewRelationnamePublic($request)		
    {				
				$result = $this->profileModel->viewRelation();
				return $result;
	}
}


















