<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: ProfileModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the ProfileModel.
*/

class ProfileModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    // add Notification
    public function addNotification($data)
    {
    $this->db->insert('user_notification',$data);
    $inser_id = $this->db->insert_id();
    if($inser_id != 0)
    {
      return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    // view Notification
     public function viewNotificationWhere($where2)
    {
        $this->db->select('user_notification.id,user_notification.message,user_notification.sender_name,user_notification.created_at');
        $this->db->from('user_notification');
          
        $this->db->where($where2);
        $this->db->where('status', '0');
        $query = $this->db->get();
        return $query->result();
    }

     // view Notification
     public function viewNotificationWhereread($where2)
    {
        $this->db->select('user_notification.id,user_notification.message,user_notification.sender_name,user_notification.created_at');
        $this->db->from('user_notification');
       // $this->db->join('user_details','user_details.user_id = user_notification.user_id'); 
        $this->db->where($where2);
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }
    
     // view Notification Total
     public function viewNotificationTotatWhere($where2)
    {
        $this->db->select('count(*) as Notification');
        $this->db->from('user_notification');
          
        $this->db->where($where2);
        $this->db->where('status', '0');
        $query = $this->db->get();
        return $query->result();
    }
    
    // add Education Dertails
    public function addEducation($data)
    {
    $this->db->insert('user_education',$data);
    $inser_id = $this->db->insert_id();
    if($inser_id != 0)
    {
      return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     public function editEducationdeta($data,$where)
    {
    $this->db->where($where);
        $this->db->update('user_education',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editNotification($data,$where)
    {
    $this->db->where($where);
        $this->db->update('user_notification',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
     public function viewEducationWhere($where)
    {
        $this->db->select('*');
        $this->db->from('user_education');
        
     $this->db->where($where);
    $query = $this->db->get();
     return $query->result();
    }
    // Deleet Experience Data
    public function deletedExperience($experience_where)
    {
     $this->db->where($experience_where);
    $this->db->delete('user_previouscompany');
    if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
     // Delete Education Dayta
    public function deleteEducationData($where_education)
    {
        $this->db->where($where_education);
        $this->db->delete('user_education');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // add Experience Details
    public function addExperience($data)
    {
     $this->db->insert('user_previouscompany',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
     public function editExperience($data,$where)
    {
    $this->db->where($where);
        $this->db->update('user_previouscompany',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
     public function viewExperienceWhere($where)
    {
        $this->db->select('*');
        $this->db->from('user_previouscompany');
        
     $this->db->where($where);
    $query = $this->db->get();
     return $query->result();
    }
 public function viewUseryz($profile_where)
    {
        
        $this->db->select('*');
        $this->db->from('users');
     $this->db->where($profile_where);
    $query = $this->db->get();
     return $query->result();
   
    }
   public function viewUser()
    {
        
        $this->db->select('*');
        $this->db->from('users');
      
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $query = $this->db->get();
        return $query->result();
   
    }

    public function viewUserWhere($where)
    {
        $this->db->select('*');
        $this->db->from('users');
        
         $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
         $this->db->join('user_personal','user_personal.user_id = users.id');
         $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
     $this->db->where($where);
    $query = $this->db->get();
     return $query->result();
    }
    public function viewUserWhereEmployee($where)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('user_details','user_details.user_id = users.id');
     $this->db->where($where);
    $query = $this->db->get();
     return $query->result();
    }
  public function editProfile($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    //Edit User Details
public function editUserDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

     //Edit User Details
public function editUserDetails2($data,$where)
{
    $this->db->where($where);
    $this->db->update('user_details',$data);
    
    if($this->db->affected_rows() != 1) 
    {
       return FALSE;
    }
    else 
    {
        return TRUE;
    }
}
// Edit Relative Details
    public function editRelativeDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_emergency_contact',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Edit Personal Details
    public function editDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_personal',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function edit_BankDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users_bank_details',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    //edit user Education where conditioneditFamily
     public function editeducation($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_education',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
      //edit user Education where conditioneditFamily
     public function editFamily($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users_family',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
    //view Some User Date
     public function viewGender()
    {
        
        $this->db->select('*');
        $this->db->from('user_gender');
      
       
        $query = $this->db->get();
        return $query->result();
   
    }
    //view Some User Date
     public function viewReligion()
    {
        
        $this->db->select('*');
        $this->db->from('user_religion');
      
       
        $query = $this->db->get();
        return $query->result();
   
    }
    public function viewMartialStatus()
    {
    
    $this->db->select('*');
    $this->db->from('user_martial_status');
    $query = $this->db->get();
    return $query->result();
    }
     public function viewnationality()
    {
    $this->db->select('*');
    $this->db->from('user_nationality');
    $query = $this->db->get();
    return $query->result();
    }
     public function viewRelation()
    {
    $this->db->select('*');
    $this->db->from('user_relationname');
    $query = $this->db->get();
    return $query->result();
    }
}