<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: DesignationModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Designations.
*/

class DesignationModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
// Designation Name All ready present check Availabilty
public function DesignationAvailablity($where_designation)
{
 $query = $this->db->get_where('company_designations',$where_designation);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
}
    public function addDesignation($data)
    {
        $this->db->insert('company_designations',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteDesignation($where_designation)
    {
        $this->db->where($where_designation);
        $this->db->delete('company_designations');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editDesignation($data,$where)
    {
        $this->db->where($where);
        $this->db->update('company_designations',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewDesignation()
    {
        $this->db->select('company_designations.id as designation_id, company_designations.designation_name, company_designations.department_id, company_departments.department_name, company_departments.branch_id, company_branches.branch_name, company_branches.branch_location');
        $this->db->from('company_designations');
        $this->db->join('company_departments', 'company_departments.id = company_designations.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
         $this->db->where('company_branches.is_active', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewDesignationWhere($where)
    {
        $this->db->select('company_designations.id as designation_id, company_designations.designation_name, company_designations.department_id, company_departments.department_name, company_departments.branch_id, company_branches.branch_name, company_branches.branch_location');
        $this->db->from('company_designations');
        $this->db->join('company_departments', 'company_departments.id = company_designations.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
         $this->db->where('company_branches.is_active', 1);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewDepartmentWhere($department_where)
    {
        $this->db->select('company_departments.id as department_id, company_departments.department_name, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location');
        $this->db->from('company_departments');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
        $this->db->where($department_where);
            $this->db->where('company_branches.is_active', 1);
        $query = $this->db->get();
        return $query->result();
    }
}