<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Designation.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the Designation.
*/

class Designation extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('designationModel');
	}
	// check Designation Availability
public function _checkDesignationAvailablity($request)
{
$module_name = 'designation';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the Department Location. If User has the permission for that. */

			if(isset($request->designation_name) AND isset($request->department_id))
			{
				$where_designation = array(
				'designation_name' => $request->designation_name,
					'department_id' => $request->department_id
				);

				$result = $this->designationModel->DesignationAvailablity($where_designation);
				if($result)
					return array("message" => $request->designation_name." is Available.");
				else
					$this->api->_sendError("Designation Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Designation.");
		}
}
	public function _addDesignation($request)
    {		
		$module_name = 'designation';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Designations.. If User has the permission for that. */

			if(isset($request->designation_name) AND isset($request->department_id))
			{
      $status = $this->_checkDesignationAvailablity($request);
				$designation = array(
					'designation_name' => $request->designation_name,
					'department_id' => $request->department_id
				);
				$result = $this->designationModel->addDesignation($designation);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Designation Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Designation Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Designation.");
		}
    }

    public function _deleteDesignation($request)
    {		
		$module_name = 'designation';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Designations.. If User has the permission for that. */

			if(isset($request->designation_id))
			{
				$where_designation = array(
					'id' => $request->designation_id,
				);
				$result = $this->designationModel->deleteDesignation($where_designation);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Designation Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Designation Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Designation.");
		}
    }

    public function _editDesignation($request)
    {		
		$designation = array();
		$module_name = 'designation';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$designation_where = array();
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Add the Designations.. If User has the permission for that. */

			if(isset($request->designation_id))
			{
				$designation_where = array(
					'id' => $request->designation_id
				);

				if(isset($request->designation_name))
					$designation['designation_name'] = $request->designation_name;
              // $status = $this->_checkDesignationAvailablity($request);
				if(isset($request->department_id))
					$designation['department_id'] = $request->department_id;
				
				$result = $this->designationModel->editDesignation($designation,$designation_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Designation Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Designation Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Designation.");
		}
    }

    public function _viewDesignation($request)
    {		
		$module_name = 'designation';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$designation_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Designations.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->designation_id)){
					$designation_where['company_designations.id'] = $request->designation_id;
           $result = $this->designationModel->viewDesignationWhere($designation_where);
					return $result;                                             
     }
     else	if($request->department_id === 'all'){
           $result = $this->designationModel->viewDesignation();
					return $result;                                                      
          }
			else	if(isset($request->department_id)){
					$designation_where['company_designations.department_id'] = $request->department_id;
             $result = $this->designationModel->viewDesignationWhere($designation_where);
					return $result;                                                      
          }
				else
				{
					$result = $this->designationModel->viewDesignation();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Designation.");
		}
    }
    
     public function _viewDesignationSelect($request)
    {		
		$module_name = 'designation';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$designation_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Designations.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->designation_id)){
					$designation_where['company_designations.id'] = $request->designation_id;
           $result = $this->designationModel->viewDesignationWhere($designation_where);
					return $result;                                             
     }
				else
				{
					$result = $this->designationModel->viewDesignation();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Designation.");
		}
	}
	
	public function _viewDesignationEmployee($request)
    {		
		$module_name = 'designation';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$designation_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Designations.. If User has the permission for that. */

			if(TRUE)
			{
			           
				//	$designation_where['company_designations.id'] = $request->designation_id;
					$user = $this->api->_GetTokenData();
				   $userbranch_id = $user['branch_id'];
				   $where_branch = array('branch_id'=>$userbranch_id);
				 $returnData =   $this->designationModel->viewDepartmentWhere($where_branch);
				 foreach($returnData as $result)
				 {
					 $department_ids[] = $result->department_id;
				 }
				 
				if(!empty($department_ids))
					$where = 'company_designations.department_id IN ('.implode(',',$department_ids).')';
				else
					$where = 'company_designations.department_id = 0';

                    $result = $this->designationModel->viewDesignationWhere($where);
					return $result;                                             
   
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Designation.");
		}
    }
}
