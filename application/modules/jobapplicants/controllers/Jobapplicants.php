<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Jobs Applicants.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the Jobs.
*/

class Jobapplicants extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('jobApplicantsModel');
	}
	
	/* -------------------------------------------------------------------------- */
	/*                            This is A Public API                            */
	/* -------------------------------------------------------------------------- */

	

    public function _changeStatusOfJobApplicant($request)
    {		
		$jobApplicant = array();
		$module_name = 'job_applicants';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we change the status of job applicant .. If User has the permission for that. */

			if(isset($request->job_applicant_id) AND isset($request->status))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$job_applicant_where = array(
					'id' => $request->job_applicant_id
				);

				$jobApplicant = array(
					'status' => $request->status,
					'modify_by' => $user_id
				);
				
				$result = $this->jobApplicantsModel->editJobApplicant($jobApplicant,$job_applicant_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Applicant Status Changed Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Applicant Status Not Changed. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Job Applicant.");
		}
    }
// view logged in refereal by data
 public function _viewReferredData($request)
    {		
		
			/* Here we View the user job_applicants. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$job_applicant_where['job_applicants.referral_by'] = $user_id;				
		   	$result = $this->jobApplicantsModel->viewJobApplicantWhere($job_applicant_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
// view job applicants
    public function _viewJobApplicant($request)
    {		
		$module_name = 'job_applicants';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$job_applicant_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Job Applicants.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->job_applicant_id))
					$job_applicant_where['job_applicants.id'] = $request->job_applicant_id;
				if(isset($request->job_id))
					$job_applicant_where['job_applicants.job_id'] = $request->job_id;
				if(isset($request->job_applicant_status_id))
					$job_applicant_where['job_applicants.job_applicant_status_id'] = $request->job_applicant_status_id;
				
				if(!empty($job_applicant_where))
				{
					$result = $this->jobApplicantsModel->viewJobApplicantWhere($job_applicant_where);
					return $result;
				}
				else
				{
					$result = $this->jobApplicantsModel->viewJobApplicant();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Job Applicants.");
		}
	}

	public function _downloadApplicantResume($request)
    {		
		$module_name = 'job_applicants';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we download the Job Applicants.. If User has the permission for that. */

			if(isset($request->job_applicant_id))
			{
				$job_applicant_where = array(
					'job_applicants.id' => $request->job_applicant_id
				);

				$result = $this->jobApplicantsModel->viewJobApplicantResume($job_applicant_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Jobs Applicants Resumes.");
		}
	}

	/* -------------------------------------------------------------------------- */
	/*           This Is Available To All Users No Module Check Required          */
	/* -------------------------------------------------------------------------- */

	public function _referJobApplicant($request)
    {		
		if(isset($request->job_id) AND isset($request->first_name) AND isset($request->last_name) AND isset($request->phone) AND isset($request->email))
		{
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];

			$jobApplicant = array(
				'job_id' => $request->job_id,
				'first_name' => $request->first_name,
				'last_name' => $request->last_name,
				'phone' => $request->phone,
				'email'	=> $request->email,
				'job_applicant_status_id'	=>	1,
				'is_refferal' => 1,
				'referral_by' => $user_id
			);

			$result = $this->jobApplicantsModel->addJobApplicant($jobApplicant);
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'Job Applicant Added Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, Job Applicant Not Added. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
    }
}
