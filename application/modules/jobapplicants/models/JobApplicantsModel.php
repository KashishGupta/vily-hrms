<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: JobsModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Jobs.
*/

class JobApplicantsModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function addJobApplicant($data)
    {
        $this->db->insert('job_applicants',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteJobApplicant($where_job)
    {
        $this->db->where($where_job);
        $this->db->delete('job_applicants');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editJobApplicant($data,$where)
    {
        $this->db->where($where);
        $this->db->update('job_applicants',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewJobApplicant()
    {
        $this->db->select('job_applicants.id as job_applicant_id,job_applicants.status, job_applicants.first_name,job_applicants.experience,job_applicants.current_location,job_applicants.current_designation,job_applicants.highest_education,job_applicants.skills,job_applicants.current_company,job_applicants.education, job_applicants.email, job_applicants.phone, job_applicants.apply_date, job_applicants.resume, job_applicant_status.id as job_applicant_status_id, job_applicant_status.job_applicant_status as applicant_status, job_applicant_status.status_color as applicant_status_color ,jobs.id as job_id, jobs.job_number, company_departments.department_name, company_branches.branch_name, company_branches.branch_location, job_types.job_type, job_types.type_color');
        $this->db->from('job_applicants');
        $this->db->join('jobs', 'job_applicants.job_id = jobs.id');
        $this->db->join('job_applicant_status', 'job_applicants.job_applicant_status_id = job_applicant_status.id');
        $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
        $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
        //$this->db->group_by('jobs.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewJobApplicantWhere($where)
    {
        $this->db->select('job_applicants.id as job_applicant_id,users.first_name as referredby, job_applicants.first_name,job_applicants.experience,job_applicants.current_location,job_applicants.current_designation,job_applicants.highest_education,job_applicants.skills,job_applicants.current_company,job_applicants.education, job_applicants.email, job_applicants.phone, job_applicants.apply_date, job_applicants.resume, job_applicant_status.id as job_applicant_status_id, job_applicant_status.job_applicant_status as applicant_status, job_applicant_status.status_color as applicant_status_color ,jobs.id as job_id, jobs.job_number, company_departments.department_name, company_branches.branch_name, company_branches.branch_location, job_types.job_type, job_types.type_color,job_applicants.status');
        $this->db->from('job_applicants');
        
        $this->db->join('jobs', 'job_applicants.job_id = jobs.id');
        $this->db->join('users', 'job_applicants.referral_by = users.id');
        $this->db->join('job_applicant_status', 'job_applicants.job_applicant_status_id = job_applicant_status.id');
        $this->db->join('job_types', 'job_types.id = jobs.job_type_id');
        $this->db->join('company_departments', 'company_departments.id = jobs.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
        $this->db->where($where);
        //$this->db->group_by('jobs.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewJobApplicantResume($where)
    {
        $this->db->select('job_applicants.id as job_applicant_id, job_applicants.first_name, job_applicants.education, job_applicants.resume as resume_link');
        $this->db->from('job_applicants');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
}