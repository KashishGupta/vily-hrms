<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Offboarding.php
	Date: July 03, 2020
	Info: This is the main class which is hold all the Functions of the Offboarding.
*/

class Offboarding extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('offboardingModel');
	}
	
	/* NOTE-----------------------------------------------------------------------------------*/
	/*            This APIs Is Used For User Who Is Add The Offboarding                           */
	/*            Only That User Is Change/Edit and View Who Is Create The Offboarding              */
	/* ---------------------------------------------------------------------------------------*/
 
    // view Offboarding Self 
    public function _SelfOfboarding($request)
    {
   // In This Method Only Logged In User Is View His Query 
		// There is No Functionality Like To View Other User
		
			/* Here we View the Query.. If User has the permission for that. */
		
			if(TRUE)
			{
		      $user = $this->api->_GetTokenData();
			  $user_id = $user['id'];
              $whereSelf = array('user_id'=>$user_id);
              $result = $this->offboardingModel->viewOffboardingWhere($whereSelf);
			  return $result;
       
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
    public function _viewOffboarding($request)
    {		
		// In This Method Only Logged In User Is View His Query 
		// There is No Functionality Like To View Other User
		$module_name = 'offboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$offboarding_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Query.. If User has the permission for that. */
		
			if(TRUE)
			{
				if(isset($request->user_id))
				{
				$offboarding_where['offboarding_users.user_id'] = $request->user_id;
                $result = $this->offboardingModel->viewOffboardingWhere($offboarding_where);
				return $result;
				}
                else if(isset($request->offboarding_id))
				{
				$offboarding_where['offboarding_users.id'] = $request->offboarding_id;
                $result = $this->offboardingModel->viewOffboardingWhere($offboarding_where);
				return $result;
				}
				else
        {
        $result = $this->offboardingModel->viewOffboarding();
				return $result;
        }
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Offboarding.");
		}
	}
	//  Delete Offboarding Letter
    public function _deleteOffboarding($request)
    {
		$module_name = 'offboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Onboarding. If User has the permission for that. */
			if(isset($request->offboarding_id))
			{
				$where_offboarding = array(
					'id' => $request->offboarding_id,
				);
				$result = $this->offboardingModel->offboardingDelete($where_offboarding);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Offboarding Letter Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Offboarding Document Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Offboarding Letter.");
		}
    }
   // view   Offboarding For Users
   public function _viewOffboardingUser($request)
    {	
			
		$module_name = 'offboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(!empty($request->user_id == 01))
				{
					$user_ids = $this->users->_getUserId();
				
					if(!empty($user_ids))
					$where = 'offboarding_status.user_id IN ('.implode(',',$user_ids).')  AND offboarding_status.status = 0';
				else
					$where = 'offboarding_status.user_id = 0  AND offboarding_status.status = 0';
					$result = $this->offboardingModel->viewOffboardingUser($where);
					return $result;
				}
				else
				{

				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;	
				

				$user_ids = $this->users->_getUserId();
				
				if(!empty($user_ids))
					$where = 'offboarding_status.user_id IN ('.implode(',',$user_ids).')  AND offboarding_status.status = 0';
				else
					$where = 'offboarding_status.user_id = 0  AND offboarding_status.status = 0';

				if(!empty($users_where))
				{
					$result = $this->offboardingModel->viewUserOffboardingWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->offboardingModel->viewOffboardingUser($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}

	// view Offboarding For completed
	public function _viewOffboardingCompleteFilter($request)
	{	
			
		$module_name = 'offboarding';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(!empty($request->user_id == 01))
				{
					$user_ids = $this->users->_getUserId();
				
					if(!empty($user_ids))
					$where = 'offboarding_status.user_id IN ('.implode(',',$user_ids).')  AND offboarding_status.status = 1';
				else
					$where = 'offboarding_status.user_id = 0  AND offboarding_status.status = 1';
					$result = $this->offboardingModel->viewOffboardingUser($where);
					return $result;
				}
				else
				{

				if(isset($request->user_id))
					$users_where['offboarding_status.user_id'] = $request->user_id;	
				$user_ids = $this->users->_getUserId();
				
				if(!empty($user_ids))
					$where = 'offboarding_status.user_id IN ('.implode(',',$user_ids).')  AND offboarding_status.status = 1';
				else
					$where = 'offboarding_status.user_id = 0  AND offboarding_status.status = 1';

				if(!empty($users_where))
				{
					$result = $this->offboardingModel->viewUserOffboardingWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->offboardingModel->viewOffboardingUser($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}
	 // view   Offboarding For Users
	 public function _viewOffboardingUserCompleted($request)
	 {	
			 
		 $module_name = 'offboarding';
		 $permissions = $this->users->_checkPermission($module_name);
		 foreach($permissions as $permission){}
		 $users_where = array();
		 
		 if($permission->can_view == 'Y')
		 {
			 /* Here we View the userss.. If User has the permission for that. */
 
			 if(TRUE)
			 {
				 if(isset($request->user_id))
					 $users_where['offboarding_status.user_id'] = $request->user_id;	
				 $user_ids = $this->users->_getUserId();
				 
				 if(!empty($user_ids))
					 $where = 'offboarding_status.user_id IN ('.implode(',',$user_ids).')  AND offboarding_status.status = 1';
				 else
					 $where = 'offboarding_status.user_id = 0  AND offboarding_status.status = 1';
 
				 if(!empty($users_where))
				 {
					 $result = $this->offboardingModel->viewUserOffboardingWhere($users_where,$where);
					 return $result;
				 }
				 else
				 {
					 $result = $this->offboardingModel->viewOffboardingUser($where);
					 return $result;
				 }
		
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		 }
		 else
		 {
			 $this->api->_sendError("Sorry, You Have No Permission To View users.");
		 }
	 }
	
}
