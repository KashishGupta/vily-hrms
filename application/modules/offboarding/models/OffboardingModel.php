<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: OffboardingModel.php
	Date: July 03, 2020
	Info: This is the main class which is hold all the Models of the Queries.
*/

class OffboardingModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    // Edit offboarding Document
    public function EditoffboardingStatus($data,$where)
    {
        $this->db->where($where);
        $this->db->update('offboarding_status',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

// add offboarding Document
  public function Insertoffboarding($data)
    {
        $this->db->insert('offboarding_users',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function viewOffboarding()
    {
    $this->db->select('*');
    $this->db->from('offboarding_users');
    $quesy = $this->db->get();
    return  $query->result();
    }
    public function viewOffboardingWhere($offboarding_where)
    {
       
        $this->db->select('*');
        $this->db->from('offboarding_users');
        $this->db->where($offboarding_where);
        $query = $this->db->get();
        return $query->result();
    }

// Delete Offboarding Id
   public function offboardingDelete($where_offboarding)
    {
        $this->db->where($where_offboarding);
        $this->db->delete('offboarding_users');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // viewUsers For Offboarding
    public function viewOffboardingUser($where2)
    {
      
        $this->db->select('users.id,users.first_name,users.emp_id,user_details.user_image,users.phone,users.email,company_designations.designation_name');
        $this->db->from('offboarding_status');
         $this->db->join('users', 'users.id = offboarding_status.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
// View Where Offboarding For Users
    public function viewUserOffboardingWhere($where,$where2)
    {
       $this->db->select('users.id,users.first_name,users.emp_id,user_details.user_image,users.phone,users.email,company_designations.designation_name');
       $this->db->from('offboarding_status');
       $this->db->join('users', 'users.id = offboarding_status.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->where($where);
        $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
}