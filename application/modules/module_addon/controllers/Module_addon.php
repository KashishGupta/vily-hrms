<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Module.php
	Date: September 17, 2020
	Info: This is the main class which is hold all the Functions of the Module.
*/

class Module_addon extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('moduleModel');
	}
	

	// change status Addons
	public function _editModulestatus($request)
    {
		$moduleStatus = array();
		$module_name = 'module_addon';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Module Status.. If User has the permission for that. */

			if(isset($request->module_id))
			{
				$where_module_status = array(
					'id' => $request->module_id
				);

				if(isset($request->module_status))
					$moduleStatus['status'] = $request->module_status;

				$result = $this->moduleModel->editFacialStatus($moduleStatus,$where_module_status);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Module Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Module Status Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Module Status.");
		}
	}
	
	// change status Addons
	public function _editPolicyStatus($request)
    {
		$moduleStatus = array();
		$module_name = 'module_addon';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Module Status.. If User has the permission for that. */

			if(isset($request->policy_id))
			{
				$where_module_status = array(
					'id' => $request->policy_id
				);

				if(isset($request->policy_status))
					$moduleStatus['status'] = $request->policy_status;

				$result = $this->moduleModel->editFacialStatus($moduleStatus,$where_module_status);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Module Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Module Status Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Module Policy.");
		}
	}
	// change status Addons
	public function _editActivityStatus($request)
    {
		$moduleStatus = array();
		$module_name = 'module_addon';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Module Status.. If User has the permission for that. */

			if(isset($request->activity_id))
			{
				$where_module_status = array(
					'id' => $request->activity_id
				);

				if(isset($request->activity_status))
					$moduleStatus['status'] = $request->activity_status;

				$result = $this->moduleModel->editFacialStatus($moduleStatus,$where_module_status);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Module Activity Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Module Status Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Module Policy.");
		}
    }
	// view Modules
	public function _viewsModules($request)
    {	
			
		$module_name = 'module_addon';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_module = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Modules .. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->module_id))
				{
					$where_module['id'] = $request->module_id;
					$result = $this->moduleModel->viewModuleWhere($where_module);
					return $result;
				}
				else
				{
					$result = $this->moduleModel->viewModule();
					return $result;
				
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Modules.");
		}
	}
	
		// view Modules
		public function _viewActivity($request)
		{	
				
			$module_name = 'module_addon';
			$permissions = $this->users->_checkPermission($module_name);
			foreach($permissions as $permission){}
			$where_module = array();
			
			if($permission->can_view == 'Y')
			{
				/* Here we View the Modules .. If User has the permission for that. */
	
				if(TRUE)
				{
					if(isset($request->activity_id))
					{
						$where_module['id'] = $request->activity_id;
						$result = $this->moduleModel->viewModuleWhere($where_module);
						return $result;
					}
					else
					{
						$result = $this->moduleModel->viewModule();
						return $result;
					
					}
				}
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, You Have No Permission To View Modules.");
			}
		}

	// view Modules
	public function _viewPolicies($request)
    {	
			
		$module_name = 'module_addon';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_module = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Modules .. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->module_id))
				{
					$where_module['id'] = $request->module_id;
					$result = $this->moduleModel->viewModuleWhere($where_module);
					return $result;
				}
				else
				{
					$result = $this->moduleModel->viewModule();
					return $result;
				
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Modules.");
		}
    }
}
