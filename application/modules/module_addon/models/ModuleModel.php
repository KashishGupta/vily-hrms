<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: ModuleModel.php
	Date: September 17, 2020
	Info: This is the main class which is hold all the Models of the ModuleModel.
*/

class ModuleModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    // view Modules
    public function viewModule()
    {
        $this->db->select('*');
        $this->db->from('facial_recognition');
        $query = $this->db->get();
        return $query->result();
    }
     // view Modules
     public function viewPolicy()
     {
         $this->db->select('*');
         $this->db->from('facial_recognition');
         $query = $this->db->get();
         return $query->result();
     }
// view Modules where
    public function viewModuleWhere($where)
    {
        $query = $this->db->get_where('facial_recognition', $where);
        return $query->result();
    }

    // modules
    public function editFacialStatus($data,$where)
    {
        $this->db->where($where);
        $this->db->update('facial_recognition',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

}