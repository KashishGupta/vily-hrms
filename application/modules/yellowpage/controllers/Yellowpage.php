<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Yellowpage.php
	Date: November 19, 2020
	Info: This is the main class which is hold all the Functions of the users.
*/

class Yellowpage extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('yellowPageModel');	
	}
 
   // view Yellow Page
    public function _viewYellowPage($request)
    {		
		
		$module_name = 'yellow_pages';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the users.. If User has the permission for that. */

			if(TRUE)
			{
				if(!empty($request->user_id == 01))
				{
					$result = $this->yellowPageModel->viewYellowPagewhere();
					return $result;
				}else{
				if(isset($request->department_id))
					$holiday_where['users.department_id'] = $request->department_id;
				if(isset($request->designation_id))
					$holiday_where['users.designation_id'] = $request->designation_id;
				if(isset($request->emp_id))
					$holiday_where['users.emp_id'] = $request->emp_id;
                 if(isset($request->user_id))
					$holiday_where['users.id'] = $request->user_id;
				if(isset($request->user_code))
					$holiday_where['users.user_code'] = $request->user_code;
				if(!empty($holiday_where))
				{
					$result = $this->yellowPageModel->viewYellowPage($holiday_where);
					return $result;
				}
				
				else
				{
					$result = $this->yellowPageModel->viewYellowPagewhere();
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Yellow Pages.");
		}
	}
   // view Yellow Page
   public function _viewYellowPageUsers($request)
   {		
	   
	   $module_name = 'yellow_pages';
	   $permissions = $this->users->_checkPermission($module_name);
	   foreach($permissions as $permission){}
	   
	   if($permission->can_view == 'Y')
	   {
		   /* Here we View the users.. If User has the permission for that. */

		   if(TRUE)
		   {
			 
			   if(isset($request->department_id))
				   $holiday_where['users.department_id'] = $request->department_id;
			   if(isset($request->designation_id))
				   $holiday_where['users.designation_id'] = $request->designation_id;
			   if(isset($request->emp_id))
				   $holiday_where['users.emp_id'] = $request->emp_id;
				if(isset($request->user_id))
				   $holiday_where['users.id'] = $request->user_id;
			   if(isset($request->user_code))
				   $holiday_where['users.user_code'] = $request->user_code;
			   if(!empty($holiday_where))
			   {
				   $result = $this->yellowPageModel->viewYellowPage($holiday_where);
				   return $result;
			   }
			   
			   else
			   {
				   $result = $this->yellowPageModel->viewYellowPagewhere();
				   return $result;
			   }
			   // if(isset($request->department_id))
			   // 	$user_details['user_company_details.department_id'] = $request->department_id;
			   // $result = $this->yellowPageModel->viewYellowPage($user_details);
			   // return $result;
		 
		   }
		   else
		   {
			   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		   }
	   }
	   else
	   {
		   $this->api->_sendError("Sorry, You Have No Permission To View Yellow Pages.");
	   }
   }



}
