<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap singh
	File Name: YellowPageModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Users.
*/

class YellowPageModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function viewYellowPage($where)
    {
        $this->db->select('*');
        $this->db->from('users');
         $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
         $this->db->join('user_personal','user_personal.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','users.id = user_details.user_id');
        $this->db->where($where);
      
        $query = $this->db->get();
        return $query->result();
    }
     public function viewYellowPagewhere()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        $this->db->join('user_personal','user_personal.user_id = users.id');
       $this->db->join('company_designations','company_designations.id = users.designation_id');
       $this->db->join('company_departments','company_departments.id = users.department_id');
       $this->db->join('company_branches','company_branches.id = users.branch_id');
       $this->db->join('user_details','users.id = user_details.user_id');
        $query = $this->db->get();
        return $query->result();
    }
}