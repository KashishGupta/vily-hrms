<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: ProjectrModel.php
	Date: June 15, 2020
	Info: This is the main class which is hold all the Models of the Projects.
*/

class ProjectsModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    // Add Project
    public function addProject($data)
    {
        $this->db->insert('user_project',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    // Delete Project
     public function deleteProject($where_project)
    {
        $this->db->where($where_project);
        $this->db->delete('user_project');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    //edit Project
     public function editProject($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_project',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
   
         // view projects
     public function viewProjectSelf($user_id)
     {
        
        $query = $this->db->query("select user_project.id,creator_name,creator_usercode,project_name,client_name,start_date,end_date,project_language,project_leader_id,project_description,project_team_name,project_file,status,department_name,branch_name, company_branches.id as branch_id,company_departments.id as department_id from user_project INNER JOIN company_departments ON company_departments.id = user_project.department_id INNER JOIN company_branches ON company_branches.id = company_departments.branch_id where CONCAT(',', project_team, ',') like '%,$user_id,%'");
    return $query->result();
     }
    
  
      // leave And Tickets Merge 
      public function viewLeaderProjectwhere($where)
      {
          $this->db->select('users.first_name');
          $this->db->from('users');
         $this->db->where($where);
           $query = $this->db->get();
          return $query->row();
      }
      // leave And Tickets Merge 
      public function viewLeaveticketwhere($where)
      {
          $this->db->select('users.first_name');
          $this->db->from('users');
         $this->db->where($where);
           $query = $this->db->get();
          return $query->result();
      }

      // view projects
     public function viewProject()
     { 
        $this->db->select('user_project.id,user_project.creator_name,user_project.creator_usercode,user_project.department_id,user_project.project_name,user_project.client_name,user_project.start_date,user_project.end_date,user_project.project_language,
        user_project.project_leader_id,user_project.project_description,user_project.project_team_name,user_project.project_file,user_project.status,company_departments.department_name,company_branches.id as branch_id,company_branches.branch_name');
         $this->db->from('user_project');
        $this->db->join('company_departments', 'company_departments.id = user_project.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
         
         $query = $this->db->get();
         return $query->result();
     }

     // view projects
     public function viewProjectWhere($where)
    { 
        $this->db->select('user_project.id,user_project.creator_name,user_project.creator_usercode,user_project.department_id,user_project.project_name,user_project.client_name,user_project.start_date,user_project.end_date,user_project.project_language,
        user_project.project_leader_id,user_project.project_description,user_project.project_team_name,user_project.project_file,user_project.status,company_departments.department_name,company_branches.id as branch_id,company_branches.branch_name');
         $this->db->from('user_project');
        $this->db->join('company_departments', 'company_departments.id = user_project.department_id');
        $this->db->join('company_branches', 'company_branches.id = company_departments.branch_id');
         $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    // view Total Projects
    public function ViewTotalProjects()
    {
       $this->db->select('count(*) as totalProjects');
       $this->db->from('user_project');
       
        $query = $this->db->get();
        return $query->result();
    }
    
    // view Total Ongoing Projects
    public function ViewOngoingProjects()
    {
       $this->db->select('count(*) as totalOngoing');
       $this->db->from('user_project');
         $this->db->where('status', 1);
        
        $query = $this->db->get();
        return $query->result();
    }
     // view Total Ongoing Projects
    public function ViewPendingProjects()
    {
       $this->db->select('count(*) as totalOngoing');
       $this->db->from('user_project');
         $this->db->where('status', 0);
         
        $query = $this->db->get();
        return $query->result();
    }
     // view Total Ongoing Projects
     public function ViewCompleteProjects()
     {
        $this->db->select('count(*) as totalComplete');
        $this->db->from('user_project');
        
          $this->db->where('status', 2);
         
         $query = $this->db->get();
         return $query->result();
     }
    
    
    }