<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/Slack.php';
/* 
	Project: Willy HRMS
	Author: Pratap  Singh
	File Name: Projects.php
	Date: June 15, 2020
	Info: This is the main class which is hold all the Functions of the users.
*/

class Projects extends MY_Controller {

    public function __construct(){
		parent::__construct();
    	$this->objOfSlack = new slack();
		$this->load->model('projectsModel');
	}
 // add Projects
  public function _addProject($request)
  {
  $module_name = 'projects';
  $permissions = $this->users->_checkPermission($module_name);
  foreach($permissions as $permission){}
  if($permission->can_add == 'Y')
  {
  if(isset($request->project_name) AND isset($request->client_name) AND isset($request->project_team) AND isset($request->start_date) AND isset($request->end_date)  AND isset($request->project_language) AND isset($request->project_leader_id) AND isset($request->project_description) AND isset($request->project_file) AND isset($request->department_id)){
  
  $user = $this->api->_GetTokenData();
        $user_id = $user['first_name'];
        $user_code = $user['user_code'];
        $teamUser = $request->project_team;
        $whereuser = implode(',',$teamUser);
        $where = 'users.id IN ('.implode(',',$teamUser).') ';
        
          $results = $this->projectsModel->viewLeaveticketwhere($where);
          foreach($results as $result)
			 {
				 $user_names[] = $result->first_name;
			 }
			
          $arrProjectTeamName = implode(',',$user_names);
				$project = array(
          'project_name' => $request->project_name,
          'creator_usercode' => $user_code,
					'client_name' => $request->client_name,
					'start_date' => $request->start_date,
					'end_date' => $request->end_date,
					'project_language' => $request->project_language,
					'project_leader_id' => $request->project_leader_id,
					'project_description' => $request->project_description,
					'project_file' => $request->project_file,
          'department_id' => $request->department_id,
          'project_team'=>$whereuser,
          'project_team_name'=>$arrProjectTeamName,
        	'status'	=> 0,
					'creator_name'	=> $user_id
				
				);
      $result = $this->projectsModel->addProject($project);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Projects Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Projects Not Added. Please Try Again!!");
				}
  }
  else
  {
  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
  }
  else
  {
  $this->api->_sendError("Sorry, You Have No Permission To Add Project.");
  }
  }
 // view Projects
 public function _viewProject($request)
 {
		$module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
   if(TRUE){
    if(isset($request->project_id)){
    $project_where['user_project.id'] = $request->project_id;
    $result = $this->projectsModel->viewProjectWhere($project_where);
    return $result;
  }
  else
  {
    $result = $this->projectsModel->viewProject();
    return $result;
  }
   
   }
   else
   {
   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
    }
   else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Project.");
		}
  }

  

  // view Projects
 public function _viewProjectfilter($request)
 {
		$module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
   if(TRUE){
     if ($request->department_id === 'all')
     {
      $user_ids = $this->users->_getUserId();
      if(!empty($user_ids))
        $where = 'user_project.project_leader_id IN ('.implode(',',$user_ids).')';
      else
        $where = 'user_project.project_leader_id = 0';
        $result = $this->projectsModel->viewProject($where);
        return $result;
     }else{
      if(isset($request->department_id))
      $project_where['user_project.department_id'] = $request->department_id;
    if(isset($request->project_id))
    $project_where['user_project.id'] = $request->project_id;
  $user_ids = $this->users->_getUserId();
  if(!empty($user_ids))
    $where = 'user_project.project_leader_id IN ('.implode(',',$user_ids).')';
  else
    $where = 'user_project.project_leader_id = 0';

if(!empty($project_where))
  {
    $result = $this->projectsModel->viewProjectWhere($project_where,$where);
    return $result;
  }
  else
  {
    $result = $this->projectsModel->viewProject($where);
    return $result;
  }
}
   }
   else
   {
   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
    }
   else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Project.");
		}
  }
  public function _viewProjectSelf($request)
  {
   
     if(TRUE)
     {
      $user = $this->api->_GetTokenData();
		$user_id = $user['id'];
	
		$result = $this->projectsModel->viewProjectSelf($user_id);
	    return $result;
     }
     else
     {
      $this->api->_sendError("Sorry, There Is Some Perameter Missing. Please Try Again!!");
     }
   
  }
  // Delete project
  public function _deleteProject($request)
  {
		$module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
     if(isset($request->project_id))
     {
     $where_project = array ('id' =>$request->project_id);
     $result = $this->projectsModel->deleteProject($where_project);
     if($result)
     {
     return array(
						'status' => '1',
						'message' => 'Project Deleted Successfully.'
					);
     }
     else
     {
     $this->api->_sendError("Sorry, Project Not Deleted. Please Try Again!!");
     }
     }
     else
     {
     $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
     }
    }
   else
   {
   	$this->api->_sendError("Sorry, You Have No Permission To Delete Project.");
   }
  }
  // edit Projects
  public function _editProject($request)
  {
  	$module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
	if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Project.. If User has the permission for that. */

			if(isset($request->project_id))
			{
				$whereProject = array(
					'id' => $request->project_id
				);
   
     if(isset($request->project_name))
					$project['project_name'] = $request->project_name;
     if(isset($request->client_name))
					$project['client_name'] = $request->client_name;
     if(isset($request->end_date))
        $project['end_date'] = $request->end_date;
     if(isset($request->start_date))
					$project['start_date'] = $request->start_date;
     if(isset($request->project_file))
					$project['project_file'] = $request->project_file;
     if(isset($request->department_id))
        $project['department_id'] = $request->department_id;
      if(isset($request->project_language))
        $project['project_language'] = $request->project_language;
     if(isset($request->project_leader_id))
					$project['project_leader_id'] = $request->project_leader_id;
     if(isset($request->project_description))
					$project['project_description'] = $request->project_description;
                                          
    $result = $this->projectsModel->editProject($project,$whereProject);
    if($result)
    {
    return array(
						'status' => '1',
						'message' => 'Project Updated Successfully.'
					);
    }
    else
    {
    $this->api->_sendError("Sorry, Project Not Updated. Please Try Again!!");
    }
   }
   else
   {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
   }
   else
   {
   $this->api->_sendError("Sorry, You Have No Permission To Edit Project.");
   }
  }
  // Total Project 
  public function _totalProject($request)
  {
  $module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
   	if($permission->can_view == 'Y')
	 {
   if(TRUE)
   {
    
     
   $result = $this->projectsModel->ViewTotalProjects();
    return $result;
   }
   else
   {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
   }
  else
  {
  $this->api->_sendError("Sorry, You Have No Permission To View Total Project.");
  }
	}
 // Total Project Self
 public function _totalProjectSelf($request)
 {
 $module_name = 'projects';
   $permissions = $this->users->_checkPermission($module_name);
   foreach($permissions as $permission){}
   
    if($permission->can_view == 'Y')
  {
  if(TRUE)
  {
    $user = $this->api->_GetTokenData();
		$user_id = $user['id'];
    $query = $this->db->query("select COUNT(*) as totalProjects from user_project  where CONCAT(',', project_team, ',') like '%,$user_id,%'
    ");
return $query->result(); 
  
  }
  else
  {
   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
  }
  }
 else
 {
 $this->api->_sendError("Sorry, You Have No Permission To View Total Project.");
 }
 }
 // Total Ongoing Project Project 
  public function _totalOngoingProject($request)
  {
  $module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
   	if($permission->can_view == 'Y')
	 {
   if(TRUE)
   {
   
   
   $result = $this->projectsModel->ViewOngoingProjects();
    return $result;
   }
   else
   {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
   }
  else
  {
  $this->api->_sendError("Sorry, You Have No Permission To View Total Ongoing Project.");
  }
	}
 
   // Total Ongoing Project Project Self
   public function _totalOngoingProjectSelf($request)
   {
   $module_name = 'projects';
     $permissions = $this->users->_checkPermission($module_name);
     foreach($permissions as $permission){}
     
      if($permission->can_view == 'Y')
    {
    if(TRUE)
    {
      
    
      $user = $this->api->_GetTokenData();
      $user_id = $user['id'];
      $query = $this->db->query("select COUNT(*) as totalOngoing from user_project  where CONCAT(',', project_team, ',') like '%,$user_id,%' AND status = 1
      ");
  return $query->result(); 
    }
    else
    {
     $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
    }
    }
   else
   {
   $this->api->_sendError("Sorry, You Have No Permission To View Total Ongoing Project.");
   }
   }
  
 // Total Pending Project Project 
  public function _totalPendingProject($request)
  {
  $module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
   	if($permission->can_view == 'Y')
	 {
   if(TRUE)
   {
   
   $result = $this->projectsModel->ViewPendingProjects();
    return $result;
   }
   else
   {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
   }
  else
  {
  $this->api->_sendError("Sorry, You Have No Permission To View Total Pending Project.");
  }
  }
  
  // Total Pending Project Project Self 
  public function _totalPendingProjectSelf($request)
  {
  $module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
   	if($permission->can_view == 'Y')
	 {
   if(TRUE)
   {
    
    
    $user = $this->api->_GetTokenData();
      $user_id = $user['id'];
      $query = $this->db->query("select COUNT(*) as totalPending from user_project  where CONCAT(',', project_team, ',') like '%,$user_id,%' AND status = 0
      ");
  return $query->result();
   }
   else
   {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
   }
  else
  {
  $this->api->_sendError("Sorry, You Have No Permission To View Total Pending Project.");
  }
	}

  // Total complete Project  
  public function _totalCompleteProject($request)
  {
  $module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
   	if($permission->can_view == 'Y')
	 {
   if(TRUE)
   {
   
   $result = $this->projectsModel->ViewCompleteProjects();
    return $result;
   }
   else
   {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
   }
  else
  {
  $this->api->_sendError("Sorry, You Have No Permission To View Total Pending Project.");
  }
  }

  // Total complete Project Project Self 
  public function _totalCompleteProjectSelf($request)
  {
  $module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
   	if($permission->can_view == 'Y')
	 {
   if(TRUE)
   {
    
    
    $user = $this->api->_GetTokenData();
      $user_id = $user['id'];
      $query = $this->db->query("select COUNT(*) as totalComplete from user_project  where CONCAT(',', project_team, ',') like '%,$user_id,%' AND status = 2
      ");
  return $query->result();
   }
   else
   {
    $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
   }
   }
  else
  {
  $this->api->_sendError("Sorry, You Have No Permission To View Total Pending Project.");
  }
	}
  // edit Status Project 
 public function _changeProjectStatus($request)
    {
		$module_name = 'projects';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Edit the Project.. If User has the permission for that. */
			if(isset($request->project_id) AND isset($request->status))
			{
				$project_where = array(
					'id' => $request->project_id
				);
				$projects = array(
					'status' => $request->status
				);
				
				$result = $this->projectsModel->editProject($projects,$project_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Project Status Changed Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Project Status Not Changed. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Project Status.");
		}
    }
 
 
 }