<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Holiday.php
	Date: November 29, 2019
	Info: This is the main class which is hold all the Functions of the Holidays.
*/

class Holiday extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('holidayModel');		
	}
	public function _addHolidayBulk($request)
    {
		
		$branch = array();
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the holiday.. If User has the permission for that. */

			if(isset($request->csvFile))
			{
				$baseimage = $request->csvFile;
				$source_image = str_replace('data:application/vnd.ms-excel;base64,','',$baseimage);
				
				$decoded = base64_decode($source_image);
			
				$arrCsvData=explode("\n",$decoded);
			     array_pop($arrCsvData);
				array_shift($arrCsvData);
				$full_stats = array();
				foreach($arrCsvData as $line) {
					$full_stats[] = str_getcsv($line, "\t");
					foreach($full_stats as $Csv => $arrvalue)
					{
					 foreach($arrvalue as $arrCsv => $arrvaluebreak)
					{
						if (!empty($arrvaluebreak)) { 
						 $explode=explode(',',$arrvaluebreak);	
						 $holiday = array(
							'holiday_title' => $explode[0],
							'holiday_date' => $explode[1],
							'branch_id' => $explode[2]
						);	
						}					}
					}
					//print_r($explode);
				 $this->db->insert('company_holidays',$holiday);
					
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Holiday.");
		}
    }
	public function _addHoliday($request)
    {		
		$holidayData = array();
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Holidays.. If User has the permission for that. */

			if(isset($request->holiday_title) AND isset($request->branch_id) AND isset($request->holiday_date))
			{
      	$holidayStartDate=strtotime($request->holiday_date);
					$dayOfStartLeave = date("l", $holidayStartDate);
      $currentDate = date('d-m-Y');
					$currentDate=strtotime($currentDate);
				/* End Date Conversion */
           if($holidayStartDate < $currentDate){
           	$this->api->_sendError('Holiday Cannot Be Add On Past Date.');
           }
			    $status = $this->users->_checkDateAvailability($request);
				$current_time=date("Y-m-d");
				$holidayData = array(
					'holiday_title' => $request->holiday_title,
					'holiday_date' => $request->holiday_date,
					'branch_id' => $request->branch_id
					
				);
				$result = $this->holidayModel->addHoliday($holidayData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Holiday Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Holiday Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Holiday.");
		}
    }

    public function _deleteHoliday($request)
    {		
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Holiday. If User has the permission for that. */

			if(isset($request->holiday_id))
			{
				$where_holiday = array(
					'id' => $request->holiday_id,
				);
				$result = $this->holidayModel->deleteHoliday($where_holiday);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Holiday Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Holiday Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Holiday.");
		}
    }

    public function _editHoliday($request)
    {
		$holidayData = array();
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Holidays.. If User has the permission for that. */

			if(isset($request->holiday_id))
			{
				$holiday_where = array(
					'id' => $request->holiday_id
				);

				if(isset($request->holiday_title))
					$holidayData['holiday_title'] = $request->holiday_title;
				if(isset($request->holiday_date))
					$holidayData['holiday_date'] = $request->holiday_date;
					$holidayStartDate=strtotime($request->holiday_date);
					$dayOfStartLeave = date("l", $holidayStartDate);
      $currentDate = date('d-m-Y');
					$currentDate=strtotime($currentDate);
				/* End Date Conversion */
           if($holidayStartDate < $currentDate){
           	$this->api->_sendError('Holiday Cannot Be Add On Past Date.');
           }
				$result = $this->holidayModel->editHoliday($holidayData,$holiday_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Holiday Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Holiday Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Holiday.");
		}
    }

    public function _viewHoliday($request)
    {		
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$holiday_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Holiday.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->holiday_id))
				{
					$holiday_where['company_holidays.id'] = $request->holiday_id;
					$result = $this->holidayModel->viewHolidayWhere($holiday_where);
					return $result;
				}
                else if($request->branch_id == 'all')
				{
					$result = $this->holidayModel->viewHoliday();
				return $result;
				}
                else if(isset($request->branch_id))
				{
					$holiday_where['company_holidays.branch_id'] = $request->branch_id;
					$result = $this->holidayModel->viewHolidayWhere($holiday_where);
					return $result;
				}
				else
				{
					$result = $this->holidayModel->viewHoliday();
					return $result;
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Holiday.");
		}
	}
	// view Holiday for edit
	// view Total Holiday 
	public function _viewEditHoliday($request)
    {		
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$holiday_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Holiday.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->holiday_id))
				{
					$holiday_where['company_holidays.id'] = $request->holiday_id;
					$result = $this->holidayModel->viewHolidayWhereEdit($holiday_where);
					return $result;
				}else{
					$result = $this->holidayModel->viewHoliday();
					return $result;
				}
				
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Holiday.");
		}
    }
	// view Total Holiday 
	public function _viewTotalHoliday($request)
    {		
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$holiday_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Holiday.. If User has the permission for that. */

			if(TRUE)
			{
				
					$result = $this->holidayModel->totalHoliday();
					return $result;
				
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Holiday.");
		}
    }
    // view Holiday By Branch Token Data
     public function _viewHolidayBranch($request)
    {		
		$module_name = 'holiday';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$holiday_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Holiday.. If User has the permission for that. */
	        $user = $this->api->_GetTokenData();
			$branch_id = $user['branch_id'];
			if(TRUE)
			{
					$holiday_where['company_holidays.branch_id'] = $branch_id;
			$result = $this->holidayModel->viewHolidayWhere($holiday_where);
			return $result;
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Holiday.");
		}
    }
}
