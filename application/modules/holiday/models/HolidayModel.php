<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Devendra Agarwal
	File Name: HolidayModel.php
	Date: November 29, 2019
	Info: This is the main class which is hold all the Models of the Departments.
*/

class HolidayModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
   function checkDateAvailability($where_date)
    {
        $query = $this->db->get_where('company_holidays',$where_date);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }
    public function addHoliday($data)
    {
        $this->db->insert('company_holidays',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function deleteHoliday($where_holiday)
    {
        $this->db->where($where_holiday);
        $this->db->delete('company_holidays');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editHoliday($data,$where)
    {
        $this->db->where($where);
        $this->db->update('company_holidays',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function viewHoliday()
    {
       
        
   
         $this->db->select('company_holidays.id as holiday_id, company_holidays.holiday_title, company_holidays.holiday_date, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location');
        $this->db->from('company_holidays');
        $this->db->join('company_branches', 'company_branches.id = company_holidays.branch_id');
        $this->db->order_by("holiday_date", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function viewHolidayWhere($where)
    {
	
        $this->db->select('company_holidays.id as holiday_id, company_holidays.holiday_title, company_holidays.holiday_date, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location');
        $this->db->from('company_holidays');
        $this->db->join('company_branches', 'company_branches.id = company_holidays.branch_id');
        $this->db->order_by("holiday_date", "asc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewHolidayWhereEdit($where)
    {
        $this->db->select('company_holidays.id as holiday_id, company_holidays.holiday_title, company_holidays.holiday_date, company_branches.id as branch_id, company_branches.branch_name, company_branches.branch_location');
        $this->db->from('company_holidays');
        $this->db->join('company_branches', 'company_branches.id = company_holidays.branch_id');
        $this->db->order_by("holiday_date", "asc");
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    // count all holiday in branch
    public function totalHoliday()
    {
       $date = new DateTime("now");
       $curr_date = $date->format('Y-m-d ');
       $this->db->select('count(*) as totalHoliday');
       $this->db->from('company_holidays');
       $query = $this->db->get();
       return $query->result();
    }
}