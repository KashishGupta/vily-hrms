<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Leave.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the users.
*/

class Leave extends MY_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('leaveModel');
		$this->load->model('userModel');	
	}
	
	public function _addLeave($request)
    {
		
		$leaveData = array();
		
			/* Here we Add the Leaves.. If User has the permission for that. */
			if(isset($request->user_id) AND isset($request->start_date) AND isset($request->end_date) AND isset($request->leave_policy))
			{
		 					
							 
		  $userLeaveTypeWhere = array(
			'id' => $request->leave_policy
		 );
		// get Leave Policy
		  $user = $this->leaveModel->getLeaveTypes($userLeaveTypeWhere);
		  
		  if($user->back_date == 1)
		  {
		 
				 /* Date Conversion and Other Things */
				 $leaveStartDate=strtotime($request->start_date);
				 $dayOfStartLeave = date("l", $leaveStartDate);

				 $leaveEndDate=strtotime($request->end_date);
				 $dayOfEndLeave = date("l", $leaveEndDate);

				 $currentDate = date('d-m-Y');
				 $currentDate=strtotime($currentDate);
				 /* End Date Conversion */
				 if($leaveStartDate > $currentDate){
				 $this->api->_sendError('Sick Leave Cannot Be Applied On Future Date.');
				 }
				 /* Weekend and Date Ahead Check Start */
				 if($leaveStartDate > $leaveEndDate){
				 $this->api->_sendError('End Date Ahead From Start Date.');
				 }

				 if($dayOfStartLeave == 'Sunday') {
				 $this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
				 }
				 if($dayOfEndLeave == 'Sunday'){
				 $this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
				 }
				 $LeaveHalfstatus = $request->allow_half;
							if($LeaveHalfstatus == 1)
							{
								/* Date Conversion and Other Things */
								$leaveStartDate=strtotime($request->start_date);
								$dayOfStartLeave = date("l", $leaveStartDate);

								$leaveEndDate=strtotime($request->end_date);
								$dayOfEndLeave = date("l", $leaveEndDate);

								$currentDate = date('d-m-Y');
								$currentDate=strtotime($currentDate);
								/* End Date Conversion */
								if($leaveStartDate < $leaveEndDate){
								$this->api->_sendError('Half day leave can be apply for one day.');
								}
								/*if($leaveStartDate > $currentDate){
								$this->api->_sendError('Half Leave Cannot Be Applied On Future Date.');
								}*/
								

								/* Weekend and Date Ahead Check Start */
								if($leaveStartDate > $leaveEndDate){
								$this->api->_sendError('End Date Ahead From Start Date.');
								}

								if($dayOfStartLeave == 'Sunday') {
								$this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
								}
								if($dayOfEndLeave == 'Sunday'){
								$this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
								}
							}
									$Leaveshortstatus = $request->allow_short;
									if($Leaveshortstatus == 1)
									{
											/* Date Conversion and Other Things */
											$leaveStartDate=strtotime($request->start_date);
											$dayOfStartLeave = date("l", $leaveStartDate);

											$leaveEndDate=strtotime($request->end_date);
											$dayOfEndLeave = date("l", $leaveEndDate);

											$currentDate = date('d-m-Y');
											$currentDate=strtotime($currentDate);
											/* End Date Conversion */
											if($leaveStartDate < $leaveEndDate){
												$this->api->_sendError('Short leave can be apply for one day.');
											}
											/*if($leaveStartDate > $currentDate){
											$this->api->_sendError('Short Leave Cannot Be Applied On Future Date.');
											}*/
											
											/* Weekend and Date Ahead Check Start */
											if($leaveStartDate > $leaveEndDate){
											$this->api->_sendError('End Date Ahead From Start Date.');
											}

											if($dayOfStartLeave == 'Sunday') {
											$this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
											}
											if($dayOfEndLeave == 'Sunday'){
											$this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
											}
									}
		 }		
		 else
		 {
			 /* Check Sick Leave */
			 /* Date Conversion and Other Things */
			 $leaveStartDate=strtotime($request->start_date);
			 $dayOfStartLeave = date("l", $leaveStartDate);

			 $leaveEndDate=strtotime($request->end_date);
			 $dayOfEndLeave = date("l", $leaveEndDate);

			 $currentDate = date('d-m-Y');
			 $currentDate=strtotime($currentDate);
			 /* End Date Conversion */
			 if($leaveStartDate < $currentDate){
			 $this->api->_sendError('Leave Cannot Be Applied On Past Date.');
			 }
			 /* Weekend and Date Ahead Check Start */
			 if($leaveStartDate > $leaveEndDate){
			 $this->api->_sendError('End Date Ahead From Start Date.');
			 }
			 if($leaveStartDate < $currentDate){
			 $this->api->_sendError('Start Date Ahead From End Date.');
			 }
			 if($dayOfStartLeave == 'Sunday') {
			 $this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
			 }
			 if($dayOfEndLeave == 'Sunday'){
			 $this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
			 }
			 $LeaveHalfstatus = $request->allow_half;
							if($LeaveHalfstatus == 1)
							{
								/* Date Conversion and Other Things */
								$leaveStartDate=strtotime($request->start_date);
								$dayOfStartLeave = date("l", $leaveStartDate);

								$leaveEndDate=strtotime($request->end_date);
								$dayOfEndLeave = date("l", $leaveEndDate);

								$currentDate = date('d-m-Y');
								$currentDate=strtotime($currentDate);
								/* End Date Conversion */
								if($leaveStartDate < $leaveEndDate){
								$this->api->_sendError('Half day leave can be apply for one day.');
								}
								/*if($leaveStartDate > $currentDate){
								$this->api->_sendError('Half Leave Cannot Be Applied On Future Date.');
								}*/
								if($leaveStartDate < $currentDate){
								$this->api->_sendError('Half Leave Cannot Be Applied On Past Date.');
								}

								/* Weekend and Date Ahead Check Start */
								if($leaveStartDate > $leaveEndDate){
								$this->api->_sendError('End Date Ahead From Start Date.');
								}

								if($dayOfStartLeave == 'Sunday') {
								$this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
								}
								if($dayOfEndLeave == 'Sunday'){
								$this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
								}
							}
									$Leaveshortstatus = $request->allow_short;
									if($Leaveshortstatus == 1)
									{
											/* Date Conversion and Other Things */
											$leaveStartDate=strtotime($request->start_date);
											$dayOfStartLeave = date("l", $leaveStartDate);

											$leaveEndDate=strtotime($request->end_date);
											$dayOfEndLeave = date("l", $leaveEndDate);

											$currentDate = date('d-m-Y');
											$currentDate=strtotime($currentDate);
											/* End Date Conversion */
											if($leaveStartDate < $leaveEndDate){
												$this->api->_sendError('Short leave can be apply for one day.');
											}
											/*if($leaveStartDate > $currentDate){
											$this->api->_sendError('Short Leave Cannot Be Applied On Future Date.');
											}*/
											if($leaveStartDate < $currentDate){
											$this->api->_sendError('Short Leave Cannot Be Applied On Past Date.');
											}
											/* Weekend and Date Ahead Check Start */
											if($leaveStartDate > $leaveEndDate){
											$this->api->_sendError('End Date Ahead From Start Date.');
											}

											if($dayOfStartLeave == 'Sunday') {
											$this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
											}
											if($dayOfEndLeave == 'Sunday'){
											$this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
											}
									}
			  // Leave Apply Before
			  $date = date('Y-m-d');
			  $arrbefore = $user->apply_before_days;
			
			  $arrDays =  date('Y-m-d', strtotime($date. ' + '.$arrbefore.' days'));
			 
			  $leaveDayDate=strtotime($arrDays);
			  $leaveStartDate=strtotime($request->start_date);
			  if($leaveStartDate < $leaveDayDate){
				$this->api->_sendError("Sorry, You can apply $user->policy_name before $arrbefore days");
			  }
		 }
		 
		  $changeBy = $this->api->_dateDiffInDays($request->start_date,$request->end_date);
		  // max balance Days
		  $arrmaxdays = $user->max_balance_days;
		  $diffDay = $changeBy+1;
		  if($arrmaxdays < $diffDay)
		  {
			$this->api->_sendError("Sorry , You can't apply $user->policy_name more than  $arrmaxdays days ");  
		  }
		 
		  // is Date of Joining Considerd
		  
		   $arrconsecutive = $user->is_doj_considered;
		   if($arrconsecutive == 0)
		   {
			 $this->api->_sendError('Sorry, You Cant Apply Leave Date Of Joining Considerd.');  
		   }
		   
		  /* Max Consecutive No. Of Leaves same type */
		  $leaveUser_id =  $request->user_id;
		  $Leavepolicyid =  $request->leave_policy;
		
		$LeaveDate = $request->start_date;
			

	
		$leavecurrent = $this->leaveModel->ViewLeaveMonth($LeaveDate,$Leavepolicyid,$leaveUser_id);
		
		$arrPolicyType=[];
		
		foreach($leavecurrent as $policyType)
		{
			$arrPolicyType[] = $policyType->start_date;
			 			
		}

		$arrcompanyPolicy =  count($arrPolicyType);
	
		
		$consecutiveLeave = $user->max_consecutive;
		
		if($arrcompanyPolicy == $consecutiveLeave)
		{
			$this->api->_sendError("Sorry , You can't apply $user->policy_name more than  $consecutiveLeave days in a month.");	
		}
                 	/* Set Leave Data in Array Start */
					$leaveData = array(
						'user_id' => $request->user_id,
						'start_date' => $request->start_date,
						'end_date' => $request->end_date,
						'leave_policy_id' => $request->leave_policy,
						'status' => 0,
					);
					
					if(isset($request->reason))
						$leaveData['reason'] = $request->reason;
						if(isset($request->allow_half))
						$leaveData['allow_half'] = $request->allow_half;
						if(isset($request->allow_short))
						$leaveData['allow_short'] = $request->allow_short;
						
					



				/* Leave Not Applied On Holiday Check Start */
					$user = $this->api->_GetTokenData();
					$holiday_where = array(
						'company_holidays.branch_id' => $user['branch_id']
					);
					
					$holidays = $this->leaveModel->viewHolidayWhere($holiday_where);
					foreach($holidays as $holiday)
					{

						$holidayDate = strtotime($holiday->holiday_date);
		
						if($leaveStartDate == $holidayDate)
						{
							$this->api->_sendError('Leave Not Apply On Holidays, Select Another Start Date.');
						}
						if($leaveEndDate == $holidayDate)
						{
							$this->api->_sendError('Leave Not Apply On Holidays, Select Another End Date.');
						}
					}
			/*	 Leave Not Applied On Holiday Check End */

				/* Already Apply Leave On That Day Check Start */
					$leave_where['user_leave.user_id'] = $request->user_id;
					$result = $this->leaveModel->viewLeaveWhere2($leave_where);
					$leaveStartDate=strtotime($request->start_date);
					$leaveEndDate=strtotime($request->end_date);
					
					foreach($result as $leave)
					{
						
						$DateBegin = strtotime($leave->start_date);
						$DateEnd = strtotime($leave->end_date);
		               
						if((($leaveStartDate > $DateBegin) && ($leaveStartDate < $DateEnd)) || (($leaveEndDate > $DateBegin) && ($leaveEndDate < $DateEnd)) || (($leaveEndDate == $DateBegin) && ($leaveEndDate == $DateEnd)) || (($leaveStartDate == $DateBegin) && ($leaveStartDate == $DateEnd)) || (($leaveStartDate == $DateEnd) && ($DateBegin == $leaveStartDate)))
						{
							$this->api->_sendError('leave already applied on that particular date.');
						}

						/*if($leave->status == 0)
						{
							$this->api->_sendError('Sorry, Your last leave has not been approved yet.');
						}*/
					}
				/* Already Apply Leave On That Day Check End */				

				/* Data Present For That Particular Leave Type With User In User_Leave_Details Check Start */
					$userLeaveWhere = array(
						'user_id' => $request->user_id,
						'leave_policy_id' => $request->leave_policy
					);
					
					if(!($this->leaveModel->userHasLeaveType($userLeaveWhere)))
					{
						$leaveNumberWhere = array(
							'id' => $request->leave_policy
						);
						$numberOfLeaves = $this->leaveModel->getNumberOfLeaveByType($leaveNumberWhere);
						
						$userLeaveDetails = array(
							'user_id' => $request->user_id,
							'leave_policy_id' => $request->leave_policy,
							'no_of_leave' => $numberOfLeaves->total_per_year
						);
						$detailResult = $this->leaveModel->insertUserLeaveType($userLeaveDetails);
						if(!$detailResult)
						{
							$this->api->_sendError("Sorry, There is Some Internal Settings Error. Please Try Again!!");
						}
					}
					$userLeaveTypeWhere = array(
						'id' => $request->leave_policy
					 );
					// get Leave Policy
					  $user = $this->leaveModel->getLeaveTypes($userLeaveTypeWhere);
					/* Get Leave Details */	
					if($user->negative_balance_allowed  == 0)
					{
						
						$userLeaveWhere = array(
							'leave_policy_id' => $request->leave_policy,
							'user_id' => $request->user_id
						);
						$userLeaveDetail = $this->leaveModel->getleaveDetailscheck($userLeaveWhere);
						$noofLeave = $userLeaveDetail->no_of_leave;
								
						$changeBy2 = $this->api->_dateDiffInDays($request->start_date,$request->end_date);
						$changeBy = $changeBy2+1;
						$finalBalance = $noofLeave - $changeBy;
						//print_r($finalBalance);
						if(($finalBalance < $changeBy) && ($finalBalance < 0))
						{
							$this->api->_sendError('Sorry, Nagative Leave Not Allowed In Your Policy.'); 
							
						}
						
					}	
								
							/* Data Present For That Particular Leave Type With User In User_Leave_Details Check End */
							/* send mail To User Add Leave By Company */
							/* $user = $this->api->_GetTokenData();
							$user_id = $user['first_name'];
							$createdat = date('Y-m-d H:i:s');
							$position="Your Leave Has Beeen Appllied";
							$from="pratapsingh@ezdatechnology.com";


							if($request->leave_type == 1){
							$Leave_status="Planned Leaves" ; 
							}else{
							$Leave_status="Casual Leaves";  
							}
							$apply_to=$request->start_date;
							$from_to=$request->end_date;
							$this->email->from($from);
							$this->email->to("pratapsingh@ezdatechnology.com");
							$this->email->subject($position);
							$this->email->message('
							<br><div style="background-color: #fff;"><div><center> <h2> Ezdat Technology </h2></center><hr> <br>
							<table rules="all" style="border-color: #666;" cellpadding="10">
							<tr style= "background: #cad2d9;" > <td><strong>Applied By:</strong></td>
							<td>'.$user_id.'</td></tr>
							<tr style= "background: #cad2d9;" > <td><strong>Apply On:</strong></td>
							<td>'.$createdat.'</td></tr>
							<tr style= "background: #cad2d9;" > <td><strong>From:</strong></td>
							<td>'.$apply_to.'</td></tr>
							<tr style= "background: #cad2d9;" > <td><strong> To:</strong></td>
							<td>'.$from_to.'</td></tr>
							<tr style= "background: #cad2d9;" > <td><strong>Leave Type:</strong></td>
							<td>'.$Leave_status.'</td></tr>
							<td><strong>Your Account In Ezdat Technology Is Ready. You Can Simply Sign In Now</strong></td></tr>
							</table>
							');
							$this->email->set_mailtype('html');
							$this->email->send();*/
							// Email Send 
							/* After All Check Add Leave */
				
				$result = $this->leaveModel->addLeave($leaveData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Not Added. Please Try Again!!");
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }

    public function _deleteLeave($request)
    {		
		$module_name = 'leave';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Leaves.. If User has the permission for that. */

			if(isset($request->leave_id))
			{
				$where_leave = array(
					'user_leave.id' => $request->leave_id,
				);

				/* Leave Action Is Perform Or Not Check Start */
					$leaves = $this->leaveModel->getLeaveWhere($where_leave);
					foreach($leaves as $leaveDetail) {}

					if($leaveDetail->status != 0)
					{
						$this->api->_sendError("Sorry, Action Already Performed On This Leave.");
					}
				/* Leave Action Is Perform Or Not Check End */

				$result = $this->leaveModel->deleteLeave($where_leave);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Leave Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Leave Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Leave.");
		}
    }

    public function _editLeave($request)
    {
		
			/* Here we Update/Edit the Leaves.. If User has the permission for that. */

			if(isset($request->leave_id) AND isset($request->user_id) AND isset($request->start_date) AND isset($request->end_date) AND isset($request->leave_policy))
			{
				$userLeaveTypeWhere = array(
					'id' => $request->leave_policy
				 );
				// get Leave Policy
				$user = $this->leaveModel->getLeaveTypes($userLeaveTypeWhere);
		  
				if($user->back_date == 1)
				{
			   
					   /* Date Conversion and Other Things */
					   $leaveStartDate=strtotime($request->start_date);
					   $dayOfStartLeave = date("l", $leaveStartDate);
	  
					   $leaveEndDate=strtotime($request->end_date);
					   $dayOfEndLeave = date("l", $leaveEndDate);
	  
					   $currentDate = date('d-m-Y');
					   $currentDate=strtotime($currentDate);
					   /* End Date Conversion */
					   if($leaveStartDate > $currentDate){
					   $this->api->_sendError('Sick Leave Cannot Be Applied On Future Date.');
					   }
					   /* Weekend and Date Ahead Check Start */
					   if($leaveStartDate > $leaveEndDate){
					   $this->api->_sendError('End Date Ahead From Start Date.');
					   }
	  
					   if($dayOfStartLeave == 'Sunday') {
					   $this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
					   }
					   if($dayOfEndLeave == 'Sunday'){
					   $this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
					   }
					   $LeaveHalfstatus = $request->allow_half;
								  if($LeaveHalfstatus == 1)
								  {
									  /* Date Conversion and Other Things */
									  $leaveStartDate=strtotime($request->start_date);
									  $dayOfStartLeave = date("l", $leaveStartDate);
	  
									  $leaveEndDate=strtotime($request->end_date);
									  $dayOfEndLeave = date("l", $leaveEndDate);
	  
									  $currentDate = date('d-m-Y');
									  $currentDate=strtotime($currentDate);
									  /* End Date Conversion */
									  if($leaveStartDate < $leaveEndDate){
									  $this->api->_sendError('Half day leave can be apply for one day.');
									  }
									  /*if($leaveStartDate > $currentDate){
									  $this->api->_sendError('Half Leave Cannot Be Applied On Future Date.');
									  }*/
									  
	  
									  /* Weekend and Date Ahead Check Start */
									  if($leaveStartDate > $leaveEndDate){
									  $this->api->_sendError('End Date Ahead From Start Date.');
									  }
	  
									  if($dayOfStartLeave == 'Sunday') {
									  $this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
									  }
									  if($dayOfEndLeave == 'Sunday'){
									  $this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
									  }
								  }
										  $Leaveshortstatus = $request->allow_short;
										  if($Leaveshortstatus == 1)
										  {
												  /* Date Conversion and Other Things */
												  $leaveStartDate=strtotime($request->start_date);
												  $dayOfStartLeave = date("l", $leaveStartDate);
	  
												  $leaveEndDate=strtotime($request->end_date);
												  $dayOfEndLeave = date("l", $leaveEndDate);
	  
												  $currentDate = date('d-m-Y');
												  $currentDate=strtotime($currentDate);
												  /* End Date Conversion */
												  if($leaveStartDate < $leaveEndDate){
													  $this->api->_sendError('Short leave can be apply for one day.');
												  }
												  /*if($leaveStartDate > $currentDate){
												  $this->api->_sendError('Short Leave Cannot Be Applied On Future Date.');
												  }*/
												  
												  /* Weekend and Date Ahead Check Start */
												  if($leaveStartDate > $leaveEndDate){
												  $this->api->_sendError('End Date Ahead From Start Date.');
												  }
	  
												  if($dayOfStartLeave == 'Sunday') {
												  $this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
												  }
												  if($dayOfEndLeave == 'Sunday'){
												  $this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
												  }
										  }
			   }
				 else
				 {
					 /* Check Sick Leave */
					 /* Date Conversion and Other Things */
					 $leaveStartDate=strtotime($request->start_date);
					 $dayOfStartLeave = date("l", $leaveStartDate);
		
					 $leaveEndDate=strtotime($request->end_date);
					 $dayOfEndLeave = date("l", $leaveEndDate);
		
					 $currentDate = date('d-m-Y');
					 $currentDate=strtotime($currentDate);
					 /* End Date Conversion */
					 if($leaveStartDate < $currentDate){
					 $this->api->_sendError('Leave Cannot Be Applied On Past Date.');
					 }
					 /* Weekend and Date Ahead Check Start */
					 if($leaveStartDate > $leaveEndDate){
					 $this->api->_sendError('End Date Ahead From Start Date.');
					 }
					 if($leaveStartDate < $currentDate){
					 $this->api->_sendError('Start Date Ahead From End Date.');
					 }
					 if($dayOfStartLeave == 'Sunday') {
					 $this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
					 }
					 if($dayOfEndLeave == 'Sunday'){
					 $this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
					 }
				 }
				  // Leave Apply Before
				  $date = $user->apply_before_date;
				  $arrbefore = $user->apply_before_days;
				
				  $arrDays =  date('Y-m-d', strtotime($date. ' + '.$arrbefore.' days'));
				  $leaveDayDate=strtotime($arrDays);
				  $leaveStartDate=strtotime($request->start_date);
				  if($leaveStartDate < $leaveDayDate){
					$this->api->_sendError("Sorry, You can apply $user->policy_name before $arrbefore days");
				  }
				  $changeBy = $this->api->_dateDiffInDays($request->start_date,$request->end_date);
				  // max balance Days
				  $arrmaxdays = $user->max_balance_days;
				  $diffDay = $changeBy+1;
				  if($arrmaxdays < $diffDay)
				  {
					$this->api->_sendError("Sorry , You can't apply $user->policy_name not more than  $arrmaxdays days ");  
				  }
				 
				  // is Date of Joining Considerd
				  
				   $arrconsecutive = $user->is_doj_considered;
				   if($arrconsecutive == 0)
				   {
					 $this->api->_sendError('Sorry, You Cant Apply Leave Date Of Joining Considerd.');  
				   }
				   
				  /* Max Consecutive No. Of Leaves same type */
				  $leaveUser_id =  $request->user_id;
				  $Leavepolicyid =  $request->leave_policy;
				
				$LeaveDate = $request->start_date;
					
		
			
				$leavecurrent = $this->leaveModel->ViewLeaveMonth($LeaveDate,$Leavepolicyid,$leaveUser_id);
				
				$arrPolicyType=[];
				
				foreach($leavecurrent as $policyType)
				{
					$arrPolicyType[] = $policyType->start_date;
								 
				}
		
				$arrcompanyPolicy =  count($arrPolicyType);
			
				
				$consecutiveLeave = $user->max_consecutive;
				
				if($arrcompanyPolicy == $consecutiveLeave)
				{
					$this->api->_sendError("Sorry , You can't apply $user->policy_name more than  $consecutiveLeave days in a month.");		
				}
							 /* Set Leave Data in Array Start */
							$leaveData = array(
								'user_id' => $request->user_id,
								'start_date' => $request->start_date,
								'end_date' => $request->end_date,
								'leave_policy_id' => $request->leave_policy,
								'status' => 0,
							);
							
							if(isset($request->reason))
								$leaveData['reason'] = $request->reason;
								if(isset($request->allow_half))
								$leaveData['allow_half'] = $request->allow_half;
								if(isset($request->allow_short))
								$leaveData['allow_short'] = $request->allow_short;
								
							
		
		
		
						/* Leave Not Applied On Holiday Check Start */
							$user = $this->api->_GetTokenData();
							$holiday_where = array(
								'company_holidays.branch_id' => $user['branch_id']
							);
							
							$holidays = $this->leaveModel->viewHolidayWhere($holiday_where);
							foreach($holidays as $holiday)
							{
		
								$holidayDate = strtotime($holiday->holiday_date);
				
								if($leaveStartDate == $holidayDate)
								{
									$this->api->_sendError('Leave Not Apply On Holidays, Select Another Start Date.');
								}
								if($leaveEndDate == $holidayDate)
								{
									$this->api->_sendError('Leave Not Apply On Holidays, Select Another End Date.');
								}
							}
					/*	 Leave Not Applied On Holiday Check End */
		
						/* Already Apply Leave On That Day Check Start */
							$leave_where['user_leave.user_id'] = $request->user_id;
							$result = $this->leaveModel->viewLeaveWhere2($leave_where);
							$leaveStartDate=strtotime($request->start_date);
							$leaveEndDate=strtotime($request->end_date);
							
							foreach($result as $leave)
							{
		
								$DateBegin = strtotime($leave->start_date);
								$DateEnd = strtotime($leave->end_date);
								
								if((($leaveStartDate > $DateBegin) && ($leaveStartDate < $DateEnd)) || (($leaveEndDate > $DateBegin) && ($leaveEndDate < $DateEnd)) || (($leaveEndDate == $DateBegin) && ($leaveEndDate == $DateEnd)) || (($leaveStartDate == $DateBegin) && ($leaveStartDate == $DateEnd)) || (($leaveStartDate == $DateEnd)) || (($DateBegin == $leaveStartDate)))
								{
									$this->api->_sendError('Choose Date Again, Leave Already Apply On Those Date.');
								}
		
								/*if($leave->status == 0)
								{
									$this->api->_sendError('Sorry, Your last leave has not been approved yet.');
								}*/
							}
						/* Already Apply Leave On That Day Check End */				
		
						/* Data Present For That Particular Leave Type With User In User_Leave_Details Check Start */
							$userLeaveWhere = array(
								'user_id' => $request->user_id,
								'leave_policy_id' => $request->leave_policy
							);
							
							if(!($this->leaveModel->userHasLeaveType($userLeaveWhere)))
							{
								$leaveNumberWhere = array(
									'id' => $request->leave_policy
								);
								$numberOfLeaves = $this->leaveModel->getNumberOfLeaveByType($leaveNumberWhere);
								
								$userLeaveDetails = array(
									'user_id' => $request->user_id,
									'leave_policy_id' => $request->leave_policy,
									'no_of_leave' => $numberOfLeaves->total_per_year
								);
								$detailResult = $this->leaveModel->insertUserLeaveType($userLeaveDetails);
								if(!$detailResult)
								{
									$this->api->_sendError("Sorry, There is Some Internal Settings Error. Please Try Again!!");
								}
							}
							$userLeaveTypeWhere = array(
								'id' => $request->leave_policy
							 );
							// get Leave Policy
							  $user = $this->leaveModel->getLeaveTypes($userLeaveTypeWhere);
							/* Get Leave Details */	
							if($user->negative_balance_allowed  == 0)
							{
								
								$userLeaveWhere = array(
									'leave_policy_id' => $request->leave_policy,
									'user_id' => $request->user_id
								);
								$userLeaveDetail = $this->leaveModel->getleaveDetailscheck($userLeaveWhere);
								$noofLeave = $userLeaveDetail->no_of_leave;
										
								$changeBy2 = $this->api->_dateDiffInDays($request->start_date,$request->end_date);
								$changeBy = $changeBy2+1;
								$finalBalance = $noofLeave - $changeBy;
								//print_r($finalBalance);
								if(($finalBalance < $changeBy) && ($finalBalance < 0))
								{
									$this->api->_sendError('Sorry, Nagative Leave Not Allowed In Your Policy.'); 
									
								}
								
							}	
										$LeaveHalfstatus = $request->allow_half;
									if($LeaveHalfstatus == 1)
									{
										/* Date Conversion and Other Things */
										$leaveStartDate=strtotime($request->start_date);
										$dayOfStartLeave = date("l", $leaveStartDate);
		
										$leaveEndDate=strtotime($request->end_date);
										$dayOfEndLeave = date("l", $leaveEndDate);
		
										$currentDate = date('d-m-Y');
										$currentDate=strtotime($currentDate);
										/* End Date Conversion */
										if($leaveStartDate < $leaveEndDate){
										$this->api->_sendError('Half day leave can be apply for one day.');
										}
										/*if($leaveStartDate > $currentDate){
										$this->api->_sendError('Half Leave Cannot Be Applied On Future Date.');
										}*/
										if($leaveStartDate < $currentDate){
										$this->api->_sendError('Half Leave Cannot Be Applied On Past Date.');
										}
		
										/* Weekend and Date Ahead Check Start */
										if($leaveStartDate > $leaveEndDate){
										$this->api->_sendError('End Date Ahead From Start Date.');
										}
		
										if($dayOfStartLeave == 'Sunday') {
										$this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
										}
										if($dayOfEndLeave == 'Sunday'){
										$this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
										}
									}
											$Leaveshortstatus = $request->allow_short;
											if($Leaveshortstatus == 1)
											{
													/* Date Conversion and Other Things */
													$leaveStartDate=strtotime($request->start_date);
													$dayOfStartLeave = date("l", $leaveStartDate);
		
													$leaveEndDate=strtotime($request->end_date);
													$dayOfEndLeave = date("l", $leaveEndDate);
		
													$currentDate = date('d-m-Y');
													$currentDate=strtotime($currentDate);
													/* End Date Conversion */
													if($leaveStartDate < $leaveEndDate){
													$this->api->_sendError('Short Leave Can Not Be Exceed From One Day.');
													}
													/*if($leaveStartDate > $currentDate){
													$this->api->_sendError('Short Leave Cannot Be Applied On Future Date.');
													}*/
													if($leaveStartDate < $currentDate){
													$this->api->_sendError('Short Leave Cannot Be Applied On Past Date.');
													}
													/* Weekend and Date Ahead Check Start */
													if($leaveStartDate > $leaveEndDate){
													$this->api->_sendError('End Date Ahead From Start Date.');
													}
		
													if($dayOfStartLeave == 'Sunday') {
													$this->api->_sendError('Leave Not Apply On Sunday, Select Another Start Date.');
													}
													if($dayOfEndLeave == 'Sunday'){
													$this->api->_sendError('Leave Not Apply On Sunday, Select Another End Date.');					
													}
											}
									/* Data Present For That Particular Leave Type With User In User_Leave_Details Check End */
									/* send mail To User Add Leave By Company */
									/* $user = $this->api->_GetTokenData();
									$user_id = $user['first_name'];
									$createdat = date('Y-m-d H:i:s');
									$position="Your Leave Has Beeen Appllied";
									$from="pratapsingh@ezdatechnology.com";
		
		
									if($request->leave_type == 1){
									$Leave_status="Planned Leaves" ; 
									}else{
									$Leave_status="Casual Leaves";  
									}
									$apply_to=$request->start_date;
									$from_to=$request->end_date;
									$this->email->from($from);
									$this->email->to("pratapsingh@ezdatechnology.com");
									$this->email->subject($position);
									$this->email->message('
									<br><div style="background-color: #fff;"><div><center> <h2> Ezdat Technology </h2></center><hr> <br>
									<table rules="all" style="border-color: #666;" cellpadding="10">
									<tr style= "background: #cad2d9;" > <td><strong>Applied By:</strong></td>
									<td>'.$user_id.'</td></tr>
									<tr style= "background: #cad2d9;" > <td><strong>Apply On:</strong></td>
									<td>'.$createdat.'</td></tr>
									<tr style= "background: #cad2d9;" > <td><strong>From:</strong></td>
									<td>'.$apply_to.'</td></tr>
									<tr style= "background: #cad2d9;" > <td><strong> To:</strong></td>
									<td>'.$from_to.'</td></tr>
									<tr style= "background: #cad2d9;" > <td><strong>Leave Type:</strong></td>
									<td>'.$Leave_status.'</td></tr>
									<td><strong>Your Account In Ezdat Technology Is Ready. You Can Simply Sign In Now</strong></td></tr>
									</table>
									');
									$this->email->set_mailtype('html');
									$this->email->send();*/
									// Email Send 
									/* After All Check Add Leave */
						$leave_where = array(
							'user_id' => $request->user_id,
							'id' => $request->leave_id
						);
							$result = $this->leaveModel->editLeave($leaveData,$leave_where);
						if($result)
						{
							return array(
								'status' => '1',
								'message' => 'Leave Updated Successfully.'
							);
						}
						else
						{
							$this->api->_sendError("Sorry, Leave Not Updated. Please Try Again!!");
						}
						
					
				

				
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
// Add tickets comments
public function _addLeaveComments($request)
{

       if(isset($request->leave_id) AND isset($request->comments))
       {
			
		$user = $this->api->_GetTokenData();
		$user_id = $user['id'];
       $addcomments['leave_id'] = $request->leave_id;
       $addcomments['user_id'] = $user_id;
       $addcomments['comments'] = $request->comments;
			 
	 $result = $this->leaveModel->addLeaveComments($addcomments);  // Update users Table

	 if($result)
	 {
		return array(
	   'status' => '1',
	   'message' => 'Leave Comment Added Successfully.'
	   );
	 }
	 else
	 {
$attendance = array('message' => 'Sorry, Some Error Occured. Please uploadAgain Again!!.');
 $this->api->_sendError($attendance);  
	  
	 }
								 
 
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	
}

// view Leave Comments 
public function _viewLeaveComments($request)
    {		
		
			/* Here we View the Leaves.. If User has the permission for that. */
			if(TRUE)
			{
				if(isset($request->leave_id))
				{
					$leave_where['leave_comments.leave_id'] = $request->leave_id;
					$result = $this->leaveModel->viewLeaveCommentsWhere($leave_where);
				return $result;
				}else{
					$result = $this->leaveModel->viewLeaveComments();
					return $result;
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	public function _viewLeave($request)
    {		
		// In This Method Only Logged In User Is View His Leave 
		
		$leave_where = array();
		
	
			/* Here we View the Leaves.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$leave_where['user_leave.user_id'] = $user_id;
				if(isset($request->leave_id))
				{
					$leave_where['user_leave.id'] = $request->leave_id;
				}
				$result = $this->leaveModel->viewLeaveWhere($leave_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	public function _viewLeave_Where($request)
    {		
		
		$leave_where = array();
		
		
			/* Here we View the Leaves.. If User has the permission for that. */
			
			if(TRUE)
			{
			
				
				if(isset($request->leave_id))
				{
					$leave_where['user_leave.id'] = $request->leave_id;
					$result = $this->leaveModel->viewLeaveemployeeWhere($leave_where);
				return $result;
				}else{
					$result = $this->leaveModel->viewLeaveemployee();
				return $result;
				}
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
			public function _viewBalanceLeave($request)
			{		
			
			
					/* Here we View the Leaves.. If User has the permission for that. */
				
					if(TRUE)
					{
						
						$user = $this->api->_GetTokenData();
						$test = $user['leave_policy_id'];
						$where = $test;
						$result = $this->leaveModel->viewLeaveBalance($where);
						return $result;
					}
					else
					{
						$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
					}
			
			}
        // paid leave 
		public function _viewPaidLeave($request)
		{		
				/* Here we View the Leaves.. If User has the permission for that. */
			
				if(TRUE)
				{
					$result = $this->leaveModel->viewLeavePaid();
					return $result;
				}
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
		
		}

	// total on a Leave Today
public function _totalLeaveToday($request)
{
	$module_name = 'leave';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{	
if(TRUE)
{
      if(isset($request->user_id))
					$users_where['user_details.user_id'] = $request->user_id;

				$user_ids = $this->users->_getUserId();
       
				if(!empty($user_ids))
						$where = 'user_details.user_id IN ('.implode(',',$user_ids).') AND users.is_active = 1';
					else
						$where = 'user_details.user_id = 0 AND users.is_active = 1';

				if(!empty($users_where))
				{
					$result = $this->leaveModel->checkTotalLeaveWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->leaveModel->checkTotalLeave($where);
					return $result;
				}
}
else
{
$this->api->_sendResponse("Sorry, There Is Some Perameter Missing!.");
}
}
else
{
$this->api->_sendResponse("Sorry, You Have No Permission To View Total Absent Users.");
}
}


// Maximum Carry days 
public function _maximumcarryDays($request)
{
	
					$query2 = $this->db->query("SELECT * FROM `leave_policy`");
					$result1=  $query2->result();
					$arr = array();
					foreach($result1 as $row)
					{
						$arr[] = $row->check_leave_type;		
					}	
					$unique_data = array_unique($arr);
	
	// now use foreach loop on unique data
foreach($unique_data as $check_leave_type) 
{
	//print_r($check_leave_type);
	if($check_leave_type == 1)
	{
			$query_2 = $this->db->query("SELECT * FROM `leave_policy` WHERE check_leave_type = 1");
			$result_1=  $query_2->result();
		$accrual_array = array();
		foreach($result_1 as $row_1)
		{
			$accrual_array[] = $row_1->accural_method;	
		}	
	
		$unique_accrualArray = array_unique($accrual_array);
		foreach($unique_accrualArray as $accrualMethod)
		{
			if($accrualMethod == 1)
			{
				$query23 = $this->db->query("SELECT * from user_details");
				$query3=  $query23->result();
			   $user_joiningDate=[];
			foreach($query3 as $row4)
			{
				$d = date('d', strtotime($row4->joining_date));
				$m =  date("m", strtotime($row4->joining_date));
				$yearBegin = date("Y");
				$yearEnd = $yearBegin + 20; 
				$years = range($yearBegin, $yearEnd, 1);
				$user_data = array();
				foreach($years as $year)
				{
				   $date = $year.'-'.$m.'-'.$d;
				  
				   $user_data[] = $date;
			   
			    }
             
			
			
			       $currentDate = date('Y-m-d');
				   if(in_array($currentDate, $user_data))
				   {
				   $user_joiningDate[]=$row4->user_id;
				   }	
				}  
				$string_version = implode(',', $user_joiningDate);
				$where = 'user_leave_details.user_id IN ('.$string_version.') ';
				$user_datastr = $this->leaveModel->getDataWhere($where);
				foreach($user_datastr as $row_91){
					$userLeaveWhere = array(
						'user_id' => $row_91->user_id,
						'leave_policy_id' => $row_91->leave_policy_id
					);
					
						$user = $this->leaveModel->getleaveDetailsTotal($userLeaveWhere);
						$noofleaves=[];
						foreach($user as $noofleave){
							
							$noofleaves['no_of_leave']=$noofleave->no_of_leave + $noofleave->total_per_year;
						
							$result = $this->leaveModel->editLeaveDetails($noofleaves,$userLeaveWhere);
							if(!$result)
							{
								$this->api->_sendError("Sorry, There is Some Internal Settings Error. Please Try Again!!");
							}
						}
				}
				 
              
			}
			else if($accrualMethod == 2)
			{
				$query_11 = $this->db->query("SELECT * from user_leave_details");
			$query_22=  $query_11->result();
			
			foreach($query_22 as $row_33)
			{
				$arrperYear = $row->total_per_year;
				$arrperyear = '2021-01-01';
				$d = date('d', strtotime($arrperyear));
				$m =  date("m", strtotime($arrperyear));
				$yearBegin = date("Y");
				$yearEnd = $yearBegin + 20; 
				$years = range($yearBegin, $yearEnd, 1);
				$user_data = array();
			   foreach($years as $year)
			   {
				$date = $year.'-'.$m.'-'.$d;
			   
				$user_data[] = $date;
			   }
			 
				$currentDate = date('Y-m-d');
				if(in_array($currentDate, $user_data))
				{
					$userLeaveWhere = array(
						'user_id' => $row_33->user_id,
						'leave_policy_id' => $row_33->leave_policy_id
					);
					
						$user = $this->leaveModel->getleaveDetailsTotal($userLeaveWhere);
						$noofleaves=[];
						foreach($user as $noofleave){
							
							$noofleaves['no_of_leave']=$noofleave->no_of_leave + $noofleave->total_per_year;
						
							$result = $this->leaveModel->editLeaveDetails($noofleaves,$userLeaveWhere);
							if(!$result)
							{
								$this->api->_sendError("Sorry, There is Some Internal Settings Error. Please Try Again!!");
							}
						}
				}
			}
			}
            else if($accrualMethod == 3)
		{
			$query_23 = $this->db->query("SELECT * from user_leave_details");
			$query_3=  $query_23->result();
			
			foreach($query_3 as $row_5)
			{
				
				$months = array("01-01", "02-01", "03-01","04-01","05-01", "06-01", "07-01","08-01","09-01", "10-01", "11-01","12-01");
			$arrMonth = array();
			foreach ($months as $month) {
			$arrMonth[]= $month;
			}
			$currentDate = date('m-d');
			if(in_array($currentDate, $arrMonth))
			{
				
				$userLeaveWhere = array(
					'user_id' => $row_5->user_id,
					'leave_policy_id' => $row_5->leave_policy_id
				);
				
					$user = $this->leaveModel->getleaveDetailsTotal($userLeaveWhere);
					$noofleaves=[];
                    foreach($user as $noofleave){
						
						$noofleaves['no_of_leave']=$noofleave->no_of_leave + $noofleave->total_per_year;
					
						$result = $this->leaveModel->editLeaveDetails($noofleaves,$userLeaveWhere);
						if(!$result)
						{
							$this->api->_sendError("Sorry, There is Some Internal Settings Error. Please Try Again!!");
						}
					}
					
				
			}
			} 
		
		}else{

		}
		}
	}
	
}
}

	public function _viewApprovedLeave($request)
    {		
		
			/* Here we View the Leaves.. If User has the permission for that. */
			
			if(TRUE)
			{
				
				$user = $this->api->_GetTokenData();
				$test = $user['leave_policy_id'];
				$where = 'leave_policy.id IN ('.$test.') ';
				$where2 =  $test;
				$result = $this->leaveModel->viewLeaveApprovedWhere($where,$where2);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	public function _leaveTotal($request)
	{		
		
			/* Here we View the Leaves.. If User has the permission for that. */
			
			if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$test = $user['leave_policy_id'];
				$where = 'leave_policy.id IN ('.$test.') ';
				$result = $this->leaveModel->viewToataLeave($where);
			    return $result;	
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	public function _leaveReport($request)
	{		
		// In This Method Only Logged In User Is View His Leave 
		// There is No Functionality Like To View Other User
		$module_name = 'leave';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$leave_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leaves.. If User has the permission for that. */
			$user = $this->api->_GetTokenData();
			$user_id = $user['id'];
			if(TRUE)
			{
				$leave_where['user_leave_details.user_id'] = $user_id;				
				$result = $this->leaveModel->viewLeaveReport($leave_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Leave.");
		}
	}
	//merge leave or Tickets
	public function _viewUserId($request)
	{		
	
		
			/* Here we View the Leaves/Tickets.. If User has the permission for that. */
	
			if(TRUE)
			{
				if(isset($request->user_id)){
					$users_where['users.id'] = $request->user_id;
					$result = $this->leaveModel->viewLeavesticket($users_where);
					return $result;
				}
				else{
					$result = $this->leaveModel->viewLeavesticketwhere();
					return $result;
				}
			}
			
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	// total Attendance And Total Present Employees functionality
public function _totalPresentEmployee($request)
{
	    $module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{	
if(TRUE)
{
      if(isset($request->user_id))
					$users_where['user_details.user_id'] = $request->user_id;

			 
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
						$where = 'users.id IN ('.implode(',',$user_ids).')';
					else
						$where = 'users.id = 0';

				if(!empty($users_where))
				{
					$result = $this->leaveModel->checkTotalPresentWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->leaveModel->checkTotalPresent($where);
					return $result;
				}
         }
       else
      {
       $this->api->_sendResponse("Sorry, There Is Some Perameter Missing!.");
      }
      }
      else
    {
     $this->api->_sendResponse("Sorry, You Have No Permission To View Total Present Users.");
     }
}
// total Attendance And Total Present Employees functionality
public function _totalAbsentEmployee($request)
{
	    $module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{	
    if(TRUE)
      {
					$user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
						$where = 'user_leave.user_id IN ('.implode(',',$user_ids).')';
					else
						$where = 'user_leave.user_id = 0';

				
					$result = $this->leaveModel->checkTotalAbsent($where);
					return $result;
				}
			
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
      }
      else
    {
     $this->api->_sendResponse("Sorry, You Have No Permission To View Total Present Users.");
     }
}


// graph total employee absent or leave 
// total Attendance And Total Present Employees functionality
public function _totalAbsentGraph($request)
{
	    $module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{	
    if(TRUE)
      {
				
					$result = $this->leaveModel->totalAbsentGraph();
					return $result;
				}
			
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
      }
      else
    {
     $this->api->_sendResponse("Sorry, You Have No Permission To View Total Present Users.");
     }
}

// graph total employee absent or leave 
// total Attendance And Total Present Employees functionality
public function _totalHalfLeaveShort($request)
{
	    $module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{	
    if(TRUE)
      {
				
					$result = $this->leaveModel->totalHalfLeave();
					return $result;
				}
			
				else
				{
					$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
      }
      else
    {
     $this->api->_sendResponse("Sorry, You Have No Permission To View Total Present Users.");
     }
}

//merge leave or Tickets
public function _viewBranchName($request)
{		


		/* Here we View the Leaves/Tickets.. If User has the permission for that. */

		if(TRUE)
		{
			if(isset($request->branch_id)){
				$users_where['company_branches.id'] = $request->branch_id;
			 
			
				$result = $this->leaveModel->viewBranchwhere($users_where);
				return $result;
			}
			else
			{
				$result = $this->leaveModel->viewBranch();
				return $result;
			}
		}
		
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	
}
	//merge leave or Tickets
	public function _viewUserName($request)
	{		
	
	
			/* Here we View the Leaves/Tickets.. If User has the permission for that. */
	
			if(TRUE)
			{
				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;
					if(isset($request->department_id))
					$users_where['users.department_id'] = $request->department_id;
					if(isset($request->branch_id))
					$users_where['users.branch_id'] = $request->branch_id;		
					

				if(!empty($users_where))
				{
					$result = $this->userModel->viewUserWhere22($users_where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewUser22();
					return $result;
				}
			}
			
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}


}
