<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: LeaveModel.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Models of the Leave.
*/

class LeaveModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function addLeave($data)
    {
        $this->db->insert('user_leave',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function addLeaveComments($data)
    {
        $this->db->insert('leave_comments',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    // leave And Tickets Merge 
    public function viewLeaveCommentsWhere($where)
    {
       
        $this->db->select('*');
        $this->db->from('leave_comments');
        $this->db->join('users', 'users.id = leave_comments.user_id');
       $this->db->where($where);
         $query = $this->db->get();
        return $query->result();
    }
     // leave And Tickets Merge 
     public function viewLeaveComments()
    { 
         $this->db->select('*');
        $this->db->from('leave_comments');
        $this->db->join('users', 'users.id = leave_comments.user_id');
         $query = $this->db->get();
        return $query->result();
    }

    public function deleteLeave($where_leave)
    {
        $this->db->where($where_leave);
        $this->db->delete('user_leave');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function getDataWhere($where)
    {
        $this->db->select('*');
        $this->db->from('user_leave_details');
       
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function editLeave($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_leave',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editLeaveDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_leave_details',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
// view Current Month type
public function ViewLeaveMonth($LeaveDate,$Leavepolicyid,$leaveUser_id)
{

   
    $query = $this->db->query("SELECT user_leave.start_date FROM user_leave WHERE MONTH(start_date) = MONTH('$LeaveDate') AND YEAR(start_date) = YEAR('$LeaveDate') And `user_id` = $leaveUser_id AND `leave_policy_id` = $Leavepolicyid");
 $arrTicketsData = $query->result();
 return $arrTicketsData;
 //$sql = $this->db->last_query();
 //echo $sql;
}
  
 
    public function viewLeave()
    {
        $this->db->select('user_leave.id as leave_id, user_leave.start_date, user_leave.end_date, user_leave.reason, user_leave.user_id, users.first_name, users.email, company_leave_types.id as leave_type_id, company_leave_types.leave_type, user_leave.status ');
        $this->db->from('user_leave');
     
         $this->db->join('users', 'users.id = user_leave.user_id');
        $this->db->join('company_leave_types', 'user_leave.leave_type_id = company_leave_types.id');
        $query = $this->db->get();
        return $query->result();
    }
         public function viewLeaves()
        {
        $this->db->select('user_leave.id as leave_id, user_leave.start_date, user_leave.end_date, user_leave.reason,user_leave.user_id,users.first_name,users.email, company_leave_types.id as leave_type_id, company_leave_types.leave_type, user_leave.status');
        $this->db->from('user_leave');
      
          $this->db->join('users', 'users.id = user_leave.user_id');
        $this->db->join('company_leave_types', 'user_leave.leave_type_id = company_leave_types.id');
       
        $query = $this->db->get();
        return $query->result();
        }
    public function viewLeaveWhere2($where)
    {
        $this->db->select('user_leave.id as leave_id, user_leave.start_date, user_leave.end_date, user_leave.reason, user_leave.user_id, users.first_name, users.email, leave_policy.id as leave_policy_id, leave_policy.policy_name, user_leave.status, user_leave.allow_short, user_leave.allow_half ');
        $this->db->from('user_leave');
     
         $this->db->join('users', 'users.id = user_leave.user_id');
        $this->db->join('leave_policy', 'user_leave.leave_policy_id = leave_policy.id');
        $this->db->where($where);
         $query = $this->db->get();
        return $query->result();
    }

    public function viewLeaveWhere($where)
    {
        $this->db->select('user_leave.id as leave_id, user_leave.start_date,user_leave.reason_admin, user_leave.end_date, user_leave.reason, user_leave.user_id, users.first_name, users.email, leave_policy.id as leave_policy_id, leave_policy.policy_name, user_leave.status, user_leave.allow_short, user_leave.allow_half ');
        $this->db->from('user_leave');
     
         $this->db->join('users', 'users.id = user_leave.user_id');
        $this->db->join('leave_policy', 'user_leave.leave_policy_id = leave_policy.id');
        $this->db->where($where);
         $query = $this->db->get()->result();
         foreach ($query as $key => $value) {
           
            $date1 = strtotime($value->start_date);  
            $date2 = strtotime( $value->end_date); 
            $diff = abs($date2 - $date1);  
            $years = floor($diff / (365*60*60*24));  
            $months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24)); 
            $days = floor(($diff - $years * 365*60*60*24 -  
            $months*30*60*60*24)/ (60*60*24)); 
            $dateDif = "";
        	if($value->allow_short==1)
            {
                $dateDif = 0.25;
            }
            else if($value->allow_half == 1)
            {
              $dateDif  = 0.50;  
            }
            else{
                $dateDif  = $days;   
            }
            $data_leave['leave_id'] = $value->leave_id;
            $data_leave['user_id'] = $value->user_id;
            $data_leave['first_name'] = $value->first_name;
            $data_leave['allow_short'] = $value->allow_short;
            $data_leave['allow_half'] = $value->allow_half;
            $data_leave['start_date'] = $value->start_date;
            $data_leave['end_date'] = $value->end_date;
            $data_leave['reason'] = $value->reason;
            $data_leave['leave_policy_id'] = $value->leave_policy_id;
            $data_leave['status'] = $value->status;
            $data_leave['reason_admin'] = $value->reason_admin;
            
            $data_leave['policy_name'] = $value->policy_name;
            $data_leave['days'] = $dateDif;
            $result_holiday[] = $data_leave;
           
     }
        return $result_holiday;
    }
    //view Leave total by employee
     public function viewLeaveemployee()
    {
        $this->db->select('user_leave.id as leave_id,company_branches.branch_name,users.user_code,company_designations.designation_name,company_departments.department_name, user_leave.user_id,user_leave.allow_half,user_leave.allow_short, users.first_name, users.email, users.emp_id, user_leave.start_date, user_leave.end_date, user_leave.reason,user_leave.status,leave_policy.policy_name,user_details.user_image,user_leave.reason_admin');
        $this->db->from('user_leave');
         
        $this->db->join('leave_policy','leave_policy.id = user_leave.leave_policy_id');
        $this->db->join('users', 'users.id = user_leave.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
     
       
        $query = $this->db->get()->result();
        foreach ($query as $key => $value) {
          
            $date1 = strtotime($value->start_date);  
            $date2 = strtotime( $value->end_date); 
            $diff = abs($date2 - $date1);  
            $years = floor($diff / (365*60*60*24));  
            $months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24)); 
            $days = floor(($diff - $years * 365*60*60*24 -  
            $months*30*60*60*24)/ (60*60*24)); 
            $dateDif = "";
        	if($value->allow_short==1)
            {
                $dateDif = 0.25;
            }
            else if($value->allow_half == 1)
            {
              $dateDif  = 0.50;  
            }
            else{
                $dateDif  = $days;   
            }
            $data_leave['leave_id'] = $value->leave_id;
            $data_leave['user_image'] = $value->user_image;
            $data_leave['user_id'] = $value->user_id;
            $data_leave['first_name'] = $value->first_name;
            $data_leave['allow_short'] = $value->allow_short;
            $data_leave['allow_half'] = $value->allow_half;
            $data_leave['start_date'] = $value->start_date;
            $data_leave['end_date'] = $value->end_date;
            $data_leave['reason'] = $value->reason;
            $data_leave['status'] = $value->status;
            $data_leave['policy_name'] = $value->policy_name;
            $data_leave['emp_id'] = $value->emp_id;
            $data_leave['status'] = $value->status;
            $data_leave['branch_name'] = $value->branch_name;
            $data_leave['designation_name'] = $value->designation_name;
            $data_leave['department_name'] = $value->department_name;
            $data_leave['user_code'] = $value->user_code;
            $data_leave['reason_admin'] = $value->reason_admin;
           
            $data_leave['days'] = $dateDif;
            $result_holiday[] = $data_leave;
           
     }
        return $result_holiday;
    }
    //view Leave total by employee
    public function viewLeaveemployeeWhere($where)
    {
        $this->db->select('user_leave.id as leave_id,company_branches.branch_name,users.user_code,company_designations.designation_name,company_departments.department_name, user_leave.user_id,user_leave.allow_half,user_leave.allow_short, users.first_name, users.email, users.emp_id, user_leave.start_date, user_leave.end_date, user_leave.reason,user_leave.status,leave_policy.policy_name,user_details.user_image,user_leave.reason_admin');
        $this->db->from('user_leave');
         
        $this->db->join('leave_policy','leave_policy.id = user_leave.leave_policy_id');
        $this->db->join('users', 'users.id = user_leave.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->where($where);
        $query = $this->db->get()->result();
        foreach ($query as $key => $value) {
          
            $date1 = strtotime($value->start_date);  
            $date2 = strtotime( $value->end_date); 
            $diff = abs($date2 - $date1);  
            $years = floor($diff / (365*60*60*24));  
            $months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24)); 
            $days = floor(($diff - $years * 365*60*60*24 -  
            $months*30*60*60*24)/ (60*60*24)); 
            $dateDif = "";
        	if($value->allow_short==1)
            {
                $dateDif = 0.25;
            }
            else if($value->allow_half == 1)
            {
              $dateDif  = 0.50;  
            }
            else{
                $dateDif  = $days;   
            }
            $data_leave['leave_id'] = $value->leave_id;
            $data_leave['user_image'] = $value->user_image;
            $data_leave['user_id'] = $value->user_id;
            $data_leave['first_name'] = $value->first_name;
            $data_leave['allow_short'] = $value->allow_short;
            $data_leave['allow_half'] = $value->allow_half;
            $data_leave['start_date'] = $value->start_date;
            $data_leave['end_date'] = $value->end_date;
            $data_leave['reason'] = $value->reason;
            $data_leave['status'] = $value->status;
            $data_leave['policy_name'] = $value->policy_name;
            $data_leave['emp_id'] = $value->emp_id;
            $data_leave['status'] = $value->status;
            $data_leave['branch_name'] = $value->branch_name;
            $data_leave['designation_name'] = $value->designation_name;
            $data_leave['department_name'] = $value->department_name;
            $data_leave['user_code'] = $value->user_code;
            $data_leave['reason_admin'] = $value->reason_admin;
           
            $data_leave['days'] = $dateDif;
            $result_holiday[] = $data_leave;
           
     }
        return $result_holiday;
    }
    // view Tickets
     public function viewTickets()
    {
       $this->db->select('*');
        $this->db->from('user_tickets');
        $this->db->join('users', 'users.id = user_tickets.user_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('role', 'role.id = users.role_id');
       
         $query = $this->db->get();
        return $query->result();
    }
    // leave And Tickets Merge 
     public function viewLeavesticket($where)
    {
       
        $this->db->select('users.id,users.first_name,users.emp_id,users.is_active');
        $this->db->from('users');
       $this->db->where($where);
         $query = $this->db->get();
        return $query->result();
    }
     // leave And Tickets Merge 
     public function viewLeavesticketwhere()
    { 
         $this->db->select('users.id,users.first_name,users.emp_id,users.is_active');
        $this->db->from('users');
      
         $query = $this->db->get();
        return $query->result();
    }

    public function viewLeavesFirstwhere($where)
    {
        $this->db->select('users.id,users.first_name,users.emp_id');
        $this->db->from('users');
       $this->db->where($where);
         $query = $this->db->get();
        return $query->result();
    }

    public function viewLeavesFirst()
    {
        $this->db->select('users.id,users.first_name,users.emp_id');
        $this->db->from('users');
         $query = $this->db->get();
        return $query->result();
    }

    public function viewBranchwhere($where)
    {
        $this->db->select('*');
        $this->db->from('company_branches');
       $this->db->where($where);
         $query = $this->db->get();
        return $query->result();
    }

    public function viewBranch()
    {
        $this->db->select('*');
        $this->db->from('company_branches');
         $query = $this->db->get();
        return $query->result();
    }
// check total absent
 public function checkTotalAbsent($where)
 {
   // print_r($where);
   $this->db->select('count(*) as totalonleave');
        $this->db->from('user_leave');
        $this->db->where('DATE(NOW()) BETWEEN start_date AND end_date AND user_leave.status = 1');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
       
 }

 
 
    //view Total Leave
 public function viewToataLeave($where)
    {
        $this->db->select('sum(total_per_year) as leaves');
        $this->db->from('leave_policy');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

     public function viewLeaveApprovedWhere($where,$where2)
    {
        //print_r($where2);
        $total_taken_leave=0;
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];			
        $this->db->select('sum(total_per_year) as total');
        $this->db->from('leave_policy');
        $this->db->where($where);
        $query1 = $this->db->get();
        $query2 = $this->db->query("SELECT sum(total_per_year) as taken_leaveAdd FROM `leave_policy` WHERE id IN ($where2) AND id NOT IN (SELECT leave_policy_id from user_leave_details WHERE user_id= '".$user_id."')");
        $query3 = $this->db->query("SELECT sum(no_of_leave) as taken_leave from user_leave_details WHERE user_id= '".$user_id."'");
        $result1=  $query1->row();
        //print_r($result1);
        $result2=  $query2->row();
       // print_r($result2);
        $result3=  $query3->row();
        $addLeaves = $result2->taken_leaveAdd+$result3->taken_leave;
        $total_taken_leave = $result1->total-$addLeaves;

        return $total_taken_leave;
    }

    public function totalAbsentGraph()
    {
    //   // print_r($where);
       $this->db->select('count(*) as totalonleave');
           $this->db->from('user_leave');
           $this->db->where('DATE(NOW()) BETWEEN start_date AND end_date AND user_leave.status = 1');
            $query = $this->db->get();
           $test = $query->result();
           foreach($test as $key => $row)
            {
            $countdata['data1'] = $row->totalonleave;
            $counting[] = $countdata;
            }
           // attendance data 
           $query = $this->db->query("SELECT count(*) as totalonpresent FROM users INNER JOIN user_details on user_details.user_id = users.id LEFT JOIN attendance_mark on users.id = attendance_mark.user_id and attendance_mark.date=date(now()) WHERE `role_id` NOT IN ('1') AND attendance_mark.status = 0");
           $arrAttendanceData = $query->row();
           $countdata2['data2'] = $arrAttendanceData->totalonpresent;
           $counting2[] = $countdata2;
          $query2 = $this->db->query("SELECT count(*) as totalonabsent FROM users INNER JOIN user_details on user_details.user_id = users.id LEFT JOIN attendance_mark on users.id = attendance_mark.user_id and attendance_mark.date=date(now()) WHERE `role_id` NOT IN ('1')");
          $arrAttendanceData1 = $query2->row();
          $addLeaves = $arrAttendanceData1->totalonabsent-$arrAttendanceData->totalonpresent;
          $countdata3['data3'] = $addLeaves;
          $counting3[] = $countdata3;
         $merge = array_merge($counting, $counting2,$counting3);
         return $merge;  
    }

    public function totalHalfLeave()
    {
    //   // print_r($where);
       $this->db->select('count(*) as totalshortToday');
           $this->db->from('user_leave');
           $this->db->where('DATE(NOW()) BETWEEN start_date AND end_date AND user_leave.status = 1 AND user_leave.allow_short = 1');
            $query = $this->db->get();
           $test = $query->row();
           $countshortToday['totalshortToday'] = $test->totalshortToday;
           $countingShortToday[] = $countshortToday;
           // attendance data
           $this->db->select('count(*) as totalHalfToday');
           $this->db->from('user_leave');
           $this->db->where('DATE(NOW()) BETWEEN start_date AND end_date AND user_leave.status = 1 AND user_leave.allow_half = 1');
            $query2 = $this->db->get();
           $test2 = $query2->row();
           $countHalfToday['totalHalfToday'] = $test2->totalHalfToday;
           $countingHalfToday[] = $countHalfToday;
           // attendance data  
           $this->db->select('count(*) as totalLeaveToday');
           $this->db->from('user_leave');
           $this->db->where('DATE(NOW()) BETWEEN start_date AND end_date AND user_leave.status = 1');
            $query3 = $this->db->get();
           $test3 = $query3->row();
           $countLeaveToday['totalLeaveToday'] = $test3->totalLeaveToday;
           $countingLeaveToday[] = $countLeaveToday;
           $query4 = $this->db->query("select count(*) as totalLeaveMonth FROM user_leave where MONTH(start_date)=MONTH(now()) and YEAR(end_date)=YEAR(now()) AND status=1");
           $result4=  $query4->row();
           $countLeaveMonth['totalLeaveMonth'] = $result4->totalLeaveMonth;
           $countingLeaveMonth[] = $countLeaveMonth;
           // short leave this month
           $query5= $this->db->query("select count(*) as totalshortMonth FROM user_leave where MONTH(start_date)=MONTH(now()) and YEAR(end_date)=YEAR(now()) AND status=1 AND allow_short = 1");
           $result5=  $query5->row();
           $countshortMonth['totalshortMonth'] = $result5->totalshortMonth;
           $countingshortMonth[] = $countshortMonth;

            // Half leave this month
            $query6= $this->db->query("select count(*) as totalHalfMonth FROM user_leave where MONTH(start_date)=MONTH(now()) and YEAR(end_date)=YEAR(now()) AND status=1 AND allow_short = 1");
            $result6=  $query6->row();
            $countHalfMonth['totalHalfMonth'] = $result6->totalHalfMonth;
            $countingHalfMonth[] = $countHalfMonth;
       // total absent attendance 
       $query6 = $this->db->query("SELECT count(*) as totalonpresent FROM users INNER JOIN user_details on user_details.user_id = users.id LEFT JOIN attendance_mark on users.id = attendance_mark.user_id and attendance_mark.date=date(now()) WHERE `role_id` NOT IN ('1') AND attendance_mark.status = 0");
       $arrAttendanceData = $query6->row();
      $query7 = $this->db->query("SELECT count(*) as totalonabsent FROM users INNER JOIN user_details on user_details.user_id = users.id LEFT JOIN attendance_mark on users.id = attendance_mark.user_id and attendance_mark.date=date(now()) WHERE `role_id` NOT IN ('1')");
      $arrAttendanceData1 = $query7->row();
      $addLeaves = $arrAttendanceData1->totalonabsent-$arrAttendanceData->totalonpresent;
      $countabsent['totalonabsent'] = $addLeaves;
      $countingAbsent[] = $countabsent;       
           

         $merge = array_merge($countingShortToday,$countingHalfToday,$countingLeaveToday,$countingLeaveMonth,$countingshortMonth,$countingHalfMonth,$countingAbsent);
         return $merge;  
    }
    public function viewLeaveBalance($where)
    {   $total_remained_leave=0;
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];			
        $query1 = $this->db->query("SELECT sum(total_per_year) as remained_leave FROM `leave_policy` WHERE id IN ($where) AND id NOT IN (SELECT leave_policy_id from user_leave_details WHERE user_id= '".$user_id."')");
        $query2 = $this->db->query("SELECT sum(no_of_leave) as total from user_leave_details WHERE user_id= '".$user_id."'");
        $result1=  $query1->row();
       // print_r($result1);
        $result2=  $query2->row();
        $total_remained_leave=$result1->remained_leave+$result2->total;
        return $total_remained_leave;
        
    }

    public function viewLeavePaid()
    {  
        $user = $this->api->_GetTokenData();
        $user_id = $user['id'];			
       // $query1 = $this->db->query("SELECT sum(total_per_year) as remained_leave FROM `leave_policy` WHERE id IN ($where) AND id NOT IN (SELECT leave_policy_id from user_leave_details WHERE user_id= '".$user_id."')");
        $query2 = $this->db->query("SELECT sum(paid_leave) as paid_total from user_leave_details WHERE user_id= '".$user_id."'");
       
        $result2=  $query2->row();
        return $result2;
        
    }
    public function userHasLeaveType($where)
    {
        $query = $this->db->get_where('user_leave_details',$where);
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    public function getleaveDetails($userLeaveWhere) {
        // Check The Credentials
        $query = $this->db->get_where('user_leave_details' , $userLeaveWhere);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('user_leave_details');
          
            $this->db->where('user_leave_details.id', $row->id);
        
           $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }

    public function getleaveDetailsTotal($userLeaveWhere) {
        
        $this->db->select('*');
        $this->db->from('user_leave_details');
        $this->db->join('leave_policy', 'user_leave_details.leave_policy_id = leave_policy.id');
        $this->db->where($userLeaveWhere);
    
       $user = $this->db->get()->result();

        return $user;
    
}

    public function getleaveDetailscheck($userLeaveWhere) {
        // Check The Credentials
        $query = $this->db->get_where('user_leave_details' , $userLeaveWhere);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('user_leave_details');
          
            $this->db->where('user_leave_details.id', $row->id);
        
           $userLeaveDetail = $this->db->get()->row();

            return $userLeaveDetail;
        }
        else{
            return FALSE;
        }
    }
    public function getLeaveTypes($userLeaveTypeWhere) {
        // Check The Credentials
        $query = $this->db->get_where('leave_policy' , $userLeaveTypeWhere);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('leave_policy');
          
            $this->db->where('leave_policy.id', $row->id);
        
           $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
    
    public function getNumberOfLeaveByType($where)
    {
        $this->db->select('total_per_year');
        $this->db->from('leave_policy');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function insertUserLeaveType($data)
    {
        $this->db->insert('user_leave_details',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

    public function getLeaveWhere($where)
    {
        $query = $this->db->get_where('user_leave', $where);
        return $query->result();
    }

    public function viewLeaveReport($where)
    {
        $this->db->select('user_leave_details.user_id, user_leave_details.no_of_leave as remaining_leave, cleave.leave_type,  cleave.no_of_leave as total_leave, users.first_name, users.emp_id');
        $this->db->from('user_leave_details');
         $this->db->join('users', 'users.id = user_leave_details.user_id');
        $this->db->join('company_leave_types as cleave', 'user_leave_details.leave_type_id = cleave.id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewHolidayWhere($where)
    {
        $this->db->select('company_holidays.id as holiday_id, company_holidays.holiday_title, company_holidays.holiday_date');
        $this->db->from('company_holidays');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewLeaveTypeWhere($where)
    {
        $this->db->select('*');
        $this->db->from('leave_policy');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

     // total Leave attandance 
     public function checkTotalLeaveWhere($where1,$where2)
     {
        $total_absent_employee=0;
        $this->db->select('count(*) as Present' );
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->where($where2);
         $this->db->where($where1);
        $query = $this->db->get();
        $result1 =  $query->row();
        // date wise count employee
     $date = new DateTime("now");
  
  $curr_date = $date->format('Y-m-d ');
 
  $this->db->select('count(*) as leaveAbsent');
  $this->db->from('attendances'); 
  $this->db->where('date', $curr_date);
         $query = $this->db->get();
         $result2 =  $query->row();
         
         $total_remained_leave = $result1->Present-$result2->leaveAbsent;
         return $total_remained_leave;
     }
     //SELECT * FROM user_leave WHERE `start_date` BETWEEN '2020-11-19' AND '2020-11-19' OR '2020-11-23' BETWEEN `start_date` AND `end_date`

      // total Leave attandance 
     public function checkTotalLeave($where1)
     {
        $total_absent_employee=0;
        $this->db->select('count(*) as Present' );
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');

         $this->db->where($where1);
        $query = $this->db->get();
        $result1 =  $query->row();
              
    // date wise count employee
     $date = new DateTime("now");
  
  $curr_date = $date->format('Y-m-d ');
 
  $this->db->select('count(*) as leaveAbsent');
  $this->db->from('attendances'); 
  $this->db->where('date', $curr_date);
         $query = $this->db->get();
         $result2 =  $query->row();
        
         $total_remained_leave = $result1->Present-$result2->leaveAbsent;
         return $total_remained_leave;
     }

     // total present attandance 
 public function checkTotalPresentWhere($where1,$where2)
    {
        $user_ids = $this->users->_getUserId();
					if(!empty($user_ids))
						$whereLeave = 'user_leave.user_id IN ('.implode(',',$user_ids).')';
					else
						$whereLeave = 'user_leave.user_id = 0';
   $this->db->select('count(*) as totalonleave');
   $this->db->from('user_leave');
   $this->db->where('DATE(NOW()) BETWEEN start_date AND end_date AND user_leave.status = 1');
   $this->db->where($whereLeave);
   $query = $this->db->get();
   $testLeave = $query->row();

  $this->db->select('count(*) as present');
  $this->db->from('users');
  $this->db->join('company_designations','company_designations.id = users.designation_id');
  $this->db->join('company_departments','company_departments.id = users.department_id');
  $this->db->join('company_branches','company_branches.id = users .branch_id');
  $this->db->join('user_details','user_details.user_id = users.id');
  $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
  $this->db->join('user_personal','user_personal.user_id = users.id');
   $this->db->where($where);
         $this->db->where($where1);
         $this->db->where($where2);
        $query = $this->db->get();
        $testUsers =  $query->row();
        $total_present_users = $testUsers->Present - $testLeave->totalonleave;
        return $total_present_users;
    }
    
     // total present attandance 
    public function checkTotalPresent($where1)
    {
        $user_ids = $this->users->_getUserId();
        if(!empty($user_ids))
            $whereLeave = 'user_leave.user_id IN ('.implode(',',$user_ids).')';
        else
            $whereLeave = 'user_leave.user_id = 0';
   
        $this->db->select('count(*) as totalonleave');
   $this->db->from('user_leave');
   $this->db->where('DATE(NOW()) BETWEEN start_date AND end_date AND user_leave.status = 1');
   $this->db->where($whereLeave);
   $query = $this->db->get();
   $testLeave = $query->row();

  $this->db->select('count(*) as present');
  $this->db->from('users');
  $this->db->join('company_designations','company_designations.id = users.designation_id');
  $this->db->join('company_departments','company_departments.id = users.department_id');
  $this->db->join('company_branches','company_branches.id = users .branch_id');
  $this->db->join('user_details','user_details.user_id = users.id');
  $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
  $this->db->join('user_personal','user_personal.user_id = users.id');
         $this->db->where($where1);
        $query = $this->db->get();
        $testUsers =  $query->row();
        $total_present_users = $testUsers->present - $testLeave->totalonleave;
        return $total_present_users;
    }
}