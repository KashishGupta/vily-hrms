<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: EmployeeModel.php
	Date: May 12, 2020
	Info: This is the main class which is hold all the Functions of the Employee.
*/

class EmployeesModel  extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
//View Employee Details 
       public function viewEmployee()
    {
        $this->db->select('*');
        $this->db->from('users');
       
        $query = $this->db->get();
        return $query->result();
    }
    public function insertData($table,$data)
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        $empId = 'EMP'.$insert_id;
        $data['emp_id'] = $empId;
        if($insert_id != 0)
        {
            return $insert_id;
        }
        else    
        {
            return 0;
        }
    }

// Edit Personal Details
public function editDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_personal',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Edit Bank Details
    public function edit_BankDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users_bank_details',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
     // Get Data where  
     public function getdataPersonalDetails($user_personal) {
        // Check The Credentials
        $query = $this->db->get_where('user_personal' , $user_personal);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('user_personal');
            $this->db->where('user_personal.id', $row->id);
            $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
     // Get Data where  
     public function getdatabankDetails($user_bank) {
        // Check The Credentials
        $query = $this->db->get_where('users_bank_details' , $user_bank);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('users_bank_details');
            $this->db->where('users_bank_details.id', $row->id);
            $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
    // Edit Relative Details
    public function editRelativeDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_emergency_contact',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
public function editUserDetails($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
 
}
