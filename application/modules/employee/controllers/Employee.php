
 <?php
 
defined('BASEPATH') OR exit('No direct script access allowed');
//require APPPATH . '/libraries/S3.php';
/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Jobs.php
	Date: may 12, 2020
	Info: This is the main class which is hold all the Functions of the Employee.
*/

class Employee extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		//$this->objOfS3 = new s3();
		$this->load->model('employeesModel');
	}
 
    //edit personal details
   
     public function _editPersonal($request)
    {
		$userPersonal = array();
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$user_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Employee.. If User has the permission for that. */
          
			if(isset($request->user_id))
			{
				$user_where = array(
					'user_personal.user_id' =>$request->user_id
				);

				if(isset($request->passport_no))
					$userPersonal['passport_no'] = $request->passport_no;
				if(isset($request->passport_expiry))
					$userPersonal['passport_expiry'] = $request->passport_expiry;
	           if(isset($request->nationality))
					$userPersonal['nationality'] = $request->nationality;
				 if(isset($request->religion))
					$userPersonal['religion'] = $request->religion;
				 if(isset($request->marital_status))
					$userPersonal['marital_status'] = $request->marital_status;
				 if(isset($request->no_of_children))
					$userPersonal['no_of_children'] = $request->no_of_children;
					if(isset($request->contactperson_name))
					$userPersonal['contactperson_name'] = $request->contactperson_name;
				 if(isset($request->contactperson_no))
					$userPersonal['contactperson_no'] = $request->contactperson_no;
				 if(isset($request->relationship))
					$userPersonal['relationship'] = $request->relationship;
				$result = $this->employeesModel->editDetails($userPersonal,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Employee Personal Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Employee Personal Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Employee Personal.");
		}
    }
	
	// bank details and relative information / user Experinece / user Education Details 
	public function _onboardingBankDetails($request)
	{
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_edit == 'Y')
		{
			if(isset($request->user_id))
			{
				if(isset($request->account_no))
				$user_bank = array('account_no' => $request->account_no);
				$bankdata1 = $this->employeesModel->getdatabankDetails($user_bank);
				if($bankdata1 == TRUE)
				{
					$this->api->_sendError("Sorry,Account No Already Present In Our Record!!");
					
				}
				if(isset($request->pan_card))
				$user_bank = array('pan_card' => $request->pan_card);
				$bankdata1 = $this->employeesModel->getdatabankDetails($user_bank);
				if($bankdata1 == TRUE)
				{
					$this->api->_sendError("Sorry,Pan card No Already Present In Our Record!!");
					
				}
				if(isset($request->aadhar_card))
				$user_bank = array('aadhar_card' => $request->aadhar_card);
				$bankdata1 = $this->employeesModel->getdatabankDetails($user_bank);
				if($bankdata1 == TRUE)
				{
					$this->api->_sendError("Sorry,Adhar card No Already Present In Our Record!!");
					
				}
				// chank already present or not
				$user_bank = array('user_id' => $request->user_id);
				$bankdata = $this->employeesModel->getdatabankDetails($user_bank);
				if($bankdata == TRUE)
				{
					$this->api->_sendError("Sorry, You Are Already Fill the Bank Details!!");
					
				}
				
				if(isset($request->holder_name))
					$user_bank['holder_name'] = $request->holder_name;
				if(isset($request->account_no))
					$user_bank['account_no'] = $request->account_no;
				if(isset($request->bank_name))
					$user_bank['bank_name'] = $request->bank_name;
				if(isset($request->branch))
					$user_bank['branch'] = $request->branch;
				if(isset($request->ifsc))
					$user_bank['ifsc'] = $request->ifsc;
				
					$user_bank['pan_card'] = $request->pan_card;
				
					$user_bank['aadhar_card'] = $request->aadhar_card;
				$result = $this->employeesModel->insertData('users_bank_details',$user_bank);
					
						 if($result)
						 {
							 return array(
								 'status' => '1',
								 'message' => 'User Bank Details onboarding Successfully.'
							 );
						 }
						 else
						 {
							 $this->api->_sendError("Sorry, User Bank Details Not Saved. Please Try Again!!");
						 }
			}
			else{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else{
			$this->api->_sendError("Sorry, You Have No Permission To Add Employee Onboarding.");
		}

	}
	//  relative information  or personal
	public function _onboardingPersonalDetails($request)
	{
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_edit == 'Y')
		{
			if(isset($request->user_id))
			{
				$user_personal = array('user_id' => $request->user_id);
				$bankdata = $this->employeesModel->getdataPersonalDetails($user_personal);
				if($bankdata == TRUE)
				{
					$this->api->_sendError("Sorry, You Are Already Fill the Personal Details!!");
					
				}
				if(isset($request->passport_no))
						$user_personal['passport_no'] = $request->passport_no;
					if(isset($request->passport_expiry))
						$user_personal['passport_expiry'] = $request->passport_expiry;
					if(isset($request->nationality))
						$user_personal['nationality'] = $request->nationality;
					if(isset($request->religion))
						$user_personal['religion'] = $request->religion;
					if(isset($request->marital_status)) 
						$user_personal['marital_status'] = $request->marital_status;
					if(isset($request->no_of_children))
						$user_personal['no_of_children'] = $request->no_of_children;
					if(isset($request->contactperson_name))
						$user_personal['contactperson_name'] = $request->contactperson_name;
					if(isset($request->contactperson_no))
						$user_personal['contactperson_no'] = $request->contactperson_no;
					if(isset($request->relationship))
						$user_personal['relationship'] = $request->relationship;
					$result = $this->employeesModel->insertData('user_personal',$user_personal);
						 if($result)
						 {
							 return array(
								 'status' => '1',
								 'message' => 'User Personal Details onboarding Successfully.'
							 );
						 }
						 else
						 {
							 $this->api->_sendError("Sorry, User Personal Details Not Saved. Please Try Again!!");
						 }
			}
			else{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else{
			$this->api->_sendError("Sorry, You Have No Permission To Add Employee Onboarding.");
		}

	}
     //Edit Relative EMPLOYEE details
     public function _editeEmployeeDetails($request)
    {
		$relative = array();
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
			$user_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Branchs.. If User has the permission for that. */
          
			if(isset($request->user_id))
			{
				$user_where = array(
					'id' => $request->user_id
				);
				$usertr_where = $request->user_id;
				$query = $this->db->query("SELECT * FROM users WHERE id ='".$usertr_where."'");
				  $arrTicketsData = $query->row();
				  if($arrTicketsData->branch_id == 0){
				if(isset($request->branch_id))
					$details['branch_id'] = $request->branch_id;
	           if(isset($request->department_id))
					$details['department_id'] = $request->department_id;
					 if(isset($request->designation_id))
					$details['designation_id'] = $request->designation_id;
				$result = $this->employeesModel->editUserDetails($details,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Office Details Submitted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Employee Not Added. Please Try Again!!");
				}
			}else{
				$this->api->_sendError("Sorry, You are already fill details!!");
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Employee.");
		}
    }
 //Edit Employee Bank Details details
     public function _editBank($request)
    {
		$userBank = array();
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$user_where = array();
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Branchs.. If User has the permission for that. */
          
			if(isset($request->user_id))
			{
				$user_where = array(
					'users_bank_details.user_id' =>$request->user_id
				);

				if(isset($request->holder_name))
					$userBank['holder_name'] = $request->holder_name;
				if(isset($request->account_no))
					$userBank['account_no'] = $request->account_no;
	           if(isset($request->bank_name))
					$userBank['bank_name'] = $request->bank_name;
				 if(isset($request->branch))
					$userBank['branch'] = $request->branch;
				 if(isset($request->salary))
					$userBank['salary'] = $request->salary;
				 if(isset($request->ifsc))
					$userBank['ifsc'] = $request->ifsc;
					 if(isset($request->pan_card))
					$userBank['pan_card'] = $request->pan_card;
					 if(isset($request->aadhar_card))
					$userBank['aadhar_card'] = $request->aadhar_card;
				$result = $this->employeesModel->edit_BankDetails($userBank,$user_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Bank Details Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Bank Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Bank.");
		}
	}
	
  public function _viewEmployee($request)
    {		
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Roles.. If User has the permission for that. */

			if(TRUE)
			{
				
					$result = $this->employeesModel->viewEmployee();
					return $result;
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View employee.");
		}
	}
 
}