<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Leavemanagement.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Functions of the users.
*/

class Leavemanagement extends MY_Controller{

    public function __construct(){
		parent::__construct();
		$this->load->model('leaveManagementModel');
	}

	// Approve and Disapprove Leave Function
    public function _leaveAction($request)
    {
	
		$userLeave = array();
		$module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Leave Management Type.. If User has the permission for that. */

			if(isset($request->leave_id) AND isset($request->action) AND isset($request->reason))
			{
				$where_leave = array(
					'id' => $request->leave_id
				);
				$leaves = $this->leaveManagementModel->getLeaveWhere($where_leave);
				
				foreach($leaves as $leave) {}
			
				if($leave->status != 0)
				{ 
					$this->api->_sendError("Sorry, Action Already Performed On This Leave.");
				}
				else
				{
					if($leave->allow_half == 1) // When User Want to Half  The Leave
					{
						if($request->action == 1)			// When User Want to Approve The Leave
						{
							/*if(isset($request->user_id))
                            {
                            $user_checkid = $request->user_id;  
							}else{}
							$credential = array('id'=>$user_checkid);
							$resultuser = $this->leaveManagementModel->getUserwhere($credential);
							$attendanceData = array( 
								'user_id' => $resultuser->id,
								'emp_id' => $resultuser->emp_id,
								'start_date' => $request->start_date,
								'end_date' => $request->start_date,
								'days' => $request->days,
								'status'=>1,
							);  
							  $resultforuseArr = $this->leaveManagementModel->addAttendanceDateData($attendanceData);*/

							$userLeave['status'] = 1;
						 
							$result = $this->leaveManagementModel->editLeave($userLeave,$where_leave);
							//  check Policy
							$where_leave_policy = array(
								'leave_policy.id' => $leave->leave_policy_id
							);
							$userLeavepolicy = $this->leaveManagementModel->getleavePolicycheck($where_leave_policy);
								// Weekend conversation	 
								if($userLeavepolicy->is_weekend_excluded == 0){
									$startDate =new DateTime($leave->start_date);	 
									$endDate =new DateTime($leave->end_date);
									$sundays = array();
	
									while ($startDate <= $endDate) {
									if ($startDate->format('w') == 0) {
									$sundays[] = $startDate->format('Y-m-d');
									}
	
									$startDate->modify('+1 day');
									}
									$arrSunday =  count($sundays);
									
									}else{
										$arrSunday =  0;
									}
									// Holiday Excluded
								if($userLeavepolicy->is_holiday_excluded == 0){
								$startDate =new DateTime($leave->start_date);	 
								$endDate =new DateTime($leave->end_date);
								$endDate = $endDate->modify( '+1 day' );
	
								$interval = new DateInterval('P1D');
								$daterange = new DatePeriod($startDate, $interval ,$endDate);
								$result=[];
								
							
								$holiday_where = array('company_holidays.branch_id' => $resultuser->branch_id);
							   
								$holidays = $this->leaveManagementModel->viewHolidayWhere($holiday_where);
								
								$arrHoliday=[];
								foreach($holidays as $holiday)
								{
									foreach($daterange as $date){
								
										$leaveStartDate = strtotime($date->format("Y-m-d"));
										$holidayDate = strtotime($holiday->holiday_date);
	
											if($leaveStartDate == $holidayDate)
											{
												$arrHoliday[] = $holiday->holiday_date;
											}	
									}
								}
								$arrcompanyHoliday =  count($arrHoliday);
								}else{
									$arrcompanyHoliday =  0;
								}
								
								
							$where_user_leave = array(
								'user_leave_details.user_id' => $leave->user_id,
								'user_leave_details.leave_policy_id' => $leave->leave_policy_id
							);
							
							$userLeaveDetail = $this->leaveManagementModel->getleaveDetailscheck($where_user_leave);
							$noofLeave = $userLeaveDetail->no_of_leave;
							$totalperyear = $userLeavepolicy->total_per_year;
							
							$changeBy2 = $this->api->_dateDiffInDays($leave->start_date,$leave->end_date);
							$changeBy3 = 0.50;
							$changeBy =$changeBy3 - $arrSunday - $arrcompanyHoliday;
							$finalBalance = $noofLeave - $changeBy;
						
						   if(($finalBalance < $changeBy) && ($finalBalance < 0))
							{
								if($userLeaveDetail->paid_leave == ''){
								$leave_data = array(
									'no_of_leave' => 0,
									'paid_leave' => $finalBalance
								);
							}else{
								$paidLeave = $userLeaveDetail->paid_leave;
								$arrFinalBal = $finalBalance + $paidLeave;
								$leave_data = array(
									'no_of_leave' => 0,
									'paid_leave' => $arrFinalBal
								);
							}
								$result = $this->leaveManagementModel->editTypeLeave($leave_data,$where_user_leave);
								if($result)
								{
									return array(
										'status' => '1',
										'message' => 'Leave Approved Successfully.'
									);
								}
								else
								{
									$this->api->_sendError("Sorry, Leave Approved Action Not Performed. Please Try Again!!");
								}
							}
							else{
								$leave_finaldata = array(
									'no_of_leave' => $finalBalance
									//'paid_leave' => $arrFinalBal
								);
								$result = $this->leaveManagementModel->editTypeLeave($leave_finaldata,$where_user_leave);
								if($result)
								{
									return array(
										'status' => '1',
										'message' => 'Leave Approved Successfully.'
									);
								}
								else
								{
									$this->api->_sendError("Sorry, Leave Approved Action Not Performed. Please Try Again!!");
								}
							}
						}
						else if($request->action == 2)			// When User Want to Dispprove The Leave
						{
							
							$userLeave['status'] = 2;
							$userLeave['reason_admin'] = $request->reason;
							
							$result = $this->leaveManagementModel->editLeave($userLeave,$where_leave);
							if($result)
							{
								return array(
									'status' => '1',
									'message' => 'Leave Disapproved Successfully.'
								);
							}
							else
							{
								$this->api->_sendError("Sorry, Leave Disapproved Action Not Performed. Please Try Again!!");
							}
						}
						else
						{
							$this->api->_sendError("Sorry, You Are Performed Wrong Task. Please Try Again!!");
						}
					}
					else if($leave->allow_short == 1) // When User Want to Half  The Leave
					{
						if($request->action == 1)			// When User Want to Approve The Leave
						{

							/*if(isset($request->user_id))
                            {
                            $user_checkid = $request->user_id;  
							}else{}
							$credential = array('id'=>$user_checkid);
							$resultuser = $this->leaveManagementModel->getUserwhere($credential);
							$attendanceData = array( 
								'user_id' => $resultuser->id,
								'emp_id' => $resultuser->emp_id,
								'start_date' => $request->start_date,
								'end_date' => $request->start_date,
								'status'=>1,
								'days' => $request->days,
							);  
							  $resultforuseArr = $this->leaveManagementModel->addAttendanceDateData($attendanceData);*/
							$userLeave['status'] = 1;
						 
							$result = $this->leaveManagementModel->editLeave($userLeave,$where_leave);
							//  check Policy
							$where_leave_policy = array(
								'leave_policy.id' => $leave->leave_policy_id
							);
							$userLeavepolicy = $this->leaveManagementModel->getleavePolicycheck($where_leave_policy);
								// Weekend conversation	 
								if($userLeavepolicy->is_weekend_excluded == 0){
									$startDate =new DateTime($leave->start_date);	 
									$endDate =new DateTime($leave->end_date);
									
									$sundays = array();
	
									while ($startDate <= $endDate) {
									if ($startDate->format('w') == 0) {
									$sundays[] = $startDate->format('Y-m-d');
									}
	
									$startDate->modify('+1 day');
									}
								//print_r($sundays);
									$arrSunday =  count($sundays);
									
									}else{
										$arrSunday =  0;
									}
									
									// Holiday Excluded
								if($userLeavepolicy->is_holiday_excluded == 0){
								$startDate =new DateTime($leave->start_date);	 
								$endDate =new DateTime($leave->end_date);
								$endDate = $endDate->modify( '+1 day' );
	
								$interval = new DateInterval('P1D');
								$daterange = new DatePeriod($startDate, $interval ,$endDate);
								$result=[];
								
							
								$holiday_where = array('company_holidays.branch_id' => $resultuser->branch_id);
							   
								$holidays = $this->leaveManagementModel->viewHolidayWhere($holiday_where);
								
								$arrHoliday=[];
								foreach($holidays as $holiday)
								{
									foreach($daterange as $date){
								
										$leaveStartDate = strtotime($date->format("Y-m-d"));
										$holidayDate = strtotime($holiday->holiday_date);
	
											if($leaveStartDate == $holidayDate)
											{
												$arrHoliday[] = $holiday->holiday_date;
											}	
									}
								}
								$arrcompanyHoliday =  count($arrHoliday);
								}else{
									$arrcompanyHoliday =  0;
								}
								
								
								
								
							$where_user_leave = array(
								'user_leave_details.user_id' => $leave->user_id,
								'user_leave_details.leave_policy_id' => $leave->leave_policy_id
							);
							
							$userLeaveDetail = $this->leaveManagementModel->getleaveDetailscheck($where_user_leave);
							$noofLeave = $userLeaveDetail->no_of_leave;
							$totalperyear = $userLeavepolicy->total_per_year;
							
							//$changeBy2 = $this->api->_dateDiffInDays($leave->start_date,$leave->end_date);
							$changeBy3 = 0.25;
							$changeBy =$changeBy3 - $arrSunday - $arrcompanyHoliday;
							$finalBalance = $noofLeave - $changeBy;
						
						   if(($finalBalance < $changeBy) && ($finalBalance < 0))
							{
								if($userLeaveDetail->paid_leave == ''){
								$leave_data = array(
									'no_of_leave' => 0,
									'paid_leave' => $finalBalance
								);
							}else{
								$paidLeave = $userLeaveDetail->paid_leave;
								$arrFinalBal = $finalBalance + $paidLeave;
								$leave_data = array(
									'no_of_leave' => 0,
									'paid_leave' => $arrFinalBal
								);
							}
								$result = $this->leaveManagementModel->editTypeLeave($leave_data,$where_user_leave);
								if($result)
								{
									return array(
										'status' => '1',
										'message' => 'Leave Approved Successfully.'
									);
								}
								else
								{
									$this->api->_sendError("Sorry, Leave Approved Action Not Performed. Please Try Again!!");
								}
							}
							else{
								$leave_finaldata = array(
									'no_of_leave' => $finalBalance
									//'paid_leave' => $arrFinalBal
								);
								$result = $this->leaveManagementModel->editTypeLeave($leave_finaldata,$where_user_leave);
								if($result)
								{
									return array(
										'status' => '1',
										'message' => 'Leave Approved Successfully.'
									);
								}
								else
								{
									$this->api->_sendError("Sorry, Leave Approved Action Not Performed. Please Try Again!!");
								}
							}
						}
						else if($request->action == 2)			// When User Want to Dispprove The Leave
						{
							$userLeave['status'] = 2;
							$userLeave['reason_admin'] = $request->reason;
							
							$result = $this->leaveManagementModel->editLeave($userLeave,$where_leave);
							if($result)
							{
								return array(
									'status' => '1',
									'message' => 'Leave Disapproved Successfully.'
								);
							}
							else
							{
								$this->api->_sendError("Sorry, Leave Disapproved Action Not Performed. Please Try Again!!");
							}
						}
						else
						{
							$this->api->_sendError("Sorry, You Are Performed Wrong Task. Please Try Again!!");
						}
					}
					else{
                    	if($request->action == 1)			// When User Want to Approve The Leave
					{
						/*if(isset($request->user_id))
                            {
								$user_checkid = $request->user_id;
								
						
							}else{}
							$credential = array('id'=>$user_checkid);
							$resultuser = $this->leaveManagementModel->getUserwhere($credential);
							
							$attendanceData = array( 
								'user_id' => $resultuser->id,
								'emp_id' => $resultuser->emp_id,
								'start_date' => $request->start_date,
								'end_date' => $request->start_date,
								'status'=>1,
								'days' => $request->days,
							);  
							  $resultforuseArr = $this->leaveManagementModel->addAttendanceDateData($attendanceData);*/
						$userLeave['status'] = 1;
						/* $user = $this->api->_GetTokenData();
		          	$user_id = $user['first_name'];
				
				   $position="Your Leave Has Beeen Approved";
                   $from="pratapsingh@ezdatechnology.com";
                 
                    $Leave_status=$request->leave_type;
                   $apply_to=$request->start_date;
                    $from_to=$request->end_date;
                   	$this->email->from($from);
               $this->email->to("pratapsingh@ezdatechnology.com");
               $this->email->subject($position);
               $url="";
             $this->email->message('
            <br><div style="background-color: #fff;"><div><center> <h2> Ezdat Technology </h2></center><hr> <br>
          <table rules="all" style="border-color: #666;" cellpadding="10">
          
          
           <tr style= "background: #cad2d9;" > <td><strong>Declined By:</strong></td>
         <td>'.$user_id.'</td></tr>
         <tr style= "background: #cad2d9;" > <td><strong>Apply On:</strong></td>
         <td>'.$apply_to.'</td></tr>
           <tr style= "background: #cad2d9;" > <td><strong>From To:</strong></td>
         <td>'.$from_to.'</td></tr>
          <tr style= "background: #cad2d9;" > <td><strong>Leave Type:</strong></td>
         <td>'.$Leave_status.'</td></tr>
          <td><strong>Your Account In Ezdat Technology Is Ready. You Can Simply Sign In Now</strong></td></tr>
            </table>
           ');
           $this->email->set_mailtype('html');
		   $this->email->send();*/
		   //echo $this->email->print_debugger();  

						$result = $this->leaveManagementModel->editLeave($userLeave,$where_leave);
						//  check Policy
						$where_leave_policy = array(
							'leave_policy.id' => $leave->leave_policy_id
						);
						$userLeavepolicy = $this->leaveManagementModel->getleavePolicycheck($where_leave_policy);
							// Weekend conversation	 
							if($userLeavepolicy->is_weekend_excluded == 0){
								$startDate =new DateTime($leave->start_date);	 
								$endDate =new DateTime($leave->end_date);
								$sundays = array();

								while ($startDate <= $endDate) {
								if ($startDate->format('w') == 0) {
								$sundays[] = $startDate->format('Y-m-d');
								}

								$startDate->modify('+1 day');
								}
								$arrSunday =  count($sundays);
                                
								}else{
									$arrSunday =  0;	
								}
								// Holiday Excluded
							if($userLeavepolicy->is_holiday_excluded == 0){
							$startDate =new DateTime($leave->start_date);	 
							$endDate =new DateTime($leave->end_date);
							$endDate = $endDate->modify( '+1 day' );

							$interval = new DateInterval('P1D');
							$daterange = new DatePeriod($startDate, $interval ,$endDate);
							$result=[];
							
						
							$holiday_where = array('company_holidays.branch_id' => $resultuser->branch_id);
                           
							$holidays = $this->leaveManagementModel->viewHolidayWhere($holiday_where);
							
						    $arrHoliday=[];
							foreach($holidays as $holiday)
							{
								foreach($daterange as $date){
							
									$leaveStartDate = strtotime($date->format("Y-m-d"));
									$holidayDate = strtotime($holiday->holiday_date);

										if($leaveStartDate == $holidayDate)
										{
											$arrHoliday[] = $holiday->holiday_date;
										}	
								}
							}
							$arrcompanyHoliday =  count($arrHoliday);
							}else{
								$arrcompanyHoliday = 0;
							}
							
							
						$where_user_leave = array(
							'user_leave_details.user_id' => $leave->user_id,
							'user_leave_details.leave_policy_id' => $leave->leave_policy_id
						);
						
						$userLeaveDetail = $this->leaveManagementModel->getleaveDetailscheck($where_user_leave);
						$noofLeave = $userLeaveDetail->no_of_leave;
						$totalperyear = $userLeavepolicy->total_per_year;
						
						$changeBy2 = $this->api->_dateDiffInDays($leave->start_date,$leave->end_date);
						$changeBy3 = $changeBy2+1;
						$changeBy =$changeBy3 - $arrSunday - $arrcompanyHoliday;
						$finalBalance = $noofLeave - $changeBy;
					
					   if(($finalBalance < $changeBy) && ($finalBalance < 0))
						{
							if($userLeaveDetail->paid_leave == ''){
							$leave_data = array(
								'no_of_leave' => 0,
								'paid_leave' => $finalBalance
							);
						}else{
							$paidLeave = $userLeaveDetail->paid_leave;
							$arrFinalBal = $finalBalance + $paidLeave;
							$leave_data = array(
								'no_of_leave' => 0,
								'paid_leave' => $arrFinalBal
							);
						}
							$result = $this->leaveManagementModel->editTypeLeave($leave_data,$where_user_leave);
							if($result)
							{
								return array(
									'status' => '1',
									'message' => 'Leave Approved Successfully.'
								);
							}
							else
							{
								$this->api->_sendError("Sorry, Leave Approved Action Not Performed. Please Try Again!!");
							}
						}
						else{
							$leave_finaldata = array(
								'no_of_leave' => $finalBalance
								//'paid_leave' => $arrFinalBal
							);
							$result = $this->leaveManagementModel->editTypeLeave($leave_finaldata,$where_user_leave);
							if($result)
							{
								return array(
									'status' => '1',
									'message' => 'Leave Approved Successfully.'
								);
							}
							else
							{
								$this->api->_sendError("Sorry, Leave Approved Action Not Performed. Please Try Again!!");
							}
						}
						
						
						
					}
					else if($request->action == 2)			// When User Want to Dispprove The Leave
					{
						$userLeave['status'] = 2;
						$userLeave['reason_admin'] = $request->reason;
						/* $user = $this->api->_GetTokenData();
			            $user_id = $user['first_name'];
				   $reasonDeclined=$request->reason;
				   $position="Your Leave Has Beeen Declined";
                   $from="pratapsingh@ezdatechnology.com";
                  $to=$request->email;
                    $Leave_status=$request->leave_type;
                   $apply_to=$request->start_date;
                    $from_to=$request->end_date;
                   	$this->email->from($from);
               $this->email->to("pratapsingh@ezdatechnology.com");
               $this->email->subject($position);
               $url="";
             $this->email->message('
            <br><div style="background-color: #fff;"><div><center> <h2> Ezdat Technology </h2></center><hr> <br>
          <table rules="all" style="border-color: #666;" cellpadding="10">
      
          <tr style= "background: #cad2d9;" > <td><strong>Reason:</strong></td>
          <td>'.$reasonDeclined.'</td></tr>
           <tr style= "background: #cad2d9;" > <td><strong>Declined By:</strong></td>
         <td>'.$user_id.'</td></tr>
         <tr style= "background: #cad2d9;" > <td><strong>Apply On:</strong></td>
         <td>'.$apply_to.'</td></tr>
           <tr style= "background: #cad2d9;" > <td><strong>From To:</strong></td>
         <td>'.$from_to.'</td></tr>
          <tr style= "background: #cad2d9;" > <td><strong>Leave Type:</strong></td>
         <td>'.$Leave_status.'</td></tr>
          <td><strong>Your Account In Ezdat Technology Is Ready. You Can Simply Sign In Now</strong></td></tr>
            </table>
           ');
           $this->email->set_mailtype('html');
           $this->email->send();*/
						$result = $this->leaveManagementModel->editLeave($userLeave,$where_leave);
						if($result)
						{
							return array(
								'status' => '1',
								'message' => 'Leave Disapproved Successfully.'
							);
						}
						else
						{
							$this->api->_sendError("Sorry, Leave Disapproved Action Not Performed. Please Try Again!!");
						}
					}
					else
					{
						$this->api->_sendError("Sorry, You Are Performed Wrong Task. Please Try Again!!");
					}
					}

				
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Leave Type.");
		}
    }
// total Employee 
public function _totalEmployeeLeave($request)
{		
	
	$module_name = 'leave_management';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_view == 'Y')
	{		
		/* Here we Add the userss.. If User has the permission for that. */

		if(TRUE)
		{
			if(isset($request->user_id))
				$users_where['users.id'] = $request->user_id;

			$user_ids = $this->users->_getUserId();
			if(!empty($user_ids))
				$where = 'users.id IN ('.implode(',',$user_ids).') ';
			else
				$where = 'users.id = 0 ';

			if(!empty($users_where))
			{
				$result = $this->leaveManagementModel->viewTotalEmployee($users_where,$where);
				return $result;
			}
			else
			{
				$result = $this->leaveManagementModel->viewEmployee($where);
				return $result;
			}

		
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
	}
}
    public function _viewAllLeave($request)
    {		
		$module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_leave = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leave Type.. If User has the permission for that. */
		if(TRUE)
			{
				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;
			  	$user_ids = $this->users->_getUserId();
					$where = 'users.id IN ('.implode(',',$user_ids).')';
				if(!empty($users_where))
				{
					$result = $this->leaveManagementModel->viewLeaveWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->leaveManagementModel->viewLeave($where);
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Leave Type.");
		}
	}
		public function _viewAllLeave_Where($request)
    {		
		// In This Method Only Logged In User Is View His Leave 
		// There is No Functionality Like To View Other User
		$module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$leave_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leaves.. If User has the permission for that. */
		
			if(TRUE)
			{
			
				if(isset($request->leave_id))
				{
					$leave_where['user_leave.id'] = $request->leave_id;
				}
				$result = $this->leaveManagementModel->viewLeave($leave_where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Leave.");
		}
	}
	public function _fullLeaveReport($request)
	{
		$module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Full Leaves Report. If User has the permission for that. */
			if(TRUE)
			{
			    	$user_ids = $this->users->_getUserId();
			
					$where = 'udetail.user_id IN ('.implode(',',$user_ids).')';
				
				$result = $this->leaveManagementModel->viewLeaveReport($where);
				return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Full Leave Report.");
		}
	}

	public function _setUserLeave($request)
	{
		$module_name = 'leave_management';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y' AND $permission->can_view == 'Y')
		{
			if(isset($request->user_id) AND isset($request->leave_number))
			{
				$user_where = array(
					'user_id' => $request->user_id
				);

				$result2 = $this->leaveManagementModel->deleteUserLeave($role_where);
				$leave_number = $request->leave_number;
				$result = $this->leaveManagementModel->setUserLeave($leave_number);

				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Leave Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, User Leave Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View and Edit Leaves.");
		}
	}
}
