<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: LeaveManagementModel.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Models of the Leave Types.
*/

class LeaveManagementModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
 //view Total Employee
 public function viewTotalEmployee($where,$where2)
 {
     
    $this->db->select('count(*) as id' );
     $this->db->from('users');
      $this->db->where($where);
      $this->db->where($where2);
     $query = $this->db->get();
     return $query->result();
 }
  public function viewEmployee($where)
 {
     
    $this->db->select('count(*) as id' );
     $this->db->from('users');
      $this->db->where($where);
   
     $query = $this->db->get();
     return $query->result();
 }
    public function addLeaveType($data)
    {
        $this->db->insert('company_leave_types',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function viewHolidayWhere($where)
    {
        $this->db->select('company_holidays.id as holiday_id, company_holidays.holiday_title, company_holidays.holiday_date');
        $this->db->from('company_holidays');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function deleteLeaveType($where_leave_type)
    {
        $this->db->where($where_leave_type);
        $this->db->delete('company_leave_types');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editLeave($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_leave',$data);
        
        if($this->db->affected_rows() != 1)
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function editTypeLeave($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_leave_details',$data);
        
        if($this->db->affected_rows() != 1)
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // check attendance date 

public function addAttendanceDateData($data)
{
    $this->db->insert('attendance_mark',$data);
    $insert_id = $this->db->insert_id();
    if($insert_id != 0)
    {
        return TRUE;
    }
    else    
    {
        return FALSE;
    }
}
    public function getUserwhere($credential) {
        // Check The Credentials
        $query = $this->db->get_where('users' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('users');
          
            $this->db->where('users.id', $row->id);
        
           $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
    public function updateUserLeave($finalBalance,$where)
    {
        $this->db->set('no_of_leave', 'no_of_leave-'.$finalBalance.'', FALSE);
        $this->db->where($where);
        $this->db->update('user_leave_details');
        
        if($this->db->affected_rows() != 1)
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function updateUserLeavepaid($finalBalance,$where)
    {
        $this->db->set('paid_leave', 'paid_leave-'.$finalBalance.'', FALSE);
        $this->db->where($where);
        $this->db->update('user_leave_details');
        
        if($this->db->affected_rows() != 1)
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function updateUserLeavepaidSet($test,$where)
    {
        $this->db->set('no_of_leave', 'no_of_leave-'.$test.'', FALSE);
        $this->db->where($where);
        $this->db->update('user_leave_details');
        
        if($this->db->affected_rows() != 1)
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
    public function viewLeaveWhere($where,$where2)
    {
        $this->db->select('user_leave.id as leave_id,company_branches.branch_name,users.user_code,company_designations.designation_name,company_departments.department_name, user_leave.user_id,user_leave.allow_half,user_leave.allow_short, users.first_name, users.email, users.emp_id, user_leave.start_date, user_leave.end_date, user_leave.reason,user_leave.status,leave_policy.policy_name,user_details.user_image,user_leave.reason_admin');
        $this->db->from('users');
         $this->db->join('user_leave','user_leave.user_id = users.id');
        $this->db->join('leave_policy','leave_policy.id = user_leave.leave_policy_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->where($where);
        $this->db->where($where2);
        $query = $this->db->get()->result();
        foreach ($query as $key => $value) {
          
            $date1 = strtotime($value->start_date);  
            $date2 = strtotime( $value->end_date); 
            $diff = abs($date2 - $date1);  
            $years = floor($diff / (365*60*60*24));  
            $months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24)); 
            $days = floor(($diff - $years * 365*60*60*24 -  
            $months*30*60*60*24)/ (60*60*24)); 
            $dateDif = "";
        	if($value->allow_short==1)
            {
                $dateDif = 0.25;
            }
            else if($value->allow_half == 1)
            {
              $dateDif  = 0.50;  
            }
            else{
                $dateDif  = $days;   
            }
            $data_leave['leave_id'] = $value->leave_id;
            $data_leave['user_id'] = $value->user_id;
            $data_leave['first_name'] = $value->first_name;
            $data_leave['allow_short'] = $value->allow_short;
            $data_leave['allow_half'] = $value->allow_half;
            $data_leave['start_date'] = $value->start_date;
            $data_leave['end_date'] = $value->end_date;
            $data_leave['reason'] = $value->reason;
            $data_leave['status'] = $value->status;
            $data_leave['policy_name'] = $value->policy_name;
            $data_leave['emp_id'] = $value->emp_id;
            $data_leave['status'] = $value->status;
            $data_leave['branch_name'] = $value->branch_name;
            $data_leave['designation_name'] = $value->designation_name;
            $data_leave['department_name'] = $value->department_name;
            $data_leave['user_code'] = $value->user_code;
            $data_leave['reason_admin'] = $value->reason_admin;
           
            $data_leave['days'] = $dateDif;
            $result_holiday[] = $data_leave;
           
     }
        return $result_holiday;
    }

    public function viewLeave($where2)
    {
        $this->db->select('user_leave.id as leave_id,company_branches.branch_name,users.user_code,company_designations.designation_name,company_departments.department_name, user_leave.user_id,user_leave.allow_half,user_leave.allow_short, users.first_name, users.email, users.emp_id, user_leave.start_date, user_leave.end_date, user_leave.reason,user_leave.status,leave_policy.policy_name,user_details.user_image,user_leave.reason_admin');
        $this->db->from('users');
        $this->db->join('user_leave','users.id = user_leave.user_id');
        $this->db->join('leave_policy','leave_policy.id = user_leave.leave_policy_id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->where($where2);
        $query = $this->db->get()->result();
        foreach ($query as $key => $value) {
           
            $date1 = strtotime($value->start_date);  
            $date2 = strtotime( $value->end_date); 
            $diff = abs($date2 - $date1);  
            $years = floor($diff / (365*60*60*24));  
            $months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24)); 
            $days = floor(($diff - $years * 365*60*60*24 -  
            $months*30*60*60*24)/ (60*60*24)); 
            $dateDif = "";
        	if($value->allow_short==1)
            {
                $dateDif = 0.25;
            }
            else if($value->allow_half == 1)
            {
              $dateDif  = 0.50;  
            }
            else{
                $dateDif  = $days;   
            }
            $data_leave['leave_id'] = $value->leave_id;
            $data_leave['user_id'] = $value->user_id;
            $data_leave['first_name'] = $value->first_name;
            $data_leave['allow_short'] = $value->allow_short;
            $data_leave['allow_half'] = $value->allow_half;
            $data_leave['start_date'] = $value->start_date;
            $data_leave['end_date'] = $value->end_date;
            $data_leave['reason'] = $value->reason;
            $data_leave['status'] = $value->status;
            $data_leave['policy_name'] = $value->policy_name;
            $data_leave['emp_id'] = $value->emp_id;
            $data_leave['branch_name'] = $value->branch_name;
            $data_leave['designation_name'] = $value->designation_name;
            $data_leave['department_name'] = $value->department_name;
            $data_leave['user_code'] = $value->user_code;
            $data_leave['reason_admin'] = $value->reason_admin;
            $data_leave['user_image'] = $value->user_image;
            $data_leave['days'] = $dateDif;
            $result_holiday[] = $data_leave;
           
     }
        return $result_holiday;
    }

    public function getLeaveWhere($where)
    {
        $query = $this->db->get_where('user_leave', $where);
        return $query->result();
    }

    public function getLeaveTypeWhere($where)
    {
        $query = $this->db->get_where('leave_policy', $where);
        return $query->result();
    }

    public function viewLeaveReport($where)
    {
        $this->db->select('user_leave_details.user_id, user_leave_details.no_of_leave as remaining_leave, cleave.leave_type,  cleave.no_of_leave as total_leave, users.first_name, users.emp_id');
        $this->db->from('user_leave_details');
        $this->db->join('users', 'users.id = user_leave_details.user_id');
        $this->db->join('company_leave_types as cleave', 'user_leave_details.leave_type_id = cleave.id');
      
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function deleteUserLeave($where_user)
    {
        $this->db->where($where_user);
        $this->db->delete('user_leave_details');
        
        if($this->db->affected_rows() == 0) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function setUserLeave($data)
    {
        $this->db->insert_batch('user_leave_details',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }


    public function getleaveDetailscheck($where_user_leave) {
        // Check The Credentials
        $query = $this->db->get_where('user_leave_details' , $where_user_leave);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('user_leave_details');
          
            $this->db->where('user_leave_details.id', $row->id);
        
           $userLeaveDetail = $this->db->get()->row();

            return $userLeaveDetail;
        }
        else{
            return FALSE;
        }
    }

    public function getleavePolicycheck($where_leave_policy) {
        // Check The Credentials
        $query = $this->db->get_where('leave_policy' , $where_leave_policy);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('*');
            $this->db->from('leave_policy');
          
            $this->db->where('leave_policy.id', $row->id);
        
           $userLeavepolicy = $this->db->get()->row();

            return $userLeavepolicy;
        }
        else{
            return FALSE;
        }
    }

}