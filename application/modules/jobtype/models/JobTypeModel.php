<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: JobTypeModel.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Models of the Leave Types.
*/

class JobTypeModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
// check job type if allready exist in data base 
public function checkJobType($where_type)
{
$query = $this->db->get_where('job_types',$where_type);
 if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
}
    public function addJobType($data)
    {
        $this->db->insert('job_types',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    
    public function adddashboardType($data)
    {
        $this->db->insert('dash_request',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function addContactsType($data)
    {
        $this->db->insert('demo_request',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    public function addContactType($data)
    {
        $this->db->insert('contact_form',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
    
    public function deleteJobType($where_leave_type)
    {
        $this->db->where($where_leave_type);
        $this->db->delete('job_types');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editJobType($data,$where)
    {
        $this->db->where($where);
        $this->db->update('job_types',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
//is active branch
     public function viewactiveTypeJob()
    {
       $query = $this->db->query("SELECT * FROM `job_types` WHERE status = 0");
         return $query->result_array(); 
    }
    public function viewJobType()
    {
        $this->db->select('*');
        $this->db->from('job_types');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewJobTypeWhere($where)
    {
        $query = $this->db->get_where('job_types', $where);
        return $query->result();
    }
}