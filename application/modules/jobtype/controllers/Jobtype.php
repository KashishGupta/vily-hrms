<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Jobtype.php
	Date: November 27, 2019
	Info: This is the main class which is hold all the Functions of the Jobtype.
*/

class Jobtype extends MY_Controller{

    public function __construct(){
		parent::__construct();
		$this->load->model('jobTypeModel');
	}
  public function _chechJobTYpeAvailablity($request)
  {
  $module_name = 'job_type';
  $permissions = $this->users->_checkPermission($module_name);
  foreach($permissions as $permission){}
  	if($permission->can_add == 'Y')
		{
			/* Here we check the Job Type. If User has the permission for that. */

			if(isset($request->job_type))
			{
				$where_type = array(
					'job_type' => $request->job_type
				);

				$result = $this->jobTypeModel->checkJobType($where_type);
				if($result)
					return array("message" => $request->job_type." is Available.");
				else
					$this->api->_sendError("Job Type Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Job Type.");
		}
    }	
	public function _addJobType($request)
    {
		$jobType = array();
		$module_name = 'job_type';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Job Type.. If User has the permission for that. */

			if(isset($request->job_type) AND isset($request->jobtype_description))
			{
                $status = $this->_chechJobTYpeAvailablity($request);
				$jobType = array(
					'job_type' => $request->job_type,
                'status'=>0,
                'jobtype_description'=> $request->jobtype_description
				);

				if(isset($request->type_color))
					$jobType['type_color'] = $request->type_color;
				else	
					$jobType['type_color'] = "#009efb"; 		// Primary Blue Color
				
				$result = $this->jobTypeModel->addJobType($jobType);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Type Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Type Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Job Type.");
		}
	}
	
	public function _dash_form($request)
    {
		//$jobType = array();
		
			/* Here we Add the Job Type.. If User has the permission for that. */

			if(isset($request->first_name) AND isset($request->email) AND isset($request->phone_no) AND  isset($request->company_name) AND isset($request->description))
			{
               
				$contactType = array(
					'first_name' => $request->first_name,
				'email'=>$request->email,
				'phone_no'=>$request->phone_no,
				'company_name'=>$request->company_name,
                'description'=> $request->description
				);
				
				$result = $this->jobTypeModel->adddashboardType($contactType);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Mail Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Mail Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
	public function _contacts_Form($request)
    {
		//$jobType = array();
		
			/* Here we Add the Job Type.. If User has the permission for that. */

			if(isset($request->name) AND isset($request->email) AND isset($request->total_employee) AND  isset($request->company_name) AND  isset($request->budget)  AND  isset($request->description))
			{
               
				$contactType = array(
					'name' => $request->name,
				'email'=>$request->email,
				'company_name'=>$request->company_name,
				'total_employee'=>$request->total_employee,
				'budget'=>$request->budget,
                'description'=> $request->description
				);
				
				$result = $this->jobTypeModel->addContactsType($contactType);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Mail Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Mail Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }
	public function _contactForm($request)
    {
		$jobType = array();
		
			/* Here we Add the Job Type.. If User has the permission for that. */

			if(isset($request->name) AND isset($request->email) AND isset($request->contact_no) AND  isset($request->company_name) AND  isset($request->total_employee)  AND  isset($request->message))
			{
               
				$contactType = array(
					'name' => $request->name,
				'email'=>$request->email,
				'company_name'=>$request->company_name,
				'total_employee'=>$request->total_employee,
				'message'=>$request->message,
                'contact_no'=> $request->contact_no
				);
				
				$result = $this->jobTypeModel->addContactType($contactType);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Mail Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Mail Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
    }

    public function _deleteJobType($request)
    {		
		$module_name = 'job_type';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Job Type.. If User has the permission for that. */

			if(isset($request->job_type_id))
			{
				$where_job_type = array(
					'id' => $request->job_type_id,
				);
				$result = $this->jobTypeModel->deleteJobType($where_job_type);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Type Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Type Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Job Type.");
		}
    }

    public function _editJobType($request)
    {
		$jobType = array();
		$module_name = 'job_type';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Job Type.. If User has the permission for that. */

			if(isset($request->job_type_id))
			{
				$where_job_type = array(
					'id' => $request->job_type_id
				);

				if(isset($request->job_type))
					$jobType['job_type'] = $request->job_type;
			//	$status = $this->_chechJobTYpeAvailablity($request);
                if(isset($request->jobtype_description))
					$jobType['jobtype_description'] = $request->jobtype_description;
				if(isset($request->type_color))
					$jobType['type_color'] = $request->type_color;

				$result = $this->jobTypeModel->editJobType($jobType,$where_job_type);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Type Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Type Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Job Type.");
		}
    }
// Active or iactive status
 public function _editJobTypeActive($request)
    {
		$jobType = array();
		$module_name = 'job_type';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Job Type.. If User has the permission for that. */

			if(isset($request->job_type_id))
			{
				$where_job_type = array(
					'id' => $request->job_type_id
				);

				if(isset($request->status))
					$jobType['status'] = $request->status;
    
				$result = $this->jobTypeModel->editJobType($jobType,$where_job_type);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Job Type Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Job Type Status Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Job Type Status.");
		}
    }
//view Job Type
    public function _viewJobType($request)
    {		
		$module_name = 'job_type';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where_job_type = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Leave Type.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->job_type_id))
				{
					$where_job_type['id'] = $request->job_type_id;
					$result = $this->jobTypeModel->viewJobTypeWhere($where_job_type);
					return $result;
				}
				else
				{
					$result = $this->jobTypeModel->viewJobType();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Job Type.");
		}
    }
    
    // Active Job Type
    public function _viewactiveJobType($request)
    {		
		$module_name = 'job_type';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		if($permission->can_view == 'Y')
		{
			/* Here we View the job_type.. If User has the permission for that. */

			if(TRUE)
			{
					$result = $this->jobTypeModel->viewactiveTypeJob();
					return $result;
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Job TYpe.");
		}
    }
}
