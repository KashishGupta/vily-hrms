<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
 
 //require_once BASEPATH. 'libraries/Session/session.php';
/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Users.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the users.
*/

class Users extends MY_Controller {

    public function __construct(){
		parent::__construct();
     
    $this->load->library('email');	
		$this->load->model('userModel');	
	}


	// User Tab Login
	public function _userTabLogin($request)
	{
		if(isset($request->emp_id))
		{
			$empid = $request->emp_id;
		
			
			// Set Credential
			$credential	=	array('emp_id' => $empid);
			$user = $this->userModel->login($credential);

			if($user == FALSE)
			{
				$test = array('message' => 'Invalid Login Details.');
				$this->api->_sendError($test);
				//$this->api->_sendError("Invalid Login Details..");
			}
			else
			{
				if($user->is_active == 0)
				{
					$test = array('message' => 'This User is Currently Inactive.');
					$this->api->_sendError($test);
				//	$this->api->_sendError("This User is Currently Inactive.");
				}
				
				return $user;
			}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	// its use to ezdatechnology attendance App
	public function _userAppLogin($request)
	{
		if(isset($request->email) AND isset($request->password) AND isset($request->imei))
		{
			$email = $request->email;
			$password = $email."".md5($request->password);
			
			// Set Credential
			$credential	=	array('email' => $email , 'password' => md5($password));
			// check The credetial email or password 
			$user = $this->userModel->login($credential);
			$credential2	=	array('imei_number' => $request->imei);
			$userImei = $this->userModel->getdatawhere('users', $credential2);
			$counters = $userImei->result();
		
			
			if($user == FALSE)
			{
				$test = array('message' => 'Invalid Login Details.');
				$this->api->_sendError($test);
			}else{
             if($user->role_id == 1){
				
				if($user->is_device_registered == 0){
					foreach($counters as $counter){
					if($counter->imei_number == $request->imei)
					{
						$test = array('message' => 'User Already Register This Device.');
				$this->api->_sendError($test);
				
				}
			}
					if(!empty($user->id)){
						$users = array( 
							'imei_number' => $request->imei,
						 	'is_device_registered' => 1
						);   
						$users_where = array(
							'id' => $user->id
						); 
						$result = $this->userModel->editUser($users,$users_where); 
						
					 }
				
				}
			
				if($user->is_active == 0)
				{
					$test = array('message' => 'This User is Currently Inactive.');
				$this->api->_sendError($test);
					
				}
				
				return $user;
			
		  }else{
			if($user->is_device_registered == 0){
				foreach($counters as $counter){
				if($counter->imei_number == $request->imei)
				{
					$test = array('message' => 'User Already Register This Device.');
			$this->api->_sendError($test);
			
			}
		}
		
				if(!empty($user->id)){
					$users = array( 
						'imei_number' => $request->imei,
						'is_device_registered' => 1
					);   
					$users_where = array(
						'id' => $user->id
					); 
					$result = $this->userModel->editUser($users,$users_where); 
					
				 }
		
					}
			
				if($user->is_active == 0)
				{
					$test = array('message' => 'This User is Currently Inactive.');
				$this->api->_sendError($test);
					
				}
				
				return $user;
			
		}
	}
		}
		
		else
		{
			$test = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!');
				$this->api->_sendError($test);
		
		}
	}
	public function _addUserBulk($request)
    {
		
		$module_name = 'users';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Branches.. If User has the permission for that. */

			if(isset($request->csvFile))
			{
			   $baseimage = $request->csvFile;
				$source_image = str_replace('data:application/vnd.ms-excel;base64,','',$baseimage);
				//print_r($source_image);
				$decoded = base64_decode($source_image);
				$arrCsvData=explode("\n",$decoded);
				//print_r($arrCsvData);
				//die;
				array_pop($arrCsvData);
				array_shift($arrCsvData);
				$full_stats = array();
				foreach($arrCsvData as $line) {
					$full_stats[] = str_getcsv($line, "\t");
					foreach($full_stats as $Csv => $arrvalue)
					{
					 foreach($arrvalue as $arrCsv => $arrvaluebreak)
					{
						if (!empty($arrvaluebreak)) {
							
						 
						 $explode=explode(',',$arrvaluebreak);	
						
						 $user = array(
							'device_token' => 'web',
							'user_code'=> $this->api->_getRandomString(6),
							'is_active' => 1,
							'leave_policy_id'=>0,
							'parent_id'=>0,
							'first_name' => $explode[0],
							'phone'=> $explode[1],
							'email' => $explode[2],
							'password' => md5($explode[2].''.md5( $explode[3]))
							
						);
						$userDetail = array(
							'father_name' => $explode[4],
							'personal_email'=>$explode[5],
							'address' =>$explode[6],
							'gender' => $explode[7],
							'dob'=> $explode[8],
						    'joining_date' => $explode[9],
						);
						$userPersonal = array(
							'passport_no' => $explode[10],
							'passport_expiry'=>$explode[11],
							'nationality' =>$explode[12],
							'religion' => $explode[13],
							'marital_status'=> $explode[14],
						    'no_of_children' => $explode[15],
							'contactperson_name'=> $explode[16],
							'contactperson_no'=> $explode[17],
							'relationship'=> $explode[18],

						);
						//print_r($userPersonal);
						//die;
						}					}
					}
				 $this->db->insert('users',$user);
				 $last_id = $this->db->insert_id();
			$empId = 'EMP'.$last_id;
			$data2['emp_id'] = $empId;
         	$this->db->where('id',$last_id);
       
			$this->db->update('users',$data2);	
            $userDetail['user_id']=$last_id;
			$this->db->insert('user_details',$userDetail);
			$userPersonal['user_id']=$last_id;
			$this->db->insert('user_personal',$userPersonal);
			$onboardingstatus = array(
				'user_id' => $last_id,
				'status' =>0
			);
			$this->db->insert('onboarding_status',$onboardingstatus);
			
			  //------- user Offboarding status Document
			  $onffboardingstatus = array(
				'user_id' => $last_id,
				'status' =>0
			);
			$this->db->insert('offboarding_status',$onffboardingstatus);
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Branch.");
		}
    }
public function _userLogin($request)
	{
		if(isset($request->email) AND isset($request->password))
		{
			$email = $request->email;
			$password = $email."".md5($request->password);
			
			// Set Credential
			$credential	=	array('email' => $email , 'password' => md5($password));
			$user = $this->userModel->login($credential);
			
			if($user == FALSE)
			{
				$test = array('message' => 'Invalid Login Details.');
				$this->api->_sendError($test);
			}else{
             if($user->role_id == 1){
				if($user->is_active == 0)
				{
					$test = array('message' => 'This User is Currently Inactive.');
				$this->api->_sendError($test);
					
				}	
				return $user;
			
		  }else{
			
				if($user->is_active == 0)
				{
					$test = array('message' => 'This User is Currently Inactive.');
				$this->api->_sendError($test);
					
				}
				return $user;
			
		}
	}
		}
		
		else
		{
			$test = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!');
				$this->api->_sendError($test);
		
		}
	}
// user Company Log in For Tab 
public function _userLoginCompany($request)
	{
		if(isset($request->app_email) AND isset($request->app_password))
		{
			$email = $request->app_email;
			$password = $email."".md5($request->app_password);
			
			// Set Credential
			$credential	=	array('app_email' => $email , 'app_password' => md5($password));
			//print_r($credential);
			$user = $this->userModel->login($credential);
			
			if($user == FALSE)
			{
				$test = array('message' => 'Invalid Login Details.');
				$this->api->_sendError($test);
			}else{
				if($user->role_id == 0)
				{
					$test = array('message' => 'Invalid User');
					$this->api->_sendError($test);
				}	
				return $user;
			
		  
    	}
		}
		else
		{
			$test = array('message' => 'Sorry, There is Some Parameter Missing. Please Try Again!!');
			$this->api->_sendError($test);
		}
	}
// check get Current user Permission
	public function _getCurrentUserPermission($request)
	{
		$user = $this->api->_GetTokenData();
		$permission = $this->userModel->getUserPermission($user['first_name']);
  // echo "Test";
		return $permission;
	}

	// Not Call From API it's Called From Other Controller
	public function _checkPermission($module_name)
	{
		$user = $this->api->_GetTokenData();
		$userId = $user['role_id'];
		if($userId == 1){
		if(!$this->userModel->checkUserStatus($user['id']))
		{
			$this->api->_sendError("This User is Currently Inactive.");
		}
		$permission = $this->userModel->getUserPermissionByModule($user['role_id'],$module_name);
		return $permission;
	}else{
		$user = $this->api->_GetTokenData();
		if(!$this->userModel->checkUserStatus($user['id']))
		{
			$this->api->_sendError("This User is Currently Inactive.");
		}
		$permission = $this->userModel->getUserPermissionByModuleDesignation($user['designation_id'],$module_name);
		return $permission;	
	}
	}
	
	public function _checkDateAvailability($request)
	{		
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the email. If User has the permission for that. */

			if(isset($request->holiday_date))
			{
				$where_Date = array(
					'holiday_date' => $request->holiday_date
				);
                // check date Availability in Holiday Table
				$result = $this->userModel->checkDateAvailability($where_Date);
				if($result)
					return array("message" => $request->holiday_date." is Available.");
				else
					$this->api->_sendError("Date Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Holiday.");
		}
    }
    
    //Birthday Upcoming 
    public function _checkUpcomingBirthday($request)
	{			
			/* Here we check the email. If User has the permission for that. */
      if(TRUE)
      {	
      if(isset($request->user_id))
      {
      	$useridwhere['users.id'] = $request->user_id;				
			$result = $this->userModel->viewUser($useridwhere);
				return $result;
      }
      else
      {
      $result = $this->userModel->checkUsersBirthday();
      return $result;
      }
      }  
       else
       {
       $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
       }
   
	}
	// birthday user Celebration logged in user
    public function _checkUserBirthday($request)
	{			
	/* Here we check the email. If User has the permission for that. */
      if(TRUE)
      {
      $result = $this->userModel->checkCelebrationBirthday();
      return $result;
      }  
       else
       {
       $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
       }
   
    }
    //count all new employee cont by joining date
    public function _countAllNewEmployee($request){
    $module_name = 'employee';
    $permissions = $this->_checkPermission($module_name);
    foreach($permissions as $permission){}
    
    if($permission->can_view == 'Y')
    {
    // if user Give Permission  then View All New Employees
    $result = $this->userModel->checkAllNewEmployee();
    return $result;
    
    }
    else{
    $this->api->_sendError("Sorry, You Have No Permission View New Employee.");
    }
    
    }
    //check new employee joining
    public function _checkNewEmployeedata($request){
    $module_name = 'employee';
    $permissions = $this->_checkPermission($module_name);
    foreach($permissions as $permission){}
    
    if($permission->can_view == 'Y')
    {
    // if user Give Permission  then View All New Employees
    $result = $this->userModel->checkAllNewEmployee();
    return $result;
    
    }
    else{
    $this->api->_sendError("Sorry, You Have No Permission View New Employee.");
    }
    
    }
    // Change Password Api
    	public function _changePassword($request)
	{		
		
			/* Here we check the email. If User has the permission for that. */

			if(isset($request->old_password) AND isset($request->user_id) AND isset($request->email) AND isset($request->password) AND isset($request->confirm_password))
			{
      $passcheck = $request->password;
      // Validate password strength
      $uppercase = preg_match('@[A-Z]@', $passcheck);
      $lowercase = preg_match('@[a-z]@', $passcheck);
      $number    = preg_match('@[0-9]@', $passcheck);
      $specialChars = preg_match('@[^\w]@', $passcheck);

if(!$uppercase || !$lowercase || !$number || !$specialChars) {
  
    	$this->api->_sendError(" Sorry, include at least one upper case letter, one number, and one special character.");
}else if(strlen($passcheck) < 6){
	$this->api->_sendError("Sorry, Password should be at least 6 characters in length!!");
}
      if($request->password != $request->confirm_password)
				{
					$this->api->_sendError("Password And Confirm Password Do Not Match.");
				}
			    $email=$request->email;
			    	$password = $email."".md5($request->old_password);
				$where_password = array(
					'password' => md5($password)
				);

				$result = $this->userModel->checkOldPasswordAvailability($where_password);
				if($result == TRUE){
			 $users_where = array(
					'id' => $request->user_id
				);

				$users = array(
					'email' => $request->email,
					'password' => md5($request->email.''.md5($request->password))
				);
				
				$result = $this->userModel->editUser($users,$users_where); 
					if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Password Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Password Not Updated. Please Try Again!!");
				}
				}
				else{
					$this->api->_sendError("Sorry, Old Password Is Not Match Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		
	}
	

	 // Change Password Api
	 public function _changePasswordEmployee($request)
	 {		
		 
			 /* Here we check the email. If User has the permission for that. */
 
			 if(isset($request->user_id) AND isset($request->email) AND isset($request->password) AND isset($request->confirm_password))
			 {
	   $passcheck = $request->password;
	   // Validate password strength
	   $uppercase = preg_match('@[A-Z]@', $passcheck);
	   $lowercase = preg_match('@[a-z]@', $passcheck);
	   $number    = preg_match('@[0-9]@', $passcheck);
	   $specialChars = preg_match('@[^\w]@', $passcheck);
 
 if(!$uppercase || !$lowercase || !$number || !$specialChars) {
   
		 $this->api->_sendError(" Sorry, include at least one upper case letter, one number, and one special character.");
 }else if(strlen($passcheck) < 6){
	 $this->api->_sendError("Sorry, Password should be at least 6 characters in length!!");
 }
	   if($request->password != $request->confirm_password)
				 {
					 $this->api->_sendError("Password And Confirm Password Do Not Match.");
				 }
 
			  $users_where = array(
					 'users.id' => $request->user_id
				 );
 
				 $users = array(
					'email' => $request->email,
					 'password' => md5($request->email.''.md5($request->password))
				 );
				
				 $result = $this->userModel->editUser($users,$users_where); 
					 if($result)
				 {
					 return array(
						 'status' => '1',
						 'message' => 'Password Updated Successfully.'
					 );
				 }
				 else
				 {
					 $this->api->_sendError("Sorry, Password Not Updated. Please Try Again!!");
				 }
				 
			 }
			 else
			 {
				 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			 }
		 
	 }
    //Check first_name Available 
	public function _checkfirst_nameAvailability($request)
	{		
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the email. If User has the permission for that. */

			if(isset($request->phone))
			{
				$where_first_name = array(
					'phone' => $request->phone
				);

				$result = $this->userModel->checkfirst_nameAvailability($where_first_name);
				if($result)
					return array("message" => $request->phone." is Available.");
				else
					$this->api->_sendError("Phone No. Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add users.");
		}
    }

	public function _checkEmailAvailability($request)
	{		
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the email. If User has the permission for that. */

			if(isset($request->email))
			{
				$where_email = array(
					'email' => $request->email
				);

				$result = $this->userModel->checkEmailAvailability($where_email);
				if($result)
					return array("message" => $request->email." is Available.");
				else
					$this->api->_sendError("Email Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add users.");
		}
	}
	
 public function _addUser($request)
{
	$module_name = 'employee';
	$permissions = $this->_checkPermission($module_name);
	foreach($permissions as $permission){}
	
	if($permission->can_add == 'Y')
	{
	/* Here we Add the users.. If User has the permission for that. */

		if(isset($request->first_name) AND isset($request->email) AND isset($request->joining_date) AND isset($request->dob) AND isset($request->password) AND isset($request->confirm_password) AND isset($request->branch_id) AND isset($request->designation_id) AND isset($request->department_id) AND isset($request->joining_date))
		{
			$passcheck = $request->password;
				// Validate password strength
		  $uppercase = preg_match('@[A-Z]@', $passcheck);
		  $lowercase = preg_match('@[a-z]@', $passcheck);
		  $number    = preg_match('@[0-9]@', $passcheck);
		  $specialChars = preg_match('@[^\w]@', $passcheck);
		  
		  if(!$uppercase || !$lowercase || !$number || !$specialChars) {
			
				  $this->api->_sendError(" Sorry, include at least one upper case letter, one number, and one special character in Password.");
		  }else if(strlen($passcheck) < 6){
			  $this->api->_sendError("Sorry, Password should be at least 6 characters in length!!");
		  }
			if($request->password != $request->confirm_password)
			{
				$this->api->_sendError("Password Mismatch.");
			}
			$leaveStartDate=strtotime($request->joining_date);
			$dayOfStartLeave = date("l", $leaveStartDate);
		
			$leaveEndDate=strtotime($request->dob);
			$dayOfEndLeave = date("l", $leaveEndDate);
		
			$currentDate = date('d-m-Y');
			$currentDate=strtotime($currentDate);
			if($leaveEndDate > $currentDate){
				$this->api->_sendError('Dob Cannot Be Add On Future Date.');
			}
			if($leaveStartDate > $currentDate){
				$this->api->_sendError('Joining Date Cannot Be Add On Future Date.');
			}
            $status = $this->_checkfirst_nameAvailability($request);
            $status2 = $this->_checkEmailAvailability($request);
		
		//--------- No Need To Add Anything in this Array
		 $userCode=  $this->api->_getRandomString(6);
			$users = array(
				'first_name' => $request->first_name,
				'email' => $request->email,
				'branch_id' => $request->branch_id,
				'designation_id' => $request->designation_id,
				'department_id' => $request->department_id,
                'device_token'=>'web',
                'phone'=>$request->phone,
				'user_code' => $userCode,
				'password' => md5($request->email.''.md5($request->password)),
				'is_active' => 1
			);
			
            $user_id = $this->userModel->insertData('users',$users);
        	
	
			//$last_id = $this->db->insert_id();
			$empId = 'EMP'.$user_id;
			$data2['emp_id'] = $empId;
         	$this->db->where('id',$user_id);
       
			$this->db->update('users',$data2);
			
			if($user_id == 0)
				$this->api->_sendError("Something Went Wrong. Please Try Again!!");
				$users_details = array(
					'user_id' => $user_id,
					'dob'=> $request->dob,
					'joining_date'=>$request->joining_date
				);
				if(isset($request->father_name))
				$users_details['father_name'] = $request->father_name;
			if(isset($request->personal_email))
				$users_details['personal_email'] = $request->personal_email;
			if(isset($request->gender))
				$users_details['gender'] = $request->gender;
			if(isset($request->address))
				$users_details['address'] = $request->address;
			if(isset($request->user_image))
				$users_details['user_image'] = $request->user_image;
				$userExperience_result = $this->userModel->insertData('user_details',$users_details);
		
              //------- user onboarding status Document
      $onboardingstatus = array(
		'user_id' => $user_id,
		'status' =>0
	);
	
	 $userExperience_result = $this->userModel->insertData('onboarding_status',$onboardingstatus);
	  //------- user Offboarding status Document
	  $onffboardingstatus = array(
		'user_id' => $user_id,
		'status' =>0
	);
	 $userofffboarding_result = $this->userModel->insertData('offboarding_status',$onboardingstatus);
			if($userofffboarding_result != 0)
			{
				return array(
					'status' => '1',
					'message' => 'User Added Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, User Not Added. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Required Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Add User.");
	}
}

    public function _deactivateUser($request)
    {	
	
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the userss.. If User has the permission for that. */

			if(isset($request->users_id) AND isset($request->status) )
			{
				$where_users = array(
					'id' => $request->users_id,
				);
			
				if($request->status == 1)
				{
					
				$data = array(
					'is_active' => 1
				);
				$result = $this->userModel->editUser($data,$where_users);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Activate Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, User Not Activate. Please Try Again!!");
				}
			}
			else if($request->status == 0)
			{
				$data = array(
					'is_active' => 0
				);
				$result = $this->userModel->editUser($data,$where_users);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'User Deactivate Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, User Not Deactivated. Please Try Again!!");
				}
			}
			else{
				$this->api->_sendError("Sorry, You Are Performed Wrong Task. Please Try Again!!");
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Users.");
		}
	}
	

    public function _viewUser($request)
    {	
			
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(!empty($request->is_active == 4))
				{
					$user_ids = $this->_getUserId();
					if(!empty($user_ids))
						$where = 'users.id IN ('.implode(',',$user_ids).') ';
					else
						$where = 'users.id = 0 ';
					$result = $this->userModel->viewUser($where);
					return $result;
				}else{
				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;	
				if(isset($request->is_active))
					$users_where['users.is_active'] = $request->is_active;
					if(isset($request->user_code))
					$users_where['users.user_code'] = $request->user_code;

					$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'users.id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'users.id = 0';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewUserWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewUser($where);
					return $result;
				}
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}

	// Edit User Details In Edit Form
	public function _viewUserEdit($request)
    {	
			
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				
				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;	
				if(isset($request->is_active))
					$users_where['users.is_active'] = $request->is_active;
					if(isset($request->user_code))
					$users_where['users.user_code'] = $request->user_code;
				if(!empty($users_where))
				{
					$result = $this->userModel->viewUserEditWhere($users_where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewUserEdit();
					return $result;
				}
			}
		
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}
	// view users in dash Board 
	public function _viewUserEmployee($request)
    {	
			
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{

					$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'users.id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'users.id = 0';

				
					$result = $this->userModel->viewUser($where);
					return $result;
		
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}
// view users in dash Board 
	public function _viewUserDashboard($request)
    {	
			
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				
				if(!empty($request->designation_id == 'all'))
				{
					$user_ids = $this->_getUserId();
					if(!empty($user_ids))
						$where = 'users.id IN ('.implode(',',$user_ids).') ';
					else
						$where = 'users.id = 0 ';
					$result = $this->userModel->viewUser($where);
					return $result;
				}else{
				if(isset($request->designation_id))
					$users_where['users.designation_id'] = $request->designation_id;

					$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'users.id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'users.id = 0';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewUserWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewUser($where);
					return $result;
				}
			}
			}
		
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}

	public function _viewUserEmployeeDashboard($request)
    {	
			
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				
				if(isset($request->user_id))
					$where['users.id'] = $request->user_id;	
				if(isset($request->is_active))
					$where['users.is_active'] = $request->is_active;
					if(isset($request->user_code))
					$where['users.user_code'] = $request->user_code;
					if(isset($request->designation_id))
					$where['users.designation_id'] = $request->designation_id;
				$user_ids = $this->_getUserId();
				
				

				if(!empty($where))
				{
					$result = $this->userModel->viewUser($where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewUseremloyee();
					return $result;
				}
			}
		
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}

	

 public function _getUserId()
 {		
	 $module_name = 'employee';
	 $permissions = $this->_checkPermission($module_name);
	 foreach($permissions as $permission){}
	 $users_where = array();
	 $user_ids = array();
	 
	 if($permission->can_view == 'Y')
	 {
		 /* Here we View the userss.. If User has the permission for that. */

		 if(TRUE)
		 {
			 
			 $user = $this->api->_GetTokenData();
			
			 $results = $this->userModel->getUserId($user['parent_id'],$user['branch_id']);
			
			 foreach($results as $result)
			 {
				 $user_ids[] = $result->id;
			 }
			 return $user_ids;
		 }
		 else
		 {
			 $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		 }
	 }
	 else
	 {
		 $this->api->_sendError("Sorry, You Have No Permission To View users.");
	 }
 }


public function _totalAbsentEmployee($request)
{
	$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{	
    if(TRUE)
        {
           if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->_getUserId();
       
				if(!empty($user_ids))
					$where = 'attendances.user_id IN ('.implode(',',$user_ids).')';
				else
					$where = 'users.id = 0';

				if(!empty($users_where))
				{
					$result = $this->userModel->checkTotalAbsentWhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->checkTotalAbsent($where);
					return $result;
				}
             }
			else
				{
				$this->api->_sendResponse("Sorry, There Is Some Perameter Missing!.");
				}
			}
				else
				{
				$this->api->_sendResponse("Sorry, You Have No Permission To View Total Absent Users.");
				}
						}



// total Employee 
	public function _totalEmployee($request)
	{		
		
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{		
			/* Here we Add the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'users.id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'users.id = 0 ';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewTotalEmployee($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewEmployee($where);
					return $result;
				}

			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}
	public function _totalMaleEmployee($request)
	{		
		
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{		
			/* Here we Add the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->user_id))
					$users_where['user_details.user_id'] = $request->user_id;

				$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'user_details.user_id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'user_details.user_id = 0 ';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewTotalMaleEmployee($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewEmployeeMale($where);
					return $result;
				}	
				
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}
	// Total Female Employee 
	public function _totalFemaleEmployee($request)
	{		
		
		$module_name = 'employee';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		if($permission->can_view == 'Y')
		{		
			/* Here we Add the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->user_id))
					$users_where['user_details.user_id'] = $request->user_id;

				$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'user_details.user_id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'user_details.user_id = 0 ';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewTotalFemaleEmployee($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewEmployeeFemale($where);
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Users.");
		}
	}
	
public function _forgetPassword($request)
	{
		if(isset($request->email))
		{
			$email = $request->email;
		
			// Set Credential
			$credential	=	array('email' => $email);
			$user = $this->userModel->login($credential);
          if($user == TRUE)
			{
					$to = $request->email;
				$position="Password Forget";
                   $from="hr@ezdatechnology.com";
                   	$this->email->from($from);
               $this->email->to($to);
               $this->email->subject($position);
               $url="http://54.88.145.186/login/resetPassword";
             $this->email->message('
            <br><div style="background-color: #fff;"><div><center> <h2> Ezdat Technology Password Reset Request </h2></center><hr> <br>
          <table rules="all" style="border-color: #666;" cellpadding="10">
           <tr style= "background: #cad2d9;" > <td><strong>Email:</strong></td>
          <td>'.$to.'</td></tr>
          <tr style= "background: #cad2d9;" > <td><strong>Url:</strong></td>
          <td><a href="'.$url.'">Reset Your Password</a></td></tr>
           
          <td><strong>Your Account In Ezdat Technology Is Ready. You Can Simply Sign In Now</strong></td></tr>
            </table>
           ');
           $this->email->set_mailtype('html');
           
            $this->email->send();
           
			}
			if($user == FALSE)
			{
				$this->api->_sendError("Invalid Email..");
			}
			else
			{
				if($user->is_active == 0)
				{
					$this->api->_sendError("This User is Currently Inactive.");
				}
				return $user;
			}
		}
		
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	
	public function _resetPassword($request)
	{		
			if(isset($request->user_id) AND isset($request->email) AND isset($request->password) AND isset($request->confirm_password))
			{
			    if($request->password != $request->confirm_password)
				{
					$this->api->_sendError("Password And Confirm Password Do Not Match.");
				}
			 $passcheck = $request->password;
      // Validate password strength
$uppercase = preg_match('@[A-Z]@', $passcheck);
$lowercase = preg_match('@[a-z]@', $passcheck);
$number    = preg_match('@[0-9]@', $passcheck);
$specialChars = preg_match('@[^\w]@', $passcheck);

if(!$uppercase || !$lowercase || !$number || !$specialChars) {
  
    	$this->api->_sendError(" Sorry, include at least one upper case letter, one number, and one special character.");
}else if(strlen($passcheck) < 6){
	$this->api->_sendError("Sorry, Password should be at least 6 characters in length!!");
}

				$users_where = array(
					'id' => $request->user_id
				);

				$users = array(
					'email' => $request->email,
					'password' => md5($request->email.''.md5($request->password)),
				);
				$result = $this->userModel->editUser($users,$users_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'password Reset Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Password Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
	
	}
  //merge leave or Tickets in a branch
	public function _mergeleavetickets($request)
{		
		$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'users.id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'users.id = 0 ';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewLeaveticketwhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewLeaveticket($where);
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}
 // view All tickets in a branch
 public function _allTicketsBranch($request)
{		
		$module_name = 'users';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$users_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the userss.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->user_id))
					$users_where['users.id'] = $request->user_id;

				$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'users.id IN ('.implode(',',$user_ids).') ';
				else
					$where = 'users.id = 0 ';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewAllTicketwhere($users_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewAllticket($where);
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View users.");
		}
	}
	// Policy Fo Select
// view Task
public function _userViewPolicy($request)
{

     if(TRUE)
	{
	    if(isset($request->user_id))
		$credential	=	array('id' => $request->user_id);
		
		$user = $this->userModel->login($credential);
		$policy = $user->leave_policy_id;
		$where = 'leave_policy.id IN ('.$policy.') ';
		$results = $this->userModel->viewPolicywhere($where);
		return $results;
   } 
	else
	{
	$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	}
  
}
 // Add Leave Policy To User
 public function _assignLeavePolicy($request)
 {
	
	 $module_name = 'employee';
	   $permissions = $this->_checkPermission($module_name);
	   foreach($permissions as $permission){}
	   
	   if($permission->can_add == 'Y')
	   {
	if(isset($request->user_id) AND isset($request->leave_policy_id))
	{ 
		
		$whereuser = implode("','",$request->leave_policy_id);
		$query = $this->db->query("SELECT * FROM `leave_policy` WHERE `policy_name` IN ('$whereuser')");
		$arrProjectsData1 = $query->result();
		
		
		$arrtempData =  array();
	  foreach($arrProjectsData1 as $team1)
	  {
		   $arrtempData[] = $team1->id;
		    /* Data Present For That Particular Leave Type With User In User_Leave_Details Check Start */
		 $userLeaveWhere = array(
			'user_id' => $request->user_id,
			'leave_policy_id' => $team1->id
		);
		
		if(!($this->userModel->userHasLeaveType($userLeaveWhere)))
		{
			$leaveNumberWhere = array(
				'id' => $team1->id
			);
			$numberOfLeaves = $this->userModel->getNumberOfLeaveByType($leaveNumberWhere);
			
			$userLeaveDetails = array(
				'user_id' => $request->user_id,
				'leave_policy_id' => $team1->id,
				'no_of_leave' => $numberOfLeaves->total_per_year
			);
			$detailResult = $this->userModel->insertUserLeaveType($userLeaveDetails);
			if(!$detailResult)
			{
				$this->api->_sendError("Sorry, There is Some Internal Settings Error. Please Try Again!!");
			}
		}
	  } 
		
	   $policy_wherename = implode(",",$arrtempData);
	   $where_user = array('id' =>$request->user_id);
	   $policy_name = array('leave_policy_id' =>$policy_wherename);

	   $result = $this->userModel->editUserPolicy($policy_name,$where_user);
	   if($result)
	   {
	    return array(
		 'status' => '1',
		 'message' => 'Policy Assign To user Successfully.'
		 );
	   }
	 else
	    {
	       $this->api->_sendError("Sorry, Already policy assign to user. Please Try Again!!");
        }
	}
	else
	{
	$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	}
   } 
   else
   {
   $this->api->_sendError("Sorry, You Have No Permission To Policy Assign To user.");
   }
 }
 // assign Task 
  public function _taskAssign($request)
  {
  	$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
     if(isset($request->user_id) AND isset($request->task_name) AND isset($request->description) AND isset($request->priority) AND isset($request->due_date))
     { 
     $taskStartDate=strtotime($request->due_date);
    
         $currentDate = date('d-m-Y');
         	$currentDate=strtotime($currentDate);
      if($taskStartDate < $currentDate){
           	$this->api->_sendError('Task Cannot Be Add On Past Date.');
           }
     	$user = $this->api->_GetTokenData();
				$user_id = $user['first_name'];
     $addTask = array(
     'assign_by'=>$user_id,
     'task_name'=>$request->task_name,
     'priority'=>$request->priority,
      'description'=>$request->description,
     'due_date'=>$request->due_date,
     'user_id'=>$request->user_id
     );
     
     	$result = $this->userModel->insertData('user_task',$addTask);
      if($result)
      {
      return array(
						'status' => '1',
						'message' => 'Task Added Successfully.'
					);
      }
      else
				{
					$this->api->_sendError("Sorry, Task Not Added. Please Try Again!!");
				}
     }
     else
     {
     $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
     }
    } 
    else
    {
    $this->api->_sendError("Sorry, You Have No Permission To Assign Task.");
    }
  }
  // view Task
  public function _userViewTask($request)
  {
  
   if(TRUE)
			{
      if(isset($request->task_id))
					$task_where['user_task.id'] = $request->task_id;

				$user_ids = $this->_getUserId();
				if(!empty($user_ids))
					$where = 'user_task.user_id IN ('.implode(',',$user_ids).')';
				else
					$where = 'user_task.user_id = 0';

				if(!empty($users_where))
				{
					$result = $this->userModel->viewTaskWhere($task_where,$where);
					return $result;
				}
				else
				{
					$result = $this->userModel->viewTask($where);
					return $result;
				}
			} 
      else
      {
      $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
      }
    
  }
   
   // delete task
   public function _deleteTask($request)
   {
    	$module_name = 'employee';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
     if(isset($request->task_id))
     {
     $task_where = array('id'=>$request->task_id);
     $result = $this->userModel->deleteTask($task_where);
     if($result)
     {
     return array(
						'status' => '1',
						'message' => 'Task Deleted Successfully.'
					);
     }
      else
				{
					$this->api->_sendError("Sorry, Task Not Deletd. Please Try Again!!");
				}
     }
     else
     {
      $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
     }
    }
    else
    {
     $this->api->_sendError("Sorry, You Have No Permission To Delete Task.");
    }
}
// edit Task
 public function _editTask($request)
 {
   
    if(isset($request->task_id))
	{
		$task_where = array(
			'id' => $request->task_id
		);
	if(isset($request->status))
	   $task['status'] = $request->status;
     $result = $this->userModel->editTask($task,$task_where);
     if($result)
     {
     return array(
				'status' => '1',
				'message' => 'Task Updated Successfully.'
				);
     }
     else
     {
     $this->api->_sendError("Sorry, Task Not Updated. Please Try Again!!");
     }
    }
	else
	{
		$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
	}
   
 }
 // view task self
 public function _viewTaskSelf($request)
 {
	 
	   if(TRUE)
	   {
		$user = $this->api->_GetTokenData();
		$user_id = $user['id'];
		$users_where = array(
			'user_id' => $user_id
		);
		$result = $this->userModel->viewTask($users_where);
	    return $result;

	   }
	   else
	   {
		   $this->api->_sendError("Sorry, There Is Some Perameter Missiong. Please Try Again!!");
	   }
 }
 
// view task self
public function _viewLastLogin($request)
{
	
	  if(TRUE)
	  {
	   $user = $this->api->_GetTokenData();
	   $user_id = $user['id'];
	   $users_where = array(
		   'user_id' => $user_id
	   );
	   $result = $this->userModel->getUserLastSession($users_where);
	   return $result;

	  }
	  else
	  {
		  $this->api->_sendError("Sorry, There Is Some Perameter Missiong. Please Try Again!!");
	  }
	
}
 public function _countEmployemrntGrowth()
 {
 $module_name = 'users';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		if($permission->can_view == 'Y')
		{
			/* Here we View the users.. If User has the permission for that. */

			if(TRUE)
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['branch_id'];
					$result = $this->userModel->employeeGrowth($user_id);
					return $result;
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Employee Growth.");
		}
 }
 
 public function _countEmployeeGraph()
 {
 $module_name = 'users';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
	
		if($permission->can_view == 'Y')
		{
			/* Here we View the users.. If User has the permission for that. */

			if(TRUE)
			{
			
					$result = $this->userModel->employeeGraph();
					return $result;
			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Employee Graph.");
		}
 }
 // Change Admin Branch 
 public function _changeBranch($request)
    {
		$module_name = 'users';
		$permissions = $this->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Edit the Branch.. If User has the permission for that. */
			if(isset($request->branch_id))
			{
				$user = $this->api->_GetTokenData();
				$user_id = $user['id'];
				$branch_where = array('user_id' =>$user_id);
				$branch = array('branch_id' => $request->branch_id);
				
				$result = $this->userModel->editAdminBranch($branch,$branch_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Admin Branch Change Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Branch  Not Changed. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To User Branch.");
		}
	} 
	

	// view Accural Method
	public function _viewAccuralForLeave($request)
	{
		  if(TRUE)
		  {
			$query = $this->db->query("SELECT * FROM `accrual_method`");
			$result = $query->result();
		    return $result;
		  }
		  else
		  {
			  $this->api->_sendError("Sorry, There Is Some Perameter Missiong. Please Try Again!!");
		  } 
	}
 
}