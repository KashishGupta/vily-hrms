<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: UserModel.php
	Date: March 15, 2020
	Info: This is the main class which is hold all the Models of the Users.
*/

class UserModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
   // login Imei
   function getdatawhere($table_name,$where){
    $this->db->order_by("id", "desc");
    $query = $this->db->get_where($table_name , $where);
    return $query;
}
    function login($credential) {
        // Check The Credentials
        $query = $this->db->get_where('users' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
             
            $this->db->select('users.id,users.device_token,users.is_device_registered,users.imei_number,users.user_code,users.first_name,users.email,users.emp_id,users.phone,users.role_id,users.branch_id,users.designation_id,users.department_id,users.is_active,users.parent_id,users.leave_policy_id');
            $this->db->from('users');
            $this->db->where('users.id', $row->id);
            $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }

    function loginTab($credential) {
        // Check The Credentials
        $query = $this->db->get_where('users' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
             
            $this->db->select('users.id,users.device_token,users.is_device_registered,users.imei_number,users.user_code,users.first_name,users.email,users.emp_id,users.phone,users.role_id,users.branch_id,users.designation_id,users.is_active,company_branches.is_active as branch_status');
            $this->db->from('users');
            $this->db->join('company_designations','company_designations.id = users.designation_id');
         
            $this->db->join('company_branches','company_branches.id = users.branch_id');
            $this->db->where('users.id', $row->id);
            $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
     function forgetPassword($credential) {
        // Check The Credentials
        $query = $this->db->get_where('users' , $credential);
        // If User Exists
        if ($query->num_rows() > 0) {

            $row = $query->row();
            
            $this->db->select('users.id,users.device_token,users.is_device_registered,users.imei_number,users.user_code,users.first_name,users.email,users.emp_id,users.phone,users.role_id,users.branch_id,users.designation_id,users.is_active,company_branches.is_active as branch_status');
            $this->db->from('users');
            $this->db->join('company_designations','company_designations.id = users.designation_id');
             $this->db->join('company_branches','company_branches.id = users.branch_id');
            $this->db->where('users.id', $row->id);
            $user = $this->db->get()->row();

            return $user;
        }
        else{
            return FALSE;
        }
    }
   
    
    public function userHasLeaveType($where)
    {
        $query = $this->db->get_where('user_leave_details',$where);
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
   
    public function getNumberOfLeaveByType($where)
    {
        $this->db->select('total_per_year');
        $this->db->from('leave_policy');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    public function insertUserLeaveType($data)
    {
        $this->db->insert('user_leave_details',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }

     function checkOldPasswordAvailability($where_password)
    {
          // Check The Credentials
        $query = $this->db->get_where('users' , $where_password);
        // If User Exists
        if ($query->num_rows() > 0) {


            return TRUE;
        }
        else{
            return FALSE;
        }
    
    }
   function checkDateAvailability($where_Date)
    {
        $query = $this->db->get_where('company_holidays',$where_Date);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }
    function checkfirst_nameAvailability($where_first_name)
    {
        $query = $this->db->get_where('users',$where_first_name);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }

    function checkEmailAvailability($where_email)
    {
        $query = $this->db->get_where('users' , $where_email);
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }

    function getUserPermission($first_name)
    {
        
$query = $this->db->query("select r_role.resource_id, r.resource_name, r.display_name as name, r_role.can_add, r_role.can_edit, r_role.can_view, r_role.can_delete from resource r 
inner join resource_role r_role on r.id=r_role.resource_id
inner join role ro on ro.id=r_role.role_id 
inner join users_role users_r on ro.id=users_r.role_id
inner join users u on u.id=users_r.user_id where u.first_name='".$first_name."'");
    $permissions = $query->result();
       // $permissions = $this->db->query("CALL get_user_permission()")->result();
        return $permissions;
    }

    function getUserPermissionByModule($role_id,$module)
    {
        $this->db->select('*');
        $this->db->from('resource_role');
        $this->db->join('resource', 'resource_role.resource_id = resource.id');
        $this->db->where('resource_role.role_id', $role_id);
        $this->db->where('resource.resource_name', $module);
        $permissions = $this->db->get()->result();
        return $permissions;
    }
    function getUserPermissionByModuleDesignation($designation,$module)
    {
        $this->db->select('*');
        $this->db->from('resource_role');
        $this->db->join('resource', 'resource_role.resource_id = resource.id');
        $this->db->where('resource_role.designation_id', $designation);
        $this->db->where('resource.resource_name', $module);
        $permissions = $this->db->get()->result();
        return $permissions;
    }
    public function insertData($table,$data)
    {
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
       
        if($insert_id != 0)
        {
            return $insert_id;
        }
        else    
        {
            return 0;
        }
    }
    public function editUserdetail($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_company_details',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }


    
    public function viewPolicywhere($where)
    {
         $this->db->select('*');
          $this->db->from('leave_policy');  
          $this->db->where($where);
          $query = $this->db->get();
          return $query->result();
    }


 public function viewTaskWhere($where,$where2)
 {
  $this->db->select('user_task.id,user_task.assign_by,user_task.task_name,user_task.priority,user_task.due_date,user_task.user_id,user_task.status,user_task.description,users.first_name,users.emp_id');
       $this->db->from('user_task'); 
       $this->db->join('users', 'users.id = user_task.user_id');  
       $this->db->where($where);
        $this->db->where($where2); 
       $query = $this->db->get();
       return $query->result();
 }
  public function viewTask($where2)
 {
    $this->db->select('user_task.id,user_task.assign_by,user_task.task_name,user_task.priority,user_task.due_date,user_task.user_id,user_task.status,user_task.description,users.first_name,users.emp_id');
       $this->db->from('user_task');
       $this->db->join('users', 'users.id = user_task.user_id');  
        $this->db->where($where2);  
       $query = $this->db->get();
       return $query->result();
 }
// Delete Task Id
   public function deleteTask($task_where)
    {
        $this->db->where($task_where);
        $this->db->delete('user_task');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function editTask($data,$where)
    {
        $this->db->where($where);
        $this->db->update('user_task',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    //editUserPolicy
    public function editUserPolicy($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // edit users
    public function editUser($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
    public function insertUserSession($data)
    {   
        //$lastLoggedData = date('d-m-Y h:i:s');
        $where=array('user_id'=>$data['user_id'],'device_token'=>$data['device_token']);
        $arrUserSessionData=$this->getUserSession($where);
       
        if(!empty($arrUserSessionData->id)){
          $lastLogin = date('d-m-Y h:i:s');
          $updateData['status']=1;
          $updateData['last_login']=$lastLogin;
          $updateWhere['id']=$arrUserSessionData->id;
         
          $success = $this->updateUserSession($updateData,$updateWhere);
          
        }
        else{
           $this->db->insert('user_session',$data);
           $insert_id = $this->db->insert_id();
          if($insert_id != 0)
          {
              return TRUE;
          }
          else    
          {
              return FALSE;
          }
        }    
    }

    public function insertLastSession($data)
    {   
        //$lastLoggedData = date('d-m-Y h:i:s');
        $where=array('user_id'=>$data['user_id']);
        $arrLastSessionData = $this->getUserLastLoginData($where);
        if(!empty($arrLastSessionData->id)){
          $lastLogin = date('d-m-Y h:i:s');
          $updateData['time']=$lastLogin;
          $updateWhere['id']=$arrLastSessionData->id;
         
          $success = $this->updateUserLastSession($updateData,$updateWhere);
          
        }
        else{
           $this->db->insert('last_loginsession',$data);
           $insert_id = $this->db->insert_id();
          if($insert_id != 0)
          {
              return TRUE;
          }
          else    
          {
              return FALSE;
          }
        }    
    }
    public function updateUserSession($data,$where)
    {
   
        $this->db->where($where);
        $this->db->update('user_session',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function updateUserLastSession($data,$where)
    {
   
        $this->db->where($where);
        $this->db->update('last_loginsession',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
     public function getUserSession($where)
    {
       
        $this->db->select('*');
        $this->db->from('user_session'); 
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->row();
       
        
    }
    public function getUserLastSession($where)
    {
       
        $this->db->select('*');
        $this->db->from('last_loginsession'); 
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->result();
       
        
    }
    public function getUserLastLoginData($where)
    {
       
        $this->db->select('*');
        $this->db->from('last_loginsession'); 
        $this->db->where($where); 
        $query = $this->db->get();
        return $query->row();
       
        
    }
    
     public function employeeAttendance()
    {
       
        $this->db->select('count(*) as project');
        $this->db->from('user_project');
        
        $query = $this->db->get();
        return $query->result();
    }
    // View Employee Growth 
    public function employeeGraph()
    {
    // tickets By Year
    $query = $this->db->query("SELECT YEAR(date) as ticketDate , Count(*) as ticketsCount FROM `user_tickets` GROUP BY YEAR(date) ORDER BY 1");
    $arrTicketsData = $query->result();
    $i=0;
    foreach($arrTicketsData as $key => $test)
    {
    if($i>0){
      $ticketsDate['ticket'] = $ticketsDate['ticket']." , ".$test->ticketDate;
     $ticketsDate['data2']= $ticketsDate['data2']." , ".$test->ticketsCount;
    }
    else{
    $ticketsDate['ticket'] = $test->ticketDate;
     $ticketsDate['data2']= $test->ticketsCount;
    }
     
     $arrTickets[0] = $ticketsDate;
     $i++;
    }
    //Attendance By Year
    $query = $this->db->query("SELECT YEAR(date) as attendanceDate , Count(*) as attendanceCount FROM `attendances` GROUP BY YEAR(date) ORDER BY 1");
    $arrAttendanceData = $query->result();
    $i=0;
    foreach($arrAttendanceData as $key => $value)
    {
    
    if($i>0){
     $attendanceData['attendance'] =$attendanceData['data1'].",". $value->attendanceDate;
     $attendanceData['data1'] =  $attendanceData['data1'].",".$value->attendanceCount;
    
     }
    else{
    
     $attendanceData['attendance'] = $value->attendanceDate;
     $attendanceData['data1'] = $value->attendanceCount;
    }
     $arrAttendance[0] = $attendanceData;
      $i++;
    } 
    //project by Year
    $query = $this->db->query("SELECT YEAR(start_date) as ProjectDate , Count(*) as ProjectCount FROM `user_project` GROUP BY YEAR(start_date) ORDER BY 1");
    $arrProjectsData = $query->result();
     $i=0;
    foreach($arrProjectsData as $key => $row)
    {
     if($i>0){
     $projectDate['project'] = $projectDate['project'].",".$row->ProjectDate;
     $projectDate['data3'] =$projectDate['data3'].",". $row->ProjectCount;
      }
    else{
     $projectDate['project'] = $row->ProjectDate;
     $projectDate['data3'] = $row->ProjectCount;
    }
     $arrProjects[0]= $projectDate;
     $i++;
    }
    $arrMerge = array_merge($arrTickets,$arrAttendance,$arrProjects);
    return $arrMerge;
    }
     public function employeeGrowth()
    {//projects
    $this->db->select('count(*) as project');
        $this->db->from('user_project');
        $query = $this->db->get();
        $test = $query->result();
        foreach($test as $key => $row)
        {
        $countdata['data1'] = $row->project;
         $counting[] = $countdata;
        }
        //tickets
        $this->db->select('count(*) as tickets');
        $this->db->from('user_tickets');
        $testing = $this->db->get();
        $aarData = $testing->result();
        foreach($aarData as $key => $value)
        {
        $countticket['data2'] = $value->tickets;
         $countingtickets[] = $countticket;
        }
         //attendance
          $this->db->select('count(*) as attendance');
        $this->db->from('attendance_mark');
        $attandancedata = $this->db->get();
        $aarDataAttendance = $attandancedata->result();
        foreach($aarDataAttendance as $key => $valueData)
        {
        $countattendance['data3'] = $valueData->attendance;
         $countingattendance[] = $countattendance;
        }
        //merge array
         $merge = array_merge($counting, $countingtickets ,$countingattendance);
        return $merge;  
    }
 // leave And Tickets Merge 
     public function viewLeaveticket($where)
    {
       
        $this->db->select('company_branches.branch_name,users.emp_id,users.first_name,user_leave.leave_type_id,user_leave.reason,user_leave.start_date,user_leave.end_date,user_tickets.ticket_type,user_tickets.date');
        $this->db->from('users');
     
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
       $this->db->join('user_leave', 'user_leave.user_id = users.id');
       $this->db->join('user_tickets', 'user_tickets.user_id = users.id');
       
       $this->db->where($where);
         $query = $this->db->get();
        return $query->result();
    }
     // leave And Tickets Merge 
     public function viewLeaveticketwhere($where,$where2)
    {
        $this->db->select('company_branches.branch_name,users.emp_id,users.first_name,user_leave.leave_type_id,user_leave.reason,user_leave.start_date,user_leave.end_date,user_tickets.ticket_type.user_tickets.date');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_leave', 'user_leave.user_id = users.id');
        $this->db->join('user_tickets', 'user_tickets.user_id = users.id');
    
       
       $this->db->where($where);
         $this->db->where($where2);
         $query = $this->db->get();
        return $query->result();
    }
    
    // leave And Tickets Merge 
     public function viewAllticket($where)
    {
       
        $this->db->select('company_branches.branch_name,users.emp_id,users.first_name,user_tickets.ticket_type,user_tickets.date,user_tickets.title,user_details.user_image');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
       $this->db->join('user_tickets', 'user_tickets.user_id = users.id');
       $this->db->join('user_details','user_details.user_id = users.id');
       
       $this->db->where($where);
         $query = $this->db->get();
        return $query->result();
    }
     // leave And Tickets Merge 
     public function viewAllTicketwhere($where,$where2)
    {
        $this->db->select('company_branches.branch_name,users.emp_id,users.first_name,user_tickets.ticket_type.user_tickets.date,user_tickets.title,user_details.user_image');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_tickets', 'user_tickets.user_id = users.user_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       $this->db->where($where);
         $this->db->where($where2);
         $query = $this->db->get();
        return $query->result();
    }
    
     
    public function viewUseremloyee()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('user_education','user_education.user_id = users.id');
         $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
         $this->db->join('user_personal','user_personal.user_id = users.id');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewUserEdit()
    {
        $this->db->select('users.id,users.first_name,users.emp_id,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,user_details.father_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email,users_bank_details.holder_name,users_bank_details.account_no,users_bank_details.bank_name,users_bank_details.branch,users_bank_details.salary,users_bank_details.ifsc,users_bank_details.pan_card,users_bank_details.aadhar_card,user_personal.passport_no,user_personal.passport_expiry,user_personal.nationality,user_personal.religion,user_personal.marital_status,
        user_personal.no_of_children,user_personal.contactperson_name,user_personal.contactperson_no,user_personal.relationship');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users.branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        $this->db->join('user_personal','user_personal.user_id = users.id');
        
        $query = $this->db->get();
        return $query->result();
    }

    public function viewUserEditWhere($where)
    {
        $this->db->select('users.id,users.first_name,users.emp_id,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,user_details.father_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email,users_bank_details.holder_name,users_bank_details.account_no,users_bank_details.bank_name,users_bank_details.branch,users_bank_details.salary,users_bank_details.ifsc,users_bank_details.pan_card,users_bank_details.aadhar_card,user_personal.passport_no,user_personal.passport_expiry,user_personal.nationality,user_personal.religion,user_personal.marital_status,
        user_personal.no_of_children,user_personal.contactperson_name,user_personal.contactperson_no,user_personal.relationship');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        $this->db->join('user_personal','user_personal.user_id = users.id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function viewUser($where2)
    {
        $this->db->select('users.id,users.first_name,users.emp_id,users.is_active,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,user_details.father_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        //$this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        //$this->db->join('user_personal','user_personal.user_id = users.id');
        $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewUserWhere($where,$where2)
    {
        $this->db->select('users.id,users.first_name,users.emp_id,company_departments.department_name,users.is_active,company_branches.branch_name,users.is_active,company_designations.designation_name,user_details.father_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        //$this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        //$this->db->join('user_personal','user_personal.user_id = users.id');
        $this->db->where($where);
        $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
    public function viewUser22()
    {
        $this->db->select('users.id,users.first_name,users.emp_id,users.is_active,company_departments.department_name,company_branches.branch_name,users.is_active,company_designations.designation_name,user_details.father_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        $this->db->join('user_personal','user_personal.user_id = users.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function viewUserWhere22($where)
    {
        $this->db->select('users.id,users.first_name,users.emp_id,company_departments.department_name,users.is_active,company_branches.branch_name,users.is_active,company_designations.designation_name,user_details.father_name,users.phone,user_details.personal_email,user_details.address,user_details.gender,user_details.dob,user_details.joining_date,user_details.user_image,users.user_code,users.designation_id
        ,users.branch_id,users.department_id,users.email');
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
        $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        $this->db->join('user_personal','user_personal.user_id = users.id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function checkUserStatus($user_id)
    {
        $this->db->select('users.is_active');
        $this->db->from('users');
        $this->db->where('users.id', $user_id);
        $user = $this->db->get()->row();

        if($user->is_active == 1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    // Check Upcoming Birthday 
    public function checkCelebrationBirthday()
    {
       // 
       $user = $this->api->_GetTokenData();
       $user_id = $user['id'];
       $query = $this->db->query("SELECT `dob`,user_id,user_image,first_name FROM user_details INNER JOIN `users` ON user_details.user_id = users.id WHERE dob !='' AND day (`dob`) = day(CURRENT_DATE()) AND dob !='' AND MONTH (`dob`) = MONTH(CURRENT_DATE()) AND user_id !='".$user_id."' ORDER BY DAY(`dob`)
       ");
    $arrTicketsData = $query->result();
    foreach ($arrTicketsData as $key => $value) {
        $data['first_name'] ="Happy Birthday To You"." ". $value->first_name;
        $data['dob'] = $value->dob;
        $result[] = $data;
         }
        return $result;
        
    }
    // Check Upcoming Birthday 
    public function checkUsersBirthday()
    {
       // 
       $user = $this->api->_GetTokenData();
       $user_id = $user['id'];
       $query = $this->db->query("SELECT `dob`,user_id,user_image,first_name FROM user_details INNER JOIN `users` ON user_details.user_id = users.id WHERE dob !='' AND day (`dob`) = day(CURRENT_DATE()) AND dob !='' AND MONTH (`dob`) = MONTH(CURRENT_DATE()) AND user_id !='".$user_id."' ORDER BY DAY(`dob`)

       ");
    $arrTicketsData = $query->result();
      
        return $arrTicketsData;
        
    }
    //view New Employee
   public function checkAllNewEmployee(){
   
 $date = new DateTime("now");
 
 $curr_date = $date->format('Y-m-d ');

 $this->db->select('count(*) as NewEmployee');
 $this->db->from('user_details'); 
 $this->db->where('joining_date', $curr_date);
        $query = $this->db->get();
        return $query->result();
   
    }
   
    
    // total present attandance 
    public function checkTotalAbsentWhere($where1,$where2)
    {
   
 $this->db->select('count(*) as absent');
 $this->db->from('attendances'); 
 //$this->db->where('date', $curr_date);
  $this->db->where('final_status', '0');
         $this->db->where($where1);
         $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
    
     // total present attandance 
    public function checkTotalAbsent($where1)
    {
 $this->db->select('count(*) as absent');
 $this->db->from('attendances'); 
 //$this->db->where('date', $curr_date);
  $this->db->where('final_status', '0');
         $this->db->where($where1);
        $query = $this->db->get();
        return $query->result();
    }
    
   
    //view Total Employee
    public function viewTotalEmployee($where,$where2)
    {
        
       $this->db->select('count(*) as id' );
       $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       // $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        //$this->db->join('user_personal','user_personal.user_id = users.id');
         $this->db->where($where);
         $this->db->where($where2);
        $query = $this->db->get();
        return $query->result();
    }
     public function viewEmployee($where)
    {
        
       $this->db->select('count(*) as id' );
       $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       // $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        //$this->db->join('user_personal','user_personal.user_id = users.id');
         $this->db->where($where);
      
        $query = $this->db->get();
        return $query->result();
    }
     public function viewTotalMaleEmployee($where,$where2)
    {
        $this->db->select('count(*) as total_male' );
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       // $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
       // $this->db->join('user_personal','user_personal.user_id = users.id');

         $this->db->where($where);
            $this->db->where($where2);
         $this->db->where('user_details.gender', 'male');
        $query = $this->db->get();
        return $query->result();
    }
 public function viewEmployeeMale($where)
    {
        
        $this->db->select('count(*) as total_male' );
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       //  $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
       // $this->db->join('user_personal','user_personal.user_id = users.id');
         $this->db->where($where);
         $this->db->where('user_details.gender', 'male');
        $query = $this->db->get();
        return $query->result();
    }
     public function viewTotalFemaleEmployee($where,$where2)
    {
        
        $this->db->select('count(*) as total_Female' );
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       // $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
       // $this->db->join('user_personal','user_personal.user_id = users.id');
         $this->db->where($where);
           $this->db->where($where2);
         $this->db->where('user_details.gender', 'female');
        $query = $this->db->get();
        return $query->result();
    }
     public function viewEmployeeFemale($where)
    {
        
        $this->db->select('count(*) as total_Female' );
        $this->db->from('users');
        $this->db->join('company_designations','company_designations.id = users.designation_id');
        $this->db->join('company_departments','company_departments.id = users.department_id');
        $this->db->join('company_branches','company_branches.id = users .branch_id');
        $this->db->join('user_details','user_details.user_id = users.id');
       // $this->db->join('users_bank_details','users_bank_details.user_id = users.id');
        //$this->db->join('user_personal','user_personal.user_id = users.id');
         $this->db->where($where);
         $this->db->where('user_details.gender', 'female');
        $query = $this->db->get();
        return $query->result();
    }
    public function getUserId($parent_of,$branch_id)
    {
       
        // SELECT users_role.user_id FROM users_role WHERE users_role.role_id IN (3,4)
        $user = $this->api->_GetTokenData();
        $roleId = $user['role_id'];
        $userId = $user['id'];
        if($roleId == 1)
        {
            $this->db->select('users.id');
            $this->db->from('users');
            $this->db->where("users.id NOT IN (".$userId.")");
            $result = $this->db->get()->result();
            return $result;
        }else{
          
            $this->db->select('users.id');
            $this->db->from('users');
            $this->db->where('users.designation_id IN ('.$parent_of.')');
            $sub_query = $this->db->get_compiled_select();
            
            $this->db->select('users.id');
            $this->db->from('users');

        if($branch_id != 0)
            $this->db->where("users.branch_id",$branch_id);

        $this->db->where("users.id IN (".$sub_query.")");
        $result = $this->db->get()->result();
        return $result;
      
        }
    }
    
    // Admin Change Branch
    public function editAdminBranch($data,$where)
    {
        $this->db->where($where);
        $this->db->update('users',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
}