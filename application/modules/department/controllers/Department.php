<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Department.php
	Date: November 12, 2019
	Info: This is the main class which is hold all the Functions of the Departments.
*/

class Department extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('departmentModel');		
	}
	
	public function _addDepartment($request)
    {		
		$department = array();
		$module_name = 'department';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Departments.. If User has the permission for that. */

			if(isset($request->department_name) AND isset($request->branch_id))
			{
       $status = $this->_checkDepartmentAvailablity($request);
				$department = array(
					'department_name' => $request->department_name,
					'branch_id' => $request->branch_id
				);
				$result = $this->departmentModel->addDepartment($department);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Department Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Department Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Department.");
		}
    }
// check Department Availability
public function _checkDepartmentAvailablity($request)
{
$module_name = 'department';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we check the Department Location. If User has the permission for that. */

			if(isset($request->department_name) AND isset($request->branch_id))
			{
				$where_department = array(
				'department_name' => $request->department_name,
					'branch_id' => $request->branch_id
				);

				$result = $this->departmentModel->DepartmentAvailablity($where_department);
				if($result)
					return array("message" => $request->department_name." is Available.");
				else
					$this->api->_sendError("Department Already Present.");
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Department.");
		}
}
    public function _deleteDepartment($request)
    {		
		$module_name = 'department';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete the Departments.. If User has the permission for that. */

			if(isset($request->department_id))
			{
				$where_department = array(
					'id' => $request->department_id,
				);
				$result = $this->departmentModel->deleteDepartment($where_department);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Department Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Department Not Deleted. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Department.");
		}
    }

    public function _editDepartment($request)
    {
		$department = array();
		$module_name = 'department';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Departments.. If User has the permission for that. */

			if(isset($request->department_id))
			{
            //  $status = $this->_checkDepartmentAvailablity($request);
				$department_where = array(
					'id' => $request->department_id
				);

				if(isset($request->department_name))
					$department['department_name'] = $request->department_name;
     
				if(isset($request->branch_id))
					$department['branch_id'] = $request->branch_id;

				$result = $this->departmentModel->editDepartment($department,$department_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Department Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Department Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Department.");
		}
    }

    public function _viewDepartment($request)
    {		
		$module_name = 'department';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$department_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Departments.. If User has the permission for that. */

				if(TRUE)
				{

				if(isset($request->department_id)){
					$department_where['company_departments.id'] = $request->department_id;
					$result = $this->departmentModel->viewDepartmentWhere($department_where);
					return $result; 
				}   

				else if($request->branch_id === 'all'){
					$result = $this->departmentModel->viewDepartment();
					return $result;                                                      
				}                   
				else if(isset($request->branch_id)){
					$department_where['company_departments.branch_id'] = $request->branch_id;
					$result = $this->departmentModel->viewDepartmentWhere($department_where);
					return $result;
				}
				else
				{
					$result = $this->departmentModel->viewDepartment();
					return $result;
				}
				}
				else
				{
				   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
				}
		}
	    	else
	     	{
	     		$this->api->_sendError("Sorry, You Have No Permission To View Department.");
	     	}
    }
    
    public function _viewDepartmentSelect($request)
    {	
			if(TRUE)
			{

				if(isset($request->department_id)){
				$department_where['company_departments.id'] = $request->department_id;
				$result = $this->departmentModel->viewDepartmentWhere($department_where);
				return $result; 
			}   
			else
			{
				$result = $this->departmentModel->viewDepartment();
				return $result;
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}	
	}
	// view Department for employee
	public function _viewDepartmentEmployee($request)
    {		
		
			/* Here we View the Departments.. If User has the permission for that. */

			if(TRUE)
			{

				$user = $this->api->_GetTokenData();
				$userbranch_id = $user['branch_id'];
				$where_branch = array('branch_id'=>$userbranch_id);
				$result = $this->departmentModel->viewDepartmentWhere($where_branch);
				return $result; 
			   

			
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
			
    }
}
