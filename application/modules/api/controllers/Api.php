<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/CreatorJwt.php';

#Project: Willy HRMS
#Author: Pratap singh
#File Name: Api.php
#Date: November 12, 2019
#Info: This is the main class which is hold all the Api of the system.

class Api extends MY_Controller {

// Constructor Function 
	public function __construct(){
		date_default_timezone_set('Asia/Kolkata');
		parent::__construct();
		$this->objOfJwt = new CreatorJwt();
        //$this->objOfSlack = new Slack();
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: POST");
		header("Access-Control-Allow-Headers: *");
		$request = array();
		$this->load->model('apiFunction');
    $this->load->model('userModel');
	}
// End of Constructor Function

//	Required API Functions For : Send Response, Error, Token, Check User Request

	/* -------------------------------------------------------------------------- */
	/*        Used To Send Response To User/Gateway Only Function With View       */
	/* -------------------------------------------------------------------------- */
	public function sendResponseAttendance($response)
	{
		try
		{
			//$data['response']['success'] = true;
			$data['response'] = $response;
			$this->load->view('api/result', $data);
		}
		catch (Exception $e)
		{
			$this->api->_sendError("Wrong Call to API..");
		}		
	}
	public function sendResponse($response)
	{
		try
		{
			$data['response']['success'] = true;
			$data['response']['data'] = $response;
			$this->load->view('api/result', $data);
		}
		catch (Exception $e)
		{
			$this->api->_sendError("Wrong Call to API..");
		}		
	}
	/* userd For Attendance only
	public function sendAttendanceResponse($response)
	{
		try
		{
			$data['response']['success'] = false;
			$data['response']['data'] = $response;
			$this->load->view('api/result', $data);
		}
		catch (Exception $e)
		{
			$this->api->_sendError("Wrong Call to API..");
		}		
	}
      userd For Attendance only*/
	/* -------------------------------------------------------------------------- */
	/*                         Used To Send Error Message                         */
	/* -------------------------------------------------------------------------- */

	public function _sendError($message)
	{		
		$data['response']['success'] = false;
		$data['response']['data'] = $message;
		$this->load->view('api/result', $data);
	}

	/* -------------------------------------------------------------------------- */
	/*                      Used To Generate The Login Token                      */
	/* -------------------------------------------------------------------------- */

	public function _loginToken($user)
    {
		$user->loginTime = date('d-m-Y h:i:s');
		$user->Token = $this->objOfJwt->GenerateToken($user);  // This is User JWT Token
  
          $useSession= [
                  'user_id'=>$user->id,
                  'auth_token'=>$user->Token,
                  'device_token'=>$user->device_token,
                  'status'=>1
                ];
   
    $this->userModel->insertUserSession($useSession);
		$this->sendResponse($user);
    }

	/* -------------------------------------------------------------------------- */
	/*                       Use To Decrypt The Login Token                       */
	/* -------------------------------------------------------------------------- */
	
	/* -------------------------------------------------------------------------- */
	/*                      Used To Forget password                     */
	/* -------------------------------------------------------------------------- */
		public function _forgetpass($user)
    {

		$this->sendResponse($user);
    }

    /* -------------------------------------------------------------------------- */
         /*                       Use To Get Token Data                      */
	/* -------------------------------------------------------------------------- */
    public function _GetTokenData()
    {
    	$received_Token = $this->input->request_headers('Authorization');
        try
        {
    			$jwtData = $this->objOfJwt->DecodeToken($received_Token['Token']);
    			
    			$timeFirst  = strtotime($jwtData['loginTime']);
    			$timeSecond = strtotime(date('d-m-Y h:i:s'));
    			$differenceInSeconds = $timeSecond - $timeFirst;
    			
					$user_id=$jwtData['id'];
					$device_token=$jwtData['device_token'];
					$where =array('status'=>0,'device_token'=>$device_token, 'user_id'=>$user_id);
					$arrUserSessionData= $this->userModel->getUserSession($where);
           /* -------------------------------------------------------------------------- */
              /*                       Use To Get Session Data                      */
	     /* -------------------------------------------------------------------------- */
          if(!empty($arrUserSessionData->id))
    			{
				$tokenExpire = 	array('message'=>'Unauthorized Token!');
				$this->_sendError($tokenExpire);
    			}
          /* -------------------------------------------------------------------------- */
              /*                       Use To Set Time In Token                     */
	     /* -------------------------------------------------------------------------- */
          if($differenceInSeconds > 7200)
    			{
					$lastLogin = date('d-m-Y h:i:s');
					$user_id=$jwtData['id'];
					$userlastSession=
					[
					'user_id'=>$user_id,
					'time'=>$lastLogin
					];

                    $responsetest = $this->userModel->insertLastSession($userlastSession);
    				$this->_sendError("Token Time Expired.");
    			}
			
            return $jwtData;
        }
        catch (Exception $e)
        {
            http_response_code('401');
            $this->api->_sendError("Token Not Verified.");
        }
	}
	
	/* -------------------------------------------------------------------------- */
	/*       Use To Check Request Need Token Or Not (Public Or Private API)       */
	/* -------------------------------------------------------------------------- */

	public function _checkAuthFreeRequest($action)
	{
		$authFreeArray = array(
			'maximumcarryDays','viewAttendanceOnUser','viewAttendanceUserCalandar','viewAttendanceCalandar','userLogin','userAppLogin','userLoginCompany','forgetPassword','viewJobPublic','addJobApplicant','viewprofilePublic','forgetPassword','contactForm','contacts_Form','dash_form'
		,'resetPassword','viewGenderPublic','viewReligionPublic','addVisitorsPerDay','viewUserName','viewBranchName','userTabLogin','viewMartialPublic','viewNationalityPublic','viewRelationnamePublic','faceComparision','viewAttendancePublic','viewLastLogin','viewJobEducation','viewAccuralForLeave','viewEnrollAttendance');
		if (in_array($action, $authFreeArray))
		{			
			return TRUE;
		}
		else
		{
			$received_Token = $this->input->request_headers('Authorization');
			if(isset($received_Token['Token']))
			{
				$userData = $this->_GetTokenData();			
				return TRUE;				 
			}
			else
			{
				$this->_sendError("No Auth Token Found.");
			}
		}
	}

// End Of Required API Functions	

/* -------------------------------------------------------------------------- */
 /*                       Use To Attendance Auth Free Data                      */
/* -------------------------------------------------------------------------- */
public function callApiAttendance($action = '')
{
	/*
		Get Request Here and Process the Switch-Case Statement
		And call the appropriate function
		otherwise call sendError Function with $message
	*/
	if($action == NULL)
	{
		$this->_sendError("Sorry You Entered In Wrong Room!!");
	}
	else
	{
		// Check Request Method Is POST or NOT
		
		$request = json_decode(file_get_contents("php://input"));
			if(!$request) 
			{
				//$request = json_encode($_POST);
				$request = json_decode($request);
			}
		
		
		$result = $this->_checkAuthFreeRequest($action);
		
				switch ($action) 
				{

						// 1. User API Case
						case 'viewAttendanceCalandar':
						$this->_viewAttendanceCalandar($request);
						break;
						case 'viewAttendanceUserCalandar':
						$this->_viewAttendanceUserCalandar($request);
						break;
						case 'viewAttendanceOnUser':
						$this->_viewAttendanceOnUser($request);
						break;
						case 'maximumcarryDays':
						$this->_maximumcarryDays($request);
						break;
							
								
						
					// 0. Default Case When Requested API Not Found             
					default:
					$this->_sendError("No Record Found Like -> '".$action."'.");
					break;
			    }
	}
}
	/* -------------------------------------------------------------------------- */
	/*     Main Function Which is Actually Called By The All User (App & Web)     */
	/* -------------------------------------------------------------------------- */

	public function callApi($action = '')
	{
		/*
			Get Request Here and Process the Switch-Case Statement
			And call the appropriate function
			otherwise call sendError Function with $message
		*/
		if($action == NULL)
		{
			$this->_sendError("Sorry You Entered In Wrong Room!!");
		}
		else
		{
			// Check Request Method Is POST or NOT
			if($_SERVER['REQUEST_METHOD'] == "POST") 
			{
			$request = json_decode(file_get_contents("php://input"));
				if(!$request) 
				{
					$request = json_encode($_POST);
					$request = json_decode($request);
				}
			}
			else
			{
				$this->_sendError("Access Wrong Server Method!!");
			}
			/* -------------------------------------------------------------------------- */
              /*                      Check Auth Free Api Function                    */
	       /* -------------------------------------------------------------------------- */
			$result = $this->_checkAuthFreeRequest($action);
			
					switch ($action) {

							// 1. User API Case
							case 'viewTokenExpired':
							$this->_viewTokenExpired($request);
							break;
							case 'verify_otp':
							$this->_verify_otp($request);
							break;
							case 'resend_otp':
							$this->_resend_otp($request);
							break;							
							case 'userLogin':
							$this->_userLogin($request);
							break;
							case 'userAppLogin':
							$this->_userAppLogin($request);
							break;
							case 'userLoginCompany':
							$this->_userLoginCompany($request);
							break;
							case 'userTabLogin':
							$this->_userTabLogin($request);
							break;
							case 'checkUpcomingBirthday':
							$this->_checkUpcomingBirthday($request);
							break;
							case 'checkUserBirthday':
							$this->_checkUserBirthday($request);
							break;
							case 'viewUserEdit':
							$this->_viewUserEdit($request);
							break;
							case 'logout':
							$this->_logout($request);
							break;
							case 'forgetPassword':
							$this->_forgetPassword($request);
							break;
							case 'resetPassword':
							$this->_resetPassword($request);
							break;
							case 'totalEmployee':
							$this->_totalEmployee($request);
							break;
							case 'changePassword':
							$this->_changePassword($request);
							break;
							case 'changePasswordEmployee':
							$this->_changePasswordEmployee($request);
							break;
							case 'totalMaleEmployee':
							$this->_totalMaleEmployee($request);
							break;
							case 'totalFemaleEmployee':
							$this->_totalFemaleEmployee($request);
							break;	
							case 'checkUsernameAvailability':
							$this->_checkUsernameAvailability($request);
							break;
							case 'checkEmailAvailability':
							$this->_checkEmailAvailability($request);
							break;
							case 'addUser':
							$this->_addUser($request);
							break;
							case 'addUserBulk':
							$this->_addUserBulk($request);
							break;
							case 'countAllNewEmployee':
							$this->_countAllNewEmployee($request);
							break;
							case 'deactivateUser':
							$this->_deactivateUser($request);
							break;
							case 'addParentUser':
							$this->_addParentUser($request);
							break;
							case 'viewOnUsercode':
							$this->_viewOnUsercode($request);
							break;
							case 'viewOnboardingUser':
							$this->_viewOnboardingUser($request);
							break;
							case 'viewOnboardingUserFilter':
							$this->_viewOnboardingUserFilter($request);
							break;
							case 'viewOnboardingComplete':
							$this->_viewOnboardingComplete($request);
							break;
							case 'viewOnboardingCompleteFilter':
							$this->_viewOnboardingCompleteFilter($request);
							break;
							
							case 'addEducationonboarding':
							$this->_addEducationonboarding($request);
							break;
							case 'addExperienceOnboarding':
							$this->_addExperienceOnboarding($request);
							break;
							
							case 'checkExperienceDetailsAvailability':
							$this->_checkExperienceDetailsAvailability($request);
							break;
							
							case 'checkBasicDetailsAvailability':
							$this->_checkBasicDetailsAvailability($request);
							break;
							case 'checkEducationDetailsAvailability':
							$this->_checkEducationDetailsAvailability($request);
							break;
							case 'checkBankDetailsAvailability':
							$this->_checkBankDetailsAvailability($request);
							break;
							
							case 'editStatusdoc':
							$this->_editStatusdoc($request);
							break;
							case 'viewUser':
							$this->_viewUser($request);
							break;
							case 'viewUserDashboard':
							$this->_viewUserDashboard($request);
							break;
							case 'viewUserEmployee':
							$this->_viewUserEmployee($request);
							break;
							
							case 'viewUserEmployeeDashboard':
							$this->_viewUserEmployeeDashboard($request);
							break;
							
							case 'GetTokenData':
							$this->_GetTokenData($request);
							break;

							case 'getCurrentUserPermission':
							$this->_getCurrentUserPermission($request);
							break;
							case 'allTicketsBranch':
							$this->_allTicketsBranch($request);
							break;
							case 'checkNewEmployeedata':
							$this->_checkNewEmployeedata($request);
							break;
							case 'taskAssign':
							$this->_taskAssign($request);
							break;
							case 'userViewTask':
							$this->_userViewTask($request);
							break;
							case 'viewTicketsPriority':
							$this->_viewTicketsPriority($request);
							break;
							
							case 'deleteTicketsType':
							$this->_deleteTicketsType($request);
							break;
							case 'editTicketsType':
							$this->_editTicketsType($request);
							break;
							case 'addTicketsType':
							$this->_addTicketsType($request);
							break;
							case 'viewTicketsEdit':
							$this->_viewTicketsEdit($request);
							break;
							case 'viewTicketsType':
							$this->_viewTicketsType($request);
							break;
							case 'deleteTask':
							$this->_deleteTask($request);
							break;
							case 'editTask':
							$this->_editTask($request);
							break;
							case 'countEmployemrntGrowth':
							$this->_countEmployemrntGrowth($request);
							break;
							case 'countEmployeeGraph':
							$this->_countEmployeeGraph($request);
							break;
							case 'totalPresentEmployee':
							$this->_totalPresentEmployee($request);
							break;
							case 'totalAbsentEmployee':
							$this->_totalAbsentEmployee($request);
							break;
							case 'totalAbsentGraph':
							$this->_totalAbsentGraph($request);
							break;
							case 'totalHalfLeaveShort':
							$this->_totalHalfLeaveShort($request);
							break;
                            
							case 'totalLeaveToday':
							$this->_totalLeaveToday($request);
							break;
							case 'viewTaskSelf':
							$this->_viewTaskSelf($request);
							break;
							case 'viewLastLogin':
							$this->_viewLastLogin($request);
							break;
							case 'changeBranch':
							$this->_changeBranch($request);
							break;
							/* -------------------------------------------------------------------------- */
                               /*                     2. Use Branch API Case                     */
	                       /* -------------------------------------------------------------------------- */
							case 'checkBranchAvailability':
							$this->_checkBranchAvailability($request);
							break;
							case 'addBranch':
							$this->_addBranch($request);
							break;
							case 'addBranchBulk':
							$this->_addBranchBulk($request);
							break;
							case 'userConversationclick':
							$this->_userConversationclick($request);
							break;
							case 'userConversation':
							$this->_userConversation($request);
							break;
							case 'deleteBranch':
							$this->_deleteBranch($request);
							break;
							case 'viewconversationUser':
							$this->_viewconversationUser($request);
							break;
							case 'viewconversationSingleUser':
							$this->_viewconversationSingleUser($request);
							break;
							case 'editBranch':
							$this->_editBranch($request);
							break;
							case 'editBranchActive':
							$this->_editBranchActive($request);
							break;
							case 'viewactiveBranch':
							$this->_viewactiveBranch($request);
							break;
							case 'viewBranch':
							$this->_viewBranch($request);
							break;
							case 'viewTokenBranch':
							$this->_viewTokenBranch($request);
							break;
							case 'viewJobBranch':
							$this->_viewJobBranch($request);
							break;
                          /* -------------------------------------------------------------------------- */
                               /*                   3.Use Department API Case                      */
	                      /* -------------------------------------------------------------------------- */
							case 'addDepartment':
							$this->_addDepartment($request);
							break;

							case 'deleteDepartment':
							$this->_deleteDepartment($request);
							break;

							case 'editDepartment':
							$this->_editDepartment($request);
							break;
							case 'checkDepartmentAvailablity':
							$this->_checkDepartmentAvailablity($request);
							break;
							case 'viewDepartment':
							$this->_viewDepartment($request);
							break;
							case 'viewDepartmentSelect':
							$this->_viewDepartmentSelect($request);
							break;
							case 'viewDepartmentEmployee':
							$this->_viewDepartmentEmployee($request);
							break;
							 /* -------------------------------------------------------------------------- */
                               /*                   4.Designation API Case                     */
	                        /* -------------------------------------------------------------------------- */
							case 'addDesignation':
							$this->_addDesignation($request);
							break;
							case 'deleteDesignation':
							$this->_deleteDesignation($request);
							break;
							case 'editDesignation':
							$this->_editDesignation($request);
							break;
							case 'viewDesignation':
							$this->_viewDesignation($request);
							break;
							case 'checkDesignationAvailablity':
							$this->_checkDesignationAvailablity($request);
							break;
							case 'viewDesignationSelect':
							$this->_viewDesignationSelect($request);
							break;
							case 'viewDesignationEmployee':
							$this->_viewDesignationEmployee($request);
							break;
							 /* -------------------------------------------------------------------------- */
                               /*                   4.Role API Case                 */
	                        /* -------------------------------------------------------------------------- */
							case 'addRole':
							$this->_addRole($request);
							break;
							case 'deleteRole':
							$this->_deleteRole($request);
							break;
							case 'editRole':
							$this->_editRole($request);
							break;
							case 'viewRole':
							$this->_viewRole($request);
							break;
							case 'roleAssign':
							$this->_roleAssign($request);
							break;
							case 'setModulePermission':
							$this->_setModulePermission($request);
							break;
							case 'getRolePermission':
							$this->_getRolePermission($request);
							break;
							case 'setParentRole':
							$this->_setParentRole($request);
							break;
							case 'getParentRole':
							$this->_getParentRole($request);
							break;
							case 'getUserId':
							$this->_getUserId($request);
							break;
							 /* -------------------------------------------------------------------------- */
                               /*                   5.Use To Leave Policy Api Case                */
	                        /* -------------------------------------------------------------------------- */
							case 'userViewPolicy':
							$this->_userViewPolicy($request);
							break;
							case 'assignLeavePolicy':
							$this->_assignLeavePolicy($request);
							break;
                             /* -------------------------------------------------------------------------- */
                               /*                   4.Use To Leave Api Case                */
	                        /* -------------------------------------------------------------------------- */
							case 'addLeave':
							$this->_addLeave($request);
							break;
							case 'leaveTotal':
							$this->_leaveTotal($request);
							break;
							case 'deleteLeave':
							$this->_deleteLeave($request);
							break;
							case 'editLeave':
							$this->_editLeave($request);
							break;
							case 'mergeleavetickets':
							$this->_mergeleavetickets($request);
							break;
							case 'viewLeave':
							$this->_viewLeave($request);
							break;
							case 'addLeaveComments':
							$this->_addLeaveComments($request);
							break;
							case 'viewLeaveComments':
							$this->_viewLeaveComments($request);
							break;
							case 'leaveReport':
							$this->_leaveReport($request);
							break;
							case 'viewLeave_Where':
							$this->_viewLeave_Where($request);
							break;
							case 'viewUserId':
							$this->_viewUserId($request);
							break;
							case 'viewUserName':
							$this->_viewUserName($request);
							break;
							case 'viewBranchName':
							$this->_viewBranchName($request);
							break;
                            /* -------------------------------------------------------------------------- */
                               /*                   7.Use To Leave Type API Case               */
	                        /* -------------------------------------------------------------------------- */
							case 'addLeaveType':
							$this->_addLeaveType($request);
							break;
							case 'deleteLeaveType':
							$this->_deleteLeaveType($request);
							break;
							case 'editLeaveType':
							$this->_editLeaveType($request);
							break;
							case 'viewLeaveType':
							$this->_viewLeaveType($request);
							break;
							case 'viewLeaveTypeSelected':
							$this->_viewLeaveTypeSelected($request);
							break;
							case 'viewLgggedUserPloicies':
							$this->_viewLgggedUserPloicies($request);
							break;
							case 'checkLeaveType_nameAvailability':
							$this->_checkLeaveType_nameAvailability($request);
							break;
							case 'addLeavePolicy':
							$this->_addLeavePolicy($request);
							break;
							case 'viewLeavePloicies':
							$this->_viewLeavePloicies($request);
							break;
							case 'deleteLeavePolicy':
							$this->_deleteLeavePolicy($request);
							break;
							case 'editLeavePolicy':
							$this->_editLeavePolicy($request);
							break;
							case 'viewaccrual_period':
							$this->_viewaccrual_period($request);
							break;
				            /* -------------------------------------------------------------------------- */
                               /*                   8.Use To Leave Management API Case               */
	                        /* -------------------------------------------------------------------------- */
							case 'viewBalanceLeave':
							$this->_viewBalanceLeave($request);
							break;
							case 'viewPaidLeave':
							$this->_viewPaidLeave($request);
							break;
							case 'viewAllLeave':
							$this->_viewAllLeave($request);
							break;
							case 'viewAllLeave_Where':
							$this->_viewAllLeave_Where($request);
							break;
							case 'leaveAction':
							$this->_leaveAction($request);
							break;
							case 'totalEmployeeLeave':
							$this->_totalEmployeeLeave($request);
							break;
							case 'fullLeaveReport':
							$this->_fullLeaveReport($request);
							break;
							case 'setUserLeave':
							$this->_setUserLeave($request);
							break;
							case 'viewApprovedLeave':
							$this->_viewApprovedLeave($request);
							break;
							/* -------------------------------------------------------------------------- */
                               /*                   9.Use To Holiday API Case               */
	                        /* -------------------------------------------------------------------------- */
							case 'checkDateAvailability':
							$this->_checkDateAvailability($request);
							break;
							case 'addHoliday':
							$this->_addHoliday($request);
							break;
							case 'addHolidayBulk':
							$this->_addHolidayBulk($request);
							break;
							case 'deleteHoliday':
							$this->_deleteHoliday($request);
							break;
							case 'editHoliday':
							$this->_editHoliday($request);
							break;
							case 'viewTotalHoliday':
							$this->_viewTotalHoliday($request);
							break;
							case 'viewHoliday':
							$this->_viewHoliday($request);
							break;
							case 'viewEditHoliday':
							$this->_viewEditHoliday($request);
							break;
							case 'viewHolidayBranch':
							$this->_viewHolidayBranch($request);
							break;
							/* -------------------------------------------------------------------------- */
                               /*                   10.Use To Yellow PageS API Case               */
	                        /* -------------------------------------------------------------------------- */
							case 'viewYellowPage':
							$this->_viewYellowPage($request);
							break;
							case 'viewYellowPageUsers':
							$this->_viewYellowPageUsers($request);
							break;
                            /* -------------------------------------------------------------------------- */
                               /*                   11.Use To Job Type API Case              */
	                        /* -------------------------------------------------------------------------- */
							case 'addJobType':
							$this->_addJobType($request);
							break;
							case 'contactForm':
							$this->_contactForm($request);
							break;
							case 'contacts_Form':
							$this->_contacts_Form($request);
							break;
							case 'dash_form':
							$this->_dash_form($request);
							break;		
							case 'deleteJobType':
							$this->_deleteJobType($request);
							break;
							case 'editJobType':
							$this->_editJobType($request);
							break;
							case 'viewJobEducation':
							$this->_viewJobEducation($request);
							break;
							case 'viewJobType':
							$this->_viewJobType($request);
							break;
							case 'editJobTypeActive':
							$this->_editJobTypeActive($request);
							break;
							case 'viewLastSevenApplication':
							$this->_viewLastSevenApplication($request);
							break;
							case 'viewactiveJobType':
							$this->_viewactiveJobType($request);
							break;
							case 'chechJobTYpeAvailablity':
							$this->_chechJobTYpeAvailablity($request);
							break;
                            /* -------------------------------------------------------------------------- */
                               /*                   12.Job Status API Case              */
	                        /* -------------------------------------------------------------------------- */
							case 'addJobStatus':
							$this->_addJobStatus($request);
							break;
							case 'deleteJobStatus':
							$this->_deleteJobStatus($request);
							break;
							case 'viewApplicatiionMonth':
							$this->_viewApplicatiionMonth($request);
							break;
							case 'viewChartApplication':
							$this->_viewChartApplication($request);
							break;
							case 'editJobStatus':
							$this->_editJobStatus($request);
							break;
							case 'totalOpenJobs':
							$this->_totalOpenJobs($request);
							break;
							case 'totalJobs':
							$this->_totalJobs($request);
							break;
							case 'viewJobStatus':
							$this->_viewJobStatus($request);
							break;
							case 'totalHoldJobs':
							$this->_totalHoldJobs($request);
							break;
							case 'totalCloseJobs':
							$this->_totalCloseJobs($request);
							break;
							case 'viewApprovedDateJob':
							$this->_viewApprovedDateJob($request);
							break;
							case 'viewTodayJob':
							$this->_viewTodayJob($request);
							break;
							case 'viewCurrentMonthJob':
							$this->_viewCurrentMonthJob($request);
							break;
							case 'totalLastMonthJobs':
							$this->_totalLastMonthJobs($request);
							break;
							/* -------------------------------------------------------------------------- */
                               /*                   15.Use To Job Applicant Status API Case             */
	                        /* -------------------------------------------------------------------------- */
							case 'addJobApplicantStatus':
							$this->_addJobApplicantStatus($request);
							break;
							case 'deleteJobApplicantStatus':
							$this->_deleteJobApplicantStatus($request);
							break;
							case 'editJobApplicantStatus':
							$this->_editJobApplicantStatus($request);
							break;
							case 'viewJobApplicantStatus':
							$this->_viewJobApplicantStatus($request);
							break;
							case 'chechJobStatusAvailablity':
							$this->_chechJobStatusAvailablity($request);
							break;
							case 'viewReferredData':
							$this->_viewReferredData($request);
							break;
							case 'viewAccuralForLeave':
							$this->_viewAccuralForLeave($request);
							break;
							/* -------------------------------------------------------------------------- */
                               /*                   16.Use To Jobs API Case             */
	                        /* -------------------------------------------------------------------------- */
							case 'addJob':
							$this->_addJob($request);
							break;
							case 'deleteJob':
							$this->_deleteJob($request);
							break;
							case 'editJob':
							$this->_editJob($request);
							break;
							case 'viewJob':
							$this->_viewJob($request);
							break;
							case 'viewJobPublic':
							$this->_viewJobPublic($request);
							break;
							case 'changeJobStatus':
							$this->_changeJobStatus($request);
							break;
							/* -------------------------------------------------------------------------- */
                               /*                   17.Use To Jobs Applicant API Case            */
	                        /* -------------------------------------------------------------------------- */
							case 'addJobApplicant':
							$this->_addJobApplicant($request);
							break;
							case 'changeStatusOfJobApplicant':
							$this->_changeStatusOfJobApplicant($request);
							break;
							case 'viewJobApplicant':
							$this->_viewJobApplicant($request);
							break;
							case 'downloadApplicantResume':
							$this->_downloadApplicantResume($request);
							break;
							case 'referJobApplicant':
							$this->_referJobApplicant($request);
							break;
                            /* -------------------------------------------------------------------------- */
                               /*                   18.Use To Jobs Interview API Case            */
	                        /* -------------------------------------------------------------------------- */
							case 'addJobInterview':
							$this->_addJobInterview($request);
							break;
							case 'changeJobInterviewStatus':
							$this->_changeJobInterviewStatus($request);
							break;
							case 'editJobInterview':
							$this->_editJobInterview($request);
							break;
							case 'viewJobInterview':
							$this->_viewJobInterview($request);
							break;
							/* -------------------------------------------------------------------------- */
                               /*                   19.Use To Events API Case            */
	                        /* -------------------------------------------------------------------------- */
							case 'allEvents':
							$this->_allEvents($request);
							break;
							case 'viewevetWhere':
							$this->_viewevetWhere($request);
							break;
							case 'addEvent':
							$this->_addEvent($request);
							break;
							case 'deleteEvent':
							$this->_deleteEvent($request);
							break;
							case 'editEvent':
							$this->_editEvent($request);
							break;
							case 'viewEvent':
							$this->_viewEvent($request);
							break;
							case 'viewTodayEvent':
							$this->_viewTodayEvent($request);
							break;
                            /* -------------------------------------------------------------------------- */
                               /*                   20. Use To Attendance API            */
	                        /* -------------------------------------------------------------------------- */
							case 'registerUserTab':
							$this->_registerUserTab($request);
							break;
							case 'userFaceEnrollTab':
							$this->_userFaceEnrollTab($request);
							break;
							case 'markMultipleAttendance':
							$this->_markMultipleAttendance($request);
							break;
							case 'markAttendance':
							$this->_markAttendance($request);
							break;
							case 'checkAttendance':
							$this->_checkAttendance($request);
							break;
							case 'checkImeiAvailability':
							$this->_checkImeiAvailability($request);
							break;
							case 'viewAttendance':
							$this->_viewAttendance($request);
							break;
							case 'viewAttendanceFilter':
							$this->_viewAttendanceFilter($request);
							break;
							case 'viewAttendanceCalandar':
							$this->_viewAttendanceCalandar($request);
							break;
							 /* -------------------------------------------------------------------------- */
                               /*                   21. Use To Attendance Management API Case            */
	                        /* -------------------------------------------------------------------------- */
							case 'viewAllAttendance':
							$this->_viewAllAttendance($request);
							break;
							case 'registerUser':
							$this->_registerUser($request);
							break;
							case 'registerNewDeveice':
							$this->_registerNewDeveice($request);
							break;
							case 'userFaceEnroll':
							$this->_userFaceEnroll($request);
							break;
							case 'faceComparision':
							$this->_faceComparision($request);
							break;
							case 'viewAttendancePublic':
							$this->_viewAttendancePublic($request);
							break;
							case 'updateProfileImage':
							$this->_updateProfileImage($request);
							break;
							case 'viewAttendanceTodayPresent':
							$this->_viewAttendanceTodayPresent($request);
							break;
							case 'viewAttendancePerDay':
							$this->_viewAttendancePerDay($request);
							break;
							case 'addVisitorsPerDay':
							$this->_addVisitorsPerDay($request);
							break;
							case 'viewVisitos':
							$this->_viewVisitos($request);
							break;
							case 'viewAttendanceSelfUser':
							$this->_viewAttendanceSelfUser($request);
							break;
							case 'viewAttendanceSelfUserFilter':
							$this->_viewAttendanceSelfUserFilter($request);
							break;
							case 'viewEnrollAttendance':
							$this->_viewEnrollAttendance($request);
							break;
							case'viewSelfAttendance':
							$this->_viewSelfAttendance($request);
							break;
                             /* -------------------------------------------------------------------------- */
                               /*                   22. Use To Profile Management API Case            */
	                        /* -------------------------------------------------------------------------- */
							case 'addNotfication':
							$this->_addNotfication($request);
							break;
							case 'viewNotification':
							$this->_viewNotification($request);
							break;
							case 'viewNotificationRaead':
							$this->_viewNotificationRaead($request);
							break;
							
							case 'viewTotalNotification':
							$this->_viewTotalNotification($request);
							break;
							case 'notificationRead':
							$this->_notificationRead($request);
							break;
							
							case 'viewProfile':
							$this->_viewProfile($request);
							break;
							case 'editProfile':
							$this->_editProfile($request);
							break;
							case 'editUsersDetails':
							$this->_editUsersDetails($request);
							break;
							case 'viewprofilePublic':
							$this->_viewprofilePublic($request);
							break;
							case 'viewProfileEmployee':
							$this->_viewProfileEmployee($request);
							break;
							
							case 'editPersonalDetails':
							$this->_editPersonalDetails($request);
							break;
							
							case 'onboardingPersonalDetails':
							$this->_onboardingPersonalDetails($request);
							break;
							case 'onboardingBankDetails':
							$this->_onboardingBankDetails($request);
							break;
							case 'editRelativeDetails':
							$this->_editRelativeDetails($request);
							break;
							case 'editEducationDetails':
							$this->_editEducationDetails($request);
							break;
							case 'addExperienceDetails':
							$this->_addExperienceDetails($request);
							break;
							case 'viewExperienceEmployee':
							$this->_viewExperienceEmployee($request);
							break;
							case 'editExperienceDetails':
							$this->_editExperienceDetails($request);
							break;

							case 'addEducationDetails':
							$this->_addEducationDetails($request);
							break;
							case 'editEducation':
							$this->_editEducation($request);
							break;
							case 'viewEducationbEmployee':
							$this->_viewEducationbEmployee($request);
							break;
							case 'deleteEducationData':
							$this->_deleteEducationData($request);
							break;
							case 'deleteExperienceData':
							$this->_deleteExperienceData($request);
							break;
							// 22. Reiumbersemnt Management API


							case 'addReimbursement':
							$this->_addReimbursement($request);
							break;

							case 'deleteReimbursementTravel':
							$this->_deleteReimbursementTravel($request);
							break;
							case 'editReimbursementTravel':
							$this->_editReimbursementTravel($request);
							break;
							
							case 'addReimbursementTravelPolicy':
							$this->_addReimbursementTravelPolicy($request);
							break;
							case 'addReimbursementhotelPolicy':
							$this->_addReimbursementhotelPolicy($request);
							break;
							case 'viewReimbursementHotel':
							$this->_viewReimbursementHotel($request);
							break;
							case 'viewReimbursementTravelPolicy':
							$this->_viewReimbursementTravelPolicy($request);
							break;
							
							case 'deleteReimbursement':
							$this->_deleteReimbursement($request);
							break;
							case 'editReimbursement':
							$this->_editReimbursement($request);
							break;
							case 'viewReimbursement':
							$this->_viewReimbursement($request);
							break;
							case 'editReimbursementStatus':
							$this->_editReimbursementStatus($request);
							break;
							case 'editchangeStatus':
							$this->_editchangeStatus($request);
							break;

							case 'viewReimbursementBranch':
							$this->_viewReimbursementBranch($request);
							break;
							case 'viewReimbursementeditBranch':
							$this->_viewReimbursementeditBranch($request);
							break;
							
							case 'viewReimbursementApproved':
							$this->_viewReimbursementApproved($request);
							break;
							case 'viewReimbursementDeclined':
							$this->_viewReimbursementDeclined($request);
							break;
							case 'viewTourReimbursement':
							$this->_viewTourReimbursement($request);
							break;
							case 'viewFoodReimbursement':
							$this->_viewFoodReimbursement($request);
							break;
							case 'viewOtherReimbursement':
							$this->_viewOtherReimbursement($request);
							break;
							case 'editFoodStatus':
							$this->_editFoodStatus($request);
							break;
							case 'editReimbursementTour':
							$this->_editReimbursementTour($request);
							break;
							case 'editReimbursementOther':
							$this->_editReimbursementOther($request);
							break;
							case 'editOtherStatus':
							$this->_editOtherStatus($request);
							break;
							case 'editTourStatus':
							$this->_editTourStatus($request);
							break;
							case 'viewTourEmployee':
							$this->_viewTourEmployee($request);
							break;
							case 'viewFoodEmployee':
							$this->_viewFoodEmployee($request);
							break;
							case 'viewOtherEmployee':
							$this->_viewOtherEmployee($request);
							break;
							case 'viewHotelEmployee':
							$this->_viewHotelEmployee($request);
							break;
							case 'editFood':
							$this->_editFood($request);
							break;
							//23 Payroll Management Api
							case 'viewPayroll':
							$this->_viewPayroll($request);
							break;
							case 'viewPayrollother':
							$this->_viewPayrollother($request);
							break;
							
							case 'viewpayrollPending':
							$this->_viewpayrollPending($request);
							break;
							case 'viewpayrollComplete':
							$this->_viewpayrollComplete($request);
							break;
							
							case 'addPayroll':
							$this->_addPayroll($request);
							break;
							case 'addPayrollCtc':
							$this->_addPayrollCtc($request);
							break;
							
							case 'deletePayroll':
							$this->_deletePayroll($request);
							break;
							case 'checkPayrollAvailability':
							$this->_checkPayrollAvailability($request);
							break;
							case 'checkPayrollNotAvailability':
							$this->_checkPayrollNotAvailability($request);
							break;
							case 'viewpayrollCreateSearch':
							$this->_viewpayrollCreateSearch($request);
							break;
							
							case 'editPayrollCtc':
							$this->_editPayrollCtc($request);
							break;
							
							case 'viewpayrolleditcase':
							$this->_viewpayrolleditcase($request);
							break;
							
							//Approve Leave  Api
							case 'approveLeave':
							$this->_approveLeave($request);
							break;
							// Resource Api
							case 'viewResources':
							$this->_viewResources($request);
							break;
							case 'addResources':
							$this->_addResources($request);
							break;
							case 'deleteResources':
							$this->_deleteResources($request);
							break;
							case 'viewAllResources':
							$this->_viewAllResources($request);
							break;
							case 'viewAllRoll':
							$this->_viewAllRoll($request);
							break;
							case 'editResource':
							$this->_editResource($request);
							break;
							//Onboarding Apis
							case 'addExperience';
							$this->_addExperience($request);
							break;
							case 'addOtherdocument';
							$this->_addOtherdocument($request);
							break;
							case 'addPersonalDocument';
							$this->_addPersonalDocument($request);
							break;
							case 'addOnboarding';
							$this->_addOnboarding($request);
							break;
							case 'deleteOther';
							$this->_deleteOther($request);
							break;
							case 'deleteExperience';
							$this->_deleteExperience($request);
							break;
							case 'deletePersonal';
							$this->_deletePersonal($request);
							break;
							case 'deleteOnboarding';
							$this->_deleteOnboarding($request);
							break;
							case 'viewOtherDocument';
							$this->_viewOtherDocument($request);
							break;
							case 'checkDocumentUploaded';
							$this->_checkDocumentUploaded($request);
							break;
							case 'checkOfficeDetails';
							$this->_checkOfficeDetails($request);
							break;
							case 'checkParentDetails';
							$this->_checkParentDetails($request);
							break;
							case 'viewOnboardingOffice';
							$this->_viewOnboardingOffice($request);
							break;
							case 'viewOnboardingBank';
							$this->_viewOnboardingBank($request);
							break;
							
							case 'checkLeavePolicyDetails';
							$this->_checkLeavePolicyDetails($request);
							break;
							
							case 'checkExperienceDetails';
							$this->_checkExperienceDetails($request);
							break;
							case 'checkBankDetails';
							$this->_checkBankDetails($request);
							break;
							
							case 'checkEducationDetails';
							$this->_checkEducationDetails($request);
							break;
							
							case 'checkPersonalDocument';
							$this->_checkPersonalDocument($request);
							break;
							case 'checkOtherDocument';
							$this->_checkOtherDocument($request);
							break;
							
							case 'checkExperienceDocument';
							$this->_checkExperienceDocument($request);
							break;
							
							case 'viewExperience';
							$this->_viewExperience($request);
							break;
							case 'viewPersonalDocument';
							$this->_viewPersonalDocument($request);
							break;
							case 'editOtherdocument';
							$this->_editOtherdocument($request);
							break;
							case 'editPersonaldocument';
							$this->_editPersonaldocument($request);
							break;
							case 'editExperience';
							$this->_editExperience($request);
							break;
							case 'editOnboarding';
							$this->_editOnboarding($request);
							break;
							case 'viewOnboarding';
							$this->_viewOnboarding($request);
							break;
							case 'viewLoggedinUser';
							$this->_viewLoggedinUser($request);
							break;

							case 'viewLoggedinPersonal';
							$this->_viewLoggedinPersonal($request);
							break;

							case 'viewLoggedinExperience';
							$this->_viewLoggedinExperience($request);
							break;
							case 'viewLoggedinOther';
							$this->_viewLoggedinOther($request);
							break;
							// Tickets API
							case 'addTickets';
							$this->_addTickets($request);
							break;
							case 'deleteTickets';
							$this->_deleteTickets($request);
							break;
							case 'viewTickets';
							$this->_viewTickets($request);
							break;
							case 'editTickets';
							$this->_editTickets($request);
							break;
							case 'editTicketsActive';
							$this->_editTicketsActive($request);
							break;
							case 'addTicketsComments';
							$this->_addTicketsComments($request);
							break;
							case 'viewTicketsComments';
							$this->_viewTicketsComments($request);
							break;
							
							case 'viewTicketsTotal';
							$this->_viewTicketsTotal($request);
							break;
							case 'viewTicketsleaveTotal';
							$this->_viewTicketsleaveTotal($request);
							break;
							case 'viewTotalLowPriority';
							$this->_viewTotalLowPriority($request);
							break;
							case 'viewTotalHighTicket';
							$this->_viewTotalHighTicket($request);
							break;
							case 'viewTicketsLoggedInuser';
							$this->_viewTicketsLoggedInuser($request);
							break;
							case 'viewTicketsTotalSelf':
							$this->_viewTicketsTotalSelf($request);
							break;
							case 'viewTicketsleaveSelf':
							$this->_viewTicketsleaveSelf($request);
							break;
							case 'viewTicketsPayrollSelf':
							$this->_viewTicketsPayrollSelf($request);
							break;
							case 'viewTotalmediumTicket':
							$this->_viewTotalmediumTicket($request);
							break;
							case 'viewTicketsSelf':
							$this->_viewTicketsSelf($request);
							break;
							// view some apis Data like gender martial status 
							case 'viewGenderPublic';
							$this->_viewGenderPublic($request);
							break;
							case 'viewReligionPublic';
							$this->_viewReligionPublic($request);
							break;
							case 'viewMartialPublic';
							$this->_viewMartialPublic($request);
							break;
							case 'viewNationalityPublic';
							$this->_viewNationalityPublic($request);
							break;
							case 'viewRelationnamePublic';
							$this->_viewRelationnamePublic($request);
							break;
							// Employee Data
							case 'viewEmployee';
							$this->_viewEmployee($request);
							break;
							case 'editDetails':
							$this->_editDetails($request);
							break;
							case 'editDetailsprofile':
							$this->_editDetailsprofile($request);
							break;
							
							case 'editPersonal':
							$this->_editPersonal($request);
							break;
							case 'editeEmployeeDetails':
							$this->_editeEmployeeDetails($request);
							break;
							case 'editBank':
							$this->_editBank($request);
							break;
							// Projects
							case 'viewProject':
							$this->_viewProject($request);
							break;
							case 'addProject':
							$this->_addProject($request);
							break;
							case 'deleteProject':
							$this->_deleteProject($request);
							break;
							case 'editProject':
							$this->_editProject($request);
							break;
							case 'totalProject':
							$this->_totalProject($request);
							break;
							case 'changeProjectStatus':
							$this->_changeProjectStatus($request);
							break;
							case 'totalOngoingProject':
							$this->_totalOngoingProject($request);
							break;
							case 'totalPendingProject':
							$this->_totalPendingProject($request);
							break;
							case 'viewProjectSelf':
							$this->_viewProjectSelf($request);
							break;
							case 'viewProjectfilter':
							$this->_viewProjectfilter($request);
							break;
							case 'totalProjectSelf':
							$this->_totalProjectSelf($request);
							break;
							case 'totalPendingProjectSelf':
							$this->_totalPendingProjectSelf($request);
							break;
							case 'totalCompleteProjectSelf':
							$this->_totalCompleteProjectSelf($request);
							break;
							
							case 'totalCompleteProject':
							$this->_totalCompleteProject($request);
							break;
							
							case 'totalOngoingProjectSelf':
							$this->_totalOngoingProjectSelf($request);
							break;
							// offboarding Api Routers
							case 'addOffboarding':
							$this->_addOffboarding($request);
							break;
							case 'viewOffboarding':
							$this->_viewOffboarding($request);
							break;
							case 'deleteOffboarding':
							$this->_deleteOffboarding($request);
							break;
							case 'SelfOfboarding':
							$this->_SelfOfboarding($request);
							break;
							case 'viewOffboardingUser':
							$this->_viewOffboardingUser($request);
							break;
							case 'viewOffboardingUserCompleted':
							$this->_viewOffboardingUserCompleted($request);
							break;
							case 'viewOffboardingCompleteFilter':
							$this->_viewOffboardingCompleteFilter($request);
							break;
							
							// Asstes 
							
							case 'viewNoneAssetsIssued';
							$this->_viewNoneAssetsIssued($request);
							break;
							case 'viewAssetsGraph';
							$this->_viewAssetsGraph($request);
							break;
							
							case 'viewAssetsIssued';
							$this->_viewAssetsIssued($request);
							break;
							case 'viewAssetsIssuedid';
							$this->_viewAssetsIssuedid($request);
							break;
							case 'addAssetsCategory';
							$this->_addAssetsCategory($request);
							break;
							case 'viewAssetsCategory';
							$this->_viewAssetsCategory($request);
							break;
							case 'editAssetsCategoryStatus';
							$this->_editAssetsCategoryStatus($request);
							break;
							case 'deletAssetsCategory';
							$this->_deletAssetsCategory($request);
							break;
							case 'editAssetsCategory';
							$this->_editAssetsCategory($request);
							break;
							case 'checkAssetsCategoryAvailability';
							$this->_checkAssetsCategoryAvailability($request);
							break;
							case 'viewAssetsCategoryActive';
							$this->_viewAssetsCategoryActive($request);
							break;
							// assets Comapny
							case 'checkAssetsSerialNoAvailability';
							$this->_checkAssetsSerialNoAvailability($request);
							break;
							case 'checkAssetsBillNoAvailability';
							$this->_checkAssetsBillNoAvailability($request);
							break;
							case 'checkAssetsidAvailability';
							$this->_checkAssetsidAvailability($request);
							break;
							
							case 'addAssetsCompany';
							$this->_addAssetsCompany($request);
							break;
							case 'checkCompanyEmailAvailability';
							$this->_checkCompanyEmailAvailability($request);
							break;
							
							case 'viewAssetsCompany';
							$this->_viewAssetsCompany($request);
							break;
							case 'deleteCompanyAssets';
							$this->_deleteCompanyAssets($request);
							break;
							case 'editAssetsCompany';
							$this->_editAssetsCompany($request);
							break;
							// Assets For User
							case 'addAssetsUser';
							$this->_addAssetsUser($request);
							break;
							
							case 'viewAssetsUserTotal';
							$this->_viewAssetsUserTotal($request);
							break;
							case 'viewAssetsUser';
							$this->_viewAssetsUser($request);
							break;
							case 'viewAsseteditsUser';
							$this->_viewAsseteditsUser($request);
							break;
							case 'viewResendActivity';
							$this->_viewResendActivity($request);
							break;
							
							case 'viewNoneIssueAsseteditsUser';
							$this->_viewNoneIssueAsseteditsUser($request);
							break;
							case 'editAssetsUserStatus';
							$this->_editAssetsUserStatus($request);
							break;
							break;
							case 'editAssetsUser';
							$this->_editAssetsUser($request);
							break;
							case 'deleteUserAssets';
							$this->_deleteUserAssets($request);
							break;
							case 'viewAssetsCompanySelect';
							$this->_viewAssetsCompanySelect($request);
							break;
							// Assets For Self
							case 'viewAssetsSelf';
							$this->_viewAssetsSelf($request);
							break;
							// company	
							case 'viewCountries';
							$this->_viewCountries($request);
							break;
							case 'viewStates';
							$this->_viewStates($request);
							break;
							case 'viewCities';
							$this->_viewCities($request);
							break;
							
							case 'viewCompany';
							$this->_viewCompany($request);
							break;
							case 'editCompany';
							$this->_editCompany($request);
							break;
							case 'deleteCompany';
							$this->_deleteCompany($request);
							break;
							case 'addCompany';
							$this->_addCompany($request);
							break;
							// company Modules
							case 'viewsModules';
							$this->_viewsModules($request);
							break;
							case 'editModulestatus';
							$this->_editModulestatus($request);
							break;
							case 'viewPolicies';
							$this->_viewPolicies($request);
							break;
							case 'editPolicyStatus';
							$this->_editPolicyStatus($request);
							break;
							case 'editActivityStatus';
							$this->_editActivityStatus($request);
							break;

							case 'viewActivity';
							$this->_viewActivity($request);
							break;
							
							
					// 0. Default Case When Requested API Not Found				
					default:
					$this->_sendError("No Record Found Like -> '".$action."'.");
					break;
	        }
	 }
}
//-------------------End Main Function-----------------------

// Other Functions

	/* -------------------------------------------------------------------------- */
	/*                    Some Function Which is Used in System                   */
	/* -------------------------------------------------------------------------- */

	public function _dateDiffInDays($date1,$date2)  
	{ 
		// Calulating the difference in timestamps 
		$diff = strtotime($date2) - strtotime($date1);
		// 1 day = 24 hours 
		// 24 * 60 * 60 = 86400 seconds 
		return abs(round($diff / 86400)); 
	}

	function _getRandomString($length_of_string)
	{
		return substr(sha1(time()), 0, $length_of_string); 
	}	

//End Other Function

	/* -------------------------------------------------------------------------- */
	/*                      All API Function Start From Here                      */
	/* -------------------------------------------------------------------------- */

// 1. Users API
   public function _logout($request)
	{			
    $user = $this->_GetTokenData();
    $user_id=$user['id'];
    $device_token=$user['device_token'];
    $wherewCond=array('user_id'=>$user_id,'device_token'=>$device_token);
    
    $data=array('status'=>0);
		$response=$this->userModel->updateUserSession($data,$wherewCond);
		$lastLogin = date('d-m-Y h:i:s');
		$userlastSession= [
			'user_id'=>$user_id,
			'time'=>$lastLogin
		  ];

       $responsetest = $this->userModel->insertLastSession($userlastSession);
		$this->sendResponse($response);
	}
	
	public function _userAppLogin($request)
    {
        $user = $this->users->_userAppLogin($request);
		$this->_loginToken($user);
	}
	public function _userLoginCompany($request)
    {
        $user = $this->users->_userLoginCompany($request);
		$this->sendResponse($user);
	}
	
	public function _userTabLogin($request)
    {
        $user = $this->users->_userTabLogin($request);
		$this->_loginToken($user);
	}
	
	public function _userLogin($request)
    {
        $user = $this->users->_userLogin($request);
		$this->_loginToken($user);
    }
    public function _forgetPassword($request)
    {
        $user = $this->users->_forgetPassword($request);
			$this->_forgetpass($user);
	}
	
	public function _changePasswordEmployee($request)
	{
		$response = $this->users->_changePasswordEmployee($request);
		$this->sendResponse($response);
	}
    	public function _changePassword($request)
	{
		$response = $this->users->_changePassword($request);
		$this->sendResponse($response);
	}
    public function _resetPassword($request)
    {
        $response = $this->users->_resetPassword($request);
		$this->sendResponse($response);
    }
    public function _totalFemaleEmployee($request)
    {
        $response = $this->users->_totalFemaleEmployee($request);
		$this->sendResponse($response);
    }
     public function _totalEmployee($request)
    {
        $response = $this->users->_totalEmployee($request);
		$this->sendResponse($response);
    }
     public function _totalMaleEmployee($request)
    {
        $response = $this->users->_totalMaleEmployee($request);
		$this->sendResponse($response);
    }
	public function _getCurrentUserPermission($request)
	{
		$response = $this->users->_getCurrentUserPermission($request);
		$this->sendResponse($response);
	}

	public function _checkUsernameAvailability($request)
	{
		$response = $this->users->_checkUsernameAvailability($request);
		$this->sendResponse($response);
	}
	public function _viewUserEdit($request)
	{
		$response = $this->users->_viewUserEdit($request);
		$this->sendResponse($response);
	}
	
	public function _changeBranch($request)
	{
		$response = $this->users->_changeBranch($request);
		$this->sendResponse($response);
	}
    public function _countAllNewEmployee($request)
	{
		$response = $this->users->_countAllNewEmployee($request);
		$this->sendResponse($response);
	}
	public function _allTicketsBranch($request)
	{
		$response = $this->users->_allTicketsBranch($request);
		$this->sendResponse($response);
	}
 	public function _checkEmailAvailability($request)
	{
		$response = $this->users->_checkEmailAvailability($request);
		$this->sendResponse($response);
	}
	
	public function _checkUserBirthday($request)
	{
		$response = $this->users->_checkUserBirthday($request);
		$this->sendResponse($response);
	}
	public function _checkUpcomingBirthday($request)
	{
		$response = $this->users->_checkUpcomingBirthday($request);
		$this->sendResponse($response);
	}
	public function _addUser($request)
	{			
		$response = $this->attendance->_addUser($request);
		$this->sendResponse($response);
	}
	public function _addUserBulk($request)
	{			
		$response = $this->users->_addUserBulk($request);
		$this->sendResponse($response);
	}
	
	public function _deactivateUser($request)
	{			
		$response = $this->users->_deactivateUser($request);
		$this->sendResponse($response);
	}
	
	public function _addParentUser($request)
	{			
		$response = $this->onboarding->_addParentUser($request);
		$this->sendResponse($response);
	}
	
	public function _viewOnUsercode($request)
	{			
		$response = $this->onboarding->_viewOnUsercode($request);
		$this->sendResponse($response);
	}
	
	public function _viewOnboardingUserFilter($request)
	{			
		$response = $this->onboarding->_viewOnboardingUserFilter($request);
		$this->sendResponse($response);
	}
	public function _viewOnboardingUser($request)
	{			
		$response = $this->onboarding->_viewOnboardingUser($request);
		$this->sendResponse($response);
	}
	public function _viewOnboardingComplete($request)
	{			
		$response = $this->onboarding->_viewOnboardingComplete($request);
		$this->sendResponse($response);
	}
	public function _viewOnboardingCompleteFilter($request)
	{			
		$response = $this->onboarding->_viewOnboardingCompleteFilter($request);
		$this->sendResponse($response);
	}
	
	public function _addEducationonboarding($request)
	{			
		$response = $this->onboarding->_addEducationonboarding($request);
		$this->sendResponse($response);
	}
	public function _addExperienceOnboarding($request)
	{			
		$response = $this->onboarding->_addExperienceOnboarding($request);
		$this->sendResponse($response);
	}
	
	public function _checkExperienceDetailsAvailability($request)
	{			
		$response = $this->onboarding->_checkExperienceDetailsAvailability($request);
		$this->sendResponse($response);
	}
	
	public function _checkBasicDetailsAvailability($request)
	{			
		$response = $this->onboarding->_checkBasicDetailsAvailability($request);
		$this->sendResponse($response);
	}
	public function _checkEducationDetailsAvailability($request)
	{			
		$response = $this->onboarding->_checkEducationDetailsAvailability($request);
		$this->sendResponse($response);
	}
	public function _checkBankDetailsAvailability($request)
	{			
		$response = $this->onboarding->_checkBankDetailsAvailability($request);
		$this->sendResponse($response);
	}
	
	public function _editStatusdoc($request)
	{			
		$response = $this->onboarding->_editStatusdoc($request);
		$this->sendResponse($response);
	}
	public function _viewUserEmployeeDashboard($request)
	{
		$response = $this->users->_viewUserEmployeeDashboard($request);
		$this->sendResponse($response);
	}
	public function _viewUserDashboard($request)
	{
		$response = $this->users->_viewUserDashboard($request);
		$this->sendResponse($response);
	}
	public function _viewUserEmployee($request)
	{
		$response = $this->users->_viewUserEmployee($request);
		$this->sendResponse($response);
	}
	
	public function _viewUser($request)
	{
		$response = $this->users->_viewUser($request);
		$this->sendResponse($response);
	}

	public function _getUserId($request)
	{
		$response = $this->users->_getUserId($request);
		$this->sendResponse($response);
	}
 	public function _checkNewEmployeedata($request)
	{
		$response = $this->users->_checkNewEmployeedata($request);
		$this->sendResponse($response);
	}

   	public function _taskAssign($request)
	{
		$response = $this->users->_taskAssign($request);
		$this->sendResponse($response);
	}
 	public function _userViewTask($request)
	{
		$response = $this->users->_userViewTask($request);
		$this->sendResponse($response);
	}
    public function _deleteTask($request)
	{
		$response = $this->users->_deleteTask($request);
		$this->sendResponse($response);
	}
    public function _editTask($request)
	{
		$response = $this->users->_editTask($request);
		$this->sendResponse($response);
	}
   public function _countEmployemrntGrowth($request)
	{
		$response = $this->users->_countEmployemrntGrowth($request);
		$this->sendResponse($response);
	}
    public function _countEmployeeGraph($request)
	{
		$response = $this->users->_countEmployeeGraph($request);
		$this->sendResponse($response);
	}
 
    public function _totalPresentEmployee($request)
	{
		$response = $this->leave->_totalPresentEmployee($request);
		$this->sendResponse($response);
	}
	public function _totalAbsentEmployee($request)
	{
		$response = $this->leave->_totalAbsentEmployee($request);
		$this->sendResponse($response);
	}
	public function _totalAbsentGraph($request)
	{
		$response = $this->leave->_totalAbsentGraph($request);
		$this->sendResponse($response);
	}
	public function _totalHalfLeaveShort($request)
	{
		$response = $this->leave->_totalHalfLeaveShort($request);
		$this->sendResponse($response);
	}
	
    public function _totalLeaveToday($request)
	{
		$response = $this->leave->_totalLeaveToday($request);
		$this->sendResponse($response);
	}
    public function _tokenId($request)
	{
		$response = $this->users->_tokenId($request);
		$this->sendResponse($response);
	}
	public function _viewTaskSelf($request)
	{
		$response = $this->users->_viewTaskSelf($request);
		$this->sendResponse($response);
	}
	public function _viewLastLogin($request)
	{
		$response = $this->users->_viewLastLogin($request);
		$this->sendResponse($response);
	}
// 2. Branch API

	public function _addBranch($request)
	{			
		$response = $this->branch->_addBranch($request);
		$this->sendResponse($response);
	}
	public function _addBranchBulk($request)
	{			
		$response = $this->branch->_addBranchBulk($request);
		$this->sendResponse($response);
	}
	
	public function _userConversationclick($request)
	{			
		$response = $this->branch->_userConversationclick($request);
		$this->sendResponse($response);
	}
	public function _userConversation($request)
	{			
		$response = $this->branch->_userConversation($request);
		$this->sendResponse($response);
	}
	public function _deleteBranch($request)
	{			
		$response = $this->branch->_deleteBranch($request);
		$this->sendResponse($response);
	}
	public function _viewconversationUser($request)
	{			
		$response = $this->branch->_viewconversationUser($request);
		$this->sendResponse($response);
	}
	public function _viewconversationSingleUser($request)
	{			
		$response = $this->branch->_viewconversationSingleUser($request);
		$this->sendResponse($response);
	}
	
	public function _viewTokenExpired($request)
	{			
		$response = $this->branch->_viewTokenExpired($request);
		$this->sendResponse($response);
	}
	public function _editBranchActive($request)
	{			
		$response = $this->branch->_editBranchActive($request);
		$this->sendResponse($response);
	}
	
	public function _editBranch($request)
	{			
		$response = $this->branch->_editBranch($request);
		$this->sendResponse($response);
	}
   public function _viewJobBranch($request)
	{			
		$response = $this->branch->_viewJobBranch($request);
		$this->sendResponse($response);
	}

	public function _viewactiveBranch($request)
	{			
		$response = $this->branch->_viewactiveBranch($request);
		$this->sendResponse($response);
	}
 	public function _checkBranchAvailability($request)
	{			
		$response = $this->branch->_checkBranchAvailability($request);
		$this->sendResponse($response);
	}
	public function _viewBranch($request)
	{			
		$response = $this->branch->_viewBranch($request);
		$this->sendResponse($response);
	}
	public function _viewTokenBranch($request)
	{			
		$response = $this->branch->_viewTokenBranch($request);
		$this->sendResponse($response);
	}
	
// 3. Department API

	public function _addDepartment($request)
	{			
		$response = $this->department->_addDepartment($request);
		$this->sendResponse($response);
	}

	public function _deleteDepartment($request)
	{			
		$response = $this->department->_deleteDepartment($request);
		$this->sendResponse($response);
	}

	public function _editDepartment($request)
	{			
		$response = $this->department->_editDepartment($request);
		$this->sendResponse($response);
	}

	public function _checkDepartmentAvailablity($request)
	{			
		$response = $this->department->_checkDepartmentAvailablity($request);
		$this->sendResponse($response);
	}

	public function _viewDepartment($request)
	{			
		$response = $this->department->_viewDepartment($request);
		$this->sendResponse($response);
	}
    public function _viewDepartmentSelect($request){ 	
		$response = $this->department->_viewDepartmentSelect($request);
		$this->sendResponse($response);
	}   
	public function _viewDepartmentEmployee($request){
		$response = $this->department->_viewDepartmentEmployee($request);
		$this->sendResponse($response);
	} 
	                                                                                                                                                                                                                                                                                     
// 4. Designation API                                                                                                                                                                                                                                                                                              

	public function _addDesignation($request)
	{			
		$response = $this->designation->_addDesignation($request);
		$this->sendResponse($response);
	}

	public function _deleteDesignation($request)
	{			
		$response = $this->designation->_deleteDesignation($request);
		$this->sendResponse($response);
	}

	public function _editDesignation($request)
	{			
		$response = $this->designation->_editDesignation($request);
		$this->sendResponse($response);
	}

	public function _viewDesignation($request)
	{			
		$response = $this->designation->_viewDesignation($request);
		$this->sendResponse($response);
	}

public function _checkDesignationAvailablity($request)
	{			
		$response = $this->designation->_checkDesignationAvailablity($request);
		$this->sendResponse($response);
	}
 public function _viewDesignationSelect($request)
 {
        $response = $this->designation->_viewDesignationSelect($request);
		$this->sendResponse($response);
 }
 public function _viewDesignationEmployee($request)
 {
        $response = $this->designation->_viewDesignationEmployee($request);
		$this->sendResponse($response);
 }
 
// 5. Roles API 

	public function _addRole($request)
	{			
		$response = $this->role->_addRole($request);
		$this->sendResponse($response);
	}

	public function _deleteRole($request)
	{			
		$response = $this->role->_deleteRole($request);
		$this->sendResponse($response);
	}

	public function _editRole($request)
	{			
		$response = $this->role->_editRole($request);
		$this->sendResponse($response);
	}

	public function _viewRole($request)
	{			
		$response = $this->role->_viewRole($request);
		$this->sendResponse($response);
	}

	public function _roleAssign($request)
	{			
		$response = $this->role->_roleAssign($request);
		$this->sendResponse($response);
	}

	public function _setModulePermission($request)
	{			
		$response = $this->role->_setModulePermission($request);
		$this->sendResponse($response);
	}

	public function _getRolePermission($request)
	{			
		$response = $this->role->_getRolePermission($request);
		$this->sendResponse($response);
	}

	public function _setParentRole($request)
	{			
		$response = $this->role->_setParentRole($request);
		$this->sendResponse($response);
	}

	public function _getParentRole($request)
	{			
		$response = $this->role->_getParentRole($request);
		$this->sendResponse($response);
	}

// 6. Leave API
   public function _mergeleavetickets($request)
	{
		$response = $this->users->_mergeleavetickets($request);
		$this->sendResponse($response);
	}
    public function _viewApprovedLeave($request)
	{
		$response = $this->leave->_viewApprovedLeave($request);
		$this->sendResponse($response);
	}
	public function _viewLeave_Where($request)
	{
		$response = $this->leave->_viewLeave_Where($request);
		$this->sendResponse($response);
	}
	public function _addLeave($request)
	{
		$response = $this->leave->_addLeave($request);
		$this->sendResponse($response);
	}
    public function _leaveTotal($request)
	{
		$response = $this->leave->_leaveTotal($request);
		$this->sendResponse($response);
	}
	public function _deleteLeave($request)
	{			
		$response = $this->leave->_deleteLeave($request);
		$this->sendResponse($response);
	}

	public function _editLeave($request)
	{			
		$response = $this->leave->_editLeave($request);
		$this->sendResponse($response);
	}

	public function _viewLeave($request)
	{			
		$response = $this->leave->_viewLeave($request);
		$this->sendResponse($response);
	}
	public function _addLeaveComments($request)
	{			
		$response = $this->leave->_addLeaveComments($request);
		$this->sendResponse($response);
	}
	public function _viewLeaveComments($request)
	{			
		$response = $this->leave->_viewLeaveComments($request);
		$this->sendResponse($response);
	}
	
	public function _leaveReport($request)
	{			
		$response = $this->leave->_leaveReport($request);
		$this->sendResponse($response);
	}

// 7. Leave Type API

	public function _addLeaveType($request)
	{
		$response = $this->leavesetting->_addLeaveType($request);
		$this->sendResponse($response);
	}

	public function _deleteLeaveType($request)
	{			
		$response = $this->leavesetting->_deleteLeaveType($request);
		$this->sendResponse($response);
	}

	public function _editLeaveType($request)
	{			
		$response = $this->leavesetting->_editLeaveType($request);
		$this->sendResponse($response);
	}
	
	public function _viewLeaveType($request)
	{			
		$response = $this->leavesetting->_viewLeaveType($request);
		$this->sendResponse($response);
	}
	public function _viewLgggedUserPloicies($request)
	{			
		$response = $this->leavesetting->_viewLgggedUserPloicies($request);
		$this->sendResponse($response);
	}
	
	public function _viewLeaveTypeSelected($request)
	{			
		$response = $this->leavesetting->_viewLeaveTypeSelected($request);
		$this->sendResponse($response);
	}
	public function _viewBalanceLeave($request)
	{			
		$response = $this->leave->_viewBalanceLeave($request);
		$this->sendResponse($response);
	}
	public function _viewPaidLeave($request)
	{			
		$response = $this->leave->_viewPaidLeave($request);
		$this->sendResponse($response);
	}
	
  public function _checkLeaveType_nameAvailability($request)
  {
  $response = $this->leavesetting->_checkLeaveType_nameAvailability($request);
  $this->sendResponse($response);
  }
  public function _addLeavePolicy($request)
  {
  $response = $this->leavesetting->_addLeavePolicy($request);
  $this->sendResponse($response);
  }
  public function _maximumcarryDays($request)
  {
  $response = $this->leave->_maximumcarryDays($request);
  $this->sendResponse($response);
  }
  
  public function _viewLeavePloicies($request)
  {
  $response = $this->leavesetting->_viewLeavePloicies($request);
  $this->sendResponse($response);
  }
  public function _deleteLeavePolicy($request)
  {
  $response = $this->leavesetting->_deleteLeavePolicy($request);
  $this->sendResponse($response);
  }
  public function _editLeavePolicy($request)
  {
  $response = $this->leavesetting->_editLeavePolicy($request);
  $this->sendResponse($response);
  }
  public function _viewaccrual_period($request)
  {
  $response = $this->leavesetting->_viewaccrual_period($request);
  $this->sendResponse($response);
  }
  
   public function _viewUserId($request)
  {
  $response = $this->leave->_viewUserId($request);
  $this->sendResponse($response);
  }
  public function _viewUserName($request)
  {
  $response = $this->leave->_viewUserName($request);
  $this->sendResponse($response);
  }
  public function _viewBranchName($request)
  {
  $response = $this->leave->_viewBranchName($request);
  $this->sendResponse($response);
  }
  
// 8. Leave Management API

	public function _viewAllLeave_Where($request)
	{			
		$response = $this->leavemanagement->_viewAllLeave_Where($request);
		$this->sendResponse($response);
	}
	public function _viewAllLeave($request)
	{			
		$response = $this->leavemanagement->_viewAllLeave($request);
		$this->sendResponse($response);
	}

	public function _leaveAction($request)
	{			
		$response = $this->leavemanagement->_leaveAction($request);
		$this->sendResponse($response);
	}
	public function _totalEmployeeLeave($request)
	{			
		$response = $this->leavemanagement->_totalEmployeeLeave($request);
		$this->sendResponse($response);
	}
	
	public function _fullLeaveReport($request)
	{			
		$response = $this->leavemanagement->_fullLeaveReport($request);
		$this->sendResponse($response);
	}
	public function _viewAccuralForLeave($request)
	{			
		$response = $this->users->_viewAccuralForLeave($request);
		$this->sendResponse($response);
	}

	public function _setUserLeave($request)
	{			
		$response = $this->leavemanagement->_setUserLeave($request);
		$this->sendResponse($response);
	}

// 9. Holiday API
	public function _checkDateAvailability($request)
	{			
		$response = $this->users->_checkDateAvailability($request);
		$this->sendResponse($response);
	}

	public function _addHoliday($request)
	{			
		$response = $this->holiday->_addHoliday($request);
		$this->sendResponse($response);
	}
	public function _addHolidayBulk($request)
	{			
		$response = $this->holiday->_addHolidayBulk($request);
		$this->sendResponse($response);
	}
	

	public function _deleteHoliday($request)
	{			
		$response = $this->holiday->_deleteHoliday($request);
		$this->sendResponse($response);
	}

	public function _editHoliday($request)
	{			
		$response = $this->holiday->_editHoliday($request);
		$this->sendResponse($response);
	}
	
	public function _viewTotalHoliday($request)
	{			
		$response = $this->holiday->_viewTotalHoliday($request);
		$this->sendResponse($response);
	}
	public function _viewHoliday($request)
	{			
		$response = $this->holiday->_viewHoliday($request);
		$this->sendResponse($response);
	}
	
	public function _viewEditHoliday($request)
 {
 $response = $this->holiday->_viewEditHoliday($request);
 $this->sendResponse($response);
 }
 public function _viewHolidayBranch($request)
 {
 $response = $this->holiday->_viewHolidayBranch($request);
 $this->sendResponse($response);
 }
// 10. Yellow Page API

	public function _viewYellowPage($request)
	{			
		$response = $this->yellowpage->_viewYellowPage($request);
		$this->sendResponse($response);
	}
	public function _viewYellowPageUsers($request)
	{			
		$response = $this->yellowpage->_viewYellowPageUsers($request);
		$this->sendResponse($response);
	}

// 11. Job Type API

	public function _addJobType($request)
	{
		$response = $this->jobtype->_addJobType($request);
		$this->sendResponse($response);
	}
	public function _contactForm($request)
	{
		$response = $this->jobtype->_contactForm($request);
		$this->sendResponse($response);
	}
	public function _dash_form($request)
	{
		$response = $this->jobtype->_dash_form($request);
		$this->sendResponse($response);
	}
	
	public function _contacts_Form($request)
	{
		$response = $this->jobtype->_contacts_Form($request);
		$this->sendResponse($response);
	}
	
	public function _deleteJobType($request)
	{			
		$response = $this->jobtype->_deleteJobType($request);
		$this->sendResponse($response);
	}

	public function _editJobType($request)
	{			
		$response = $this->jobtype->_editJobType($request);
		$this->sendResponse($response);
	}
	public function _viewJobEducation($request)
	{			
		$response = $this->jobs->_viewJobEducation($request);
		$this->sendResponse($response);
	}
	
	public function _viewJobType($request)
	{			
		$response = $this->jobtype->_viewJobType($request);
		$this->sendResponse($response);
	}
 public function _viewactiveJobType($request)
	{			
		$response = $this->jobtype->_viewactiveJobType($request);
		$this->sendResponse($response);
	}
public function _editJobTypeActive($request)
	{			
		$response = $this->jobtype->_editJobTypeActive($request);
		$this->sendResponse($response);
	}
	public function _viewLastSevenApplication($request)
	{			
		$response = $this->jobs->_viewLastSevenApplication($request);
		$this->sendResponse($response);
	}
	
   public function _chechJobTYpeAvailablity($request)
   {
   $response = $this->jobtype->_chechJobTYpeAvailablity($request);
		$this->sendResponse($response);
   }
// 12. Job Status API

	public function _addJobStatus($request)
	{
		$response = $this->jobstatus->_addJobStatus($request);
		$this->sendResponse($response);
	}

	public function _deleteJobStatus($request)
	{			
		$response = $this->jobstatus->_deleteJobStatus($request);
		$this->sendResponse($response);
	}

	public function _editJobStatus($request)
	{			
		$response = $this->jobstatus->_editJobStatus($request);
		$this->sendResponse($response);
	}

	public function _viewJobStatus($request)
	{			
		$response = $this->jobstatus->_viewJobStatus($request);
		$this->sendResponse($response);
	}

// 13. Job Applicant Status API

	public function _addJobApplicantStatus($request)
	{
		$response = $this->jobapplicantstatus->_addJobApplicantStatus($request);
		$this->sendResponse($response);
	}

	public function _deleteJobApplicantStatus($request)
	{			
		$response = $this->jobapplicantstatus->_deleteJobApplicantStatus($request);
		$this->sendResponse($response);
	}

	public function _editJobApplicantStatus($request)
	{			
		$response = $this->jobapplicantstatus->_editJobApplicantStatus($request);
		$this->sendResponse($response);
	}

	public function _viewJobApplicantStatus($request)
	{			
		$response = $this->jobapplicantstatus->_viewJobApplicantStatus($request);
		$this->sendResponse($response);
	}
 public function _chechJobStatusAvailablity($request)
	{			
		$response = $this->jobapplicantstatus->_chechJobStatusAvailablity($request);
		$this->sendResponse($response);
	}


// 14. Jobs API

	public function _addJob($request)
	{
		$response = $this->jobs->_addJob($request);
		$this->sendResponse($response);
	}
	public function _totalLastMonthJobs($request)
	{
		$response = $this->jobs->_totalLastMonthJobs($request);
		$this->sendResponse($response);
	}
	
	public function _deleteJob($request)
	{			
		$response = $this->jobs->_deleteJob($request);
		$this->sendResponse($response);
	}

	public function _editJob($request)
	{			
		$response = $this->jobs->_editJob($request);
		$this->sendResponse($response);
	}

	public function _viewJob($request)
	{			
		$response = $this->jobs->_viewJob($request);
		$this->sendResponse($response);
	}
	
	public function _viewApplicatiionMonth($request)
	{			
		$response = $this->jobs->_viewApplicatiionMonth($request);
		$this->sendResponse($response);
	}
	
	public function _viewChartApplication($request)
	{			
		$response = $this->jobs->_viewChartApplication($request);
		$this->sendResponse($response);
	}
 	public function _viewCurrentMonthJob($request)
	{			
		$response = $this->jobs->_viewCurrentMonthJob($request);
		$this->sendResponse($response);
	}
public function _viewTodayJob($request)
{
$response = $this->jobs->_viewTodayJob($request);
$this->sendResponse($response);
}
	public function _viewJobPublic($request)
	{			
		$response = $this->jobs->_viewJobPublic($request);
		$this->sendResponse($response);
	}

	public function _changeJobStatus($request)
	{			
		$response = $this->jobs->_changeJobStatus($request);
		$this->sendResponse($response);
	}
public function _totalOpenJobs($request)
{
$response = $this->jobs->_totalOpenJobs($request);
$this->sendResponse($response);
}
public function _totalJobs($request)
{
$response = $this->jobs->_totalJobs($request);
$this->sendResponse($response);
}
public function _totalHoldJobs($request)
{
$response = $this->jobs->_totalHoldJobs($request);
$this->sendResponse($response);
}
public function _totalCloseJobs($request)
{
$response = $this->jobs->_totalCloseJobs($request);
$this->sendResponse($response);
}
public function _viewApprovedDateJob($request)
{
$response = $this->jobs->_viewApprovedDateJob($request);
$this->sendResponse($response);
}

// 15. Job Applicant API

	public function _addJobApplicant($request)
	{
		$response = $this->tickets->_addJobApplicant($request);
		$this->sendResponse($response);
	}

	public function _changeStatusOfJobApplicant($request)
	{
		$response = $this->jobapplicants->_changeStatusOfJobApplicant($request);
		$this->sendResponse($response);
	}

	public function _viewJobApplicant($request)
	{			
		$response = $this->jobapplicants->_viewJobApplicant($request);
		$this->sendResponse($response);
	}

	public function _downloadApplicantResume($request)
	{			
		$response = $this->jobapplicants->_downloadApplicantResume($request);
		$this->sendResponse($response);
	}

	public function _referJobApplicant($request)
	{			
		$response = $this->jobapplicants->_referJobApplicant($request);
		$this->sendResponse($response);
	}
 public function _viewReferredData($request)
	{			
		$response = $this->jobapplicants->_viewReferredData($request);
		$this->sendResponse($response);
	}
// 16. Job Interview API

	public function _addJobInterview($request)
	{
		$response = $this->jobinterview->_addJobInterview($request);
		$this->sendResponse($response);
	}

	public function _changeJobInterviewStatus($request)
	{
		$response = $this->jobinterview->_changeJobInterviewStatus($request);
		$this->sendResponse($response);
	}

	public function _editJobInterview($request)
	{
		$response = $this->jobinterview->_editJobInterview($request);
		$this->sendResponse($response);
	}

	public function _viewJobInterview($request)
	{			
		$response = $this->jobinterview->_viewJobInterview($request);
		$this->sendResponse($response);
	}

// 19. Event API

	public function _addEvent($request)
	{			
		$response = $this->events->_addEvent($request);
		$this->sendResponse($response);
	}

	public function _deleteEvent($request)
	{			
		$response = $this->events->_deleteEvent($request);
		$this->sendResponse($response);
	}

	public function _editEvent($request)
	{			
		$response = $this->events->_editEvent($request);
		$this->sendResponse($response);
	}

	public function _viewevetWhere($request)
	{			
		$response = $this->events->_viewevetWhere($request);
		$this->sendResponse($response);
	}
	
	public function _viewTodayEvent($request)
	{			
		$response = $this->events->_viewTodayEvent($request);
		$this->sendResponse($response);
	}
	public function _viewEvent($request)
	{			
		$response = $this->events->_viewEvent($request);
		$this->sendResponse($response);
	}
 public function _allEvents($request)
	{			
		$response = $this->events->_allEvents($request);
		$this->sendResponse($response);
	}

// 20. Attendance API

   public function _checkAttendance($request)
	{			
		$response = $this->attendance->_checkAttendance($request);
		$this->sendResponse($response);
	}
	
	public function _userFaceEnrollTab($request)
	{			
		$response = $this->attendance->_userFaceEnrollTab($request);
		$this->sendResponse($response);
	}
	public function _registerUserTab($request)
	{			
		$response = $this->attendance->_registerUserTab($request);
		$this->sendResponse($response);
	}
	
	public function _markMultipleAttendance($request)
	{			
		$response = $this->attendance->_markMultipleAttendance($request);
		$this->sendResponse($response);
	}
	public function _markAttendance($request)
	{			
		$response = $this->attendance->_markAttendance($request);
		$this->sendResponse($response);
	}
	public function _checkImeiAvailability($request)
	{			
		$response = $this->attendance->_checkImeiAvailability($request);
		$this->sendResponse($response);
	}
	
	public function _viewAttendance($request)
	{			
		$response = $this->attendance->_viewAttendance($request);
		$this->sendResponse($response);
	}
	public function _viewAttendanceFilter($request)
	{			
		$response = $this->attendance->_viewAttendanceFilter($request);
		$this->sendResponse($response);
	}
	public function _viewAttendanceCalandar($request)
	{			
		$response = $this->attendance->_viewAttendanceCalandar($request);
		$this->sendResponseAttendance($response);
	}
	public function _viewAttendanceUserCalandar($request)
	{			
		$response = $this->attendance->_viewAttendanceUserCalandar($request);
		$this->sendResponseAttendance($response);
	}
	public function _viewAttendanceOnUser($request)
	{			
		$response = $this->attendance->_viewAttendanceOnUser($request);
		$this->sendResponseAttendance($response);
	}
	
	public function _registerNewDeveice($request)
	{			
		$response = $this->attendance->_registerNewDeveice($request);
		$this->sendResponse($response);
	}
	public function _registerUser($request)
	{			
		$response = $this->attendance->_registerUser($request);
		$this->sendResponse($response);
	}
 	public function _userFaceEnroll($request)
	{			
		$response = $this->attendance->_userFaceEnroll($request);
		$this->sendResponse($response);
	}
    public function _viewAttendancePublic($request)
	{			
		$response = $this->attendance->_viewAttendancePublic($request);
		$this->sendResponse($response);
	}
	
	public function _updateProfileImage($request)
	{			
		$response = $this->tickets->_updateProfileImage($request);
		$this->sendResponse($response);
	}
	public function _viewAttendanceTodayPresent($request)
	{			
		$response = $this->attendance->_viewAttendanceTodayPresent($request);
		$this->sendResponse($response);
	}
	
	public function _viewAttendancePerDay($request)
	{			
		$response = $this->attendance->_viewAttendancePerDay($request);
		$this->sendResponse($response);
	}
	public function _addVisitorsPerDay($request)
	{			
		$response = $this->attendance->_addVisitorsPerDay($request);
		$this->sendResponse($response);
	}
	public function _viewVisitos($request)
	{			
		$response = $this->attendance->_viewVisitos($request);
		$this->sendResponse($response);
	}
	
	public function _viewAttendanceSelfUser($request)
	{			
		$response = $this->attendance->_viewAttendanceSelfUser($request);
		$this->sendResponse($response);
	}
	public function _viewAttendanceSelfUserFilter($request)
	{			
		$response = $this->attendance->_viewAttendanceSelfUserFilter($request);
		$this->sendResponse($response);
	}
	public function _viewEnrollAttendance($request)
	{			
		$response = $this->attendance->_viewEnrollAttendance($request);
		$this->sendResponse($response);
	}
	
	public function _viewSelfAttendance($request)
	{			
		$response = $this->attendance->_viewSelfAttendance($request);
		$this->sendResponse($response);
	}
	
 	public function _faceComparision($request)
	{			
		$response = $this->attendance->_faceComparision($request);
		$this->sendResponse($response);
	}
// 21. Attendance Management API

	public function _viewAllAttendance($request)
	{			
		$response = $this->attendancemanagement->_viewAllAttendance($request);
		$this->sendResponse($response);
	}
	// 22. Profile Management API
	
	public function _addNotfication($request)
 {
 $response = $this->profile->_addNotfication($request);
		$this->sendResponse($response);
 }
 
 public function _viewTotalNotification($request)
 {
 $response = $this->profile->_viewTotalNotification($request);
		$this->sendResponse($response);
 }
 public function _notificationRead($request)
 {
 $response = $this->profile->_notificationRead($request);
		$this->sendResponse($response);
 }
 
 public function _viewNotification($request)
 {
 $response = $this->profile->_viewNotification($request);
		$this->sendResponse($response);
 }
 public function _viewNotificationRaead($request)
 {
 $response = $this->profile->_viewNotificationRaead($request);
		$this->sendResponse($response);
 }
 
	public function _viewProfile($request)
	{			
		$response = $this->profile->_viewProfile($request);
		$this->sendResponse($response);
	}
	public function _editProfile($request)
	{			
		$response = $this->profile->_editProfile($request);
		$this->sendResponse($response);
	}
	public function _editUsersDetails($request)
	{			
		$response = $this->profile->_editUsersDetails($request);
		$this->sendResponse($response);
	}
	public function _viewprofilePublic($request)
	{			
		$response = $this->profile->_viewprofilePublic($request);
		$this->sendResponse($response);
	}
	public function _viewProfileEmployee($request)
	{			
		$response = $this->profile->_viewProfileEmployee($request);
		$this->sendResponse($response);
	}
	
	public function _editRelativeDetails($request)
	{			
		$response = $this->profile->_editRelativeDetails($request);
		$this->sendResponse($response);
	}
	public function _editEducationDetails($request)
	{			
		$response = $this->profile->_editEducationDetails($request);
		$this->sendResponse($response);
	}
	public function _editPersonalDetails($request)
	{			
		$response = $this->profile->_editPersonalDetails($request);
		$this->sendResponse($response);
	}
	
 public function _addExperienceDetails($request)
 {
 	$response = $this->profile->_addExperienceDetails($request);
		$this->sendResponse($response);
 }
 public function _viewExperienceEmployee($request)
 {
 $response = $this->profile->_viewExperienceEmployee($request);
		$this->sendResponse($response);
 }
 public function _editExperienceDetails($request){
 $response = $this->profile->_editExperienceDetails($request);
		$this->sendResponse($response);
   }
   public function _addEducationDetails($request)
 {
 	$response = $this->profile->_addEducationDetails($request);
		$this->sendResponse($response);
 }
 public function _editEducation($request)
 {
 $response = $this->profile->_editEducation($request);
	$this->sendResponse($response);
 }
 public function _viewEducationbEmployee($request){
 $response = $this->profile->_viewEducationbEmployee($request);
		$this->sendResponse($response);
   }
   public function _deleteEducationData($request)
   {
   $response = $this->profile->_deleteEducationData($request);
   $this->sendResponse($response);
   }
   public function _deleteExperienceData($request)
   {
   $response = $this->profile->_deleteExperienceData($request);
   $this->sendResponse($response);
   }
		// 23. Reiumbersemnt Management API
	
	public function _addReimbursement($request)
	{			
		$response = $this->tickets->_addReimbursement($request);
		$this->sendResponse($response);
	}
 
	public function _deleteReimbursement($request)
	{			
		$response = $this->reimbursement->_deleteReimbursement($request);
		$this->sendResponse($response);
	}
	public function _deleteReimbursementTravel($request)
	{			
		$response = $this->reimbursement->_deleteReimbursementTravel($request);
		$this->sendResponse($response);
	}
	public function _editReimbursementTravel($request)
	{			
		$response = $this->reimbursement->_editReimbursementTravel($request);
		$this->sendResponse($response);
	}
	
	public function _addReimbursementTravelPolicy($request)
	{			
		$response = $this->reimbursement->_addReimbursementTravelPolicy($request);
		$this->sendResponse($response);
	}
	public function _addReimbursementhotelPolicy($request)
	{			
		$response = $this->reimbursement->_addReimbursementhotelPolicy($request);
		$this->sendResponse($response);
	}
	public function _viewReimbursementHotel($request)
	{			
		$response = $this->reimbursement->_viewReimbursementHotel($request);
		$this->sendResponse($response);
	}
	public function _viewReimbursementTravelPolicy($request)
	{			
		$response = $this->reimbursement->_viewReimbursementTravelPolicy($request);
		$this->sendResponse($response);
	}
	
	public function _editReimbursement($request)
	{			
		$response = $this->reimbursement->_editReimbursement($request);
		$this->sendResponse($response);
	}
	public function _viewReimbursement($request)
	{			
		$response = $this->reimbursement->_viewReimbursement($request);
		$this->sendResponse($response);
	}
	
 public function _editReimbursementStatus($request)
	{			
		$response = $this->reimbursement->_editReimbursementStatus($request);
		$this->sendResponse($response);
	}
	public function _editchangeStatus($request)
	{			
		$response = $this->reimbursement->_editchangeStatus($request);
		$this->sendResponse($response);
	}
	public function _viewReimbursementBranch($request)
 {
 $response = $this->reimbursement->_viewReimbursementBranch($request);
		$this->sendResponse($response);
 }
 public function _viewReimbursementeditBranch($request)
 {
 $response = $this->reimbursement->_viewReimbursementeditBranch($request);
		$this->sendResponse($response);
 }
 
 public function _viewReimbursementApproved($request)
 {
 $response = $this->reimbursement->_viewReimbursementApproved($request);
		$this->sendResponse($response);
 }
 public function _viewReimbursementDeclined($request)
 {
 $response = $this->reimbursement->_viewReimbursementDeclined($request);
		$this->sendResponse($response);
 }
  public function _viewTourReimbursement($request)
 {
 $response = $this->reimbursement->_viewTourReimbursement($request);
		$this->sendResponse($response);
 }
 public function _viewFoodReimbursement($request)
 {
 $response = $this->reimbursement->_viewFoodReimbursement($request);
 $this->sendResponse($response);
 }
 public function _viewOtherReimbursement($request)
 {
 $response = $this->reimbursement->_viewOtherReimbursement($request);
 $this->sendResponse($response);
 }
  public function _editFoodStatus($request)
 {
 $response = $this->reimbursement->_editFoodStatus($request);
 $this->sendResponse($response);
 }
 public function _editOtherStatus($request)
 {
 $response = $this->reimbursement->_editOtherStatus($request);
 $this->sendResponse($response);
 }
 public function _editTourStatus($request)
 {
 $response = $this->reimbursement->_editTourStatus($request);
 $this->sendResponse($response);
 }
 public function _viewTourEmployee($request)
 {
 $response = $this->reimbursement->_viewTourEmployee($request);
 $this->sendResponse($response);
 }
 public function _viewFoodEmployee($request)
 {
 $response = $this->reimbursement->_viewFoodEmployee($request);
 $this->sendResponse($response);
 }
 public function _viewOtherEmployee($request)
 {
 $response = $this->reimbursement->_viewOtherEmployee($request);
 $this->sendResponse($response);
 }
  public function _viewHotelEmployee($request)
 {
 $response = $this->reimbursement->_viewHotelEmployee($request);
 $this->sendResponse($response);
 } 
 public function _editFood($request)
 {
 $response = $this->reimbursement->_editFood($request); 
 $this->sendResponse($response);
 }
  public function _editReimbursementTour($request)
 {
 $response = $this->reimbursement->_editReimbursementTour($request); 
 $this->sendResponse($response);
 }
 
 public function _editReimbursementOther($request)
 {
 $response = $this->reimbursement->_editReimbursementOther($request); 
 $this->sendResponse($response);
 }
 
	//24 User Pay roll System
	
	public function _viewPayrollother($request)
	{			
		$response = $this->payroll->_viewPayrollother($request);
		$this->sendResponse($response);
	}
	public function _viewPayroll($request)
	{			
		$response = $this->payroll->_viewPayroll($request);
		$this->sendResponse($response);
	}
	public function _viewpayrollPending($request)
	{			
		$response = $this->payroll->_viewpayrollPending($request);
		$this->sendResponse($response);
	}
	public function _viewpayrollComplete($request)
	{			
		$response = $this->payroll->_viewpayrollComplete($request);
		$this->sendResponse($response);
	}
	
	public function _addPayroll($request)
	{			
		$response = $this->payroll->_addPayroll($request);
		$this->sendResponse($response);
	}
	public function _addPayrollCtc($request)
	{			
		$response = $this->payroll->_addPayrollCtc($request);
		$this->sendResponse($response);
	}
	
	public function _deletePayroll($request)
	{			
		$response = $this->payroll->_deletePayroll($request);
		$this->sendResponse($response);
	}
	public function _checkPayrollAvailability($request)
	{			
		$response = $this->payroll->_checkPayrollAvailability($request);
		$this->sendResponse($response);
	}
	public function _checkPayrollNotAvailability($request)
	{			
		$response = $this->payroll->_checkPayrollNotAvailability($request);
		$this->sendResponse($response);
	}
	public function _viewpayrollCreateSearch($request)
	{			
		$response = $this->payroll->_viewpayrollCreateSearch($request);
		$this->sendResponse($response);
	}
	
	public function _editPayrollCtc($request)
	{			
		$response = $this->payroll->_editPayrollCtc($request);
		$this->sendResponse($response);
	}
	
	public function _viewpayrolleditcase($request)
	{			
		$response = $this->payroll->_viewpayrolleditcase($request);
		$this->sendResponse($response);
	}
	
	//approve leave Api
	public function _approveLeave($request)
	{			
		$response = $this->payroll->_approveLeave($request);
		$this->sendResponse($response);
	}


	//Resouces Api
	public function _viewResources($request)
	{
		$response = $this->resources->_viewResources($request);
		$this->sendResponse($response);
	}
	public function _addResources($request)
	{
		$response = $this->resources->_addResources($request);
		$this->sendResponse($response);
	}
	public function _deleteResources($request)
	{
		$response = $this->resources->_deleteResources($request);
		$this->sendResponse($response);
	}
	public function _viewAllResources($request)
	{
		$response = $this->resources->_viewAllResources($request);
		$this->sendResponse($response);
	}
		public function _viewAllRoll($request)
	{
		$response = $this->resources->_viewAllRoll($request);
		$this->sendResponse($response);
	}
	public function _editResource($request)
	{
		$response = $this->resources->_editResource($request);
		$this->sendResponse($response);
	}
	//Onboarding Apis
		public function _viewPersonalDocument($request)
	{
		$response = $this->onboarding->_viewPersonalDocument($request);
		$this->sendResponse($response);
	}
	public function _addPersonalDocument($request)
	{
		$response = $this->tickets->_addPersonalDocument($request);
		$this->sendResponse($response);
	}
	public function _addExperience($request)
	{
		$response = $this->tickets->_addExperience($request);
		$this->sendResponse($response);
	}
	public function _addOtherdocument($request)
	{
		$response = $this->tickets->_addOtherdocument($request);
		$this->sendResponse($response);
	}
	public function _addOnboarding($request)
	{
		$response = $this->tickets->_addOnboarding($request);
		$this->sendResponse($response);
	}
	public function _deleteOther($request)
	{
		$response = $this->onboarding->_deleteOther($request);
		$this->sendResponse($response);
	}
	public function _deleteExperience($request)
	{
		$response = $this->onboarding->_deleteExperience($request);
		$this->sendResponse($response);
	}
	public function _deletePersonal($request)
	{
		$response = $this->onboarding->_deletePersonal($request);
		$this->sendResponse($response);
	}
	public function _deleteOnboarding($request)
	{
		$response = $this->onboarding->_deleteOnboarding($request);
		$this->sendResponse($response);
	}
	public function _editExperience($request)
	{
		$response = $this->onboarding->_editExperience($request);
		$this->sendResponse($response);
	}
	public function _editOtherdocument($request)
	{
		$response = $this->onboarding->_editOtherdocument($request);
		$this->sendResponse($response);
	}
	public function _editPersonaldocument($request)
	{
		$response = $this->onboarding->_editPersonaldocument($request);
		$this->sendResponse($response);
	}
	public function _viewOnboarding($request)
	{
		$response = $this->onboarding->_viewOnboarding($request);
		$this->sendResponse($response);
	}
	public function _viewOtherDocument($request)
	{
		$response = $this->onboarding->_viewOtherDocument($request);
		$this->sendResponse($response);
	}
	public function _checkDocumentUploaded($request)
	{
		$response = $this->onboarding->_checkDocumentUploaded($request);
		$this->sendResponse($response);
	}
	public function _checkOfficeDetails($request)
	{
		$response = $this->onboarding->_checkOfficeDetails($request);
		$this->sendResponse($response);
	}
	public function _checkParentDetails($request)
	{
		$response = $this->onboarding->_checkParentDetails($request);
		$this->sendResponse($response);
	}
	
	public function _viewOnboardingBank($request)
	{
		$response = $this->onboarding->_viewOnboardingBank($request);
		$this->sendResponse($response);
	}
	public function _viewOnboardingOffice($request)
	{
		$response = $this->onboarding->_viewOnboardingOffice($request);
		$this->sendResponse($response);
	}
	
	public function _checkLeavePolicyDetails($request)
	{
		$response = $this->onboarding->_checkLeavePolicyDetails($request);
		$this->sendResponse($response);
	}
	
	public function _checkExperienceDetails($request)
	{
		$response = $this->onboarding->_checkExperienceDetails($request);
		$this->sendResponse($response);
	}
	public function _checkBankDetails($request)
	{
		$response = $this->onboarding->_checkBankDetails($request);
		$this->sendResponse($response);
	}
	
	public function _checkEducationDetails($request)
	{
		$response = $this->onboarding->_checkEducationDetails($request);
		$this->sendResponse($response);
	}
	
	public function _checkPersonalDocument($request)
	{
		$response = $this->onboarding->_checkPersonalDocument($request);
		$this->sendResponse($response);
	}
	public function _checkOtherDocument($request)
	{
		$response = $this->onboarding->_checkOtherDocument($request);
		$this->sendResponse($response);
	}
	
	public function _checkExperienceDocument($request)
	{
		$response = $this->onboarding->_checkExperienceDocument($request);
		$this->sendResponse($response);
	}
	
	public function _viewExperience($request)
	{
		$response = $this->onboarding->_viewExperience($request);
		$this->sendResponse($response);
	}
 
 public function _viewLoggedinPersonal($request)
	{
		$response = $this->onboarding->_viewLoggedinPersonal($request);
		$this->sendResponse($response);
	}
//Edit Onboarding
    public function _editOnboarding($request)
	{
		$response = $this->onboarding->_editOnboarding($request);
		$this->sendResponse($response);
	}

   public function _viewLoggedinExperience($request)
	{
		$response = $this->onboarding->_viewLoggedinExperience($request);
		$this->sendResponse($response);
	}
  public function _viewLoggedinUser($request)
	{
		$response = $this->onboarding->_viewLoggedinUser($request);
		$this->sendResponse($response);
	}
 public function _viewLoggedinOther($request)
	{
		$response = $this->onboarding->_viewLoggedinOther($request);
		$this->sendResponse($response);
	}
	// Tickets Api's
	
	public function _deleteTicketsType($request)
	{
		$response = $this->tickets->_deleteTicketsType($request);
		$this->sendResponse($response);
	}
	public function _editTicketsType($request)
	{
		$response = $this->tickets->_editTicketsType($request);
		$this->sendResponse($response);
	}
	public function _addTicketsType($request)
	{
		$response = $this->tickets->_addTicketsType($request);
		$this->sendResponse($response);
	}
	public function _viewTicketsEdit($request)
	{
		$response = $this->tickets->_viewTicketsEdit($request);
		$this->sendResponse($response);
	}
	public function _viewTicketsPriority($request)
	{
		$response = $this->tickets->_viewTicketsPriority($request);
		$this->sendResponse($response);
	}
	public function _viewTicketsType($request)
	{
		$response = $this->tickets->_viewTicketsType($request);
		$this->sendResponse($response);
	}
    public function _addTickets($request)
	{
		$response = $this->tickets->_addTickets($request);
		$this->sendResponse($response);
	}
	public function _deleteTickets($request)
	{
		$response = $this->tickets->_deleteTickets($request);
		$this->sendResponse($response);
	}
	public function _viewTickets($request)
	{
		$response = $this->tickets->_viewTickets($request);
		$this->sendResponse($response);
	}
	public function _editTickets($request)
	{
		$response = $this->tickets->_editTickets($request);
		$this->sendResponse($response);
	}
	public function _addTicketsComments($request)
	{
		$response = $this->tickets->_addTicketsComments($request);
		$this->sendResponse($response);
	}
	public function _viewTicketsComments($request)
	{
		$response = $this->tickets->_viewTicketsComments($request);
		$this->sendResponse($response);
	}
 public function _editTicketsActive($request)
	{
		$response = $this->tickets->_editTicketsActive($request);
		$this->sendResponse($response);
	}
  public function _viewTicketsTotal($request)
	{
		$response = $this->tickets->_viewTicketsTotal($request);
		$this->sendResponse($response);
	}
  public function _viewTicketsleaveTotal($request)
	{
		$response = $this->tickets->_viewTicketsleaveTotal($request);
		$this->sendResponse($response);
	}
   public function _viewTotalHighTicket($request)
	{
		$response = $this->tickets->_viewTotalHighTicket($request);
		$this->sendResponse($response);
	}
  public function _viewTotalLowPriority($request)
	{
		$response = $this->tickets->_viewTotalLowPriority($request);
		$this->sendResponse($response);
	}
	public function _viewTicketsleaveSelf($request)
	{
		$response = $this->tickets->_viewTicketsleaveSelf($request);
		$this->sendResponse($response);
	}
 public function _viewTicketsLoggedInuser($request)
	{
		$response = $this->tickets->_viewTicketsLoggedInuser($request);
		$this->sendResponse($response);
	}
	public function _viewTicketsTotalSelf($request)
	{
		$response = $this->tickets->_viewTicketsTotalSelf($request);
		$this->sendResponse($response);
	}
	public function _viewTicketsPayrollSelf($request)
	{
		$response = $this->tickets->_viewTicketsPayrollSelf($request);
		$this->sendResponse($response);
	}
	public function _viewTotalmediumTicket($request)
	{
		$response = $this->tickets->_viewTotalmediumTicket($request);
		$this->sendResponse($response);
	}
	
	public function _viewTicketsSelf($request)
	{
		$response = $this->tickets->_viewTicketsSelf($request);
		$this->sendResponse($response);
	}
 // Some Api data
 	public function _viewGenderPublic($request)
	{
		$response = $this->profile->_viewGenderPublic($request);
		$this->sendResponse($response);
	}
 public function _viewReligionPublic($request)
	{
		$response = $this->profile->_viewReligionPublic($request);
		$this->sendResponse($response);
	}
 public function _viewMartialPublic($request)
	{
		$response = $this->profile->_viewMartialPublic($request);
		$this->sendResponse($response); 
	}
  public function _viewNationalityPublic($request)
	{
		$response = $this->profile->_viewNationalityPublic($request);
		$this->sendResponse($response); 
	}
  public function _viewRelationnamePublic($request)
	{
		$response = $this->profile->_viewRelationnamePublic($request);
		$this->sendResponse($response); 
	}
 
 // Emplyee data
 
 public function _userViewPolicy($request)
 {
	 $response = $this->users->_userViewPolicy($request);
	 $this->sendResponse($response); 
 }
 public function _assignLeavePolicy($request)
 {
	 $response = $this->users->_assignLeavePolicy($request);
	 $this->sendResponse($response); 
 }
  public function _viewEmployee($request)
	{
		$response = $this->employee->_viewEmployee($request);
		$this->sendResponse($response); 
	}
 public function _editDetails($request)
	{			
		$response = $this->attendance->_editDetails($request);
		$this->sendResponse($response);
	}
	public function _editDetailsprofile($request)
	{			
		$response = $this->profile->_editDetailsprofile($request);
		$this->sendResponse($response);
	}
	
 	public function _editPersonal($request)
	{			
		$response = $this->employee->_editPersonal($request);
		$this->sendResponse($response);
	}
	
	public function _onboardingPersonalDetails($request)
	{
		$response = $this->employee->_onboardingPersonalDetails($request);
		$this->sendResponse($response);
	}
	public function _onboardingBankDetails($request)
	{
		$response = $this->employee->_onboardingBankDetails($request);
		$this->sendResponse($response);
	}
 	public function _editeEmployeeDetails($request)
	{			
		$response = $this->employee->_editeEmployeeDetails($request);
		$this->sendResponse($response);
	}
	public function _editBank($request)
	{			
		$response = $this->employee->_editBank($request);
		$this->sendResponse($response);
	}
	// projects
 public function _viewProject($request)
 {
 $response = $this->projects->_viewProject($request);
 $this->sendResponse($response);
 }
 
 public function _addProject($request)
 {
 $response = $this->tickets->_addProject($request);
 $this->sendResponse($response);
 }
 
 public function _deleteProject($request)
 {
 $response = $this->projects->_deleteProject($request);
 $this->sendResponse($response);
 }
 
 public function _editProject($request)
 {
 $response = $this->tickets->_editProject($request);
 $this->sendResponse($response);
 }
 
 public function _totalProject($request)
 {
 $response = $this->projects->_totalProject($request);
 $this->sendResponse($response);
 }
 
 public function _changeProjectStatus($request)
 {
 $response = $this->projects->_changeProjectStatus($request);
 $this->sendResponse($response);
 }

 public function _totalOngoingProject($request)
 {
 $response = $this->projects->_totalOngoingProject($request);
 $this->sendResponse($response);
 }
 
 public function _totalPendingProject($request)
 {
 $response = $this->projects->_totalPendingProject($request);
 $this->sendResponse($response);
 }

 public function _viewProjectSelf($request)
 {
 $response = $this->projects->_viewProjectSelf($request);
 $this->sendResponse($response);
 }
 public function _viewProjectfilter($request)
 {
 $response = $this->projects->_viewProjectfilter($request);
 $this->sendResponse($response);
 }
 
 public function _totalProjectSelf($request)
 {
 $response = $this->projects->_totalProjectSelf($request);
 $this->sendResponse($response);
 }
 public function _totalOngoingProjectSelf($request)
 {
 $response = $this->projects->_totalOngoingProjectSelf($request);
 $this->sendResponse($response);
 }
 public function _totalPendingProjectSelf($request)
 {
 $response = $this->projects->_totalPendingProjectSelf($request);
 $this->sendResponse($response);
 }
 public function _totalCompleteProject($request)
 {
 $response = $this->projects->_totalCompleteProject($request);
 $this->sendResponse($response);
 }

 public function _totalCompleteProjectSelf($request)
 {
 $response = $this->projects->_totalCompleteProjectSelf($request);
 $this->sendResponse($response);
 }
 
// off boarding Routings

 public function _addOffboarding($request)
 {
 $response = $this->tickets->_addOffboarding($request);
 $this->sendResponse($response);
 }
 
 public function _viewOffboardingCompleteFilter($request)
 {
 $response = $this->offboarding->_viewOffboardingCompleteFilter($request);
 $this->sendResponse($response);
 }
 public function _viewOffboardingUserCompleted($request)
 {
 $response = $this->offboarding->_viewOffboardingUserCompleted($request);
 $this->sendResponse($response);
 }
 
  public function _viewOffboarding($request)
 {
 $response = $this->offboarding->_viewOffboarding($request);
 $this->sendResponse($response);
 }
  public function _deleteOffboarding($request)
 {
 $response = $this->offboarding->_deleteOffboarding($request);
 $this->sendResponse($response);
 }
 public function _SelfOfboarding($request)
 {
 $response = $this->offboarding->_SelfOfboarding($request);
 $this->sendResponse($response);
 }
 public function _viewOffboardingUser($request)
 {
 $response = $this->offboarding->_viewOffboardingUser($request);
 $this->sendResponse($response);
 }
 
 public function _verify_otp($request)
 {
 $response = $this->attendance->_verify_otp($request);
 $this->sendResponse($response);
 }
 public function _resend_otp($request)
 {
 $response = $this->attendance->_resend_otp($request);
 $this->sendResponse($response);
 }
 
 //  Assets Category
 
 public function _viewNoneAssetsIssued($request)
 {
 $response = $this->assets->_viewNoneAssetsIssued($request);
 $this->sendResponse($response);
 }
 public function _viewAssetsGraph($request)
 {
 $response = $this->assets->_viewAssetsGraph($request);
 $this->sendResponse($response);
 }
 
 public function _viewAssetsIssued($request)
 {
 $response = $this->assets->_viewAssetsIssued($request);
 $this->sendResponse($response);
 }
 public function _viewAssetsIssuedid($request)
 {
 $response = $this->assets->_viewAssetsIssuedid($request);
 $this->sendResponse($response);
 }
 public function _addAssetsCategory($request)
 {
 $response = $this->assets->_addAssetsCategory($request);
 $this->sendResponse($response);
 }
 
 public function _viewAssetsCategory($request)
 {
 $response = $this->assets->_viewAssetsCategory($request);
 $this->sendResponse($response);
 }

 public function _editAssetsCategoryStatus($request)
 {
 $response = $this->assets->_editAssetsCategoryStatus($request);
 $this->sendResponse($response);
 }
 
 public function _deletAssetsCategory($request)
 {
 $response = $this->assets->_deletAssetsCategory($request);
 $this->sendResponse($response);
 }
 public function _editAssetsCategory($request)
 {
 $response = $this->assets->_editAssetsCategory($request);
 $this->sendResponse($response);
 }
 
 public function _checkAssetsCategoryAvailability($request)
 {
 $response = $this->assets->_checkAssetsCategoryAvailability($request);
 $this->sendResponse($response);
 }
 public function _viewAssetsCategoryActive($request)
 {
 $response = $this->assets->_viewAssetsCategoryActive($request);
 $this->sendResponse($response);
 }
// assets For Coampany
public function _checkAssetsBillNoAvailability($request)
 {
 $response = $this->assets->_checkAssetsBillNoAvailability($request);
 $this->sendResponse($response);
 }
 public function _checkAssetsidAvailability($request)
 {
 $response = $this->assets->_checkAssetsidAvailability($request);
 $this->sendResponse($response);
 }
 
public function _checkAssetsSerialNoAvailability($request)
 {
 $response = $this->assets->_checkAssetsSerialNoAvailability($request);
 $this->sendResponse($response);
 }
public function _addAssetsCompany($request)
 {
 $response = $this->assets->_addAssetsCompany($request);
 $this->sendResponse($response);
 }
 public function _viewAssetsCompany($request)
 {
 $response = $this->assets->_viewAssetsCompany($request);
 $this->sendResponse($response);
 }
 
 public function _deleteCompanyAssets($request)
 {
 $response = $this->assets->_deleteCompanyAssets($request);
 $this->sendResponse($response);
 }
 public function _editAssetsCompany($request)
 {
 $response = $this->assets->_editAssetsCompany($request);
 $this->sendResponse($response);
 }

 public function _viewAssetsCompanySelect($request)
 {
 $response = $this->assets->_viewAssetsCompanySelect($request);
 $this->sendResponse($response);
 }

// Add Assets User

public function _addAssetsUser($request)
{
	$response = $this->assets->_addAssetsUser($request);
	$this->sendResponse($response);	
}

public function _viewAssetsUserTotal($request)
{
	$response = $this->assets->_viewAssetsUserTotal($request);
	$this->sendResponse($response);	
}
public function _viewAssetsUser($request)
{
	$response = $this->assets->_viewAssetsUser($request);
	$this->sendResponse($response);	
}
public function _viewAsseteditsUser($request)
{
	$response = $this->assets->_viewAsseteditsUser($request);
	$this->sendResponse($response);	
}
public function _viewResendActivity($request)
{
	$response = $this->assets->_viewResendActivity($request);
	$this->sendResponse($response);	
}

public function _viewNoneIssueAsseteditsUser($request)
{
	$response = $this->assets->_viewNoneIssueAsseteditsUser($request);
	$this->sendResponse($response);	
}
public function _editAssetsUserStatus($request)
{
	$response = $this->assets->_editAssetsUserStatus($request);
	$this->sendResponse($response);	
}

public function _deleteUserAssets($request)
{
	$response = $this->assets->_deleteUserAssets($request);
	$this->sendResponse($response);	
}

public function _editAssetsUser($request)
{
	$response = $this->assets->_editAssetsUser($request);
	$this->sendResponse($response);	
}
public function _viewAssetsSelf($request)
{
	$response = $this->assets->_viewAssetsSelf($request);
	$this->sendResponse($response);	
}
// comapny

public function _checkCompanyEmailAvailability($request)
{
	$response = $this->company->_checkCompanyEmailAvailability($request);
	$this->sendResponse($response);	
}
public function _viewCountries($request)
{
	$response = $this->company->_viewCountries($request);
	$this->sendResponse($response);	
}
public function _viewStates($request)
{
	$response = $this->company->_viewStates($request);
	$this->sendResponse($response);	
}
public function _viewCities($request)
{
	$response = $this->company->_viewCities($request);
	$this->sendResponse($response);	
}

public function _viewCompany($request)
{
	$response = $this->company->_viewCompany($request);
	$this->sendResponse($response);	
}
public function _editCompany($request)
{
	$response = $this->attendance->_editCompany($request);
	$this->sendResponse($response);	
}

public function _addCompany($request)
{
	$response = $this->company->_addCompany($request);
	$this->sendResponse($response);	
}
public function _deleteCompany($request)
{
	$response = $this->company->_deleteCompany($request);
	$this->sendResponse($response);	
}

// company Modules
public function _viewsModules($request)
{
	$response = $this->module_addon->_viewsModules($request);
	$this->sendResponse($response);	
}
public function _editModulestatus($request)
{
	$response = $this->module_addon->_editModulestatus($request);
	$this->sendResponse($response);	
}
public function _viewPolicies($request)
{
	$response = $this->module_addon->_viewPolicies($request);
	$this->sendResponse($response);	
}
public function _editPolicyStatus($request)
{
	$response = $this->module_addon->_editPolicyStatus($request);
	$this->sendResponse($response);	
}
public function _editActivityStatus($request)
{
	$response = $this->module_addon->_editActivityStatus($request);
	$this->sendResponse($response);	
}
public function _viewActivity($request)
{
	$response = $this->module_addon->_viewActivity($request);
	$this->sendResponse($response);	
}

} 