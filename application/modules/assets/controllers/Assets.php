<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: Assets.php
	Date: August 20, 2020
	Info: This is the main class which is hold all the Functions of the Assets.
*/

class Assets extends MY_Controller {

    public function __construct(){
		parent::__construct();	
		$this->load->model('assetsModel');
    }
    
    // check Assets Category If Already Exist In DataBase
 public function _checkAssetsCategoryAvailability($request)
 {		
         $module_name = 'assets';
     $permissions = $this->users->_checkPermission($module_name);
     foreach($permissions as $permission){}
     
     if($permission->can_add == 'Y')
     {
         /* Here we check the Assets Category. If User has the permission for that. */

         if(isset($request->category_assets))
         {
             $where_category = array(
                 'category_assets' => $request->category_assets
             );

             $result = $this->assetsModel->checkAssetsNameAvailability($where_category);
             if($result)
                 return array("message" => $request->category_assets." is Available.");
             else
                 $this->api->_sendError("Category Already Present.");
         }
         else
         {
             $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
         }
     }
     else
     {
         $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
     }
 }
 
  // check Bill no. Availability, If Already Exist In DataBase 
  public function _checkAssetsBillNoAvailability($request)
  {		 
		  $module_name = 'assets';
	  $permissions = $this->users->_checkPermission($module_name);
	  foreach($permissions as $permission){}
	  
	  if($permission->can_add == 'Y')
	  {
		  /* Here we check Bill no. Availability. If User has the permission for that. */
 
		  if(isset($request->assets_billno))
		  {
			  $where_assets = array(
				  'assets_billno' => $request->assets_billno
			  );
 
			  $result = $this->assetsModel->checkAssetsSerialAvailability($where_assets);
			  if($result)
				  return array("message" => $request->assets_billno." is Available.");
			  else
				  $this->api->_sendError("Bill No. Already Present.");
		  }
		  else
		  {
			  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		  }
	  }
	  else
	  {
		  $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
	  }
  }

  // Total Assets Graphp View
  public function _viewAssetsGraph($request)
 {		 
		 $module_name = 'assets';
	 $permissions = $this->users->_checkPermission($module_name);
	 foreach($permissions as $permission){}
	 
	 if($permission->can_add == 'Y')
	 {
		 
		 $results = $this->assetsModel->totalAssetsGraph(); 
		 return $results;
	 }
	 else
	 {
		 $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
	 }
 }
  // check None Issued Assets
 public function _viewNoneAssetsIssued($request)
 {		 
		 $module_name = 'assets';
	 $permissions = $this->users->_checkPermission($module_name);
	 foreach($permissions as $permission){}
	 
	 if($permission->can_add == 'Y')
	 {
		 /* Here we check the Non Issued Assets. If User has the permission for that. */
		 $user_ids = $this->_viewAssetsIssuedid();
		
		 if(!empty($user_ids))
					$where = 'assets_company.id NOT IN ('.implode(',',$user_ids).')';
				else
					$where = 'assets_company.id = 0';
		 $results = $this->assetsModel->checkAssetsIssued1($where); 
		 return $results;
	 }
	 else
	 {
		 $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
	 }
 }
 // check Assets Issued 
 public function _viewAssetsIssued($request)
 {		 
		 $module_name = 'assets';
	 $permissions = $this->users->_checkPermission($module_name);
	 foreach($permissions as $permission){}
	 
	 if($permission->can_add == 'Y')
	 {
		 /* Here we check the issued Assets. If User has the permission for that. */
		 $user_ids = $this->_viewAssetsIssuedid();
		
		 if(!empty($user_ids))
					$where = 'assets_company.id IN ('.implode(',',$user_ids).')';
				else
					$where = 'assets_company.id = 0';
		 $results = $this->assetsModel->checkAssetsIssued1($where);
			  
			 
		 return $results;
	 }
	 else
	 {
		 $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
	 }
 }
  // check issued Id  Assets
  public function _viewAssetsIssuedid()
  {		 
		  $module_name = 'assets';
	  $permissions = $this->users->_checkPermission($module_name);
	  foreach($permissions as $permission){}
	  
	  if($permission->can_add == 'Y')
	  {
		  /* Here we check the issued Id. If User has the permission for that. */
 
		  $results = $this->assetsModel->checkAssetsIssued();
				foreach($results as $result)
				{
					$user_ids[] = $result->asset_company_id;     
				}
				return $user_ids;
		 
	  }
	  else
	  {
		  $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
	  }
  }
  // check Assets id availability. If Already Exist In DataBase
  public function _checkAssetsidAvailability($request)
  {		 
		  $module_name = 'assets';
	  $permissions = $this->users->_checkPermission($module_name);
	  foreach($permissions as $permission){}
	  
	  if($permission->can_add == 'Y')
	  {
		  /* Here we check the Assets id availability, If User has the permission for that. */
 
		  if(isset($request->asset_company_id))
		  {
			  $where_assets = array(
				  'asset_company_id' => $request->asset_company_id
			  );
 
			  $result = $this->assetsModel->checkAssetsCompany($where_assets);
			  if($result)
				  return array("message" => $request->asset_company_id." is Available.");
			  else
				  $this->api->_sendError("Assets is Already Given To Other User.");
		  }
		  else
		  {
			  $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		  }
	  }
	  else
	  {
		  $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
	  }
  }
 // check the serial no , If Already Exist In DataBase
 public function _checkAssetsSerialNoAvailability($request)
 {		 
         $module_name = 'assets';
     $permissions = $this->users->_checkPermission($module_name);
     foreach($permissions as $permission){}
     
     if($permission->can_add == 'Y')
     {
         /* Here we check the Assets serial no Availabilty. If User has the permission for that. */

         if(isset($request->serial_no))
         {
             $where_assets = array(
                 'serial_no' => $request->serial_no
             );

             $result = $this->assetsModel->checkAssetsSerialAvailability($where_assets);
             if($result)
                 return array("message" => $request->serial_no." is Available.");
             else
                 $this->api->_sendError("Serial No. Already Present.");
         }
         else
         {
             $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
         }
     }
     else
     {
         $this->api->_sendError("Sorry, You Have No Permission To Add Assets.");
     }
 }
	// add Assets Category
	public function _addAssetsCategory($request)
    {
		$assetsData = array();
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Assets. If User has the permission for that. */

			if(isset($request->category_assets) AND isset($request->description))
			{ 
                $status = $this->_checkAssetsCategoryAvailability($request);
				$user_company = $this->assetsModel->viewCompany();
				$companyid = $user_company->id;
				$assetsData = array(
					'category_assets' => $request->category_assets,
					'company_id' => $companyid,
					'description' => $request->description,
					'status' => 1
				);
				$result = $this->assetsModel->addAssetsCategory($assetsData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Assets Category Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Assets Category Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Assets Category.");
		}
    }

// edit Assets Category Status
 public function _editAssetsCategoryStatus($request)
    {
		$assetsData = array();
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_edit == 'Y')
		{
			/* Here we Update/Edit the Assets Category Status.. If User has the permission for that. */

			if(isset($request->category_id))
			{
				$category_where = array(
					'id' => $request->category_id
				
				);
				if(isset($request->status))
					$assetsData['status'] = $request->status;
                 
				$result = $this->assetsModel->editAssetsCategory($assetsData,$category_where);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Category Status Updated Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Category Status Not Updated. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Edit Assets Status.");
		}
    }

    // Edit Assets Category
 public function _editAssetsCategory($request)
 {
     $assetsData = array();
     $module_name = 'assets';
     $permissions = $this->users->_checkPermission($module_name);
     foreach($permissions as $permission){}
     
     if($permission->can_edit == 'Y')
     {
         /* Here we Update/Edit the Assets Category , If User has the permission for that. */

         if(isset($request->category_id))
         {
             $category_where = array(
                 'id' => $request->category_id
             
             );
             if(isset($request->category_assets))
				 $assetsData['category_assets'] = $request->category_assets;
				// $status = $this->_checkAssetsCategoryAvailability($request);
                 if(isset($request->description))
                 $assetsData['description'] = $request->description;


             $result = $this->assetsModel->editAssetsCategory($assetsData,$category_where);
             if($result)
             {
                 return array(
                     'status' => '1',
                     'message' => 'Assets Category Updated Successfully.'
                 );
             }
             else
             {
                 $this->api->_sendError("Sorry, Assets Category  Not Updated. Please Try Again!!");
             }
         }
         else
         {
             $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
         }
     }
     else
     {
         $this->api->_sendError("Sorry, You Have No Permission To Edit Assets Category.");
     }
 }

 // delete Assets Category
   public function _deletAssetsCategory($request)
   {
	   $assetsData = array();
	   $module_name = 'assets';
	   $permissions = $this->users->_checkPermission($module_name);
	   foreach($permissions as $permission){}
	   
	   if($permission->can_delete == 'Y')
	   {
		   /* Here we check the assets category, if exits in dataBase. */
  
		   if(isset($request->category_id))
		   {
			   $category_where = array(
				   'id' => $request->category_id
			   
			   );
			   $where_category = array(
				'asset_category_id' => $request->category_id
			
			);
			   $result = $this->assetsModel->checkcategoryAvailability($where_category);
			   if($result == TRUE) 
			   {
				$this->api->_sendError("First, Delete This Category From User Issued Assets");
			   }
			   $where_category = array(
				'category_id' => $request->category_id
			
			);
			   $result = $this->assetsModel->checkcompanyAvailability($where_category);
			   if($result == TRUE) 
			   {
				$this->api->_sendError("First, Delete This Category in Company Assets.");
			   }
			   
			   
			   $result = $this->assetsModel->deleteAssetsCategory($category_where);
			   if($result)
			   {
				   return array(
					   'status' => '1',
					   'message' => 'Category Delete Successfully.'
				   );
			   }
			   else
			   {
				   $this->api->_sendError("Sorry, Category  Not Delete. Please Try Again!!");
			   }
		   }
		   else
		   {
			   $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		   }
	   }
	   else
	   {
		   $this->api->_sendError("Sorry, You Have No Permission To Delete Category.");
	   }
   }
  // view Assets Category
  public function _viewAssetsCategory($request)
    {		
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$category_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Category Assets.. If User has the permission for that. */
              
			if(TRUE)
			{

				if(isset($request->category_id ))
				{
					$category_where['assets_category.id'] = $request->category_id ;
					$result = $this->assetsModel->viewAssetsCategoryWhere($category_where);
					return $result;
                }
                else if($request->status === '4')
				{
				$result = $this->assetsModel->viewAssetsCategory();
				return $result;
				}
               else	if(isset($request->status))
				{
					$categorys_where['status'] = $request->status;
					$result = $this->assetsModel->viewAssetsCategoryWhere($categorys_where);
					return $result;
				}
				else
				{
					$result = $this->assetsModel->viewAssetsCategory();
					return $result;
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Category.");
		}
    }

     // view Assets Category Active Status for selected indexes
  public function _viewAssetsCategoryActive($request)
  {		
      $module_name = 'assets';
      $permissions = $this->users->_checkPermission($module_name);
      foreach($permissions as $permission){}
      $category_where = array();
      
      if($permission->can_view == 'Y')
      {
          /* Here we View the category Assets. If User has the permission for that. */
            
          if(TRUE)
          {

              if(isset($request->category_id ))
              {
                  $category_where['assets_category.id'] = $request->category_id ;
                  $result = $this->assetsModel->viewAssetsActiveWhere($category_where);
                  return $result;
              }
              
              else
              {
                  $result = $this->assetsModel->viewAssetsActive();
                  return $result;
              }
          }
          else
          {
              $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
          }
      }
      else
      {
          $this->api->_sendError("Sorry, You Have No Permission To View Category Assets.");
      }
  }

// Add Coampany Assets
public function _addAssetsCompany($request)
    {
	
		
		$assetsData = array();
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the Company Assets. If User has the permission for that. */

            if(isset($request->assets_name) AND isset($request->purchage_date) AND isset($request->purchage_from) AND isset($request->purchage_by)
            AND isset($request->serial_no) AND isset($request->assets_amount) AND isset($request->assets_cond) AND isset($request->assets_warranty)
           AND isset($request->description) AND isset($request->category_id))
			{ 
				
				$status = $this->_checkAssetsSerialNoAvailability($request);
				$status2 = $this->_checkAssetsBillNoAvailability($request);
				$assetsPurchageDate=strtotime($request->purchage_date);
				$currentDate = date('d-m-Y');
	            $currentDate=strtotime($currentDate);
				if($assetsPurchageDate > $currentDate){
					$this->api->_sendError('Assets Cannot Be Add On Future Date.');
				}
				$user_company = $this->assetsModel->viewCompany();
				$companyid = $user_company->id;
				$assetsData = array(
                    'assets_code	' => $this->api->_getRandomString(6),
					'assets_name' => $request->assets_name,
					'company_id' => $companyid,
					'purchage_date' =>$request->purchage_date,
                    'purchage_from' => $request->purchage_from  ,
                    'purchage_by' => $request->purchage_by,
                    'serial_no' => $request->serial_no,
                    'assets_cond' => $request->assets_cond,
                    'assets_warranty' => $request->assets_warranty,
                    'assets_amount' => $request->assets_amount,
					'assets_billno' => $request->assets_billno,
					'description' => $request->description,
                    'category_id' => $request->category_id
                   
				);
				$result = $this->assetsModel->addAssetsCompany($assetsData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Company Assets Added Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Company Assets Not Added. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Add Company Assets.");
		}
    }

// View  Company Assets
 public function _viewAssetsCompany($request)
    {		
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$designation_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Company Assets.. If User has the permission for that. */

			if(TRUE)
			{
				if(isset($request->assets_id)){
					$assets_where['assets_company.id'] = $request->assets_id;
                    $result = $this->assetsModel->viewAssetsCompanyWhere($assets_where);
					return $result;                                             
            }
            else if(isset($request->assets_code)){
                $assets_where['assets_company.assets_code'] = $request->assets_code ;
                $result = $this->assetsModel->viewAssetsCompanyWhere($assets_where);
                return $result;                                                      
		      }
		else if(isset($request->asset_company_id)){
			$assets_wheres['assets_company.id'] = $request->asset_company_id;
			$result = $this->assetsModel->viewAssetsCompanyWhere($assets_wheres);
			return $result;                                                      
	      }
           else	if($request->category_id  === 'all'){
			$user_ids = $this->_viewAssetsIssuedid();
		
			if(!empty($user_ids))
					   $where = 'assets_company.id NOT IN ('.implode(',',$user_ids).')';
				   else
					   $where = 'assets_company.id = 0';
			$results = $this->assetsModel->viewAssetsCompanyWhere($where); 
			return $results;                                                    
            }
			else if(isset($request->category_id )){
					$assets_where['assets_company.category_id'] = $request->category_id ;
                    $result = $this->assetsModel->viewAssetsCompanyWhere($assets_where);
					return $result;                                                      
            }
           
			else
			{
				$result = $this->assetsModel->viewAssetsCompany();
				return $result;
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Assets.");
		}
	}
	// View  Company Assets For selected Indexes
	public function _viewAssetsCompanySelect($request)
    {		
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		$designation_where = array();
		
		if($permission->can_view == 'Y')
		{
			/* Here we View the Company Assets.. If User has the permission for that. */

			if(TRUE)
			{
				
		if (isset($request->asset_company_id)){
			$assets_wheres['assets_company.id'] = $request->asset_company_id;
			$result = $this->assetsModel->viewAssetsCompanyWhereselect($assets_wheres);
			return $result;                                                      
	      }
           
			else
			{
				$result = $this->assetsModel->viewAssetsCompanyselect();
				return $result;
			}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To View Assets.");
		}
    }
    // Delete Company Assets
    public function _deleteCompanyAssets($request)
    {
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_delete == 'Y')
		{
			/* Here we Delete Company Assets. If User has the permission for that. */

			if(isset($request->assets_id))
			{
				$where_assets= array(
					'id' => $request->assets_id,
				);
				$where_company = array(
					'asset_company_id' => $request->assets_id
				
				);
				   $result = $this->assetsModel->checkcompanyUserAvailability($where_company);
				   if($result == TRUE) 
				   {
					$this->api->_sendError("First, Delete This Company Assets From User Issued Assets");
				   }
				$result = $this->assetsModel->deleteAssetsCompany($where_assets);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Company Assets Deleted Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Company Assets Not Deleted. Please Try !");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To Delete Company Assets.");
		}
    }


    // edit Company Assets
 public function _editAssetsCompany($request)
 {
     $assetsData = array();
     $module_name = 'assets';
     $permissions = $this->users->_checkPermission($module_name);
     foreach($permissions as $permission){}
     
     if($permission->can_edit == 'Y')
     {
         /* Here we Update/Edit the Company Assets , If User has the permission for that. */

         if(isset($request->assets_code))
         {
             $company_where = array(
                 'assets_code' => $request->assets_code
             
             );
             if(isset($request->assets_name))
                 $assetsData['assets_name'] = $request->assets_name;
                 if(isset($request->purchage_date))
                 $assetsData['purchage_date'] = $request->purchage_date;
                 if(isset($request->purchage_from))
                 $assetsData['purchage_from'] = $request->purchage_from;
                 if(isset($request->purchage_by))
                 $assetsData['purchage_by'] = $request->purchage_by;
                 if(isset($request->serial_no))
				 $assetsData['serial_no'] = $request->serial_no;
				// $status = $this->_checkAssetsSerialNoAvailability($request);
                 if(isset($request->assets_cond))
                 $assetsData['assets_cond'] = $request->assets_cond;
                 if(isset($request->assets_warranty))
                 $assetsData['assets_warranty'] = $request->assets_warranty;
                 if(isset($request->assets_amount))
                 $assetsData['assets_amount'] = $request->assets_amount;
                 if(isset($request->assets_billno))
                 $assetsData['assets_billno'] = $request->assets_billno;
                 if(isset($request->description))
                 $assetsData['description'] = $request->description;
                 if(isset($request->category_id))
                 $assetsData['category_id'] = $request->category_id;

                // $status = $this->_checkAssetsSerialNoAvailability($request);
				//$status2 = $this->_checkAssetsBillNoAvailability($request);
				$assetsPurchageDate=strtotime($request->purchage_date);
				$currentDate = date('d-m-Y');
	            $currentDate=strtotime($currentDate);
				if($assetsPurchageDate > $currentDate){
					$this->api->_sendError(' Company AsstesCan Not Be Edit Future Date.');
				}
             $result = $this->assetsModel->editAssetsCompany($assetsData,$company_where);
             if($result)
             {
                 return array(
                     'status' => '1',
                     'message' => 'Company Assets Updated Successfully.'
                 );
             }
             else
             {
                 $this->api->_sendError("Sorry, Company Assets Not Updated. Please Try Again!!");
             }
         }
         else
         {
             $this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
         }
     }
     else
     {
         $this->api->_sendError("Sorry, You Have No Permission To Edit Company Assets.");
     }
 }
// issue Assets To User Api
public function _addAssetsUser($request)
    {
		$assetssData = array();
		$module_name = 'assets';
		$permissions = $this->users->_checkPermission($module_name);
		foreach($permissions as $permission){}
		
		if($permission->can_add == 'Y')
		{
			/* Here we Add the User Assets. If User has the permission for that. */

			if(isset($request->issue_date) AND isset($request->issue_to) AND isset($request->asset_company_id) AND isset($request->asset_category_id))
			{ 
				$status = $this->_checkAssetsidAvailability($request);
				$user = $this->api->_GetTokenData();
				$userFirstName = $user['first_name'];
				$user_company = $this->assetsModel->viewCompany();
				$userCompany_id = $user_company->id;

				$IssueDate=strtotime($request->issue_date);
				$dayOfIssue = date("l", $IssueDate);

				$currentDate = date('d-m-Y');
				$currentDate=strtotime($currentDate);
				/* End Date Conversion */
				if($IssueDate > $currentDate){
				$this->api->_sendError('Assst Cannot Be Issue On Future Date.');
				}
				$userAssetsData = array(
					'issue_date' => $request->issue_date,
                    'issued_by' => $userFirstName,
                    'company_id' => $userCompany_id,
					'user_id' => $request->issue_to,
                    'status'=>1,
					'asset_company_id'=>$request->asset_company_id,
					'asset_category_id'=>$request->asset_category_id
				);
				$result = $this->assetsModel->addAssetsuser($userAssetsData);
				if($result)
				{
					return array(
						'status' => '1',
						'message' => 'Asset given To User Successfully.'
					);
				}
				else
				{
					$this->api->_sendError("Sorry, Asset is Not Given. Please Try Again!!");
				}
			}
			else
			{
				$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, You Have No Permission To give asstes to User.");
		}
    }

// View Assets User
public function _viewAssetsUser($request)
{		
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_view == 'Y')
	{
		/* Here we View the Assets.. If User has the permission for that. */

		if(TRUE)
		{
			if(!empty($request->status == 4))
			{
				$result = $this->assetsModel->viewAssetsUser();
			return $result;
			}
	        else{
				if(isset($request->assetsUser_id))
				$assetsUser_where['assets_user.id'] = $request->assetsUser_id;
				if(isset($request->status))
				$assetsUser_where['assets_user.status'] = $request->status;
				if(!empty($assetsUser_where)){
				$result = $this->assetsModel->viewAssetsUserWhere($assetsUser_where);
				return $result;                                             
		}
		else
		{
			$result = $this->assetsModel->viewAssetsUser();
			return $result;
		}
	 }
	}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To View Assets.");
	}
}

// View Assets User
public function _viewAssetsUserTotal($request)
{		
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_view == 'Y')
	{
		/* Here we View the Assets.. If User has the permission for that. */

		if(TRUE)
		{
				if(isset($request->assetsUser_id))
				$assetsUser_where['assets_user.id'] = $request->assetsUser_id;
				if(isset($request->status))
				$assetsUser_where['assets_user.status'] = $request->status;
				if(!empty($assetsUser_where)){
				$result = $this->assetsModel->viewAssetsUserWhere($assetsUser_where);
				return $result;                                             
		}
		else
		{
			$result = $this->assetsModel->viewAssetsUser();
			return $result;
		}
	
	}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To View Assets.");
	}
}

// View Assets User
public function _viewAsseteditsUser($request)
{		
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_view == 'Y')
	{
		/* Here we View the Assets.. If User has the permission for that. */

		if(TRUE)
		{
			if(isset($request->assetsUser_id))
				{
					$assetsUser_where['assets_user.id'] = $request->assetsUser_id;
					$result = $this->assetsModel->viewAssetsUserWhere($assetsUser_where);
					return $result;
				}
				else
				{
					$result = $this->assetsModel->viewAssetsUser();
					return $result;
				}
				
	   }
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To View Assets.");
	}
}
// vie Recent Assets
//SELECT * FROM assets_user ORDER BY id DESC LIMIT 0,1
public function _viewResendActivity($request)
{		
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_view == 'Y')
	{
		/* Here we View the Assets.. If User has the permission for that. */

		if(TRUE)
		{
			
					$result = $this->assetsModel->viewResendActivity();
					return $result;
				
				
	   }
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To View Assets.");
	}
}
// View Assets User
public function _viewNoneIssueAsseteditsUser($request)
{		
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	if($permission->can_view == 'Y')
	{
		/* Here we View the Assets.. If User has the permission for that. */

		if(TRUE)
		{
			if(isset($request->assetsUser_id))
				{
					$assetsUser_where['assets_company.id'] = $request->assetsUser_id;
					$result = $this->assetsModel->checkAssetsIssued1($assetsUser_where);
					return $result;
				}
				else
				{
					$result = $this->assetsModel->checkAssetsNoneIssued1();
					return $result;
				}
				
	   }
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To View Assets.");
	}
}

// Edit Change Status User
public function _editAssetsUserStatus($request)
{
	$assetsUserData = array();
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	
	if($permission->can_edit == 'Y')
	{
		/* Here we Update/Edit the Assets Category Status.. If User has the permission for that. */

		if(isset($request->userStatus_id))
		{
			$UserAssets_where = array(
				'id' => $request->userStatus_id
			
			);
			if(isset($request->status))
				$assetsUserData['status'] = $request->status;

			$result = $this->assetsModel->editAssetsUser($assetsUserData,$UserAssets_where);
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'User Status Updated Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, User Status Not Updated. Please Try Again!!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Edit users Assets.");
	}
}

// Edit Update User issue Assets
public function _editAssetsUser($request)
{
	
	$assetsUserData = array();
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	
	if($permission->can_edit == 'Y')
	{
		/* Here we Update/Edit the Assets Category Status.. If User has the permission for that. */
		
		if(isset($request->userStatus_id))
		{
			$UserAssets_where = array(
				'id' => $request->userStatus_id
			
			);
			$user1 = $this->api->_GetTokenData();
				$userFirstName = $user1['first_name'];
			$user = $this->assetsModel->getAssets_inRow($UserAssets_where);
            $arrAssetsRelease = array(
				'user_id'=>$user->user_id,
				'released_by'=>$userFirstName,
				'asset_company_id'=>$user->asset_company_id,
				'asset_category_id'=>$user->asset_category_id
			);
			
			$result23 = $this->assetsModel->addAssetsRelease($arrAssetsRelease);
		if($request->is_release == 1)			// When User Want to Approve The Leave
		{
			
			
			$result = $this->assetsModel->deleteAssetsUser($UserAssets_where);
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'User Assets Released Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, User Assets Not released. Please Try -!");
			}
		}
		// arrData is Release  Data For User Edit 
		else if($request->is_release == 0)
		{
			if(isset($request->asset_category_id))
				$assetsUserData['asset_category_id'] = $request->asset_category_id;
				if(isset($request->issue_date))
				$assetsUserData['issue_date'] = $request->issue_date;
				if(isset($request->user_id))
				$assetsUserData['user_id'] = $request->user_id;
				if(isset($request->asset_company_id))
				$assetsUserData['asset_company_id'] = $request->asset_company_id;

			$result = $this->assetsModel->editAssetsUser($assetsUserData,$UserAssets_where);
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'User Assets Updated Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, User Assets  Not Updated. Please Try Again!!");
			}
		}
		else{
			$this->api->_sendError("Sorry, Assets User Action Not Performed. Please Try Again!!");
		}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Edit User Assets.");
	}
}

// Assets User Delete 
public function _deleteUserAssets($request)
{
	$module_name = 'assets';
	$permissions = $this->users->_checkPermission($module_name);
	foreach($permissions as $permission){}
	
	if($permission->can_delete == 'Y')
	{
		/* Here we Delete the Assets. If User has the permission for that. */

		if(isset($request->userStatus_id))
		{
			$where_assets= array(
				'id' => $request->userStatus_id,
			);
			$result = $this->assetsModel->deleteAssetsUser($where_assets);
			if($result)
			{
				return array(
					'status' => '1',
					'message' => 'User Assets Deleted Successfully.'
				);
			}
			else
			{
				$this->api->_sendError("Sorry, Assets Not Deleted. Please Try -!");
			}
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	}
	else
	{
		$this->api->_sendError("Sorry, You Have No Permission To Delete Assets.");
	}
}
// View Assets User
public function _viewAssetsSelf($request)
{		
	
		/* Here we View the Assets.. If User has the permission for that. */

		if(TRUE)
		{
			    $user = $this->api->_GetTokenData();
				$userFirstName = $user['id'];
				//print_r($userFirstName);
				//die;
				$assetsUser_where['assets_user.user_id'] = $userFirstName;
				$result = $this->assetsModel->viewAssetstWhere($assetsUser_where);
				return $result;                                             
		
		}
		else
		{
			$this->api->_sendError("Sorry, There is Some Parameter Missing. Please Try Again!!");
		}
	
}

}


