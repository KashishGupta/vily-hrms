<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 
	Project: Willy HRMS
	Author: Pratap Singh
	File Name: AssetsModel.php
	Date: August 20, 2020
	Info: This is the main class which is hold all the Models of the Assets.
*/

class AssetsModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
  
    // check Assets Name If Allready Exist
 function checkAssetsNameAvailability($where_assets)
 {
     $query = $this->db->get_where('assets_category',$where_assets);
     if ($query->num_rows() > 0)
         return FALSE;
     else
         return TRUE;
 }

 // check category id user Asets Table
 function checkcategoryAvailability($where_category)
 {
       // Check The Credentials
     $query = $this->db->get_where('assets_user' , $where_category);
     // If User Exists
     if ($query->num_rows() > 0) {


        return TRUE;
    }
    else{
        return FALSE;
    }
 
 }
 
 // check Company id user Asets Table
 function checkcompanyAvailability($where_company)
 {
       // Check The Credentials
     $query = $this->db->get_where('assets_company' , $where_company);
     // If User Exists
     if ($query->num_rows() > 0) {


        return TRUE;
    }
    else{
        return FALSE;
    }
 
 }
 // check Company id user Asets Table
 function checkcompanyUserAvailability($where_company)
 {
       // Check The Credentials
     $query = $this->db->get_where('assets_user' , $where_company);
     // If User Exists
     if ($query->num_rows() > 0) {


        return TRUE;
    }
    else{
        return FALSE;
    }
 
 }
 // assets company data un a row
 public function getAssets_inRow($credential) {
    // Check The Credentials
    $query = $this->db->get_where('assets_user' , $credential);
    // If User Exists
    if ($query->num_rows() > 0) {

        $row = $query->row();
        
        $this->db->select('*');
        $this->db->from('assets_user');
      
        $this->db->where('assets_user.id', $row->id);
    
       $user = $this->db->get()->row();

        return $user;
    }
    else{
        return FALSE;
    }
}

// total Asstes Graph
 public function totalAssetsGraph()
    {
    // print_r($where);
       $this->db->select('count(*) as totalAssets');
           $this->db->from('assets_company');
            $query = $this->db->get();
           $result = $query->result();
           foreach($result as $key => $row)
            {
            $countdata['data1'] = $row->totalAssets;
            $counting[] = $countdata;
            }
           // issued Assets data 
           $asstes_ids = $this->assets->_viewAssetsIssuedid();
		
           if(!empty($asstes_ids))
                      $where = 'assets_company.id IN ('.implode(',',$asstes_ids).')';
                  else
                      $where = 'assets_company.id = 0';

           $this->db->select('count(*) as totalIssuedAssets');
           $this->db->from('assets_company');
           $this->db->where($where);
            $query1 = $this->db->get();
           $result1 = $query1->result();
           foreach($result1 as $key => $row1)
           {
           $countdata1['data2'] = $row1->totalIssuedAssets;
           $counting1[] = $countdata1;
           }
           $ssets_ids1 = $this->assets->_viewAssetsIssuedid();
		
           if(!empty($ssets_ids1))
                      $where2 = 'assets_company.id NOT IN ('.implode(',',$ssets_ids1).')';
                  else
                      $where2 = 'assets_company.id = 0';
        $this->db->select('count(*) as totalNoneIssuedAssets');
        $this->db->from('assets_company');
        $this->db->where($where2);
        $query2 = $this->db->get();
        $result2 = $query2->result();
        foreach($result2 as $key => $row2)
        {
        $countdata2['data3'] = $row2->totalNoneIssuedAssets;
        $counting2[] = $countdata2;
        }


         $merge = array_merge($counting, $counting1,$counting2);
         return $merge;  
    }

 function checkAssetsCompany($where_assets)
 {
    $query = $this->db->get_where('assets_user',$where_assets);
    if ($query->num_rows() > 0)
        return FALSE;
    else
        return TRUE;
 }

 // total Asstes Graph
 public function viewResendActivity()
{
     // print_r($where);
     $query = $this->db->query("SELECT assets_company.assets_name,users.first_name,user_details.user_image,assets_user.created_at,assets_user.issued_by FROM assets_user INNER join assets_company ON assets_company.id = assets_user.asset_company_id INNER join users ON users.id = assets_user.user_id INNER join user_details ON user_details.user_id = assets_user.user_id ORDER BY assets_user.id DESC LIMIT 0,1");
     $assetsuser = $query->result();
            foreach($assetsuser as $key => $row)
             {
                 $countdata['created_at'] = $row->created_at;
                 $countdata['assets_name'] = $row->assets_name;
                 $countdata['asset_issue'] = $row->issued_by;
                 $countdata['first_name'] = $row->first_name;
                 $countdata['user_image'] = $row->user_image;
             $counting[] = $countdata;
             }
        
            $query2 = $this->db->query("SELECT assets_company.assets_name,users.first_name,user_details.user_image,asstes_released.created_at,asstes_released.released_by FROM asstes_released INNER join assets_company ON assets_company.id = asstes_released.asset_company_id INNER join users ON users.id = asstes_released.user_id INNER join user_details ON user_details.user_id = asstes_released.user_id ORDER BY asstes_released.id DESC LIMIT 0,1");
            $result = $query2->result();
           foreach($result as $key => $row1)
           {
           $countdata1['created_at'] = $row1->created_at;
           $countdata1['assets_name'] = $row1->assets_name;
           $countdata1['released_by'] = $row1->released_by;
           $countdata1['first_name'] = $row1->first_name;
           $countdata1['user_image'] = $row1->user_image;
           $counting1[] = $countdata1;
           }
          

         $merge = array_merge($counting, $counting1);
         return $merge;  
 }

// check Assets Serial no If Allready Exist
function checkAssetsSerialAvailability($where_assets)
{
    $query = $this->db->get_where('assets_company',$where_assets);
    if ($query->num_rows() > 0)
        return FALSE;
    else
        return TRUE;
    
}


    public function addAssetsCategory($data)
    {
        $this->db->insert('assets_category',$data);
        $insert_id = $this->db->insert_id();
        if($insert_id != 0)
        {
            return TRUE;
        }
        else    
        {
            return FALSE;
        }
    }
// Add Assets Company

public function addAssetsCompany($data)
{
    $this->db->insert('assets_company',$data);
    $insert_id = $this->db->insert_id();
    if($insert_id != 0)
    {
        return TRUE;
    }
    else    
    {
        return FALSE;
    }
}
public function addAssetsRelease($data)
{
    $this->db->insert('asstes_released',$data);
    $insert_id = $this->db->insert_id();
    if($insert_id != 0)
    {
        return TRUE;
    }
    else    
    {
        return FALSE;
    }
}
// Add Assets User

public function addAssetsuser($data)
{
    $this->db->insert('assets_user',$data);
    $insert_id = $this->db->insert_id();
    if($insert_id != 0)
    {
        return TRUE;
    }
    else    
    {
        return FALSE;
    }
}

    public function deleteAssetsCompany($where_assets)
    {
        $this->db->where($where_assets);
        $this->db->delete('assets_company');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function deleteAssetsCategory($where_assets)
    {
        $this->db->where($where_assets);
        $this->db->delete('assets_category');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function editAssetsCategory($data,$where)
    {
        $this->db->where($where);
        $this->db->update('assets_category',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    //  edit assets Company
    public function editAssetsCompany($data,$where)
    {
        $this->db->where($where);
        $this->db->update('assets_company',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    
    //  edit assets User
    public function editAssetsUser($data,$where)
    {
        $this->db->where($where);
        $this->db->update('assets_user',$data);
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    // Edit Assets User
    public function deleteAssetsUser($where_assets)
    {
        $this->db->where($where_assets);
        $this->db->delete('assets_user');
        
        if($this->db->affected_rows() != 1) 
        {
           return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }
    public function viewCompany()
    {
        $this->db->select('*');
        $this->db->from('company');
        $query = $this->db->get();
        return $query->row();
    }
    public function viewAssetsActiveWhere($where)
    {
        $user_company = $this->assetsModel->viewCompany();
        $companyid = $user_company->id;
        $company = array('company_id'=>$companyid);
        $this->db->select('*');
        $this->db->from('assets_category');
        $this->db->where($where);
        $this->db->where($company);
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function checkAssetsIssued1($where)
    {
        $this->db->select('*');
        $this->db->from('assets_company');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
 
    }
    public function checkAssetsNoneIssued1()
    {
        $this->db->select('*');
        $this->db->from('assets_company');
        $query = $this->db->get();
        return $query->result();
 
    }

    public function checkAssetsIssued()
    {
        $this->db->select('assets_user.asset_company_id');
        $this->db->from('assets_user');
        
        $query = $this->db->get();
        return $query->result();
 
    }
    public function viewAssetsActive()
    {
        $user_company = $this->assetsModel->viewCompany();
        $companyid = $user_company->id;
          $company = array('company_id'=>$companyid);
        $this->db->select('*');
        $this->db->from('assets_category');
        $this->db->where($company);
        $this->db->where('status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewAssetsCategory()
    {
        $user_company = $this->assetsModel->viewCompany();
        $companyid = $user_company->id;
          $company = array('company_id'=>$companyid);
        $this->db->select('*');
        $this->db->from('assets_category');
        $this->db->where($company);
        $query = $this->db->get();
        return $query->result();
    }

    public function viewAssetsCategoryWhere($where)
    {
        $user_company = $this->assetsModel->viewCompany();
        $companyid = $user_company->id;
          $company = array('company_id'=>$companyid);
        $this->db->select('*');
        $this->db->from('assets_category');
        $this->db->where($company);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
   // view Asstes For Coampany
   public function viewAssetsCompanyselect()
   {
       $usercompany = $this->api->_GetTokenData();
       $usercompanyid = $usercompany['company_id'];
  $company = array('company_id'=>$usercompanyid);
       $this->db->select('*');
       $this->db->from('assets_company');
      // $this->db->join('assets_category','assets_company.category_id = assets_category.id');
     
       $query = $this->db->get();
       return $query->result();
   }

   public function viewAssetsCompanyWhereselect($where)
   {
       $usercompany = $this->api->_GetTokenData();
       $usercompanyid = $usercompany['company_id'];
       $company = array('company_id'=>$usercompanyid);
       $this->db->select('*');
       $this->db->from('assets_company');
     //  $this->db->join('assets_category','assets_company.category_id = assets_category.id');
    
       $this->db->where($where);
       $query = $this->db->get();
       return $query->result();
   }
    // view Asstes For Coampany
    public function viewAssetsCompany()
    { $user_company = $this->assetsModel->viewCompany();
        $companyid = $user_company->id;
          $company = array('company_id'=>$companyid);
        $this->db->select('assets_company.id,assets_company.assets_name,assets_company.purchage_date,assets_company.purchage_from,assets_company.purchage_by,assets_company.serial_no,assets_company.assets_cond,assets_company.assets_warranty,assets_company.assets_amount,assets_company.assets_billno,assets_company.description,assets_company.category_id,assets_company.company_id,assets_company.assets_code,assets_category.category_assets,assets_category.status');
        $this->db->from('assets_company');
        $this->db->join('assets_category','assets_company.category_id = assets_category.id');
      
        $query = $this->db->get();
        return $query->result();
    }

    public function viewAssetsCompanyWhere($where)
    {
        $user_company = $this->assetsModel->viewCompany();
        $companyid = $user_company->id;
          $company = array('company_id'=>$companyid);
        $this->db->select('assets_company.id,assets_company.assets_name,assets_company.purchage_by,assets_company.purchage_date,assets_company.purchage_from,assets_company.purchage_by,assets_company.serial_no,assets_company.assets_cond,assets_company.assets_warranty,assets_company.assets_amount,assets_company.assets_billno,assets_company.description,assets_company.category_id,assets_company.company_id,assets_company.assets_code,assets_category.category_assets,assets_category.status');
        $this->db->from('assets_company');
        $this->db->join('assets_category','assets_company.category_id = assets_category.id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
      // view Asstes For Coampany
      public function viewAssetsUser()
      {
          $this->db->select('assets_user.id,assets_user.issue_date,assets_user.issue_to,assets_user.issued_by,assets_user.status,assets_user.asset_company_id,
          assets_company.assets_code,assets_company.assets_name,assets_company.serial_no,assets_company.category_id,company_branches.branch_name,users.first_name,users.emp_id,assets_category.category_assets');
          $this->db->from('assets_user');
          $this->db->join('assets_category','assets_user.asset_category_id = assets_category.id');
          $this->db->join('assets_company','assets_user.asset_company_id = assets_company.id');
          $this->db->join('users','assets_user.user_id = users.id');
          $this->db->join('company_branches','company_branches.id = users.branch_id');
       $query = $this->db->get();
          return $query->result();
      }
  
      public function viewAssetsUserWhere($where)
      {
          $this->db->select('assets_user.id,assets_user.issue_date,assets_user.is_release,assets_user.asset_category_id,assets_user.user_id,assets_user.issued_by,assets_user.status,assets_user.asset_company_id,
          assets_company.assets_code,assets_company.assets_name,assets_company.serial_no,assets_company.category_id,company_branches.branch_name,users.first_name,users.emp_id,assets_category.category_assets');
          $this->db->from('assets_user');
          $this->db->join('assets_category','assets_user.asset_category_id = assets_category.id');
          $this->db->join('assets_company','assets_user.asset_company_id = assets_company.id');
          $this->db->join('users','assets_user.user_id = users.id');
          $this->db->join('company_branches','company_branches.id = users.branch_id');
          $this->db->where($where);
          $query = $this->db->get();
          return $query->result();
      }

      public function viewAssetstWhere($where)
      {
        $user_company = $this->assetsModel->viewCompany();
        $companyid = $user_company->id;
          $company = array('company_id'=>$companyid);
          $this->db->select('assets_user.id,assets_user.issue_date,assets_user.user_id,assets_user.issued_by,assets_user.status,assets_user.asset_company_id,
          assets_company.assets_code,assets_company.assets_name,assets_company.serial_no,assets_company.category_id');
          $this->db->from('assets_user');
         // $this->db->join('assets_category','assets_user.asset_category_id = assets_category.id');
          $this->db->join('assets_company','assets_user.asset_company_id = assets_company.id');
        
          $this->db->where('status', 1);
          $this->db->where($where);
          $query = $this->db->get();
          return $query->result();
      }
}