<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Login';
$route['Dashboard'] = "Vily/Dashboard";

$route['Branch'] = "Vily/Branch";
$route['BranchAdd'] = "Vily/BranchAdd";

$route['Department'] = "Vily/Department";

$route['Designation'] = "Vily/Designation";

$route['EmployeeList'] = "Vily/EmployeeList";
$route['EmployeeAdd'] = "Vily/EmployeeAdd";
$route['Employee'] = "Vily/Employee";
$route['EmployeeEdit/(:any)'] = 'Vily/EmployeeEdit/$1';

$route['LeaveComment/(:any)'] = 'Vily/LeaveComment/$1';

$route['Leave'] = "Vily/Leave";
$route['Leave'] = "Vily/Leave";

$route['Attendance'] = "Vily/Attendance";
$route['AttendanceReport'] = "Vily/AttendanceReport";



$route['Tickets'] = "Vily/Tickets";

$route['ProjectDashboard'] = "Vily/ProjectDashboard";
$route['ProjectAdd'] = "Vily/ProjectAdd";
$route['ProjectEdit/(:any)'] = 'Vily/ProjectEdit/$1';
$route['Projects'] = "Vily/Projects";


$route['JobDashboard'] = "Vily/JobDashboard";
$route['JobAdd'] = "Vily/JobAdd";
$route['JobEdit/(:any)'] = 'Vily/JobEdit/$1';



$route['JobType'] = "Vily/JobType";
$route['JobStatus'] = "Vily/JobStatus";
$route['Jobs'] = "Vily/Jobs";
$route['PayrollAddSalary'] = "Vily/PayrollAddSalary";
$route['PayrollAddCTC/(:any)'] = 'Vily/PayrollAddCTC/$1';
$route['PayrollSalaryAdd/(:any)/(:any)'] = 'Vily/PayrollSalaryAdd/$1/$2';
$route['PayrollView2/(:any)/(:any)'] = 'Vily/PayrollView2/$1/$2';
$route['paySlip/(:any)/(:any)/(:any)'] = 'Vily/paySlip/$1/$2/$3';


$route['PayrollSalary'] = "Vily/PayrollSalary";
$route['PayrollView'] = "Vily/PayrollView";

$route['AssetsCategory'] = "Vily/AssetsCategory";
$route['Assets'] = "Vily/Assets";
$route['AssetAdd'] = "Vily/AssetAdd";
$route['AssetsDetails/(:any)'] = "Vily/AssetsDetails/$1";
$route['AssetEdit/(:any)'] = "Vily/AssetEdit/$1";


$route['AssetsManagement'] = "Vily/AssetsManagement";

$route['ReimbursementList'] = "Vily/ReimbursementList";
$route['Reimbursement'] = "Vily/Reimbursement";
$route['ReimbursementView/(:any)'] = 'Vily/ReimbursementView/$1';


$route['onboardingList'] = "Vily/onboardingList";
$route['onBoardingAddDetails/(:any)'] = 'Vily/onBoardingAddDetails/$1';
$route['onBoardingAddDocument/(:any)'] = 'Vily/onBoardingAddDocument/$1';
$route['onBoardingView/(:any)'] = 'Vily/onBoardingView/$1';


$route['offBoardingList'] = "Vily/offBoardingList";
$route['offBoardingAdd/(:any)'] = 'Vily/offBoardingAdd/$1';
$route['offBoardingView/(:any)'] = 'Vily/offBoardingView/$1';



$route['Role'] = "Vily/Role";
$route['UpdatePermission/(:any)'] = 'Vily/UpdatePermission/$1';

$route['LeaveType'] = "Vily/LeaveType";

$route['LeavePolicy'] = "Vily/LeavePolicy";
$route['Leavepolicy_edit/(:any)'] = 'Vily/Leavepolicy_edit/$1';


$route['ProfileAdmin'] = "Vily/ProfileAdmin";
$route['CompanySetting'] = "Vily/CompanySetting";


$route['ReimbursementPolicy'] = "Vily/ReimbursementPolicy";

$route['ChangePassword'] = "Vily/ChangePassword";

$route['Visitor'] = "Vily/Visitor";

$route['Calendar'] = "Vily/Calendar";

$route['YellowPageList'] = "Vily/YellowPageList";

$route['Holiday'] = "Vily/Holiday";

$route['Notification'] = "Vily/Notification";

$route['Chat/(\d+)'] = 'Vily/Chat/$1';

// For Employee Side remove Hrms 
$route['Home'] = "Hrms/Dashboard";

$route['Branch_emp'] = "Hrms/Branch";

$route['Department_emp'] = "Hrms/Department";

$route['Designation_emp'] = "Hrms/Designation";

$route['EmployeeList_emp'] = "Hrms/EmployeeList";
$route['EmployeeAdd_emp'] = "Hrms/EmployeeAdd";
$route['Employee_emp'] = "Hrms/Employee";

$route['EmployeeView_emp/(:any)'] = 'Hrms/EmployeeView/$1';
$route['EmployeeEdit_emp/(:any)'] = 'Hrms/EmployeeEdit/$1';

$route['Attendance_emp'] = 'Hrms/AttendanceEmp';

$route['LeaveType_emp'] = 'Hrms/LeaveType';
$route['Leave_emp'] = 'Hrms/Leave';
$route['LeaveSelf_emp'] = 'Hrms/LeaveSelf';

$route['Tickets_emp'] = 'Hrms/Tickets';
$route['Ticketadd_emp'] = 'Hrms/Ticketadd';


$route['ProjectList_emp'] = 'Hrms/ProjectList';

$route['JobDashboard_emp'] = 'Hrms/JobDashboard';
$route['JobAdd_emp'] = 'Hrms/JobAdd';
$route['JobEdit_emp/(:any)'] = 'Hrms/JobEdit/$1';
$route['JobType_emp'] = 'Hrms/JobType';
$route['JobStatus_emp'] = 'Hrms/JobStatus';
$route['Jobs_emp'] = 'Hrms/Jobs';
$route['jobApplicants_emp/(:any)'] = 'Hrms/jobApplicants/$1';
$route['JobPublic_emp'] = 'Hrms/JobPublic';
$route['JobDetails_emp/(:any)'] = 'Hrms/JobDetails/$1';
$route['JobApply_emp/(:any)'] = 'Hrms/JobApply/$1';
$route['JobApplyStatus_emp'] = 'Hrms/JobApplyStatus';

$route['PayrollView_emp'] = 'Hrms/PayrollView';
$route['paySlip_emp/(:any)/(:any)/(:any)'] = 'Hrms/paySlip/$1/$2/$3';

$route['AssetsCategory_emp'] = "Hrms/AssetsCategory";
$route['Assets_emp'] = "Hrms/Assets";
$route['AssetAdd_emp'] = "Hrms/AssetAdd";
$route['AssetsSelf_emp'] = "Hrms/AssetsSelf";
$route['AssetsManagement_emp'] = "Hrms/AssetsManagement";
$route['AssetEdit_emp/(:any)'] = "Hrms/AssetEdit/$1";

$route['ReimbursementList_emp'] = "Hrms/ReimbursementList";
$route['Reimbursement_emp'] = "Hrms/Reimbursement";
$route['ReimbursementApproved_emp'] = "Hrms/ReimbursementApproved";
$route['ReimbursementDenied_emp'] = "Hrms/ReimbursementDenied";
$route['ReimbursementView_emp/(:any)'] = "Hrms/ReimbursementView/$1";



$route['Calendar_emp'] = "Hrms/Calendar";

$route['YellowPageList_emp'] = "Hrms/YellowPageList";

$route['YellowPage'] = "Hrms/YellowPage";

$route['Holiday_emp'] = "Hrms/Holiday";

$route['Notification_emp'] = "Hrms/Notification";

$route['offBoardingList_emp'] = "Hrms/offBoardingList";
$route['offBoarding_emp'] = "Hrms/offBoarding";
$route['offBoardingComplete_emp'] = "Hrms/offBoardingComplete";

$route['Profile'] = "Hrms/Profile";
$route['Chat_emp/(\d+)'] = 'Hrms/Chat/$1';
$route['ChangePassword_emp'] = "Hrms/ChangePassword";

$route['404_override'] = 'Vily/not_found';

$route['translate_uri_dashes'] = FALSE;
