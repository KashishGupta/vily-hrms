<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Controller class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {

    public function __construct() 
	{
        parent::__construct();
        $this->load->module(array(
            'api','users','yellowpage','branch','department','designation','leave','leavesetting','leavemanagement','holiday',
            'jobtype','jobstatus','jobapplicantstatus','jobs','jobapplicants','jobinterview',
            'offboarding','events','profile','attendance','attendancemanagement','reimbursement','payroll','resources','onboarding',
            'tickets','employee','projects','assets','company','module_addon'
        ));
    }
}