<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	// Constructor Function Which Load All Neccessary Models,Libraries,Helpers
   public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        //$this->load->library('session');
         $this->load->model('LoginModel');
        $this->load->database();
         $this->load->helper(array('cookie', 'url')); 
        
    }
    
   public function index()
	{
	      
        $this->load->view('login');
    }
	
    // Forget Pasword by mail
	public function forgotPassword()
	{
	    	$this->load->view('forgotPassword');
	
	}
 // Reset Pasword
	public function resetPassword()
	{
	 	$this->load->view('resetPassword');
	
	}

 public function logout()
    {
     setcookie('ci_session',"",time()-3600);
	    $this->LoginModel->clear_cache();
	    $this->session->sess_destroy();
        redirect(base_url() , 'refresh');
    }
}