<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hrms extends CI_Controller {

	// Constructor Function Which Load All Neccessary Models,Libraries,Helpers
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->driver('session');
        $this->load->library('email');
        $this->load->helper('email');
    }
	

//=========================================Sidebar Menu 	
    public function YellowPage()
	{
		$data['title'] = "YellowPage | Ezdat HRMS";
		$data['page_name'] = "YellowPage";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function YellowPageList()
	{
		$data['title'] = "YellowPage List | Ezdat HRMS";
		$data['page_name'] = "YellowPageList";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function YellowPageView($userid="")
	{
		$data['title'] = "YellowPage View | Ezdat HRMS";
	    $data['userid'] = $userid;
		$data['page_name'] = "YellowPageView";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Holiday()
	{
		$data['title'] = "Holiday | Ezdat HRMS";
		$data['page_name'] = "Holiday";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	
	public function Chat($userid = '')
	{
		$data['page_name'] = "Chat";
		$data['title'] = "Chat | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Calendar()
	{
		$data['title'] = "Calendar | Ezdat HRMS";
		$data['page_name'] = "Calendar";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Notification()
	{
		$data['title'] = "Notification | Ezdat HRMS";
		$data['page_name'] = "Notification";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	// Payroll 
	public function PayrollView()
	{
		$data['page_name'] = "PayrollView";
		$data['title'] = "Payroll View | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function paySlip($userId='' ,$montha = '',$yeara = '')
	{
		$data['page_name'] = "paySlip";
		$data['title'] = "Payroll Slip | Ezdat HRMS";
		$data['userId'] = $userId;
		$data['montha'] = $montha;
		$data['yeara'] = $yeara;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
//=========================================HRMS	

	public function Dashboard()
	{
		$data['title'] = "Dashboard | Ezdat HRMS";
		$data['page_name'] = "Dashboard";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function Branch()
	{
		$data['title'] = "Branch | Ezdat HRMS";
		$data['page_name'] = "Branch";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function BranchAdd()
	{
		$data['title'] = "Add Branch | Ezdat HRMS";
		$data['page_name'] = "BranchAdd";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

     public function Department()
	{
		$data['title'] = "Department | Ezdat HRMS";
		$data['page_name'] = "Department";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function Designation()
	{
		$data['title'] = "Designation | Ezdat HRMS";
		$data['page_name'] = "Designation";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
    public function Employee()
	{
		$data['title'] = "Employee | Ezdat HRMS";
		$data['page_name'] = "Employee";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	 public function EmployeeList()
	{
		$data['title'] = "Employee | Ezdat HRMS";
		$data['page_name'] = "EmployeeList";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function EmployeeView($userid = '')
	{
		$data['title'] = "Employee View | Ezdat HRMS";
		$data['page_name'] = "EmployeeView";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
    public function EmployeeEdit($userid='')
	{
		$data['title'] = "Employee Edit | Ezdat HRMS";
		$data['page_name'] = "EmployeeEdit";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function EmployeeAdd()
	{
		$data['title'] = "Employee Add | Ezdat HRMS";
		$data['page_name'] = "EmployeeAdd";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function AttendanceEmp()
	{
		$data['title'] = "Attendance Employee | Ezdat HRMS";
		$data['page_name'] = "AttendanceEmp";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function LeaveType()
	{
		$data['title'] = "Leave Type | Ezdat HRMS";
		$data['page_name'] = "LeaveType";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Leave()
	{
		$data['title'] = "Leave | Ezdat HRMS";
		$data['page_name'] = "Leave";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
 	public function LeaveSelf()
	{
		$data['title'] = "Leave Self | Ezdat HRMS";
		$data['page_name'] = "LeaveSelf";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
    public function Tickets()
	{
		$data['title'] = "Tickets | Ezdat HRMS";
		$data['page_name'] = "Tickets";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	
	public function TicketsCommnets($ticketid = '')
	{
		$data['page_name'] = "ticketComments";
		$data['title'] = "Tickets | Ezdat HRMS";
		$data['ticketid'] = $ticketid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function TicketEdit($ticketid = '')
	{
		$data['page_name'] = "ticketEdit";
		$data['title'] = "Tickets Edit | Ezdat HRMS";
		$data['ticketid'] = $ticketid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Ticketadd()
	{
		$data['page_name'] = "Ticketadd";
		$data['title'] = "Tickets Add| Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	//=========================================Project
	public function Projects()
	{
		$data['title'] = "Project  | Ezdat HRMS";
		$data['page_name'] = "Projects";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ProjectList()
	{
		$data['title'] = "Project List | Ezdat HRMS";
		$data['page_name'] = "ProjectList";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ProjectAdd()
	{
		$data['page_name'] = "ProjectAdd";
		$data['title'] = "Add Projects | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function ProjectEdit($userid = '')
	{
		$data['page_name'] = "ProjectEdit";
		$data['title'] = "Edit Projects | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ProjectDetails($projectid = '')
	{
		$data['page_name'] = "ProjectDetails";
		$data['title'] = "Project Details | Ezdat HRMS";
		$data['projectid'] = $projectid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	
	
	
	//=========================================Job 	
    public function JobDashboard()
	{
		$data['title'] = "Job Dashboard | Ezdat HRMS";
		$data['page_name'] = "JobDashboard";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function JobType()
	{
		$data['title'] = "Job Type | Ezdat HRMS";
		$data['page_name'] = "JobType";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function JobStatus()
	{
		$data['title'] = "Job Status | Ezdat HRMS";
		$data['page_name'] = "JobStatus";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Jobs()
	{
		$data['title'] = "Jobs | Ezdat HRMS";
		$data['page_name'] = "Jobs";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function JobAdd()
	{
		$data['title'] = "Job Add| Ezdat HRMS";
		$data['page_name'] = "JobAdd";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function JobEdit($jobid = '')
	{
		$data['title'] = "Job Edit | Ezdat HRMS";
		$data['page_name'] = "JobEdit";
		$data['jobid'] = $jobid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	
	public function JobApplyApplicants($jobId = '')
	{
		$data['title'] = "Job Apply Applicants | Ezdat HRMS";
		$data['page_name'] = "JobApplyApplicants";
		$data['jobId'] = $jobId;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function JobPublic()
	{
		$data['title'] = "Job Public | Ezdat HRMS";
		$data['page_name'] = "JobPublic";
		$this->load->view('HRMS/Hrmsmain', $data);
	}
	public function JobDetails($jobId = '')
	{
		$data['title'] = "Job Details | Ezdat HRMS";
		$data['page_name'] = "JobDetails";
		$data['jobId'] = $jobId;
		$this->load->view('HRMS/Hrmsmain', $data);
	}
    public function JobApply($jobId = '')
	{
		$data['title'] = "Job Apply | Ezdat HRMS";
		$data['page_name'] = "JobApply";
		$data['jobId'] = $jobId;
		$this->load->view('HRMS/Hrmsmain', $data);
	}
	public function JobApplyStatus()
	{
		$data['title'] = "Job Apply Status | Ezdat HRMS";
		$data['page_name'] = "JobApplyStatus";
		$this->load->view('HRMS/Hrmsmain', $data);
	}
	

	//=========================================HRMS	Assets
	public function AssetsCategory ()
	{
		$data['page_name'] = "AssetsCategory";
		$data['title'] = "Assets Category | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Assets()
	{
		$data['page_name'] = "Assets";
		$data['title'] = "Assets | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function AssetAdd()
	{
		$data['page_name'] = "AssetAdd";
		$data['title'] = "Add Asset | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function AssetEdit($assetcode='')
	{
		$data['page_name'] = "AssetEdit";
		$data['assetcode'] = $assetcode;
		$data['title'] = "Edit Asset | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function AssetDetails($assetcode='')
	{
		$data['page_name'] = "AssetDetails";
		$data['title'] = "Asset Details | Ezdat HRMS";
		$data['assetcode'] = $assetcode;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function AssetsManagement()
	{
		$data['page_name'] = "AssetsManagement";
		$data['title'] = "Add Issue Assets | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function AssetsSelf()
	{
		$data['page_name'] = "AssetsSelf";
		$data['title'] = "Assets Self | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}




	//=========================================Reimbursement 	
	public function Reimbursement()
	{
		$data['title'] = "Reimbursement | Ezdat HRMS";
		$data['page_name'] = "Reimbursement";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ReimbursementList()
	{
		$data['title'] = "Reimbursement | Ezdat HRMS";
		$data['page_name'] = "ReimbursementList";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ReimbursementView($reimbursementid='')
	{
		$data['title'] = "Reimbursement View | Ezdat HRMS";
		$data['page_name'] = "ReimbursementView";
		$data['reimbursementid'] = $reimbursementid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ReimbursementApproved()
	{
		$data['title'] = "Reimbursement Approved | Ezdat HRMS";
		$data['page_name'] = "ReimbursementApproved";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ReimbursementDenied()
	{
		$data['title'] = "Reimbursement Denied | Ezdat HRMS";
		$data['page_name'] = "ReimbursementDenied";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	
	// Self Reimbursement
	public function ReimbursementSelf()
	{
		$data['title'] = "Reimbursement Self | Ezdat HRMS";
		$data['page_name'] = "ReimbursementSelf";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ReimbursementSelfList()
	{
		$data['title'] = "Reimbursement Self | Ezdat HRMS";
		$data['page_name'] = "ReimbursementSelfList";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ReimbursementSelfAdd()
	{
		$data['title'] = "Reimbursement Self Add | Ezdat HRMS";
		$data['page_name'] = "ReimbursementSelfAdd";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	
	public function ReimbursementSelfView($reimbursementid='')
	{
		$data['title'] = "Reimbursement Self View | Ezdat HRMS";
		$data['page_name'] = "ReimbursementSelfView";
		$data['reimbursementid'] = $reimbursementid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function onBoarding()
	{
		$data['page_name'] = "onBoarding";
		$data['title'] = "On-Boarding Pending | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function onBoardingList()
	{
		$data['page_name'] = "onBoardingList";
		$data['title'] = "On-Boarding List | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	} 	
	public function onBoardingAdd($userid='')
	{
		$data['page_name'] = "onBoardingAdd";
		$data['title'] = "Add On-Boarding | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}	

	public function onBoardingAddDocument($userid='')
	{
		$data['page_name'] = "onBoardingAddDocument";
		$data['title'] = "Add On-Boarding Employee Document| Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function onBoardingAddDetails($userid='')
	{
		$data['page_name'] = "onBoardingAddDetails";
		$data['title'] = "Add On-Boarding Employee Details| Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function onBoardingView($userid='')
	{
		$data['page_name'] = "onBoardingView";
		$data['title'] = "View On-Boarding | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('HRMS/Hrmsmain',$data);
	}
    public function onBoardingSelf()
	{
		$data['page_name'] = "onBoardingSelf";
		$data['title'] = "On-Boarding Self | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}

	public function onBoardingComplete()
	{
		$data['page_name'] = "onBoardingComplete";
		$data['title'] = "On-Boarding Complete | Ezdat HRMS";
		$this->load->view('HRMS/Hrmsmain',$data);
	}



// offboarding 
public function offBoarding()
{
	$data['page_name'] = "offBoarding";
	$data['title'] = "Off-Boarding | Ezdat HRMS";
	$this->load->view('HRMS/Hrmsmain',$data);
}
public function offBoardingList()
{
	$data['page_name'] = "offBoardingList";
	$data['title'] = "Off-Boarding List | Ezdat HRMS";
	$this->load->view('HRMS/Hrmsmain',$data);
}
public function offBoardingComplete()
{
	$data['page_name'] = "offBoardingComplete";
	$data['title'] = "Off-Boarding Comlete | Ezdat HRMS";
	$this->load->view('HRMS/Hrmsmain',$data);
}
public function offBoardingAdd($userid='')
{
	$data['page_name'] = "offBoardingAdd";
	$data['title'] = "Add Off-Boarding | Ezdat HRMS";
	$data['userid'] = $userid;
	$this->load->view('HRMS/Hrmsmain',$data);
}
public function offBoardingView($userid='')
{
	$data['page_name'] = "offBoardingView";
	$data['title'] = "View offBoarding | Ezdat HRMS";
	$data['userid'] = $userid;
	$this->load->view('HRMS/Hrmsmain',$data);
}
public function offboardingSelf()
{
	$data['page_name'] = "offboardingSelf";
	$data['title'] = "View offBoarding Self | Ezdat HRMS";
	$this->load->view('HRMS/Hrmsmain',$data);
}

	// User Profile
	public function Profile()
	{
		$data['title'] = "Profile | Ezdat HRMS";
		$data['page_name'] = "Profile";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function ChangePassword()
	{
		$data['title'] = "Change Password | Ezdat HRMS";
		$data['page_name'] = "ChangePassword";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
	public function Users()
	{
		$data['title'] = "Users | Ezdat HRMS";
		$data['page_name'] = "Users";
		$this->load->view('HRMS/Hrmsmain',$data);
	}
}