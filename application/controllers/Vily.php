<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vily extends CI_Controller {

	// Constructor Function Which Load All Neccessary Models,Libraries,Helpers
	public function __construct()
    {
		parent::__construct();
		//parent::Model();
		$this->load->database();
        date_default_timezone_set('Asia/Kolkata');
       // $this->load->helper(array('form', 'url'));
       // $this->load->library('form_validation');
       // $this->load->model('admin_model');
      //  $this->load->model('company_model');
        //$this->load->library('s3');
        // $this->output->enable_profiler(TRUE);  
    }
    
    
	//=========================================Side bar menu	
	

// not_found
public function not_found()
{
	$this->load->view('404');
}

	// company set up seeds
	public function CompanySetting()
	{
		$data['page_name'] = "CompanySetting";
		$data['title'] = "Company Setting | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function AttendanceCalendar()
	{
		$data['page_name'] = "AttendanceCalendar";
		$data['title'] = " Attendance Calendar | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function Attendance_Users($userid="")
	{
		$data['userid'] = $userid;
		$data['page_name'] = "Attendance_Users";
		$data['title'] = " Attendance Users | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

   
	// calender 
	public function Calendar()
	{
		$data['page_name'] = "Calendar";
		$data['title'] = "Calendar | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function YellowPage()
	{
		$data['page_name'] = "YellowPage";
		$data['title'] = "Yellow Page Card | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function YellowPageList()
	{
		$data['page_name'] = "YellowPageList";
		$data['title'] = "Yellow Page List | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function YellowPageView($userid="")
	{
		
	    $data['userid'] = $userid;
		$data['page_name'] = "YellowPageView";
		$data['title'] = "Yellow Page View | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function Holiday()
	{
		$data['page_name'] = "Holiday";
		$data['title'] = "Holiday | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function Notification()
	{
		$data['page_name'] = "Notification";
		$data['title'] = "Notification | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function Chat($userid = '')
	{
		$data['page_name'] = "Chat";
		$data['title'] = "Chat | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}
	
//=========================================HRMS	Main Menu

	public function Dashboard()
	{
		$data['page_name'] = "Dashboard";
		$data['title'] = "Dashboard | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	
	public function Branch()
	{
		$data['page_name'] = "Branch";
		$data['title'] = "Branch | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function BranchAdd()
	{
		$data['page_name'] = "BranchAdd";
		$data['title'] = "Add Branch | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function Department()
	{
		$data['page_name'] = "Department";
		$data['title'] = "Department | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function Designation()
	{
		$data['page_name'] = "Designation";
		$data['title'] = "Designation | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function Employee()
	{
		$data['page_name'] = "Employee";
		$data['title'] = "Employee Card | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function EmployeeList()
	{
		$data['page_name'] = "EmployeeList";
		$data['title'] = "Employee List | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
 	public function EmployeeAdd()
	{
		$data['page_name'] = "EmployeeAdd";
		$data['title'] = "Add Employee | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function EmployeeView($userid = '')
	{
		$data['page_name'] = "EmployeeView";
		$data['title'] = "View Employee | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}
    public function EmployeeEdit($userid='')
	{
		$data['page_name'] = "EmployeeEdit";
		$data['title'] = "Edit Employee | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}

	public function LeaveType()
	{
		$data['page_name'] = "LeaveType";
		$data['title'] = "Leave Type | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	} 
	public function Leavepolicy_edit($policy_id='')
	{
		$data['page_name'] = "Leavepolicy_edit";
		$data['title'] = "Leave Policy Edit | Ezdat HRMS";
		$data['policy_id'] = $policy_id;
		$this->load->view('admin/main',$data);
	} 
	public function LeavePolicy()
	{
		$data['page_name'] = "LeavePolicy";
		$data['title'] = "Leave Policy | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	} 
	public function Leave()
	{
		$data['page_name'] = "Leave";
		$data['title'] = "Leave | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function LeaveComment($leave_id='')
	{
		$data['page_name'] = "LeaveComment";
		$data['title'] = "Leave Comment | Ezdat HRMS";
		$data['leave_id'] = $leave_id;
		$this->load->view('admin/main',$data);
	}

	public function Tickets()
	{
		$data['page_name'] = "Tickets";
		$data['title'] = "Tickets | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function TicketComments($ticketid = '')
	{
		$data['page_name'] = "TicketComments";
		$data['title'] = "Tickets | Ezdat HRMS";
		$data['ticketid'] = $ticketid;
		$this->load->view('admin/main',$data);
	}
//=========================================Project
	public function ProjectDashboard()
	{
		$data['page_name'] = "ProjectDashboard";
		$data['title'] = "Project Dashboard | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function Projects()
	{
		$data['page_name'] = "Projects";
		$data['title'] = "Project Card | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function ProjectDetails($projectid = '')
	{
		$data['page_name'] = "ProjectDetails";
		$data['title'] = "Project Details | Ezdat HRMS";
		$data['projectid'] = $projectid;
		$this->load->view('admin/main',$data);
	}
	public function ProjectAdd()
	{
		$data['page_name'] = "ProjectAdd";
		$data['title'] = "Add Projects | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function ProjectEdit($userid = '')
	{
		$data['page_name'] = "ProjectEdit";
		$data['title'] = "Edit Projects | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}
	


//=========================================Project
	public function CompanyDashboard()
	{
		$data['page_name'] = "CompanyDashboard";
		$data['title'] = "Company Dashboard | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function CompanyAdd()
	{
		$data['page_name'] = "CompanyAdd";
		$data['title'] = "Add Company Dashboard | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function CompanyEdit()
	{
		$data['page_name'] = "CompanyEdit";
		$data['title'] = "Edit Company Dashboard | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	


//=========================================Job 	
	public function JobDashboard()
	{
		$data['page_name'] = "JobDashboard";
		$data['title'] = "Job Dashboard | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function JobType()
	{
		$data['page_name'] = "JobType";
		$data['title'] = "Job Type | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function JobStatus()
	{
		$data['page_name'] = "JobStatus";
		$data['title'] = "Job Status | Ezdat HRMS";
		$this->load->view('admin/main', $data);
	}
	public function Jobs()
	{
		$data['page_name'] = "Jobs";
		$data['title'] = "Jobs | Ezdat HRMS";
		$this->load->view('admin/main', $data);
	}
	public function JobAdd()
	{
		$data['page_name'] = "JobAdd";
		$data['title'] = "Add Job | Ezdat HRMS";
		$this->load->view('admin/main', $data);
	}
	public function JobEdit($jobid = '')
	{
		$data['page_name'] = "JobEdit";
		$data['jobid'] = $jobid;
		$data['title'] = "Edit Job | Ezdat HRMS";
		$this->load->view('admin/main', $data);
	}
	public function JobApplicants($jobId = '')
	{
		$data['page_name'] = "JobApplicants";
		$data['jobId'] = $jobId;
		$data['title'] = "Job Applicants | Ezdat HRMS";
		$this->load->view('admin/main', $data);
	}
	
//=========================================HRMS	Payroll

	public function PayrollSalary()
	{
		$data['page_name'] = "PayrollSalary";
		$data['title'] = "Payroll Salary | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function PayrollSalaryAdd($montha = '',$yeara = '')
	{
		$data['page_name'] = "PayrollSalaryAdd";
		$data['title'] = "Add Payroll Salary | Ezdat HRMS";
		   $data['montha'] = $montha;
		   $data['yeara'] = $yeara;
		$this->load->view('admin/main',$data);
	}

	public function PayrollAddSalary()
	{
		$data['page_name'] = "PayrollAddSalary";
		$data['title'] = "Payroll Add Salary | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function PayrollAddCTC($userId='')
	{
		$data['page_name'] = "PayrollAddCTC";
		$data['title'] = "Payroll Add CTC | Ezdat HRMS";
		$data['userId'] = $userId;
		$this->load->view('admin/main',$data);
	}
	public function PayrollView2($montha = '',$yeara = '')
	{
		$data['page_name'] = "PayrollView2";
		$data['title'] = "Payroll View | Ezdat HRMS";
		   $data['montha'] = $montha;
		   $data['yeara'] = $yeara;
		$this->load->view('admin/main',$data);
	}
	public function PayrollView()
	{
		$data['page_name'] = "PayrollView";
		$data['title'] = "Payroll View | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function PaySlip($userId='' ,$montha = '',$yeara = '')
	{
		$data['page_name'] = "PaySlip";
		$data['title'] = "Payroll Slip | Ezdat HRMS";
		$data['userId'] = $userId;
		$data['montha'] = $montha;
		$data['yeara'] = $yeara;
		$this->load->view('admin/main',$data);
	}
  
  

//=========================================HRMS	Assets
	public function AssetsCategory ()
	{
		$data['page_name'] = "AssetsCategory";
		$data['title'] = "Assets Category | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function Assets()
	{
		$data['page_name'] = "Assets";
		$data['title'] = "Assets | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function AssetAdd()
	{
		$data['page_name'] = "AssetAdd";
		$data['title'] = "Add Asset | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function AssetEdit($assetcode='')
	{
		$data['page_name'] = "AssetEdit";
		$data['assetcode'] = $assetcode;
		$data['title'] = "Edit Asset | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function AssetsDetails($assetcode='')
	{
		$data['page_name'] = "AssetsDetails";
		$data['title'] = "Assets Details | Ezdat HRMS";
		$data['assetcode'] = $assetcode;
		$this->load->view('admin/main',$data);
	}
	public function AssetsManagement()
	{
		$data['page_name'] = "AssetsManagement";
		$data['title'] = "Assets Management | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}


//=========================================Reimbursement 	
	public function Reimbursement()
	{
		$data['page_name'] = "Reimbursement";
		$data['title'] = "Reimbursement | Ezdat HRMS";	
		$this->load->view('admin/main',$data);
	}
	public function ReimbursementList()
	{
		$data['page_name'] = "ReimbursementList";
		$data['title'] = "Reimbursement List | Ezdat HRMS";	
		$this->load->view('admin/main',$data);
	}
	public function ReimbursementView($reimbursementid='')
	{
		$data['page_name'] = "ReimbursementView";
		$data['title'] = "View Reimbursement | Ezdat HRMS";
		$data['reimbursementid'] = $reimbursementid;
		$this->load->view('admin/main',$data);
	}
	public function ReimbursementApproved()
	{
		$data['page_name'] = "ReimbursementApproved";
		$data['title'] = "Approved Reimbursement | Ezdat HRMS";	
		$this->load->view('admin/main',$data);
	}
	public function ReimbursementDenied()
	{
		$data['page_name'] = "ReimbursementDenied";
		$data['title'] = "Denied Reimbursement | Ezdat HRMS";	
		$this->load->view('admin/main',$data);
	}
	public function ReimbursementPolicy()
	{
		$data['page_name'] = "ReimbursementPolicy";
		$data['title'] = "Reimbursement Policy| Ezdat HRMS";	
		$this->load->view('admin/main',$data);
	}



//=========================================Module Addon
	public function moduleAddon()
	{
		$data['page_name'] = "ModuleAddon";
		$data['title'] = "module Addon | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}


//=========================================onBoarding  offBoarding
	public function onBoarding()
	{
		$data['page_name'] = "onBoarding";
		$data['title'] = "On-Boarding Pending | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function onboardingList()
	{
		$data['page_name'] = "onboardingList";
		$data['title'] = "On-Boarding Pending | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function onBoardingAddDocument($userid='')
	{
		$data['page_name'] = "onBoardingAddDocument";
		$data['title'] = "Add On-Boarding Employee Document| Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}
	public function onBoardingAddDetails($userid='')
	{
		$data['page_name'] = "onBoardingAddDetails";
		$data['title'] = "Add On-Boarding Employee Details| Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}
	public function onBoardingView($userid='')
	{
		$data['page_name'] = "onBoardingView";
		$data['title'] = "View On-Boarding | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}

	public function onBoardingComplete()
	{
		$data['page_name'] = "onBoardingComplete";
		$data['title'] = "On-Boarding Complete | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	
	public function offBoarding()
	{
		$data['page_name'] = "offBoarding";
		$data['title'] = "Off-Boarding | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function offBoardingList()
	{
		$data['page_name'] = "offboardingList";
		$data['title'] = "Off-Boarding | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function offBoardingComplete()
	{
		$data['page_name'] = "offBoardingComplete";
		$data['title'] = "Off-Boarding Comlete | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	public function offBoardingAdd($userid='')
	{
		$data['page_name'] = "offBoardingAdd";
		$data['title'] = "Add Off-Boarding | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}
	public function offBoardingView($userid='')
	{
		$data['page_name'] = "offBoardingView";
		$data['title'] = "View offBoarding | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}




	public function Attendance()
	{
		$data['page_name'] = "Attendance";
		$data['title'] = "Attendance | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function AttendanceReport()
	{
		$data['page_name'] = "AttendanceReport";
		$data['title'] = "Attendance Report| Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}



	public function Role()
	{
		$data['page_name'] = "Role";
		$data['title'] = "Role | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function UpdatePermission($userid = '')
	{
		$data['page_name'] = "UpdatePermission";
		$data['title'] = "Update Permission | Ezdat HRMS";
		$data['userid'] = $userid;
		$this->load->view('admin/main',$data);
	}

	public function LeaveSetting()
	{
		$data['page_name'] = "LeaveSetting";
		$data['title'] = "Leave Setting | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function AppSetting()
	{
		$data['page_name'] = "AppSetting";
		$data['title'] = "App Setting | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function Social()
	{
		$data['page_name'] = "Social";
		$data['title'] = "Social | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}

	public function ChangePassword()
	{
		$data['page_name'] = "ChangePassword";
		$data['title'] = "Change Password | Ezdat HRMS";
		$this->load->view('admin/main',$data);
	}
	
 	public function ProfileAdmin()
	{
		$data['page_name'] = "ProfileAdmin";
		$data['title'] = "Admin Profile | Ezdat HRMS";	
		$this->load->view('admin/main',$data);
	}

	public function Visitor()
	{
		$data['page_name'] = "Visitor";
		$data['title'] = " Visitor | Ezdat HRMS";	
		$this->load->view('admin/main',$data);
	}

	

//======404
	public function error()
	{
		$data['page_name'] = "error";
		$this->load->view('admin/main',$data);
	}
//======Privacy Policy
	public function privacy_policy()
	{
		$data['page_name'] = "privacy_policy";
		$this->load->view('admin/main',$data);
	}
}