"use strict";
 function Getdata(){
    return $.ajax({  
        url: base_url+"viewEvent",
                       // url: http://pratap.ezdatechnology.com/index.php/api/callApi/viewEvent",  
                        method:"POST",  
                        dataType:"json", 
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('Token', localStorage.token);
                        },
                        success:function(response){
                        
                           loadCalender(response.data);
                        } 
                         
                   });
   }
    var promise = Getdata();
    var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        // alert(date);
         function loadCalender(ObjData,date)
    {
$('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
    },


    defaultDate: date,
    editable: true,
    
    droppable: true, // this allows things to be dropped onto the calendar
    drop: function() {
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            $(this).remove();
        }
    },
    eventLimit: true, // allow "more" link when too many events
    events: ObjData,
});
}
    