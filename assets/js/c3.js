
$(function() {
    "use strict";
        function GetYearlyGrowthData(){
        return $.ajax({  
            url: base_url+"countEmployeeGraph",
               // url: http://pratap.ezdatechnology.com/index.php/api/callApi/viewEvent",  
                method:"POST",  
                dataType:"json", 
                beforeSend: function(xhr){
                    xhr.setRequestHeader('Token', localStorage.token);
                },
                success:function(response){
                
                   loadYearlyGraph(response.data);
                } 
                   
           });
       }
       var test = GetYearlyGrowthData();
     //alert(test);
      function loadYearlyGraph(data){
      //alert(data[0]['ticket']);
      // alert(data[]['data2']);
      //console.log(data);
      var project=data[1].data1;
      var ticket=data[0].data2;
      var attendance=data[2].data3;
      var year=data[2].year;
      console.log(year);
     var projectArr = project.split(',');
     var ticketArr = ticket.split(',');
     var attArr = attendance.split(',');
     var arryear=year.split(',');
    console.log(projectArr);
    console.log(ticketArr);
    console.log(attArr);
    //return;
        var chart = c3.generate({
            bindto: '#chart-employment', // id of chart wrapper
            data: {
                columns: [
                    // each columns data
                      projectArr,ticketArr,attArr
                    
                ],
                type: 'bar', // default type of chart
                colors: {
                    'data1': '#2B3856',
                    'data2': '#438D80',
                    'data3': '#ADD8E6'
                },
                names: {
                    // name of each serie
                    'data1': 'Ticket',
                    'data2': 'Projects',
                    'data3': 'Attendence'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    // name of each category
                   categories: arryear
                },
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 0,
                top: 0
            },
        });
    }  


    function GetGraphData(){
    return $.ajax({  
        url: base_url+"countEmployemrntGrowth",
           // url: http://pratap.ezdatechnology.com/index.php/api/callApi/viewEvent",  
            method:"POST",  
            dataType:"json", 
            beforeSend: function(xhr){
                xhr.setRequestHeader('Token', localStorage.token);
            },
            success:function(response){
            
               loadGraph(response.data);
            } 
             
       });
   }
    GetGraphData();
   
    function loadGraph(data)
    {
   // alert(data[]['data2']);
        var chart = c3.generate({
            bindto: '#chart-donut', // id of chart wrapper
            data: {
                columns: [
                    // each columns data
                    ['data1', data[0]['data1']],
                    ['data2', data[1]['data2']],
                    ['data3', data[2]['data3']]
                ],
                type: 'donut', // default type of chart
                colors: {
                    'data1': '#2B3856',
                    'data2': '#438D80',
                    'data3': '#ADD8E6'
                },
                names: {
                    // name of each serie
                    'data1': 'Project',
                    'data2': 'Tickets',
                    'data3': 'Attendence'
                }
            },
            axis: {
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 0,
                top: 0
            },
        });
    }


function GetYearlyJobData(){
    return $.ajax({  
        url: base_url+"viewChartApplication", 
            method:"POST",  
            dataType:"json", 
            beforeSend: function(xhr){
                xhr.setRequestHeader('Token', localStorage.token);
            },
            success:function(response){
            
               loadYearlyJob(response.data);
            } 
               
       });
   }
   var test = GetYearlyJobData();
 //alert(test);
   function loadYearlyJob(data)
    {
   //alert(data['0']['Applicants']);
        var chart = c3.generate({
            bindto: '#chart-combination', // id of chart wrapper
            data: {
                columns: [
                    // each columns data
                    ['data1', data[0]['data1']],
                    ['data2', data[1]['data2']],
                    ['data3', data[2]['data3']],
                    ['data4', data[3]['data4']],
                    ['data5', data[4]['data5']],
                ],
                type: 'bar', // default type of chart

                colors: {
                    'data1': '#ff8800',
                    'data2': '#4076c3',
                    'data3': '#fed284',
                    'data4': '#34738a',
                    'data5': '#4076c3'
                },
                names: {
                    // name of each serie
                    'data1': 'Applications',
                    'data2': 'Applied',
                    'data3': 'Shortlisted',
                    'data4': 'Interviwed',
                    'data5': 'Rejected'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    // name of each category
                     categories: [data[0]['Applicants']]
                },
            },
            bar: {
                width: 15
            },
            legend: {
                show: true, //hide legend
            },
            padding: {
                bottom: 0,
                top: 0
            },
        });
    }    
});





$(function() {
    "use strict";
    // Current Ticket Status
    $(document).ready(function(){
        var chart = c3.generate({
            bindto: '#chart-combination-ticket', // id of chart wrapper
            data: {
                columns: [
                    // each columns data
                    ['data1', 1,2,4,9,6,3,2,5,8,7],
                    ['data2', 7,5,2,1,6,4,9,8,3,2],
                    ['data3', 7,5,3,1,5,9,8,5,2,6],
                    ['data4', 1,2,3,5,4,8,5,2,6,1],
                ],
                type: 'bar', // default type of chart
                types: {
                    'data2': "line",
                    'data3': "spline",
                },
                groups: [
                    [ 'data1', 'data4']
                ],
                colors: {
                    'data1': '#fed284',
                    'data2': '#ff7f81',
                    'data3': '#44b39b',
                    'data4': '#004660'
                },
                names: {
                    // name of each serie
                    'data1': 'Development',
                    'data2': 'Marketing',
                    'data3': 'Design',
                    'data4': 'Sales'
                }
            },
            axis: {
                x: {
                    type: 'category',
                    // name of each category
                    categories: ['Jun 1','Jun 2','Jun 3','Jun 4','Jun 5','Jun 6','Jun 7','Jun 8','Jun 9','Jun 10']
                },
            },
            bar: {
                width: 16
            },
            legend: {
                show: false, //hide legend
            },
            padding: {
                bottom: 0,
                top: 0
            },
        });
    });

});